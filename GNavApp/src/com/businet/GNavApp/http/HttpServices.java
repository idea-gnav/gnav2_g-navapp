package com.businet.GNavApp.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpServices {

	public static enum RequestType {

		POST, GET
	}


	//public static String USER_AGENT = "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52";

	public static String sendHttpRequest(
			String url,
			RequestType requestType,
			String params,
			String authorizationToken)
			throws IOException {


		try {
			System.out.println("SEND " + requestType.toString() + " REQUEST TO [" + url + "] , PARAMS [" + params + "]"
					+ " TOKEN = [" + authorizationToken + "]");

			URL obj = new URL(url);

			HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
			httpURLConnection.setRequestMethod(requestType.toString());
			httpURLConnection.setRequestProperty("Accept", "*/*");

			httpURLConnection.setRequestProperty("jdk.http.auth.tunneling.disabledSchemes", "");

			if (authorizationToken != null)
				httpURLConnection.setRequestProperty("Authorization", authorizationToken); // set Authorization Token for
																						// request

		    httpURLConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

			httpURLConnection.setDoOutput(true);
			OutputStream os = httpURLConnection.getOutputStream();

			if (params != null) {
				os.write(params.getBytes());
			}

			os.flush();
			os.close();

			int responseCode = httpURLConnection.getResponseCode();
			System.out.println("Http Response Code :: " + responseCode);


			if (responseCode == HttpURLConnection.HTTP_OK) { // success

				BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(),"UTF-8"));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				System.out.println("RESULT : " + response.toString());

				return response.toString();
			} else {
				System.out.println("unsuccess");
			}

			return null;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return "Exeption";
		}


	}
}
