package com.businet.GNavApp.resources;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.MachineArrayListModel;
import com.businet.GNavApp.models.MachineListModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Commons;



@Path("/constructionList")
@Stateless
public class ConstructionListResource {

	private static final Logger logger = Logger.getLogger(ConstructionListResource.class.getName());

	@EJB
	IMachineService machineService;

	@EJB
	IUserService userService;

	@EJB
	IDtcDitailService dtcDitailService;


	/**
	 * [API No.47] 施工情報一覧取得
	 * @param userId - ユーザーID
	 * @param kibanSelect - 機番選択区分
	 * @param customerNamePriority - お客様名検索優先フラグ
	 * @param kibanInput - 機番入力
	 * @param serialNumber - 機械管理番号
	 * @param searchFlg - 検索条件
	 * @param sortFlg - 並び替え条件
	 * @param customerManagementNoFrom - お客様管理番号From
	 * @param customerManagementNoTo - お客様管理番号To
	 * @param manufacturerSerialNumberFrom - SCM機番From
	 * @param manufacturerSerialNumberTo - SCM機番To
	 * @param scmModel - SCM型式
	 * @param customerName - お客様名
	 * @param warningCurrentlyInProgress - DTC発生中
	 * @param warningDateFrom - DTC検索From
	 * @param warningDateTo - DTC検索To
	 * @param hourMeterFrom - アワメータ開始
	 * @param hourMeterTo - アワメータ終了
     * @param dealerCd -代理店コード
	 * @param fuelLevel - 燃料レベル
	 * @param defLevel - 尿素レベル
	 * @param favoriteSearchFlg - お気に入り
	 * @param toukatsubuCd - 統括部コード
	 * @param kyotenCd - 拠点コード
	 * @param serviceKoujyouCd - サービス工場コード
	 * @param searchRangeFromIdo - 検索範囲中心緯度
	 * @param searchRangeFromKeido - 検索範囲中心経度
	 * @param searchKibanInput - 検索範囲中心機番入力
	 * @param searchRangeDistance - 検索範囲距離
	 * @param constructionInput - 施工情報入力
	 * @param searchRangeUnit - 単位選択区分
	 * @param startRecord - レコード開始位置
	 * @param deviceLanguage - デバイス言語
	 * @return Response.JSON MachineListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
			@FormParam("userId") String userId,
			@FormParam("kibanSelect") Integer kibanSelect,
			@FormParam("customerNamePriority") Integer customerNamePriority,
			@FormParam("kibanInput") String kibanInput,
			@FormParam("serialNumber") Long serialNumber,
			@FormParam("searchFlg") Integer searchFlg,
			@FormParam("sortFlg") Integer sortFlg,
			@FormParam("customerManagementNoFrom") String customerManagementNoFrom,
			@FormParam("customerManagementNoTo") String customerManagementNoTo,
			@FormParam("manufacturerSerialNumberFrom") String manufacturerSerialNumberFrom,
			@FormParam("manufacturerSerialNumberTo") String manufacturerSerialNumberTo,
			@FormParam("scmModel") String _scmModel,
			@FormParam("customerName") String customerName,
			@FormParam("warningCurrentlyInProgress") Integer warningCurrentlyInProgress,
			@FormParam("warningDateFrom") String warningDateFrom,
			@FormParam("warningDateTo") String warningDateTo,
			@FormParam("hourMeterFrom") Integer hourMeterFrom,
			@FormParam("hourMeterTo") Integer hourMeterTo,
			@FormParam("dealerCd") String dealerCd,
			@FormParam("fuelLevel") Integer fuelLevel,
			@FormParam("defLevel") Integer defLevel,
			@FormParam("favoriteSearchFlg") Integer favoriteSearchFlg,
			@FormParam("toukatsubuCd") String toukatsubuCd,
			@FormParam("kyotenCd") String kyotenCd,
			@FormParam("serviceKoujyouCd") String serviceKoujyouCd,
			@FormParam("searchRangeFromIdo") Double searchRangeFromIdo,
			@FormParam("searchRangeFromKeido") Double searchRangeFromKeido,
			@FormParam("searchKibanInput") String searchKibanInput,
			@FormParam("searchRangeDistance") Integer searchRangeDistance,
			@FormParam("constructionInput") Integer constructionInput,
			@FormParam("searchRangeUnit") Integer searchRangeUnit,
			@FormParam("startRecord") Integer startRecord,
			@FormParam("deviceLanguage") Integer deviceLanguage
			) {


		logger.info("[GNAV][POST] userId="+userId+", deviceLanguage="+deviceLanguage+", kibanSelect="+kibanSelect+", customerNamePriority="+customerNamePriority+", kibanInput="+kibanInput+", searchFlg="+searchFlg
				+", sortFlg="+sortFlg+", serialNumber="+serialNumber+", customerManagementNoFrom="+customerManagementNoFrom+", customerManagementNoTo="+customerManagementNoTo
				+", manufacturerSerialNumberFrom="+manufacturerSerialNumberFrom+", manufacturerSerialNumberTo="+manufacturerSerialNumberTo
				+", scmModel="+_scmModel+", customerName="+customerName+", warningCurrentlyInProgress="+warningCurrentlyInProgress
				+", warningDateFrom="+warningDateFrom+", warningDateTo="+warningDateTo+", hourMeterFrom="+hourMeterFrom+", hourMeterTo="+hourMeterTo
				+", dealerCd="+dealerCd+", fuelLevel="+fuelLevel+", defLevel="+defLevel+", favoriteSearchFlg="+favoriteSearchFlg+", toukatsubuCd="+toukatsubuCd
				+", kyotenCd="+kyotenCd+", serviceKoujyouCd="+serviceKoujyouCd+", searchRangeFromIdo="+searchRangeFromIdo+", searchRangeFromKeido="+searchRangeFromKeido
				+", searchKibanInput="+searchKibanInput+", searchRangeDistance="+searchRangeDistance+", constructionInput="+constructionInput+", searchRangeUnit="+searchRangeUnit+", startRecord="+startRecord
				);

		MachineListModel machineListModel = new MachineListModel();
		try {


			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);


			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String groupNo = null;
			if(user.get(0)[3]!=null)
				groupNo = (String)user.get(0)[3];

			String kengenCd = null;
			if(user.get(0)[4]!=null)
				kengenCd = user.get(0)[4].toString();

			// (user.get(0)[4]).toString()


			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();



			// 検索点中央位置の緯度・経度をセット
			Double _searchRangeFromIdo = searchRangeFromIdo;
			Double _searchRangeFromKeido = searchRangeFromKeido;

			if(searchKibanInput != null) {
				List<Object[]> latlng = machineService.findByLatlngNativeQuery(searchKibanInput);

				if(latlng!=null && (latlng.get(0)[0]!=null && latlng.get(0)[1]!=null)) {
					_searchRangeFromIdo = Double.parseDouble(latlng.get(0)[0].toString());
					_searchRangeFromKeido = Double.parseDouble(latlng.get(0)[1].toString());
				}else {
					// 特定位置検索の機番が存在しない又は複数ある、又は機番に緯度経度がない場合は処理ステータス1601を返却
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_LATLNG)).build();
				}
			}
			machineListModel.setSearchRangeFromIdo(_searchRangeFromIdo);
			machineListModel.setSearchRangeFromKeido(_searchRangeFromKeido);

			//道路機械のみ検索
			String machineType = "1";

			//施工情報検索
			Integer constructionSearchFlg = 1;


			/*
			 * Get Machines
			 */
			List<Object[]> machines = machineService.findByMachineListNativeQuery(
					userId,
					kibanSelect,
					// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
					customerNamePriority,
					// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//
					kibanInput,
					searchFlg,
					sortFlg,
					customerManagementNoFrom,
					customerManagementNoTo,
					manufacturerSerialNumberFrom,
					manufacturerSerialNumberTo,
					machineType,
					_scmModel,
					customerName,
					warningCurrentlyInProgress,
					warningDateFrom,
					warningDateTo,
					null,
					hourMeterFrom,
					hourMeterTo,
					dealerCd,
					fuelLevel,
					defLevel,
					null,
					null,
					null,
					favoriteSearchFlg,
					toukatsubuCd,
					kyotenCd,
					serviceKoujyouCd,
					typeFlg,
					kigyouCd,
					sosikiCd,
					groupNo,
					kengenCd,
					_searchRangeFromIdo,
					_searchRangeFromKeido,
					searchRangeDistance,
					null,
					null,
					searchRangeUnit,
					languageCd,
					null,
					null,
					constructionInput,
					constructionSearchFlg,
					// **↓↓↓　2021/02/18  iDEA山下 定期整備検索5型除外対応↓↓↓**//
					null
					// **↑↑↑　2021/02/18  iDEA山下 定期整備検索5型除外対応↑↑↑**//
					);

			ArrayList<MachineArrayListModel> machineList = new ArrayList<MachineArrayListModel>();

			if(machines != null) {
				Integer maxCount = Constants.CON_MAX_COUNT;
				if(machines.size() >= maxCount )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				Integer record = startRecord-1;
				for(int counter =  record; counter < (record+50) ; counter++ ) {

					MachineArrayListModel machineArrayListModel = new MachineArrayListModel();


					if( machines.size() == counter) {
						break;}

					Object[] machine = machines.get(counter);

					Long kibanSerno = 0l;
					if(machine[0]!=null)
						kibanSerno = ((Number)machine[0]).longValue();

					String manufacturerSerialNumber = "";
					if(machine[1]!=null)
						manufacturerSerialNumber = (String)machine[1];


					String customerManagementNo = "";
					if(machine[3]!=null)
						customerManagementNo = (String)machine[3];

					String customerManagementName = "";
					if(machine[4]!=null)
						customerManagementName = (String)machine[4];

					String scmModel = "";
					if(machine[5]!=null)
						scmModel = (String)machine[5];

					Double ido = null;
					if(machine[8]!=null)
						ido =  ConvertUtil.parseLatLng(((Number)machine[8]).intValue());

					Double keido = null;
					if(machine[9]!=null)
						keido = ConvertUtil.parseLatLng(((Number)machine[9]).intValue());


					double hourMeter = 0;
					if(machine[10]!=null)
						hourMeter = ((Number)machine[10]).doubleValue();

					Integer nenryoLv = null;
					if(machine[11]!=null)
						nenryoLv = ((Number)machine[11]).intValue();

					Integer uraWaterLv = null;
					if(machine[12]!=null)
						uraWaterLv = ((Number)machine[12]).intValue();

					Timestamp latestUtcCommonTimestamp = null;
					String latestUtcCommonString = null;
					if(machine[13]!=null) {
						latestUtcCommonTimestamp = (Timestamp)machine[13];
						latestUtcCommonString = ConvertUtil.formatYMD(latestUtcCommonTimestamp);
					}

					String conType = null;
					if(machine[14]!=null)
						conType = (String)machine[14];

					Integer machineModel = null;
					if(machine[15]!=null)
						machineModel = ((Number)machine[15]).intValue();

					Integer constructionFlg = 0;
					if(machine[17]!=null)
						constructionFlg = 1;



					//機械管理番号
					machineArrayListModel.setSerialNumber(kibanSerno);
					//機械カテゴリー区分
					machineArrayListModel.setMachineModelCategory(Commons.identifyKibanType(manufacturerSerialNumber,conType));
					//SCM機番
					machineArrayListModel.setManufacturerSerialNumber(manufacturerSerialNumber);
					//お客様管理番号
					machineArrayListModel.setCustomerManagementNo(customerManagementNo);
					//お客様名
					machineArrayListModel.setCustomerManagementName(customerManagementName);
					//SCMモデル
					machineArrayListModel.setScmModel(scmModel);
					//最新所在地
					String positionStr = machineService.findByMachinePosition(kibanSerno, conType, languageCd);
					if(positionStr!=null)
						machineArrayListModel.setLatestLocation(positionStr);
					//緯度
					machineArrayListModel.setIdo(ido);
					//経度
					machineArrayListModel.setKeido(keido);
					//最新アワメータ
					machineArrayListModel.setHourMeter(Math.floor(hourMeter/60.0*10)/10);
					//最新通信日
					machineArrayListModel.setLatestUtcCommonDateTime(latestUtcCommonString);
					//燃料レベル
					machineArrayListModel.setFuelLevel(nenryoLv);
					//尿素レベル
					machineArrayListModel.setDefLevel(uraWaterLv);
					//施工情報登録フラグ
					machineArrayListModel.setConstructionFlg(constructionFlg);
					// 機械アイコン
					DisplayConditionModel displayCondition = Commons.machineIconType(machineService.findByMachineTypeControl(conType, machineModel), manufacturerSerialNumber);
					machineArrayListModel.setIconType(displayCondition.getMachineIconType());

					// **↓↓↓　2020/01/30  iDEA山下 発生中警報レベル追加 ↓↓↓**//

					//定期整備 レベル取得
					Integer periodiclevel = 0;
					if(machine[16]!=null)
						periodiclevel = ((Number)machine[16]).intValue();

					//DTC レベル取得
					List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
							kibanSerno, 1, null, null, null, Integer.parseInt(kengenCd.toString()), ConvertUtil.convertLanguageCd(deviceLanguage),1);
					Integer dtcLv = 1;

					if(dtcs != null) {
						Object[] dtc = dtcs.get(0);
						if(dtc[8]!=null)
							dtcLv = Integer.parseInt(dtc[8].toString());
					}

					//警報アイコン種別
					machineArrayListModel.setAlertLevelM(Commons.alertLevel(dtcLv, periodiclevel));

					// **↑↑↑　2020/01/30  iDEA山下 発生中警報レベル追加 ↑↑↑**//



					machineList.add(machineArrayListModel);
				}

				machineListModel.setMachineList(machineList);
				machineListModel.setMachineCount(machines.size());

			}else {
				machineListModel.setMachineCount(0);
			}


			machineListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(machineListModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
