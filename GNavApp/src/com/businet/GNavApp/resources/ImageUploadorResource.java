package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.Consumes;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.construction.IConstructionService;
import com.businet.GNavApp.ejbs.surveyService.ISurveyService;
import com.businet.GNavApp.models.ImageUploadModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Base64;
import com.businet.GNavApp.util.BunaryToBufferedImage;

@Path("/imageUploador")
@Stateless
public class ImageUploadorResource {

	private static final Logger logger = Logger.getLogger(ImageUploadorResource.class.getName());

	@EJB
	ISurveyService surveyService;

	@EJB
	IConstructionService constructionService;
	
	/**
	 * [API No.35] 画像アップロード
	 * 
	 * @param userId
	 * @param imageType
	 * @param code1
	 * @param code2
	 * @param code3
	 * @param image
	 * @param deviceLanguage
	 * @return Response.JSON Status
	 */

	@POST
	@Consumes(MediaType.APPLICATION_JSON) // add
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(ImageUploadModel imageUpload) {

		logger.info("[GNAV][POST] userId=" + imageUpload.getUserId() + ", imageType=" + imageUpload.getImageType()
				+ ", code1=" + imageUpload.getCode1() + ", code2=" + imageUpload.getCode2() + ", code3="
				+ imageUpload.getCode3() + ", image=" + imageUpload.getImage() + ", deviceLanguage="
				+ imageUpload.getDeviceLanguage());

		try {

			String userId = imageUpload.getUserId();
			Integer imageType = imageUpload.getImageType();
			String code1 = imageUpload.getCode1();
			String code2 = imageUpload.getCode2();
			String code3 = imageUpload.getCode3();
			String image = imageUpload.getImage();
			int deviceLanguage = imageUpload.getDeviceLanguage();
			
			if(imageUpload.getImageType()==1) {
				String fileName = "survey-" + code1 + "-" + code2 + ".jpg";
				String outputImagePath = Constants.FILE_STORAGE_IMAGE_PATH + "SurveyResultImage/"+ fileName;

				if (image != null && image.length() > 0) {

					saveStorageImageFile(outputImagePath, image);

					// Process Survey image
					if (imageType == 1) {
						Integer surveyRsId = Integer.parseInt(code1);
						Integer questionId = Integer.parseInt(code2);
						Integer rs = surveyService.updateSurveyImage(fileName, userId, surveyRsId, questionId);
					}

				}				
			}else if(imageUpload.getImageType()==2) {
				String fileName = "constraction-" + code1 + "-" + code2 + ".jpg";
				String outputImagePath = Constants.FILE_STORAGE_IMAGE_PATH  + "ConstructionImage/"+ fileName;
				if (image != null && image.length() > 0) {
					saveStorageImageFile(outputImagePath, image);
					Integer constructionResultId = Integer.parseInt(code1);
					Integer photoNo = Integer.parseInt(code2);
					Integer rs = constructionService.updateConstructionResultPhoto(fileName, userId,constructionResultId,photoNo);
				}
			}
			
			return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}

	private void saveStorageImageFile(String outputImagePath, String image) throws IOException {

		logger.config("Image decode : ImageIO.write : Start.");

		byte[] decodeBinary = Base64.decodeBase64(image);
		logger.config("Image decode : ImageIO.write : decodeBinary OK!");

		BufferedImage bufferedImage = BunaryToBufferedImage.readBinary(decodeBinary);
		logger.config("Image decode : ImageIO.write : ImageIO.read OK! ");

		ByteArrayOutputStream outPutStream = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "jpg", outPutStream);

		logger.config("Image decode : ImageIO.write : OutPutStream OK!");

		BufferedImage bufImage = ImageIO.read(new ByteArrayInputStream(outPutStream.toByteArray()));
		FileOutputStream out = new FileOutputStream(outputImagePath);
		ImageIO.write(bufImage, "jpg", out);
		out.close();
		logger.config("Image decode : ImageIO.write : FileOutputStream OK!");
		logger.config("Image decode : ImageIO.write : End.");

		return;
	}

	private void deleteStorageImageFile(String outputImagePath) throws IOException {

		logger.config("deleteStorageImageFile : outputImagePath=" + outputImagePath);

		File file = new File(outputImagePath);
		if (file.exists()) {
			if (file.delete()) {
				logger.config("Image file deleted.");
			} else {
				logger.warning("Image file deletion failed.");
			}
		} else {
			logger.warning("Image file not found.");
		}

	}
}
