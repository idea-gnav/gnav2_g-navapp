package com.businet.GNavApp.resources;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.report.IReportService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.entities.TblTeijiReport;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.GraphReportModel;
import com.businet.GNavApp.models.ReportOperatingListModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Commons;

@Path("/graphReport")
@Stateless
public class GraphReportResource {

	private static final Logger logger = Logger.getLogger(GraphReportResource.class.getName());

	private double unitF=1;

	private int constructionFlg=0;

	@EJB
	IReportService reportService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;


	/**
	 * [API No.11] TññÚ×æ¾
	 * @param serialNumber
	 * @param searchDateFrom
	 * @param searchDateTo
	 * @param userId
	 * @return Response.JSON GraphReportModel
	 * @throws ParseException
	 */
	@SuppressWarnings("deprecation")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response getReport(
				@FormParam("serialNumber") Long serialNumber,
				@FormParam("searchDateFrom") String searchDateFrom,
				@FormParam("searchDateTo") String searchDateTo,
				@FormParam("userId") String userId,
				@FormParam("deviceLanguage") Integer deviceLanguage
			) throws ParseException {

		logger.info("[GNAV][POST] userId="+userId+", serialNumber="+serialNumber+", searchDateFrom="+searchDateFrom+", searchDateTo="+searchDateTo+", deviceLanguage="+deviceLanguage);

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("#.#");
			GraphReportModel graphReportModel = new GraphReportModel();

			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);
			Date dateFrom = new Date(sdf.parse(searchDateFrom).getTime());
			Date dateTo = new Date(sdf.parse(searchDateTo).getTime());

			MstMachine machine = machineService.findByMachine(serialNumber);
			if (machine!=null) {

				graphReportModel.setSerialNumber(serialNumber);

				graphReportModel.setYearMonth(new SimpleDateFormat("yyyy-MM").format(dateFrom));

				graphReportModel.setCustomerManagementNo(machine.getUserKanriNo());

				graphReportModel.setManufacturerSerialNumber(machine.getKiban());

//				@BJeSæª	machineModelCategory
				graphReportModel.setMachineModelCategory(Commons.identifyKibanType(machine.getKiban(),machine.getConType()));

//				SCM^® scmModel
				graphReportModel.setScmModel(machine.getModelCd());

//				ÅVÌÝn latestLocation
				String positionStr = machineService.findByMachinePosition(machine.getKibanSerno(), machine.getConType(), languageCd);
				if(positionStr!=null)
					graphReportModel.setLatestLocation(positionStr);

//				ÅVÜx ido
				if(machine.getNewIdo() != null)
					graphReportModel.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));

//				ÅVox keido
				if(machine.getNewKeido() != null)
					graphReportModel.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));

				if(machine.getNewHourMeter()!=null)
					graphReportModel.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);

//				R¿gpÊ
				graphReportModel.setFuelLevel(machine.getNenryoLv());

//				AfÁïÊ
				graphReportModel.setDefLevel(machine.getUreaWaterLevel());

//				ÅIÊMú
				if(machine.getRecvTimestamp()!=null)
					graphReportModel.setLatestUtcCommonDateTime(sdf.format(machine.getRecvTimestamp()));

//				¨ql¼
				if(machine.getSosikiCd()!=null)
					graphReportModel.setCustomerManagementName(sosikiService.findByKigyouSosikiName(machine.getSosikiCd()));

//				Ú§ätO		constructionFlg
				DisplayConditionModel displayCondition = Commons.machineIconType(machineService.findByMachineTypeControl(machine.getConType(), machine.getMachineModel()), machine.getKiban());
				// @BACR
				graphReportModel.setIconType(displayCondition.getMachineIconType());
				// ÚtO
				constructionFlg = displayCondition.getGrapheReportFlg();
				graphReportModel.setConstructionFlg(constructionFlg);

			}



//			|[gîñæ¾
			java.sql.Timestamp to = new java.sql.Timestamp(dateTo.getTime());
			to.setHours(23);
			to.setMinutes(59);
			to.setSeconds(59);
			List<Object[]> objList = reportService.getAllInfo(serialNumber, new java.sql.Timestamp(dateFrom.getTime()), to);

		if (objList != null && objList.size()>0) {

			List<TblTeijiReport> reportInfoList = parseReport(objList);

			int reportCount = reportInfoList.size();
			logger.config("[GNAV][DEBUG] reportListCount="+reportCount);

			if (reportCount > 0) {

				List<ReportOperatingListModel> dailyRpList = new ArrayList<ReportOperatingListModel>();
				BigDecimal engineOperatingAllTotal = new BigDecimal(0);
				BigDecimal machineOperatingAllTotal = new BigDecimal(0);
				BigDecimal nenryoConsumAllTotal = new BigDecimal(0);
				BigDecimal waterConsumAllTotal = new BigDecimal(0);
				BigDecimal constructionTimeAllTotal = new BigDecimal(0);


//				úWv
				for (TblTeijiReport rp : reportInfoList) {

					ReportOperatingListModel dayRp = new ReportOperatingListModel();

					// @Bìú
					dayRp.setDate(sdf.format(rp.getKikaiKadobiLocal()));
					logger.config("[GNAV][DEBUG][D] kikaiKadobiLocal="+sdf.format(rp.getKikaiKadobiLocal()));

					// GWÒ­Ô engineOperatingTime
					BigDecimal engineOperating = new BigDecimal(0);
					if(rp.getHourMeter() != null) {
						engineOperating = new BigDecimal(rp.getHourMeter());
						dayRp.setEngineOperatingTime(ConvertUtil.convertTimeHhMm(rp.getHourMeter()));
						engineOperatingAllTotal = engineOperatingAllTotal.add(new BigDecimal(rp.getHourMeter()));
						logger.config("[GNAV][DEBUG][D] engineOperatingTime="+engineOperating.doubleValue());
					}else {
						// add HH:MM aishikawa 2018/10/16 :Start
						dayRp.setEngineOperatingTime("00:00");	//nullÌêA¾¦IÉ\¦Î
						// add HH:MM aishikawa 2018/10/16 :End
					}

					// @BìÔ machineOperatingTime
					BigDecimal machineOperating = new BigDecimal(0);
					if (rp.getKikaiSosaTime() != null) {
						machineOperating = new BigDecimal(rp.getKikaiSosaTime());
						dayRp.setMachineOperatingTime(ConvertUtil.convertTimeHhMm(rp.getKikaiSosaTime()));
						machineOperatingAllTotal = machineOperatingAllTotal.add(new BigDecimal(rp.getKikaiSosaTime()));
						logger.config("[GNAV][DEBUG][D] machineOperatingTime="+machineOperating.doubleValue());
					}else {
						// add HH:MM aishikawa 2018/10/16 :Start
						dayRp.setMachineOperatingTime("00:00");	//nullÌêA¾¦IÉ\¦Î
						// add HH:MM aishikawa 2018/10/16 :End
					}


					// R¿ÁïÊ
					BigDecimal fuelConsumption = new BigDecimal(0);
					if(rp.getNenryoConsum() != null) {
						fuelConsumption = new BigDecimal(df.format(rp.getNenryoConsum()));
						dayRp.setFuelConsumption(fuelConsumption.doubleValue());
						nenryoConsumAllTotal = nenryoConsumAllTotal.add(fuelConsumption);
						logger.config("[GNAV][DEBUG][D] fuelConsumption="+fuelConsumption.doubleValue());
					}


					// AfÁïÊ
					BigDecimal defConsumption = new BigDecimal(0);
					if(rp.getUreaWaterConsum() != null) {
						defConsumption = new BigDecimal(df.format(Math.floor(rp.getUreaWaterConsum()/100)/10)).setScale(1, BigDecimal.ROUND_HALF_UP);
						logger.config("[GNAV][DEBUG][D] defConsumption="+((rp.getUreaWaterConsum()/unitF)/1000)+" :noformat");
						dayRp.setDefConsumpiton(defConsumption.doubleValue());
						waterConsumAllTotal = waterConsumAllTotal.add(defConsumption);
						logger.config("[GNAV][DEBUG][D] defConsumption="+defConsumption.doubleValue());
					}

					// Rï fuelEffciency
					// **««« 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  «««**//
					BigDecimal fuelEffciency = new BigDecimal(0);
					if(fuelConsumption.doubleValue()>0 && engineOperating.doubleValue()>0 ) {
						// **««« 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  «««**//
//						double hourMeter = new BigDecimal(df.format(Math.floor(engineOperating.doubleValue()/60.0*10)/10)).doubleValue();
//						if(hourMeter>0) {
//							fuelEffciency = new BigDecimal(fuelConsumption.doubleValue()/hourMeter).setScale(1, BigDecimal.ROUND_HALF_UP);
//						BigDecimal fuelEffciency = new BigDecimal(0);
						if(machine.getConType().equals("ND") || machine.getConType().equals("D")) {
							fuelEffciency = new BigDecimal(rp.getNenryoConsum()/rp.getHourMeter()*60).setScale(1, BigDecimal.ROUND_HALF_UP);
						}else {
							Double hourMeter = new BigDecimal(df.format(Math.floor(engineOperating.doubleValue()/60.0*10)/10)).doubleValue();
							if(hourMeter>0 && !hourMeter.equals(0.0)) {
								fuelEffciency = new BigDecimal(fuelConsumption.doubleValue()/hourMeter).setScale(1, BigDecimal.ROUND_HALF_UP);
							}
						}
//						logger.config("[GNAV][DEBUG][D] fuelEffciency="+fuelEffciency.doubleValue());
//						dayRp.setFuelEffciency(fuelEffciency.doubleValue());
						// **ªªª 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  ªªª**//
					}
					logger.config("[GNAV][DEBUG][D] fuelEffciency="+fuelEffciency.doubleValue());
					dayRp.setFuelEffciency(fuelEffciency.doubleValue());
					// **ªªª 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  ªªª**//

					//ACh¦ idlePercentage
//					if(engineOperating.doubleValue()>0 && machineOperating.doubleValue()>0) {
					if(engineOperating.doubleValue()>0) {
						double idleTime = new BigDecimal(engineOperating.doubleValue()-machineOperating.doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
						logger.config("[GNAV][DEBUG][D] idleTime="+idleTime);
						if(idleTime > 0) {
							BigDecimal idlePercentage = new BigDecimal((idleTime*100/engineOperating.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);
							dayRp.setIdlePercentage(idlePercentage.doubleValue());
							logger.config("[GNAV][DEBUG][D] idlePercentage="+idlePercentage.doubleValue());
						}
					}

					if(constructionFlg==1) {

						// {HÔ
						BigDecimal constructionTime = new BigDecimal(0);

						if(rp.getSekoTime() != null) {
							constructionTime = new BigDecimal(rp.getSekoTime());
							dayRp.setConstructionTime(ConvertUtil.convertTimeHhMm(rp.getSekoTime()));
							constructionTimeAllTotal = constructionTimeAllTotal.add(new BigDecimal(rp.getSekoTime()));
							logger.config("[GNAV][DEBUG][D] constructionTime="+constructionTime.doubleValue());
						}else {
							// add HH:MM aishikawa 2018/10/16 :Start
							if(machine.getConType().equals("D") || machine.getConType().equals("ND"))
								dayRp.setConstructionTime("00:00");	//nullÌêA¾¦IÉ\¦Î
							// add HH:MM aishikawa 2018/10/16 :End
						}

						//{HACh¦ conIdlePercentage
						if(engineOperating.doubleValue()>0) {
							double conIdleTime = new BigDecimal(engineOperating.doubleValue()-constructionTime.doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
							logger.config("[GNAV][DEBUG][D] conIdleTime="+conIdleTime);
							if(conIdleTime > 0) {
								BigDecimal idlePercentage = new BigDecimal((conIdleTime*100/engineOperating.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);
								dayRp.setConstructionPercentage(idlePercentage.doubleValue());
								logger.config("[GNAV][DEBUG][D] idlePercentage="+idlePercentage.doubleValue());
							}
						}

					}


					dailyRpList.add(dayRp);
				}
				graphReportModel.setOperatingPartialList(dailyRpList);


//		WeeklyTotalF Tv
				logger.config("[GNAV][DEBUG][W] getWeeklyTotal()");
				// **««« 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  «««**//
//				graphReportModel.setOperatingWeekTotalList(getWeeklyTotal(reportInfoList, dateFrom, dateTo, constructionFlg));
				graphReportModel.setOperatingWeekTotalList(getWeeklyTotal(reportInfoList, dateFrom, dateTo, constructionFlg, machine.getConType()));
				// **ªªª 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  ªªª**//


//		TotalF v
				graphReportModel.setEngineOperatingTimeTotal(ConvertUtil.convertTimeHhMm(engineOperatingAllTotal.doubleValue()));
				graphReportModel.setMachineOperatingTimeTotal(ConvertUtil.convertTimeHhMm(machineOperatingAllTotal.doubleValue()));
				graphReportModel.setFuelConsumptionTotal(nenryoConsumAllTotal.doubleValue());
				graphReportModel.setDefConsumpitonTotal(waterConsumAllTotal.doubleValue());
				logger.config("[GNAV][DEBUG][M] engineOperating="+engineOperatingAllTotal.doubleValue());
				logger.config("[GNAV][DEBUG][M] machineOperating="+machineOperatingAllTotal.doubleValue());
				logger.config("[GNAV][DEBUG][M] nenryoConsum="+nenryoConsumAllTotal.doubleValue());
				logger.config("[GNAV][DEBUG][M] waterConsum="+waterConsumAllTotal.doubleValue());

				// Rï
				// **««« 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  «««**//
				BigDecimal fuelEffciency = new BigDecimal(0);
				if(nenryoConsumAllTotal.doubleValue()>0 && engineOperatingAllTotal.doubleValue()>0) {
					// **««« 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  «««**//
//					double hourMeterAllTotal = new BigDecimal(df.format(Math.floor(engineOperatingAllTotal.doubleValue()/60.0*10)/10)).doubleValue();
//					if(hourMeterAllTotal>0) {
//						BigDecimal fuelEffciency = new BigDecimal(nenryoConsumAllTotal.doubleValue()/hourMeterAllTotal).setScale(1, BigDecimal.ROUND_HALF_UP);
//						graphReportModel.setFuelEffciencyTotal(fuelEffciency.doubleValue());
//						logger.config("[GNAV][DEBUG][M] fuelEffciency="+fuelEffciency.doubleValue());
//					}
					// **««« 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  «««**//
//					BigDecimal fuelEffciency = new BigDecimal(0);
					if(machine.getConType().equals("ND") || machine.getConType().equals("D")) {
						fuelEffciency = new BigDecimal(nenryoConsumAllTotal.doubleValue()/engineOperatingAllTotal.doubleValue()*60).setScale(1, BigDecimal.ROUND_HALF_UP);
					}else {
						double hourMeterAllTotal = new BigDecimal(df.format(Math.floor(engineOperatingAllTotal.doubleValue()/60.0*10)/10)).doubleValue();
						if(hourMeterAllTotal>0) {
							fuelEffciency = new BigDecimal(nenryoConsumAllTotal.doubleValue()/hourMeterAllTotal).setScale(1, BigDecimal.ROUND_HALF_UP);
						}
					}
//					graphReportModel.setFuelEffciencyTotal(fuelEffciency.doubleValue());
//					logger.config("[GNAV][DEBUG][M] fuelEffciency="+fuelEffciency.doubleValue());
					// **ªªª 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  ªªª**//
				}
				graphReportModel.setFuelEffciencyTotal(fuelEffciency.doubleValue());
				logger.config("[GNAV][DEBUG][M] fuelEffciency="+fuelEffciency.doubleValue());
				// **ªªª 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  ªªª**//

				// ACh¦
				if(engineOperatingAllTotal.doubleValue()>0) {
					double idleTime = new BigDecimal((engineOperatingAllTotal.doubleValue()-machineOperatingAllTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();	// Ûßë·Î
					logger.config("[GNAV][DEBUG][M] idleTime="+(engineOperatingAllTotal.doubleValue()-machineOperatingAllTotal.doubleValue())+" :noformat");
					logger.config("[GNAV][DEBUG][M] idleTime="+idleTime);
					if(idleTime > 0) {
						BigDecimal idlePercentage = new BigDecimal((idleTime*100/engineOperatingAllTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);
						graphReportModel.setIdlePercentageTotal(idlePercentage.doubleValue());
						logger.config("[GNAV][DEBUG][M] idlePercentage="+idlePercentage.doubleValue());
					}
				}


				// {HtO1
				if(constructionFlg==1) {
					graphReportModel.setConstructionTimeTotal(ConvertUtil.convertTimeHhMm(constructionTimeAllTotal.doubleValue()));
					logger.config("[GNAV][DEBUG][M] constructionTime="+constructionTimeAllTotal.doubleValue());
					logger.config("[GNAV][DEBUG][M] engineOperatingAllTotal="+engineOperatingAllTotal.doubleValue());

					// {HACh¦
					if(engineOperatingAllTotal.doubleValue()>0) {
						double conIdleTime = new BigDecimal((engineOperatingAllTotal.doubleValue()-constructionTimeAllTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();	// Ûßë·Î
						logger.config("[GNAV][DEBUG][M] conIdleTime="+(engineOperatingAllTotal.doubleValue()-constructionTimeAllTotal.doubleValue())+" :noformat");
						logger.config("[GNAV][DEBUG][M] conIdleTime="+conIdleTime);
						if(conIdleTime > 0) {
							BigDecimal idlePercentage = new BigDecimal((conIdleTime*100/engineOperatingAllTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);
							graphReportModel.setConstructionPercentageTotal(idlePercentage.doubleValue());
							logger.config("[GNAV][DEBUG][M] conIdlePercentage="+idlePercentage.doubleValue());
						}
					}
				}


			}
		}

		graphReportModel.setStatusCode(Constants.CON_OK);
		return Response.ok(graphReportModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}








	/**
	 * T|[gWv
	 * @param reportInfoList
	 * @param dateFrom
	 * @param dateTo
	 * @return List<ReportOperatingListModel>
	 *
	 * BigDecimal.ROUND_HALF_UP Ûßë·Îô
	 */
	public List<ReportOperatingListModel> getWeeklyTotal(List<TblTeijiReport> reportInfoList, Date dateFrom, Date dateTo, Integer constructionFlg, String conType){

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("#.#");

			int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(dateFrom));
			int month = Integer.parseInt(new SimpleDateFormat("M").format(dateFrom));
			int startDay = Integer.parseInt(new SimpleDateFormat("d").format(dateFrom));
			int endDay = Integer.parseInt(new SimpleDateFormat("d").format(dateTo));
			int reportCount = reportInfoList.size();
			logger.config("[GNAV][DEBUG] reportCount="+reportCount);
			logger.config("[GNAV][DEBUG] startDay="+startDay);
			logger.config("[GNAV][DEBUG] endDay  ="+endDay);


			BigDecimal engineOperatingWeekTotal = new BigDecimal(0);
			BigDecimal machineOperatingWeekTotal = new BigDecimal(0);
			BigDecimal nenryoConsumWeekTotal = new BigDecimal(0);
			BigDecimal waterConsumWeekTotal = new BigDecimal(0);
			BigDecimal constructionTimeWeekTotal = new BigDecimal(0);	//add

			List<ReportOperatingListModel> weekReportList = new ArrayList<ReportOperatingListModel>();
			ReportOperatingListModel weekReport = new ReportOperatingListModel();

			int count = 0;
			Calendar cal = new GregorianCalendar(year ,month-1 ,startDay);


			for(int dayCounter = startDay; dayCounter <= endDay; dayCounter++) {

				cal = new GregorianCalendar(year ,month-1 ,dayCounter);

				if(cal.get(Calendar.DAY_OF_WEEK)==2 || cal.get(Calendar.DATE)==1) {
					engineOperatingWeekTotal = new BigDecimal(0);
					machineOperatingWeekTotal = new BigDecimal(0);
					nenryoConsumWeekTotal = new BigDecimal(0);
					waterConsumWeekTotal = new BigDecimal(0);
					constructionTimeWeekTotal = new BigDecimal(0);			//add

					weekReport = new ReportOperatingListModel();
					weekReport.setDateFrom(sdf.format(cal.getTime()));
				}

				if(count<reportCount) {
					if(sdf.format(cal.getTime()).equals(sdf.format(reportInfoList.get(count).getKikaiKadobiLocal()))) {

						TblTeijiReport rp = reportInfoList.get(count);

						if(rp.getHourMeter() != null) {
							BigDecimal hourMeter = new BigDecimal(rp.getHourMeter());
							engineOperatingWeekTotal = engineOperatingWeekTotal.add(hourMeter);
						}

						if (rp.getKikaiSosaTime() != null) {
							BigDecimal kikaiSosa = new BigDecimal(rp.getKikaiSosaTime());
							machineOperatingWeekTotal = machineOperatingWeekTotal.add(kikaiSosa);
						}

						if(rp.getNenryoConsum() != null) {
							BigDecimal nenryoConsum = new BigDecimal(rp.getNenryoConsum());
							nenryoConsumWeekTotal = nenryoConsumWeekTotal.add(nenryoConsum);
						}



						if(rp.getUreaWaterConsum() != null) {
							BigDecimal defConsum = new BigDecimal(0);
							defConsum = new BigDecimal(df.format(Math.floor(rp.getUreaWaterConsum()/100)/10)).setScale(1, BigDecimal.ROUND_HALF_UP);
							waterConsumWeekTotal = waterConsumWeekTotal.add(defConsum);
						}


						if(rp.getSekoTime() != null) {
							BigDecimal sekoTime = new BigDecimal(rp.getSekoTime());
							constructionTimeWeekTotal = constructionTimeWeekTotal.add(sekoTime);
						}

						count++;
					}
				}

				if(cal.get(Calendar.DAY_OF_WEEK)==1 || cal.get(Calendar.DATE)==endDay) {

					logger.config("[GNAV][DEBUG] dateTo="+sdf.format(cal.getTime()));
					weekReport.setDateTo(sdf.format(cal.getTime()));

					weekReport.setEngineOperatingTime(ConvertUtil.convertTimeHhMm(engineOperatingWeekTotal.doubleValue()));
					weekReport.setMachineOperatingTime(ConvertUtil.convertTimeHhMm(machineOperatingWeekTotal.doubleValue()));
					weekReport.setFuelConsumption(nenryoConsumWeekTotal.doubleValue());
					weekReport.setDefConsumpiton(waterConsumWeekTotal.doubleValue());
					logger.config("[GNAV][DEBUG][W] engineOperating="+engineOperatingWeekTotal.doubleValue());
					logger.config("[GNAV][DEBUG][W] machineOperating="+machineOperatingWeekTotal.doubleValue());
					logger.config("[GNAV][DEBUG][W] nenryoConsum="+nenryoConsumWeekTotal.doubleValue());
					logger.config("[GNAV][DEBUG][W] waterConsum="+waterConsumWeekTotal.doubleValue());

					// Rï
					// **««« 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  «««**//
					BigDecimal fuelEffciency = new BigDecimal(0);
					if(nenryoConsumWeekTotal.doubleValue() > 0 && engineOperatingWeekTotal.doubleValue() > 0) {
//						double hourMeterWeek = new BigDecimal(df.format(Math.floor(engineOperatingWeekTotal.doubleValue()/60.0*10)/10)).doubleValue();
//						if(hourMeterWeek>0) {
//							BigDecimal fuelEffciency = new BigDecimal(nenryoConsumWeekTotal.doubleValue()/hourMeterWeek).setScale(1, BigDecimal.ROUND_HALF_UP);
//							weekReport.setFuelEffciency(fuelEffciency.doubleValue());
//							logger.config("[GNAV][DEBUG][W] fuelEffciency="+fuelEffciency.doubleValue());
//						}
						// **««« 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  «««**//
						// **««« 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  «««**//
//						BigDecimal fuelEffciency = new BigDecimal(0);
						if(conType.equals("ND") || conType.equals("D")) {
							fuelEffciency = new BigDecimal(nenryoConsumWeekTotal.doubleValue()/engineOperatingWeekTotal.doubleValue()*60).setScale(1, BigDecimal.ROUND_HALF_UP);
						}else {
							double hourMeterWeek = new BigDecimal(df.format(Math.floor(engineOperatingWeekTotal.doubleValue()/60.0*10)/10)).doubleValue();
							if(hourMeterWeek>0) {
								fuelEffciency = new BigDecimal(nenryoConsumWeekTotal.doubleValue()/hourMeterWeek).setScale(1, BigDecimal.ROUND_HALF_UP);
							}
						}
//						weekReport.setFuelEffciency(fuelEffciency.doubleValue());
//						logger.config("[GNAV][DEBUG][W] fuelEffciency="+fuelEffciency.doubleValue());
						// **ªªª 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  ªªª**//
					}
					weekReport.setFuelEffciency(fuelEffciency.doubleValue());
					logger.config("[GNAV][DEBUG][W] fuelEffciency="+fuelEffciency.doubleValue());
					// **ªªª 2018/11/13 LBNÎì  OtÉ0¾¦IÉÔpÎ  ªªª**//

					// ACh¦
					if(engineOperatingWeekTotal.doubleValue() > 0) {
						double idleTime = new BigDecimal(engineOperatingWeekTotal.doubleValue()-machineOperatingWeekTotal.doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();	// Ûßë·Î
						logger.config("[GNAV][DEBUG][W] idleTime="+(engineOperatingWeekTotal.doubleValue()-machineOperatingWeekTotal.doubleValue())+" :noformat");
						logger.config("[GNAV][DEBUG][W] idleTime="+idleTime);
						if(idleTime>0) {
							BigDecimal idlePercentage = new BigDecimal((idleTime*100/engineOperatingWeekTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);;
							weekReport.setIdlePercentage(idlePercentage.doubleValue());
							logger.config("[GNAV][DEBUG][W] idlePercentage="+idlePercentage.doubleValue());
						}
					}

					// {HtO
					if(constructionFlg==1) {
						weekReport.setConstructionTime(ConvertUtil.convertTimeHhMm(constructionTimeWeekTotal.doubleValue()));
						// {HACh¦	add
						if(engineOperatingWeekTotal.doubleValue() > 0) {
							double idleTime = new BigDecimal(engineOperatingWeekTotal.doubleValue()-constructionTimeWeekTotal.doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();	// Ûßë·Î
							logger.config("[GNAV][DEBUG][W] constructionIdleTime="+(engineOperatingWeekTotal.doubleValue()-constructionTimeWeekTotal.doubleValue())+" :noformat");
							logger.config("[GNAV][DEBUG][W] constructionIdleTime="+idleTime);

							if(idleTime>0) {
								BigDecimal conIdlePercentage = new BigDecimal((idleTime*100/engineOperatingWeekTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);;
								weekReport.setConstructionPercentage(conIdlePercentage.doubleValue());
								logger.config("[GNAV][DEBUG][W] constructionIdlePercentage="+conIdlePercentage.doubleValue());
							}
						}
					}

					weekReportList.add(weekReport);
				}
			}
			return weekReportList;
	}


	/**
	 * è|[gÏ·
	 * @param list
	 * @return
	 */
	public List<TblTeijiReport> parseReport(List<Object[]> list) {

		List<TblTeijiReport> rst = new ArrayList<TblTeijiReport>();

		for (Object[] item : list) {
			TblTeijiReport rp = new TblTeijiReport();
			rp.setKikaiKadobiLocal((java.sql.Timestamp) item[0]);
			rp.setHourMeter((Double) item[1]);
			rp.setKikaiSosaTime((Double) item[2]);
			rp.setNenryoConsusum((Double) item[3]);
			rp.setUreaWaterConsum((Double) item[4]);
			rp.setSekoTime((Double) item[5]);
			rst.add(rp);
		}
		return rst;
	}



}
