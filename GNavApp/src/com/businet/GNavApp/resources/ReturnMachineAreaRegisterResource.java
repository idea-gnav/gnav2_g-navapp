package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.returnMachine.IReturnMachineService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ReturnMachineAreaRegisterModel;

@Path("/returnMachineAreaRegister")
@Stateless
public class ReturnMachineAreaRegisterResource {

	private static final Logger logger = Logger.getLogger(ReturnMachineAreaRegisterResource.class.getName());

	@EJB
	IReturnMachineService returnMachineService;

	/**
	 * [API No.26] リターンマシンエリア登録
	 *
	 * @param userId                        - ユーザーID
	 * @param customerName                  - お客様名
	 * @param searchRangeDistance           - 検索距離
	 * @param searchRangeFromIdo            - 検索点緯度
	 * @param searchRangeFromKeido          - 検索点経度
	 * @param areaName                      - エリア名称
	 * @param areaNumber                    - エリアNo
	 * @param warningCurrentlyInProgressFlg - DTCチェックフラグ
	 * @param periodicServiceNoticeFlg      - 定期整備予告チェックフラグ
	 * @param periodicServiceNowDueFlg      - 定期整備警告チェックフラグ
	 * @param hourMeterFlg                  - アワメータチェックフラグ
	 * @param hourMeter                     - アワメータ時間以下指定
	 * @param areaDelFlg                    - エリア削除フラグ
	 * @return Response.JSON ReturnMachineListModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId, @FormParam("customerName") String customerName,
			@FormParam("searchRangeDistance") Integer searchRangeDistance,
			@FormParam("searchRangeFromIdo") Double searchRangeFromIdo,
			@FormParam("searchRangeFromKeido") Double searchRangeFromKeido, @FormParam("areaName") String areaName,
			@FormParam("areaNumber") Integer areaNumber,
			@FormParam("warningCurrentlyInProgressFlg") Integer warningCurrentlyInProgressFlg,
			@FormParam("periodicServiceNoticeFlg") Integer periodicServiceNoticeFlg,
			@FormParam("periodicServiceNowDueFlg") Integer periodicServiceNowDueFlg,
			@FormParam("hourMeterFlg") Integer hourMeterFlg, @FormParam("hourMeter") Double hourMeter,
			@FormParam("areaDelFlg") Integer areaDelFlg) {

		logger.info("[GNAV][POST] userId=" + userId + ", customerName=" + customerName + ", searchRangeDistance="
				+ searchRangeDistance + ", searchRangeFromIdo=" + searchRangeFromIdo + ", searchRangeFromKeido="
				+ searchRangeFromKeido + ", areaName=" + areaName + ", areaNumber=" + areaNumber
				+ ", warningCurrentlyInProgressFlg=" + warningCurrentlyInProgressFlg + ", periodicServiceNoticeFlg="
				+ periodicServiceNoticeFlg + ", periodicServiceNowDueFlg=" + periodicServiceNowDueFlg
				+ ", hourMeterFlg=" + hourMeterFlg + ", hourMeter=" + hourMeter + ", areaDelFlg=" + areaDelFlg);

		try {

			Double _searchRangeFromIdo = null;
			Double _searchRangeFromKeido = null;

			if (searchRangeFromIdo != null && searchRangeFromKeido != null) {
				_searchRangeFromIdo = searchRangeFromIdo * 3600 * 1000;
				_searchRangeFromKeido = searchRangeFromKeido * 3600 * 1000;
			}

			// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//

			String kyotenCd = returnMachineService.getKyotenCdByUserID(userId);


			// Insert Mode
			if (areaNumber == null) {

				Integer areaNo = returnMachineService.getMaxAreaNoByKyotenCd(kyotenCd);
				areaNumber = areaNo == null ? 1 : areaNo + 1;

				returnMachineService.insertTblReturnMachineArea(userId,kyotenCd, areaNumber, customerName, searchRangeDistance,
						_searchRangeFromIdo, _searchRangeFromKeido, areaName, warningCurrentlyInProgressFlg,
						periodicServiceNoticeFlg, periodicServiceNowDueFlg, hourMeterFlg, hourMeter);

			}
			// Delete Mode
			else if (areaDelFlg != null && areaDelFlg == 1)
				returnMachineService.updateDelFlgTblReturnMachineArea(userId,kyotenCd, areaNumber);
			// Update Mode
			else
				returnMachineService.updateTblReturnMachineArea(userId,kyotenCd, areaNumber, customerName, searchRangeDistance,
						_searchRangeFromIdo, _searchRangeFromKeido, areaName, warningCurrentlyInProgressFlg,
						periodicServiceNoticeFlg, periodicServiceNowDueFlg, hourMeterFlg, hourMeter);
			// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
			ReturnMachineAreaRegisterModel register = new ReturnMachineAreaRegisterModel();
			register.setAreaNumber(areaNumber);
			register.setStatusCode(Constants.CON_OK);


			return Response.ok(register).build();

		} catch (Exception e) {
			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}
}
