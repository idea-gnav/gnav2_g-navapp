package com.businet.GNavApp.resources;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.construction.IConstructionService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.entities.TblSekoInfo;
import com.businet.GNavApp.models.ConstructionCountryModel;
import com.businet.GNavApp.models.ConstructionDetailModel;
import com.businet.GNavApp.models.ConstructionMaterialTypeModel;
import com.businet.GNavApp.models.ConstructionPdfLinkModel;
import com.businet.GNavApp.models.ConstructionPdfModel;
import com.businet.GNavApp.models.ConstructionReportPhotoModel;
import com.businet.GNavApp.models.ConstructionWorkTypeModel;
import com.businet.GNavApp.models.ReturnContainer;


@Path("/constructionDetail")
@Stateless
public class ConstructionDetailResource {

	private static final Logger logger = Logger.getLogger(ConstructionDetailResource.class.getName());

	@EJB
	IConstructionService constructionService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	/**
	 * [API No.48] 施工情報詳細取得
	 *
	 * @param userId
	 * @param serialNumber
	 * @param constructionResultId
	 * @param deviceLanguage
	 * @return Response.JSON ConstructionDetailModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId,@FormParam("serialNumber") Integer serialNumber,
			@FormParam("constructionResultId") Integer constructionResultId,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", serialNumber=" + serialNumber
				+ ", constructionResultId=" + constructionResultId + ", deviceLanguage=" + deviceLanguage);

		try {
			ConstructionDetailModel constructionDetail = new ConstructionDetailModel();

			// 機種、機械管理番号、アワメータ取得、組織コード
			List<Object[]> machine = constructionService.findMachineInfo((long)serialNumber,constructionResultId);
			Object[] result = machine.get(0);
			// 機種
			constructionDetail.setScmModel(((String)result[0]));
			// 機番
			constructionDetail.setSerialNumber(((String)result[1]));
			// アワメータ
			Double hourmeter = 0.0;
			if(((Number)result[2]) != null) {
				hourmeter = ((Number)result[2]).doubleValue();
				constructionDetail.setHourMeter(Math.floor(hourmeter/60.0*10)/10);
			}else {
				constructionDetail.setHourMeter(0.0);
			}
			// お客様名
			String sosikiCd = ((String)result[3]);
			if(sosikiCd != null) {
				constructionDetail.setCustomerManagementName(sosikiService.findByKigyouSosikiName(sosikiCd));
			}

			// 編集可否チェックフラグ
			Integer editableFlg = 0;

			// 新規or照会判別
			Boolean isViewResultMode = (constructionResultId != null);

			// 施工情報取得
			if (isViewResultMode) {
				// 編集可否チェックフラグ
				editableFlg = (constructionService.constructionEditableCheck(constructionResultId, userId));

				// 施工情報テーブルより取得
				TblSekoInfo sekoInfo = constructionService.findConstructionResultById(constructionResultId);
				if (sekoInfo != null) {
					//↓↓↓↓↓↓↓↓↓項目セット処理追加↓↓↓↓↓↓↓↓↓
					// 調査者
					constructionDetail.setResearchUser(sekoInfo.getResearchUser());
					// 調査日
					constructionDetail.setResearchDate(ConvertUtil.formatYMD(sekoInfo.getResearchDate()));
					// 調査場所
					constructionDetail.setResearchLocation(sekoInfo.getResearchLocation());
					// 工事量
					constructionDetail.setWorkVolume(sekoInfo.getWorkVolume());
					// 工事量単位
					constructionDetail.setWorkVolumeUnit(sekoInfo.getWorkVolumeUnit());
					// 国
					constructionDetail.setReportCountry(sekoInfo.getReportCountry());
					// 合材種類
					constructionDetail.setMaterialType(sekoInfo.getMaterialType());
					// 合材温度使用有無
					constructionDetail.setMaterialTemperatureUse(sekoInfo.getMaterialTemperatureUse());
					// 合材温度
					constructionDetail.setMaterialTemperature(sekoInfo.getMaterialTemperature());
					// 合材温度単位
					constructionDetail.setMaterialTemperatureUnit(sekoInfo.getMaterialTemperatureUnit());
					// 外気温度
					constructionDetail.setOutsideAirTemperature(sekoInfo.getOutsideAirTemperature());
					// 外気温度単位
					constructionDetail.setOutsideAirTemperatureUnit(sekoInfo.getOutsideAirTemperatureUnit());
					// プレート加熱温度
					constructionDetail.setPlateTemperature(sekoInfo.getPlateTemperature());
					// プレート加熱温度単位
					constructionDetail.setPlateTemperatureUnit(sekoInfo.getPlateTemperatureUnit());
					// 工事の種類
					constructionDetail.setWorkType(sekoInfo.getWorkType());
					// 現場状況
					constructionDetail.setSpotSituation(sekoInfo.getSpotSituation());
					// 舗装距離入力有無
					constructionDetail.setPavementDistanceUse(sekoInfo.getPavementDistanceUse());
					// 舗装距離
					constructionDetail.setPavementDistance(sekoInfo.getPavementDistance());
					// 舗装幅From
					constructionDetail.setPavementWidth(sekoInfo.getPavementWidth());
					// 舗装幅To
					constructionDetail.setPavementWidthTo(sekoInfo.getPavementWidthTo());//2020.01.14 DucNKT added
					// 敷き均し-左
					constructionDetail.setPavementThicknessLevelingL(sekoInfo.getPavementThicknessLevelingL());
					// 敷き均し-中央
					constructionDetail.setPavementThicknessLevelingC(sekoInfo.getPavementThicknessLevelingC());
					// 敷き均し-右
					constructionDetail.setPavementThicknessLevelingR(sekoInfo.getPavementThicknessLevelingR());
					// 施工速度
					constructionDetail.setConstructionSpeed(sekoInfo.getConstructionSpeed());
					// レベリングシリンダ目盛-左
					constructionDetail.setLevelingCylinderScaleL(sekoInfo.getLevelingCylinderScaleL());
					// レベリングシリンダ目盛-右
					constructionDetail.setLevelingCylinderScaleR(sekoInfo.getLevelingCylinderScaleR());
					// 手動シックネス目盛-左
					constructionDetail.setManualSixnessScaleL(sekoInfo.getManualSixnessScaleL());
					// 手動シックネス目盛-右
					constructionDetail.setManualSixnessScaleR(sekoInfo.getManualSixnessScaleR());
					// 手動シックネス目盛単位
					constructionDetail.setManualSixnessScaleUnit(sekoInfo.getManualSixnessScaleUnit());
					// 段差調整目盛-左
					constructionDetail.setStepAdjustmentScaleL(sekoInfo.getStepAdjustmentScaleL());
					// 段差調整目盛-右
					constructionDetail.setStepAdjustmentScaleR(sekoInfo.getStepAdjustmentScaleR());
					// クラウン
					constructionDetail.setCrown(sekoInfo.getCrown());
					// スロープクラウン-左
					constructionDetail.setSlopeCrownL(sekoInfo.getSlopeCrownL());
					// スロープクラウン-右
					constructionDetail.setSlopeCrownR(sekoInfo.getSlopeCrownR());
					// 伸縮モールドボード高さ-左
					constructionDetail.setStretchMoldBoardHeightL(sekoInfo.getStretchMoldBoardHeightL());
					// 伸縮モールドボード高さ-右
					constructionDetail.setStretchMoldBoardHeightR(sekoInfo.getStretchMoldBoardHeightR());
					// タンパ回転数入力有無
					constructionDetail.setTampaRotationsUse(sekoInfo.getTampaRotationsUse());
					// タンパ回転数
					constructionDetail.setTampaRotations(sekoInfo.getTampaRotations());
					// タンパオート機能使用有無
					constructionDetail.setTampaAutoFunction(sekoInfo.getTampaAutoFunction());
					// バイブレータ回転数
					constructionDetail.setVibratorRotations(sekoInfo.getVibratorRotations());
					// スクリュー高さ
					constructionDetail.setScrewHeight(sekoInfo.getScrewHeight());
					// スクリュー速度目盛-左
					constructionDetail.setScrewSpeedScaleL(sekoInfo.getScrewSpeedScaleL());
					// スクリュー速度目盛-右
					constructionDetail.setScrewSpeedScaleR(sekoInfo.getScrewSpeedScaleR());
					// コンベヤ速度目盛-左
					constructionDetail.setConveyorSpeedScaleL(sekoInfo.getConveyorSpeedScaleL());
					// コンベヤ速度目盛-右
					constructionDetail.setConveyorSpeedScaleR(sekoInfo.getConveyorSpeedScaleR());
					// スクリューフロコン使用の有無
					constructionDetail.setScrewFlowControlUse(sekoInfo.getScrewFlowControlUse());
					// AGCの使用状況
					constructionDetail.setAgcUse(sekoInfo.getAgcUse());
					// AGC使用種類
					constructionDetail.setAgcType(sekoInfo.getAgcType());
					// エクステンションスクリュー有無
					constructionDetail.setExtensionScrewUse(sekoInfo.getExtensionScrewUse());
					// エクステンションスクリュー
					constructionDetail.setExtensionScrew(sekoInfo.getExtensionScrew());
					// 舗装不具合--粗骨材分離(縞)
					constructionDetail.setPavementTrouble1(sekoInfo.getPavementTrouble1());
					// 舗装不具合-引きずり
					constructionDetail.setPavementTrouble2(sekoInfo.getPavementTrouble2());
					// 舗装不具合-クラック
					constructionDetail.setPavementTrouble3(sekoInfo.getPavementTrouble3());
					// 舗装不具合-あばた
					constructionDetail.setPavementTrouble4(sekoInfo.getPavementTrouble4());
					// 舗装不具合-むしれ
					constructionDetail.setPavementTrouble5(sekoInfo.getPavementTrouble5());
					// 舗装不具合-色違い
					constructionDetail.setPavementTrouble6(sekoInfo.getPavementTrouble6());
					// 舗装不具合-ドラッギングホール
					constructionDetail.setPavementTrouble7(sekoInfo.getPavementTrouble7());
					// 舗装不具合-密度不足
					constructionDetail.setPavementTrouble8(sekoInfo.getPavementTrouble8());
					// 舗装不具合-不陸
					constructionDetail.setPavementTrouble9(sekoInfo.getPavementTrouble9());
					// 舗装不具合-フラッシング
					constructionDetail.setPavementTrouble10(sekoInfo.getPavementTrouble10());
					// 舗装不具合-筋
					constructionDetail.setPavementTrouble11(sekoInfo.getPavementTrouble11());
					// 舗装不具合-ほうき目
					constructionDetail.setPavementTrouble12(sekoInfo.getPavementTrouble12());
					// 舗装不具合-スクリードマーク
					constructionDetail.setPavementTrouble13(sekoInfo.getPavementTrouble13());
					// 舗装不具合-タンパマーク
					constructionDetail.setPavementTrouble14(sekoInfo.getPavementTrouble14());
					// 合材処理トン数
					constructionDetail.setTonnageMixedMaterials(sekoInfo.getTonnageMixedMaterials());
					// お客様評価
					constructionDetail.setCustomerEvaluation(sekoInfo.getCustomerEvaluation());
					// リヤスロープ-左
					constructionDetail.setRearSlopeL(sekoInfo.getRearSlopeL());
					// リヤスロープ-右
					constructionDetail.setRearSlopeR(sekoInfo.getRearSlopeR());
					// リヤアタック 縮め-左
					constructionDetail.setRearAttackShrinkL(sekoInfo.getRearAttackShrinkL());
					// リヤアタック 縮め-右
					constructionDetail.setRearAttackShrinkR(sekoInfo.getRearAttackShrinkR());
					// リヤアタック 中間-左
					constructionDetail.setRearAttackMiddleL(sekoInfo.getRearAttackMiddleL());
					// リヤアタック 中間-右
					constructionDetail.setRearAttackMiddleR(sekoInfo.getRearAttackMiddleR());
					// リヤアタック 伸ばし-左
					constructionDetail.setRearAttackStretchL(sekoInfo.getRearAttackStretchL());
					// リヤアタック 伸ばし-右
					constructionDetail.setRearAttackStretchR(sekoInfo.getRearAttackStretchR());

					//画像リスト
					ArrayList<ConstructionReportPhotoModel> constructionReportPhotoModel = new ArrayList<ConstructionReportPhotoModel>(); // 画像リスト
					List<Object[]> photoList = constructionService.findPhotoList((long)serialNumber,constructionResultId);
					if(photoList!=null) {
						Integer maxRecord = photoList.size();
						for(int counter = 0; counter < maxRecord ; counter++ ) {
							ConstructionReportPhotoModel constructionReportPhoto = new ConstructionReportPhotoModel();
							Object[] rs =  photoList.get(counter);
							constructionReportPhoto.setPhotoNo(((Number)rs[0]).intValue());														// 画像No
							constructionReportPhoto.setSekoPhotoPath(Constants.UPLOADED_IMAGE_URL + "ConstructionImage/" + (String)rs[1]);	// 画像URL
							constructionReportPhoto.setPhotoDate((String)ConvertUtil.formatYMD((Timestamp) rs[2]));								// 画像操作日
//							constructionReportPhoto.setPhotoComment((String)rs[3]);																// 画像コメント

							constructionReportPhotoModel.add(constructionReportPhoto);
						}
						constructionDetail.setReportPhotoList(constructionReportPhotoModel);
					}
				}
			}else {
				// 調査者
				constructionDetail.setResearchUser(constructionService.getUserName(userId,(long)serialNumber,constructionResultId));
				// 調査日
				Date date = new Date();
				Timestamp time = new Timestamp(date.getTime());
				constructionDetail.setResearchDate(ConvertUtil.formatYMD(time));

			}

			// 編集可否フラグセット
			constructionDetail.setEditableFlg(editableFlg);

			// 国リスト取得
			ArrayList<ConstructionCountryModel> constructionCountryArraylistModel = new ArrayList<ConstructionCountryModel>();                // 国リスト
			ArrayList<ConstructionMaterialTypeModel> constructionMaterialTypeArraylistModel = new ArrayList<ConstructionMaterialTypeModel>(); // 合材リスト
			ArrayList<ConstructionWorkTypeModel> constructionWorkTypeArraylistModel = new ArrayList<ConstructionWorkTypeModel>();             // 工事種類
			List<Object[]> countryList = constructionService.findCountryList(deviceLanguage);
			if(countryList!=null) {
				Integer maxRecord1 = countryList.size();
				for(int counter1 = 0; counter1 < maxRecord1 ; counter1++ ) {
					ConstructionCountryModel constructionCountry = new ConstructionCountryModel();
					Object[] rs1 =  countryList.get(counter1);
					constructionCountry.setCountryId(((Number)rs1[0]).intValue());	// 国ID
					constructionCountry.setCountry((String)rs1[1]);					// 国名称
					constructionCountry.setUnit((String)rs1[2]);					// 単位

					constructionCountryArraylistModel.add(constructionCountry);

					// 合材リスト取得
					List<Object[]> materialTypeList = constructionService.findMaterialTypeList(deviceLanguage,((Number)rs1[0]).intValue());
					if(materialTypeList!=null) {
						Integer maxRecord2 = materialTypeList.size();
						for(int counter2 = 0; counter2 < maxRecord2 ; counter2++ ) {
							ConstructionMaterialTypeModel constructionMaterialType = new ConstructionMaterialTypeModel();
							Object[] rs2 =  materialTypeList.get(counter2);
							constructionMaterialType.setMaterialTypeCountryId(((Number)rs2[0]).intValue());	// 合材取得用国ID
							constructionMaterialType.setMaterialTypeId(((Number)rs2[1]).intValue());		// 合材ID
							constructionMaterialType.setMaterialType((String)rs2[2]);						// 合材名称

							constructionMaterialTypeArraylistModel.add(constructionMaterialType);
						}
						constructionDetail.setMaterialTypeList(constructionMaterialTypeArraylistModel);
					}

					// 工事種類取得
					List<Object[]> workTypeList = constructionService.findWorkTypeList(deviceLanguage,((Number)rs1[0]).intValue());
					if(workTypeList!=null) {
						Integer maxRecord3 = workTypeList.size();
						for(int counter3 = 0; counter3 < maxRecord3 ; counter3++ ) {
							ConstructionWorkTypeModel constructionWorkType = new ConstructionWorkTypeModel();
							Object[] rs3 =  workTypeList.get(counter3);
							constructionWorkType.setWorkTypeCountryId(((Number)rs3[0]).intValue());	// 工事種類取得用国ID
							constructionWorkType.setWorkTypeId(((Number)rs3[1]).intValue());		// 工事ID
							constructionWorkType.setWorkType((String)rs3[2]);						// 工事名称

							constructionWorkTypeArraylistModel.add(constructionWorkType);
						}
						constructionDetail.setWorkTypeList(constructionWorkTypeArraylistModel);
					}
				}
				constructionDetail.setCountryList(constructionCountryArraylistModel);
			}

			// 舗装不具合PDFリスト取得
			ArrayList<ConstructionPdfModel> constructionPdfModel = new ArrayList<ConstructionPdfModel>(); // 舗装不具合PDFリスト
			ArrayList<ConstructionPdfLinkModel> constructionPdfLinkModel = new ArrayList<ConstructionPdfLinkModel>(); // 舗装不具合PDFリスト
			List<Object[]> pdfList = constructionService.findPavementPdfList(constructionDetail.getScmModel(),ConvertUtil.convertLanguageCd(deviceLanguage));
			if(pdfList!=null) {
				int backNo = 0;
				Integer maxRecord4 = pdfList.size();
				for(int counter4 = 0; counter4 < maxRecord4 ; counter4++ ) {
					ConstructionPdfModel constructionPdf = new ConstructionPdfModel();
					Object[] rs4 =  pdfList.get(counter4);
					if(((Number)rs4[0]).intValue() != backNo) {
						constructionPdf.setChoiceId(((Number)rs4[0]).intValue());			// 舗装不具合選択肢ID						
						constructionPdfModel.add(constructionPdf);
						constructionPdfLinkModel = new ArrayList<ConstructionPdfLinkModel>();
					}
					ConstructionPdfLinkModel constructionPdfLink = new ConstructionPdfLinkModel();
					constructionPdfLink.setTroubleName((String)rs4[1]);						// 舗装不具合名称
					constructionPdfLink.setPdfLink(Constants.PDF_URL + (String)rs4[2]);	// PDFリンク
					constructionPdfLinkModel.add(constructionPdfLink);
					constructionPdf.setLinkList(constructionPdfLinkModel);

					backNo = ((Number)rs4[0]).intValue();
				}
				constructionDetail.setPdfList(constructionPdfModel);
			}

			constructionDetail.setStatusCode(Constants.CON_OK);
			return Response.ok(constructionDetail).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}
}
