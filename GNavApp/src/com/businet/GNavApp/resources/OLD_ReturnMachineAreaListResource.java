package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.returnMachine.OLD_IReturnMachineService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.OLD_AreaListModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.OLD_ReturnMachineAreaListModel;

@Path("/old_returnMachineAreaList")
@Stateless
public class OLD_ReturnMachineAreaListResource {

	private static final Logger logger = Logger.getLogger(OLD_ReturnMachineAreaListResource.class.getName());

	@EJB
	OLD_IReturnMachineService returnMachineService;

	@EJB
	IUserService userService;

	/**
	 * [API No.23] リターンマシンエリア一覧
	 *
	 * @param userId       - ユーザーＩＤ
	 * @param sortFlg      - 並び替え条件
	 * @param startRecord  - レコード開始位置
	 * @param customerName - お客様名入力
	 * @return Response.JSON ReturnMachineAreaListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId, @FormParam("sortFlg") Integer sortFlg,
			@FormParam("startRecord") Integer startRecord, @FormParam("customerName") String customerName) {

		logger.info("[GNAV][POST] userId=" + userId + ", sortFlg=" + sortFlg + ", startRecord=" + startRecord
				+ ", customerName=" + customerName);

		OLD_ReturnMachineAreaListModel returnMachineAreaList = new OLD_ReturnMachineAreaListModel();

		try {

			List<Object[]> returnMachineAreas = returnMachineService.findByReturnMachineAreaNativeQuery(userId, sortFlg,
					customerName);

			ArrayList<OLD_AreaListModel> areaList = new ArrayList<OLD_AreaListModel>();

			int returnAreaCount = 0;
			if (returnMachineAreas != null) {
				returnAreaCount = returnMachineAreas.size();

				if (returnAreaCount >= Constants.CON_MAX_COUNT)
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				Integer record = startRecord - 1;

				for (int counter = record; counter < (record + 50); counter++) {
					OLD_AreaListModel area = new OLD_AreaListModel();

					if(returnAreaCount == counter)
						break;

					Object[] rs = returnMachineAreas.get(counter);

                    if (rs[0] != null)
                        area.setAreaNumber(((Number) rs[0]).intValue());

					if (rs[1] != null)
                        area.setCustomerName(rs[1].toString());

					if (rs[2] != null)
                        area.setAreaName(rs[2].toString());

					areaList.add(area);

				}

			}

			returnMachineAreaList.setAreaList(areaList);
			returnMachineAreaList.setReturnAreaCount(returnAreaCount);
			returnMachineAreaList.setStatusCode(Constants.CON_OK);

			return Response.ok(returnMachineAreaList).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
