package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.returnMachine.OLD_IReturnMachineService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.models.ReturnContainer;

@Path("/old_returnMachineRegister")
@Stateless
public class OLD_ReturnMachineRegisterResource {

	private static final Logger logger = Logger.getLogger(OLD_ReturnMachineRegisterResource.class.getName());

	@EJB
	OLD_IReturnMachineService returnMachineService;

	@EJB
	IMachineService machineService;

	/**
	 * [API No.29] リターンマシン登録・削除
	 *
	 * @param userId
	 * @param addSerialNumber
	 * @param removeSerialNumber
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId,
			@FormParam("addSerialNumber") String _addSerialNumber,
			@FormParam("removeSerialNumber") String _removeSerialNumber) {

		logger.info("[GNAV][POST] userId=" + userId + ", addSerialNumber=" + _addSerialNumber + ", removeSerialNumber="
				+ _removeSerialNumber);

		// Integer.parseInt(addSerialNumber)
		// Long.parseLong(addSerialNumber)
		try {

			// 機械をリターンマシンに登録を行う。
			String[] addSerialNumbers = null;
			if (_addSerialNumber != null && !_addSerialNumber.equals("")) {
				addSerialNumbers = _addSerialNumber.split(",");
				for (String addSerialNumber : addSerialNumbers) {
					MstMachine machine = machineService.findByMachine(Long.parseLong(addSerialNumber));

                    returnMachineService.addReturnMachine(Long.parseLong(addSerialNumber), userId);
					returnMachineService.addReturnMachineWork(Long.parseLong(addSerialNumber), userId,machine.getNewIdo(),machine.getNewKeido());
				}
			}

			// 機械のリターンマシン削除を行う。
			String[] removeSerialNumbers = null;
			if (_removeSerialNumber != null && !_removeSerialNumber.equals("")) {
				removeSerialNumbers = _removeSerialNumber.split(",");
				for (String removeSerialNumber : removeSerialNumbers) {
                    returnMachineService.removeReturnMachine(Long.parseLong(removeSerialNumber), userId);
				}
			}
			// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
			return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
