package com.businet.GNavApp.resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.models.GoogleDirectionServiceModel;
import com.businet.GNavApp.models.ReturnContainer;


@Path("/googleDirectionService")
@Stateless
public class GoogleDirectionService {

	private static final Logger logger = Logger.getLogger(GoogleDirectionService.class.getName());
	private static final String CON_MODE_CAR = "driving";
	private static final String CON_MODE_WALK = "walking";
	private static String CON_googleApiUrl = "https://maps.googleapis.com/maps/api/directions/json";
	private static String http_proxy = "http://shifilter13.shi.co.jp:8080/";


	/**
	 * [API No.18] GoogleMapルート検索サービス
	 * @param origin
	 * @param destination
	 * @param transitMode
	 * @param units
	 * @param language
	 * @param key
	 * @param compress
	 * @return Response.JSON GoogleDirectionServiceModel
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("origin") String origin, @FormParam("destination") String destination,
			@FormParam("transit_mode") String transitMode, @FormParam("units") String units,
			@FormParam("language") String language, @FormParam("key") String key,
			@FormParam("compress") String compress) throws IOException, InterruptedException {

		logger.info("[GNAV][POST] origin=" + origin + ", destination=" + destination + ", units=" + units
				+ ", transit_mode=" + transitMode + ", language=" + language + ", key=" + key + ", compress="
				+ compress);

		boolean isCompress = false;

		StringBuilder query = new StringBuilder();
		query.append("?origin=").append(origin);
		query.append("&destination=").append(destination);

		if (units != null)
			query.append("&units=").append(units);
		if (language != null)
			query.append("&language=").append(language);
		if (key != null)
			query.append("&key=").append(URLEncoder.encode(key, "UTF-8"));
		if (transitMode != null)
			query.append("&transit_mode=").append(transitMode);

		if (compress != null && compress.equals("yes"))
			isCompress = true;

		String carRouter = GetRouterData(CON_MODE_CAR, query.toString(), isCompress);
//		Thread.sleep(5000);
		String walkRouter = GetRouterData(CON_MODE_WALK, query.toString(), isCompress);

		if (carRouter == null || walkRouter == null) {
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

		GoogleDirectionServiceModel returnData = new GoogleDirectionServiceModel();
		returnData.setStatusCode(Constants.CON_OK);
		returnData.setCarRouterData(carRouter);
		returnData.setWalkRouterData(walkRouter);

//		String str = new String(DatatypeConverter.parseBase64Binary("user:123"));
		logger.config("[GNAV][DEBUG] carRouter=" + carRouter.toString());
//		logger.config("[GNAV][DEBUG] carRouter="+new String(DatatypeConverter.printBase64Binary(carRouter)));
		logger.config("[GNAV][DEBUG] walkRouter=" + walkRouter.toString());
//		logger.config("[GNAV][DEBUG] carRouter="+new String(DatatypeConverter.printBase64Binary(walkRouter)));

		return Response.ok(returnData).build();

	}

	private static Map<String, String> proxyMap = null;

	private Map<String, String> parseProxy(String urlStr) {
		Map<String, String> result = null;
		if (urlStr == null || urlStr.isEmpty()) {
			return result;
		}

		try {
			URL url = new URL(urlStr);
			String host = url.getHost();
			int port = url.getPort();
			String userInfo = url.getUserInfo();
			String user = null;
			String pass = null;
			if (userInfo != null && !userInfo.isEmpty()) {
				String[] userInfos = userInfo.split(":");

				if (userInfos != null && userInfos.length > 0) {
					user = userInfos[0].trim();
					if (userInfos.length > 1) {
						pass = userInfos[1].trim();
					}
				}
			}
			result = new HashMap<>();
			result.put("host", host);
			result.put("port", String.valueOf(port));
			result.put("user", user);
			result.put("pass", pass);
		} catch (MalformedURLException e) {
			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
		}
		return result;
	}

	private String GetRouterData(String mode, String query, boolean compressed) throws IOException {

		proxyMap = parseProxy(http_proxy);

		URL url = new URL(CON_googleApiUrl + query + "&mode=" + mode);
		HttpURLConnection con = null;
		if (proxyMap != null) {
			// Java 8 Update 111からjava.netパッケージのHTTPS接続の際のトンネリングにBasic認証を使用できない設定の回避設定
			System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");

			String host = proxyMap.get("host");
			int port = Integer.parseInt(proxyMap.get("port"));

			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));

			// プロキシ認証が必要な場合か否か
			if ((proxyMap.get("user") != null) && (proxyMap.get("pass") != null)) {
				Authenticator authenticator = new Authenticator() {
					public PasswordAuthentication getPasswordAuthentication() {
						return (new PasswordAuthentication(proxyMap.get("user"), (proxyMap.get("pass")).toCharArray()));
					}
				};
				Authenticator.setDefault(authenticator);
			}

			con = (HttpURLConnection) url.openConnection(proxy);
		} else {
			con = (HttpURLConnection) url.openConnection();
		}

		con.setRequestMethod("GET");

		int responseCode;
		responseCode = con.getResponseCode();

		logger.info("google direction responseCode " + responseCode);

		if (responseCode != 200) {
			return null;
		}

		InputStream responseStream = con.getInputStream();

		if (compressed) {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			GZIPOutputStream gzipStream = new GZIPOutputStream(result);
			byte[] buffer = new byte[2048];
			int length;
			while ((length = responseStream.read(buffer)) != -1) {
				gzipStream.write(buffer, 0, length);
			}

			gzipStream.flush();
			gzipStream.close();
			responseStream.close();

			return DatatypeConverter.printBase64Binary(result.toByteArray());
		} else {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[2048];
			int length;
			while ((length = responseStream.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			responseStream.close();

			return result.toString("UTF-8");
		}

	}

}
