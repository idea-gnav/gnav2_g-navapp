package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machineSerchInfo.IMachineSearchInfoService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstUser;
import com.businet.GNavApp.models.MachineSearchInfoModel;
import com.businet.GNavApp.models.ReturnContainer;




@Path("/machineSearchInfo")
@Stateless
public class MachineSearchInfo {

	private static final Logger logger = Logger.getLogger(MachineSearchInfo.class.getName());

	@EJB
	IMachineSearchInfoService machineSearchInfoService;

	@EJB
	IUserService userService;

	/**
	 * [API No.19] 機械詳細検索情報
	 * @param userId
	 * @return Response.JSON MachineSearchInfoModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId,
					// 2019/04/10 DucNKT 定期整備画面に追加されました：開始
					@FormParam("deviceLanguage") Integer deviceLanguage,
					// 2019/04/10 DucNKT 定期整備画面に追加されました：終了
					//**↓↓↓ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↓↓↓**//
					@FormParam("paverOnlyFlg") Integer paverOnlyFlg,
					//**↑↑↑ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↑↑↑**//
					// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
					@FormParam("customerNamePriority") Integer customerNamePriority
					// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//
				) {

		logger.info("[GNav][POST] userId="+userId+", deviceLanguage="+deviceLanguage+", paverOnlyFlg="+paverOnlyFlg+", customerNamePriority="+customerNamePriority );

		MachineSearchInfoModel machineSearchInfoModel = new MachineSearchInfoModel();

		try {

				// ユーザー情報取得
				MstUser user = userService.findByUser(userId);

				//**↓↓↓ 2019/11/07 iDEA山下 組織権限取得　API内共通化 ↓↓↓**//
				// ユーザ組織権限取得
				List<Object[]> userKengen = userService.findByUserSosikiKengenNativeQuery(userId);
				String sosikiCd = null;
				if(userKengen.get(0)[1]!=null)
					sosikiCd = (String)userKengen.get(0)[1];

				String kigyouCd = null;
				if(userKengen.get(0)[2]!=null)
					kigyouCd = (String)userKengen.get(0)[2];

				String groupNo = null;
				if(userKengen.get(0)[3]!=null)
					groupNo = (String)userKengen.get(0)[3];

				Integer processTypeFlg = null;
				if(userKengen.get(0)[5]!=null)
					processTypeFlg = ((Number)userKengen.get(0)[5]).intValue();
				else
					return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
				//**↑↑↑ 2019/11/07 iDEA山下 組織権限取得追加　API内共通化 ↑↑↑**//

				machineSearchInfoModel.setDealerNameFlg(1);
				machineSearchInfoModel.setCustomerNameFlg(1);

				// 拠店検索項目制御

				//**↓↓↓ 2019/11/07 iDEA山下 組織権限取得　API内共通化 ↓↓↓**//
				// **↓↓↓　2018/12/17 LBN石川 拠店検索権限制御対応 ↓↓↓**//
//				if(user.getLanguageCd().equals("ja") || user.getLanguageCd().equals("jp"))
//					machineSearchInfoModel.setCustomerInfoFlg(1);
//				else
//					machineSearchInfoModel.setCustomerInfoFlg(0);
				Integer customerInfoFlg = 0;
//				if(user.getLanguageCd().equals("ja") || user.getLanguageCd().equals("jp")) {
//					List<Object[]> userSosikiKengen = userService.findByUserSosikiKengenNativeQuery(userId);
//					if(userSosikiKengen.get(0)[5]!=null) {
//						Integer processTypeFlg = Integer.parseInt(userSosikiKengen.get(0)[5].toString());
//						if(processTypeFlg!=null)
//							// 処理タイプ 0:SCM  1:地区
//							if(processTypeFlg==0 || processTypeFlg==1)
//								customerInfoFlg = 1;
//					}
//				}
				if(processTypeFlg!=null) {
					if(processTypeFlg==0 || processTypeFlg==1)
						customerInfoFlg = 1;
				}
				machineSearchInfoModel.setCustomerInfoFlg(customerInfoFlg);
				// **↑↑↑　2018/12/17 LBN石川 拠店検索権限制御対応 ↑↑↑**//
				//**↑↑↑ 2019/11/07 iDEA山下 組織権限取得　API内共通化 ↑↑↑**//

				// SCMモデル一覧取得
				//**↓↓↓ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↓↓↓**//
//				List<String> scmModelList = machineSearchInfoService.findByScmModelList();
				if(paverOnlyFlg == null)
					paverOnlyFlg = 0;
				//**↓↓↓ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限  ↓↓↓**//
//				List<String> scmModelList = machineSearchInfoService.findByScmModelList(paverOnlyFlg);
				List<String> scmModelList = machineSearchInfoService.findByScmModelList(paverOnlyFlg,processTypeFlg,kigyouCd,sosikiCd,groupNo);
				//**↑↑↑ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限 ↑↑↑**//
				//**↑↑↑ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↑↑↑**//
				if(scmModelList!=null) {
					logger.config("[GNAV][DEBUG] scmModelList="+scmModelList.size());
					machineSearchInfoModel.setScmModelList(scmModelList);
				}

				HashMap<String, String> hashMap = new HashMap<>();
				ArrayList<HashMap<String, String>> hashMapList = new ArrayList<>();


				// 代理店一覧
				List<Object[]>  dealerList = machineSearchInfoService.findByDairitenList();
				logger.config("[GNAV][DEBUG] dealerList="+dealerList.size());
				for(int index =0; index < dealerList.size(); index++) {
					hashMap.put("dealerCd", dealerList.get(index)[0].toString());
					hashMap.put("dealerName", dealerList.get(index)[1].toString());
					hashMapList.add(hashMap);
					hashMap = new HashMap<String, String>();
				}
				machineSearchInfoModel.setDealerList(hashMapList);
				hashMapList = new ArrayList<>();

				// **↓↓↓　2018/12/17 LBN石川 拠店検索権限制御対応 ↓↓↓**//
//				if(user.getLanguageCd().equals("ja") || user.getLanguageCd().equals("jp")) {
				if(customerInfoFlg==1) {
				// **↑↑↑　2018/12/17 LBN石川 拠店検索権限制御対応 ↑↑↑**//

					// 統括部一覧取得
					List<Object[]> toukatsubuList = machineSearchInfoService.findByToukatsubuList();
					logger.config("[GNAV][DEBUG] toukatsubuList="+toukatsubuList.size());
					for(int index = 0; index < toukatsubuList.size(); index++) {
						hashMap.put("toukatsubuCd", toukatsubuList.get(index)[0].toString());
						hashMap.put("toukatsubuName", toukatsubuList.get(index)[1].toString());
						hashMapList.add(hashMap);
						hashMap = new HashMap<String, String>();
					}
					machineSearchInfoModel.setToukatsubuList(hashMapList);
	//				hashMap.clear();
	//				hashMapList.clear();
					hashMapList = new ArrayList<>();


					// 拠店一覧
					List<Object[]> kyotenList = machineSearchInfoService.findByKyotenList() ;
					logger.config("[GNAV][DEBUG] kyotenList="+kyotenList.size());
					for(int index = 0; index < kyotenList.size(); index++) {
						hashMap.put("kyotenCd", kyotenList.get(index)[0].toString());
						hashMap.put("kyotenName", kyotenList.get(index)[1].toString());
						hashMapList.add(hashMap);
						hashMap = new HashMap<String, String>();
					}
					machineSearchInfoModel.setKyotenList(hashMapList);
					hashMapList = new ArrayList<>();

					// サービス工場一覧
					List<Object[]> serviceKoujouList = machineSearchInfoService.findByServiceKoujouList();
					for(int index = 0; index < serviceKoujouList.size(); index++) {
						hashMap.put("serviceKoujyouCd", serviceKoujouList.get(index)[0].toString());
						hashMap.put("serviceKoujyouName", serviceKoujouList.get(index)[1].toString());
						hashMapList.add(hashMap);
						hashMap = new HashMap<String, String>();
					}
					machineSearchInfoModel.setServiceKoujyouList(hashMapList);
					hashMapList = new ArrayList<>();

				}
				// 2019/04/10 DucNKT 定期整備画面に追加されました：開始
				// 定期整備部品一覧取得
				//↓↓↓ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :start ↓↓↓//
				List<String> maintainecePartItemList = machineSearchInfoService.findByMaintainecePartItemList(ConvertUtil.convertLanguageCd(deviceLanguage));
				logger.config("[GNAV][DEBUG] maintainecePartItemList="+maintainecePartItemList.size());
				if(maintainecePartItemList!=null) {
					machineSearchInfoModel.setMaintainecePartItemList(maintainecePartItemList);
				}

//				ArrayList<MaintainecePartItemArrayListModel> maintainecePartItemArrayList = new ArrayList<MaintainecePartItemArrayListModel>();
//				List<Object[]> maintaineceParts = machineSearchInfoService.findByMaintainecePartItemList(ConvertUtil.convertLanguageCd(deviceLanguage));
//				if(maintaineceParts != null) {
//
//					int listSize = maintaineceParts.size();
//					for(Integer listCount = 0 ; listCount < listSize; listCount++ ) {
//						MaintainecePartItemArrayListModel maintainecePartItemListModel = new MaintainecePartItemArrayListModel();
//						Object[] listColumn = maintaineceParts.get(listCount);
//
//						if(listColumn[0] != null) {
//							maintainecePartItemListModel.setMaintainecePartNumber(((Number)listColumn[0]).intValue());
//						}
//						if(listColumn[1] != null) {
//							maintainecePartItemListModel.setMaintainecePartName((String)listColumn[1]);
//						}
//						//↑↑↑ 2019/05/15 iDEA山下 GNavの定期整備部品番号は一意ではないためリスト値除外 :end  ↑↑↑//
//						maintainecePartItemArrayList.add(maintainecePartItemListModel);
//					}
//				}
//				if(!maintainecePartItemArrayList.isEmpty()) {
//					machineSearchInfoModel.setMaintainecePartItemList(maintainecePartItemArrayList);
//				}else {
//					machineSearchInfoModel.setMaintainecePartItemList(null);
//				}
				//↑↑↑ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :end ↑↑↑//
				// 2019/04/10 DucNKT 定期整備画面に追加されました：終了

				// **↓↓↓ 2019/10/17  RASIS岡本 お客様名検索対応↓↓↓**//
				// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
				if(customerNamePriority != null && customerNamePriority == 1) {
					//お客様名検索モードのみリスト値取得
					List<String> customerNameList = machineSearchInfoService.findByCustomerNameList(processTypeFlg,kigyouCd,sosikiCd,groupNo);
					logger.config("[GNAV][DEBUG] customerNameList="+customerNameList.size());

					if(customerNameList!=null) {
						machineSearchInfoModel.setCustomerNameList(customerNameList);
					}
				}
				// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//
				// **↑↑↑ 2019/10/17  RASIS岡本 お客様名検索対応↑↑↑**//


				machineSearchInfoModel.setStatusCode(Constants.CON_OK);

				return Response.ok(machineSearchInfoModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(Constants.CON_UNKNOWN_ERROR).build();
		}
	}
}
