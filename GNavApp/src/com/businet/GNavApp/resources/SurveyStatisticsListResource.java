package com.businet.GNavApp.resources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.SurveyStatisticsArraylistModel;
import com.businet.GNavApp.models.SurveyStatisticsListModel;

@Path("/surveyStatisticsList")
@Stateless
public class SurveyStatisticsListResource {

	private static final Logger logger = Logger.getLogger(SurveyStatisticsListResource.class.getName());


	@EJB
	ISurveyStatisticsService surveyStatisticsService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IUserService userService;


	/**
	 * [API No.36] アンケート統計一覧取得
	 * @param userId
	 * @param sortFlg
	 * @param deviceLanguage
	 * @param startRecord
	 * @return Response.JSON surveyStatisticsListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("sortFlg") Integer sortFlg,
				@FormParam("deviceLanguage") Integer deviceLanguage,
				@FormParam("startRecord") Integer startRecord
			){

		logger.info("[GNAV][POST] userId="+userId+", sortFlg="+sortFlg+", deviceLanguage="+deviceLanguage+", startRecord="+startRecord);

		SurveyStatisticsListModel surveyStatisticsListModel = new SurveyStatisticsListModel();
		ArrayList<SurveyStatisticsArraylistModel> surveystatisticsArraylist = new ArrayList<SurveyStatisticsArraylistModel>();

		try {

			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String groupNo = null;
			if(user.get(0)[3]!=null)
				groupNo = (String)user.get(0)[3];

			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();


			//アンケート統計一覧取得
			List<Object[]> surveyStatisticsList = surveyStatisticsService.findSurveyStatistics(sortFlg ,deviceLanguage ,sosikiCd ,kigyouCd ,groupNo ,typeFlg);

			if(surveyStatisticsList!=null) {
				Integer maxCount = Constants.CON_MAX_COUNT;
				if(surveyStatisticsList.size() >= maxCount )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				surveyStatisticsListModel.setSurveyStatisticsCount(surveyStatisticsList.size());
				Integer record = startRecord-1;			// 1 - 51 - 101 -
				Integer maxRecord = surveyStatisticsList.size();

				for(int counter = record; counter < (record+50) ; counter++ ) {

					if(counter==(maxRecord))
						break;

					SurveyStatisticsArraylistModel surveyStatisticsArraylistModel = new SurveyStatisticsArraylistModel();

					Object[] result = surveyStatisticsList.get(counter);

					surveyStatisticsArraylistModel.setSurveyId(((Number)result[0]).intValue()); //アンケートID
					surveyStatisticsArraylistModel.setSurveyName((String)result[1]); //アンケート名
					SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm");
					if(result[2]!=null) {
						surveyStatisticsArraylistModel.setInputTo(sdf.format(result[2])); //入力期限
					}
					surveyStatisticsArraylistModel.setTargetFigures(((Number)result[3]).intValue()); //回答目標数
					surveyStatisticsArraylistModel.setAchievementFigures(((Number)result[4]).intValue()); //回答数
					surveyStatisticsArraylistModel.setAchievementRate(((Number)result[5]).intValue()); //回答率

					surveystatisticsArraylist.add(surveyStatisticsArraylistModel);
				}

				surveyStatisticsListModel.setSurveyList(surveystatisticsArraylist);

			}else {
				surveyStatisticsListModel.setSurveyStatisticsCount(0);

			}
			surveyStatisticsListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(surveyStatisticsListModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
