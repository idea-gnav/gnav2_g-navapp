package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.googleMapAPILog.IGoogleMapAPILogService;
import com.businet.GNavApp.models.ReturnContainer;



@Path("/googleMapApiLog")
@Stateless
public class GoogleMapApiLog {

	private static final Logger logger = Logger.getLogger(GoogleMapApiLog.class.getName());

	@EJB
	IGoogleMapAPILogService googleMapApiLogService;


	/**
	 * [API No.17] GoogleMapAPI���샍�O
	 * @param userId
	 * @param operStatus
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("operatingStatus") String operStatus
			) {

		logger.info("[GNAV][POST] userId="+userId+", operatingStatus="+operStatus);

		try {
			int operatingStatus;
			operatingStatus = Integer.parseInt(operStatus);

			googleMapApiLogService.insertLog(userId, operatingStatus,Constants.GOOGLE_MAP_LOG_FLAG);

			return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}
}
