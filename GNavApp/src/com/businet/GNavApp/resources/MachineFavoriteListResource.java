package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.favorite.IMachineFavoriteListService;
import com.businet.GNavApp.entities.TblUserSetting;
import com.businet.GNavApp.models.MachineFavoriteArraylistModel;
import com.businet.GNavApp.models.MachineFavoriteListModel;
import com.businet.GNavApp.models.ReturnContainer;


@Path("/machineFavoriteList")
@Stateless
public class MachineFavoriteListResource {

	private static final Logger logger = Logger.getLogger(MachineFavoriteListResource.class.getName());


	@EJB
	IMachineFavoriteListService favoriteListService;


	/**
	 * [API No.13] ���C�ɓ���ꗗ�擾
	 * @param userId
	 * @param sortFlg
	 * @param startRecord
	 * @return Response.JSON MachineFavoriteListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response machineFavoriteList(
				@FormParam("userId") String userId,
				@FormParam("sortFlg") Integer sortFlg,
				@FormParam("startRecord") Integer startRecord
			){

		logger.info("[GNAV][POST] userId="+userId+", sortFlg="+sortFlg+", startRecord="+startRecord);

		MachineFavoriteListModel machineFavoriteListModel = new MachineFavoriteListModel();
		ArrayList<MachineFavoriteArraylistModel> machineFavoriteArraylist = new ArrayList<MachineFavoriteArraylistModel>();

		try {

			//�I���@�Ԏ擾
			TblUserSetting userSetting = favoriteListService.findKibanSelect(userId);

			logger.config("[GNAV][DEBUG] kibanSelect="+userSetting.getKibanSelect());

			//���C�ɓ���@�B�ꗗ�擾
			List<Object[]> favoriteMachineList = favoriteListService.findFavoriteMachineNativeQuery(userId, userSetting.getKibanSelect(), sortFlg);

			if(favoriteMachineList!=null) {

				machineFavoriteListModel.setMachineFavoriteCount(favoriteMachineList.size());
				Integer record = startRecord-1;			// 1 - 51 - 101 -
				Integer maxRecord = favoriteMachineList.size();

				for(int counter = record; counter < (record+50) ; counter++ ) {

					if(counter==(maxRecord))
						break;

					MachineFavoriteArraylistModel machineFavoriteArrayListModel = new MachineFavoriteArraylistModel();


					Object[] result = favoriteMachineList.get(counter);

//					Integer serialNumber = ((Number)result[1]).intValue();
					Long serialNumber = ((Number)result[1]).longValue();
					machineFavoriteArrayListModel.setSerialNumber(serialNumber);
					machineFavoriteArrayListModel.setManufacturerSerialNumber((String)result[2]);

					if((String)result[4] != null)
						machineFavoriteArrayListModel.setCustomerManagementNo((String)result[4]);
					else
						machineFavoriteArrayListModel.setCustomerManagementNo("");

					//������2019/11/20 RASIS ���{���q�l���Ή�������
					if((String) result[5] != null)
						machineFavoriteArrayListModel.setCustomerName((String)result[5]);
					else
						machineFavoriteArrayListModel.setCustomerName(""); 
					//������2019/11/20 RASIS ���{���q�l���Ή�������
					
					machineFavoriteArraylist.add(machineFavoriteArrayListModel);
				}


				machineFavoriteListModel.setMachineList(machineFavoriteArraylist);

			}else {
				machineFavoriteListModel.setMachineFavoriteCount(0);

			}
			machineFavoriteListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(machineFavoriteListModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}


	}
}
