package com.businet.GNavApp.resources;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.DtcDetailArrayListModel;
import com.businet.GNavApp.models.DtcDetailListModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Commons;


@Path("/dtcDetailList")
@Stateless
public class DtcDetailListResource {

	private static final Logger logger = Logger.getLogger(DtcDetailListResource.class.getName());

	@EJB
	IDtcDitailService dtcDitailService;

	@EJB
	IMachineService machineService;

	@EJB
	IUserService userService;

	@EJB
	ISosikiService sosikiService;


	/**
	 * [API No.8] DTC詳細一覧取得
	 * @param userId
	 * @param serialNumber
	 * @param sortFlg
	 * @param warningDateFrom
	 * @param warningDateTo
	 * @param warningCd
	 * @param startRecord
	 * @return Response.JSON DtcDetailListModel
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("serialNumber") Long serialNumber,
				@FormParam("sortFlg") Integer sortFlg,
				@FormParam("warningDateFrom") String warningDateFrom,
				@FormParam("warningDateTo") String warningDateTo,
				@FormParam("warningCd") String warningCd,
				@FormParam("deviceLanguage") Integer deviceLanguage,
				@FormParam("startRecord") Integer startRecord
			) throws Exception {

		logger.info("[GNAV][POST] userId="+userId+", serialNumber="+serialNumber+", sortFlg="+sortFlg+", warningDateFrom="+warningDateFrom+", warningDateTo="+warningDateTo
				+", warningCd="+warningCd+", deviceLanguage="+deviceLanguage+", startRecord="+startRecord);

		try {

			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);
			String kengenCd = null;
			if(user.get(0)[4]!=null)
				kengenCd = user.get(0)[4].toString();

			DtcDetailListModel dtcDetailListModel = new DtcDetailListModel();

			MstMachine machine = machineService.findByMachine(serialNumber);

			if(machine != null) {
				dtcDetailListModel.setSerialNumber(machine.getKibanSerno());

				dtcDetailListModel.setManufacturerSerialNumber(machine.getKiban());

				// 機械カテゴリ区分	machineModelCategory
				dtcDetailListModel.setMachineModelCategory(Commons.identifyKibanType(machine.getKiban(),machine.getConType()));

				dtcDetailListModel.setCustomerManagementNo(machine.getUserKanriNo());

				dtcDetailListModel.setScmModel(machine.getModelCd());

				// 所在地
//				dtcDetailListModel.setLatestLocation(machine.getPosition());
				String positionStr = machineService.findByMachinePosition(machine.getKibanSerno(), machine.getConType(), ConvertUtil.convertLanguageCd(deviceLanguage));
				if(positionStr!=null)
					dtcDetailListModel.setLatestLocation(positionStr);


				// 緯度
				if(machine.getNewIdo()!=null)
					dtcDetailListModel.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));

				// 経度
				if(machine.getNewKeido()!=null)
					dtcDetailListModel.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));

				// アワメータ
				if(machine.getNewHourMeter()!=null)
					dtcDetailListModel.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);

				// 燃料レベル
				dtcDetailListModel.setFuelLevel(machine.getNenryoLv());

				if(machine.getRecvTimestamp() != null)
					dtcDetailListModel.setLatestUtcCommonDateTime(ConvertUtil.formatYMD(machine.getRecvTimestamp()));

				dtcDetailListModel.setDefLevel(machine.getUreaWaterLevel());

				if(machine.getSosikiCd()!=null)
					dtcDetailListModel.setCustomerManagementName(sosikiService.findByKigyouSosikiName(machine.getSosikiCd()));


				// 機械アイコン
				DisplayConditionModel displayCondition = Commons.machineIconType(machineService.findByMachineTypeControl(machine.getConType(), machine.getMachineModel()), machine.getKiban());
				dtcDetailListModel.setIconType(displayCondition.getMachineIconType());

				// **↓↓↓　2020/01/30  iDEA山下 発生中警報レベル追加 ↓↓↓**//
				//定期整備 レベル取得
				Integer periodiclevel = 0;
				Integer yokokuCount = machine.getYokokuCount();
				Integer keyikokuCount = machine.getKeyikokuCount();
				if(keyikokuCount > 0 )
					periodiclevel = 2;
				else if(yokokuCount > 0)
					periodiclevel = 1;


				//DTC レベル取得
				List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
						serialNumber, 1, null, null, null, Integer.parseInt(kengenCd.toString()), ConvertUtil.convertLanguageCd(deviceLanguage),1);
				Integer dtcLv = 1;

				if(dtcs != null) {
					Object[] dtc = dtcs.get(0);
					if(dtc[8]!=null)
						dtcLv = Integer.parseInt(dtc[8].toString());
				}

				//警報アイコン種別
				dtcDetailListModel.setAlertLevelM(Commons.alertLevel(dtcLv, periodiclevel));

				// **↑↑↑　2020/01/30  iDEA山下 発生中警報レベル追加 ↑↑↑**//


			}


			List<Object[]> userSosikiKengen =userService.findByUserSosikiKengenNativeQuery(userId);

			// **↓↓↓　2020/01/30  iDEA山下 発生中警報レベル追加対応 ↓↓↓**//
//			List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
//							serialNumber, sortFlg, warningDateFrom, warningDateTo, warningCd, Integer.parseInt(userSosikiKengen.get(0)[4].toString()), ConvertUtil.convertLanguageCd(deviceLanguage));
			List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
					serialNumber, sortFlg, warningDateFrom, warningDateTo, warningCd, Integer.parseInt(userSosikiKengen.get(0)[4].toString()), ConvertUtil.convertLanguageCd(deviceLanguage),0);
			// **↑↑↑　2020/01/30  iDEA山下 発生中警報レベル追加対応 ↑↑↑**//


			if(dtcs != null) {

				Integer maxCount = 1001;
				if(dtcs.size() >= maxCount ) {
					dtcDetailListModel.setStatusCode(Constants.CON_WARNING_MAXIMUN_NUMBER);
					return Response.ok(dtcDetailListModel).build();
				}

				ArrayList<DtcDetailArrayListModel> dtcDetailArrayList = new ArrayList<DtcDetailArrayListModel>();

				Integer record = startRecord-1;
				for(int counter =  record; counter < (record+50) ; counter++ ) {

					if( dtcs.size() == counter) {
						break;}

					DtcDetailArrayListModel dtcDetailArrayListModel = new DtcDetailArrayListModel();

					Object[] dtc = dtcs.get(counter);

					double hourMeter = 0;
					if(dtc[9]!=null) {
						hourMeter = ((Number)dtc[9]).intValue();
						dtcDetailArrayListModel.setHourMeter(Math.floor(hourMeter/60.0*10)/10);
					}

					String alertLv = null;
					if(dtc[8]!=null) {
						String al = (String)dtc[8];
						if(al.equals("1"))
							alertLv = "0";
						else if(al.equals("2"))
							alertLv = "1";
						else if(al.equals("3"))
							alertLv = "2";
					}
//					if(dtc[8]!=null)
//						alertLv = (String)dtc[8];
					dtcDetailArrayListModel.setAlertLv(alertLv);

					//発生日時現地時間
					//復旧日時現地時間
					SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm");
					if((Timestamp)dtc[10]!=null) {
						dtcDetailArrayListModel.setHasseiDate(sdf.format((Timestamp)dtcs.get(counter)[10]));
					}
					if((Timestamp)dtc[11]!=null)
						dtcDetailArrayListModel.setFukkyuDate(sdf.format((Timestamp)dtcs.get(counter)[11]));

					//warnHistoryContent
					StringBuilder sb = new StringBuilder();
					String dtcEventNo = null;
					if(dtc[16]!=null) {
						dtcEventNo = (String)dtc[16];
//						sb.append(dtcEventNo);	// FLG_VISIBLE
					}
					dtcDetailArrayListModel.setDtcEventNo(dtcEventNo);

					if(dtc[5]!=null)
						sb.append((String)dtc[5]+"-");	// KUBUN_NAME
					if(dtc[6]!=null)
						sb.append((String)dtc[6]);		// EVENT_NAME


					dtcDetailArrayListModel.setWarnHistoryContent(new String(sb));		//DTC内容


					/*
					 * pdf存在チェック　0:PDF無 1:PDF有
					 */
					String pdf = dtcDitailService.findByPDF(serialNumber, dtcEventNo, ConvertUtil.convertLanguageCd(deviceLanguage));
					if(pdf!=null)
						dtcDetailArrayListModel.setPdfFlg(1);
					else
						dtcDetailArrayListModel.setPdfFlg(0);

					dtcDetailArrayList.add(dtcDetailArrayListModel);

				}


				dtcDetailListModel.setDtcDetailList(dtcDetailArrayList);
				dtcDetailListModel.setDtcCount(dtcs.size());

			}else {
				// count 0
				dtcDetailListModel.setDtcDetailList(null);
				dtcDetailListModel.setDtcCount(0);

			}

			dtcDetailListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(dtcDetailListModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}


