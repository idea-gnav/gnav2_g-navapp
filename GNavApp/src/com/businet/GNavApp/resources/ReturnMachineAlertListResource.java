package com.businet.GNavApp.resources;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.returnMachine.IReturnMachineService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ReturnMachineAlertListModel;
import com.businet.GNavApp.models.ReturnMachineModel;

@Path("/returnMachineAlertList")
@Stateless
public class ReturnMachineAlertListResource {

	private static final Logger logger = Logger.getLogger(ReturnMachineAlertListResource.class.getName());

	@EJB
	IReturnMachineService returnMachineService;

	/**
	 * [API No.22] リターンマシン通知一覧取得
	 *
	 * @param userId         - ユーザーＩＤ
	 * @param kibanSelect 	 - 機番選択区分
	 * @param sortFlg        - 並び替え条件
	 * @param startRecord    - レコード開始位置
	 * @param returnDateFrom - リターンマシン発生日From
	 * @param returnDateTo   - リターンマシン発生日To
	 * @param customerName   - お客様名入力
	 * @return Response.JSON ReturnMachineListModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
			@FormParam("userId") String userId
			,@FormParam("kibanSelect") Integer kibanSelect
			,@FormParam("sortFlg") Integer sortFlg
			,@FormParam("startRecord") Integer startRecord
			,@FormParam("returnDateFrom") String returnDateFrom
			,@FormParam("returnDateTo") String returnDateTo
			,@FormParam("customerName") String customerName) {

		logger.info("[GNAV][POST] userId=" + userId + ", kibanSelect="+ kibanSelect +", sortFlg=" + sortFlg + ", startRecord=" + startRecord
				+ ", returnDateFrom=" + returnDateFrom + ", returnDateTo=" + returnDateTo + ", customerName="
				+ customerName);


		ReturnMachineAlertListModel returnMachineAlertList = new ReturnMachineAlertListModel();

		try {

			List<Object[]> returnMachineAlerts = returnMachineService.findByReturnMachineAlertNativeQuery(userId, kibanSelect, sortFlg, returnDateFrom, returnDateTo, customerName);

			ArrayList<ReturnMachineModel> returnMachineList = new ArrayList<ReturnMachineModel>();

			int returnAlertCount = 0;

			if(returnMachineAlerts != null) {
				returnAlertCount = returnMachineAlerts.size();

				if(returnAlertCount >= Constants.CON_MAX_COUNT )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				Integer record = startRecord-1;

				for(int counter =  record; counter < (record+50) ; counter++ ) {
				ReturnMachineModel returnMachine = new ReturnMachineModel();

				if(returnAlertCount == counter)
					break;

				Object[] rm = returnMachineAlerts.get(counter);

				if(rm[0] != null)
					returnMachine.setReturnMachineIconType(((Number) rm[0]).intValue());

				if(rm[1]!=null)
					returnMachine.setSerialNumber(((Number)rm[1]).longValue());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				if ((Timestamp) rm[2] != null)
					returnMachine.setReturnDate(sdf.format((Timestamp) rm[2]));

				if (rm[3] != null)
					returnMachine.setManufacturerSerialNumber((String) rm[3]);

				if (rm[4] != null)
					returnMachine.setCustomerManagementNo((String) rm[4]);

				if (rm[5] != null)
					returnMachine.setCustomerName((String) rm[5]);

				returnMachineList.add(returnMachine);

				}

			}

			returnMachineAlertList.setReturnMachineList(returnMachineList);
			returnMachineAlertList.setReturnAlertCount(returnAlertCount);
			returnMachineAlertList.setStatusCode(Constants.CON_OK);

			return Response.ok(returnMachineAlertList).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
