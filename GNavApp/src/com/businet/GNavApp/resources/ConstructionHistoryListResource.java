package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.construction.IConstructionService;
import com.businet.GNavApp.models.ConstructionHistoryListModel;
import com.businet.GNavApp.models.ConstructionResultModel;
import com.businet.GNavApp.models.ReturnContainer;

@Path("/constructionHistoryList")
@Stateless
public class ConstructionHistoryListResource {

	private static final Logger logger = Logger.getLogger(ConstructionHistoryListResource.class.getName());

	@EJB
	IConstructionService constructionService;

	/**
	 * [API No.49] �{�H��񗚗��ꗗ
	 *
	 * @param userId
	 * @param sortFlg
	 * @param startRecord
	 * @param serialNumber
	 * @param deviceLanguage
	 * @return Response.JSON ConstructionHistoryListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response machineFavoriteList(@FormParam("userId") String userId, @FormParam("sortFlg") Integer sortFlg,
			@FormParam("startRecord") Integer startRecord, @FormParam("serialNumber") Long serialNumber,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", sortFlg=" + sortFlg + ", startRecord=" + startRecord
				+ ", serialNumber=" + serialNumber + ", deviceLanguage=" + deviceLanguage);

		ConstructionHistoryListModel constructionHistoryList = new ConstructionHistoryListModel();

		try {
			// �{�H���ꗗ�擾
			List<Object[]> constructionResults = constructionService.findConstructionHistoryList(serialNumber, sortFlg);
			ArrayList<ConstructionResultModel> constructionResultList = new ArrayList<ConstructionResultModel>();

			Integer constructionResultCount = 0;
			if(constructionResults!=null) {
				constructionResultCount = constructionResults.size();
				Integer maxCount = Constants.CON_MAX_COUNT;
				if(constructionResultCount >= maxCount )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				Integer record = startRecord-1;
				for(int counter =  record; counter < (record+50) ; counter++ ) {
					if( constructionResultCount == counter)
						break;
					Object[] rs = constructionResults.get(counter);

					ConstructionResultModel constructionResult = new ConstructionResultModel();
					if (rs[0] != null)
						constructionResult.setReportDate((String) rs[0]);
					if (rs[1] != null)
						constructionResult.setResearcher((String) rs[1]);
					if (rs[2] != null)
						constructionResult.setResearchLocation((String) rs[2]);
					if (rs[3] != null)
						constructionResult.setReportNo(((Number) rs[3]).intValue());

					constructionResultList.add(constructionResult);
				}
			}

			constructionHistoryList.setConstructionResultList(constructionResultList);
			constructionHistoryList.setConstructionHistoryCount(constructionResultCount);
			constructionHistoryList.setStatusCode(Constants.CON_OK);
			return Response.ok(constructionHistoryList).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
