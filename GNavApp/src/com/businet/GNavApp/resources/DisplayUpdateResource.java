package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.displayUpdate.IDisplayUpdateService;
import com.businet.GNavApp.models.ReturnContainer;



@Path("/displayUpdate")
@Stateless
public class DisplayUpdateResource {

	private static final Logger logger = Logger.getLogger(DisplayUpdateResource.class.getName());

	@EJB
	IDisplayUpdateService displayUpdateService;


	/**
	 * [API No.16] έθξρXV
	 * @param userId
	 * @param unitSelect
	 * @param kibanSelect
	 * @param customerNamePriority
	 * @param searchRangeDistance
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("unitSelect") Integer unitSelect,
				@FormParam("kibanSelect") Integer kibanSelect,
				@FormParam("customerNamePriority") Integer customerNamePriority,
				@FormParam("searchRangeDistance") Integer searchRangeDistance
		) {

		logger.info("[GNAV][POST] userId="+userId+", unitSelect="+unitSelect+", kibanSelect="+kibanSelect+", customerNamePriority="+customerNamePriority+", searchRangeDistance="+searchRangeDistance);

	try {
		// **««« 2019/11/28 iDEARΊ ¨qlΌυDζtOΗΑ «««**//
//		displayUpdateService.displayUpdate(userId, unitSelect, kibanSelect, searchRangeDistance);
		displayUpdateService.displayUpdate(userId, unitSelect, kibanSelect, customerNamePriority, searchRangeDistance);
		// **ͺͺͺ 2019/11/28 iDEARΊ ¨qlΌυDζtOΗΑ ͺͺͺ**//
		return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

	}catch(Exception e) {

		logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
		return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
	}


	}
}
