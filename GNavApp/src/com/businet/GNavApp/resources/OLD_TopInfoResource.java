package com.businet.GNavApp.resources;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcNotice.IDtcNoticeService;
import com.businet.GNavApp.ejbs.returnMachine.OLD_IReturnMachineService;
import com.businet.GNavApp.ejbs.topInfo.ITopInfoService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.TblDocuments;
import com.businet.GNavApp.entities.TblNews;
import com.businet.GNavApp.models.DocumentListModel;
import com.businet.GNavApp.models.DtcNoticesListModel;
import com.businet.GNavApp.models.NewsListModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ReturnMachineNoticeListModel;
import com.businet.GNavApp.models.TopInfoModel;


@Path("/old_topInfo")
@Stateless
public class OLD_TopInfoResource {

	private static final Logger logger = Logger.getLogger(OLD_TopInfoResource.class.getName());

	@EJB
	ITopInfoService topInfoService;

	@EJB
	IUserService userService;

	@EJB
	IDtcNoticeService dtcService;

	// **↓↓↓ 2019/06/07 DucNKT レターんマシーン 追加 ↓↓↓**//
	@EJB
	OLD_IReturnMachineService returnMachineService;
	// **↑↑↑ 2019/06/07 DucNKT レターんマシーン 追加 ↑↑↑**//

	/**
	 * [API No.4] お知らせ一覧取得
	 * @param userId
	 * @param startRecord
	 * @param deviceLanguage
	 * @return Response.JSON TopInfoModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("startRecord") Integer startRecord,
				@FormParam("deviceLanguage") Integer deviceLanguage
			) {

		logger.info("[GNAV][POST] userId="+userId+", startRecord="+startRecord+", deviceLanguage="+deviceLanguage);

		try {

			TopInfoModel topInfoModel = new TopInfoModel();



			// ユーザー組織権限コード取得
			List<Object[]> userSosikiKengen =userService.findByUserSosikiKengenNativeQuery(userId);
			Integer kengenCd = null;
			if(userSosikiKengen != null) {
				kengenCd = Integer.parseInt(userSosikiKengen.get(0)[4].toString());	// [4] KENGEN_CD
			}

			// 資料テーブル一覧取得
			List<TblDocuments> tblDocuments = topInfoService.findByDocuments(ConvertUtil.convertLanguageCd(deviceLanguage), Constants.CON_APP_FLG);
			if(tblDocuments != null) {
				ArrayList<DocumentListModel> documentList = new ArrayList<DocumentListModel>();
				for(TblDocuments document : tblDocuments) {
					int documentCheck = 0;
					DocumentListModel documentListModel = new DocumentListModel();
					documentCheck = checkTarget(userId, kengenCd, document.getTargetUserId(), document.getTargetKengenCd());

					if(documentCheck>0) {
						documentListModel.setDocumentDate(document.getOpDate().toString());
						documentListModel.setDocumentName(document.getDocuName());
						documentListModel.setDocumentUrl(Constants.PDF_URL + document.getDocuPath());
						documentList.add(documentListModel);
					}
				}
				if(documentList!=null) {
					topInfoModel.setDocumentList(documentList);
					topInfoModel.setDocumentCount(documentList.size());
				}

			}else {
				topInfoModel.setDocumentCount(0);
			}

			// ニュース一覧取得
			List<TblNews> tblNews = topInfoService.findByNews(ConvertUtil.convertLanguageCd(deviceLanguage), Constants.CON_APP_FLG);

			if(tblNews != null) {
				ArrayList<NewsListModel> newsList = new ArrayList<NewsListModel>();
				for(TblNews news : tblNews) {
					int newCheck = 0;
					NewsListModel newsListModel = new NewsListModel();
					newCheck = checkTarget(userId, kengenCd, news.getTargetUserId(), news.getTargetKengenCd());

					if(newCheck>0) {
						newsListModel.setNewsContents(news.getNewsContents());
						newsListModel.setNewsDate(new SimpleDateFormat("yyyy-MM-dd").format(news.getOpDate()));
						newsList.add(newsListModel);
					}
				}
				if(newsList!=null) {
					topInfoModel.setNewsList(newsList);
					topInfoModel.setNewsCount(newsList.size());
				}

			}else {
				topInfoModel.setNewsCount(0);

			}


			// 不整合データの削除フラグ更新
			dtcService.updateNoMatchTblFavoriteDtcHistory(userId);

			// 復旧済みの既読処理
			dtcService.updateFukkyuDtcTblFavoriteDtcHistory(userId);

			// **↓↓↓ 2019/02/28 iDEA Yamashita　お気に入りDTCの件数更新はバッチでのみ実施 削除 ↓↓↓**//
			//  お気に入り登録されている機械でDTC未復旧の直近2日分のDTCを取得、該当がある場合は記録
			// dtcService.insertByNotExistsTblFavoriteDtcHistory(userId, kengenCd, null, ConvertUtil.convertLanguageCd(deviceLanguage));
			// **↑↑↑ 2019/02/28 iDEA Yamashita　お気に入りDTCの件数更新はバッチでのみ実施 削除 ↑↑↑**//

			// 未読件数取得
			Integer badgeCount = dtcService.findByNoReadCount(userId);
			if(badgeCount!=null) {
				if(badgeCount>=1000)
					badgeCount=999;	// max
				topInfoModel.setDtcBadgeCount(badgeCount);
			}

			// Favorite DTC 2days and No read
			List<Object[]> dtcNotice = dtcService.findByDtcNoticesNativeQuery(userId, kengenCd, ConvertUtil.convertLanguageCd(deviceLanguage));
			ArrayList<DtcNoticesListModel> dtcNoticesList = new ArrayList<DtcNoticesListModel>();
			if(dtcNotice != null) {

				topInfoModel.setDtcCount(dtcNotice.size());
				Integer record = startRecord-1;			// 1 - 51 - 101 -
				Integer maxRecord = dtcNotice.size();

				for(int counter = record; counter < (record+50) ; counter++ ) {

					DtcNoticesListModel dtcNoticesListModel = new DtcNoticesListModel();

					if(counter==(maxRecord))
						break;

					Object[] dtc = dtcNotice.get(counter);

//					Integer kibanSerno = 0;
					Long kibanSerno = 0l;
					if(dtc[0]!=null) {
						kibanSerno = ((Number)dtc[0]).longValue();
						dtcNoticesListModel.setSerialNumber(kibanSerno);
					}

					String manufacturerSerialNumber = null;
					if(dtc[15]!=null)
						manufacturerSerialNumber = (String)dtc[15];
					dtcNoticesListModel.setManufacturerSerialNumber(manufacturerSerialNumber);

					if(manufacturerSerialNumber!=null && manufacturerSerialNumber.length()>8) {
						if(manufacturerSerialNumber.substring(7,8).equals("6"))
							dtcNoticesListModel.setMachineModelCategory(1); 	//1: model 6
						else
							dtcNoticesListModel.setMachineModelCategory(2);		//2: model 7
					}else {
						dtcNoticesListModel.setMachineModelCategory(2);
					}

//					String lbxSerialNumber = "";
//					if(dtc[16]!=null)
//						lbxSerialNumber = (String)dtc[16];
//					dtcNoticesListModel.setLbxSerialNumber(lbxSerialNumber);

					String customerManagementNo = "";
					if(dtc[17]!=null)
						customerManagementNo = (String)dtc[17];
					dtcNoticesListModel.setCustomerManagementNo(customerManagementNo);

					String alertLv = "";
					if(dtc[8]!=null) {
						String al = (String)dtc[8];
						if(al.equals("1"))
							alertLv = "0";
						else if(al.equals("2"))
							alertLv = "1";
						else if(al.equals("3"))
							alertLv = "2";
					}
					dtcNoticesListModel.setAlertLv(alertLv);

					//発生日時現地時間
					SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm");
					if((Timestamp)dtc[10]!=null)
						dtcNoticesListModel.setHasseiDate(sdf.format((Timestamp)dtc[10]));

					//DTC内容
					StringBuilder sb = new StringBuilder();
					if(dtc[3]!=null)
						sb.append(dtc[3].toString()+":");	// FLG_VISIBLE
//					else
//						sb.append(dtc[4].toString());	// EVENT_CODE 文字列書式 0000

					if(dtc[5]!=null)
						sb.append(dtc[5].toString()+"-");	// KUBUN_NAME

					if(dtc[6]!=null)
						sb.append(dtc[6].toString());	// EVENT_NAME
					dtcNoticesListModel.setWarnHistoryContent(new String(sb));



					if(dtc[19]!=null)
						dtcNoticesListModel.setReadFlg(Integer.parseInt(dtc[19].toString()));
					else
						dtcNoticesListModel.setReadFlg(1);

					dtcNoticesList.add(dtcNoticesListModel);
				}

				topInfoModel.setDtcNoticesList(dtcNoticesList);

			}else {
				topInfoModel.setDtcCount(0);
				topInfoModel.setDtcNoticesList(new ArrayList<DtcNoticesListModel>());

			}

			// **↓↓↓ 2019/06/07 DucNKT レターんマシーン 追加 ↓↓↓**//
            returnMachineService.updateAllReadFlgTblReturnMachineHistory(userId);

            Integer returnMachineBadgeCount = returnMachineService.findByUnReadCount(userId);

			if (returnMachineBadgeCount >= 1000)
				returnMachineBadgeCount = 999; // max
			topInfoModel.setReturnMachineBadgeCount(returnMachineBadgeCount);

			List<Object[]> returnMachines = returnMachineService.findByReturnMachineNoticeNativeQuery(userId);

			ArrayList<ReturnMachineNoticeListModel> returnMachineNoticeList = new ArrayList<ReturnMachineNoticeListModel>();

			Integer returnMachineCount = 0;

			if (returnMachines != null) {
				returnMachineCount = returnMachines.size();

				for (int i = 0; i < returnMachineCount; i++) {
					ReturnMachineNoticeListModel returnMachine = new ReturnMachineNoticeListModel();
					Object[] rm = returnMachines.get(i);

					if (rm[0] != null)
						returnMachine.setSerialNumber(((Number) rm[0]).longValue());

					if (rm[1] != null)
						returnMachine.setManufacturerSerialNumber((String) rm[1]);

					if (rm[2] != null)
						returnMachine.setCustomerManagementNo((String) rm[2]);

					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					if ((Timestamp) rm[3] != null)
						returnMachine.setReturnDate(sdf.format((Timestamp) rm[3]));

					if (rm[4] != null)
						returnMachine.setReturnMachineIconType(((Number) rm[4]).intValue());

					if (rm[5] != null)
						returnMachine.setKigyouName((String) rm[5]);

					if (rm[6] != null)
						returnMachine.setReadFlg(((Number) rm[6]).intValue());

					returnMachineNoticeList.add(returnMachine);
				}
			}

			topInfoModel.setReturnMachineCount(returnMachineCount);
			topInfoModel.setReturnMachineNoticeList(returnMachineNoticeList);

			// **↑↑↑ 2019/06/07 DucNKT レターんマシーン 追加 ↑↑↑**//

			topInfoModel.setStatusCode(Constants.CON_OK);
			return Response.ok(topInfoModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}


	}





	// **↓↓↓　2018/06/20 LBN石川  要望不具合管理台帳 NoXX G@NavApp多言語対応改修のため 変更 ↓↓↓**//
	/**
	 * ニュース・資料ターゲット
	 *
	 */
	private int checkTarget(String userId, Integer kengenCd, String targetUserId, String targetKengenCd) {

		int flg = 0;

		if(targetUserId==null && targetKengenCd==null)
			flg = 1;
		else if(targetUserId!=null && Arrays.asList(targetUserId.split(",")).contains(userId))
			flg = 1;
		else if(targetKengenCd!=null && Arrays.asList(targetKengenCd.split(",")).contains(kengenCd.toString()))
			flg = 1;

		return flg;
	}
	// **↑↑↑　2018/06/20 LBN石川  要望不具合管理台帳 NoXX G@NavApp多言語対応改修のため 変更 ↑↑↑**//
}
