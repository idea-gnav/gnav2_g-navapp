package com.businet.GNavApp.resources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.surveyService.ISurveyService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.SurveyArraylistModel;
import com.businet.GNavApp.models.SurveyListModel;
import com.businet.GNavApp.util.Commons;


@Path("/surveyList")
@Stateless
public class SurveyListResource {

	private static final Logger logger = Logger.getLogger(SurveyListResource.class.getName());


	@EJB
	ISurveyService surveyListService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IDtcDitailService dtcDitailService;

	@EJB
	IUserService userService;



	/**
	 * [API No.31] アンケート一覧取得
	 * @param userId
	 * @param sortFlg
	 * @param startRecord
	 * @param serialNumber
	 * @param deviceLanguage
	 * @return Response.JSON serveyListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response machineFavoriteList(
				@FormParam("userId") String userId,
				@FormParam("sortFlg") Integer sortFlg,
				@FormParam("startRecord") Integer startRecord,
				@FormParam("serialNumber") Integer serialNumber,
				@FormParam("deviceLanguage") Integer deviceLanguage
			){

		logger.info("[GNAV][POST] userId="+userId+", sortFlg="+sortFlg+", startRecord="+startRecord+", serialNumber="+serialNumber+", deviceLanguage="+deviceLanguage);

		SurveyListModel serveyListModel = new SurveyListModel();
		ArrayList<SurveyArraylistModel> surveyArraylist = new ArrayList<SurveyArraylistModel>();

		try {

			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);
			String kengenCd = null;
			if(user.get(0)[4]!=null)
				kengenCd = user.get(0)[4].toString();

			MstMachine machine = machineService.findByMachine((long)serialNumber);

			//map表示用項目
			if(machine != null) {
				// 機械管理番号
				serveyListModel.setSerialNumber(machine.getKibanSerno());

				// 機械カテゴリ区分
				serveyListModel.setMachineModelCategory(Commons.identifyKibanType(machine.getKiban(),machine.getConType()));

				// SCM機番
				serveyListModel.setManufacturerSerialNumber(machine.getKiban());

				// お客様管理番号
				serveyListModel.setCustomerManagementNo(machine.getUserKanriNo());

				// お客様名
				if(machine.getSosikiCd()!=null)
					serveyListModel.setCustomerManagementName(sosikiService.findByKigyouSosikiName(machine.getSosikiCd()));

				//SCM型式
				serveyListModel.setScmModel(machine.getModelCd());

				// 機械アイコン
				DisplayConditionModel displayCondition = Commons.machineIconType(machineService.findByMachineTypeControl(machine.getConType(), machine.getMachineModel()), machine.getKiban());
				serveyListModel.setIconType(displayCondition.getMachineIconType());

				// **↓↓↓　2020/01/30  iDEA山下 発生中警報レベル追加 ↓↓↓**//
				//定期整備 レベル取得
				Integer periodiclevel = 0;
				Integer yokokuCount = machine.getYokokuCount();
				Integer keyikokuCount = machine.getKeyikokuCount();
				if(keyikokuCount > 0 )
					periodiclevel = 2;
				else if(yokokuCount > 0)
					periodiclevel = 1;


				//DTC レベル取得
				long serialNo = (long)serialNumber;
				List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
						serialNo, 1, null, null, null, Integer.parseInt(kengenCd.toString()), ConvertUtil.convertLanguageCd(deviceLanguage),1);
				Integer dtcLv = 1;

				if(dtcs != null) {
					Object[] dtc = dtcs.get(0);
					if(dtc[8]!=null)
						dtcLv = Integer.parseInt(dtc[8].toString());
				}

				//警報アイコン種別
				serveyListModel.setAlertLevelM(Commons.alertLevel(dtcLv, periodiclevel));

				// **↑↑↑　2020/01/30  iDEA山下 発生中警報レベル追加 ↑↑↑**//

				// 所在地
//				dtcDetailListModel.setLatestLocation(machine.getPosition());
				String positionStr = machineService.findByMachinePosition(machine.getKibanSerno(), machine.getConType(), ConvertUtil.convertLanguageCd(deviceLanguage));
				if(positionStr!=null)
					serveyListModel.setLatestLocation(positionStr);

				// 緯度
				if(machine.getNewIdo()!=null)
					serveyListModel.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));

				// 経度
				if(machine.getNewKeido()!=null)
					serveyListModel.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));

				// アワメータ
				if(machine.getNewHourMeter()!=null)
					serveyListModel.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);

				// 最終通信日
				if(machine.getRecvTimestamp() != null)
					serveyListModel.setLatestUtcCommonDateTime(ConvertUtil.formatYMD(machine.getRecvTimestamp()));

				// 燃料レベル
				serveyListModel.setFuelLevel(machine.getNenryoLv());

				// 尿素水レベル
				serveyListModel.setDefLevel(machine.getUreaWaterLevel());

			}


			//アンケート一覧取得
			List<Object[]> surveyList = surveyListService.findSurveyNativeQuery(sortFlg ,deviceLanguage);

			if(surveyList!=null) {
				Integer maxCount = Constants.CON_MAX_COUNT;
				if(surveyList.size() >= maxCount )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				serveyListModel.setSurveyCount(surveyList.size());
				Integer record = startRecord-1;			// 1 - 51 - 101 -
				Integer maxRecord = surveyList.size();

				for(int counter = record; counter < (record+50) ; counter++ ) {

					if(counter==(maxRecord))
						break;

					SurveyArraylistModel surveyArrayListModel = new SurveyArraylistModel();

					Object[] result = surveyList.get(counter);

					surveyArrayListModel.setSurveyId(((Number)result[0]).intValue());
					surveyArrayListModel.setSurveyName((String)result[1]);
					SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm");
					if(result[2]!=null) {
						surveyArrayListModel.setInputFrom(sdf.format(result[2]));
					}
					if(result[3]!=null) {
						surveyArrayListModel.setInputTo(sdf.format(result[3]));
					}

					surveyArraylist.add(surveyArrayListModel);
				}


				serveyListModel.setSurveyList(surveyArraylist);

			}else {
				serveyListModel.setSurveyCount(0);

			}
			serveyListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(serveyListModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}


	}
}
