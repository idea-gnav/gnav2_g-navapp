package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.returnMachine.IReturnMachineService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.AreaListModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ReturnMachineAreaListModel;

@Path("/returnMachineAreaList")
@Stateless
public class ReturnMachineAreaListResource {

	private static final Logger logger = Logger.getLogger(ReturnMachineAreaListResource.class.getName());

	@EJB
	IReturnMachineService returnMachineService;

	@EJB
	IUserService userService;

	/**
	 * [API No.23] リターンマシンエリア一覧
	 *
	 * @param userId       - ユーザーＩＤ
	 * @param sortFlg      - 並び替え条件
	 * @param startRecord  - レコード開始位置
	 * @param customerName - お客様名入力
	 * @return Response.JSON ReturnMachineAreaListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId, @FormParam("sortFlg") Integer sortFlg,
			@FormParam("startRecord") Integer startRecord, @FormParam("customerName") String customerName) {

		logger.info("[GNAV][POST] userId=" + userId + ", sortFlg=" + sortFlg + ", startRecord=" + startRecord
				+ ", customerName=" + customerName);

		ReturnMachineAreaListModel returnMachineAreaList = new ReturnMachineAreaListModel();

		try {

			List<Object[]> returnMachineAreas = returnMachineService.findByReturnMachineAreaNativeQuery(userId, sortFlg,
					customerName);

			ArrayList<AreaListModel> areaList = new ArrayList<AreaListModel>();

			int returnAreaCount = 0;
			if (returnMachineAreas != null) {
				returnAreaCount = returnMachineAreas.size();

				if (returnAreaCount >= Constants.CON_MAX_COUNT)
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				Integer record = startRecord - 1;

				for (int counter = record; counter < (record + 50); counter++) {
					AreaListModel area = new AreaListModel();

					if(returnAreaCount == counter)
						break;

					Object[] rs = returnMachineAreas.get(counter);
					// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
						if (rs[0] != null)
						area.setKyotenCode(rs[0].toString());

					if (rs[1] != null)
						area.setAreaNumber(((Number) rs[1]).intValue());

					if (rs[2] != null)
						area.setCustomerName(rs[2].toString());

					if (rs[3] != null)
						area.setAreaName(rs[3].toString());
					// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//

					areaList.add(area);

				}

			}

			returnMachineAreaList.setAreaList(areaList);
			returnMachineAreaList.setReturnAreaCount(returnAreaCount);
			returnMachineAreaList.setStatusCode(Constants.CON_OK);

			return Response.ok(returnMachineAreaList).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
