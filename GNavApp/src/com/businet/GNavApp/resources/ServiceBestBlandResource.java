package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ServiceBestBlandArraylistModel;
import com.businet.GNavApp.models.ServiceBestBlandModel;


@Path("/serviceBestBland")
@Stateless
public class ServiceBestBlandResource {

	private static final Logger logger = Logger.getLogger(ServiceBestBlandResource.class.getName());


	@EJB
	ISurveyStatisticsService surveyStatisticsService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IUserService userService;


	/**
	 * [API No.38] サービスベストブランド取得
	 * @param userId
	 * @param surveyId
	 * @param year
	 * @param deviceLanguage
	 * @return Response.JSON ServiceBestBlandModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("surveyId") Integer surveyId,
				@FormParam("year") String year,
				@FormParam("deviceLanguage") Integer deviceLanguage
			){

		logger.info("[GNAV][POST] userId="+userId+", surveyId="+surveyId+", year="+year+", deviceLanguage="+deviceLanguage);

		ServiceBestBlandModel serviceBestBlandModel = new ServiceBestBlandModel();
		ArrayList<ServiceBestBlandArraylistModel> servicebestblandArraylist = new ArrayList<ServiceBestBlandArraylistModel>();

		try {

			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String groupNo = null;
			if(user.get(0)[3]!=null)
				groupNo = (String)user.get(0)[3];

			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

			//サービスベストブランド取得
		List<Object[]> serviceBestBland = surveyStatisticsService.findServiceBestBland(surveyId ,deviceLanguage, year,sosikiCd ,kigyouCd ,groupNo ,typeFlg);

		if(serviceBestBland!=null) {

				Integer maxRecord = serviceBestBland.size();

				for(int counter = 0; counter < maxRecord ; counter++ ) {

					ServiceBestBlandArraylistModel serviceBestBlandArraylistModel = new ServiceBestBlandArraylistModel();

					Object[] result = serviceBestBland.get(counter);

					serviceBestBlandArraylistModel.setFreeInputFlg(((Number)result[0]).intValue()); //フリー入力有無
					serviceBestBlandArraylistModel.setItemId(((Number)result[1]).intValue()); //選択肢ID
					serviceBestBlandArraylistModel.setItemNo(((Number)result[1]).intValue()); //項目番号
					serviceBestBlandArraylistModel.setItem(((String)result[2])); //項目名（ブランド名）
					serviceBestBlandArraylistModel.setSummaryCount(((Number)result[3]).intValue()); //集計件数

					servicebestblandArraylist.add(serviceBestBlandArraylistModel);
				}

				serviceBestBlandModel.setServiceBestBland(servicebestblandArraylist);

			}

			serviceBestBlandModel.setStatusCode(Constants.CON_OK);
			serviceBestBlandModel.setSurveyId(surveyId);

			//ヘッダー情報取得
		//	List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,null,null ,null ,null);
			List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,null,null ,null ,null,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Object[] rs = header.get(0);
			serviceBestBlandModel.setSurveyName((String)rs[0]); //アンケート名
			serviceBestBlandModel.setTargetFigures(((Number)rs[1]).intValue()); //回答目標数
			serviceBestBlandModel.setAchievementFigures(((Number)rs[2]).intValue()); //回答数

			return Response.ok(serviceBestBlandModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}
	}
}