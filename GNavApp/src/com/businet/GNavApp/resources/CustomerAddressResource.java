package com.businet.GNavApp.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.CustomerAddressModel;
import com.businet.GNavApp.models.ReturnContainer;

@Path("/customerAddress")
@Stateless
public class CustomerAddressResource {

	private static final Logger logger = Logger.getLogger(CustomerAddressResource.class.getName());

	@EJB
	ISosikiService sosikiService;

	@EJB
	IUserService userService;

	/**
	 * [API No.25] 顧客住所情報取得
	 * 
	 * @param userId       - ユーザーID
	 * @param customerName - お客様名
	 * @return Response.JSON CustomerAddressModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId, @FormParam("customerName") String customerName) {

		logger.info("[GNAV][POST] userId=" + userId + ", customerName=" + customerName);

		try {
			CustomerAddressModel customerAddress = new CustomerAddressModel();
			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if (user.get(0)[1] != null)
				sosikiCd = (String) user.get(0)[1];

			String kigyouCd = null;
			if (user.get(0)[2] != null)
				kigyouCd = (String) user.get(0)[2];

			String groupNo = null;
			if (user.get(0)[3] != null)
				groupNo = (String) user.get(0)[3];

			String kengenCd = null;
			if (user.get(0)[4] != null)
				kengenCd = user.get(0)[4].toString();

			Integer typeFlg = null;
			if (user.get(0)[5] != null)
				typeFlg = ((Number) user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

			List<Object[]> rs = sosikiService.findByCustomerName(userId, typeFlg, kigyouCd, sosikiCd, groupNo, kengenCd,
					customerName);

			if (rs != null && rs.size() == 1) {
				Object[] obj = rs.get(0);
				if (obj[0] != null)
					customerAddress.setCustomerName((String) (rs.get(0)[0]));
				if (obj[1] != null)
					customerAddress.setCustomerAddress((String) rs.get(0)[1]);
			} else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

			customerAddress.setStatusCode(Constants.CON_OK);
			return Response.ok(customerAddress).build();

		} catch (Exception e) {
			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}
}
