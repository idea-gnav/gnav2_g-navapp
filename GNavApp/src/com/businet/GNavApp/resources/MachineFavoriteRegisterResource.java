package com.businet.GNavApp.resources;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcNotice.IDtcNoticeService;
import com.businet.GNavApp.ejbs.favorite.IMachineFavoriteRegisterService;
import com.businet.GNavApp.ejbs.returnMachine.IReturnMachineService;
import com.businet.GNavApp.models.ReturnContainer;



@Path("/machineFavoriteRegister")
@Stateless
public class MachineFavoriteRegisterResource {

	private static final Logger logger = Logger.getLogger(MachineFavoriteRegisterResource.class.getName());


	@EJB
	IMachineFavoriteRegisterService favoriteRegisterService;

	@EJB
	IDtcNoticeService dtcService;

	// **↓↓↓ 2019/06/07 DucNKT レターんマシーン 追加 ↓↓↓**//
	@EJB
	IReturnMachineService returnMachineService;
	// **↑↑↑ 2019/06/07 DucNKT レターんマシーン 追加 ↑↑↑**//

	/**
	 * [API No.14] お気に入り登録/削除
	 * @param userId
	 * @param addSerialNumber
	 * @param removeSerialNumber
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
			@FormParam("userId") String userId,
			@FormParam("addSerialNumber") String _addSerialNumber,
			@FormParam("removeSerialNumber") String _removeSerialNumber,
			@FormParam("deviceLanguage") Integer deviceLanguage
		)	{

		logger.info("[GNAV][POST] userId="+userId+", addSerialNumber="+_addSerialNumber+", removeSerialNumber="+_removeSerialNumber+", deviceLanguage="+deviceLanguage);

		// Integer.parseInt(addSerialNumber)
		// Long.parseLong(addSerialNumber)
		try {

			// お気に入り登録処理
			String[] addSerialNumbers = null;
			if(_addSerialNumber != null && !_addSerialNumber.equals("")) {
				addSerialNumbers = _addSerialNumber.split(",");
				for(String addSerialNumber : addSerialNumbers){
					favoriteRegisterService.addFavoriteMachine(Long.parseLong(addSerialNumber), userId);

					// お気に入りDTC履歴の既読処理
					Integer kengenCd = null;
					if(favoriteRegisterService.findByUserSosikiKengenNativeQuery(userId).get(0)[4]!=null)
						kengenCd = Integer.parseInt(favoriteRegisterService.findByUserSosikiKengenNativeQuery(userId).get(0)[4].toString());

					// お気に入り登録されている機械でDTC未復旧の直近2日分のDTCを取得し、該当がある場合は記録する。
					// 以前登録してた機械があった場合に更新する　DEL_FLGを更新する。
					dtcService.updateTblFavoriteDtcHistory(userId, Long.parseLong(addSerialNumber), null, 0);
					dtcService.insertByNotExistsTblFavoriteDtcHistory(userId, kengenCd, Long.parseLong(addSerialNumber), ConvertUtil.convertLanguageCd(deviceLanguage));

				}

			}

			// お気に入り削除処理
			String[] removeSerialNumbers = null;
			if(_removeSerialNumber != null && !_removeSerialNumber.equals("")) {
				removeSerialNumbers = _removeSerialNumber.split(",");
				for(String removeSerialNumber : removeSerialNumbers){
					favoriteRegisterService.removeFavoriteMachine(Long.parseLong(removeSerialNumber), userId);

					// お気に入りDTC履歴の削除フラグ更新処理
					dtcService.updateTblFavoriteDtcHistory(userId, Long.parseLong(removeSerialNumber), null, 1);
				}
			}

			// 件数追加に伴い返却値をHashMapで対応。key, value
			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			// **↓↓↓ 2019/06/07 DucNKT レターんマシーン 変更 ↓↓↓**//
			Integer dtcBadge = dtcService.findByNoReadCount(userId);

			// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
			String kyotenCd = returnMachineService.getKyotenCdByUserID(userId);
			Integer returnMachineBadge = returnMachineService.findByUnReadCount(kyotenCd);
			// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//


			if (dtcBadge != null || returnMachineBadge != null) {
				Integer totalBadge = (dtcBadge != null ? dtcBadge : 0) + (returnMachineBadge != null ? returnMachineBadge : 0);
				if (totalBadge >= 1000)
					totalBadge = 999; // max
				// **↓↓↓　2019/06/07 DucNKT dtcBadgeCount -> totalBadgeCount modified ↓↓↓**//
				responseMap.put("totalBadgeCount", totalBadge);
				// **↑↑↑　2019/06/07 DucNKT  dtcBadgeCount -> totalBadgeCount modified ↑↑↑**//
			}
			// **↑↑↑ 2019/06/07 DucNKT レターんマシーン 変更 ↑↑↑**//

			responseMap.put("statusCode", 1000);

			return Response.ok(responseMap).build();
//			return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}


	}
}
