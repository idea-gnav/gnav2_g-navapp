package com.businet.GNavApp.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.ejbs.dtcNotice.IDtcNoticeService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.machineDitail.IMachineDitailService;
import com.businet.GNavApp.ejbs.returnMachine.IReturnMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.surveyService.ISurveyService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.MachineDetailModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Commons;
import com.businet.GNavApp.util.PropertyUtil;

@Path("/machineDetail")
@Stateless
public class MachineDetailResource {

	private static final Logger logger = Logger.getLogger(MachineDetailResource.class.getName());

	@EJB
	IMachineDitailService machineDetailService;

	@EJB
	IMachineService machineService;

	@EJB
	IDtcNoticeService dtcService;

	@EJB
	IUserService userService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	ISurveyService surveyService;

	@EJB
	IDtcDitailService dtcDitailService;

	// **↓↓↓ 2019/06/07 DucNKT レターんマシーン 追加 ↓↓↓**//
	@EJB
	IReturnMachineService returnMachineService;
	// **↑↑↑ 2019/06/07 DucNKT レターんマシーン 追加 ↑↑↑**//

	/**
	 * [API No.6] 機械詳細取得
	 *
	 * @param userId
	 * @param serialNumber
	 * @return Response.JSON machineDetailModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response getMachineDitail(@FormParam("userId") String userId, @FormParam("serialNumber") Long serialNumber,
			@FormParam("topFlg") Integer topFlg, @FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", serialNumber=" + serialNumber + ", topFlg=" + topFlg
				+ ", deviceLanguage=" + deviceLanguage);

		MachineDetailModel machineDetail = new MachineDetailModel();

		try {

			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);
			String kengenCd = null;
			if(user.get(0)[4]!=null)
				kengenCd = user.get(0)[4].toString();


			MstMachine machine = machineService.findByMachine(serialNumber);

			if (machine != null) {

//				機械管理番号	serialNumber
				machineDetail.setSerialNumber(machine.getKibanSerno());

//				機械カテゴリー区分	machineModelCategory
				machineDetail
						.setMachineModelCategory(Commons.identifyKibanType(machine.getKiban(), machine.getConType()));

//				最新緯度	ido
//				最新経度	keido
				Double ido = null;
				Double keido = null;
				if (machine.getNewIdo() != null)
					ido = ConvertUtil.parseLatLng(machine.getNewIdo());
				if (machine.getNewKeido() != null)
					keido = ConvertUtil.parseLatLng(machine.getNewKeido());

				machineDetail.setIdo(ido);
				machineDetail.setKeido(keido);

				// 項目制御パターン
				DisplayConditionModel displayCondition = Commons.machineIconType(
						machineService.findByMachineTypeControl(machine.getConType(), machine.getMachineModel()),
						machine.getKiban());

				Integer patternType = displayCondition.getMachineDetailType(); // machineDetail
				machineDetail.setPatternType(patternType);
				machineDetail.setDisplayItemPattern(PropertyUtil.readProperties(Constants.APP_CONFIG_PATH,
						Constants.TYPE_MACHINE_DETAIL + patternType, null));
				// 機械アイコン
				machineDetail.setIconType(displayCondition.getMachineIconType());

				// **↓↓↓　2020/01/30  iDEA山下 発生中警報レベル追加 ↓↓↓**//
				//定期整備 レベル取得
				Integer periodiclevel = 0;
				Integer yokokuCount = machine.getYokokuCount();
				Integer keyikokuCount = machine.getKeyikokuCount();
				if(keyikokuCount > 0 )
					periodiclevel = 2;
				else if(yokokuCount > 0)
					periodiclevel = 1;


				//DTC レベル取得
				List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
						serialNumber, 1, null, null, null, Integer.parseInt(kengenCd.toString()), ConvertUtil.convertLanguageCd(deviceLanguage),1);
				Integer dtcLv = 1;

				if(dtcs != null) {
					Object[] dtc = dtcs.get(0);
					if(dtc[8]!=null)
						dtcLv = Integer.parseInt(dtc[8].toString());
				}

				//警報アイコン種別
				machineDetail.setAlertLevelM(Commons.alertLevel(dtcLv, periodiclevel));

				// **↑↑↑　2020/01/30  iDEA山下 発生中警報レベル追加 ↑↑↑**//



				// 機械詳細
				// 1 お客様管理番号 managementNo
				machineDetail.setManagementNo(machine.getUserKanriNo());

				// 2 機番 manufacturerSerialNumber
				machineDetail.setManufacturerSerialNumber(machine.getKiban());

				// 3 SCM型式 scmModel
				machineDetail.setScmModel(machine.getModelCd());

				// 4 警報発生中 warningCurrentlyInProgress
				if (machine.getKeyihouHasseyiTyuCount() != null && machine.getKeyihouHasseyiTyuCount() > 0) {
					machineDetail.setWarningCurrentlyInProgress("○");
				}

				// 5 アワメータ hourMeter
				Double hourMeter = machine.getNewHourMeter();
				if (hourMeter != null)
					machineDetail.setHourMeter(Math.floor(hourMeter / 60.0 * 10) / 10);
				else
					machineDetail.setHourMeter(0.0);

				// 6 施工時間 constructionTime
				if (machine.getNewShikouTime() != null)
					machineDetail.setConstructionTime(ConvertUtil.convertTimeHhMm(machine.getNewShikouTime()));

				// 7 燃料残量 fuel
				machineDetail.setFuelLevel(machine.getNenryoLv());

				// 8 尿素水レベル defLevelAdblueLevel
				machineDetail.setDefLevel(machine.getUreaWaterLevel());

				// 9 生涯燃料消費量 fuelConsumption MST_MACHINE.NEW_NENRYO_L_0_2000
				if (machine.getNewNenryoL02000() != null)
					machineDetail.setFuelConsumption(machine.getNewNenryoL02000());

				// 10 尿素水消費量 defConsumpiton UREA_WATER_CONSUM /1000 cc--> リットル 換算せず
				if (machine.getUreaWaterConsum() != null)
//					machineDetail.setDefConsumpiton(Math.floor(machine.getUreaWaterConsum()/1000.0*10)/10);
					machineDetail.setDefConsumpiton(machine.getUreaWaterConsum());

				// 11.所在地
				String positionStr = machineService.findByMachinePosition(machine.getKibanSerno(), machine.getConType(),
						languageCd);
				// **↓↓↓ 2018/12/20 LBN石川 地図上に住所表示対応 ↓↓↓**//
				machineDetail.setLatestLocation(positionStr);
				if (positionStr != null) {
					if (ido != null && keido != null)
						machineDetail.setLatestLocationLatLng(positionStr + " (" + ido + "," + keido + ")");
					else
						machineDetail.setLatestLocationLatLng(positionStr);
				} else {
					if ((ido != null) && (keido != null))
						machineDetail.setLatestLocationLatLng("(" + ido + "," + keido + ")");
				}
//				if(positionStr!=null) {
//					if(ido!=null && keido!=null)
//						machineDetail.setLatestLocation(positionStr+" ("+ido+","+keido+")");
//					else
//						machineDetail.setLatestLocation(positionStr);
//				}else {
//					if((ido!=null) && (keido!=null))
//						machineDetail.setLatestLocation("("+ido+","+keido+")");
//				}
				// **↑↑↑ 2018/12/20 LBN石川 地図上に住所表示対応 ↑↑↑**//

				// 12 最終通信日 latestUtcCommonDateTime
				if (machine.getRecvTimestamp() != null)
					machineDetail.setLatestUtcCommonDateTime(ConvertUtil.formatYMDHM(machine.getRecvTimestamp()));

				// 13 時差 timeDifference
				if (machine.getJisa() != null)
					machineDetail.setTimeDifference(machine.getJisa());

				// 14 お客様名 customerName
				String machineCustomerName = null;
				if (machine.getSosikiCd() != null)
					machineCustomerName = sosikiService.findByKigyouSosikiName(machine.getSosikiCd());
				// Modify 2018/10/16 aishikawa :Start
//				machineDetail.setCustomerName(machineCustomerName);
				machineDetail.setCustomerManagementName(machineCustomerName);
				// Modify 2018/10/16 aishikawa :End

				// 15 業種 gyoushuNm MST_GYOUSHU.GYOUSYU_NM (MST_SOSIKI.GYOUSYU_CD)
				String gyoushuName = null;
				if (machine.getSosikiCd() != null)
					gyoushuName = sosikiService.findByGyouhshuNm(machine.getSosikiCd(), languageCd);
				machineDetail.setGyoushuNm(gyoushuName);

				// 16 代理店名 dealerName
				// **↓↓↓ 2019/10/15  iDEA山下 代理店 Web版仕様踏襲 ↓↓↓**//
				/*
				if (machine.getConType() != null) {
					if ((machine.getConType().equals("C") || machine.getConType().equals("C2"))
							&& machine.getSosikiTr() != null) {
						// トルコサブ代理店
						if (machine.getSosikiTr() != null)
							machineDetail.setDealerName(sosikiService.findByKigyouSosikiName(machine.getSosikiTr()));

					} else if ((machine.getConType().equals("C") || machine.getConType().equals("C2"))
							&& machine.getSosikiTr() == null) {
//						Integer lbxFlg = sosikiService.findByLbxFlg(machine.getSosikiCd());
						if (machine.getDairitennCd() != null) {
							// LBX機械
							if (machine.getDairitennCd() != null)
								machineDetail
										.setDealerName(sosikiService.findByKigyouSosikiName(machine.getDairitennCd()));
						} else {
							if (machine.getSosikiCd() != null)
								machineDetail.setDealerName(sosikiService.findByDiritenName(machine.getSosikiCd()));
						}
					} else {
						if (machine.getSosikiCd() != null)
							machineDetail.setDealerName(sosikiService.findByDiritenName(machine.getSosikiCd()));
					}
				}
				*/
				if(machine.getSosikiTr() != null) {
					//トルコサブ代理店コードがある場合
					machineDetail.setDealerName(sosikiService.findByKigyouSosikiName(machine.getSosikiTr()));
				}else if(machine.getDairitennCd() != null) {
					//代理店コードがある場合
					machineDetail.setDealerName(sosikiService.findByKigyouSosikiName(machine.getDairitennCd()));
				}else {
					//その他
					machineDetail.setDealerName(sosikiService.findByDiritenName(machine.getSosikiCd()));
				}
				// **↑↑↑ 2019/10/15  iDEA山下 代理店 Web版仕様踏襲 ↑↑↑**//

				// 17 納入日 deliveryDate MST_MACHINE.NONYUBI
				if (machine.getNonyubi() != null)
					machineDetail.setDeliveryDate(ConvertUtil.formatYMD(machine.getNonyubi()));

				// 18 ホットシャットダウン（クーリング時間） hotShutdown TBL_TEIJI_BUNPU_REPORT.COOLING_TIME1 最新日付
				machineDetail.setHotShutdown(machineDetailService.findByHotShutDown(machine.getKibanSerno()));

				// 19 ブレーカモード操作時間 breakerModeOpTime MST_MACHINE.NEW_BREAKER_TIME / 60
				if (machine.getNewBreakerTime() != null)
//					machineDetail.setBreakerModeOpTime(Math.floor(machine.getNewBreakerTime()/60.0*10)/10);
					machineDetail.setBreakerModeOpTime(ConvertUtil.convertTimeHhMm(machine.getNewBreakerTime()));

				// 20 リフマグ吸引回数 lifMagSuction MST_MACHINE.ASPIRATIONS_NUM
				if (machine.getAspirationsNum() != null)
					machineDetail.setLifMagSuction(machine.getAspirationsNum());

				// 21 リフマグ時間 lifMagTime MST_MACHINE.LIFTING_MAGNET_TIME / 60
				if (machine.getLiftingMagnetTime() != null)
//					machineDetail.setLifMagTime(Math.floor(machine.getLiftingMagnetTime()/60.0*10)/10);
					machineDetail.setLifMagTime(ConvertUtil.convertTimeHhMm(machine.getLiftingMagnetTime()));

				// 22 任意 手動 開始回数 manualOptionalStart MST_MACHINE.RAN_SYUDOU_SAISEI_START_NUM
				if (machine.getRanSyudouSaiseiStartNum() != null)
					machineDetail.setManualOptionalStart(machine.getRanSyudouSaiseiStartNum());

				// 23 要求 手動 開始回数 manualRequestStart MST_MACHINE.REQ_SYUDOU_SAISEI_START_NUM
				if (machine.getReqSyudouSaiseiStartNum() != null)
					machineDetail.setManualRequestStart(machine.getReqSyudouSaiseiStartNum());

				// 24 自動再生 開始回数 autoPlayStart MST_MACHINE.JIDOU_SAISEI_START_NUM
				if (machine.getJidouSaiseiStartNum() != null)
					machineDetail.setAutoPlayStart(machine.getJidouSaiseiStartNum());

				// 25 自動再生 完了回数 autoPlayOver MST_MACHINE.JIDOU_SAISEI_OVER_NUM
				if (machine.getJidouSaiseiOverNum() != null)
					machineDetail.setAutoPlayOver(machine.getJidouSaiseiOverNum());

				// 26 手動再生 開始回数 manualPlayStart MST_MACHINE.SYUDOU_SAISEI_START_NUM
				if (machine.getSyudouSaiseiStartNum() != null)
					machineDetail.setManualPlayStart(machine.getSyudouSaiseiStartNum());

				// 27 手動再生 完了回数 manualPlayOver MST_MACHINE.SYUDOU_SAISEI_OVER_NUM
				if (machine.getSyudouSaiseiOverNum() != null)
					machineDetail.setManualPlayOver(machine.getSyudouSaiseiOverNum());

				// 28 エンジンシリアル番号 engineSerialNumber
				if (machine.getEngineSerno() != null) {
					// **↓↓↓ 2018/10/23 LBN石川 ST不具合対応 エンジンシリアル番号 修正 ↓↓↓**//
					if (machine.getConType() != null && machine.getMachineModel() != null) {
						if (machine.getConType().equals("D") && machine.getMachineModel().equals(0)) {
							machineDetail.setEngineSerialNumber(null);
						} else {
							machineDetail.setEngineSerialNumber(machine.getEngineSerno());
						}
					}
//					if(machine.getMachineModel()==1 || machine.getMachineModel()==2) {
//						machineDetail.setEngineSerialNumber(machine.getEngineSerno());
//					}else if(machine.getMachineModel()==0) {
//						//
//					}
					// **↑↑↑ 2018/10/23 LBN石川 ST不具合対応 エンジンシリアル番号 修正 ↑↑↑**//
				}

				// 29 コントローラAバージョン computerAVersion
				// CtrlABuhinNo
				if (machine.getCtrlABuhinNo() != null) {
					String ctrlABuhin = machineDetailService.findByCtrlABuhinNo(machine.getMachineModel(),
							machine.getCtrlABuhinNo(), machine.getConType());
					if (ctrlABuhin != null) {
						machineDetail.setComputerAVersion(ctrlABuhin);
					} else {
//						if(!(new UtilitysEjb().isNumber(machine.getCtrlABuhinNo())))
						if (machine.getCtrlABuhinNo().length() > 1)
							machineDetail.setComputerAVersion(machine.getCtrlABuhinNo());
					}
				}

				// 30 コントローラBバージョン computerBVersion
				if (machine.getCtrlBBuhinNo() != null) {
					String ctrlBBuhin = machineDetailService.findByCtrlBBuhinNo(machine.getMachineModel(),
							machine.getCtrlBBuhinNo(), machine.getConType());
					if (ctrlBBuhin != null) {
						machineDetail.setComputerBVersion(ctrlBBuhin);
					} else {
//						if(!(new UtilitysEjb().isNumber(machine.getCtrlBBuhinNo())))
						if (machine.getCtrlBBuhinNo().length() > 1)
							machineDetail.setComputerBVersion(machine.getCtrlBBuhinNo());
					}
				}

				// 31 コントローラCバージョン computerCVersion
				if (machine.getCtrlScBuhinNo() != null && machine.getMachineModel() != null) {

					// machine_model = 1 or 2 >> C部品
					if (machine.getMachineModel() == 1 || machine.getMachineModel() == 2) {
						String ctrlCBuhin = machineDetailService.findByCtrlCBuhinNo((machine.getCtrlScBuhinNo()),
								machine.getConType());
						if (ctrlCBuhin != null) {
							machineDetail.setComputerCVersion(ctrlCBuhin);
						} else {
							machineDetail.setComputerCVersion(machine.getCtrlScBuhinNo());
						}

						// machine_model = 0
					} else if (machine.getMachineModel() == 0 && machine.getConType() != null) {
						// **↓↓↓ 2018/10/23 LBN石川 ST不具合対応 コントローラC部品 修正 ↓↓↓**//
//						String ctrlSBuhin = machineDetailService
//								.findByCtrlSBuhinNo((machine.getCtrlScBuhinNo()));
//						if(ctrlSBuhin!=null) {
//							machineDetail.setComputerCVersion(ctrlSBuhin);
//						}else{
//								machineDetail.setComputerCVersion(machine.getCtrlScBuhinNo());
//						}
						if (machine.getConType().equals("D")) {
							machineDetail.setComputerCVersion(null);
						} else {
							String ctrlSBuhin = machineDetailService.findByCtrlSBuhinNo((machine.getCtrlScBuhinNo()));
							if (ctrlSBuhin != null) {
								machineDetail.setComputerCVersion(ctrlSBuhin);
							} else {
								machineDetail.setComputerCVersion(machine.getCtrlScBuhinNo());
							}
						}
						// **↑↑↑ 2018/10/23 LBN石川 ST不具合対応 コントローラC部品 修正 ↑↑↑**//
					}

				}

				// 32 コントローラTバージョン computerTVersion
				if (machine.getCtrlTBuhinNo() != null) {
					String ctrlTBuhin = machineDetailService.findByCtrlTBuhinNo(machine.getCtrlTBuhinNo(),
							machine.getConType());
					if (ctrlTBuhin != null) {
						machineDetail.setComputerTVersion(ctrlTBuhin);
					} else {
						machineDetail.setComputerTVersion(machine.getCtrlTBuhinNo());
					}
				}

				// 33 X4メインコントローラシリアル番号 x4ControllerSerialNo
				if (machine.getX4ControllerSerialNo() != null) {
					String mainConSerial = machineDetailService.findByMainConSerialNo(machine.getX4ControllerSerialNo(),
							machine.getConType());
					if (mainConSerial != null) {
						machineDetail.setX4ControllerSerialNo(mainConSerial);
					} else {
						if (machine.getX4ControllerSerialNo().length() > 4)
							machineDetail.setX4ControllerSerialNo(machine.getX4ControllerSerialNo());
					}
				}

				// 34 X4メインコントローラ部品番号 x4ControllerPartNo
				if (machine.getX4ControllerPartNo() != null) {
					String mainConParts = machineDetailService.findByMainConParts(machine.getX4ControllerPartNo(),
							machine.getConType());
					if (mainConParts != null) {
						machineDetail.setX4ControllerPartNo(mainConParts);
					} else {
						if (machine.getX4ControllerPartNo().length() > 4)
							machineDetail.setX4ControllerPartNo(machine.getX4ControllerPartNo());
					}
				}

				// 35 HBコントローラ部品番号 hbControllerPartNo
				if (machine.getHbControllerPartNo() != null) {
					String hbConParts = machineDetailService.findByHbConParts(machine.getHbControllerPartNo());
					if (hbConParts != null) {
						machineDetail.setHbControllerPartNo(hbConParts);
					} else {
						if (machine.getHbControllerPartNo().length() > 4)
							machineDetail.setHbControllerPartNo(machine.getHbControllerPartNo());
					}
				}

				// 36 インバータ部品番号 inverterPartNo
				if (machine.getHbInverterPartNo() != null) {
					String hbInvParts = machineDetailService.findByInvParts(machine.getHbInverterPartNo());
					if (hbInvParts != null) {
						machineDetail.setInverterPartNo(hbInvParts);
					} else {
						if (machine.getHbInverterPartNo().length() > 4)
							machineDetail.setInverterPartNo(machine.getHbInverterPartNo());
					}
				}

				// 37 コンバータ部品番号 converterPartNo
				if (machine.getHbConverterPartNo() != null) {
					String hbConvParts = machineDetailService.findByConvParts(machine.getHbConverterPartNo());
					if (hbConvParts != null) {
						machineDetail.setConverterPartNo(hbConvParts);
					} else {
						if (machine.getHbConverterPartNo().length() > 4)
							machineDetail.setConverterPartNo(machine.getHbConverterPartNo());
					}
				}

				// 38 BMU部品番号 emuPartNo
				if (machine.getHbBmuPartNo() != null) {
					String hbBmuParts = machineDetailService.findByBmuParts(machine.getHbBmuPartNo());
					if (hbBmuParts != null) {
						machineDetail.setBmuPartNo(hbBmuParts);
					} else {
						if (machine.getHbBmuPartNo().length() > 4)
							machineDetail.setBmuPartNo(machine.getHbBmuPartNo());
					}
				}

				// 39 Q4000部品番号 q4000PartNo
				if (machine.getQ4000BuhinNo() != null) {
					String q4000Version = machineDetailService.findByq4000(machine.getQ4000BuhinNo(),
							machine.getConType());
					if (q4000Version != null) {
						machineDetail.setQ4000PartNo(q4000Version);
					} else {
						if (machine.getQ4000BuhinNo().length() > 4)
							machineDetail.setQ4000PartNo(machine.getQ4000BuhinNo());
					}
				}

				// 40 モニターフラッシュバージョン（メイン/リヤモニターバージョン） monitorFlashVersion
				if (machine.getMonitorRushNo() != null)
					machineDetail.setMonitorFlashVersion(machine.getMonitorRushNo());

				// 41 モニターファームウェアバージョン（サブモニターバージョン） monitorFirmwareVersion
				if (machine.getMonitorFirmNo() != null)
					machineDetail.setMonitorFirmwareVersion(machine.getMonitorFirmNo());

				// 42 サービス工場
				if (machine.getServiceKoujouCd() != null) {
					String machineServiceKoujyou = sosikiService.findByKigyouSosikiName(machine.getServiceKoujouCd());
					machineDetail.setServiceKoujyouName(machineServiceKoujyou);
				}

				// 43 統括部
//				String machineToukatsubuCd = machine.getToukatsubuCd();
				String machineKyotenCd = machine.getKyotenCd();
				machineDetail.setToukatsubuName(sosikiService.findByToukatsubuNm(machineKyotenCd));

				// 44 拠店
//				String machineKyotenCd = machine.getKyotenCd();
				machineDetail.setKyotenName(sosikiService.findByKyotenNm(machineKyotenCd));

				// 45 2DMG操作時間 twoDmgOpTime TBL_TEIJI_REPORT.TWO_DMG_TIME / 60 最新日付
				if (machineDetailService.findByTwoDmgOptTime(machine.getKibanSerno()) != null)
//					machineDetail.setTwoDmgOpTime(Math.floor(machineDetailService.findByTwoDmgOptTime(machine.getKibanSerno())/60.0*10)/10);
					machineDetail.setTwoDmgOpTime(ConvertUtil
							.convertTimeHhMm(machineDetailService.findByTwoDmgOptTime(machine.getKibanSerno())));

				// 46 2DMG（メジャーモード）操作時間 twoDmgMajorModeOpTime
				// TBL_TEIJI_REPORT.TWO_MAJOR_DMG_TIME / 60 最新日付
				if (machineDetailService.findByTwoDmgMajorOptTime(machine.getKibanSerno()) != null)
//					machineDetail.setTwoDmgMajorModeOpTime(Math.floor(machineDetailService.findByTwoDmgMajorOptTime(machine.getKibanSerno())/60.0*10)/10);
					machineDetail.setTwoDmgMajorModeOpTime(ConvertUtil
							.convertTimeHhMm(machineDetailService.findByTwoDmgMajorOptTime(machine.getKibanSerno())));

				// お気に入りフラグ favoriteDelFlg
				// favorite
				Integer favoriteDelFlg = machineService.findByFavoriteDelFlg(serialNumber, userId);
				if (favoriteDelFlg != null)
					machineDetail.setFavoriteDelFlg(favoriteDelFlg);
				else
					machineDetail.setFavoriteDelFlg(1);

				// DTC件数管理
				// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
				String kyotenCd = returnMachineService.getKyotenCdByUserID(userId);
				// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
				// topFlgが1の場合、更新処理を行う。
				if (topFlg != null) {
					if (topFlg == 1)
						dtcService.updateTblFavoriteDtcHistory(userId, serialNumber, 1, null);
					// **↓↓↓ 2019/06/07 DucNKT レターんマシーン 変更 ↓↓↓**//
					else if(topFlg == 2)
					{
						// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
						returnMachineService.updateTblReturnMachineHistory(userId,kyotenCd, serialNumber, null, 1);
						// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
					}
					// **↑↑↑ 2019/06/07 DucNKT レターんマシーン 変更 ↑↑↑**//
				}

				// 未読バッジ件数取得
				// **↓↓↓ 2019/06/07 DucNKT レターんマシーン 変更 ↓↓↓**//
				Integer dtcBadge = dtcService.findByNoReadCount(userId);
				// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
				Integer returnMachineBadge = returnMachineService.findByUnReadCount(kyotenCd);
				// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//

				if (dtcBadge != null || returnMachineBadge != null) {
					Integer totalBadge = (dtcBadge != null ? dtcBadge : 0) + (returnMachineBadge != null ? returnMachineBadge : 0);
					if (totalBadge >= 1000)
						totalBadge = 999; // max
					machineDetail.setTotalBadgeCount(totalBadge);
				}
				// **↑↑↑ 2019/06/07 DucNKT レターんマシーン 変更 ↑↑↑**//

				// **↓↓↓　2021/02/18  iDEA山下 SW2メニューへ移植の為削除↓↓↓**//
				// **↓↓↓ 2019/07/12 iDEA山下 アンケート履歴遷移ボタン追加 ↓↓↓**//
//				List<Object[]> surveyResults = surveyService.findSurveyHistoryListByMachine(serialNumber, null,
//						null, 0, deviceLanguage);
//				if(surveyResults != null && surveyResults.size() > 0) {
//					machineDetail.setSurveyFlg(1);
//				}else {
//					machineDetail.setSurveyFlg(0);
//				}
				// **↑↑↑ 2019/07/12 iDEA山下 アンケート履歴遷移ボタン追加 ↑↑↑**//
				// **↑↑↑　2021/02/18  iDEA山下 SW2メニューへ移植の為削除↑↑↑**//

				// **↓↓↓　2021/02/18  iDEA山下 定期整備検索5型除外対応↓↓↓**//
				if(machine.getMachineModel() == 0 && (machine.getConType().equals("T") || machine.getConType().equals("C") || machine.getConType().equals("S") ) && machine.getTeikiFlg() == 0 ) {

					machineDetail.setPeriodicServiceFlg(0);
				}else {
					machineDetail.setPeriodicServiceFlg(1);
				}
				// **↑↑↑　2021/02/18  iDEA山下 定期整備検索5型除外対応↑↑↑**//



			}

			machineDetail.setStatusCode(Constants.CON_OK);
			return Response.ok(machineDetail).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}

}
