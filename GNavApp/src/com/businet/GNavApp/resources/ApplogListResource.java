package com.businet.GNavApp.resources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.mailer.Mailer;
import com.businet.GNavApp.models.ReturnContainer;


@Path("/appLogList")
@Stateless
public class ApplogListResource {

	private static final Logger logger = Logger.getLogger(ApplogListResource.class.getName());

	@EJB
	Mailer mailer;


	/**
	 * [API No.15] アプリケーションログ送信
	 * @param OperatingLog
	 * @param ErrorLog
	 * @param userId
	 * @return Response.JSON statusCode
	 * @throws AddressException
	 * @throws MessagingException
	 * @throws IOException
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("appOperatingLog") String operatingLog,
				@FormParam("appErrorLog") String errorLog,
				@FormParam("userId") String userId
			) throws AddressException, MessagingException, IOException {

		logger.info("[GNAV][POST] userId="+userId+", appOperatingLog="+ operatingLog+"... , appErrorLog="+ errorLog+"...");

		try {

			String[] mailToList = Constants.APP_LOG_MAIL_ADDRESS.split(";");
			String mailSubject = "GNav App logs! ["+userId+"]";
			String mailContent = userId + ": \nThis is the GNav Application logs.";

			String dirPath = Constants.APP_LOG_PATH;
			String operaFilePath = "";
			String errFilePath = "";
			List<String> lstAttachFile = new ArrayList<String>();
			File theDir = new File(dirPath);

			if (!theDir.exists()) {
				theDir.mkdir();
			}

			BufferedWriter writer1 = null;
			BufferedWriter writer2 = null;

			try {

				if (operatingLog.length() > 0 || operatingLog != null) {
					operaFilePath = dirPath + "/appOperatingLog.log";

					writer1 = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(operaFilePath), StandardCharsets.UTF_8));
					String[] wordsOpe = operatingLog.split("'");
					for (String word : wordsOpe) {
						writer1.write(word);
						writer1.newLine();
					}
					lstAttachFile.add(operaFilePath);
					writer1.close();
				}

				if (errorLog.length() > 0 || errorLog !=null) {
					errFilePath = dirPath + "/appErrorLog.log";
					writer2 = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(errFilePath), StandardCharsets.UTF_8));
					String[] wordsErro = errorLog.split("'");
					for (String word : wordsErro) {
						writer2.write(word);
						writer2.newLine();
					}

					lstAttachFile.add(errFilePath);
					writer2.close();
				}

			} catch (Exception e) {

				logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
				return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

			} finally {
				if(writer1 != null) writer1.close();
				if(writer2 != null) writer2.close();
			}

			boolean sendResult = true;
			for(String mailTo : mailToList) {
				// メール送信
				if(!mailer.sendMail(mailTo, mailSubject, mailContent, lstAttachFile))
					sendResult = false;
			}

			if(sendResult)
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
			else
				return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();


		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}



	}
}
