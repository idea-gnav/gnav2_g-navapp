package com.businet.GNavApp.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.construction.IConstructionService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Base64;
import com.businet.GNavApp.util.BunaryToBufferedImage;
import com.businet.GNavApp.models.ConstructionRegistorPhotoListModel;
import com.businet.GNavApp.models.ConstructionRegisterInputModel;
import com.businet.GNavApp.models.ConstructionRegistorModel;


@Path("/constructionRegistor")
@Stateless
public class ConstructionRegistorResource {

	private static final Logger logger = Logger.getLogger(ConstructionRegistorResource.class.getName());

	@EJB
	IConstructionService constructionService;

	/**
	 * [API No.50] �{�H���o�^
	 *
	 * @param userId
	 * @param deviceLanguage
	 * @param reportDelFlg
	 * @param reportNo
	 * @param serialNumber
	 * @param hourMeter
	 * @param researchDate
	 * @param researchLocation
	 * @param workVolume
	 * @param workVolumeUnit
	 * @param reportCountry
	 * @param materialType
	 * @param materialTemperatureUse
	 * @param materialTemperature
	 * @param materialTemperatureUnit
	 * @param outsideAirTemperature
	 * @param outsideAirTemperatureUnit
	 * @param plateTemperature
	 * @param plateTemperatureUnit
	 * @param workType
	 * @param spotSituation
	 * @param pavementDistanceUse
	 * @param pavementDistance
	 * @param pavementWidth
	 * @param pavementWidthTo
	 * @param pavementThicknessLevelingL
	 * @param pavementThicknessLevelingC
	 * @param pavementThicknessLevelingR
	 * @param constructionSpeed
	 * @param levelingCylinderScaleL
	 * @param levelingCylinderScaleR
	 * @param manualSixnessScaleL
	 * @param manualSixnessScaleR
	 * @param manualSixnessScaleUnit
	 * @param stepAdjustmentScaleL
	 * @param stepAdjustmentScaleR
	 * @param crown
	 * @param slopeCrownL
	 * @param slopeCrownR
	 * @param stretchMoldBoardHeightL
	 * @param stretchMoldBoardHeightR
	 * @param tampaRotationsUse
	 * @param tampaRotations
	 * @param tampaAutoFunction
	 * @param vibratorRotations
	 * @param screwHeight
	 * @param screwSpeedScaleL
	 * @param screwSpeedScaleR
	 * @param conveyorSpeedScaleL
	 * @param conveyorSpeedScaleR
	 * @param screwFlowControlUse
	 * @param agcUse
	 * @param agcType
	 * @param extensionScrew
	 * @param pavementTrouble1
	 * @param pavementTrouble2
	 * @param pavementTrouble3
	 * @param pavementTrouble4
	 * @param pavementTrouble5
	 * @param pavementTrouble6
	 * @param pavementTrouble7
	 * @param pavementTrouble8
	 * @param pavementTrouble9
	 * @param pavementTrouble10
	 * @param pavementTrouble11
	 * @param pavementTrouble12
	 * @param tonnageMixedMaterials
	 * @param customerEvaluation
	 * @param rearSlopeL
	 * @param rearSlopeR
	 * @param rearAttackShrinkL
	 * @param rearAttackShrinkR
	 * @param rearAttackMiddleL
	 * @param rearAttackMiddleR
	 * @param rearAttackStretchL
	 * @param rearAttackStretchR
 	 * @param photoList
	 * @return Response.JSON ConstructionRegistorModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(ConstructionRegisterInputModel constructionRegisterParam) {

		logger.info("[GNAV][POST] userId=" + constructionRegisterParam.getUserId() + ", " + 
				"deviceLanguage=" + constructionRegisterParam.getDeviceLanguage() + ", " + 
				"reportDelFlg=" + constructionRegisterParam.getReportDelFlg() + ", " + 
				"reportNo=" + constructionRegisterParam.getReportNo() + ", " + 
				"serialNumber=" + constructionRegisterParam.getSerialNumber() + ", " + 
				"hourMeter=" + constructionRegisterParam.getHourMeter() + ", " + 
				"researcher=" + constructionRegisterParam.getResearcher() + ", " + 
				"researchDate=" + constructionRegisterParam.getResearchDate() + ", " + 
				"researchLocation=" + constructionRegisterParam.getResearchLocation() + ", " + 
				"workVolume=" + constructionRegisterParam.getWorkVolume() + ", " + 
				"workVolumeUnit=" + constructionRegisterParam.getWorkVolumeUnit() + ", " + 
				"reportCountry=" + constructionRegisterParam.getReportCountry() + ", " + 
				"materialType=" + constructionRegisterParam.getMaterialType() + ", " + 
				"materialTemperatureUse=" + constructionRegisterParam.getMaterialTemperatureUse() + ", " + 
				"materialTemperature=" + constructionRegisterParam.getMaterialTemperature() + ", " + 
				"materialTemperatureUnit=" + constructionRegisterParam.getMaterialTemperatureUnit() + ", " + 
				"outsideAirTemperature=" + constructionRegisterParam.getOutsideAirTemperature() + ", " + 
				"outsideAirTemperatureUnit=" + constructionRegisterParam.getOutsideAirTemperatureUnit() + ", " + 
				"plateTemperature=" + constructionRegisterParam.getPlateTemperature() + ", " + 
				"plateTemperatureUnit=" + constructionRegisterParam.getPlateTemperatureUnit() + ", " + 
				"workType=" + constructionRegisterParam.getWorkType() + ", " + 
				"spotSituation=" + constructionRegisterParam.getSpotSituation() + ", " + 
				"pavementDistanceUse=" + constructionRegisterParam.getPavementDistanceUse() + ", " + 
				"pavementDistance=" + constructionRegisterParam.getPavementDistance() + ", " + 
				"pavementWidth=" + constructionRegisterParam.getPavementWidth() + ", " + 
				"pavementWidthTo=" + constructionRegisterParam.getPavementWidthTo() + ", " + 
				"pavementThicknessLevelingL=" + constructionRegisterParam.getPavementThicknessLevelingL() + ", " + 
				"pavementThicknessLevelingC=" + constructionRegisterParam.getPavementThicknessLevelingC() + ", " + 
				"pavementThicknessLevelingR=" + constructionRegisterParam.getPavementThicknessLevelingR() + ", " + 
				"constructionSpeed=" + constructionRegisterParam.getConstructionSpeed() + ", " + 
				"levelingCylinderScaleL=" + constructionRegisterParam.getLevelingCylinderScaleL() + ", " + 
				"levelingCylinderScaleR=" + constructionRegisterParam.getLevelingCylinderScaleR() + ", " + 
				"manualSixnessScaleL=" + constructionRegisterParam.getManualSixnessScaleL() + ", " + 
				"manualSixnessScaleR=" + constructionRegisterParam.getManualSixnessScaleR() + ", " + 
				"manualSixnessScaleUnit=" + constructionRegisterParam.getManualSixnessScaleUnit() + ", " + 
				"stepAdjustmentScaleL=" + constructionRegisterParam.getStepAdjustmentScaleL() + ", " + 
				"stepAdjustmentScaleR=" + constructionRegisterParam.getStepAdjustmentScaleR() + ", " + 
				"crown=" + constructionRegisterParam.getCrown() + ", " + 
				"slopeCrownL=" + constructionRegisterParam.getSlopeCrownL() + ", " + 
				"slopeCrownR=" + constructionRegisterParam.getSlopeCrownR() + ", " + 
				"stretchMoldBoardHeightL=" + constructionRegisterParam.getStretchMoldBoardHeightL() + ", " + 
				"stretchMoldBoardHeightR=" + constructionRegisterParam.getStretchMoldBoardHeightR() + ", " + 
				"tampaRotationsUse=" + constructionRegisterParam.getTampaRotationsUse() + ", " + 
				"tampaRotations=" + constructionRegisterParam.getTampaRotations() + ", " + 
				"tampaAutoFunction=" + constructionRegisterParam.getTampaAutoFunction() + ", " + 
				"vibratorRotations=" + constructionRegisterParam.getVibratorRotations() + ", " + 
				"screwHeight=" + constructionRegisterParam.getScrewHeight() + ", " + 
				"screwSpeedScaleL=" + constructionRegisterParam.getScrewSpeedScaleL() + ", " + 
				"screwSpeedScaleR=" + constructionRegisterParam.getScrewSpeedScaleR() + ", " + 
				"conveyorSpeedScaleL=" + constructionRegisterParam.getConveyorSpeedScaleL() + ", " + 
				"conveyorSpeedScaleR=" + constructionRegisterParam.getConveyorSpeedScaleR() + ", " + 
				"screwFlowControlUse=" + constructionRegisterParam.getScrewFlowControlUse() + ", " + 
				"agcUse=" + constructionRegisterParam.getAgcUse() + ", " + 
				"agcType=" + constructionRegisterParam.getAgcType() + ", " + 
				"extensionScrewUse=" + constructionRegisterParam.getExtensionScrewUse() + ", " + 
				"extensionScrew=" + constructionRegisterParam.getExtensionScrew() + ", " + 
				"pavementTrouble1=" + constructionRegisterParam.getPavementTrouble1() + ", " + 
				"pavementTrouble2=" + constructionRegisterParam.getPavementTrouble2() + ", " + 
				"pavementTrouble3=" + constructionRegisterParam.getPavementTrouble3() + ", " + 
				"pavementTrouble4=" + constructionRegisterParam.getPavementTrouble4() + ", " + 
				"pavementTrouble5=" + constructionRegisterParam.getPavementTrouble5() + ", " + 
				"pavementTrouble6=" + constructionRegisterParam.getPavementTrouble6() + ", " + 
				"pavementTrouble7=" + constructionRegisterParam.getPavementTrouble7() + ", " + 
				"pavementTrouble8=" + constructionRegisterParam.getPavementTrouble8() + ", " + 
				"pavementTrouble9=" + constructionRegisterParam.getPavementTrouble9() + ", " + 
				"pavementTrouble10=" + constructionRegisterParam.getPavementTrouble10() + ", " + 
				"pavementTrouble11=" + constructionRegisterParam.getPavementTrouble11() + ", " + 
				"pavementTrouble12=" + constructionRegisterParam.getPavementTrouble12() + ", " + 
				"pavementTrouble13=" + constructionRegisterParam.getPavementTrouble13() + ", " + 
				"pavementTrouble14=" + constructionRegisterParam.getPavementTrouble14() + ", " + 
				"tonnageMixedMaterials=" + constructionRegisterParam.getTonnageMixedMaterials() + ", " + 
				"customerEvaluation=" + constructionRegisterParam.getCustomerEvaluation() + ", " + 
				"rearSlopeL=" + constructionRegisterParam.getRearSlopeL() + ", " + 
				"rearSlopeR=" + constructionRegisterParam.getRearSlopeR() + ", " + 
				"rearAttackShrinkL=" + constructionRegisterParam.getRearAttackShrinkL() + ", " + 
				"rearAttackShrinkR=" + constructionRegisterParam.getRearAttackShrinkR() + ", " + 
				"rearAttackMiddleL=" + constructionRegisterParam.getRearAttackMiddleL() + ", " + 
				"rearAttackMiddleR=" + constructionRegisterParam.getRearAttackMiddleR() + ", " + 
				"rearAttackStretchL=" + constructionRegisterParam.getRearAttackStretchL() + ", " + 
				"rearAttackStretchR=" + constructionRegisterParam.getRearAttackStretchR() + ", " +
				"photoList=" + constructionRegisterParam.getPhotoList());
		try {
			
			ConstructionRegistorModel constructionRegistor = new ConstructionRegistorModel();
			
			String userId = constructionRegisterParam.getUserId();
			Integer deviceLanguage = constructionRegisterParam.getDeviceLanguage();
			Integer reportDelFlg = constructionRegisterParam.getReportDelFlg();
			Integer reportNo = constructionRegisterParam.getReportNo();
			Long serialNumber = constructionRegisterParam.getSerialNumber();
			Double hourmeter = null;
			if(constructionRegisterParam.getHourMeter() != null) {
				hourmeter = constructionRegisterParam.getHourMeter() * 60.0;
			}
			String researcher = constructionRegisterParam.getResearcher();
			String researchDate = constructionRegisterParam.getResearchDate();
			String researchLocation = constructionRegisterParam.getResearchLocation();
			Double workVolume = constructionRegisterParam.getWorkVolume();
			Integer workVolumeUnit = constructionRegisterParam.getWorkVolumeUnit();
			Integer reportCountry = constructionRegisterParam.getReportCountry();
			Integer materialType = constructionRegisterParam.getMaterialType();
			Integer materialTemperatureUse = constructionRegisterParam.getMaterialTemperatureUse();
			Double materialTemperature = constructionRegisterParam.getMaterialTemperature();
			Integer materialTemperatureUnit = constructionRegisterParam.getMaterialTemperatureUnit();
			Double outsideAirTemperature = constructionRegisterParam.getOutsideAirTemperature();
			Integer outsideAirTemperatureUnit = constructionRegisterParam.getOutsideAirTemperatureUnit();
			Double plateTemperature = constructionRegisterParam.getPlateTemperature();
			Integer plateTemperatureUnit = constructionRegisterParam.getPlateTemperatureUnit();
			Integer workType = constructionRegisterParam.getWorkType();
			String spotSituation = constructionRegisterParam.getSpotSituation();
			Integer pavementDistanceUse = constructionRegisterParam.getPavementDistanceUse();
			Double pavementDistance = constructionRegisterParam.getPavementDistance();
			Double pavementWidth = constructionRegisterParam.getPavementWidth();
			Double pavementWidthTo = constructionRegisterParam.getPavementWidthTo();//2020.01.14 DucNKT added
			Double pavementThicknessLevelingL = constructionRegisterParam.getPavementThicknessLevelingL();
			Double pavementThicknessLevelingC = constructionRegisterParam.getPavementThicknessLevelingC();
			Double pavementThicknessLevelingR = constructionRegisterParam.getPavementThicknessLevelingR();
			Double constructionSpeed = constructionRegisterParam.getConstructionSpeed();
			Double levelingCylinderScaleL = constructionRegisterParam.getLevelingCylinderScaleL();
			Double levelingCylinderScaleR = constructionRegisterParam.getLevelingCylinderScaleR();
			Double manualSixnessScaleL = constructionRegisterParam.getManualSixnessScaleL();
			Double manualSixnessScaleR = constructionRegisterParam.getManualSixnessScaleR();
			Integer manualSixnessScaleUnit = constructionRegisterParam.getManualSixnessScaleUnit();
			Double stepAdjustmentScaleL = constructionRegisterParam.getStepAdjustmentScaleL();
			Double stepAdjustmentScaleR = constructionRegisterParam.getStepAdjustmentScaleR();
			Double crown = constructionRegisterParam.getCrown();
			Double slopeCrownL = constructionRegisterParam.getSlopeCrownL();
			Double slopeCrownR = constructionRegisterParam.getSlopeCrownR();
			Double stretchMoldBoardHeightL = constructionRegisterParam.getStretchMoldBoardHeightL();
			Double stretchMoldBoardHeightR = constructionRegisterParam.getStretchMoldBoardHeightR();
			Integer tampaRotationsUse = constructionRegisterParam.getTampaRotationsUse();
			Double tampaRotations = constructionRegisterParam.getTampaRotations();
			Integer tampaAutoFunction = constructionRegisterParam.getTampaAutoFunction();
			Double vibratorRotations = constructionRegisterParam.getVibratorRotations();
			Double screwHeight = constructionRegisterParam.getScrewHeight();
			Double screwSpeedScaleL = constructionRegisterParam.getScrewSpeedScaleL();
			Double screwSpeedScaleR = constructionRegisterParam.getScrewSpeedScaleR();
			Double conveyorSpeedScaleL = constructionRegisterParam.getConveyorSpeedScaleL();
			Double conveyorSpeedScaleR = constructionRegisterParam.getConveyorSpeedScaleR();
			Integer screwFlowControlUse = constructionRegisterParam.getScrewFlowControlUse();
			Integer agcUse = constructionRegisterParam.getAgcUse();
			Integer agcType = constructionRegisterParam.getAgcType();
			Integer extensionScrewUse = constructionRegisterParam.getExtensionScrewUse();
			String extensionScrew = constructionRegisterParam.getExtensionScrew();
			Integer pavementTrouble1 = constructionRegisterParam.getPavementTrouble1();
			Integer pavementTrouble2 = constructionRegisterParam.getPavementTrouble2();
			Integer pavementTrouble3 = constructionRegisterParam.getPavementTrouble3();
			Integer pavementTrouble4 = constructionRegisterParam.getPavementTrouble4();
			Integer pavementTrouble5 = constructionRegisterParam.getPavementTrouble5();
			Integer pavementTrouble6 = constructionRegisterParam.getPavementTrouble6();
			Integer pavementTrouble7 = constructionRegisterParam.getPavementTrouble7();
			Integer pavementTrouble8 = constructionRegisterParam.getPavementTrouble8();
			Integer pavementTrouble9 = constructionRegisterParam.getPavementTrouble9();
			Integer pavementTrouble10 = constructionRegisterParam.getPavementTrouble10();
			Integer pavementTrouble11 = constructionRegisterParam.getPavementTrouble11();
			Integer pavementTrouble12 = constructionRegisterParam.getPavementTrouble12();
			Integer pavementTrouble13 = constructionRegisterParam.getPavementTrouble13();
			Integer pavementTrouble14 = constructionRegisterParam.getPavementTrouble14();
			Double tonnageMixedMaterials = constructionRegisterParam.getTonnageMixedMaterials();
			String customerEvaluation = constructionRegisterParam.getCustomerEvaluation();
			Double rearSlopeL = constructionRegisterParam.getRearSlopeL();
			Double rearSlopeR = constructionRegisterParam.getRearSlopeR();
			Double rearAttackShrinkL = constructionRegisterParam.getRearAttackShrinkL();
			Double rearAttackShrinkR = constructionRegisterParam.getRearAttackShrinkR();
			Double rearAttackMiddleL = constructionRegisterParam.getRearAttackMiddleL();
			Double rearAttackMiddleR = constructionRegisterParam.getRearAttackMiddleR();
			Double rearAttackStretchL = constructionRegisterParam.getRearAttackStretchL();
			Double rearAttackStretchR = constructionRegisterParam.getRearAttackStretchR();
			List<ConstructionRegistorPhotoListModel> photoList = constructionRegisterParam.getPhotoList();

			// �摜�o�^�p���X�g�쐬
			List<Object[]> constphotoList = null;
			constphotoList = new ArrayList<Object[]>();
			if (reportDelFlg != 1 && photoList != null && photoList.size() > 0) {
				for (ConstructionRegistorPhotoListModel photo : photoList) {
					Object[] obj = new Object[4];
					String fileName = null;
					fileName = "-" + photo.getPhotoNo() + ".jpg";
					obj[0] = photo.getPhotoNo();
					obj[1] = fileName;
					obj[2] = photo.getImageEditFlg();
					obj[3] = photo.getPhotoDate();
					constphotoList.add(obj);
				}
			}
			
			// �{�H���o�^
			Integer constructionResultIdReturn = null;
			constructionResultIdReturn = constructionService.registerConstruction(
					userId,
					deviceLanguage,
					reportDelFlg,
					reportNo,
					serialNumber,
					hourmeter,
					researcher,
					researchDate,
					researchLocation,
					workVolume,
					workVolumeUnit,
					reportCountry,
					materialType,
					materialTemperatureUse,
					materialTemperature,
					materialTemperatureUnit,
					outsideAirTemperature,
					outsideAirTemperatureUnit,
					plateTemperature,
					plateTemperatureUnit,
					workType,
					spotSituation,
					pavementDistanceUse,
					pavementDistance,
					pavementWidth,
					pavementWidthTo,//2020.01.14 DucNKT added
					pavementThicknessLevelingL,
					pavementThicknessLevelingC,
					pavementThicknessLevelingR,
					constructionSpeed,
					levelingCylinderScaleL,
					levelingCylinderScaleR,
					manualSixnessScaleL,
					manualSixnessScaleR,
					manualSixnessScaleUnit,
					stepAdjustmentScaleL,
					stepAdjustmentScaleR,
					crown,
					slopeCrownL,
					slopeCrownR,
					stretchMoldBoardHeightL,
					stretchMoldBoardHeightR,
					tampaRotationsUse,
					tampaRotations,
					tampaAutoFunction,
					vibratorRotations,
					screwHeight,
					screwSpeedScaleL,
					screwSpeedScaleR,
					conveyorSpeedScaleL,
					conveyorSpeedScaleR,
					screwFlowControlUse,
					agcUse,
					agcType,
					extensionScrewUse,
					extensionScrew,
					pavementTrouble1,
					pavementTrouble2,
					pavementTrouble3,
					pavementTrouble4,
					pavementTrouble5,
					pavementTrouble6,
					pavementTrouble7,
					pavementTrouble8,
					pavementTrouble9,
					pavementTrouble10,
					pavementTrouble11,
					pavementTrouble12,
					pavementTrouble13,
					pavementTrouble14,
					tonnageMixedMaterials,
					customerEvaluation,
					rearSlopeL,
					rearSlopeR,
					rearAttackShrinkL,
					rearAttackShrinkR,
					rearAttackMiddleL,
					rearAttackMiddleR,
					rearAttackStretchL,
					rearAttackStretchR,
					constphotoList
					);
			
			constructionRegistor.setReportNo(constructionResultIdReturn);
			constructionRegistor.setStatusCode(Constants.CON_OK);
			return Response.ok(constructionRegistor).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
	
}
