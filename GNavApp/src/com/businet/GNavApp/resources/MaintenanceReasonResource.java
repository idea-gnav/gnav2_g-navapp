package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.CompetitorAnswerCountArrayListModel;
import com.businet.GNavApp.models.MaintenanceReasonArrayListModel;
import com.businet.GNavApp.models.MaintenanceReasonListModel;
import com.businet.GNavApp.models.ReturnContainer;

@Path("/maintenanceReason")
@Stateless
public class MaintenanceReasonResource {

	private static final Logger logger = Logger.getLogger(MaintenanceReasonResource.class.getName());

	@EJB
	ISurveyStatisticsService SurveyStatisticsService;

	@EJB
	IUserService userService;

	/**
	 * [API No.41] 自社或いは他社で整備する理由
	 *
	 * @param userId
	 * @param surveyId
	 * @param year
	 * @param deviceLanguage
	 * @return Response.JSON maintenanceReasonListModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId,
			@FormParam("surveyId") Integer surveyId,
			@FormParam("year") String year,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", surveyId=" + surveyId + ", year=" + year + ", deviceLanguage=" + deviceLanguage);

		try {
			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String groupNo = null;
			if(user.get(0)[3]!=null)
				groupNo = (String)user.get(0)[3];

			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();



			MaintenanceReasonListModel MaintenanceReasonListModel = new MaintenanceReasonListModel();

			//アンケートヘッダー情報取得
//			List<Object[]> headers  = SurveyStatisticsService.findSurveyTargetFigures(surveyId, deviceLanguage,year,null,null,null,null);
			List<Object[]> headers = SurveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,null,null ,null ,null,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Object[] header = headers.get(0);

			//アンケート名称
			String surveyName = null;
			if(header[0]!=null) {
				surveyName = (String)header[0];
			}
			MaintenanceReasonListModel.setSurveyName(surveyName);

			//アンケート回答目標数
			Integer targetFigures = 0;
			if(header[1]!=null) {
				targetFigures = ((Number)header[1]).intValue();
			}
			MaintenanceReasonListModel.setTargetFigures(targetFigures);

			//アンケート実回答数
			Integer achievementFigures = 0;
			if(header[2]!=null) {
				achievementFigures = ((Number)header[2]).intValue();
			}
			MaintenanceReasonListModel.setAchievementFigures(achievementFigures);


			//設問選択肢 選択総数を取得
		//	List<Integer> choiceCountList  = SurveyStatisticsService.choiceCountSum(year,"60,61");
			List<Integer> choiceCountList  = SurveyStatisticsService.choiceCountSum(year,"60,61",sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Integer choiceCountAll = 0;
			if(choiceCountList != null && choiceCountList.size() > 0) {
				choiceCountAll = ((Number)choiceCountList.get(0)).intValue();
			}


			//アンケート統計情報取得
		//	List<Object[]> maintenanceReasonLists = SurveyStatisticsService.findMaintenanceReason(deviceLanguage,year,choiceCountAll);
			List<Object[]> maintenanceReasonLists = SurveyStatisticsService.findMaintenanceReason(deviceLanguage,year,choiceCountAll,sosikiCd ,kigyouCd ,groupNo ,typeFlg);

			if(maintenanceReasonLists != null && maintenanceReasonLists.size() > 0) {
				ArrayList<MaintenanceReasonArrayListModel> maintenanceReasonList  = new ArrayList<MaintenanceReasonArrayListModel>();
				for (Object[] obj : maintenanceReasonLists) {

					MaintenanceReasonArrayListModel maintenanceReason = new MaintenanceReasonArrayListModel();

					String itemNo = null;
					if (obj[0] != null)
						itemNo = (String)obj[0];

					Integer itemId = null;
					if (obj[1] != null)
						itemId = ((Number)obj[1]).intValue();

					String item = null;
					if (obj[2] != null)
						item = (String)obj[2];

					Integer freeInputFlg = null;
					if (obj[3] != null)
						freeInputFlg = ((Number)obj[3]).intValue();

					Integer choiceCount = null;
					if (obj[4] != null)
						choiceCount = ((Number)obj[4]).intValue();

					Integer per = null;
					if (obj[5] != null)
						per = ((Number) obj[5]).intValue();

					maintenanceReason.setItemNo(itemNo);
					maintenanceReason.setItemId(itemId);
					maintenanceReason.setItem(item);
					maintenanceReason.setFreeInputFlg(freeInputFlg);
					maintenanceReason.setChoiceCount(choiceCount);
					maintenanceReason.setAnswerPercentage(per);

					//比較対象社割合を取得
					if(itemId != null && itemId >= 3 && itemId < 6) {
					//	List<Object[]> competitorAnswerCountLists = SurveyStatisticsService.findCompetitorAnswerCount(deviceLanguage,year,itemId);
						List<Object[]> competitorAnswerCountLists = SurveyStatisticsService.findCompetitorAnswerCount(deviceLanguage,year,itemId,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
						if(competitorAnswerCountLists != null && competitorAnswerCountLists.size() > 0) {
							ArrayList<CompetitorAnswerCountArrayListModel> competitorAnswerCountList  = new ArrayList<CompetitorAnswerCountArrayListModel>();
							for (Object[] obj2 : competitorAnswerCountLists) {
								CompetitorAnswerCountArrayListModel competitorAnswerCount = new CompetitorAnswerCountArrayListModel();

								Integer competitorChoiceCount = null;
								if(obj2[0] != null)
									competitorChoiceCount = ((Number)obj2[0]).intValue();

								String competitorChoice = null;
								if(obj2[1] != null)
									competitorChoice = (String)obj2[1];

								competitorAnswerCount.setCompetitorName(competitorChoice);
								competitorAnswerCount.setCompetitorAnswerCount(competitorChoiceCount);

								competitorAnswerCountList.add(competitorAnswerCount);
							}
							maintenanceReason.setCompetitorAnswerCountList(competitorAnswerCountList);
						}
					}
					maintenanceReasonList.add(maintenanceReason);
				}
				MaintenanceReasonListModel.setMaintenanceReasonList(maintenanceReasonList);
			}

			MaintenanceReasonListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(MaintenanceReasonListModel).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}


	}


}
