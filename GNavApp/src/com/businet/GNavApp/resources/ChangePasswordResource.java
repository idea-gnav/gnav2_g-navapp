package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.changePassword.IChangePasswordService;
import com.businet.GNavApp.ejbs.changePassword.ISw2ChangePasswordService;
import com.businet.GNavApp.ejbs.user.ISw2UserService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstUser;
import com.businet.GNavApp.entities.Sw2MstUser;
import com.businet.GNavApp.models.ReturnContainer;


@Path("/changePassword")
@Stateless
public class ChangePasswordResource {

	private static final Logger logger = Logger.getLogger(ChangePasswordResource.class.getName());

	@EJB
	IChangePasswordService changePasswordService;

	@EJB
	ISw2ChangePasswordService sw2ChangePasswordService;

	@EJB
	IUserService userService;

	@EJB
	ISw2UserService sw2UserService;

	/**
	 * [API No.3] パスワード変更
	 * @param userId
	 * @param passwordOld
	 * @param passwordNew
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId,
					@FormParam("passwordOld") String passwordOld,
					@FormParam("passwordNew") String passwordNew,
					@FormParam("systemFlg") Integer systemFlg
				) {

		logger.info("[GNAV][POST] userId="+userId+", passwordOld="+passwordOld+", passwordNew="+passwordNew);

		try {

			// **↓↓↓ 2020/08/21 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//
			// ログイン処理対象システム判定

			if(systemFlg == null) {
				int gnavFlg = 0;
				int sw2Flg = 0;

				//G@Navユーザ存在チェック
				MstUser userG = userService.findByUser(userId,passwordOld);
				if (userG != null) gnavFlg = 1;

				//SW2ユーザ存在チェック
				Sw2MstUser userS = sw2UserService.findBySw2User(userId,passwordOld);
				if (userS != null) sw2Flg = 1;

				//ユーザ重複チェック
				if (gnavFlg == 1 && sw2Flg == 1) {
					// 両DBに存在するID,PASSの場合ログイン対象システムのパラメータを貰う
					logger.config("[GNAV][DEBUG] WARNING_USER_DUPLICATION 1701");
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_USER_DUPLICATION)).build();
				}else if(gnavFlg == 1 && sw2Flg == 0) {
					//パスワード送付処理:G@Nav
					systemFlg = 0;
				}else if(gnavFlg == 0 && sw2Flg == 1) {
					//パスワード送付処理:SW2
					systemFlg = 1;
				}else if(gnavFlg == 0 && sw2Flg == 0) {
					//対象ユーザ無し
					logger.config("[GNAV][DEBUG] WARNING NO USER PASSWORD 1201 | USER NULL");
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
				}

			}
			// **↑↑↑ 2020/08/21 iDEA山下 SW2ユーザ認証追加 ↑↑↑**//



			// パスワード変更処理

			// 対象システム：G@Nav
			if(systemFlg == 0) {

				MstUser user = userService.findByUser(userId, passwordOld);

				if (user != null) {

					changePasswordService.updateByPassword(userId, passwordNew);
				//	return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

				}else {
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
				}

			// **↓↓↓ 2020/08/21 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//
			// 対象システム：SW2
			}else if(systemFlg == 1) {

				//Integer userS = sw2UserService.findBySw2User(userId, passwordOld);
				Sw2MstUser userS = sw2UserService.findBySw2User(userId, passwordOld);

				if(userS != null) {

					sw2ChangePasswordService.updateByPassword(userS.getId() , passwordNew);

				}else {
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
				}

			 }
			 // **↑↑↑ 2020/08/21 iDEA山下 SW2ユーザ認証追加 ↑↑↑**///

			return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}


	}
}
