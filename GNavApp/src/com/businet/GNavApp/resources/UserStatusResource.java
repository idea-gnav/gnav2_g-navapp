package com.businet.GNavApp.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machineSerchInfo.IMachineSearchInfoService;
import com.businet.GNavApp.ejbs.user.ISw2UserService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.ejbs.userStatus.ISw2UserStatusService;
import com.businet.GNavApp.ejbs.userStatus.IUserStatusService;
import com.businet.GNavApp.entities.MstUser;
import com.businet.GNavApp.entities.Sw2MstUser;
import com.businet.GNavApp.entities.Sw2TblGnavUserSetting;
import com.businet.GNavApp.entities.TblUserDevice;
import com.businet.GNavApp.entities.TblUserSetting;
import com.businet.GNavApp.http.HttpServices;
import com.businet.GNavApp.http.HttpServices.RequestType;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.UserStatusModel;
import com.businet.GNavApp.util.PropertyUtil;



@Path("/userStatus")
@Stateless
public class UserStatusResource {

	private static final Logger logger = Logger.getLogger(UserStatusResource.class.getName());

	@EJB
	IUserStatusService userStatusService;

	@EJB
	ISw2UserStatusService sw2UserStatusService;

	@EJB
	IUserService userService;

	@EJB
	ISw2UserService sw2UserService;

	@EJB
	IMachineSearchInfoService machineSearchInfoService;


	/**
	 * [API No.1] ユーザー認証
	 * @param userId
	 * @param password
	 * @param deviceToken
	 * @param appVersion
	 * @param deviceType
	 * @param mode
	 * @return Response.JSON　UserStatusModel
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("password") String password,
				@FormParam("deviceToken") String deviceToken,
				@FormParam("appVersion") String appVersion,
				@FormParam("deviceType") Integer deviceType,
				@FormParam("deviceLanguage") Integer deviceLanguage,
				@FormParam("mode") Integer mode,
				@FormParam("systemFlg") Integer systemFlg
			) throws Exception{


		logger.info("[GNAV][POST] userId="+userId+", password="+password+", deviceToken="+deviceToken+", deviceLanguage="+deviceLanguage
				+", appVersion="+appVersion+", deviceType="+deviceType+", mode="+mode+", systemFlg="+systemFlg);

		try {

			// ユーザー認証管理 ログイン日時取得
			String loginDtm = ConvertUtil.getTimestampUTC(0);


			// 1) アプリケーションバージョンチェック
			//    0:ログイン時 1:アクティブ時 2:起動時
			if(mode == 0 || mode == 1 || mode == 2) {
				String minVertion = null;

				minVertion = PropertyUtil.readProperties(Constants.APP_VERTION_PATH, "applicationVersion", null);
				if(minVertion==null)
					minVertion = "99999";
				logger.config("[GNAV][DEBUG] appVersion="+appVersion+", minVertion="+minVertion);

				if(appVersion.compareTo(minVertion) < 0) {

					return Response.ok(new ReturnContainer(Constants.CON_WARNING_APP_VERSION)).build();
				}else {
					if(mode==2)
						// 2:起動時はバージョンチェックのみで終了
						return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
				}
			}


			// **↓↓↓ 2020/08/06 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//

			// 2)ログイン処理対象システム判定
			if(systemFlg == null) {
				int gnavFlg = 0;
				int sw2Flg = 0;

				//G@Navユーザ存在チェック
				MstUser userG = userService.findByUser(userId, password);
				if (userG != null)
					gnavFlg = 1;

				try {
					//SW2ユーザ存在チェック
					Sw2MstUser userS = sw2UserService.findBySw2User(userId, password);
					if (userS != null)
						sw2Flg = 1;
				}catch(Exception e) {
					//SW2 DB接続不可の場合、ユーザ存在なしとする。
					logger.log(Level.WARNING, "SW2 Database Connection Error.");
					sw2Flg = 0;
				}


				//ユーザ重複チェック
				if (gnavFlg == 1 && sw2Flg == 1) {
					// 両DBに存在するID,PASSの場合ログイン対象システムのパラメータを貰う
					logger.config("[GNAV][DEBUG] WARNING_USER_DUPLICATION 1701");
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_USER_DUPLICATION)).build();
				}else if(gnavFlg == 1 && sw2Flg == 0) {
					//ログイン処理:G@Nav
					systemFlg = 0;
				}else if(gnavFlg == 0 && sw2Flg == 1) {
					//ログイン処理:SW2
					systemFlg = 1;
				}else if(gnavFlg == 0 && sw2Flg == 0) {
					//対象ユーザ無し
					logger.config("[GNAV][DEBUG] WARNING NO USER PASSWORD 1201 | USER NULL");
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
				}

			}
			// **↑↑↑ 2020/08/06 iDEA山下 SW2ユーザ認証追加 ↑↑↑**//


			UserStatusModel userStatusModel = new UserStatusModel();

			//SW2用言語CD変換
			String sw2Language = ConvertUtil.convertSw2LanguageCd(deviceLanguage);



			// 3) G@Navユーザー認証
			//    0:ログイン時 1:アクティブ時
			if(systemFlg == 0) {                                 //** 2020/08/06 iDEA山下 SW2ユーザ認証追加 **//

				if(mode == 0 || mode == 1) {

					// ユーザー存在チェック
					MstUser user = userService.findByUser(userId, password);
					if(user != null) {

						userStatusModel = gnavLoginProcess(user,deviceLanguage,deviceToken,loginDtm,deviceType,mode,1);

						if(userStatusModel.getStatusCode() != null) {
							switch (userStatusModel.getStatusCode()){
							 case 1101 :
								 return Response.ok(new ReturnContainer(Constants.CON_WARNING_PASSWORD_EXPIRED)).build();
							 case 1201 :
								 return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
							}
						}
					}else if(user == null){
						logger.config("[GNAV][DEBUG] WARNING NO USER PASSWORD 1201 | USER NULL");
						return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
					}

				}

				userStatusModel.setStatusCode(Constants.CON_OK);


				// **↓↓↓ 2019/11/01 ルート案内ロック用の速度↓↓↓**//
				userStatusModel.setRouteGuidanceLockSpeed(Constants.ROUTE_GUIDANCE_LOCK_SPEED);
				// **↑↑↑ 2019/11/01 ルート案内ロック用の速度↑↑↑**//


				//**↓↓↓ 2020/08/06 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//
				String relationSw2User = null;
				Sw2MstUser sw2User = null;
				try {
					//SW2関連ユーザチェック
					relationSw2User = sw2UserService.findByRelationUser(userId, systemFlg);
					//ユーザマスタ捜査
					sw2User = sw2UserService.findBySw2User(relationSw2User);
				}catch(Exception e) {
					//SW2 DB接続不可の場合、ユーザ存在なしとする。
					logger.log(Level.WARNING, "SW2 Database Connection Error.");
				}

				//関連ユーザがあり、MST_USERに存在する
                if(relationSw2User != null && sw2User != null) {

                	//SW2API認証キー取得
                	//暗号キー作成　ユーザID
                	String aesUserId = sw2UserStatusService.exchangeSecurityKey(relationSw2User, Constants.AES_KEY);
                	//暗号キー作成　言語CD
                	String aesLanguage = sw2UserStatusService.exchangeSecurityKey(sw2Language, Constants.AES_KEY);

                	//SW2認証キー取得(IDのみ)処理　※SW2本体API使用
					String getTokentUrl = Constants.SW2_HOST_URL + "/api-token-auth/app_auth";
					String params = "user_id=" + aesUserId +  "&language=" + aesLanguage;
					String rs = HttpServices.sendHttpRequest(getTokentUrl, RequestType.POST, params, null);

					if(rs != null && !rs.equals("Exeption")) {

						JSONObject jObj = new JSONObject(rs);

						String token = null;
						if(jObj != null) {
							if(jObj.has("token")) {
								token = (String) jObj.get("token");
							}
						}

						//Token復元
						token = sw2UserStatusService.restoreSecurityKey(token, Constants.AES_KEY);

						userStatusModel.setSw2Token(token);

					}


                	//Token取得出来た場合、SW2系パラメータ返却する
                	if(userStatusModel.getSw2Token() != null) {

                		userStatusModel.setSw2UserNumber(sw2User.getId());
                		userStatusModel.setSw2UserId(relationSw2User);

    					//設定取得or設定登録
    					Sw2TblGnavUserSetting appUserSetting = checkTblAppUserSetting(relationSw2User);
    					if(appUserSetting != null) {
    						userStatusModel.setSw2AppLogFlg(appUserSetting.getApplogFlg());
    					}

    					//SW2メニューフラグの取得
    					userStatusModel.setSw2ReportFlg(1);
    					userStatusModel.setSw2SurveyStatisticsFlg(Constants.SURVEY_MENU_FLG); //アンケート統計非公開

                	}



				}
                //**↑↑↑ 2020/08/06 iDEA山下 SW2ユーザ認証追加 ↑↑↑**//




            // 4) Sw2ユーザー認証
    		//    0:ログイン時 1:アクティブ時
			}else if(systemFlg == 1) {

				if(mode == 0 || mode == 1) {

				    //SW2認証キー取得処理　※SW2本体API使用
					String getTokentUrl = Constants.SW2_HOST_URL + "/api-token-auth/";

					String params = "user_account=" + userId + "," + sw2Language + "&password=" + password + "&language=" + sw2Language;
					String rs = HttpServices.sendHttpRequest(getTokentUrl, RequestType.POST, params, null);

					if(rs != null) {

						JSONObject jObj = new JSONObject(rs);

						String token = null;
						if(jObj != null) {
							if(jObj.has("token")) {
								token = (String) jObj.get("token");
							}
						}

						// SW2パスワード更新チェック
						String checkChangePassURL = Constants.SW2_HOST_URL + "/c/login/check_change_password/";
						String rs2 = HttpServices.sendHttpRequest(checkChangePassURL, RequestType.GET, null,"JWT " + token);
						if(rs2 != null) {

							JSONObject jObj2 = new JSONObject(rs2);
							Boolean limitClear = null;
							if(jObj2 != null) {
								if(jObj2.has("result")) {
									limitClear = (Boolean) jObj2.get("result");
								}
							}

							//パスワード変更要
							if(!limitClear){
								logger.config("[GNAV][DEBUG] WARNING_PASSWORD_EXPIRED 1101");
				        		return Response.ok(new ReturnContainer(Constants.CON_WARNING_PASSWORD_EXPIRED)).build();
							}else {
								//SW2メニュー使用可

								//ユーザマスタ捜査
								Sw2MstUser sw2User = sw2UserService.findBySw2User(userId);

								userStatusModel.setSw2Token("JWT " + token);
								userStatusModel.setSw2UserNumber(sw2User.getId());
								userStatusModel.setSw2UserId(userId);

								//SW2メニューフラグ
								userStatusModel.setSw2ReportFlg(1);
								userStatusModel.setSw2SurveyStatisticsFlg(Constants.SURVEY_MENU_FLG);  //アンケート統計非公開

								//設定取得or設定登録
								Sw2TblGnavUserSetting appUserSetting = checkTblAppUserSetting(userId);
								if(appUserSetting != null) {
									userStatusModel.setSw2AppLogFlg(appUserSetting.getApplogFlg());
								}

							}

						}else {
							return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
						}

						//G@Nav関連ユーザチェック
						String relationGnavUser = sw2UserService.findByRelationUser(userId, systemFlg);

						//関連ユーザがある場合
		                if(relationGnavUser != null) {
		                	if(mode == 0 || mode == 1) {
		    					// ユーザー存在チェック
		    					MstUser user = userService.findByUser(relationGnavUser);
		    					if(user != null) {

		    						UserStatusModel userStatusGnav = new UserStatusModel();
		    						userStatusGnav = gnavLoginProcess(user,deviceLanguage,deviceToken,loginDtm,deviceType,mode,0);

		    						if(userStatusGnav.getStatusCode() != null) {
		    							switch (userStatusModel.getStatusCode()){
		    							 case 1101 :
		    								 return Response.ok(new ReturnContainer(Constants.CON_WARNING_PASSWORD_EXPIRED)).build();
		    							 case 1201 :
		    								 return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
		    							}
		    						}else {
		    							userStatusModel.setGnavUserId(userStatusGnav.getGnavUserId());
		    							userStatusModel.setReturnMachineFlg(userStatusGnav.getReturnMachineFlg());
		    							userStatusModel.setSurveyFlg(userStatusGnav.getSurveyFlg());
		    							userStatusModel.setConstructionFlg(userStatusGnav.getConstructionFlg());
		    							userStatusModel.setUnitSelect(userStatusGnav.getUnitSelect());
		    							userStatusModel.setKibanSelect(userStatusGnav.getKibanSelect());
		    							userStatusModel.setPdfFlg(userStatusGnav.getPdfFlg());
		    							userStatusModel.setAppLogFlg(userStatusGnav.getAppLogFlg());
		    							userStatusModel.setSearchRangeDistance(userStatusGnav.getSearchRangeDistance());
		    							userStatusModel.setCustomerNamePriority(userStatusGnav.getCustomerNamePriority());
		    							userStatusModel.setMachineIconList(userStatusGnav.getMachineIconList());
		    							userStatusModel.setAlertLevelIconList(userStatusGnav.getAlertLevelIconList());
		    						}

		    					}else if(user == null){
		    						logger.config("[GNAV][DEBUG] WARNING NO USER PASSWORD 1201 | USER NULL");
		    						return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
		    					}

		    				}

		    				userStatusModel.setRouteGuidanceLockSpeed(Constants.ROUTE_GUIDANCE_LOCK_SPEED);

		                }
						userStatusModel.setStatusCode(Constants.CON_OK);

					}else {
						logger.config("[GNAV][DEBUG] WARNING NO USER PASSWORD 1201 | USER NULL");
						return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
					}

			    }



			}

			return Response.ok(userStatusModel).build();

		}catch(Exception e) {
			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}



		/**
		 * パスワード有効期限日数
		 * @param newPasswordDate
		 * @return days
		 */
		public Integer checkPasswordDays(Timestamp newPasswordDate) {

			Date datNow = new Date(System.currentTimeMillis());
			Date datLastPass = new Date(newPasswordDate.getTime());

			long diff = datNow.getTime() - datLastPass.getTime();
			long days = diff / (1000 * 60 * 60 * 24);
			logger.config("[GNAV][DEBUG] Password days="+days);
			/*
			 * パスワード変更日の3日前　days==57
			 * パスワード変更日の2日前　days==58
			 * パスワード変更日の1日前　days==59
			 * パスワード変更日期限切れ　days>=60
			 */
			return (int)days;
		}



	/**
	 * ユーザー設定テーブルチェック
	 * 存在しない場合、PDFフラグをセットし、ユーザー設定テーブルに登録する。
	 * @param MstUser ユーザーマスタ
	 * @return TblUserSetting
	 */
	public TblUserSetting checkTblUserSetting(MstUser user, Integer ProcessTypeFlg) {

		int pdfFlg = 0;

		TblUserSetting userSetting = userStatusService.findByUserSetting(user.getUserId());

		// 設定テーブルに登録が無い場合
		if(userSetting == null) {
			if(ProcessTypeFlg!=null) {
				if( ProcessTypeFlg == 0 	// SCM
					|| ProcessTypeFlg==1 	// 地区
					|| ProcessTypeFlg==2 	// 代理店
					|| ProcessTypeFlg==3	// 一般、レンタル （DTC PDF不可）
					|| ProcessTypeFlg==4	// サービス工場
					|| ProcessTypeFlg==21	// SCMSEA
					|| ProcessTypeFlg==31	// トルコ代理店
					|| ProcessTypeFlg==35	// トルコサブ代理店
						) {
					if(ProcessTypeFlg==3)
						pdfFlg = 0;
					else
						pdfFlg = 1;
				}
				/*
				 * ユーザー設定テーブルに新規登録
				 * デフォルト
				 * 	機番   0:SCM機番    user.getuserKbn()
				 * 	単位   1:ISO
				 */
				userStatusService.insertByUserSetting(user.getUserId(), 1, 0, pdfFlg);

			// 処理タイプが存在しない場合はは何もせずnullで返却する。
			}else {
				return null;
			}

		}else {
			/*
			 * 機番区分が1だった場合の考慮
			 * 過去登録分が機番区分1になってる場合もあるため。
			 */
			if(userSetting.getKibanSelect()==1)
				userStatusService.updateByUserSetting(user.getUserId(), 0);
		}

		// ユーザー設定テーブルを返却する
		return userStatusService.findByUserSetting(user.getUserId());
	}

	/**
	 * SW2ユーザー設定テーブルチェック
	 * 存在しない場合SW2ユーザー設定テーブルに登録する。
	 * @param MstUser ユーザーマスタ
	 * @return TblUserSetting
	 */
	public Sw2TblGnavUserSetting checkTblAppUserSetting(String user) {

		int pdfFlg = 0;

		Sw2TblGnavUserSetting sw2UserSetting = sw2UserStatusService.findBySw2UserSetting(user);

		// 設定テーブルに登録が無い場合
		if(sw2UserSetting == null) {
			/*
			 * ユーザー設定テーブルに新規登録
			 * デフォルト
			 * 	ログ送信フラグ   0:表示無し
			 */
			sw2UserStatusService.insertBySw2UserSetting(user);

		}

		// ユーザー設定テーブルを返却する
		return sw2UserStatusService.findBySw2UserSetting(user);
	}



	/**
	 * G@Navユーザーログイン処理
	 * @param MstUser
	 * @param String
	 * @param Integer
	 * @return UserStatusModel
	 * @throws Exception
	 */
	public UserStatusModel gnavLoginProcess(MstUser user, Integer deviceLanguage, String deviceToken, String loginDtm, Integer deviceType,Integer mode , Integer mainFlg) throws Exception {

		UserStatusModel userStatusModel = new UserStatusModel();

		userStatusModel.setGnavUserId(user.getUserId());

        if(mainFlg == 1) {
        	// 1) パスワード有効期限切れチェック
        	Timestamp newPasswordDate = user.getNewPasswordDate();
        	if(newPasswordDate==null){
        		// NULLの場合、初回パスワード変更通知
        		logger.config("[GNAV][DEBUG] WARNING_PASSWORD_EXPIRED 1101");
        		userStatusModel.setStatusCode(Constants.CON_WARNING_PASSWORD_EXPIRED);
        		return userStatusModel;
        	}
        	if(newPasswordDate!=null){
        		Integer days = checkPasswordDays(newPasswordDate);
        		// 60日以上（2か月経過）でパスワード変更通知
        		if(days>=60) {
        			logger.config("[GNAV][DEBUG] WARNING_PASSWORD_EXPIRED 1101");
        			userStatusModel.setStatusCode(Constants.CON_WARNING_PASSWORD_EXPIRED);
        			return userStatusModel;
        		}
        	}
        }

			// 2) ユーザー組織権限取得
			Integer processTypeFlg = null;
			List<Object[]> userSosikiKengen = userService.findByUserSosikiKengenNativeQuery(user.getUserId());
			if(userSosikiKengen.get(0)[5]!=null)
				processTypeFlg = Integer.parseInt(userSosikiKengen.get(0)[5].toString());

			// メニュー表示制御

			if(processTypeFlg!=null) {
				// **↓↓↓ 2019/07/29 iDEA山下 7月リリース機能小関さん猪熊さんユーザのみ有効化(一時的)↓↓↓**//
				String rmTargetUser = PropertyUtil.readProperties(Constants.APP_CONFIG_PATH, Constants.RM_TARGET_USER_LIST, null);
				String csTargetUser = PropertyUtil.readProperties(Constants.APP_CONFIG_PATH, Constants.CONSTRUCTION_TARGET_USER_LIST, null);
/*
				if(user.getUserId().matches(rmTargetUser)){
					userStatusModel.setReturnMachineFlg(1);
					userStatusModel.setSurveyFlg(1);
					userStatusModel.setConstructionFlg(1);
				}else if(processTypeFlg==0 || processTypeFlg==1) {
					//userStatusModel.setReturnMachineFlg(1);
					userStatusModel.setReturnMachineFlg(0);
					userStatusModel.setSurveyFlg(1);
					userStatusModel.setConstructionFlg(1);
				}else {
					userStatusModel.setReturnMachineFlg(0);
					userStatusModel.setSurveyFlg(0);
					userStatusModel.setConstructionFlg(0);
				}
*/

//下記本番公開設定

				//リターンマシンメニュー設定
				if(user.getUserId().equals("07J959")                          //小関さん
					|| user.getUserId().equals("11C953")                      //猪熊さん
					|| userSosikiKengen.get(0)[1].toString().equals("D42P1F") //北陸支店(SOSIKI_CD='D42P1F')
					|| userSosikiKengen.get(0)[1].toString().equals("D42P0U") //埼玉支店(SOSIKI_CD='D42P0U')
					|| user.getUserId().matches(rmTargetUser)                 //ユーザ個別公開設定
				) {
					userStatusModel.setReturnMachineFlg(1);
				}else {
					userStatusModel.setReturnMachineFlg(0);
				}

				//アンケートメニュー設定
				userStatusModel.setSurveyFlg(0);

				//舗装情報メニュー設定
				if(user.getUserId().equals("07J959")          //小関さん
					|| user.getUserId().equals("11C953")      //猪熊さん
					|| user.getUserId().matches(csTargetUser) //ユーザ個別公開設定
				) {
					userStatusModel.setConstructionFlg(1);
				}else {
					userStatusModel.setConstructionFlg(0);
				}

		}


			// 4) ユーザー設定テーブルの存在チェック
			//    なければ新規登録。ユーザー設定テーブル情報を取得。
			TblUserSetting userSetting = checkTblUserSetting(user, processTypeFlg);
			if(userSetting!=null) {
//				userStatusModel.setUnitSelect(userSetting.getUnitSelect());
				userStatusModel.setUnitSelect(1);	// ISO only
				userStatusModel.setKibanSelect(userSetting.getKibanSelect());
				userStatusModel.setPdfFlg(userSetting.getPdfFlg());
				userStatusModel.setAppLogFlg(userSetting.getApplogFlg());
				userStatusModel.setSearchRangeDistance(userSetting.getSearchRange());
				// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
				userStatusModel.setCustomerNamePriority(userSetting.getCustomerNamePriority());
				// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//
			}else {
				logger.config("[GNAV][DEBUG] WARNING NO USER PASSWORD 1201 | USER SETTING NULL");
				userStatusModel.setStatusCode(Constants.CON_WARNING_NO_USER_PASSWORD);
				return userStatusModel;
			}


			// 5) デバイストークンチェック
			//    languageCd  0:en 英語  3:ja 日本語  4:id インドネシア語  5:tr トルコ語
			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);
			if(deviceToken!=null && deviceToken.length()>0) {
				TblUserDevice userDevice = userStatusService.findByDevice(deviceToken, null);
				if(userDevice != null)
					userStatusService.updateByUserDevice(deviceToken, user.getUserId(), languageCd, loginDtm);
				else
					userStatusService.insertByUserDevice(deviceToken, deviceType, user.getUserId(), languageCd, Constants.CON_APP_FLG, loginDtm);
			}


			// 6) 機械検索情報取得(0:ログイン時のみ)
			if(mode==0) {

				// 機械アイコン画像
				HashMap<String, Object> hashMapIcon = new HashMap<String, Object>();
				ArrayList<HashMap<String, Object>> hashMapIconList = new ArrayList<>();

				// **↓↓↓ 2018/11/02 iDEA山下  機械アイコン区分一覧　外部ファイル参照化のため修正 ↓↓↓**//
//				List<Integer> iconTypeList = machineSearchInfoService.findByMachineIconList();

				String[] iconType = PropertyUtil.readProperties(Constants.APP_CONFIG_PATH, Constants.TYPE_ICON_LIST, null).split(",");

				for(int index = 0; index < iconType.length; index++) {
					// 外部ファイルから画像のパスを取得
					String iconPath = PropertyUtil.readProperties(Constants.APP_CONFIG_PATH, Constants.TYPE_ICON+iconType[index], null);
					logger.config("[GNAV][DEBUG] iconTypeList.get("+index+")="+iconType[index]+", iconPath="+iconPath);

					// バイナリ変換
					BufferedImage bufferedImage = ImageIO.read(new File(iconPath));
					ByteArrayOutputStream outPutStream = new ByteArrayOutputStream();
					ImageIO.write(bufferedImage, "png", outPutStream);
					byte[] binary = outPutStream.toByteArray();
					Object iconImageBinary = binary;

					hashMapIcon.put("iconType", iconType[index]);
					hashMapIcon.put("iconImage", iconImageBinary);
					hashMapIconList.add(hashMapIcon);
					hashMapIcon = new HashMap<String, Object>();
				}

				userStatusModel.setMachineIconList(hashMapIconList);
				// **↑↑↑ 2018/11/02 iDEA山下  機械アイコン区分一覧　外部ファイル参照化のため修正 ↑↑↑**//


				// **↓↓↓ 2020/02/05 RASIS DucNKT 発生中警報レベルアイコン区分一覧 外部ファイル参照化のため修正 ↓↓↓**//
				HashMap<String, Object> hashMapAlertLevelIcon = new HashMap<String, Object>();
				ArrayList<HashMap<String, Object>> hashMapAlertLevelIconList = new ArrayList<>();

				String[] alertLevelList = PropertyUtil
						.readProperties(Constants.APP_CONFIG_PATH, Constants.ALERT_LEVEL_ICON_LIST, null)
						.split(",");

				for (int index = 0; index < alertLevelList.length; index++) {
					// 外部ファイルから画像のパスを取得
					String alertIconPath = PropertyUtil.readProperties(Constants.APP_CONFIG_PATH,
							Constants.ALERT_ICON + alertLevelList[index], null);
					logger.config("[GNAV][DEBUG] alertLevelIconList.get(" + index + ")=" + alertLevelList[index]
							+ ", iconPath=" + alertIconPath);

					// バイナリ変換
					BufferedImage bufferedImage = ImageIO.read(new File(alertIconPath));
					ByteArrayOutputStream outPutStream = new ByteArrayOutputStream();
					ImageIO.write(bufferedImage, "png", outPutStream);
					byte[] binary = outPutStream.toByteArray();
					Object iconImageBinary = binary;

					hashMapAlertLevelIcon.put("alertLevel", alertLevelList[index]);
					hashMapAlertLevelIcon.put("alertImage", iconImageBinary);
					hashMapAlertLevelIconList.add(hashMapAlertLevelIcon);
					hashMapAlertLevelIcon = new HashMap<String, Object>();
				}

				userStatusModel.setAlertLevelIconList(hashMapAlertLevelIconList);


				// **↑↑↑ 2020/02/05 RASIS DucNKT 発生中警報レベルアイコン区分一覧 外部ファイル参照化のため修正 ↑↑↑**//

			}

			//ログイン履歴インサート
			userStatusService.insertByTblUserStatusAppLog(user.getUserId(), deviceType, ConvertUtil.convertLanguageCd(deviceLanguage),mode, loginDtm);

		return userStatusModel;
	}












}
