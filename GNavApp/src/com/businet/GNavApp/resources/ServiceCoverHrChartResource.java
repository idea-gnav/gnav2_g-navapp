package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ServiceCoverHrChartArraylistModel;
import com.businet.GNavApp.models.ServiceCoverHrChartModel;


@Path("/serviceCoverHrChart")
@Stateless
public class ServiceCoverHrChartResource {

	private static final Logger logger = Logger.getLogger(ServiceCoverHrChartResource.class.getName());


	@EJB
	ISurveyStatisticsService surveyStatisticsService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IUserService userService;


	/**
	 * [API No.37] サービスカバーHR分布図取得
	 * @param userId
	 * @param surveyId
	 * @param year
	 * @param deviceLanguage
	 * @return Response.JSON serviceCoverHrChartModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("surveyId") Integer surveyId,
				@FormParam("year") String year,
				@FormParam("deviceLanguage") Integer deviceLanguage
			){

		logger.info("[GNAV][POST] userId="+userId+", surveyId="+surveyId+", year="+year+", deviceLanguage="+deviceLanguage);

		ServiceCoverHrChartModel serviceCoverHrChartModel = new ServiceCoverHrChartModel();
		ArrayList<ServiceCoverHrChartArraylistModel> servicecoverhrchartArraylist = new ArrayList<ServiceCoverHrChartArraylistModel>();

		try {

			// ユーザー組織権限取得
						List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

						String sosikiCd = null;
						if(user.get(0)[1]!=null)
							sosikiCd = (String)user.get(0)[1];

						String kigyouCd = null;
						if(user.get(0)[2]!=null)
							kigyouCd = (String)user.get(0)[2];

						String groupNo = null;
						if(user.get(0)[3]!=null)
							groupNo = (String)user.get(0)[3];

						Integer typeFlg = null;
						if(user.get(0)[5]!=null)
							typeFlg = ((Number)user.get(0)[5]).intValue();
						else
							return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

			//サービスカバーHR分布図取得
			//List<Object[]> serviceCoverHrChart = surveyStatisticsService.findServiceCoverHrChart(surveyId ,year);
			List<Object[]> serviceCoverHrChart = surveyStatisticsService.findServiceCoverHrChart(surveyId ,year,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			if(serviceCoverHrChart!=null) {

				Integer maxRecord = serviceCoverHrChart.size();

				for(int counter = 0; counter < maxRecord ; counter++ ) {

					ServiceCoverHrChartArraylistModel serviceCoverHrChartArraylistModel = new ServiceCoverHrChartArraylistModel();

					Object[] result = serviceCoverHrChart.get(counter);

					serviceCoverHrChartArraylistModel.setSummaryName(((Number)result[0]).intValue()); //グラフ横軸名
					serviceCoverHrChartArraylistModel.setSummaryCount(((Number)result[1]).intValue()); //集計件数

					//抽出対象件数0件でも1行作成されるので、作成行が1行でサマリ件数が0のレコードは除外する
					if(maxRecord != 1) {
						servicecoverhrchartArraylist.add(serviceCoverHrChartArraylistModel);
					}else if(((Number)result[1]).intValue() != 0) {
						servicecoverhrchartArraylist.add(serviceCoverHrChartArraylistModel);
					}
				}

				serviceCoverHrChartModel.setServiceCoverHrChart(servicecoverhrchartArraylist);

			}

			serviceCoverHrChartModel.setStatusCode(Constants.CON_OK);
			serviceCoverHrChartModel.setSurveyId(surveyId);

			//ヘッダー情報取得
		//	List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,null,null ,null ,null);
			List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,null,null ,null ,null,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Object[] rs = header.get(0);
			serviceCoverHrChartModel.setSurveyName((String)rs[0]); //アンケート名
			serviceCoverHrChartModel.setTargetFigures(((Number)rs[1]).intValue()); //回答目標数
			serviceCoverHrChartModel.setAchievementFigures(((Number)rs[2]).intValue()); //回答数

			return Response.ok(serviceCoverHrChartModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}


	}
}
