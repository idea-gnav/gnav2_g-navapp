package com.businet.GNavApp.resources;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.returnMachine.IReturnMachineService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ReturnMachineListModel;
import com.businet.GNavApp.models.ReturnMachineTargetListModel;
import com.businet.GNavApp.util.Commons;

@Path("/returnMachineTargetList")
@Stateless
public class ReturnMachineTargetListResource {

	private static final Logger logger = Logger.getLogger(ReturnMachineTargetListResource.class.getName());

	@EJB
	IReturnMachineService returnMachineService;

	@EJB
	IMachineService machineService;

	@EJB
	IUserService userService;

	@EJB
	IDtcDitailService dtcDitailService;

	/**
	 * [API No.27] ^[}VÎÛêæ¾
	 *
	 * @param userId         - [U[hc
	 * @param kibanSelect    - @ÔIðæª
	 * @param sortFlg        - ÀÑÖ¦ð
	 * @param startRecord    - R[hJnÊu
	 * @param deviceLanguage - gp¾ê
	 * @return Response.JSON returnMachineAreaDetail
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId, @FormParam("kibanSelect") Integer kibanSelect,
			@FormParam("sortFlg") Integer sortFlg, @FormParam("startRecord") Integer startRecord,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", sortFlg=" + sortFlg + ", startRecord=" + startRecord
				+ ", deviceLanguage=" + deviceLanguage);

		ReturnMachineTargetListModel returnMachineTargetList = new ReturnMachineTargetListModel();

		try {

			// [U[gD Àæ¾
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);
			String kengenCd = null;
			if(user.get(0)[4]!=null)
				kengenCd = user.get(0)[4].toString();


			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

			List<Object[]> targetMachines = returnMachineService.findMachineTargetByUserID(userId, kibanSelect,
					sortFlg);

			ArrayList<ReturnMachineListModel> machineList = new ArrayList<ReturnMachineListModel>();

			int machineCount = 0;
			if (targetMachines != null) {
				machineCount = targetMachines.size();
				// **««« 2019/10/23 iDEARº 1000ãÀð «««**//
//				if (machineCount >= Constants.CON_MAX_COUNT)
//					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();
				// **ªªª 2019/10/23 iDEARº 1000ãÀð ªªª**//

				Integer record = startRecord - 1;
				for (int counter = record; counter < (record + 50); counter++) {

					ReturnMachineListModel returnMachine = new ReturnMachineListModel();

					if (machineCount == counter)
						break;

					Object[] machine = targetMachines.get(counter);

					Long kibanSerno = 0l;
					if (machine[0] != null)
						kibanSerno = ((Number) machine[0]).longValue();

					String manufacturerSerialNumber = "";
					if (machine[1] != null)
						manufacturerSerialNumber = (String) machine[1];

					String customerManagementNo = "";
					if (machine[3] != null)
						customerManagementNo = (String) machine[3];

					String customerManagementName = "";
					if (machine[4] != null)
						customerManagementName = (String) machine[4];

					String scmModel = "";
					if (machine[5] != null)
						scmModel = (String) machine[5];

					Double ido = null;
					if (machine[8] != null)
						ido = ConvertUtil.parseLatLng(((Number) machine[8]).intValue());

					Double keido = null;
					if (machine[9] != null)
						keido = ConvertUtil.parseLatLng(((Number) machine[9]).intValue());

					double hourMeter = 0;
					if (machine[10] != null)
						hourMeter = ((Number) machine[10]).doubleValue();

					Integer nenryoLv = null;
					if (machine[11] != null)
						nenryoLv = ((Number) machine[11]).intValue();

					Integer uraWaterLv = null;
					if (machine[12] != null)
						uraWaterLv = ((Number) machine[12]).intValue();

					Timestamp latestUtcCommonTimestamp = null;
					String latestUtcCommonString = null;
					if (machine[13] != null) {
						latestUtcCommonTimestamp = (Timestamp) machine[13];
						latestUtcCommonString = ConvertUtil.formatYMD(latestUtcCommonTimestamp);
					}

					String conType = null;
					if (machine[14] != null)
						conType = (String) machine[14];

					Integer machineModel = null;
					if (machine[15] != null)
						machineModel = ((Number) machine[15]).intValue();

					Integer returnMachineFlg = 0;
					if (machine[16] != null)
						returnMachineFlg = ((Number) machine[16]).intValue();

					returnMachine.setSerialNumber(kibanSerno);
					returnMachine.setMachineModelCategory(Commons.identifyKibanType(manufacturerSerialNumber, conType));
					returnMachine.setManufacturerSerialNumber(manufacturerSerialNumber);
					returnMachine.setCustomerManagementNo(customerManagementNo);
					returnMachine.setCustomerManagementName(customerManagementName);
					returnMachine.setScmModel(scmModel);

					String positionStr = machineService.findByMachinePosition(kibanSerno, conType, languageCd);
					if (positionStr != null)
						returnMachine.setLatestLocation(positionStr);

					returnMachine.setIdo(ido);
					returnMachine.setKeido(keido);
					returnMachine.setHourMeter(Math.floor(hourMeter / 60.0 * 10) / 10);
					returnMachine.setLatestUtcCommonDateTime(latestUtcCommonString);
					returnMachine.setFuelLevel(nenryoLv);
					returnMachine.setDefLevel(uraWaterLv);
					returnMachine.setReturnMachineFlg(returnMachineFlg);

					DisplayConditionModel displayCondition = Commons.machineIconType(
							machineService.findByMachineTypeControl(conType, machineModel), manufacturerSerialNumber);
					returnMachine.setIconType(displayCondition.getMachineIconType());

					// **«««@2020/01/30  iDEARº ­¶xñxÇÁ «««**//

					//èú®õ xæ¾
					Integer periodiclevel = 0;
					if(machine[17]!=null)
						periodiclevel = ((Number)machine[17]).intValue();

					//DTC xæ¾
					List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
							kibanSerno, 1, null, null, null, Integer.parseInt(kengenCd.toString()), ConvertUtil.convertLanguageCd(deviceLanguage),1);
					Integer dtcLv = 1;

					if(dtcs != null) {
						Object[] dtc = dtcs.get(0);
						if(dtc[8]!=null)
							dtcLv = Integer.parseInt(dtc[8].toString());
					}

					//xñACRíÊ
					returnMachine.setAlertLevelM(Commons.alertLevel(dtcLv, periodiclevel));

					// **ªªª@2020/01/30  iDEARº ­¶xñxÇÁ ªªª**//

					machineList.add(returnMachine);

				}
				returnMachineTargetList.setMachineList(machineList);
			}

			returnMachineTargetList.setStatusCode(Constants.CON_OK);
			returnMachineTargetList.setMachineCount(machineCount);

			return Response.ok(returnMachineTargetList).build();
		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}
}
