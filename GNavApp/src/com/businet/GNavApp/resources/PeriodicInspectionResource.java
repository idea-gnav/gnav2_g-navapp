package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.PeriodicInspectionListArrayListModel;
import com.businet.GNavApp.models.PeriodicInspectionListModel;
import com.businet.GNavApp.models.ReturnContainer;

@Path("/periodicInspection")
@Stateless
public class PeriodicInspectionResource {

	private static final Logger logger = Logger.getLogger(PeriodicInspectionResource.class.getName());

	@EJB
	ISurveyStatisticsService SurveyStatisticsService;

	@EJB
	IUserService userService;

	/**
	 * [API No.42] 定期検査・整備
	 *
	 * @param userId
	 * @param surveyId
	 * @param year
	 * @param deviceLanguage
	 * @return Response.JSON periodicInspectionListModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId,
			@FormParam("surveyId") Integer surveyId,
			@FormParam("year") String year,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", surveyId=" + surveyId + ", year=" + year + ", deviceLanguage=" + deviceLanguage);

		try {
			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String groupNo = null;
			if(user.get(0)[3]!=null)
				groupNo = (String)user.get(0)[3];

			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

			PeriodicInspectionListModel PeriodicInspectionListModel = new PeriodicInspectionListModel();

			//アンケートヘッダー情報取得
//			List<Object[]> headers  = SurveyStatisticsService.findSurveyTargetFigures(surveyId, deviceLanguage,year,null,null,null,null);
			List<Object[]> headers = SurveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,null,null ,null ,null,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Object[] header = headers.get(0);

			//アンケート名称
			String surveyName = null;
			if(header[0]!=null) {
				surveyName = (String)header[0];
			}
			PeriodicInspectionListModel.setSurveyName(surveyName);

			//アンケート回答目標数
			Integer targetFigures = 0;
			if(header[1]!=null) {
				targetFigures = ((Number)header[1]).intValue();
			}
			PeriodicInspectionListModel.setTargetFigures(targetFigures);

			//アンケート実回答数
			Integer achievementFigures = 0;
			if(header[2]!=null) {
				achievementFigures = ((Number)header[2]).intValue();
			}
			PeriodicInspectionListModel.setAchievementFigures(achievementFigures);


			//設問選択肢 選択総数を取得
		//	List<Integer> choiceCountList  = SurveyStatisticsService.choiceCountSum(year,"56");
			List<Integer> choiceCountList  = SurveyStatisticsService.choiceCountSum(year,"56",sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Integer choiceCountAll = 0;
			if(choiceCountList != null && choiceCountList.size() > 0) {
				choiceCountAll = ((Number)choiceCountList.get(0)).intValue();
			}


			//アンケート統計情報取得
		//	List<Object[]> PeriodicInspectionLists = SurveyStatisticsService.findPeriodicInspection(deviceLanguage,year,choiceCountAll);
			List<Object[]> PeriodicInspectionLists = SurveyStatisticsService.findPeriodicInspection(deviceLanguage,year,choiceCountAll,sosikiCd ,kigyouCd ,groupNo ,typeFlg);

			if(PeriodicInspectionLists != null && PeriodicInspectionLists.size() > 0) {
				ArrayList<PeriodicInspectionListArrayListModel> PeriodicInspectionList  = new ArrayList<PeriodicInspectionListArrayListModel>();
				for (Object[] obj : PeriodicInspectionLists) {

					PeriodicInspectionListArrayListModel periodicInspection = new PeriodicInspectionListArrayListModel();

					String itemNo = null;
					if(obj[0] != null)
						itemNo = (String)obj[0];

					Integer itemId = null;
					if(obj[1] != null)
						itemId = ((Number)obj[1]).intValue();

					String item = null;
					if(obj[2] != null)
						item = (String)obj[2];

					Integer freeInputFlg = null;
					if (obj[3] != null)
						freeInputFlg = ((Number)obj[3]).intValue();

					Integer itemAnswerCount = null;
					if (obj[4] != null)
						itemAnswerCount = ((Number)obj[4]).intValue();

					Double answerPercentage = null;
					if (obj[5] != null)
						answerPercentage = ((Number) obj[5]).doubleValue();


					periodicInspection.setItemNo(itemNo);
					periodicInspection.setItemId(itemId);
					periodicInspection.setItem(item);
					periodicInspection.setFreeInputFlg(freeInputFlg);
					periodicInspection.setItemAnswerCount(itemAnswerCount);
					periodicInspection.setAnswerPercentage(answerPercentage);

					PeriodicInspectionList.add(periodicInspection);
				}
				PeriodicInspectionListModel.setPeriodicInspectionList(PeriodicInspectionList);
			}

			PeriodicInspectionListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(PeriodicInspectionListModel).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}


	}


}
