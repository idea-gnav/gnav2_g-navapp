package com.businet.GNavApp.resources;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.surveyService.ISurveyService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.SurveyAnswerChoiceListModel;
import com.businet.GNavApp.models.SurveyChoiceModel;
import com.businet.GNavApp.models.SurveyDetailModel;
import com.businet.GNavApp.models.SurveyQuestionModel;

@Path("/surveyDetail")
@Stateless
public class SurveyDetailResource {

	private static final Logger logger = Logger.getLogger(SurveyDetailResource.class.getName());

	@EJB
	ISurveyService surveyService;

	@EJB
	IMachineService machineService;

	/**
	 * [API No.32] アンケート詳細取得
	 *
	 * @param userId
	 * @param surveyId
	 * @param serialNumber
	 * @param surveyResultId
	 * @param deviceLanguage
	 * @return Response.JSON SurveyDetailModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId, @FormParam("surveyId") Integer surveyId,
			@FormParam("serialNumber") Integer serialNumber, @FormParam("surveyResultId") Integer surveyResultId,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", surveyId=" + surveyId + ", serialNumber=" + serialNumber
				+ ", surveyResultId=" + surveyResultId + ", deviceLanguage=" + deviceLanguage);

		try {

			Boolean isViewResultMode = (surveyResultId != null);

			SurveyDetailModel surveyDetail = new SurveyDetailModel();
			MstMachine machine = machineService.findByMachine((long)serialNumber);

			// **↓↓↓ 2019/09/18 iDEA Nakata　各アンケートの登録ユーザーのみ再編集できるよう修正 ↓↓↓**//
//			surveyDetail.setEditableFlg(surveyService.surveyEditableCheck(surveyId));
			surveyDetail.setEditableFlg(surveyService.surveyEditableCheck(surveyId,surveyResultId, userId));
			// **↑↑↑ 2019/09/18 iDEA Nakata　各アンケートの登録ユーザーのみ再編集できるよう修正 ↑↑↑**//

			// アンケート結果取得
			if (isViewResultMode) {
				List<Object[]> rs = surveyService.findSurveyResultNativeQueryById(surveyResultId);
				if (rs != null && rs.size() > 0) {
					Object[] obj = rs.get(0);

					if (obj[0] != null)
						surveyDetail.setKigyoName(obj[0].toString());
					if (obj[1] != null)
						surveyDetail.setAnswerUser(obj[1].toString());
					if (obj[2] != null)
						surveyDetail.setPosition(obj[2].toString());
					if (obj[3]!=null) {
						surveyDetail.setSurveyDate(ConvertUtil.formatYMD((Timestamp)obj[3]));
					}if(obj[4] != null) {
						Double hourmeter =  ((Number)obj[4]).doubleValue();
						surveyDetail.setHourMeter(Math.floor(hourmeter/60.0*10)/10);
					}
				}
			}else if(!isViewResultMode) {
				if(machine.getNewHourMeter() != null) {
					String kigyouName = surveyService.findByKigyouName(machine.getSosikiCd());
					surveyDetail.setKigyoName(kigyouName);
					if(machine.getNewHourMeter()!= null) {
					    surveyDetail.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);
					}else {
						surveyDetail.setHourMeter(0.0);
					}
				}
			}

			// 回答選択肢取得
			List<Object[]> rs1 = surveyService.findChoiceNativeQuery(-1, deviceLanguage, null);
			if (rs1 != null && rs1.size() > 0) {
				ArrayList<SurveyChoiceModel> lst = new ArrayList<SurveyChoiceModel>();
				for (Object[] objects : rs1) {
					SurveyChoiceModel choice = new SurveyChoiceModel();
					if (objects[0] != null)
						choice.setChoiceId(((Number) objects[0]).intValue());
					if (objects[1] != null)
						choice.setChoice(objects[1].toString());
					lst.add(choice);
				}
				surveyDetail.setPositionChoiceList(lst);
			}

			// アンケート親取得
			// **↓↓↓ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↓↓↓**//
			/*
			List<Integer> questionIds = surveyService.findQuestionIdOfMstSurveyQuestionBySurveyId(surveyId);

			if (questionIds != null && questionIds.size() > 0) {
				int cnt = questionIds.size();
				for (int i = 0; i < cnt; i++) {
					Integer questionId = ((Number) questionIds.get(i)).intValue();
					if (questionId != null)
						setListQuestion(surveyResultId, deviceLanguage, questionId, i + 1, null, null, questionList);
				}
			}
			*/
			List<Object[]> questionIds = surveyService.findQuestionIdOfMstSurveyQuestionBySurveyId(surveyId);
			ArrayList<SurveyQuestionModel> questionList = new ArrayList<SurveyQuestionModel>();


			if(questionIds != null && questionIds.size() > 0) {
				int cnt = questionIds.size();
				for (int i = 0, j = 0; i < cnt; i++) {
					Integer questionId = ((Number) questionIds.get(i)[0]).intValue();
					Integer questionPattern = ((Number) questionIds.get(i)[1]).intValue();
					if (questionId != null && !questionPattern.equals(11)) {
						setListQuestion(surveyResultId, deviceLanguage, questionId, j + 1, null, null, questionList);
						j++;
					}else if(questionId != null && questionPattern.equals(11)) {
						setListQuestion(surveyResultId, deviceLanguage, questionId, null, null, null, questionList);
					}
				}

			}
			// **↑↑↑ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↑↑↑**//

			surveyDetail.setQuestionList(questionList);
			surveyDetail.setQuestionCount(questionList.size());

			surveyDetail.setStatusCode(Constants.CON_OK);
			return Response.ok(surveyDetail).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}

	static int count = 0;

	private void setListQuestion(Integer surveyResultId, Integer deviceLanguage, Integer questionId, Integer questionNo,
			Integer parentQuetionId, Integer parentChoiceId, ArrayList<SurveyQuestionModel> questionList) {
		count++;
		// 階層をコントロールして４階層以降は取得しない（親⇒子⇒孫まで）
		if (count > 3) {
			count = 0;
			return;
		}
		SurveyQuestionModel question = new SurveyQuestionModel();
		question.setQuestionId(questionId);
		question.setQuestionNo(questionNo);
		question.setParentQuetionId(parentQuetionId);
		question.setParentChoiceId(parentChoiceId);

		//アンケート設問回答取得
		List<Object[]> rs = surveyService.findQuestionAndAnswerBySurveyResultID(surveyResultId, deviceLanguage,
				questionId);

		if (rs == null || rs.size() == 0)
			return;

		Object[] surveyQuestion = rs.get(0);

		if (surveyQuestion[1] != null)
			question.setQuestion(surveyQuestion[1].toString());

		if (surveyQuestion[2] != null)
			question.setQuetionPattern(((Number) surveyQuestion[2]).intValue());

		if (surveyQuestion[3] != null)
			question.setParentFlg(((Number) surveyQuestion[3]).intValue());

		if (surveyQuestion[4] != null)
			question.setFreeInputFlg(((Number) surveyQuestion[4]).intValue());

		if (surveyQuestion[5] != null)
			question.setFreeInputCaption(surveyQuestion[5].toString());

		if (surveyQuestion[6] != null)
			question.setFreeInputChoiceId(((Number) surveyQuestion[6]).intValue());

		if (surveyQuestion[7] != null)
			question.setMustAnswerFlg(((Number) surveyQuestion[7]).intValue());

		if (surveyQuestion[8] != null)
			question.setMultiChoiceMax(((Number) surveyQuestion[8]).intValue());

		if (surveyQuestion[10] != null)
			question.setAnswerFreeinput(surveyQuestion[10].toString());

		if (surveyQuestion[11] != null)
			question.setAnswerImageUrl(Constants.UPLOADED_IMAGE_URL + "SurveyResultImage/" + surveyQuestion[11].toString());

		// (4) 回答選択肢を取得（QUESTION_PATTERNが1 or 2 or 3 or 4の場合のみ）
		Integer[] questionPartternsChoice = { 1, 2, 3, 4 };

		if (Arrays.asList(questionPartternsChoice).contains(question.getQuetionPattern())) {
			List<Object[]> rs3 = surveyService.findChoiceNativeQuery(questionId, deviceLanguage, 0);
			ArrayList<SurveyChoiceModel> choiceList = new ArrayList<SurveyChoiceModel>();

			if (rs3 != null && rs3.size() > 0) {
				for (Object[] obj : rs3) {
					SurveyChoiceModel choice = new SurveyChoiceModel();
					if (obj[0] != null)
						choice.setChoiceId(((Number) obj[0]).intValue());
					if (obj[1] != null)
						choice.setChoice(obj[1].toString());
					choiceList.add(choice);
				}
				question.setChoiceList(choiceList);
			}
		}
		// (5) {アンケート結果ID}が設定されている場合、複数選択回答結果を取得（QUESTION_PATTERNが2 or 4の場合のみ）
		if (surveyResultId != null) {
			ArrayList<SurveyAnswerChoiceListModel> answChoiceList = new ArrayList<SurveyAnswerChoiceListModel>();

			if (question.getQuetionPattern() == 2 || question.getQuetionPattern() == 4) {
				List<Integer> rs4 = surveyService.findAnswerChoiceIdByQuestionIDAndSurveyResultID(surveyResultId,
						questionId);
				if (rs4 != null && rs4.size() > 0) {
					SurveyAnswerChoiceListModel answers = null;
					int cnt = rs4.size();
					for (int j = 0; j < cnt; j++) {
						if(rs4.get(j)!= null) {
							answers = new SurveyAnswerChoiceListModel();
							Integer aswId = ((Number) rs4.get(j)).intValue();
							answers.setAnswerChoiceId(aswId);
							answChoiceList.add(answers);
						}
					}
				}
			} else {
				SurveyAnswerChoiceListModel answers = new SurveyAnswerChoiceListModel();
				if (surveyQuestion[9] != null) {
					answers.setAnswerChoiceId(((Number) surveyQuestion[9]).intValue());
					answChoiceList.add(answers);
				}
			}
			if (answChoiceList.size() > 0)
				question.setAnswerChoiceList(answChoiceList);

		}
		questionList.add(question);

		// 下位設問チェック
		List<Object[]> childs = surveyService.findParentChoiceIdAndChildQuestionIdByParentQuestionId(questionId);
		if (childs != null && childs.size() > 0) {

			for(int roop = 0; childs.size()> roop; roop++) {
				Object[] child = childs.get(roop);
				Integer newQuestionId = null, newParentChoiceId = null;

				if (child[0] != null)
					newQuestionId = ((Number) child[0]).intValue();
				if (child[1] != null)
					newParentChoiceId = ((Number) child[1]).intValue();

				setListQuestion(surveyResultId, deviceLanguage, newQuestionId, questionNo, questionId, newParentChoiceId,questionList);
			}
		}else {
			count = 0;
		}
	}
}
