package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ExpectedServiceListModel;
import com.businet.GNavApp.models.ExpectedServiceModel;
import com.businet.GNavApp.models.ReturnContainer;

@Path("/expectedService")
@Stateless
public class ExpectedServiceResource {



		private static final Logger logger = Logger.getLogger(ExpectedServiceResource.class.getName());

			@EJB
			ISurveyStatisticsService surveyStatisticsService;

			@EJB
			IUserService userService;

			/**
			 * [API No.44] サービスに今後期待すること
			 *
			 * @param userId             -ユーザーID
			 * @param surveyId           -アンケートID
			 * @param year               -年度
			 * @param deviceLanguage     -使用言語
			 * @return Response.JSON ExpectedServiceModel
			 */

			@POST
			@Produces(MediaType.APPLICATION_JSON)
			@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
			public Response postMethod(
					@FormParam("userId") String userId,
					@FormParam("surveyId") Integer surveyId,
					@FormParam("year") String year,
					@FormParam("deviceLanguage") Integer deviceLanguage) {

				logger.info("[GNAV][POST] userId=" + userId + ", surveyId=" + surveyId + ", year=" + year
						+ ", deviceLanguage=" + deviceLanguage);

				try {
					// ユーザー組織権限取得
					List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

					String sosikiCd = null;
					if(user.get(0)[1]!=null)
						sosikiCd = (String)user.get(0)[1];

					String kigyouCd = null;
					if(user.get(0)[2]!=null)
						kigyouCd = (String)user.get(0)[2];

					String groupNo = null;
					if(user.get(0)[3]!=null)
						groupNo = (String)user.get(0)[3];

					Integer typeFlg = null;
					if(user.get(0)[5]!=null)
						typeFlg = ((Number)user.get(0)[5]).intValue();
					else
						return Response.ok(new ReturnContainer(Constants.CON_OK)).build();


					ExpectedServiceModel expectedServiceModel = new ExpectedServiceModel();

					//アンケート名を取得
				//	List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId, deviceLanguage, year,null,null,null,null);
					List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId, deviceLanguage, year,null,null,null,null,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
					Object[] rs = header.get(0);
					expectedServiceModel.setSurveyName((String)rs[0]); //アンケート名
					expectedServiceModel.setTargetFigures(((Number)rs[1]).intValue()); //回答目標数
					expectedServiceModel.setAchievementFigures(((Number)rs[2]).intValue()); //回答数

					//詳細情報を取得する
				//	List<Integer> choiceCountList  = surveyStatisticsService.choiceCountSum(year,"63");
					List<Integer> choiceCountList  = surveyStatisticsService.choiceCountSum(year,"63",sosikiCd ,kigyouCd ,groupNo ,typeFlg);
					Integer count = 0;
					if(choiceCountList != null && choiceCountList.size() > 0) {
						count = ((Number)choiceCountList.get(0)).intValue();
					}

						//	List<Object[]> rs1 = surveyStatisticsService.findByExpectedService(deviceLanguage, count, year);
							List<Object[]> rs1 = surveyStatisticsService.findByExpectedService(deviceLanguage, count, year,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
							ArrayList<ExpectedServiceListModel> list = new ArrayList<ExpectedServiceListModel>();
							for (Object[] objects : rs1) {

								ExpectedServiceListModel dataList = new ExpectedServiceListModel();
								if(objects[0] != null)
									dataList.setChoiceId(((Number) objects[0]).intValue()); //選択肢Id
								if(objects[1] != null)
									dataList.setItemNo(objects[1].toString()); //項目番号
								if(objects[2] != null)
									dataList.setItem(objects[2].toString()); //項目名
								if(objects[3] != null)
									dataList.setChoiceCount(((Number) objects[3]).intValue()); //回答数
								if(objects[4] != null)
									dataList.setAnswerPercentage(((Number) objects[4]).intValue()); //回答率
								list.add(dataList);
								expectedServiceModel.setExpectedServiceList(list);
							}

							expectedServiceModel.setStatusCode(Constants.CON_OK);
					return Response.ok(expectedServiceModel).build();

				}catch(Exception e) {
					logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
					return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

				}
			}
}
