package com.businet.GNavApp.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.ejbs.dtcNotice.IDtcNoticeService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.periodicServiceDitail.IPeriodicServiceDitailService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.PeriodicServiceDetailArrayListModel;
import com.businet.GNavApp.models.PeriodicServiceDetailListModel;
import com.businet.GNavApp.models.ReplacementHistoryArrayListModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Commons;

@Path("/periodicServiceDetailList")
@Stateless
public class PeriodicServiceDetailListResource {

	private static final Logger logger = Logger.getLogger(PeriodicServiceDetailListResource.class.getName());

	@EJB
	IPeriodicServiceDitailService periodicServiceDitailService;

	@EJB
	IUserService userService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IDtcNoticeService dtcService;

	@EJB
	IDtcDitailService dtcDitailService;

	/**
	 * [API No.23] èú®õÚ×êæ¾
	 *
	 * @param userId
	 * @param serialNumber
	 * @return Response.JSON DtcDetailListModel
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId, @FormParam("serialNumber") Long serialNumber,
			@FormParam("deviceLanguage") Integer deviceLanguage) throws Exception {

		logger.info("[RemoteCAREApp][POST] userId=" + userId + ", serialNumber=" + serialNumber);

		try {

			// [U[gD Àæ¾
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);
			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);
     		String kengenCd = null;
			if(user.get(0)[4]!=null)
				kengenCd = user.get(0)[4].toString();

			PeriodicServiceDetailListModel periodicServiceDetailListModel = new PeriodicServiceDetailListModel();

			MstMachine machine = machineService.findByMachine(serialNumber);

			if (machine != null) {
				periodicServiceDetailListModel.setSerialNumber(machine.getKibanSerno());

				periodicServiceDetailListModel
						.setMachineModelCategory(Commons.identifyKibanType(machine.getKiban(), machine.getConType()));

				periodicServiceDetailListModel.setManufacturerSerialNumber(machine.getKiban());

				periodicServiceDetailListModel.setCustomerManagementNo(machine.getUserKanriNo());

				if (machine.getSosikiCd() != null)
					periodicServiceDetailListModel
							.setCustomerManagementName(sosikiService.findByKigyouSosikiName(machine.getSosikiCd()));

				periodicServiceDetailListModel.setScmModel(machine.getModelCd());

				DisplayConditionModel displayCondition = Commons.machineIconType(
						machineService.findByMachineTypeControl(machine.getConType(), machine.getMachineModel()),
						machine.getKiban());
				periodicServiceDetailListModel.setIconType(displayCondition.getMachineIconType());

				// **«««@2020/01/30  iDEARº ­¶xñxÇÁ «««**//
				//èú®õ xæ¾
				Integer periodiclevel = 0;
				Integer yokokuCount = machine.getYokokuCount();
				Integer keyikokuCount = machine.getKeyikokuCount();
				if(keyikokuCount > 0 )
					periodiclevel = 2;
				else if(yokokuCount > 0)
					periodiclevel = 1;


				//DTC xæ¾
				List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
						serialNumber, 1, null, null, null, Integer.parseInt(kengenCd.toString()), ConvertUtil.convertLanguageCd(deviceLanguage),1);
				Integer dtcLv = 1;

				if(dtcs != null) {
					Object[] dtc = dtcs.get(0);
					if(dtc[8]!=null)
						dtcLv = Integer.parseInt(dtc[8].toString());
				}

				//xñACRíÊ
				periodicServiceDetailListModel.setAlertLevelM(Commons.alertLevel(dtcLv, periodiclevel));

				// **ªªª@2020/01/30  iDEARº ­¶xñxÇÁ ªªª**//



				String positionStr = machineService.findByMachinePosition(machine.getKibanSerno(), machine.getConType(),
						ConvertUtil.convertLanguageCd(deviceLanguage));

				if (positionStr != null)
					periodicServiceDetailListModel.setLatestLocation(positionStr);

				if (machine.getNewIdo() != null) {
					periodicServiceDetailListModel.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));
				}
				if (machine.getNewKeido() != null) {
					periodicServiceDetailListModel.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));
				}
				if (machine.getNewHourMeter() != null) {
					periodicServiceDetailListModel.setHourMeter(Math.floor(machine.getNewHourMeter() / 60.0 * 10) / 10);
				}
				if (machine.getRecvTimestamp() != null) {
					periodicServiceDetailListModel
							.setLatestUtcCommonDateTime(ConvertUtil.formatYMD(machine.getRecvTimestamp()));
				}

				periodicServiceDetailListModel.setFuelLevel(machine.getNenryoLv());

				periodicServiceDetailListModel.setDefLevel(machine.getUreaWaterLevel());

			}

			String contype = machine.getConType();
			Integer machineModel = machine.getMachineModel();

			//««« 2019/09/30 iDEARº èú®õiÇÁ :start  «««//
			Integer wheelFlg = machine.getWheelFlg();
			//ªªª 2019/09/30 iDEARº èú®õiÇÁ :end  ªªª//

			int[] BuhinNos = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
			int[] BuhinNos_D0 = new int[] { 0, 1, 2, 3, 4, 6, 8, 9, 10, 11 };
			int[] BuhinNos_NH2_16 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
			int[] BuhinNos_NH2_17 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
			int[] BuhinNos_C2_N_ND_2 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
			int[] BuhinNos_T22 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
			//««« 2019/09/30 iDEARº èú®õiÇÁ :start  «««//
			int[] BuhinNos_N_WHEEL = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 17, 18,19 };
			//ªªª 2019/09/30 iDEARº èú®õiÇÁ :end  ªªª//

			if (contype.equals("D") && machineModel == 0) {
				BuhinNos = BuhinNos_D0;
			} else if (contype.equals("NH") && machineModel == 2) {
				String CTRL_T_BUHIN_NO = machine.getCtrlTBuhinNo();
				if (CTRL_T_BUHIN_NO != null && CTRL_T_BUHIN_NO.compareTo("KHR53861") >= 0) {
					BuhinNos = BuhinNos_NH2_17;
				} else
					BuhinNos = BuhinNos_NH2_16;
			//««« 2019/09/30 iDEARº èú®õiÇÁ :start  «««//
//			} else if ((contype.equals("C2") || contype.equals("N") || contype.equals("ND")) && machineModel == 2) {
//				BuhinNos = BuhinNos_C2_N_ND_2;
//			}
			} else if ((contype.equals("C2") || contype.equals("ND")) && machineModel == 2) {
				BuhinNos = BuhinNos_C2_N_ND_2;
			} else if(contype.equals("N") && machineModel == 2) {
				if(wheelFlg == 0) {
					BuhinNos = BuhinNos_C2_N_ND_2;
				}else if(wheelFlg == 1) {
					BuhinNos = BuhinNos_N_WHEEL;
				}
			//ªªª 2019/09/30 iDEARº èú®õiÇÁ :end  ªªª//
			} else if (contype.equals("T2") && machineModel == 2) {
				BuhinNos = BuhinNos_T22;
			}

			ArrayList<PeriodicServiceDetailArrayListModel> periodicServiceDetailArrayList = new ArrayList<PeriodicServiceDetailArrayListModel>();

				//ið·Ô êæ¾
			 	List<Object[]> serviceRequest = periodicServiceDitailService.findByServiceRequestDitailNativeQuery(
						serialNumber, machine.getConType(), machine.getMachineModel(), BuhinNos,
						ConvertUtil.convertLanguageCd(deviceLanguage),periodicServiceDetailListModel.getHourMeter());

			 	//ið·ð êæ¾
				List<Object[]> replacementHistory = periodicServiceDitailService
						.findByReplacementHistoryNativeQuery(serialNumber, BuhinNos);


				int indexOfHistorys = 0;

				if (serviceRequest != null) {
					int size = serviceRequest.size();

					for (Integer Index = 0; Index < size; Index++) {
						//PêiZbg
						Object[] serviceRequestParts = serviceRequest.get(Index);
						int BuhinNo = ((Number) serviceRequestParts[0]).intValue();
						PeriodicServiceDetailArrayListModel PeriodicServiceDetailArrayListModel = new PeriodicServiceDetailArrayListModel();
						PeriodicServiceDetailArrayListModel.setReplacementPartNo(BuhinNo);

						if (serviceRequestParts[2] != null) {
							PeriodicServiceDetailArrayListModel.setReplacementPartName((String) serviceRequestParts[2]);
						}

						if (serviceRequestParts[3] != null) {
							PeriodicServiceDetailArrayListModel.setReplacementInterval(((Number) serviceRequestParts[3]).intValue());
						}

						if (serviceRequestParts[4] != null) {
							Integer nextReplacementTime = ((Number) serviceRequestParts[4]).intValue();
							PeriodicServiceDetailArrayListModel.setTimeUntilNextReplacementTime(nextReplacementTime);

							double value = ((Number) serviceRequestParts[4]).doubleValue() / 8;
							BigDecimal bd = new BigDecimal(String.valueOf(value));
							PeriodicServiceDetailArrayListModel
								.setTimeUntilNextReplacementDay(((Number) bd.setScale(0, RoundingMode.UP)).intValue());

							if (nextReplacementTime <= 0) {
								PeriodicServiceDetailArrayListModel.setPeriodicServiceNotice(2);
							} else if (nextReplacementTime <= 100) {
								PeriodicServiceDetailArrayListModel.setPeriodicServiceNotice(1);
							} else {
								PeriodicServiceDetailArrayListModel.setPeriodicServiceNotice(0);
							}

							PeriodicServiceDetailArrayListModel.setReplacementScheduleDay(ConvertUtil
									.getDaysUTC((PeriodicServiceDetailArrayListModel.getTimeUntilNextReplacementDay())));

						}

						if (serviceRequestParts[5] != null) {
							PeriodicServiceDetailArrayListModel.setReplacementScheduleTime(((Number) serviceRequestParts[5]).doubleValue());
						}



						//i²ÆÉð·ððZbg
						ArrayList<ReplacementHistoryArrayListModel> replacementHistoryArrayList = new ArrayList<ReplacementHistoryArrayListModel>();

						if (replacementHistory != null) {

							for (; indexOfHistorys < replacementHistory.size(); indexOfHistorys++) {
								ReplacementHistoryArrayListModel ReplacementHistoryArrayListModel = new ReplacementHistoryArrayListModel();
								Object[] replacementHistorys = replacementHistory.get(indexOfHistorys);

								int _buhinNo = ((Number) replacementHistorys[0]).intValue();

								if (BuhinNo != _buhinNo) {
									break;
								}

								if (replacementHistorys[1] != null) {
									ReplacementHistoryArrayListModel
										.setReplacementNo(((Number) replacementHistorys[1]).intValue());
								}

								if (replacementHistorys[2] != null) {
									ReplacementHistoryArrayListModel
										.setReplacementHour(((Number) replacementHistorys[2]).doubleValue());
								}

								if (replacementHistorys[3] != null) {
									ReplacementHistoryArrayListModel.setReplacementDate((String) replacementHistorys[3]);
								}
								replacementHistoryArrayList.add(ReplacementHistoryArrayListModel);
							}
						}
						PeriodicServiceDetailArrayListModel.setReplacementHistoryList(replacementHistoryArrayList);
						periodicServiceDetailArrayList.add(PeriodicServiceDetailArrayListModel);
					}

				}
			if (!periodicServiceDetailArrayList.isEmpty()) {
				periodicServiceDetailListModel.setServiceRequestList(periodicServiceDetailArrayList);
			} else {
				periodicServiceDetailListModel.setServiceRequestList(null);
			}

			periodicServiceDetailListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(periodicServiceDetailListModel).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}

}
