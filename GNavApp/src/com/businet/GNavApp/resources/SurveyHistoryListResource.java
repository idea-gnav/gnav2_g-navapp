package com.businet.GNavApp.resources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.surveyService.ISurveyService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.SurveyHistoryListModel;
import com.businet.GNavApp.models.SurveyResultModel;

@Path("/surveyHistoryList")
@Stateless
public class SurveyHistoryListResource {

	private static final Logger logger = Logger.getLogger(SurveyHistoryListResource.class.getName());

	@EJB
	ISurveyService surveyService;

	/**
	 * [API No.34] アンケート履歴一覧取得
	 *
	 * @param userId
	 * @param sortFlg
	 * @param startRecord
	 * @param serialNumber
	 * @param InputDateFrom
	 * @param InputDateTo
	 * @param deviceLanguage
	 * @return Response.JSON SurveyHistoryListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response machineFavoriteList(@FormParam("userId") String userId, @FormParam("sortFlg") Integer sortFlg,
			@FormParam("startRecord") Integer startRecord, @FormParam("serialNumber") Long serialNumber,
			@FormParam("InputDateFrom") String inputDateFrom, @FormParam("InputDateTo") String inputDateTo,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", sortFlg=" + sortFlg + ", startRecord=" + startRecord
				+ ", serialNumber=" + serialNumber + ", InputDateFrom=" + inputDateFrom + ", InputDateTo=" + inputDateTo
				+ ", deviceLanguage=" + deviceLanguage);

		SurveyHistoryListModel surveyHistoryList = new SurveyHistoryListModel();

		try {
			// アンケート一覧取得
			List<Object[]> surveyResults = surveyService.findSurveyHistoryListByMachine(serialNumber, inputDateFrom,
					inputDateTo, sortFlg, deviceLanguage);
			ArrayList<SurveyResultModel> surveyResultList = new ArrayList<SurveyResultModel>();

			int surveyResultCount = 0;
			if (surveyResults != null && surveyResults.size() > 0) {
				surveyResultCount = surveyResults.size();
				Integer maxCount = Constants.CON_MAX_COUNT;
				if(surveyResultCount >= maxCount )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				Integer record = startRecord-1;
				for(int counter =  record; counter < (record+50) ; counter++ ) {
					if( surveyResultCount == counter)
						break;
					Object[] objects = surveyResults.get(counter);

					SurveyResultModel sr = new SurveyResultModel();
					if (objects[0] != null)
						sr.setSurveyResultId(((Number) objects[0]).intValue());
					if (objects[1] != null)
						sr.setSurveyId(((Number) objects[1]).intValue());
					if (objects[2] != null)
						sr.setSurveyName((String) objects[2]);
					if (objects[3] != null)
						sr.setInputDate(new SimpleDateFormat("yyyy-MM-dd").format((Date) objects[3]));
					if (objects[4] != null)
						sr.setPosition(((String) objects[4]));

					surveyResultList.add(sr);
				}
			}

			surveyHistoryList.setSurveyResultList(surveyResultList);
			surveyHistoryList.setSurveyResultCount(surveyResultCount);
			surveyHistoryList.setStatusCode(Constants.CON_OK);
			return Response.ok(surveyHistoryList).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
