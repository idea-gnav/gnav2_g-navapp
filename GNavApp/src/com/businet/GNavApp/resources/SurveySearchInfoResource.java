package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machineSerchInfo.IMachineSearchInfoService;
import com.businet.GNavApp.ejbs.surveySearchInfo.java.ISurveySearchInfoService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ModelTypeListArraylistModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ScmModelListArraylistModel;
import com.businet.GNavApp.models.SurveySearchInfoModel;




@Path("/surveySearchInfo")
@Stateless
public class SurveySearchInfoResource {

	private static final Logger logger = Logger.getLogger(SurveySearchInfoResource.class.getName());

	@EJB
	ISurveySearchInfoService surveySearchInfoService;        //仮

	@EJB
	IMachineSearchInfoService machineSearchInfoService;

	@EJB
	IUserService userService;

	/**
	 * [API No.46] アンケート統計絞り込み検索情報
	 *
	 * @param userId             -ユーザーID
	 * @param surveyId           -アンケートID
	 * @param deviceLanguage     -使用言語
	 * @return Response.JSON SurveySearchModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId,
					@FormParam("surveyId") Integer surveyId,
					@FormParam("deviceLanguage") Integer deviceLanguage
			) {

		logger.info("[GNav][POST] userId="+userId + ", surveyId="+surveyId+", deviceLanguage="+deviceLanguage);

		SurveySearchInfoModel surveySearchInfoModel = new SurveySearchInfoModel();
		ArrayList<ModelTypeListArraylistModel> modeltypelistArraylistModel = new ArrayList<ModelTypeListArraylistModel>(); //レーダーチャートリスト

		try {
			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String groupNo = null;
			if(user.get(0)[3]!=null)
				groupNo = (String)user.get(0)[3];

			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();


			HashMap<String, String> hashMap = new HashMap<>();
			ArrayList<HashMap<String, String>> hashMapList = new ArrayList<>();

			//代理店一覧を取得する
			List<Object[]> dealerList = surveySearchInfoService.findByDairitenList();
			if(dealerList != null) {
				logger.config("[GNAV][DEBUG] dealerList="+dealerList.size());
				for(int index =0; index < dealerList.size(); index++) {
					hashMap.put("dealerCd", dealerList.get(index)[0].toString());
					hashMap.put("dealerName", dealerList.get(index)[1].toString());
					hashMapList.add(hashMap);
					hashMap = new HashMap<String, String>();
				}
				surveySearchInfoModel.setDealerList(hashMapList);
			}
			hashMapList = new ArrayList<>();

			//型式一覧を取得する
			//型式リスト取得
		//	List<String> modelTypeList = surveySearchInfoService.findByModelTypeList();
			List<String> modelTypeList = surveySearchInfoService.findByModelTypeList(sosikiCd ,kigyouCd ,groupNo ,typeFlg);

			if(modelTypeList!=null) {
				for(int counter = 0; counter < modelTypeList.size(); counter++) {
					ModelTypeListArraylistModel modelTypeListArraylistModel = new ModelTypeListArraylistModel();
					modelTypeListArraylistModel.setModelType(modelTypeList.get(counter)); //型式

					//型式に紐付くモデルリスト取得
					List<String> scmModelList = surveySearchInfoService.findByModelList(modelTypeList.get(counter));
					if(scmModelList != null) {
						ArrayList<ScmModelListArraylistModel> scmModellistArraylistModel = new ArrayList<ScmModelListArraylistModel>();
						for(int counter1 = 0 ; counter1 < scmModelList.size() ; counter1++ ) {
							ScmModelListArraylistModel scmModellistArrayListModel = new ScmModelListArraylistModel();
							scmModellistArrayListModel.setModel(scmModelList.get(counter1)); //SCMモデル

							scmModellistArraylistModel.add(scmModellistArrayListModel);
							modelTypeListArraylistModel.setScmModelList(scmModellistArraylistModel);
						}
						modeltypelistArraylistModel.add(modelTypeListArraylistModel);
					}
				}
				surveySearchInfoModel.setModelTypeList(modeltypelistArraylistModel);
			}

			//作業内容一覧を取得する
			List<Object[]> applivableUsesList = surveySearchInfoService.findByApplivableUsesList(deviceLanguage);
			if(applivableUsesList != null) {
				logger.config("[GNAV][DEBUG] applivableUsesList="+applivableUsesList.size());
				for(int index = 0; index < applivableUsesList.size(); index++) {
					hashMap.put("applivableUses", applivableUsesList.get(index)[1].toString());
					hashMap.put("applivableUsesId", applivableUsesList.get(index)[0].toString());
					hashMapList.add(hashMap);
					hashMap = new HashMap<String,String>();
				}
				surveySearchInfoModel.setApplivableUsesList(hashMapList);
			}hashMapList = new ArrayList<>();

			//年度一覧を取得する
			List<String> yearList = surveySearchInfoService.findByYearList();
			if(yearList != null) {
				logger.config("[GNAV][DEBUG] yearList="+yearList.size());
				surveySearchInfoModel.setYearList(yearList);
			}

			surveySearchInfoModel.setStatusCode(Constants.CON_OK);

			return Response.ok(surveySearchInfoModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(Constants.CON_UNKNOWN_ERROR).build();
		}
	}

}
