package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.models.InspectionPDFModel;
import com.businet.GNavApp.models.ReturnContainer;




@Path("/inspectionPDF")
@Stateless
public class InspectionPDFResource {

	private static final Logger logger = Logger.getLogger(InspectionPDFResource.class.getName());

	@EJB
	IDtcDitailService dtcDitailService;


	/**
	 * [API No.12] A3_PDFæ¾
	 * @param serialNumber
	 * @param dtcEventNo
	 * @return Response.JSON pdfModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("serialNumber") Long _serialNumber,
					@FormParam("dtcEventNo") String _dtcEventNo,
					@FormParam("deviceLanguage") Integer deviceLanguage
				) {

		logger.info("[GNAV][POST] serialNumber="+_serialNumber+", dtcEventNo="+_dtcEventNo+", deviceLanguage="+deviceLanguage);

		try {
			String pdfUrl = null;
			InspectionPDFModel pdfModel = new InspectionPDFModel();

			pdfUrl = dtcDitailService.findByPDF(_serialNumber, _dtcEventNo, ConvertUtil.convertLanguageCd(deviceLanguage));

			if(pdfUrl!=null) {
				logger.config("[GNAV][DEBUG] pdfUrl="+pdfUrl);
				pdfModel.setUrl(Constants.PDF_URL + pdfUrl);
				pdfModel.setStatusCode(Constants.CON_OK);

			}else{
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
			}

			return Response.ok(pdfModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}
}
