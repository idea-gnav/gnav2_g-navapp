package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ChartListArraylistModel;
import com.businet.GNavApp.models.ChoiceListArraylistModel;
import com.businet.GNavApp.models.CompetitorListArraylistModel;
import com.businet.GNavApp.models.EvaluationItemModel;
import com.businet.GNavApp.models.ReturnContainer;


@Path("/evaluationItem")
@Stateless
public class EvaluationItemResource {

	private static final Logger logger = Logger.getLogger(EvaluationItemResource.class.getName());


	@EJB
	ISurveyStatisticsService surveyStatisticsService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IUserService userService;


	/**
	 * [API No.39] 評価項目取得
	 * @param userId
	 * @param surveyId
	 * @param dealerCd
	 * @param katashiki
	 * @param scmModel
	 * @param applicableUsesCd
	 * @param year
	 * @param deviceLanguage
	 * @return Response.JSON EvaluationItemModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("surveyId") Integer surveyId,
				@FormParam("dealerCd") String dealerCd,
				@FormParam("katashiki") String katashiki,
				@FormParam("scmModel") String scmModel,
				@FormParam("applicableUsesCd") String applicableUsesCd,
				@FormParam("year") String year,
				@FormParam("deviceLanguage") Integer deviceLanguage
			){

		logger.info("[GNAV][POST] userId="+userId+", surveyId="+surveyId+", dealerCd="+dealerCd+", katashiki="+katashiki+", scmModel="+scmModel+", applicableUsesCd="+applicableUsesCd+", year="+year+", deviceLanguage="+deviceLanguage);

		EvaluationItemModel evaluationItemModel = new EvaluationItemModel();
		ArrayList<CompetitorListArraylistModel> competitorlistArraylistModel = new ArrayList<CompetitorListArraylistModel>(); //競合リスト
		ArrayList<ChartListArraylistModel> chartlistArraylistModel = new ArrayList<ChartListArraylistModel>(); //レーダーチャートリスト


		try {
			// ユーザー組織権限取得
						List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

						String sosikiCd = null;
						if(user.get(0)[1]!=null)
							sosikiCd = (String)user.get(0)[1];

						String kigyouCd = null;
						if(user.get(0)[2]!=null)
							kigyouCd = (String)user.get(0)[2];

						String groupNo = null;
						if(user.get(0)[3]!=null)
							groupNo = (String)user.get(0)[3];

						Integer typeFlg = null;
						if(user.get(0)[5]!=null)
							typeFlg = ((Number)user.get(0)[5]).intValue();
						else
							return Response.ok(new ReturnContainer(Constants.CON_OK)).build();



			//競合リスト取得
			List<Object[]> competitorList = surveyStatisticsService.findCompetitorList(deviceLanguage);
			if(competitorList!=null) {
				Integer maxRecord = competitorList.size();
				for(int counter = 0; counter < maxRecord ; counter++ ) {
					CompetitorListArraylistModel competitorListArraylistModel = new CompetitorListArraylistModel();
					Object[] rs =  competitorList.get(counter);

					competitorListArraylistModel.setCompetitorId(((Number)rs[0]).intValue()); //選択肢ID
					competitorListArraylistModel.setCompetitorName(((String)rs[1])); //選択肢名

					competitorlistArraylistModel.add(competitorListArraylistModel);
				}
				evaluationItemModel.setCompetitorList(competitorlistArraylistModel);
			}

			//回答選択率リスト取得
			if(katashiki != null) {
				katashiki = ConvertUtil.rpStr(katashiki);
				katashiki = ConvertUtil.queryInStr(katashiki);
			}
			if(scmModel != null) {
				scmModel = ConvertUtil.queryInStr(scmModel);
			}

			//評価レベルテキスト取得
			String evaluationLvText = surveyStatisticsService.findEvaluationLvText(deviceLanguage);
			int index = evaluationLvText.lastIndexOf("(");
			String levelLongText = evaluationLvText.substring(index+1);
			levelLongText = levelLongText.substring(0,levelLongText.length()-1);
			levelLongText = levelLongText.replace(":",".");

			//各回答に分割
			String levelText = levelLongText.replace("、",",");
			levelText = levelText.replace(", ",",");
			levelText = levelText.replace(".","");
			levelText = levelText.replaceAll("[0-9]","");
			String[] strs = levelText.split(",");

		//	List<Object[]> choiceList = surveyStatisticsService.findChoiceList(surveyId , deviceLanguage , year , katashiki, scmModel ,dealerCd ,applicableUsesCd,strs);
			List<Object[]> choiceList = surveyStatisticsService.findChoiceList(surveyId , deviceLanguage , year , katashiki, scmModel ,dealerCd ,applicableUsesCd,strs
					                                             ,sosikiCd ,kigyouCd ,groupNo ,typeFlg);

			//住友データ件数取得
		//	List<Integer> sumitomoCountList = surveyStatisticsService.findSumitomoCount(surveyId , year , katashiki, scmModel ,dealerCd ,applicableUsesCd);
			List<Integer> sumitomoCountList = surveyStatisticsService.findSumitomoCount(surveyId , year , katashiki, scmModel ,dealerCd ,applicableUsesCd
					                                             ,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Integer sumitomoCount = 0;
			sumitomoCount = ((Number)sumitomoCountList.get(0)).intValue();

			//レーダーチャート作成用リスト取得
		//	List<Object[]> chartList = surveyStatisticsService.findChartList(surveyId , deviceLanguage , year , katashiki, scmModel ,dealerCd ,applicableUsesCd ,sumitomoCount);
			List<Object[]> chartList = surveyStatisticsService.findChartList(surveyId , deviceLanguage , year , katashiki, scmModel ,dealerCd ,applicableUsesCd ,sumitomoCount
					                                             ,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			if(chartList!=null) {
				Object[] rs2 = chartList.get(0);
				Double maxAvg = 1.0;
				Double minAvg = 5.0;
				if(((Number)rs2[0]).intValue() == 0) {
					int loopCount = 17; //設問に対しての1-5の回答数、回答率セットのレコード初期値
					//最大平均値、最小平均値取得（住友データのみ）
					for(int counter = 0; counter < loopCount ; counter++ ) {
						Object[] rs = chartList.get(counter);
						if(maxAvg <= ((Number)rs[3]).doubleValue()) {
							maxAvg = ((Number)rs[3]).doubleValue();
						}
						if(minAvg >= ((Number)rs[3]).doubleValue()) {
							minAvg = ((Number)rs[3]).doubleValue();
						}
					}
				}
				//レーダーチャート出力用リストにセット
				Integer record = 0; //設問に対しての1-5の回答数、回答率セットのレコード初期値
				Integer maxRecord = chartList.size();
				for(int counter = 0; counter < maxRecord ; counter++ ) {
					ChartListArraylistModel chartListArraylistModel = new ChartListArraylistModel();
					Object[] rs1 = chartList.get(counter);

					//最大、最小評価判定
					Integer setFlg = 0;
					if(((Number)rs1[3]).doubleValue()==minAvg) {
						setFlg = 2; //最小評価
					}
					if(((Number)rs1[3]).doubleValue()==maxAvg) {
						setFlg = 1; //最大評価
					}
					if(sumitomoCount == 0) {
						setFlg = 0;
					}

					chartListArraylistModel.setCompanyId(((Number)rs1[0]).intValue()); //チャート作成会社区分
					chartListArraylistModel.setItemNo(((Number)rs1[1]).intValue()); //項目番号
					chartListArraylistModel.setItem(((String)rs1[2])); //項目（評価設問）
					String val = String.format("%.1f", ((Number)rs1[3]).doubleValue());
					chartListArraylistModel.setAverageEvaluation(Double.parseDouble(val)); //平均評価
					chartListArraylistModel.setColorEvaluation(setFlg); //最大最小評価フラグ

					//回答選択率セット取得
					if(choiceList!=null && ((Number)rs1[0]).intValue() == 0) {
						ArrayList<ChoiceListArraylistModel> choicelistArraylistModel = new ArrayList<ChoiceListArraylistModel>(); //選択肢リスト

						//設問に対しての1-5の回答数、回答率
						for(int counter1=record ; counter1 < (record+5) ; counter1++ ) {
							ChoiceListArraylistModel choicelistArrayListModel = new ChoiceListArraylistModel();
							Object[] rs = choiceList.get(counter1);

							choicelistArrayListModel.setChoiceName((String)rs[0]); //選択肢名
							choicelistArrayListModel.setSummaryCount(((Number)rs[1]).intValue()); //選択人数
							choicelistArrayListModel.setChoicePercentage(((Number)rs[2]).intValue()); //選択率

							choicelistArraylistModel.add(choicelistArrayListModel);
							chartListArraylistModel.setChoiceList(choicelistArraylistModel);
							}
						record += 5; //5行分インクリメント
					}
					chartlistArraylistModel.add(chartListArraylistModel);
				}
				evaluationItemModel.setChartListArrayList(chartlistArraylistModel);
			}
			evaluationItemModel.setStatusCode(Constants.CON_OK);
			evaluationItemModel.setSurveyId(surveyId);

			//ヘッダー情報取得
		//	List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,katashiki ,scmModel ,dealerCd ,applicableUsesCd);
			List<Object[]> header = surveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,katashiki ,scmModel ,dealerCd ,applicableUsesCd,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Object[] rs = header.get(0);
			evaluationItemModel.setSurveyName((String)rs[0]); //アンケート名
			evaluationItemModel.setTargetFigures(((Number)rs[1]).intValue()); //回答目標数
			evaluationItemModel.setAchievementFigures(((Number)rs[2]).intValue()); //回答数
			evaluationItemModel.setEvaluationLvText(levelLongText); //評価レベルテキスト

			return Response.ok(evaluationItemModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}
}