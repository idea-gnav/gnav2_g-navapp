package com.businet.GNavApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.returnMachine.IReturnMachineService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.TblReturnMachineArea;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ReturnMachineAreaDetailModel;

@Path("/returnMachineAreaDetail")
@Stateless
public class ReturnMachineAreaDetailResource {

	private static final Logger logger = Logger.getLogger(ReturnMachineAreaDetailResource.class.getName());

	@EJB
	IReturnMachineService returnMachineService;

	@EJB
	IUserService userService;

	/**
	 * [API No.24] リターンマシンエリア設定情報取得
	 *
	 * @param userId     - ユーザーＩＤ
	 * @param kyotenCode - 営業担当拠点コード
	 * @param areaNumber - エリアNo
	 * @return Response.JSON returnMachineAreaDetail
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId,@FormParam("kyotenCode") String kyotenCd, @FormParam("areaNumber") Integer areaNumber) {

		logger.info("[GNAV][POST] userId=" + userId +", kyotenCD="+ kyotenCd + ", areaNumber=" + areaNumber);

		ReturnMachineAreaDetailModel returnMachineAreaDetail = new ReturnMachineAreaDetailModel();

		try {

			TblReturnMachineArea returnMachineArea = returnMachineService.findByReturnMachineAreaDetail(userId,kyotenCd,
					areaNumber);

			if (returnMachineArea == null)
				return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

			if(returnMachineArea.getKigyouName() != null)
				returnMachineAreaDetail.setCustomerName(returnMachineArea.getKigyouName());

			if(returnMachineArea.getAreaNo() != null)
				returnMachineAreaDetail.setAreaNumber(returnMachineArea.getAreaNo());

			if(returnMachineArea.getAreaName() != null)
				returnMachineAreaDetail.setAreaName(returnMachineArea.getAreaName());

			if (returnMachineArea.getRadius() != null)
				returnMachineAreaDetail.setSearchRangeDistance(returnMachineArea.getRadius());

			if (returnMachineArea.getIdo() != null)
				returnMachineAreaDetail.setSearchRangeFromIdo(ConvertUtil.parseLatLng(returnMachineArea.getIdo()));

			if (returnMachineArea.getKeido() != null)
				returnMachineAreaDetail.setSearchRangeFromKeido(ConvertUtil.parseLatLng(returnMachineArea.getKeido()));

			if (returnMachineArea.getWarningCurrentlyProgressFlg() != null)
				returnMachineAreaDetail
						.setWarningCurrentlyInProgressFlg(returnMachineArea.getWarningCurrentlyProgressFlg());

			if (returnMachineArea.getPeriodicServiceNoticeFLG() != null)
				returnMachineAreaDetail.setPeriodicServiceNoticeFlg(returnMachineArea.getPeriodicServiceNoticeFLG());

			if (returnMachineArea.getPeriodicServiceNowDueFLG() != null)
				returnMachineAreaDetail.setPeriodicServiceNowDueFlg(returnMachineArea.getPeriodicServiceNowDueFLG());

			if (returnMachineArea.getHourMeterFlg() != null)
				returnMachineAreaDetail.setHourMeterFlg(returnMachineArea.getHourMeterFlg());

			if (returnMachineArea.getHourMeter() != null)
				returnMachineAreaDetail.setHourMeter(returnMachineArea.getHourMeter());

			returnMachineAreaDetail.setStatusCode(Constants.CON_OK);

			return Response.ok(returnMachineAreaDetail).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
