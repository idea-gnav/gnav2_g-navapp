package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.SurveyFreeInputListModel;
import com.businet.GNavApp.models.SurveyFreeInputTextListModel;

@Path("/surveyFreeInputList")
@Stateless
public class SurveyFreeInputListResource {

	private static final Logger logger = Logger.getLogger(SurveyFreeInputListResource.class.getName());

	@EJB
	ISurveyStatisticsService SurveyStatisticsService;

	@EJB
	IUserService userService;


	/**
	 * [API No.45] フリー入力一覧
	 *
	 * @param userId
	 * @param surveyId
	 * @param deviceLanguage
	 * @param apiFlg
	 * @param choiceId
	 * @param startRecord
	 * @param year
	 * @return Response.JSON
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId,
			@FormParam("surveyId") Integer surveyId,
			@FormParam("deviceLanguage") Integer deviceLanguage,
			@FormParam("apiFlg") Integer apiFlg,
			@FormParam("choiceId") Integer choiceId,
			@FormParam("startRecord") Integer startRecord,
			@FormParam("year") String year
			) {

		logger.info("[GNAV][POST] userId=" + userId + ", surveyId=" + surveyId + ", deviceLanguage=" + deviceLanguage
				+ ", apiFlg=" + apiFlg+ ", choiceId=" + choiceId+ ", startRecord=" + startRecord+ ", year=" + year);

		try {
			// ユーザー組織権限取得
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String groupNo = null;
			if(user.get(0)[3]!=null)
				groupNo = (String)user.get(0)[3];

			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

			//対象設問IDをSet
			String questionId = null;
			//自社或いは他社で整備する理由
			if(apiFlg == 0)
				questionId = "60,61";
			//定期検査・整備
			else if(apiFlg == 1)
				questionId = "56";
			//購入時の重要指数
			else if(apiFlg == 2)
				questionId = "62";
			//サービスベストブランド
			else if(apiFlg == 3)
				questionId = "33";


			SurveyFreeInputListModel surveyFreeInputListModel = new SurveyFreeInputListModel();

			//設問のアンケート表示設問番号を取得
			if(apiFlg != null && apiFlg != 0) {
				List<Integer> lst1 = SurveyStatisticsService.findDispQuestionNo(questionId);
				if(lst1 != null && lst1.size() > 0) {
					Integer questionNo = ((Number)lst1.get(0)).intValue();
					surveyFreeInputListModel.setQuestionNo(questionNo);
				}
			}

			//設問文書を取得
			List<String> lst2 = SurveyStatisticsService.findQuestion(questionId, deviceLanguage);
			if(lst2 != null && lst2.size() > 0) {
				String questionName = (String)lst2.get(0);
				surveyFreeInputListModel.setQuestionName(questionName);
			}

			//フリー入力一覧を取得
		//	List<String> freeInputList = SurveyStatisticsService.findFreeInput(apiFlg,questionId,choiceId,year);
			List<String> freeInputList = SurveyStatisticsService.findFreeInput(apiFlg,questionId,choiceId,year,sosikiCd ,kigyouCd ,groupNo ,typeFlg);

			if(freeInputList != null && freeInputList.size() > 0) {

				Integer maxCount = Constants.CON_MAX_COUNT;
				if(freeInputList.size() >= maxCount )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				ArrayList<SurveyFreeInputTextListModel> surveyfreeinputtextlistModel = new ArrayList<SurveyFreeInputTextListModel>();
				Integer record = startRecord-1;
				for(int counter = record; counter < (record+50) ; counter++ ) {

					if( freeInputList.size() == counter) {
						break;}

					SurveyFreeInputTextListModel surveyfreeinputtextListModel = new SurveyFreeInputTextListModel();
					String rs = freeInputList.get(counter);
					surveyfreeinputtextListModel.setFreeInputText(((String)rs));
					surveyfreeinputtextlistModel.add(surveyfreeinputtextListModel);
					surveyFreeInputListModel.setFreeInputList(surveyfreeinputtextlistModel);
				}
				surveyFreeInputListModel.setFreeInputCount(freeInputList.size());
			}else {
				surveyFreeInputListModel.setFreeInputCount(0);
			}

			surveyFreeInputListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(surveyFreeInputListModel).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}


}
