package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.mailer.Mailer;
import com.businet.GNavApp.ejbs.user.ISw2UserService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstUser;
import com.businet.GNavApp.entities.Sw2MstUser;
import com.businet.GNavApp.models.ForgetPasswordModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.PropertyUtil;


@Path("/forgotPassword")
@Stateless
public class ForgotPasswordResource {

	private static final Logger logger = Logger.getLogger(ForgotPasswordResource.class.getName());

	@EJB
	IUserService userService;

	@EJB
	ISw2UserService sw2UserService;

	@EJB
	Mailer mailer;



	/**
	 * [API No.2] パスワード送信
	 * @param userId
	 * @param deviceLanguage
	 * @param mode
	 * @return Response.JSON ForgetPasswordModel
	 * @throws AddressException
	 * @throws MessagingException
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
			@FormParam("userId") String userId,
			@FormParam("deviceLanguage") Integer deviceLanguage,
			@FormParam("mode") Integer mode,
			@FormParam("systemFlg") Integer systemFlg
			) throws AddressException, MessagingException {

		logger.info("[GNAV][POST] userId="+userId+", deviceLanguage="+deviceLanguage+", mode="+mode+", systemFlg="+systemFlg);

		try {

			//mode:1(メール送信処理)リクエストはsystemFlg = 0(G@Nav)とする
			if(mode == 1 && systemFlg == null)
				systemFlg = 0;


			// **↓↓↓ 2020/08/19 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//

			// パスワード送付処理対象システム判定
			if(systemFlg == null) {
				int gnavFlg = 0;
				int sw2Flg = 0;

				//G@Navユーザ存在チェック
				MstUser userG = userService.findByUser(userId);
				if (userG != null) gnavFlg = 1;

				//SW2ユーザ存在チェック
				Sw2MstUser userS = sw2UserService.findBySw2User(userId);
				if (userS != null) sw2Flg = 1;

				//ユーザ重複チェック
				if (gnavFlg == 1 && sw2Flg == 1) {
					// 両DBに存在するID,PASSの場合ログイン対象システムのパラメータを貰う
					logger.config("[GNAV][DEBUG] WARNING_USER_DUPLICATION 1701");
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_USER_DUPLICATION)).build();
				}else if(gnavFlg == 1 && sw2Flg == 0) {
					//パスワード送付処理:G@Nav
					systemFlg = 0;
				}else if(gnavFlg == 0 && sw2Flg == 1) {
					//パスワード送付処理:SW2
					systemFlg = 1;
				}else if(gnavFlg == 0 && sw2Flg == 0) {
					//対象ユーザ無し
					logger.config("[GNAV][DEBUG] WARNING NO USER PASSWORD 1201 | USER NULL");
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
				}

			}

			// **↑↑↑ 2020/08/19 iDEA山下 SW2ユーザ認証追加 ↑↑↑**//


			// 対象システム：G@Nav
			if(systemFlg == 0) {
				MstUser user = userService.findByUser(userId);

				if (user != null) {

					// **↓↓↓ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応  ↓↓↓**//
					ForgetPasswordModel forgetPassword = new ForgetPasswordModel();
					String mailAddress = user.getUserMailAddr();

					if(mailAddress!=null) {
						if(isCorrectMailAddr(mailAddress))
							mailAddress = maskingMailAddress(mailAddress);
						else
							mailAddress = null;
					}else {
						mailAddress = null;
					}
					// **↑↑↑ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↑↑↑**//

					// mode0:メールアドレス表示
					if(mode.equals(0) || mode.intValue()==0) {

						// **↓↓↓ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↓↓↓**//
//						ForgetPasswordModel forgetPassword = new ForgetPasswordModel();
//						String mailAddress = user.getUserMailAddr();
//						if(mailAddress!=null)
//							forgetPassword.setMailAddress(maskingMailAddress(mailAddress));
//						else
//							forgetPassword.setMailAddress("No mail address.");
						if(mailAddress!=null)
							forgetPassword.setMailAddress(maskingMailAddress(mailAddress));
						else
							forgetPassword.setMailAddress("No mail address.");
						// **↑↑↑ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↑↑↑**//


						forgetPassword.setStatusCode(Constants.CON_OK);
						return Response.ok(forgetPassword).build();
					}


					// mode1:パスワードメール送信
					if(mode.equals(1) || mode.intValue()==1) {

						// **↓↓↓ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↓↓↓**//
						if(mailAddress!=null) {
						// **↑↑↑ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↑↑↑**//

							String title = null;
							String contents = null;

							String userName = user.getUserName();
							String password = user.getPassword();
							String mailAddr = user.getUserMailAddr();

							// デバイス言語  ja:日本語  en:英語  id:インドネシア語  tr:トルコ語
							String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

							String keyTitle = "title_"+languageCd;
							String keyContents = "contents_"+languageCd;

							title = PropertyUtil.readProperties(Constants.APP_CONFIG_PATH, keyTitle, null);
							contents = PropertyUtil.readProperties(Constants.APP_CONFIG_PATH, keyContents, null).replace("@userName", userName).replace("@password", password);

							if(title!=null && contents!=null) {
								logger.config(title);
								logger.config(contents);

								if(mailer.sendMail(mailAddr, title, contents, new ArrayList<String>()))
									return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
								else
									return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

							// **↓↓↓ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↓↓↓**//
							}else {
								// メールアドレスがNULLまたは、不正の場合は、何もせず、正常で返却。
								return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
							}
							// **↑↑↑ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↑↑↑**//

						}
					}

				}else if (user == null) {
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
				}

			// **↓↓↓ 2020/08/19 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//
			//　対象システム：SW2
			}else if(systemFlg == 1) {
				logger.config("[GNAV][DEBUG] WARNING NO PASSWORD_SEND 1702 | SW2 User");
				return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_PASSWORD_SEND)).build();

			}
			// **↑↑↑ 2020/08/19 iDEA山下 SW2ユーザ認証追加 ↑↑↑**//


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}
		return null;
	}



	// **↓↓↓ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↓↓↓**//
	/**
	 * メールアドレスをチェック
	 * @param mailAddress
	 * @return
	 */
	public boolean isCorrectMailAddr(String mailAddress) {
		return mailAddress.contains("@");
	}
	// **↑↑↑ 2019/02/19 LBN石川 不具合対応 メールアドレス登録が無い場合の対応 ↑↑↑**//




	/**
	 * メールアドレスをマスク
	 * @param mailAddress
	 * @return
	 */
	public String maskingMailAddress(String mailAddress) {

		int marker = mailAddress.indexOf("@");
		StringBuilder asterisk = new StringBuilder();
		StringBuilder mailAddr = new StringBuilder(mailAddress);

		for(int counter=2; counter < marker; counter++)
			asterisk.append("*");

		mailAddr.replace(2, marker, new String(asterisk));

		return new String(mailAddr);
	}



}
