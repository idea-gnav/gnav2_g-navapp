package com.businet.GNavApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.surveyStatistics.ISurveyStatisticsService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.ServiceEvaluationPriorityArrayListModel;
import com.businet.GNavApp.models.ServiceEvaluationPriorityModel;

@Path("/serviceEvaluationPriority")
@Stateless
public class ServiceEvaluationPriorityResource {

	private static final Logger logger = Logger.getLogger(ServiceEvaluationPriorityResource.class.getName());

	@EJB
	ISurveyStatisticsService SurveyStatisticsService;

	@EJB
	IUserService userService;

	/**
	 * [API No.40] サービス項目への評価平均と優先度
	 *
	 * @param userId
	 * @param surveyId
	 * @param year
	 * @param deviceLanguage
	 * @return Response.JSON serviceEvaluationPriorityModel
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(@FormParam("userId") String userId,
			@FormParam("surveyId") Integer surveyId,
			@FormParam("year") String year,
			@FormParam("deviceLanguage") Integer deviceLanguage) {

		logger.info("[GNAV][POST] userId=" + userId + ", surveyId=" + surveyId + ", year=" + year + ", deviceLanguage=" + deviceLanguage);

		try {

			// ユーザー組織権限取得
						List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

						String sosikiCd = null;
						if(user.get(0)[1]!=null)
							sosikiCd = (String)user.get(0)[1];

						String kigyouCd = null;
						if(user.get(0)[2]!=null)
							kigyouCd = (String)user.get(0)[2];

						String groupNo = null;
						if(user.get(0)[3]!=null)
							groupNo = (String)user.get(0)[3];

						Integer typeFlg = null;
						if(user.get(0)[5]!=null)
							typeFlg = ((Number)user.get(0)[5]).intValue();
						else
							return Response.ok(new ReturnContainer(Constants.CON_OK)).build();


			ServiceEvaluationPriorityModel serviceEvaluationPriority = new ServiceEvaluationPriorityModel();

			//ヘッダー情報取得
		//	List<Object[]> headers  = SurveyStatisticsService.findSurveyTargetFigures(surveyId, deviceLanguage,year,null,null,null,null);
			List<Object[]> headers = SurveyStatisticsService.findSurveyTargetFigures(surveyId ,deviceLanguage ,year ,null,null ,null ,null,sosikiCd ,kigyouCd ,groupNo ,typeFlg);
			Object[] header = headers.get(0);

			//アンケート名称取得
			String surveyName = null;
			if(header[0]!=null) {
				surveyName = (String)header[0];
			}
			serviceEvaluationPriority.setSurveyName(surveyName);

			//回答目標数取得
			Integer targetFigures = 0;
			if(header[1]!=null) {
				targetFigures = ((Number)header[1]).intValue();
			}
			serviceEvaluationPriority.setTargetFigures(targetFigures);

			//回答数取得
			Integer achievementFigures = 0;
			if(header[2]!=null) {
				achievementFigures = ((Number)header[2]).intValue();
			}
			serviceEvaluationPriority.setAchievementFigures(achievementFigures);



			//アンケート統計データリスト
		//	List<Object[]> evaluationPrioritys = SurveyStatisticsService.findEvaluationPrioritys(deviceLanguage,year);
			List<Object[]> evaluationPrioritys = SurveyStatisticsService.findEvaluationPrioritys(deviceLanguage,year,sosikiCd ,kigyouCd ,groupNo ,typeFlg);

			if(evaluationPrioritys != null && evaluationPrioritys.size() > 0) {
				ArrayList<ServiceEvaluationPriorityArrayListModel> prioritysLists  = new ArrayList<ServiceEvaluationPriorityArrayListModel>();
				for (Object[] obj : evaluationPrioritys) {

					ServiceEvaluationPriorityArrayListModel priorityList = new ServiceEvaluationPriorityArrayListModel();

					Integer itemNo = null;
					if (obj[0] != null)
						itemNo = Integer.parseInt((String)obj[0]);

					Integer itemId = null;
					if (obj[1] != null)
						itemId = ((Number)obj[1]).intValue();

					String item = null;
					if(obj[2] != null)
						item = (String)obj[2].toString();

					Double priority = null;
					if(obj[3] != null)
						priority = ((Number)obj[3]).doubleValue();

					Double cspoint = null;
					if(obj[4] != null) {
						cspoint = ((Number)obj[4]).doubleValue();
					}
					priorityList.setItemNo(itemNo);
					priorityList.setItemId(itemId);
					priorityList.setItem(item);
					priorityList.setPriority(priority);
					priorityList.setCsPoint(cspoint);

					prioritysLists.add(priorityList);
				}
				serviceEvaluationPriority.setServiceEvaluationPriorityList(prioritysLists);
			}

			serviceEvaluationPriority.setStatusCode(Constants.CON_OK);
			return Response.ok(serviceEvaluationPriority).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}


}
