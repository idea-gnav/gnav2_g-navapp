package com.businet.GNavApp.resources;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.dtcDitail.IDtcDitailService;
import com.businet.GNavApp.ejbs.machine.IMachineService;
import com.businet.GNavApp.ejbs.report.IReportService;
import com.businet.GNavApp.ejbs.sosiki.ISosikiService;
import com.businet.GNavApp.ejbs.user.IUserService;
import com.businet.GNavApp.entities.MstMachine;
import com.businet.GNavApp.entities.TblTeijiBunpuReport;
import com.businet.GNavApp.entities.TblTeijiReport;
import com.businet.GNavApp.models.DailyReportModel;
import com.businet.GNavApp.models.DisplayConditionModel;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.util.Commons;
import com.businet.GNavApp.util.PropertyUtil;



@Path("/dailyReport")
@Stateless
public class DailyReportResource {


	private static final Logger logger = Logger.getLogger(DailyReportResource.class.getName());


	@EJB
	IReportService reportService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IDtcDitailService dtcDitailService;

	@EJB
	IUserService userService;

	/**
	 * [API No.10] úñÚ×æ¾
	 * @param serialNumber
	 * @param userId
	 * @param searchDate
	 * @return Response.JSON DailyReportModel
	 * @throws ParseException
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("serialNumber") Long serialNumber,
			    @FormParam("userId") String userId,
				@FormParam("searchDate") String searchDate,
				@FormParam("deviceLanguage") Integer deviceLanguage
			) throws ParseException {

		logger.info("[GNAV][POST] userId="+userId+", serialNumber="+serialNumber+", searchDate="+searchDate+", deviceLanguage="+deviceLanguage);

//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat df = new DecimalFormat("#.#");

		try {

			// [U[gD Àæ¾
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String kengenCd = null;
			if(user.get(0)[4]!=null)
				kengenCd = user.get(0)[4].toString();

			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);


			DailyReportModel dailyReport = new DailyReportModel();

			MstMachine machine = machineService.findByMachine(serialNumber);
			if(machine != null) {

				// @BÇÔ serialNumber
				dailyReport.setserialNumber(machine.getKibanSerno());

				// @BJeS[æª	machineModelCategory
				dailyReport.setMachineModelCategory(Commons.identifyKibanType(machine.getKiban(),machine.getConType()));

				// ÎÛNú date
				dailyReport.setDate(searchDate);

				// SCM@Ô manufacturerSerialNumber
				dailyReport.setManufacturerSerialNumber(machine.getKiban());

				// ¨qlÇÔ customerManagementNo
				dailyReport.setCustomerManagementNo(machine.getUserKanriNo());

				// ¨ql¼Ì customerManagementName
				if(machine.getSosikiCd()!=null)
					dailyReport.setCustomerManagementName(sosikiService.findByKigyouSosikiName(machine.getSosikiCd()));


				// SCM^® scmModel
				dailyReport.setScmModel(machine.getModelCd());

				// ÅVÌÝn latestLocation
				String positionStr = machineService.findByMachinePosition(machine.getKibanSerno(), machine.getConType(), languageCd);
				if(positionStr!=null)
					dailyReport.setLatestLocation(positionStr);

				// ÅVÜx ido
				// ÅVox keido
				if(machine.getNewIdo() != null)
					dailyReport.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));
				if(machine.getNewKeido() != null)
					dailyReport.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));

				// ÅVA[^ hourMeter
				if(machine.getNewHourMeter()!=null)
					dailyReport.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);

				// ÅVÊMú latestUtcCommonDateTime
				if(machine.getRecvTimestamp()!=null)
					dailyReport.setLatestUtcCommonDateTime(ConvertUtil.formatYMD(machine.getRecvTimestamp()));

				// Afx defLevelAdblueLevel
				dailyReport.setDefLevel(machine.getUreaWaterLevel());

				// R¿x fuelLevel
				dailyReport.setFuelLevel(machine.getNenryoLv());

	 			// Ú§äp^[
				DisplayConditionModel displayCondition = Commons.machineIconType(machineService.findByMachineTypeControl(machine.getConType(), machine.getMachineModel()), machine.getKiban());
				// Úp^[æª
				Integer patternType = displayCondition.getDailyReportType();
				dailyReport.setPatternType(patternType);
				dailyReport.setDisplayItemPattern(PropertyUtil.readProperties(Constants.APP_CONFIG_PATH, Constants.TYPE_DAILY_REPORT+patternType, null));
				// @BACR
				dailyReport.setIconType(displayCondition.getMachineIconType());

				// **«««@2020/01/30  iDEARº ­¶xñxÇÁ «««**//
				//èú®õ xæ¾
				Integer periodiclevel = 0;
				Integer yokokuCount = machine.getYokokuCount();
				Integer keyikokuCount = machine.getKeyikokuCount();
				if(keyikokuCount > 0 )
					periodiclevel = 2;
				else if(yokokuCount > 0)
					periodiclevel = 1;


				//DTC xæ¾
				List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
						serialNumber, 1, null, null, null, Integer.parseInt(kengenCd.toString()), ConvertUtil.convertLanguageCd(deviceLanguage),1);
				Integer dtcLv = 1;

				if(dtcs != null) {
					Object[] dtc = dtcs.get(0);
					if(dtc[8]!=null)
						dtcLv = Integer.parseInt(dtc[8].toString());
				}

				//xñACRíÊ
				dailyReport.setAlertLevelM(Commons.alertLevel(dtcLv, periodiclevel));

				// **ªªª@2020/01/30  iDEARº ­¶xñxÇÁ ªªª**//

			}




 		// |[gWv
			TblTeijiReport ttr = reportService.findByDailyReport(serialNumber, searchDate);
			if(ttr != null) {

				// úñf[^(o[Ot) nippoData
				dailyReport.setNippoData(ttr.getNippoData());

				// 1 GWÒ­Ô engineOperatingTime
				BigDecimal engineOperating = new BigDecimal(0);
				if(ttr.getHourMeter() != null) {
//					engineOperating = ttr.getHourMeter()/60.0;
					engineOperating = new BigDecimal(ttr.getHourMeter());
					dailyReport.setEngineOperatingTime(ConvertUtil.convertTimeHhMm(ttr.getHourMeter()));
					logger.config("[GNAV][DEBUG] engineOperating="+ConvertUtil.convertTimeHhMm(ttr.getHourMeter()));
				}


				// 2 @BìÔ machineOperatingTime
				BigDecimal machineOperating = new BigDecimal(0);
				if(ttr.getKikaiSosaTime()!=null) {
					machineOperating = new BigDecimal(ttr.getKikaiSosaTime());
					dailyReport.setMachineOperatingTime(ConvertUtil.convertTimeHhMm(ttr.getKikaiSosaTime()));
					logger.config("[GNAV][DEBUG] machineOperating="+ConvertUtil.convertTimeHhMm(ttr.getKikaiSosaTime()));
				}


				// 3 ACh¦ idlePercentage
				if(engineOperating.doubleValue() > 0.0) {
					double idleTime = new BigDecimal(engineOperating.doubleValue()-machineOperating.doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
					logger.config("[GNAV][DEBUG] idleTime="+idleTime);
					if(idleTime > 0.0) {
						BigDecimal idlePercentage = new BigDecimal(idleTime*100/ttr.getHourMeter()).setScale(1, BigDecimal.ROUND_HALF_UP);
						logger.config("[GNAV][DEBUG] idlePercentage="+idlePercentage.doubleValue());
						dailyReport.setIdlePercentage(idlePercentage.doubleValue());
					}
				}

				// 4 R¿ÁïÊ fuelConsumption
				double fuelConsum = 0;		// vZp
				if(ttr.getNenryoConsum()!=null) {
					fuelConsum = new BigDecimal(df.format(Math.floor((ttr.getNenryoConsum())*10)/10)).doubleValue();
					dailyReport.setFuelConsumption(fuelConsum);
				}

				// 5 Rï fuelEffciency
				// **««« 2019/02/20 LBNÎì  FormatExceptionÎ «««**//
				if(fuelConsum > 0.0 && engineOperating.doubleValue() > 0.0) {
//				if(fuelConsum > 0.0) {
				// **ªªª 2019/02/20 LBNÎì  FormatExceptionÎ ªªª**//
					// **««« 2018/10/22 LBNÎì STsïÎ WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  «««**//
					// BigDecimal fuelEffciency = new BigDecimal(ttr.getNenryoConsum()/engineOperating).setScale(1, BigDecimal.ROUND_HALF_UP);
					BigDecimal fuelEffciency = new BigDecimal(0);;
					if(machine.getConType()!=null) {
						if(machine.getConType().equals("ND") || machine.getConType().equals("D")) {
							fuelEffciency = new BigDecimal(ttr.getNenryoConsum()/ttr.getHourMeter()*60).setScale(1, BigDecimal.ROUND_HALF_UP);	// Modify 0.5 / 9 * 60 => 3.3333
						}else {
							if(Math.floor(engineOperating.doubleValue()/60.0*10)/10 > 0) {
								fuelEffciency = new BigDecimal(ttr.getNenryoConsum()/new BigDecimal(df.format(Math.floor(engineOperating.doubleValue()/60.0*10)/10)).doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP);	// <------ this!!!!!
							}
						}
					}
					// **ªªª 2018/10/22 LBNÎì STsïÎ  WEBÅÌ¹H@BÆVxÌZoû®Ìá¢Î  ªªª**//
					dailyReport.setFuelEffciency(fuelEffciency.doubleValue());
				}

				// 6 R¿x fuelLevelDaily
				dailyReport.setFuelLevelDaily(ttr.getNenryoLv());

				// 7 AfÁïÊ defConsumpiton
				if(ttr.getUreaWaterConsum()!=null)
					// cc -> L
					dailyReport.setDefConsumpiton(new BigDecimal(df.format(Math.floor(ttr.getUreaWaterConsum()/100)/10)).doubleValue());


				// 8 Afx defLevelDaily
				dailyReport.setDefLevelDaily(ttr.getUreaWaterLv());


				// 9 WG^[âp·xMax  rageeterOndoMax
				if(ttr.getRadiatorOndoMax() != null)
					dailyReport.setRageeterOndoMax(ttr.getRadiatorOndoMax());


				// 10 ì®û·xMax oilTempartureMax
				if(ttr.getSadoyuOndoMax() != null)
					dailyReport.setOilTempartureMax(ttr.getSadoyuOndoMax());


				// 11 {HÔ constructionTime
				if(ttr.getSekoTime()!=null)
					dailyReport.setConstructionTime(ConvertUtil.convertTimeHhMm(ttr.getSekoTime()));


				// 12 Þg tonsMaterialHandling
				if(ttr.getMixtureProcessingTon()!=null) {
					dailyReport.setTonsMaterialHandling(ttr.getMixtureProcessingTon());
					logger.config("[GNAV][DEBUG] MixtureProcessingTon="+ttr.getMixtureProcessingTon());
				}

				// 13 {H£ constructionDistance
				if(ttr.getSekoKyori()!=null)
					dailyReport.setConstructionDistance(ttr.getSekoKyori());

				// 14 Ô¼obed³ batteryVoltage
				if(ttr.getVehicleBatteryVoltage()!=null)
					dailyReport.setBatteryVoltage(ttr.getVehicleBatteryVoltage());

				// 15 I^l[^­dd³ alternatorVoltage
				if(ttr.getAlternatorGeneratingVoltage()!=null)
					dailyReport.setAlternatorVoltage(ttr.getAlternatorGeneratingVoltage());

				// 16 CÓ@è®@Jnñ optionalManualStartCount
				if(ttr.getRanShudoSaiseiStartNum()!=null)
					dailyReport.setOptionalManualStartCount(ttr.getRanShudoSaiseiStartNum());

				// 17 v@è®@Jnñ requestManualStartCount
				if(ttr.getReqShudoSaiseiStartNum()!=null)
					dailyReport.setRequestManualStartCount(ttr.getReqShudoSaiseiStartNum());

				// 18 ©®Ä¶@Jnñ autoPlayStart
				if(ttr.getJidoSaiseiStartNum()!=null)
					dailyReport.setAutoPlayStart(ttr.getJidoSaiseiStartNum());

				// 19 ©®Ä¶@®¹ñ autoPlayCompleted
				if(ttr.getJidoSaiseiOverNum()!=null)
					dailyReport.setAutoPlayCompleted(ttr.getJidoSaiseiOverNum());

				// 20 è®Ä¶@Jnñ manualPlayStart				TBL_TEIJI_REPORT.SHUDO_SAISEI_START_NUM
				if(ttr.getShudoSaiseiStartNum()!=null)
					dailyReport.setManualPlayStart(ttr.getShudoSaiseiStartNum());

				// 21 è®Ä¶@®¹ñ manualPlayCompleted			TBL_TEIJI_REPORT.SHUDO_SAISEI_OVER_NUM
				if(ttr.getShudoSaiseiOverNum()!=null)
					dailyReport.setManualPlayCompleted(ttr.getShudoSaiseiOverNum());

				// 22 t}Ozøñ lifMagSuction						TBL_TEIJI_REPORT.LIFTING_MAGNET_CRANE
				if(ttr.getLiftingMagnetCrane()!=null)
					dailyReport.setLifMagSuction(ttr.getLiftingMagnetCrane());
				// **«««@2018/10/22 LBNÎì STsïÎ nullÌê 0ðÔp ÇÁ «««**//
				else
					dailyReport.setLifMagSuction(0);
				// **ªªª@2018/10/22 LBNÎì STsïÎ nullÌê 0ðÔp ÇÁ ªªª**//

				// 23 t}OÔ lifMagTime TBL_TEIJI_REPORT.LIFTING_MAGNET_TIME
				if(ttr.getLiftingMagnetTime()!=null)
					dailyReport.setLifMagTime(ConvertUtil.convertTimeHhMm(ttr.getLiftingMagnetTime()));

				// 24 zbgVbg_E hotShutdown TBL_TEIJI_BUNPU_REPORT.COOLING_TIME1
				TblTeijiBunpuReport tbr = reportService.findByTeijiBunpuReport(serialNumber, searchDate);
				if(tbr!=null) {
					if(tbr.getCoolingTime1()!=null)
						dailyReport.setHotShutdown(tbr.getCoolingTime1());
				}

				// 25 u[JìÔ breakerTime TBL_TEIJI_REPORT.BREAKER_TIME
				if(ttr.getBreakerTime()!=null)
					dailyReport.setBreakerTime(ConvertUtil.convertTimeHhMm(ttr.getBreakerTime()));

			}

			dailyReport.setStatusCode(Constants.CON_OK);
			return Response.ok(dailyReport).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}


		}
	}


