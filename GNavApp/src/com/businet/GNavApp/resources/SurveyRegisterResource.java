package com.businet.GNavApp.resources;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.annotations.Authorized;
import com.businet.GNavApp.ejbs.surveyService.ISurveyService;
import com.businet.GNavApp.models.ReturnContainer;
import com.businet.GNavApp.models.SurveyAnswerChoiceListModel;
import com.businet.GNavApp.models.SurveyRegisterInputModel;
import com.businet.GNavApp.models.SurveyRegisterModel;
import com.businet.GNavApp.models.SurveyRegisterQuestionListModel;

@Path("/surveyRegister")
@Stateless
public class SurveyRegisterResource {

	private static final Logger logger = Logger.getLogger(SurveyRegisterResource.class.getName());
	private static String Url_google_translate = "https://translation.googleapis.com/language/translate/v2";
	private static String Api_key = "AIzaSyDM1VcqFWnilumKW5rEE_UMtn1-Oga0Izg";

	@EJB
	ISurveyService surveyService;

	/**
	 * [API No.33] アンケート登録
	 *
	 * @param userId
	 * @param surveyId
	 * @param serialNumber
	 * @param surveyResultId
	 * @param surveyDelFlg
	 * @param kigyoName
	 * @param answerUser
	 * @param position
	 * @param questionList
	 * @param deviceLanguage
	 * @Param hourMeter
	 * @Param inputDate
	 * @Param ido
	 * @Param keido
	 * @return Response.JSON SurveyDetailModel
	 */

	@POST
	@Consumes(MediaType.APPLICATION_JSON) // add
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(SurveyRegisterInputModel surveyRegisterParam) {

		logger.info("[GNAV][POST] userId=" + surveyRegisterParam.getUserId() + ", surveyId="
				+ surveyRegisterParam.getSurveyId() + ", serialNumber=" + surveyRegisterParam.getSerialNumber()
				+ ", surveyResultId=" + surveyRegisterParam.getSurveyResultId() + ", surveyDelFlg="
				+ surveyRegisterParam.getSurveyDelFlg() + ", kigyoName=" + surveyRegisterParam.getKigyoName()
				+ ", answerUser=" + surveyRegisterParam.getAnswerUser() + ", position="
				+ surveyRegisterParam.getPosition() + ", questionList=" + surveyRegisterParam.getQuestionList()
				+ ", deviceLanguage=" + surveyRegisterParam.getDeviceLanguage() + ", hourMeter=" + surveyRegisterParam.getHourMeter()
				+ ", inputDate=" + surveyRegisterParam.getInputDate()
				// **↓↓↓ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↓↓↓**//
				+ ", ido=" + surveyRegisterParam.getIdo()+ ", keido=" + surveyRegisterParam.getKeido());
				// **↑↑↑ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↑↑↑**//

		try {
			SurveyRegisterModel surveyRegister = new SurveyRegisterModel();

			String userId = surveyRegisterParam.getUserId();
			Integer surveyId = surveyRegisterParam.getSurveyId();
			Long serialNumber = surveyRegisterParam.getSerialNumber();
			Integer surveyResultId = surveyRegisterParam.getSurveyResultId();
			Integer surveyDelFlg = surveyRegisterParam.getSurveyDelFlg();
			String kigyoName = surveyRegisterParam.getKigyoName();
			String answerUser = surveyRegisterParam.getAnswerUser();
			String position = surveyRegisterParam.getPosition();
			List<SurveyRegisterQuestionListModel> questionList = surveyRegisterParam.getQuestionList();
			int deviceLanguage = surveyRegisterParam.getDeviceLanguage();
			Double hourMeter = null;
			Double ido = surveyRegisterParam.getIdo();
			Double keido = surveyRegisterParam.getKeido();
			if(surveyRegisterParam.getHourMeter() != null) {
				hourMeter = surveyRegisterParam.getHourMeter() * 60.0;
			}

			String inputDate = surveyRegisterParam.getInputDate();
			List<Object[]> surveyDetails = null;

			if (surveyRegisterParam.getSurveyDelFlg() != 1 && questionList != null && questionList.size() > 0) {
				surveyDetails = new ArrayList<Object[]>();
				StringBuilder sb = new StringBuilder();
				boolean isRunTranslation = false;

				for (SurveyRegisterQuestionListModel question : questionList) {
					Object[] obj = new Object[5];

					obj[0] = question.getQuestionId();

					ArrayList<SurveyAnswerChoiceListModel> answerChoices = question.getAnswerChoiceList();
					int answerCnt = answerChoices != null ? answerChoices.size() : 0;
					if (answerCnt > 0) {
						int[] answerChoiceList = new int[answerCnt];
						for (int i = 0; i < answerCnt; i++) {
							answerChoiceList[i] = answerChoices.get(i).getAnswerChoiceId();
						}
						obj[1] = answerChoiceList;

					} else
						obj[1] = null;

					String answerFreeinput = question.getAnswerFreeinput();

					if (answerFreeinput != null && !isRunTranslation)
						isRunTranslation = true;

					sb.append("&q=" + answerFreeinput);

					if (deviceLanguage != 0) {

					}

					obj[2] = answerFreeinput;
					obj[3] = null; // answerFreeInput English language

					obj[4] = question.getAnswerImageEditFlg();
					surveyDetails.add(obj);
				}

				//if (isRunTranslation) {
				if (false) {
					sb.append("&target=en");
					String parameter = "key=" + Api_key + sb.toString();
					ArrayList<String> translatedTex = translateText(parameter);
					if (translatedTex != null && translatedTex.size() == surveyDetails.size()) {
						for (int i = 0; i < surveyDetails.size(); i++) {
							Object[] obj = surveyDetails.get(i);
							obj[3] = translatedTex.get(i);
						}
					}
				}
			}

			// **↓↓↓ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↓↓↓**//
			// languageCd  0:en 英語  3:ja 日本語  4:id インドネシア語  5:tr トルコ語
			String languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);
			Integer surveyResultIdReturn = null;
			// Mode Register
			if (surveyResultId == null) {
				surveyResultIdReturn = surveyService.registerSurvey(null, serialNumber, surveyId, kigyoName, answerUser,
						position, userId, surveyDetails, hourMeter,inputDate,languageCd);
			}
			// Mode Update
			else if (surveyDelFlg == 0) {
				surveyResultIdReturn = surveyService.registerSurvey(surveyResultId, serialNumber, surveyId, kigyoName,
						answerUser, position, userId, surveyDetails,null,inputDate,languageCd);
			}
			// **↑↑↑ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↑↑↑**//

			// Mode Delete
			else {
				surveyService.deleteSurveyResult(surveyRegisterParam.getSurveyResultId(),
						surveyRegisterParam.getUserId());
			}
			// **↓↓↓ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↓↓↓**//
			//アンケート回答ポジション作成
			surveyService.insertLastPosition(surveyResultIdReturn, serialNumber,ido, keido, userId);
			// **↑↑↑ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↑↑↑**//

			surveyRegister.setSurveyResultId(surveyResultIdReturn);
			surveyRegister.setStatusCode(Constants.CON_OK);
			return Response.ok(surveyRegister).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}

	}

	public ArrayList<String> translateText(String parameter) throws IOException {
		ArrayList<String> rs = new ArrayList<String>();

		URL obj = new URL(Url_google_translate);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		con.setRequestMethod("POST");

		String urlParameters = parameter;

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.write(urlParameters.getBytes("UTF-8"));
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();

		if (responseCode != 200)
			return null;

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		JSONObject jResponse = new JSONObject(response.toString());
		if (jResponse == null || !jResponse.has("data"))
			return null;

		JSONObject jData = jResponse.getJSONObject("data");
		if (!jData.has("translations"))
			return null;

		JSONArray list = jData.getJSONArray("translations");
		if (list == null || list.length() == 0)
			return null;

		for (int j = 0; j < list.length(); j++) {
			JSONObject res = list.getJSONObject(j);
			{
				rs.add(res.get("translatedText").toString());
				System.out.println(
						res.get("translatedText") + "detectedSourceLanguage " + res.getString("detectedSourceLanguage"));
			}
		}

		return rs;
	}

}
