package com.businet.GNavApp.util;

import java.util.logging.Logger;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.entities.MstAppMachineTypeControl;
import com.businet.GNavApp.models.DisplayConditionModel;

public class Commons {


	private static final Logger logger = Logger.getLogger(Commons.class.getName());


	private Commons() {}



	/**
	 * 6^7^¯Ê @BJeS[æª
	 * æÊ§ätO
	 *  1: R¿-\¦   Af-ñ\¦
	 *  2: R¿-\¦   Af-\¦
	 *  3: R¿-ñ\¦ Af-ñ\¦
	 */
	public static Integer identifyKibanType(String kiban,String conType) {
		Integer machineModelCategory = 3;

		if(kiban!=null && conType!=null) {
			//¹H@B
			if(conType.equals("D") || conType.equals("ND")) {
				if(kiban.length()>=6 && kiban.substring(5,6).equals("-")) {
					machineModelCategory = 3;
				}else if(kiban.length()>=5 && kiban.substring(4,5).equals("4")){
					machineModelCategory = 3;
				}else if(kiban.length()>=5 && kiban.substring(4,5).equals("6")){
			        if(kiban.length()>=8 && kiban.substring(6,8).equals("WE")){
			        	machineModelCategory = 2;
		            }else{
		             	machineModelCategory = 1;
			        }
				}else {
					machineModelCategory = 1;
				}
			//Vx
			}else{
				if(kiban.length()>=6 && kiban.substring(5,6).equals("-")){
					if(kiban.substring(4,5).equals("5")) {
						machineModelCategory = 1;
					}else {
						machineModelCategory = 3;
					}
				}else if(kiban.length()>=8 && ( kiban.substring(7,8).equals("5") || kiban.substring(7,8).equals("6") )) {
					machineModelCategory = 1;
				}else if(kiban.length()>=8 && kiban.substring(7,8).equals("7")) {
					machineModelCategory = 2;
				}else {
					machineModelCategory = 3;
				}
			}
		}

		logger.config("[DEBUG]machineModelCategory="+machineModelCategory);
		return machineModelCategory;

	}



	/**
	 * @B^®Êp^[
	 * ÊMíÊA@B^®
	 */
	public static DisplayConditionModel machineIconType(MstAppMachineTypeControl mdc, String kiban) {

		DisplayConditionModel displayCondition = new DisplayConditionModel();

		if(mdc!=null && kiban!=null) {

			if(kiban.startsWith("LBX"))
				displayCondition.setMachineIconType(Constants.LBX_ICON);
			else
				displayCondition.setMachineIconType(mdc.getIconType());
				displayCondition.setMachineDetailType(mdc.getMachineDitailType());
				displayCondition.setDailyReportType(mdc.getDailyReportType());
				displayCondition.setGrapheReportFlg(mdc.getGrapheReportFlg());

		}else {

			displayCondition.setMachineIconType(Constants.ELSE_ICON_TYPE);
			if(kiban!=null)
				if(kiban.startsWith("LBX"))
					displayCondition.setMachineIconType(Constants.LBX_ICON);
				//	displayCondition.setMachineIconType(Constants.ELSE_ICON_TYPE);
				    displayCondition.setMachineDetailType(Constants.ELSE_MACHINE_DETAIL_TYPE);
				    displayCondition.setDailyReportType(Constants.ELSE_DAILY_REPORT_TYPE);
				    displayCondition.setGrapheReportFlg(Constants.ELSE_GRAPH_REPORT_TYPE);

		}

		logger.config("[DEBUG]MachineIconType="+displayCondition.getMachineIconType());
		return displayCondition;
	}

	/**
	 * xñxACRp^[
	 * DTCAèú®õ
	 */
	public static Integer alertLevel(Integer dtcLv,Integer periodiclevel) {
		int keihoIconNo = 9; //ftHg
		boolean dtcNormal = false, dtcImportant = false, yokoku = false, keyikoku = false;
		if (dtcLv == 3)
			dtcNormal = dtcImportant = true;
		else if (dtcLv == 2)
			dtcNormal = true;

		yokoku = (periodiclevel == 1);
		keyikoku = (periodiclevel == 2);

		if (dtcImportant && keyikoku)
			keihoIconNo = 1;
		else if (dtcImportant && yokoku && !keyikoku)
			keihoIconNo = 2;
		else if (dtcNormal && !dtcImportant && keyikoku)
			keihoIconNo = 3;
		else if (dtcNormal && !dtcImportant && yokoku && !keyikoku)
			keihoIconNo = 4;
		else if (dtcImportant && !yokoku && !keyikoku)
			keihoIconNo = 5;
		else if (dtcNormal && !dtcImportant && !yokoku && !keyikoku)
			keihoIconNo = 6;
		else if (!dtcNormal && !dtcImportant && keyikoku)
			keihoIconNo = 7;
		else if (!dtcNormal && !dtcImportant && yokoku && !keyikoku)
			keihoIconNo = 8;
		else
			keihoIconNo = 9;


		logger.config("[DEBUG]alertLevel="+keihoIconNo);
		return keihoIconNo;

	}




}
