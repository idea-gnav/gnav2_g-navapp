package com.businet.GNavApp;

public class Constants {

	public static final String CON_AUTHORIZED_PUBLIC="Public";
	public static final String CON_AUTHORIZED_REQUIRED="Required";

	public static final int CON_OK = 1000;							// 成功
//	public static final int CON_WARNING_PASSWORD_3DAYS = 1001;		// 認証成功、パスワード有効期限切れ間近(3日）
	public static final int CON_WARNING_PASSWORD_EXPIRED = 1101;	// パスワード有効期限切れ、初回利用時パスワード変更
	public static final int CON_WARNING_NO_USER_PASSWORD = 1201;	// ユーザーIDまたはパスワード誤り
	public static final int CON_WARNING_APP_VERSION = 1301;			// アプリケーションバージョン警告
	public static final int CON_WARNING_MAXIMUN_NUMBER = 1401;		// 検索件数が最大件数を超える
//	public static final int CON_WARNING_PASSWORD_7DAYS = 1501;		// 認証成功、パスワード有効期限切れ間近(7日）
	public static final int CON_WARNING_NO_LATLNG = 1601;			// 特定位置集権検索の機番が存在しない又は機番に緯度経度がない
	public static final int CON_WARNING_USER_DUPLICATION = 1701;			// G@NavとSW2両方にユーザ/Passが存在する
	public static final int CON_WARNING_NO_PASSWORD_SEND = 1702;			// ユーザパスワード送信不可
	public static final int CON_UNKNOWN_ERROR = 1002;				// 例外

	public static final int CON_APP_FLG = 1;										// 1: GNavApp , 0:RemoteCAREApp

	public static final String TYPE_ICON = "iconType_";								// ログイン時 機械アイコン
	public static final String TYPE_ICON_LIST = "iconTypeList";						// ログイン時 機械アイコンリスト

	public static final String ALERT_ICON = "iconAlertLevel_";							// ログイン時 発生中警報レベルアイコン
	public static final String ALERT_LEVEL_ICON_LIST = "iconAlertLevelList";			// ログイン時 発生中警報レベルアイコンリスト

	public static final String TYPE_MACHINE_DETAIL = "machineDetailPatternType_";	// 機械詳細 項目パターン
	public static final String TYPE_DAILY_REPORT = "dailyReportPatternType_";		// 日報詳細

	public static final int LBX_ICON = 8;											// LBX機械アイコンNO

	public static final int ELSE_ICON_TYPE = 2;										// 機械アイコンデフォルト(黄アイコン)
	public static final int ELSE_MACHINE_DETAIL_TYPE = 3;							// パターン3
	public static final int ELSE_DAILY_REPORT_TYPE = 3;								// パターン3
	public static final int ELSE_GRAPH_REPORT_TYPE = 0;								// 施工表示なし

	public static final int ROUTE_GUIDANCE_LOCK_SPEED = 10;							// ルート案内ロック速度

	public static final String AES_KEY = "00bMjsDYqW0qphp8";						// 暗号化共通鍵

	public static final String RM_TARGET_USER_LIST = "rmTargetUserList";			     // リターンマシンユーザ指定公開
	public static final String CONSTRUCTION_TARGET_USER_LIST = "crTargetUserList";	 // 舗装情報ユーザ指定公開


/* ************************************
 * 開発(RASIS)サーバー設定
 *

	public static final String HOST_URL = "http://localhost:14908/GNavApp/"; //ローカルサーバ
//	public static final String HOST_URL = "http://113.33.116.157:14908/GNavApp/"; //RASISサーバアプリ確認用
	public static final String PDF_URL = "http://113.33.116.157:14908/Fileserver/GnavAppFiles/PDF/";
	public static final String APP_VERTION_PATH = "C:/Users/Administrator/Documents/profile11/opt/FILE/GNavApp/app_gnav.properties";
	public static final String APP_LOG_PATH = "/home/profile01/GNavApp/logs";
	public static final String APP_CONFIG_PATH = "C:/Users/Administrator/Documents/profile11/opt/FILE/GNavApp/config_gnav.properties";
	public static final String APP_LOG_MAIL_ADDRESS = "yamashita_m@ideacns.co.jp";
    public static final int GOOGLE_MAP_LOG_FLAG = 1;				// 0:記録しない  1:記録する
	public static final int CON_MAX_COUNT = 1001;					// 最大レコード数
	public static final String FILE_STORAGE_IMAGE_PATH = "/home/profile01/GnavAppFiles/Images/GNavImagesUpload/";
	public static final String UPLOADED_IMAGE_URL = "http://113.33.116.157:14908/Fileserver/GnavAppFiles/Images/GNavImagesUpload/";
	public static final String SW2_HOST_URL = "http://52.198.146.209"; //SW2イデアテスト環境
*/


/* ************************************
 * テストサーバー設定   ※※※使用しない！！※※※
 * GR.SCM.USER.info-gnav@shi-g.com

	public static final String HOST_URL = "https://scmgnavapptest.shi.co.jp/GNavApp/";
	public static final String PDF_URL = "https://scmgnavapptest.shi.co.jp/GnavAppFilesTest/PDF/";
	public static final String UPLOADED_IMAGE_URL = "https://scmgnavapptest.shi.co.jp/GnavAppFilesTest/Images/";

	public static final String APP_VERTION_PATH = "/home/profile11/GNavApp/app_gnav.properties";
	public static final String APP_LOG_PATH = "/home/profile11/GNavApp/logs";
	public static final String APP_CONFIG_PATH = "/home/profile11/GNavApp/config_gnav.properties";
	public static final String APP_LOG_MAIL_ADDRESS = "sumiken.dev.02@outlook.jp";


	public static final int GOOGLE_MAP_LOG_FLAG = 1;				// 0:記録しない  1:記録する
	public static final int CON_MAX_COUNT = 1001;					// 最大レコード数

	public static final String FILE_STORAGE_IMAGE_PATH = "/opt/IBM/HTTPServer85/htdocs/profile11/GnavAppFilesTest/Images/";

*/

/* ************************************
 * 新WAS　テストサーバー設定
 * GR.SCM.USER.info-gnav@shi-g.com

	public static final String HOST_URL           = "https://scmgnavapptest.shi.co.jp/GNavApp/";
	public static final String PDF_URL            = "https://scmgnavapptest.shi.co.jp/GnavAppFilesTest/PDF/";
	public static final String UPLOADED_IMAGE_URL = "https://scmgnavapptest.shi.co.jp/GnavAppFilesTest/Images/";

	public static final String APP_VERTION_PATH = "/home/profile12/GNavApp/app_gnav.properties";
	public static final String APP_LOG_PATH =     "/home/profile12/GNavApp/logs";
	public static final String APP_CONFIG_PATH =  "/home/profile12/GNavApp/config_gnav.properties";
	public static final String APP_LOG_MAIL_ADDRESS = "yamashita_m@ideacns.co.jp";

	public static final int GOOGLE_MAP_LOG_FLAG = 0;				// 0:記録しない  1:記録する
	public static final int CON_MAX_COUNT = 1001;					// 最大レコード数

	public static final String FILE_STORAGE_IMAGE_PATH = "/opt/IBM/HTTPServer/htdocs/profile12/GnavAppFilesTest/Images/";
//	public static final String SW2_HOST_URL = "http://10.90.170.23"; //SW2建機テスト環境
	public static final String SW2_HOST_URL = "https://scmsw2-test-scm.shi.co.jp";

	//アンケートメニューフラグ
	public static final int SURVEY_MENU_FLG = 1;
*/

/* ************************************
 * 新本番設定
 * GR.SCM.USER.info-gnav@shi-g.com

*/
	public static final String HOST_URL = "https://scmgnavApp2.shi.co.jp/GNavApp/";
	public static final String PDF_URL = "https://scmgnavApp2.shi.co.jp/apfile/PDF/";
	public static final String UPLOADED_IMAGE_URL = "https://scmgnavApp2.shi.co.jp/apfile/Images/";


	public static final String APP_VERTION_PATH = "/home/profile13/GNavApp/app_gnav.properties";
	public static final String APP_LOG_PATH = "/home/profile13/GNavApp/logs";
	public static final String APP_CONFIG_PATH = "/home/profile13/GNavApp/config_gnav.properties";
	public static final String APP_LOG_MAIL_ADDRESS = "GR.SCM.USER.GNAV_DEVELOPER@shi-g.com";


	public static final int GOOGLE_MAP_LOG_FLAG = 1;
	public static final int CON_MAX_COUNT = 1001;					// 最大レコード数

	public static final String FILE_STORAGE_IMAGE_PATH = "/opt/IBM/HTTPServer/htdocs/profile13/apfile/Images/";
	public static final String SW2_HOST_URL = "https://scmsw2-scm.shi.co.jp";

	//アンケートメニューフラグ(本番非公開)
	public static final int SURVEY_MENU_FLG = 0;


}
