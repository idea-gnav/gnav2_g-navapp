package com.businet.GNavApp.models;

public class DtcNoticesListModel {
//	private Integer serialNumber;
	private Long serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String customerManagementNo;
	private String alertLv;
	private String hasseiDate;
//	private String fukkyuDate;
	private String warnHistoryContent;
	// **«««@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ «««**//
	private Integer readFlg;
	// **ªªª@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ ªªª**//

	public DtcNoticesListModel() {
	}

	public Long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getLbxSerialNumber() {
		return lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}

	public String getCustomerManagementNo() {
		return customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public String getAlertLv() {
		return alertLv;
	}
	public void setAlertLv(String alertLv) {
		this.alertLv = alertLv;
	}

	public String getHasseiDate() {
		return hasseiDate;
	}
	public void setHasseiDate(String hasseiDate) {
		this.hasseiDate = hasseiDate;
	}

//	public String getFukkyuDate() {
//		return fukkyuDate;
//	}
//	public void setFukkyuDate(String fukkyuDate) {
//		this.fukkyuDate = fukkyuDate;
//	}

	public String getWarnHistoryContent() {
		return warnHistoryContent;
	}
	public void setWarnHistoryContent(String warnHistoryContent) {
		this.warnHistoryContent = warnHistoryContent;
	}

	// **«««@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ «««**//
	public Integer getReadFlg() {
		return readFlg;
	}
	public void setReadFlg(Integer readFlg) {
		this.readFlg = readFlg;
	}
	// **ªªª@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ ªªª**//

}
