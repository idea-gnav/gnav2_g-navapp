package com.businet.GNavApp.models;


public class DtcDetailArrayListModel extends BaseModel {

	private String hasseiDate;
	private String fukkyuDate;
	private String warnHistoryContent;
	private String alertLv;
	private String dtcEventNo;
	private double hourMeter;
	private Integer pdfFlg;

	public String getHasseiDate() {
		return hasseiDate;
	}
	public void setHasseiDate(String hasseiDate) {
		this.hasseiDate = hasseiDate;
	}

	public String getFukkyuDate() {
		return fukkyuDate;
	}
	public void setFukkyuDate(String fukkyuDate) {
		this.fukkyuDate = fukkyuDate;
	}

	public String getWarnHistoryContent() {
		return warnHistoryContent;
	}
	public void setWarnHistoryContent(String warnHistoryContent) {
		this.warnHistoryContent = warnHistoryContent;
	}

	public String getAlertLv() {
		return alertLv;
	}
	public void setAlertLv(String alertLv) {
		this.alertLv = alertLv;
	}

	public String getDtcEventNo() {
		return dtcEventNo;
	}
	public void setDtcEventNo(String dtcEventNo) {
		this.dtcEventNo = dtcEventNo;
	}

	public double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Integer getPdfFlg() {
		return pdfFlg;
	}
	public void setPdfFlg(Integer pdfFlg) {
		this.pdfFlg = pdfFlg;
	}
}
