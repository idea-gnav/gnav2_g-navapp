package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ExpectedServiceModel extends BaseModel {

	private Integer statusCode;
	private String surveyName;
	private Integer targetFigures;
	private Integer achievementFigures;
	private ArrayList<ExpectedServiceListModel> expectedServiceList;


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public Integer getTargetFigures() {
		return targetFigures;
	}
	public void setTargetFigures(Integer targetFigures) {
		this.targetFigures = targetFigures;
	}
	public Integer getAchievementFigures() {
		return achievementFigures;
	}
	public void setAchievementFigures(Integer achievementFigures) {
		this.achievementFigures = achievementFigures;
	}
	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}
	public ArrayList<ExpectedServiceListModel> getExpectedServiceList() {
		return expectedServiceList;
	}
	public void setExpectedServiceList(ArrayList<ExpectedServiceListModel> expectedServiceList) {
		this.expectedServiceList = expectedServiceList;
	}

}
