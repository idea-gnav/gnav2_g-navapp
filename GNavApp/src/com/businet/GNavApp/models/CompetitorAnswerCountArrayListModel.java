package com.businet.GNavApp.models;

public class CompetitorAnswerCountArrayListModel  {

	private String competitorName;
	private Integer competitorAnswerCount;


	public String getcompetitorName() {
		return competitorName;
	}
	public void setCompetitorName(String competitorName) {
		this.competitorName = competitorName;
	}

	public Integer getCompetitorAnswerCount() {
		return competitorAnswerCount;
	}
	public void setCompetitorAnswerCount(Integer competitorAnswerCount) {
		this.competitorAnswerCount = competitorAnswerCount;
	}



}
