package com.businet.GNavApp.models;

import java.util.List;

public class StatisticsInfoModel extends BaseModel{
	
	private List<StatisticsListModel> statisticsList;
	
	public StatisticsInfoModel() {}
	
	public StatisticsInfoModel(Integer statusCode,List<StatisticsListModel> statisticsList) {
		super.setStatusCode(statusCode);
		this.statisticsList = statisticsList;
	}
	
	public void setStatisticsList(List<StatisticsListModel> statisticsList) {
		this.statisticsList = statisticsList;
	}
	public List<StatisticsListModel> getStatisticsList() {
		return statisticsList;
	}
	
	
}