package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ConstructionHistoryListModel extends BaseModel  {

	private Integer statusCode;
	private ArrayList<ConstructionResultModel> constructionResultList;
	private Integer constructionHistoryCount;

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public ArrayList<ConstructionResultModel> getConstructionResultList() {
		return constructionResultList;
	}
	public void setConstructionResultList(ArrayList<ConstructionResultModel> constructionResultList) {
		this.constructionResultList = constructionResultList;
	}
	
	public Integer getConstructionHistoryCount() {
		return constructionHistoryCount;
	}
	public void setConstructionHistoryCount(Integer constructionHistoryCount) {
		this.constructionHistoryCount = constructionHistoryCount;
	}

}
