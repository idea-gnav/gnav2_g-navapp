package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ServiceEvaluationPriorityModel extends BaseModel{

	private Integer statusCode;
	private String surveyName;
	private Integer targetFigures;
	private Integer achievementFigures;
	private ArrayList<ServiceEvaluationPriorityArrayListModel> serviceEvaluationPriorityList;


	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getSurveyName() {
		return surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public Integer getTargetFigures() {
		return targetFigures;
	}

	public void setTargetFigures(Integer targetFigures) {
		this.targetFigures = targetFigures;
	}

	public Integer getAchievementFigures() {
		return achievementFigures;
	}

	public void setAchievementFigures(Integer achievementFigures) {
		this.achievementFigures = achievementFigures;
	}

	public ArrayList<ServiceEvaluationPriorityArrayListModel> getServiceEvaluationPriorityList() {
		return serviceEvaluationPriorityList;
	}

	public void setServiceEvaluationPriorityList(ArrayList<ServiceEvaluationPriorityArrayListModel> serviceEvaluationPriorityList) {
		this.serviceEvaluationPriorityList = serviceEvaluationPriorityList;
	}

}