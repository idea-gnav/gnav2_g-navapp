package com.businet.GNavApp.models;

public class DisplayConditionModel {


	private Integer machineIconType;
	private Integer machineDetailType;
	private Integer dailyReportType;
	private Integer grapheReportFlg;

	public DisplayConditionModel() {
	}


	public Integer getMachineIconType() {
		return machineIconType;
	}
	public void setMachineIconType(Integer machineIconType) {
		this.machineIconType = machineIconType;
	}

	public Integer getMachineDetailType() {
		return machineDetailType;
	}
	public void setMachineDetailType(Integer machineDetailType) {
		this.machineDetailType = machineDetailType;
	}

	public Integer getDailyReportType() {
		return dailyReportType;
	}
	public void setDailyReportType(Integer dailyReportType) {
		this.dailyReportType = dailyReportType;
	}

	public Integer getGrapheReportFlg() {
		return grapheReportFlg;
	}
	public void setGrapheReportFlg(Integer grapheReportFlg) {
		this.grapheReportFlg = grapheReportFlg;
	}

}
