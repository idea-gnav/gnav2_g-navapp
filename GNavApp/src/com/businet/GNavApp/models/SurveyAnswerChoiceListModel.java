package com.businet.GNavApp.models;

public class SurveyAnswerChoiceListModel {
	private Integer answerChoiceId;

	public Integer getAnswerChoiceId() {
		return answerChoiceId;
	}

	public void setAnswerChoiceId(Integer answerChoiceId) {
		this.answerChoiceId = answerChoiceId;
	}
}
