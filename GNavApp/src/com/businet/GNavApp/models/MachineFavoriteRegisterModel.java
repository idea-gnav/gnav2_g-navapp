package com.businet.GNavApp.models;

import java.util.List;

public class MachineFavoriteRegisterModel {

	private String userId;
	private List<MachineFavoriteRegisterArrayListModel> machineFavoriteRegisterList;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<MachineFavoriteRegisterArrayListModel> getMachineFavoriteRegisterList() {
		return machineFavoriteRegisterList;
	}
	public void setMachineFavoriteRegisterList(List<MachineFavoriteRegisterArrayListModel> machineFavoriteRegisterList) {
		this.machineFavoriteRegisterList = machineFavoriteRegisterList;
	}
}
