package com.businet.GNavApp.models;

public class PeriodicInspectionListArrayListModel extends BaseModel {

	private String itemNo;
	private Integer itemId;
	private String item;
	private Integer freeInputFlg;
	private Integer itemAnswerCount;
	private double answerPercentage;


	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}

	public Integer getFreeInputFlg() {
		return freeInputFlg;
	}
	public void setFreeInputFlg(Integer freeInputFlg) {
		this.freeInputFlg = freeInputFlg;
	}

	public Integer getItemAnswerCount() {
		return itemAnswerCount;
	}
	public void setItemAnswerCount(Integer itemAnswerCount) {
		this.itemAnswerCount = itemAnswerCount;
	}

	public double getAnswerPercentage() {
		return answerPercentage;
	}
	public void setAnswerPercentage(double answerPercentage) {
		this.answerPercentage = answerPercentage;
	}



}
