package com.businet.GNavApp.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserStatusModel {
	private Integer statusCode;
	private Integer unitSelect;
	private Integer kibanSelect;
	// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
	private Integer customerNamePriority;
	// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//
	private Integer pdfFlg;
	private Integer appLogFlg;
	private Integer searchRangeDistance;

	private List<String> scmModelList;
	private Integer dealerNameFlg;
	private Integer customerNameFlg;
	private Integer coustmerInfoFlg;
	private ArrayList<HashMap<String,String>> toukatsubuList;
	private ArrayList<HashMap<String,String>> kyotenList;
	private ArrayList<HashMap<String,String>> dealerList;
	private ArrayList<HashMap<String,String>> serviceKoujyouList;
	private ArrayList<HashMap<String,Object>> machineIconList;

	// **↓↓↓ 2020/02/05 RASIS DucNKT 発生中警報レベルアイコン区分一覧 外部ファイル参照化のため修正 ↓↓↓**//
	private ArrayList<HashMap<String,Object>> alertLevelIconList;
	// **↑↑↑ 2020/02/05 RASIS DucNKT 発生中警報レベルアイコン区分一覧 外部ファイル参照化のため修正 ↑↑↑**//


	// **↓↓↓ 2019/06/25 iDEA山下 リターンマシン,アンケート メニュー制御↓↓↓**//
	private Integer returnMachineFlg;
	private Integer surveyFlg;
	// **↑↑↑ 2019/06/25 iDEA山下 リターンマシン,アンケート メニュー制御↑↑↑**//
	// **↓↓↓ 2019/11/13 RASIS岡本 施工情報 メニュー制御↓↓↓**//
	private Integer constructionFlg;
	// **↑↑↑ 2019/11/13 RASIS岡本 施工情報 メニュー制御↑↑↑**//
	// **↓↓↓ 2019/11/01 ルート案内ロック用の速度↓↓↓**//
	private Integer routeGuidanceLockSpeed;
	// **↑↑↑ 2019/11/01 ルート案内ロック用の速度↑↑↑**//

	// **↓↓↓ 2020/08/07 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//
	private String gnavUserId;
	private Integer sw2UserNumber;
	private String sw2UserId;
	private Integer sw2AppLogFlg;
	private String sw2Token;
	private Integer sw2ReportFlg;
	private Integer sw2SurveyStatisticsFlg;
	// **↑↑↑ 2020/08/07 iDEA山下 SW2ユーザ認証追加 ↑↑↑**//



	public UserStatusModel(){
	}

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getUnitSelect() {
		return unitSelect;
	}
	public void setUnitSelect(Integer unitSelect) {
		this.unitSelect = unitSelect;
	}

	public Integer getKibanSelect() {
		return kibanSelect;
	}
	public void setKibanSelect(Integer kibanSelect) {
		this.kibanSelect = kibanSelect;
	}

	// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
	public Integer getCustomerNamePriority() {
		return customerNamePriority;
	}
	public void setCustomerNamePriority(Integer customerNamePriority) {
		this.customerNamePriority = customerNamePriority;
	}
	// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//

	public Integer getPdfFlg() {
		return pdfFlg;
	}
	public void setPdfFlg(Integer pdfFlg) {
		this.pdfFlg = pdfFlg;
	}

	public Integer getAppLogFlg() {
		return appLogFlg;
	}
	public void setAppLogFlg(Integer appLogFlg) {
		this.appLogFlg = appLogFlg;
	}

	public Integer getSearchRangeDistance() {
		return searchRangeDistance;
	}
	public void setSearchRangeDistance(Integer searchRangeDistance) {
		this.searchRangeDistance = searchRangeDistance;
	}


	public List<String> getScmModelList() {
		return scmModelList;
	}
	public void setScmModelList(List<String> scmModelList) {
		this.scmModelList = scmModelList;
	}

	public Integer getDealerNameFlg() {
		return dealerNameFlg;
	}
	public void setDealerNameFlg(Integer dealerNameFlg) {
		this.dealerNameFlg = dealerNameFlg;
	}

	public Integer getCustomerNameFlg() {
		return customerNameFlg;
	}
	public void setCustomerNameFlg(Integer customerNameFlg) {
		this.customerNameFlg = customerNameFlg;
	}

	public Integer getCoustmerInfoFlg() {
		return coustmerInfoFlg;
	}
	public void setCoustmerInfoFlg(Integer coustmerInfoFlg) {
		this.coustmerInfoFlg = coustmerInfoFlg;
	}

	public ArrayList<HashMap<String, String>> getToukatsubuList() {
		return toukatsubuList;
	}
	public void setToukatsubuList(ArrayList<HashMap<String, String>> toukatsubuList) {
		this.toukatsubuList = toukatsubuList;
	}

	public ArrayList<HashMap<String, String>> getKyotenList() {
		return kyotenList;
	}
	public void setKyotenList(ArrayList<HashMap<String, String>> kyotenList) {
		this.kyotenList = kyotenList;
	}

	public ArrayList<HashMap<String, String>> getDealerList() {
		return dealerList;
	}
	public void setDealerList(ArrayList<HashMap<String, String>> dealerList) {
		this.dealerList = dealerList;
	}

	public ArrayList<HashMap<String, String>> getServiceKoujyouList() {
		return serviceKoujyouList;
	}
	public void setServiceKoujyouList(ArrayList<HashMap<String, String>> serviceKoujyouList) {
		this.serviceKoujyouList = serviceKoujyouList;
	}

	public ArrayList<HashMap<String, Object>> getMachineIconList() {
		return machineIconList;
	}
	public void setMachineIconList(ArrayList<HashMap<String, Object>> machineIconList) {
		this.machineIconList = machineIconList;
	}

	// **↓↓↓ 2019/06/25 iDEA山下 リターンマシン,アンケート メニュー制御↓↓↓**//
	public Integer getReturnMachineFlg() {
		return returnMachineFlg;
	}
	public void setReturnMachineFlg(Integer returnMachineFlg) {
		this.returnMachineFlg = returnMachineFlg;
	}

	public Integer getSurveyFlg() {
		return surveyFlg;
	}
	public void setSurveyFlg(Integer surveyFlg) {
		this.surveyFlg = surveyFlg;
	}
	// **↑↑↑ 2019/06/25 iDEA山下 リターンマシン,アンケート メニュー制御↑↑↑**//
	// **↓↓↓ 2019/11/13 RASIS岡本 施工情報 メニュー制御↓↓↓**//
	public Integer getConstructionFlg() {
		return constructionFlg;
	}
	public void setConstructionFlg(Integer constructionFlg) {
		this.constructionFlg = constructionFlg;
	}
	// **↑↑↑ 2019/11/13 RASIS岡本 施工情報 メニュー制御↑↑↑**//


	// **↓↓↓ 2019/11/01 ルート案内ロック用の速度↓↓↓**//
	public Integer getRouteGuidanceLockSpeed() {
		return routeGuidanceLockSpeed;
	}
	public void setRouteGuidanceLockSpeed(Integer routeGuidanceLockSpeed) {
		this.routeGuidanceLockSpeed = routeGuidanceLockSpeed;
	}
	// **↑↑↑ 2019/11/01 ルート案内ロック用の速度↑↑↑**//


	// **↓↓↓ 2020/02/05 RASIS DucNKT 発生中警報レベルアイコン区分一覧 外部ファイル参照化のため修正 ↓↓↓**//
	public ArrayList<HashMap<String,Object>> getAlertLevelIconList() {
		return alertLevelIconList;
	}

	public void setAlertLevelIconList(ArrayList<HashMap<String,Object>> alertLevelIconList) {
		this.alertLevelIconList = alertLevelIconList;
	}
	// **↑↑↑ 2020/02/05 RASIS DucNKT 発生中警報レベルアイコン区分一覧 外部ファイル参照化のため修正 ↑↑↑**//

	// **↓↓↓ 2020/08/07 iDEA山下 SW2ユーザ認証追加 ↓↓↓**//
	public String getGnavUserId() {
		return gnavUserId;
	}
	public void setGnavUserId(String gnavUserId) {
		this.gnavUserId = gnavUserId;
	}

	public Integer getSw2UserNumber() {
		return sw2UserNumber;
	}
	public void setSw2UserNumber(Integer sw2UserNumber) {
		this.sw2UserNumber = sw2UserNumber;
	}

	public String getSw2UserId() {
		return sw2UserId;
	}
	public void setSw2UserId(String sw2UserId) {
		this.sw2UserId = sw2UserId;
	}

	public Integer getSw2AppLogFlg() {
		return sw2AppLogFlg;
	}
	public void setSw2AppLogFlg(Integer sw2AppLogFlg) {
		this.sw2AppLogFlg = sw2AppLogFlg;
	}

	public String getSw2Token() {
		return sw2Token;
	}
	public void setSw2Token(String sw2Token) {
		this.sw2Token = sw2Token;
	}

	public Integer getSw2ReportFlg() {
		return sw2ReportFlg;
	}
	public void setSw2ReportFlg(Integer sw2ReportFlg) {
		this.sw2ReportFlg = sw2ReportFlg;
	}

	public Integer getSw2SurveyStatisticsFlg() {
		return sw2SurveyStatisticsFlg;
	}
	public void setSw2SurveyStatisticsFlg(Integer sw2SurveyStatisticsFlg) {
		this.sw2SurveyStatisticsFlg = sw2SurveyStatisticsFlg;
	}





	// **↑↑↑ 2020/08/07 iDEA山下 SW2ユーザ認証追加 ↑↑↑**//

}
