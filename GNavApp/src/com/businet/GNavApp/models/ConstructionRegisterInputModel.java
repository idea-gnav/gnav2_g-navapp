package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ConstructionRegisterInputModel {
	private String userId;
	private Integer deviceLanguage;
	private Integer reportDelFlg;
	private Integer reportNo;
	private Long serialNumber;
	private Double hourMeter;
	private String researcher;
	private String researchDate;
	private String researchLocation;
	private Double workVolume;
	private Integer workVolumeUnit;
	private Integer reportCountry;
	private Integer materialType;
	private Integer materialTemperatureUse;
	private Double materialTemperature;
	private Integer materialTemperatureUnit;
	private Double outsideAirTemperature;
	private Integer outsideAirTemperatureUnit;
	private Double plateTemperature;
	private Integer plateTemperatureUnit;
	private Integer workType;
	private String spotSituation;
	private Integer pavementDistanceUse;
	private Double pavementDistance;
	private Double pavementWidth;
	private Double pavementWidthTo;//2020.01.14 DucNKT added
	private Double pavementThicknessLevelingL;
	private Double pavementThicknessLevelingC;
	private Double pavementThicknessLevelingR;
	private Double constructionSpeed;
	private Double levelingCylinderScaleL;
	private Double levelingCylinderScaleR;
	private Double manualSixnessScaleL;
	private Double manualSixnessScaleR;
	private Integer manualSixnessScaleUnit;
	private Double stepAdjustmentScaleL;
	private Double stepAdjustmentScaleR;
	private Double crown;
	private Double slopeCrownL;
	private Double slopeCrownR;
	private Double stretchMoldBoardHeightL;
	private Double stretchMoldBoardHeightR;
	private Integer tampaRotationsUse;
	private Double tampaRotations;
	private Integer tampaAutoFunction;
	private Double vibratorRotations;
	private Double screwHeight;
	private Double screwSpeedScaleL;
	private Double screwSpeedScaleR;
	private Double conveyorSpeedScaleL;
	private Double conveyorSpeedScaleR;
	private Integer screwFlowControlUse;
	private Integer agcUse;
	private Integer agcType;
	private Integer extensionScrewUse;
	private String extensionScrew;
	private Integer pavementTrouble1;
	private Integer pavementTrouble2;
	private Integer pavementTrouble3;
	private Integer pavementTrouble4;
	private Integer pavementTrouble5;
	private Integer pavementTrouble6;
	private Integer pavementTrouble7;
	private Integer pavementTrouble8;
	private Integer pavementTrouble9;
	private Integer pavementTrouble10;
	private Integer pavementTrouble11;
	private Integer pavementTrouble12;
	private Integer pavementTrouble13;
	private Integer pavementTrouble14;
	private Double tonnageMixedMaterials;
	private String customerEvaluation;
	private Double rearSlopeL;
	private Double rearSlopeR;
	private Double rearAttackShrinkL;
	private Double rearAttackShrinkR;
	private Double rearAttackMiddleL;
	private Double rearAttackMiddleR;
	private Double rearAttackStretchL;
	private Double rearAttackStretchR;
	private ArrayList<ConstructionRegistorPhotoListModel> photoList;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getDeviceLanguage() {
		return deviceLanguage;
	}
	public void setDeviceLanguage(Integer deviceLanguage) {
		this.deviceLanguage = deviceLanguage;
	}
	public Integer getReportDelFlg() {
		return reportDelFlg;
	}
	public void setReportDelFlg(Integer reportDelFlg) {
		this.reportDelFlg = reportDelFlg;
	}
	public Integer getReportNo() {
		return reportNo;
	}
	public void setReportNo(Integer reportNo) {
		this.reportNo = reportNo;
	}
	public Long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}
	public String getResearcher() {
		return researcher;
	}
	public void setResearcher(String researcher) {
		this.researcher = researcher;
	}
	public String getResearchDate() {
		return researchDate;
	}
	public void setResearchDate(String researchDate) {
		this.researchDate = researchDate;
	}
	public String getResearchLocation() {
		return researchLocation;
	}
	public void setResearchLocation(String researchLocation) {
		this.researchLocation = researchLocation;
	}
	public Double getWorkVolume() {
		return workVolume;
	}
	public void setWorkVolume(Double workVolume) {
		this.workVolume = workVolume;
	}
	public Integer getWorkVolumeUnit() {
		return workVolumeUnit;
	}
	public void setWorkVolumeUnit(Integer workVolumeUnit) {
		this.workVolumeUnit = workVolumeUnit;
	}
	public Integer getReportCountry() {
		return reportCountry;
	}
	public void setReportCountry(Integer reportCountry) {
		this.reportCountry = reportCountry;
	}
	public Integer getMaterialType() {
		return materialType;
	}
	public void setMaterialType(Integer materialType) {
		this.materialType = materialType;
	}
	public Integer getMaterialTemperatureUse() {
		return materialTemperatureUse;
	}
	public void setMaterialTemperatureUse(Integer materialTemperatureUse) {
		this.materialTemperatureUse = materialTemperatureUse;
	}
	public Double getMaterialTemperature() {
		return materialTemperature;
	}
	public void setMaterialTemperature(Double materialTemperature) {
		this.materialTemperature = materialTemperature;
	}
	public Integer getMaterialTemperatureUnit() {
		return materialTemperatureUnit;
	}
	public void setMaterialTemperatureUnit(Integer materialTemperatureUnit) {
		this.materialTemperatureUnit = materialTemperatureUnit;
	}
	public Double getOutsideAirTemperature() {
		return outsideAirTemperature;
	}
	public void setOutsideAirTemperature(Double outsideAirTemperature) {
		this.outsideAirTemperature = outsideAirTemperature;
	}
	public Integer getOutsideAirTemperatureUnit() {
		return outsideAirTemperatureUnit;
	}
	public void setOutsideAirTemperatureUnit(Integer outsideAirTemperatureUnit) {
		this.outsideAirTemperatureUnit = outsideAirTemperatureUnit;
	}
	public Double getPlateTemperature() {
		return plateTemperature;
	}
	public void setPlateTemperature(Double plateTemperature) {
		this.plateTemperature = plateTemperature;
	}
	public Integer getPlateTemperatureUnit() {
		return plateTemperatureUnit;
	}
	public void setPlateTemperatureUnit(Integer plateTemperatureUnit) {
		this.plateTemperatureUnit = plateTemperatureUnit;
	}
	public Integer getWorkType() {
		return workType;
	}
	public void setWorkType(Integer workType) {
		this.workType = workType;
	}
	public String getSpotSituation() {
		return spotSituation;
	}
	public void setSpotSituation(String spotSituation) {
		this.spotSituation = spotSituation;
	}
	public Integer getPavementDistanceUse() {
		return pavementDistanceUse;
	}
	public void setPavementDistanceUse(Integer pavementDistanceUse) {
		this.pavementDistanceUse = pavementDistanceUse;
	}
	public Double getPavementDistance() {
		return pavementDistance;
	}
	public void setPavementDistance(Double pavementDistance) {
		this.pavementDistance = pavementDistance;
	}
	public Double getPavementWidth() {
		return pavementWidth;
	}
	public void setPavementWidth(Double pavementWidth) {
		this.pavementWidth = pavementWidth;
	}
	public Double getPavementThicknessLevelingL() {
		return pavementThicknessLevelingL;
	}
	public void setPavementThicknessLevelingL(Double pavementThicknessLevelingL) {
		this.pavementThicknessLevelingL = pavementThicknessLevelingL;
	}
	public Double getPavementThicknessLevelingC() {
		return pavementThicknessLevelingC;
	}
	public void setPavementThicknessLevelingC(Double pavementThicknessLevelingC) {
		this.pavementThicknessLevelingC = pavementThicknessLevelingC;
	}
	public Double getPavementThicknessLevelingR() {
		return pavementThicknessLevelingR;
	}
	public void setPavementThicknessLevelingR(Double pavementThicknessLevelingR) {
		this.pavementThicknessLevelingR = pavementThicknessLevelingR;
	}
	public Double getConstructionSpeed() {
		return constructionSpeed;
	}
	public void setConstructionSpeed(Double constructionSpeed) {
		this.constructionSpeed = constructionSpeed;
	}
	public Double getLevelingCylinderScaleL() {
		return levelingCylinderScaleL;
	}
	public void setLevelingCylinderScaleL(Double levelingCylinderScaleL) {
		this.levelingCylinderScaleL = levelingCylinderScaleL;
	}
	public Double getLevelingCylinderScaleR() {
		return levelingCylinderScaleR;
	}
	public void setLevelingCylinderScaleR(Double levelingCylinderScaleR) {
		this.levelingCylinderScaleR = levelingCylinderScaleR;
	}
	public Double getManualSixnessScaleL() {
		return manualSixnessScaleL;
	}
	public void setManualSixnessScaleL(Double manualSixnessScaleL) {
		this.manualSixnessScaleL = manualSixnessScaleL;
	}
	public Double getManualSixnessScaleR() {
		return manualSixnessScaleR;
	}
	public void setManualSixnessScaleR(Double manualSixnessScaleR) {
		this.manualSixnessScaleR = manualSixnessScaleR;
	}
	public Integer getManualSixnessScaleUnit() {
		return manualSixnessScaleUnit;
	}
	public void setManualSixnessScaleUnit(Integer manualSixnessScaleUnit) {
		this.manualSixnessScaleUnit = manualSixnessScaleUnit;
	}
	public Double getStepAdjustmentScaleL() {
		return stepAdjustmentScaleL;
	}
	public void setStepAdjustmentScaleL(Double stepAdjustmentScaleL) {
		this.stepAdjustmentScaleL = stepAdjustmentScaleL;
	}
	public Double getStepAdjustmentScaleR() {
		return stepAdjustmentScaleR;
	}
	public void setStepAdjustmentScaleR(Double stepAdjustmentScaleR) {
		this.stepAdjustmentScaleR = stepAdjustmentScaleR;
	}
	public Double getCrown() {
		return crown;
	}
	public void setCrown(Double crown) {
		this.crown = crown;
	}
	public Double getSlopeCrownL() {
		return slopeCrownL;
	}
	public void setSlopeCrownL(Double slopeCrownL) {
		this.slopeCrownL = slopeCrownL;
	}
	public Double getSlopeCrownR() {
		return slopeCrownR;
	}
	public void setSlopeCrownR(Double slopeCrownR) {
		this.slopeCrownR = slopeCrownR;
	}
	public Double getStretchMoldBoardHeightL() {
		return stretchMoldBoardHeightL;
	}
	public void setStretchMoldBoardHeightL(Double stretchMoldBoardHeightL) {
		this.stretchMoldBoardHeightL = stretchMoldBoardHeightL;
	}
	public Double getStretchMoldBoardHeightR() {
		return stretchMoldBoardHeightR;
	}
	public void setStretchMoldBoardHeightR(Double stretchMoldBoardHeightR) {
		this.stretchMoldBoardHeightR = stretchMoldBoardHeightR;
	}
	public Integer getTampaRotationsUse() {
		return tampaRotationsUse;
	}
	public void setTampaRotationsUse(Integer tampaRotationsUse) {
		this.tampaRotationsUse = tampaRotationsUse;
	}
	public Double getTampaRotations() {
		return tampaRotations;
	}
	public void setTampaRotations(Double tampaRotations) {
		this.tampaRotations = tampaRotations;
	}
	public Integer getTampaAutoFunction() {
		return tampaAutoFunction;
	}
	public void setTampaAutoFunction(Integer tampaAutoFunction) {
		this.tampaAutoFunction = tampaAutoFunction;
	}
	public Double getVibratorRotations() {
		return vibratorRotations;
	}
	public void setVibratorRotations(Double vibratorRotations) {
		this.vibratorRotations = vibratorRotations;
	}
	public Double getScrewHeight() {
		return screwHeight;
	}
	public void setScrewHeight(Double screwHeight) {
		this.screwHeight = screwHeight;
	}
	public Double getScrewSpeedScaleL() {
		return screwSpeedScaleL;
	}
	public void setScrewSpeedScaleL(Double screwSpeedScaleL) {
		this.screwSpeedScaleL = screwSpeedScaleL;
	}
	public Double getScrewSpeedScaleR() {
		return screwSpeedScaleR;
	}
	public void setScrewSpeedScaleR(Double screwSpeedScaleR) {
		this.screwSpeedScaleR = screwSpeedScaleR;
	}
	public Double getConveyorSpeedScaleL() {
		return conveyorSpeedScaleL;
	}
	public void setConveyorSpeedScaleL(Double conveyorSpeedScaleL) {
		this.conveyorSpeedScaleL = conveyorSpeedScaleL;
	}
	public Double getConveyorSpeedScaleR() {
		return conveyorSpeedScaleR;
	}
	public void setConveyorSpeedScaleR(Double conveyorSpeedScaleR) {
		this.conveyorSpeedScaleR = conveyorSpeedScaleR;
	}
	public Integer getScrewFlowControlUse() {
		return screwFlowControlUse;
	}
	public void setScrewFlowControlUse(Integer screwFlowControlUse) {
		this.screwFlowControlUse = screwFlowControlUse;
	}
	public Integer getAgcUse() {
		return agcUse;
	}
	public void setAgcUse(Integer agcUse) {
		this.agcUse = agcUse;
	}
	public Integer getAgcType() {
		return agcType;
	}
	public void setAgcType(Integer agcType) {
		this.agcType = agcType;
	}
	public Integer getExtensionScrewUse() {
		return extensionScrewUse;
	}
	public void setExtensionScrewUse(Integer extensionScrewUse) {
		this.extensionScrewUse = extensionScrewUse;
	}
	public String getExtensionScrew() {
		return extensionScrew;
	}
	public void setExtensionScrew(String extensionScrew) {
		this.extensionScrew = extensionScrew;
	}
	public Integer getPavementTrouble1() {
		return pavementTrouble1;
	}
	public void setPavementTrouble1(Integer pavementTrouble1) {
		this.pavementTrouble1 = pavementTrouble1;
	}
	public Integer getPavementTrouble2() {
		return pavementTrouble2;
	}
	public void setPavementTrouble2(Integer pavementTrouble2) {
		this.pavementTrouble2 = pavementTrouble2;
	}
	public Integer getPavementTrouble3() {
		return pavementTrouble3;
	}
	public void setPavementTrouble3(Integer pavementTrouble3) {
		this.pavementTrouble3 = pavementTrouble3;
	}
	public Integer getPavementTrouble4() {
		return pavementTrouble4;
	}
	public void setPavementTrouble4(Integer pavementTrouble4) {
		this.pavementTrouble4 = pavementTrouble4;
	}
	public Integer getPavementTrouble5() {
		return pavementTrouble5;
	}
	public void setPavementTrouble5(Integer pavementTrouble5) {
		this.pavementTrouble5 = pavementTrouble5;
	}
	public Integer getPavementTrouble6() {
		return pavementTrouble6;
	}
	public void setPavementTrouble6(Integer pavementTrouble6) {
		this.pavementTrouble6 = pavementTrouble6;
	}
	public Integer getPavementTrouble7() {
		return pavementTrouble7;
	}
	public void setPavementTrouble7(Integer pavementTrouble7) {
		this.pavementTrouble7 = pavementTrouble7;
	}
	public Integer getPavementTrouble8() {
		return pavementTrouble8;
	}
	public void setPavementTrouble8(Integer pavementTrouble8) {
		this.pavementTrouble8 = pavementTrouble8;
	}
	public Integer getPavementTrouble9() {
		return pavementTrouble9;
	}
	public void setPavementTrouble9(Integer pavementTrouble9) {
		this.pavementTrouble9 = pavementTrouble9;
	}
	public Integer getPavementTrouble10() {
		return pavementTrouble10;
	}
	public void setPavementTrouble10(Integer pavementTrouble10) {
		this.pavementTrouble10 = pavementTrouble10;
	}
	public Integer getPavementTrouble11() {
		return pavementTrouble11;
	}
	public void setPavementTrouble11(Integer pavementTrouble11) {
		this.pavementTrouble11 = pavementTrouble11;
	}
	public Integer getPavementTrouble12() {
		return pavementTrouble12;
	}
	public void setPavementTrouble12(Integer pavementTrouble12) {
		this.pavementTrouble12 = pavementTrouble12;
	}
	public Integer getPavementTrouble13() {
		return pavementTrouble13;
	}
	public void setPavementTrouble13(Integer pavementTrouble13) {
		this.pavementTrouble13 = pavementTrouble13;
	}
	public Integer getPavementTrouble14() {
		return pavementTrouble14;
	}
	public void setPavementTrouble14(Integer pavementTrouble14) {
		this.pavementTrouble14 = pavementTrouble14;
	}
	public Double getTonnageMixedMaterials() {
		return tonnageMixedMaterials;
	}
	public void setTonnageMixedMaterials(Double tonnageMixedMaterials) {
		this.tonnageMixedMaterials = tonnageMixedMaterials;
	}
	public String getCustomerEvaluation() {
		return customerEvaluation;
	}
	public void setCustomerEvaluation(String customerEvaluation) {
		this.customerEvaluation = customerEvaluation;
	}
	public Double getRearSlopeL() {
		return rearSlopeL;
	}
	public void setRearSlopeL(Double rearSlopeL) {
		this.rearSlopeL = rearSlopeL;
	}
	public Double getRearSlopeR() {
		return rearSlopeR;
	}
	public void setRearSlopeR(Double rearSlopeR) {
		this.rearSlopeR = rearSlopeR;
	}
	public Double getRearAttackShrinkL() {
		return rearAttackShrinkL;
	}
	public void setRearAttackShrinkL(Double rearAttackShrinkL) {
		this.rearAttackShrinkL = rearAttackShrinkL;
	}
	public Double getRearAttackShrinkR() {
		return rearAttackShrinkR;
	}
	public void setRearAttackShrinkR(Double rearAttackShrinkR) {
		this.rearAttackShrinkR = rearAttackShrinkR;
	}
	public Double getRearAttackMiddleL() {
		return rearAttackMiddleL;
	}
	public void setRearAttackMiddleL(Double rearAttackMiddleL) {
		this.rearAttackMiddleL = rearAttackMiddleL;
	}
	public Double getRearAttackMiddleR() {
		return rearAttackMiddleR;
	}
	public void setRearAttackMiddleR(Double rearAttackMiddleR) {
		this.rearAttackMiddleR = rearAttackMiddleR;
	}
	public Double getRearAttackStretchL() {
		return rearAttackStretchL;
	}
	public void setRearAttackStretchL(Double rearAttackStretchL) {
		this.rearAttackStretchL = rearAttackStretchL;
	}
	public Double getRearAttackStretchR() {
		return rearAttackStretchR;
	}
	public void setRearAttackStretchR(Double rearAttackStretchR) {
		this.rearAttackStretchR = rearAttackStretchR;
	}
	public ArrayList<ConstructionRegistorPhotoListModel> getPhotoList() {
		return photoList;
	}
	public void setPhotoList(ArrayList<ConstructionRegistorPhotoListModel> photoList) {
		this.photoList = photoList;
	}
	public Double getPavementWidthTo() {
		return pavementWidthTo;
	}
	public void setPavementWidthTo(Double pavementWidthTo) {
		this.pavementWidthTo = pavementWidthTo;
	}
}
