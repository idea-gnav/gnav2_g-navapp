package com.businet.GNavApp.models;

import java.util.ArrayList;

public class SurveyFreeInputListModel extends BaseModel{

	private Integer statusCode;
	private Integer questionNo;
	private String questionName;
	private ArrayList<SurveyFreeInputTextListModel> freeInputList;
	private Integer freeInputCount;


	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(Integer questionNo) {
		this.questionNo = questionNo;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public ArrayList<SurveyFreeInputTextListModel> getFreeInputList() {
		return freeInputList;
	}

	public void setFreeInputList(ArrayList<SurveyFreeInputTextListModel> freeInputList) {
		this.freeInputList = freeInputList;
	}

	public Integer getFreeInputCount() {
		return freeInputCount;
	}

	public void setFreeInputCount(Integer freeInputCount) {
		this.freeInputCount = freeInputCount;
	}







}