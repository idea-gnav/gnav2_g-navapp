package com.businet.GNavApp.models;

public class ConstructionWorkTypeModel {
	private Integer workTypeCountryId;
	private Integer workTypeId;
	private String workType;
	
	public Integer getWorkTypeCountryId() {
		return workTypeCountryId;
	}
	public void setWorkTypeCountryId(Integer workTypeCountryId) {
		this.workTypeCountryId = workTypeCountryId;
	}
	public Integer getWorkTypeId() {
		return workTypeId;
	}
	public void setWorkTypeId(Integer workTypeId) {
		this.workTypeId = workTypeId;
	}
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
}
