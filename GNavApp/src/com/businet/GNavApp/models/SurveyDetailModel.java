package com.businet.GNavApp.models;

import java.util.ArrayList;

public class SurveyDetailModel extends BaseModel {

	private String kigyoName;
	private String answerUser;
	private String position;
	private Double hourMeter;
	private Integer editableFlg;
	private ArrayList<SurveyChoiceModel> positionChoiceList;
	private String surveyDate;
	private ArrayList<SurveyQuestionModel> questionList;
	private Integer questionCount;


	public String getKigyoName() {
		return kigyoName;
	}

	public void setKigyoName(String kigyoName) {
		this.kigyoName = kigyoName;
	}

	public String getAnswerUser() {
		return answerUser;
	}

	public void setAnswerUser(String answerUser) {
		this.answerUser = answerUser;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public ArrayList<SurveyChoiceModel> getPositionChoiceList() {
		return positionChoiceList;
	}

	public void setPositionChoiceList(ArrayList<SurveyChoiceModel> positionChoiceList) {
		this.positionChoiceList = positionChoiceList;
	}

	public String getSurveyDate() {
		return surveyDate;
	}

	public void setSurveyDate(String surveyDate) {
		this.surveyDate = surveyDate;
	}

	public ArrayList<SurveyQuestionModel> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(ArrayList<SurveyQuestionModel> questionList) {
		this.questionList = questionList;
	}

	public Integer getQuestionCount() {
		return questionCount;
	}

	public void setQuestionCount(Integer questionCount) {
		this.questionCount = questionCount;
	}

	public Double getHourMeter() {
		return hourMeter;
	}

	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Integer getEditableFlg() {
		return editableFlg;
	}

	public void setEditableFlg(Integer editableFlg) {
		this.editableFlg = editableFlg;
	}

}
