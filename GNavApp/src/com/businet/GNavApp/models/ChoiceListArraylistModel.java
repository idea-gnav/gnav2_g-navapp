package com.businet.GNavApp.models;

public class ChoiceListArraylistModel {

	private String choiceName;
	private Integer summaryCount;
	private Integer choicePercentage;

	public String getChoiceName() {
		return choiceName;
	}
	public void setChoiceName(String choiceName) {
		this.choiceName = choiceName;
	}
	public Integer getSummaryCount() {
		return summaryCount;
	}
	public void setSummaryCount(Integer summaryCount) {
		this.summaryCount = summaryCount;
	}	
	public Integer getChoicePercentage() {
		return choicePercentage;
	}
	public void setChoicePercentage(Integer choicePercentage) {
		this.choicePercentage = choicePercentage;
	}	
}
