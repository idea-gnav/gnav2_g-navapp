package com.businet.GNavApp.models;

public class SurveyStatisticsArraylistModel {

	private Integer surveyId;
	private String surveyName;
	private String inputTo;
	private Integer targetFigures;
	private Integer achievementFigures;
	private Integer achievementRate;
	
	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}

	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public String getInputTo() {
		return inputTo;
	}
	public void setInputTo(String inputTo) {
		this.inputTo = inputTo;
	}

	public Integer getTargetFigures() {
		return targetFigures;
	}
	public void setTargetFigures(Integer targetFigures) {
		this.targetFigures = targetFigures;
	}

	public Integer getAchievementFigures() {
		return achievementFigures;
	}
	public void setAchievementFigures(Integer achievementFigures) {
		this.achievementFigures = achievementFigures;
	}

	public Integer getAchievementRate() {
		return achievementRate;
	}
	public void setAchievementRate(Integer achievementRate) {
		this.achievementRate = achievementRate;
	}

}
