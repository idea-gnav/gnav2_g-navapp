package com.businet.GNavApp.models;

import java.util.ArrayList;

import com.businet.GNavApp.models.DtcArrayListModel;

public class DtcListModel extends BaseModel {

	private Integer statusCode;  
	private ArrayList<DtcArrayListModel> dtcList;
	private Integer machineCount;
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public ArrayList<DtcArrayListModel> getDtcList() {
		return dtcList;
	}
	public void setDtcList(ArrayList<DtcArrayListModel> dtcList) {
		this.dtcList = dtcList;
	}
	
	public Integer getMachineCount() {
		return machineCount;
	}
	public void setMachineCount(Integer machineCount) {
		this.machineCount = machineCount;
	}
}
