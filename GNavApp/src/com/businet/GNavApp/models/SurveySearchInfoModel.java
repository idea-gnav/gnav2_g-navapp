package com.businet.GNavApp.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SurveySearchInfoModel extends BaseModel {
	private Integer statusCode;
	private ArrayList<HashMap<String,String>> dealerList;
	private ArrayList<ModelTypeListArraylistModel> modelTypeList;
	private ArrayList<HashMap<String,String>> applivableUsesList;
	private List<String> yearList;
	
	

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public ArrayList<HashMap<String, String>> getDealerList() {
		return dealerList;
	}
	public void setDealerList(ArrayList<HashMap<String, String>> dealerList) {
		this.dealerList = dealerList;
	}		
	public ArrayList<ModelTypeListArraylistModel> getModelTypeList() {
		return modelTypeList;
	}
	public void setModelTypeList(ArrayList<ModelTypeListArraylistModel> modelTypeList) {
		this.modelTypeList = modelTypeList;
	}
	public ArrayList<HashMap<String, String>> getApplivableUsesList() {
		return applivableUsesList;
	}
	public void setApplivableUsesList(ArrayList<HashMap<String, String>> applivableUsesList) {
		this.applivableUsesList = applivableUsesList;
	}
	public List<String> getYearList() {
		return yearList;
	}
	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

}
