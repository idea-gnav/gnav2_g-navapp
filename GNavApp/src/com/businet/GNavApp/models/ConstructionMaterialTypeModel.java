package com.businet.GNavApp.models;

public class ConstructionMaterialTypeModel {
	private Integer materialTypeCountryId;
	private Integer materialTypeId;
	private String materialType;
	
	public Integer getMaterialTypeCountryId() {
		return materialTypeCountryId;
	}
	public void setMaterialTypeCountryId(Integer materialTypeCountryId) {
		this.materialTypeCountryId = materialTypeCountryId;
	}
	public Integer getMaterialTypeId() {
		return materialTypeId;
	}
	public void setMaterialTypeId(Integer materialTypeId) {
		this.materialTypeId = materialTypeId;
	}
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
}
