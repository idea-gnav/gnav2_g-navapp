package com.businet.GNavApp.models;

import java.util.ArrayList;

public class MachineListModel extends BaseModel {

	private Integer statusCode;
	private ArrayList<MachineArrayListModel> machineList;
	private Integer machineCount;
	private Double searchRangeFromIdo;
	private Double searchRangeFromKeido;

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public ArrayList<MachineArrayListModel> getMachineList() {
		return machineList;
	}
	public void setMachineList(ArrayList<MachineArrayListModel> machineList) {
		this.machineList = machineList;
	}

	public Integer getMachineCount() {
		return machineCount;
	}
	public void setMachineCount(Integer machineCount) {
		this.machineCount = machineCount;
	}


	// **«««@2018/5/22  LBNÎì  üÓ@Bõv]Î ÇÁ  «««**//
	public Double getSearchRangeFromIdo() {
		return searchRangeFromIdo;
	}
	public void setSearchRangeFromIdo(Double searchRangeFromIdo) {
		this.searchRangeFromIdo = searchRangeFromIdo;
	}

	public Double getSearchRangeFromKeido() {
		return searchRangeFromKeido;
	}
	public void setSearchRangeFromKeido(Double searchRangeFromKeido) {
		this.searchRangeFromKeido = searchRangeFromKeido;
	}
	// **ªªª@2018/5/22  LBNÎì  üÓ@Bõv]Î ÇÁ  ªªª**//
}
