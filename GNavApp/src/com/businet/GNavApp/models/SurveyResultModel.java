package com.businet.GNavApp.models;

public class SurveyResultModel {
	private Integer surveyResultId;
	private Integer surveyId;
	private String surveyName;
	private String inputDate;
	private String position;
	
	public Integer getSurveyResultId() {
		return surveyResultId;
	}
	public void setSurveyResultId(Integer surveyResultId) {
		this.surveyResultId = surveyResultId;
	}
	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}
	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
	
}
