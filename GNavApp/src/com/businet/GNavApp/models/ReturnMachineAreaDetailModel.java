package com.businet.GNavApp.models;


public class ReturnMachineAreaDetailModel extends BaseModel {

	private String customerName;
	private Integer areaNumber;
	private String areaName;
	private Integer searchRangeDistance;
	private Double searchRangeFromIdo;
	private Double searchRangeFromKeido;
	private Integer warningCurrentlyInProgressFlg;
	private Integer periodicServiceNoticeFlg;
	private Integer periodicServiceNowDueFlg;
	private Integer hourMeterFlg;
	private Double hourMeter;
	
	
	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	

	public Integer getAreaNumber() {
		return this.areaNumber;
	}
	public void setAreaNumber(Integer areaNumber) {
		this.areaNumber = areaNumber;
	}
	
	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getSearchRangeDistance() {
		return this.searchRangeDistance;
	}

	public void setSearchRangeDistance(Integer searchRangeDistance) {
		this.searchRangeDistance = searchRangeDistance;
	}
	
	public Double getSearchRangeFromIdo() {
		return this.searchRangeFromIdo;
	}

	public void setSearchRangeFromIdo(Double searchRangeFromIdo) {
		this.searchRangeFromIdo = searchRangeFromIdo;
	}
	
	public Double getSearchRangeFromKeido() {
		return this.searchRangeFromKeido;
	}

	public void setSearchRangeFromKeido(Double searchRangeFromKeido) {
		this.searchRangeFromKeido = searchRangeFromKeido;
	}
	
	public Integer getWarningCurrentlyInProgressFlg() {
		return this.warningCurrentlyInProgressFlg;
	}

	public void setWarningCurrentlyInProgressFlg(Integer warningCurrentlyInProgressFlg) {
		this.warningCurrentlyInProgressFlg = warningCurrentlyInProgressFlg;
	}
	
	public Integer getPeriodicServiceNoticeFlg() {
		return this.periodicServiceNoticeFlg;
	}

	public void setPeriodicServiceNoticeFlg(Integer periodicServiceNoticeFlg) {
		this.periodicServiceNoticeFlg = periodicServiceNoticeFlg;
	}
	
	public Integer getPeriodicServiceNowDueFlg() {
		return this.periodicServiceNowDueFlg;
	}

	public void setPeriodicServiceNowDueFlg(Integer periodicServiceNowDueFlg) {
		this.periodicServiceNowDueFlg = periodicServiceNowDueFlg;
	}
	
	public Integer getHourMeterFlg() {
		return this.hourMeterFlg;
	}

	public void setHourMeterFlg(Integer hourMeterFlg) {
		this.hourMeterFlg = hourMeterFlg;
	}
	
	public Double getHourMeter() {
		return this.hourMeter;
	}

	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}
	
	
}