package com.businet.GNavApp.models;

import java.sql.Timestamp;

public class DtcArrayListModel extends BaseModel {

//	private Integer serialNumber;
	private Long serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String customerManagementNo;
	private String customerManagementName;
	private String scmModel;
	private String lbxModel;
	private String latestLocation;
	private Double ido;
	private Double keido;
	private double hourMeter;
	private Timestamp latestUtcCommonDateTime;
	private Integer fuelLevel;
	private Integer defLevel;

	public Long getSerialNumber() {
		return this.serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return this.machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManufacturerSerialNumber() {
		return this.manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getLbxSerialNumber() {
		return this.lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}

	public String getCustomerManagementNo() {
		return this.customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public String getCustomerManagementName() {
		return this.customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}
	public String getScmModel() {
		return scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	public String getLbxModel() {
		return lbxModel;
	}
	public void setLbxModel(String lbxModel) {
		this.lbxModel = lbxModel;
	}

	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public double getIdo() {
		return ido;
	}
	public void setIdo(double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Timestamp getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(Timestamp latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}

	public Integer getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public Integer getDefLevel() {
		return defLevel;
	}
	public void setDefLevel(Integer defLevel) {
		this.defLevel = defLevel;
	}


}
