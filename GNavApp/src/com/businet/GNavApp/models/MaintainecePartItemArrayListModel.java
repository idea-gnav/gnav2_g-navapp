package com.businet.GNavApp.models;

public class MaintainecePartItemArrayListModel {

	//↓↓↓ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :start  ↓↓↓//
//	private Integer maintainecePartNumber;
	//↑↑↑ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :end  ↑↑↑//
	private String maintainecePartName;
	// private String maintaineceConType;

	//↓↓↓ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :start  ↓↓↓//
//	public Integer getMaintainecePartNumber() {
//		return maintainecePartNumber;
//	}

//	public void setMaintainecePartNumber(Integer maintainecePartNumber) {
//		this.maintainecePartNumber = maintainecePartNumber;
//	}
	//↑↑↑ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :end  ↑↑↑//

	public String getMaintainecePartName() {
		return maintainecePartName;
	}

	public void setMaintainecePartName(String maintainecePartName) {
		this.maintainecePartName = maintainecePartName;
	}
	/*
	public void setMaintaineceConType(String maintaineceConType) {
		this.maintaineceConType = maintaineceConType;
	}

	public String getMaintaineceConType() {
		return maintaineceConType;
	}
	*/
}
