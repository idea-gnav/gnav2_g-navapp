package com.businet.GNavApp.models;

import java.util.ArrayList;

public class PeriodicServiceDetailArrayListModel extends BaseModel {

	private Integer statusCode;
	private Integer replacementPartNo;
	private String replacementPartName;
	private Integer replacementInterval;
	private Integer timeUntilNextReplacementTime;
	private Integer timeUntilNextReplacementDay;
	private Double replacementScheduleTime;
	private String replacementScheduleDay;
	private Integer periodicServiceNotice;
	private ArrayList<ReplacementHistoryArrayListModel> replacementHistoryList;


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getReplacementPartNo() {
		return replacementPartNo;
	}
	public void setReplacementPartNo(Integer replacementPartNo) {
		this.replacementPartNo = replacementPartNo;
	}

	public String getReplacementPartName() {
		return replacementPartName;
	}
	public void setReplacementPartName(String replacementPartName) {
		this.replacementPartName = replacementPartName;
	}

	public Integer getReplacementInterval() {
		return replacementInterval;
	}
	public void setReplacementInterval(Integer replacementInterval) {
		this.replacementInterval = replacementInterval;
	}

	public Integer getTimeUntilNextReplacementTime() {
		return timeUntilNextReplacementTime;
	}
	public void setTimeUntilNextReplacementTime(Integer timeUntilNextReplacementTime) {
		this.timeUntilNextReplacementTime = timeUntilNextReplacementTime;
	}

	public Integer getTimeUntilNextReplacementDay() {
		return timeUntilNextReplacementDay;
	}
	public void setTimeUntilNextReplacementDay(Integer timeUntilNextReplacementDay) {
		this.timeUntilNextReplacementDay = timeUntilNextReplacementDay;
	}

	public Double getReplacementScheduleTime() {
		return replacementScheduleTime;
	}
	public void setReplacementScheduleTime(Double replacementScheduleTime) {
		this.replacementScheduleTime = replacementScheduleTime;
	}

	public String getReplacementScheduleDay() {
		return replacementScheduleDay;
	}
	public void setReplacementScheduleDay(String replacementScheduleDay) {
		this.replacementScheduleDay = replacementScheduleDay;
	}

	public Integer getPeriodicServiceNotice() {
		return periodicServiceNotice;
	}
	public void setPeriodicServiceNotice(Integer periodicServiceNotice) {
		this.periodicServiceNotice = periodicServiceNotice;
	}

	public ArrayList<ReplacementHistoryArrayListModel> getReplacementHistoryList() {
		return replacementHistoryList;
	}
	public void setReplacementHistoryList(ArrayList<ReplacementHistoryArrayListModel> replacementHistoryList) {
		this.replacementHistoryList = replacementHistoryList;
	}
}
