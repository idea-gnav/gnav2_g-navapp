package com.businet.GNavApp.models;

public class ReturnMachineNoticeListModel {
	private Long serialNumber;
	private String manufacturerSerialNumber;
	private String customerManagementNo;
	private String returnDate;
	private Integer returnMachineIconType;
	private String kigyouName;
	private Integer readFlg;
	
	
	public Long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}
	
	public String getCustomerManagementNo() {
		return customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}
	
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	
	public String getKigyouName() {
		return kigyouName;
	}
	public void setKigyouName(String kigyouName) {
		this.kigyouName = kigyouName;
	}
	
	public Integer getReadFlg() {
		return readFlg;
	}
	public void setReadFlg(Integer readFlg) {
		this.readFlg = readFlg;
	}
	
	public Integer getReturnMachineIconType()
	{
		return returnMachineIconType;
	}
	public void setReturnMachineIconType(Integer returnMachineIconType)
	{
		this.returnMachineIconType = returnMachineIconType;
	}
}
