package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ChartListArraylistModel {

	private Integer companyId;
	private Integer itemNo;
	private String item;
	private double averageEvaluation;
	private Integer colorEvaluation;
	private ArrayList<ChoiceListArraylistModel> choiceList;
	
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}	
	public Integer getItemNo() {
		return itemNo;
	}
	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public double getAverageEvaluation() {
		return averageEvaluation;
	}
	public void setAverageEvaluation(double averageEvaluation) {
		this.averageEvaluation = averageEvaluation;
	}	
	public Integer getColorEvaluation() {
		return colorEvaluation;
	}
	public void setColorEvaluation(Integer colorEvaluation) {
		this.colorEvaluation = colorEvaluation;
	}	
	public ArrayList<ChoiceListArraylistModel> getChoiceList() {
		return choiceList;
	}
	public void setChoiceList(ArrayList<ChoiceListArraylistModel> choiceList) {
		this.choiceList = choiceList;
	}
}
