package com.businet.GNavApp.models;

import java.util.ArrayList;

public class MaintenanceReasonArrayListModel extends BaseModel {

	private String itemNo;
	private Integer itemId;
	private String item;
	private Integer freeInputFlg;
	private Integer choiceCount;
	private Integer answerPercentage;
	private ArrayList<CompetitorAnswerCountArrayListModel> competitorAnswerCountList;


	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}

	public Integer getFreeInputFlg() {
		return freeInputFlg;
	}
	public void setFreeInputFlg(Integer freeInputFlg) {
		this.freeInputFlg = freeInputFlg;
	}

	public Integer getChoiceCount() {
		return choiceCount;
	}
	public void setChoiceCount(Integer choiceCount) {
		this.choiceCount = choiceCount;
	}

	public Integer getAnswerPercentage() {
		return answerPercentage;
	}
	public void setAnswerPercentage(Integer answerPercentage) {
		this.answerPercentage = answerPercentage;
	}

	public ArrayList<CompetitorAnswerCountArrayListModel> getCompetitorAnswerCountList() {
		return competitorAnswerCountList;
	}
	public void setCompetitorAnswerCountList(ArrayList<CompetitorAnswerCountArrayListModel> competitorAnswerCountList) {
		this.competitorAnswerCountList = competitorAnswerCountList;
	}

}
