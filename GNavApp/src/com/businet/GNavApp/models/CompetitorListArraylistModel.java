package com.businet.GNavApp.models;

public class CompetitorListArraylistModel {

	private Integer competitorId;
	private String competitorName;

	public Integer getCompetitorId() {
		return competitorId;
	}
	public void setCompetitorId(Integer competitorId) {
		this.competitorId = competitorId;
	}
	public String getCompetitorName() {
		return competitorName;
	}
	public void setCompetitorName(String competitorName) {
		this.competitorName = competitorName;
	}

}
