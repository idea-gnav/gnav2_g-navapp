package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ReturnMachineAlertListModel {

	private Integer statusCode;
	private ArrayList<ReturnMachineModel> returnMachineList;
	private Integer returnAlertCount;
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public ArrayList<ReturnMachineModel> getReturnMachineList() {
		return returnMachineList;
	}
	public void setReturnMachineList(ArrayList<ReturnMachineModel> returnMachineList) {
		this.returnMachineList = returnMachineList;
	}
	
	public Integer getReturnAlertCount() {
		return returnAlertCount;
	}
	public void setReturnAlertCount(Integer returnAlertCount) {
		this.returnAlertCount = returnAlertCount;
	}
}
