package com.businet.GNavApp.models;

//import java.sql.Date;

public class MachineArrayListModel extends BaseModel {

//	private Integer serialNumber;
	private Long serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String customerManagementNo;
	private String customerManagementName;
	private String scmModel;
	private Integer iconType;
	// **«««@2020/01/30  iDEARº ­¶xñxÇÁ «««**//
	private Integer alertLevelM;
	// **ªªª@2020/01/30  iDEARº ­¶xñxÇÁ ªªª**//
	private String latestLocation;
	private Double ido;
	private Double keido;
	private Double hourMeter;
	private String latestUtcCommonDateTime;
	private Integer fuelLevel;
	private Integer defLevel;
	private Integer favoriteDelFlg;
	// **«««@2019/04/04  iDEARº èú®õõÇÁ «««**//
	private Integer periodicServiceIconType;
	// **ªªª@2019/04/04  iDEARº èú®õõÇÁ ªªª**//
	// **«««@2019/06/25  iDEARº AP[gõÚÇÁ «««**//
	private Integer surveyFlg;
	// **ªªª@2019/06/25  iDEARº AP[gõÚÇÁ ªªª**//
	// **«««@2019/10/31  RASISª{ {HîñõÚÇÁ«««**//
	private Integer constructionFlg;
	// **ªªª@2019/10/31  RASISª{ {HîñõÚÇÁªªª**//

	public Long getSerialNumber() {
		return this.serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return this.machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManufacturerSerialNumber() {
		return this.manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getCustomerManagementNo() {
		return this.customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public String getCustomerManagementName() {
		return this.customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}
	public String getScmModel() {
		return scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}

	// **«««@2020/01/30  iDEARº ­¶xñxÇÁ «««**//
	public Integer getAlertLevelM() {
		return alertLevelM;
	}
	public void setAlertLevelM(Integer alertLevelM) {
		this.alertLevelM = alertLevelM;
	}
	// **ªªª@2020/01/30  iDEARº ­¶xñxÇÁ ªªª**//

	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonString) {
		this.latestUtcCommonDateTime = latestUtcCommonString;
	}

	public Integer getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public Integer getDefLevel() {
		return defLevel;
	}
	public void setDefLevel(Integer defLevel) {
		this.defLevel = defLevel;
	}

	public Integer getFavoriteDelFlg() {
		return favoriteDelFlg;
	}
	public void setFavoriteDelFlg(Integer favoriteDelFlg) {
		this.favoriteDelFlg = favoriteDelFlg;
	}

	// **«««@2019/04/04  iDEARº èú®õõÇÁ «««**//
	public Integer getPeriodicServiceIconType() {
		return periodicServiceIconType;
	}
	public void setPeriodicServiceIconType(Integer periodicServiceIconType) {
		this.periodicServiceIconType = periodicServiceIconType;
	}
	// **ªªª@2019/04/04  iDEARº èú®õõÇÁ ªªª**//

	// **«««@2019/06/25  iDEARº AP[gõÚÇÁ «««**//
	public Integer getSurveyFlg() {
		return surveyFlg;
	}
	public void setSurveyFlg(Integer surveyFlg) {
		this.surveyFlg = surveyFlg;
	}
	// **ªªª@2019/06/25  iDEARº AP[gõÚÇÁ ªªª**//
	// **«««@2019/10/31  RASISª{ {HîñõÚÇÁ«««**//
	public Integer getConstructionFlg() {
		return constructionFlg;
	}
	public void setConstructionFlg(Integer constructionFlg) {
		this.constructionFlg = constructionFlg;
	}
	// **ªªª@2019/10/31  RASISª{ {HîñõÚÇÁªªª**//
}
