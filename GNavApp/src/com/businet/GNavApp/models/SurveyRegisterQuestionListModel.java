package com.businet.GNavApp.models;

import java.util.ArrayList;

public class SurveyRegisterQuestionListModel {

	private Integer questionId;
	private ArrayList<SurveyAnswerChoiceListModel> answerChoiceList;
	private String answerFreeinput;
	private Integer answerImageEditFlg;
	
	public Integer getAnswerImageEditFlg() {
		return answerImageEditFlg;
	}
	public void setAnswerImageEditFlg(Integer answerImageEditFlg) {
		this.answerImageEditFlg = answerImageEditFlg;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public ArrayList<SurveyAnswerChoiceListModel> getAnswerChoiceList() {
		return answerChoiceList;
	}
	public void setAnswerChoiceList(ArrayList<SurveyAnswerChoiceListModel> answerChoiceList) {
		this.answerChoiceList = answerChoiceList;
	}
	public String getAnswerFreeinput() {
		return answerFreeinput;
	}
	public void setAnswerFreeinput(String answerFreeinput) {
		this.answerFreeinput = answerFreeinput;
	}
	
	
}
