package com.businet.GNavApp.models;

public class DailyReportModel extends BaseModel{

	private Long serialNumber;
	private Integer machineModelCategory;
	private String date;

	private String manufacturerSerialNumber;
	private String customerManagementNo;
	private String customerManagementName;
	private String scmModel;

	private Integer iconType;
	// **«««@2020/01/30  iDEARΊ ­ΆxρxΗΑ «««**//
	private Integer alertLevelM;
	// **ͺͺͺ@2020/01/30  iDEARΊ ­ΆxρxΗΑ ͺͺͺ**//
	private String latestLocation;
	private Double ido;
	private Double keido;
	private Double hourMeter;
	private String latestUtcCommonDateTime;
	private Integer defLevel;
	private Integer fuelLevel;
	private String nippoData;
	private Integer patternType;
	private String displayItemPattern;
	private String engineOperatingTime;			// Double -> String
	private String machineOperatingTime;
	private Double idlePercentage;
	private Double fuelConsumption;
	private Double fuelEffciency;
	private Integer fuelLevelDaily;
	private Double defConsumpiton;
	private Integer defLevelDaily;
	private Double rageeterOndoMax;
	private Double oilTempartureMax;
	private String constructionTime;
	private Double tonsMaterialHandling;
	private Double constructionDistance;
	private Double batteryVoltage;
	private Double alternatorVoltage;
	private Integer optionalManualStartCount;
	private Integer requestManualStartCount;
	private Integer autoPlayStart;
	private Integer autoPlayCompleted;
	private Integer manualPlayStart;
	private Integer manualPlayCompleted;
	private Integer lifMagSuction;
	private String lifMagTime;
	private Integer hotShutdown;
	private String breakerTime;




	public DailyReportModel() {
	}


	public Long getserialNumber() {
		return this.serialNumber;
	}
	public void setserialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return this.machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getDate() {
		return this.date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getManufacturerSerialNumber() {
		return this.manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getCustomerManagementNo() {
		return this.customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public String getCustomerManagementName() {
		return this.customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}

	public String getScmModel() {
		return this.scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}


//	private Integer iconFlg;
	public Integer getIconType() {
		return this.iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}

	// **«««@2020/01/30  iDEARΊ ­ΆxρxΗΑ «««**//
	public Integer getAlertLevelM() {
		return alertLevelM;
	}
	public void setAlertLevelM(Integer alertLevelM) {
		this.alertLevelM = alertLevelM;
	}
	// **ͺͺͺ@2020/01/30  iDEARΊ ­ΆxρxΗΑ ͺͺͺ**//

//	private String latestLocation;
	public String getLatestLocation() {
		return this.latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}
//	private Double ido;
	public Double getIdo() {
		return this.ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}
//	private Double keido;
	public Double getKeido() {
		return this.keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}
//	private Double hourMeter;
	public Double getHourMeter() {
		return this.hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}
//	private String latestUtcCommonDateTime;
	public String getLatestUtcCommonDateTime() {
		return this.latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}
//	private Integer defLevel;
	public Integer getDefLevel() {
		return this.defLevel;
	}
	public void setDefLevel(Integer defLevel) {
		this.defLevel = defLevel;
	}
//	private Integer fuelLevel;
	public Integer getFuelLevel() {
		return this.fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}
//	private String nippoData;
	public String getNippoData() {
		return this.nippoData;
	}
	public void setNippoData(String nippoData) {
		this.nippoData = nippoData;
	}
//	private Integer patternType;
	public Integer getPatternType() {
		return this.patternType;
	}
	public void setPatternType(Integer patternType) {
		this.patternType = patternType;
	}
//	private String displayItemPattern;
	public String getDisplayItemPattern() {
		return this.displayItemPattern;
	}
	public void setDisplayItemPattern(String displayItemPattern) {
		this.displayItemPattern = displayItemPattern;
	}
//	private Double engineOperatingTime;
	public String getEngineOperatingTime() {
		return this.engineOperatingTime;
	}
	public void setEngineOperatingTime(String engineOperatingTime) {
		this.engineOperatingTime = engineOperatingTime;
	}
//	private Double machineOperatingTime;
	public String getMachineOperatingTime() {
		return this.machineOperatingTime;
	}
	public void setMachineOperatingTime(String machineOperatingTime) {
		this.machineOperatingTime = machineOperatingTime;
	}
//	private Double idlePercentage;
	public Double getIdlePercentage() {
		return this.idlePercentage;
	}
	public void setIdlePercentage(Double idlePercentage) {
		this.idlePercentage = idlePercentage;
	}
//	private Double fuelConsumption;
	public Double getFuelConsumption() {
		return this.fuelConsumption;
	}
	public void setFuelConsumption(Double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
//	private Double fuelEffciency;
	public Double getFuelEffciency() {
		return this.fuelEffciency;
	}
	public void setFuelEffciency(Double fuelEffciency) {
		this.fuelEffciency = fuelEffciency;
	}
//	private Integer fuelLevelDaily;
	public Integer getFuelLevelDaily() {
		return this.fuelLevelDaily;
	}
	public void setFuelLevelDaily(Integer fuelLevelDaily) {
		this.fuelLevelDaily = fuelLevelDaily;
	}
//	private Double defConsumpiton;
	public Double getDefConsumpiton() {
		return this.defConsumpiton;
	}
	public void setDefConsumpiton(Double defConsumpiton) {
		this.defConsumpiton = defConsumpiton;
	}
//	private Integer defLevelDaily;
	public Integer getDefLevelDaily() {
		return this.defLevelDaily;
	}
	public void setDefLevelDaily(Integer defLevelDaily) {
		this.defLevelDaily = defLevelDaily;
	}
//	private String constructionTime;
	public String getConstructionTime() {
		return this.constructionTime;
	}
	public void setConstructionTime(String constructionTime) {
		this.constructionTime = constructionTime;
	}
//	private Double tonsMaterialHandling;
	public Double getTonsMaterialHandling() {
		return this.tonsMaterialHandling;
	}
	public void setTonsMaterialHandling(Double tonsMaterialHandling) {
		this.tonsMaterialHandling = tonsMaterialHandling;
	}
//	private Double constructionDistance;
	public Double getConstructionDistance() {
		return this.constructionDistance;
	}
	public void setConstructionDistance(Double constructionDistance) {
		this.constructionDistance = constructionDistance;
	}
//	private Double rageeterOndoMax;
	public Double getRageeterOndoMax() {
		return this.rageeterOndoMax;
	}
	public void setRageeterOndoMax(Double rageeterOndoMax) {
		this.rageeterOndoMax = rageeterOndoMax;
	}
//	private Double oilTempartureMax;
	public Double getOilTempartureMax() {
		return this.oilTempartureMax;
	}
	public void setOilTempartureMax(Double oilTempartureMax) {
		this.oilTempartureMax = oilTempartureMax;
	}
//	private Integer batteryVoltage;
	public Double getBatteryVoltage() {
		return this.batteryVoltage;
	}
	public void setBatteryVoltage(Double batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}
//	private Integer alternatorVoltage;
	public Double getAlternatorVoltage() {
		return this.alternatorVoltage;
	}
	public void setAlternatorVoltage(Double alternatorVoltage) {
		this.alternatorVoltage = alternatorVoltage;
	}
//	private Integer optionalManualStartCount;
	public Integer getOptionalManualStartCount() {
		return this.optionalManualStartCount;
	}
	public void setOptionalManualStartCount(Integer optionalManualStartCount) {
		this.optionalManualStartCount = optionalManualStartCount;
	}
//	private Integer requestManualStartCount;
	public Integer getRequestManualStartCount() {
		return this.requestManualStartCount;
	}
	public void setRequestManualStartCount(Integer requestManualStartCount) {
		this.requestManualStartCount = requestManualStartCount;
	}
//	private Integer autoPlayStart;
	public Integer getAutoPlayStart() {
		return this.autoPlayStart;
	}
	public void setAutoPlayStart(Integer autoPlayStart) {
		this.autoPlayStart = autoPlayStart;
	}
//	private Integer autoPlayCompleted;
	public Integer getAutoPlayCompleted() {
		return this.autoPlayCompleted;
	}
	public void setAutoPlayCompleted(Integer autoPlayCompleted) {
		this.autoPlayCompleted = autoPlayCompleted;
	}
//	private Integer manualPlayStart;
	public Integer getManualPlayStart() {
		return this.manualPlayStart;
	}
	public void setManualPlayStart(Integer manualPlayStart) {
		this.manualPlayStart = manualPlayStart;
	}
//	private Integer manualPlayCompleted;
	public Integer getManualPlayCompleted() {
		return this.manualPlayCompleted;
	}
	public void setManualPlayCompleted(Integer manualPlayCompleted) {
		this.manualPlayCompleted = manualPlayCompleted;
	}
//	private Integer lifMagSuction;
	public Integer getLifMagSuction() {
		return this.lifMagSuction;
	}
	public void setLifMagSuction(Integer lifMagSuction) {
		this.lifMagSuction = lifMagSuction;
	}
//	private String lifMagTime;
	public String getLifMagTime() {
		return this.lifMagTime;
	}
	public void setLifMagTime(String lifMagTime) {
		this.lifMagTime = lifMagTime;
	}
//	private Integer hotShutdown;
	public Integer getHotShutdown() {
		return this.hotShutdown;
	}
	public void setHotShutdown(Integer hotShutdown) {
		this.hotShutdown = hotShutdown;
	}
//	private String breakerTime;
	public String getBreakerTime() {
		return this.breakerTime;
	}
	public void setBreakerTime(String breakerTime) {
		this.breakerTime = breakerTime;
	}






}
