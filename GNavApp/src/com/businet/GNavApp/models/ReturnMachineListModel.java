package com.businet.GNavApp.models;

public class ReturnMachineListModel {

	private Long serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String customerManagementNo;
	private String customerManagementName;
	private String scmModel;
	private Integer iconType;
	// **«««@2020/01/30  iDEARΊ ­ΆxρxΗΑ «««**//
	private Integer alertLevelM;
	// **ͺͺͺ@2020/01/30  iDEARΊ ­ΆxρxΗΑ ͺͺͺ**//
	private String latestLocation;
	private Double ido;
	private Double keido;
	private Double hourMeter;
	private String latestUtcCommonDateTime;
	private Integer fuelLevel;
	private Integer defLevel;
	private Integer returnMachineFlg;


	public Long getSerialNumber() {
		return this.serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return this.machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManufacturerSerialNumber() {
		return this.manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getCustomerManagementNo() {
		return this.customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public String getCustomerManagementName() {
		return this.customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}
	public String getScmModel() {
		return this.scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	public Integer getIconType() {
		return this.iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}

	// **«««@2020/01/30  iDEARΊ ­ΆxρxΗΑ «««**//
	public Integer getAlertLevelM() {
		return alertLevelM;
	}
	public void setAlertLevelM(Integer alertLevelM) {
		this.alertLevelM = alertLevelM;
	}
	// **ͺͺͺ@2020/01/30  iDEARΊ ­ΆxρxΗΑ ͺͺͺ**//

	public String getLatestLocation() {
		return this.latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public Double getIdo() {
		return this.ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return this.keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Double getHourMeter() {
		return this.hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getLatestUtcCommonDateTime() {
		return this.latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonString) {
		this.latestUtcCommonDateTime = latestUtcCommonString;
	}

	public Integer getFuelLevel() {
		return this.fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public Integer getDefLevel() {
		return this.defLevel;
	}
	public void setDefLevel(Integer defLevel) {
		this.defLevel = defLevel;
	}

	public Integer getReturnMachineFlg() {
		return this.returnMachineFlg;
	}
	public void setReturnMachineFlg(Integer returnMachineFlg) {
		this.returnMachineFlg = returnMachineFlg;
	}

}
