package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ImportanceInPurchasingModel extends BaseModel {

	private Integer statusCode;
	private String surveyName;
	private Integer targetFigures;
	private Integer achievementFigures;
	private ArrayList<ImportanceInPurchasingListModel> importanceInPurchasingList;

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}
	public ArrayList<ImportanceInPurchasingListModel> getImportanceInPurchasingList() {
		return importanceInPurchasingList;
	}
	public void setImportanceInPurchasingList(ArrayList<ImportanceInPurchasingListModel> importanceInPurchasingList) {
		this.importanceInPurchasingList = importanceInPurchasingList;
	}
	public Integer getTargetFigures() {
		return targetFigures;
	}
	public void setTargetFigures(Integer targetFigures) {
		this.targetFigures = targetFigures;
	}
	public Integer getAchievementFigures() {
		return achievementFigures;
	}
	public void setAchievementFigures(Integer achievementFigures) {
		this.achievementFigures = achievementFigures;
	}


}