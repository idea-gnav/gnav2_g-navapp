package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ReturnMachineAreaListModel {

	private Integer statusCode;
	private ArrayList<AreaListModel> areaList;
	private Integer returnAreaCount;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public ArrayList<AreaListModel> getAreaList() {
		return areaList;
	}

	public void setAreaList(ArrayList<AreaListModel> areaList) {
		this.areaList = areaList;
	}

	public Integer getReturnAreaCount() {
		return returnAreaCount;
	}

	public void setReturnAreaCount(Integer returnAreaCount) {
		this.returnAreaCount = returnAreaCount;
	}
}