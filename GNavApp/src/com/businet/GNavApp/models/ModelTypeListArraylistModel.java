package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ModelTypeListArraylistModel {

	private String modelType;
	private ArrayList<ScmModelListArraylistModel> scmModelList;

	public String getModelType() {
		return modelType;
	}
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	public ArrayList<ScmModelListArraylistModel> getScmModelList() {
		return scmModelList;
	}
	public void setScmModelList(ArrayList<ScmModelListArraylistModel> scmModelList) {
		this.scmModelList = scmModelList;
	}

}
