package com.businet.GNavApp.models;



public class ReportOperatingListModel extends BaseModel{
	private String date;
	private String dateFrom;
	private String dateTo;
	private String engineOperatingTime;
	private String machineOperatingTime;
	private Double idlePercentage;
	private Double fuelConsumption;
	private Double fuelEffciency;
	private Double defConsumpiton;
	private String constructionTime;
	private Double constructionPercentage;


	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getEngineOperatingTime() {
		return engineOperatingTime;
	}
	public void setEngineOperatingTime(String engineOperatingTime) {
		this.engineOperatingTime = engineOperatingTime;
	}

	public String getMachineOperatingTime() {
		return machineOperatingTime;
	}
	public void setMachineOperatingTime(String machineOperatingTime) {
		this.machineOperatingTime = machineOperatingTime;
	}

	public Double getIdlePercentage() {
		return idlePercentage;
	}
	public void setIdlePercentage(Double idlePercentage) {
		this.idlePercentage = idlePercentage;
	}

	public Double getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(Double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public Double getFuelEffciency() {
		return fuelEffciency;
	}
	public void setFuelEffciency(Double fuelEffciency) {
		this.fuelEffciency = fuelEffciency;
	}

	public Double getDefConsumpiton() {
		return defConsumpiton;
	}
	public void setDefConsumpiton(Double defConsumpiton) {
		this.defConsumpiton = defConsumpiton;
	}


//	private double constructionTime;
	public String getConstructionTime() {
		return constructionTime;
	}
	public void setConstructionTime(String constructionTime) {
		this.constructionTime = constructionTime;
	}

//	private double constructionPercentage;
	public Double getConstructionPercentage() {
		return constructionPercentage;
	}
	public void setConstructionPercentage(Double constructionPercentage) {
		this.constructionPercentage = constructionPercentage;
	}





}
