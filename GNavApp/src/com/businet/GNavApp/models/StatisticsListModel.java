package com.businet.GNavApp.models;

public class StatisticsListModel{
	
	private String groupTitle;
	private Double circleObjSize;
	private Double aggregateTime;
	private Double aggregateUnit;

	
	public StatisticsListModel() {}
	
	public StatisticsListModel(String groupTitle, Double circleObjSize, Double aggregateTime, Double aggregateUnit) {
		this.groupTitle = groupTitle;
		this.circleObjSize = circleObjSize;
		this.aggregateTime =aggregateTime;
		this.aggregateUnit = aggregateUnit;
	}
	
	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}
	public String getGroupTitle() {
		return groupTitle;
	}
	
	public void setCircleObjSize(Double circleObjSize) {
		this.circleObjSize = circleObjSize;
	}
	public Double getCircleObjSize() {
		return circleObjSize;
	}

	public void setAggregateTime(Double aggregateTime) {
		this.aggregateTime = aggregateTime;
	}
	public Double getAggregateTime() {
		return aggregateTime;
	}
	
	public void setAggregateUnit(Double aggregateUnit) {
		this.aggregateUnit = aggregateUnit;
	}
	public Double getAggregateUnit() {
		return aggregateUnit;
	}
	
}