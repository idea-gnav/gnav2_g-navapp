package com.businet.GNavApp.models;

import java.util.ArrayList;

public class SurveyQuestionModel {
	private Integer questionId;
	private Integer questionNo;
	private String question;
	private Integer quetionPattern;
	private Integer parentFlg;
	private Integer freeInputFlg;
	private String freeInputCaption;
	private Integer freeInputChoiceId;
	private Integer mustAnswerFlg;
	private Integer multiChoiceMax;
	private Integer parentQuetionId;
	private Integer parentChoiceId;
	private ArrayList<SurveyChoiceModel> choiceList;
	private ArrayList<SurveyAnswerChoiceListModel> answerChoiceList;
	private String answerFreeinput;
	private String answerImageUrl;

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(Integer questionNo) {
		this.questionNo = questionNo;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getQuetionPattern() {
		return quetionPattern;
	}

	public void setQuetionPattern(Integer quetionPattern) {
		this.quetionPattern = quetionPattern;
	}

	public Integer getParentFlg() {
		return parentFlg;
	}

	public void setParentFlg(Integer parentFlg) {
		this.parentFlg = parentFlg;
	}

	public Integer getFreeInputFlg() {
		return freeInputFlg;
	}

	public void setFreeInputFlg(Integer freeInputFlg) {
		this.freeInputFlg = freeInputFlg;
	}

	public String getFreeInputCaption() {
		return freeInputCaption;
	}

	public void setFreeInputCaption(String freeInputCaption) {
		this.freeInputCaption = freeInputCaption;
	}

	public Integer getFreeInputChoiceId() {
		return freeInputChoiceId;
	}

	public void setFreeInputChoiceId(Integer freeInputChoiceId) {
		this.freeInputChoiceId = freeInputChoiceId;
	}

	public Integer getMustAnswerFlg() {
		return mustAnswerFlg;
	}

	public void setMustAnswerFlg(Integer mustAnswerFlg) {
		this.mustAnswerFlg = mustAnswerFlg;
	}

	public Integer getMultiChoiceMax() {
		return multiChoiceMax;
	}

	public void setMultiChoiceMax(Integer multiChoiceMax) {
		this.multiChoiceMax = multiChoiceMax;
	}

	public Integer getParentQuetionId() {
		return parentQuetionId;
	}

	public void setParentQuetionId(Integer parentQuetionId) {
		this.parentQuetionId = parentQuetionId;
	}

	public Integer getParentChoiceId() {
		return parentChoiceId;
	}

	public void setParentChoiceId(Integer parentChoiceId) {
		this.parentChoiceId = parentChoiceId;
	}

	public ArrayList<SurveyChoiceModel> getChoiceList() {
		return choiceList;
	}

	public void setChoiceList(ArrayList<SurveyChoiceModel> choiceList) {
		this.choiceList = choiceList;
	}

	public ArrayList<SurveyAnswerChoiceListModel> getAnswerChoiceList() {
		return answerChoiceList;
	}

	public void setAnswerChoiceList(ArrayList<SurveyAnswerChoiceListModel> answerChoiceList) {
		this.answerChoiceList = answerChoiceList;
	}

	public String getAnswerFreeinput() {
		return answerFreeinput;
	}

	public void setAnswerFreeinput(String answerFreeinput) {
		this.answerFreeinput = answerFreeinput;
	}

	public String getAnswerImageUrl() {
		return answerImageUrl;
	}

	public void setAnswerImageUrl(String answerImageUrl) {
		this.answerImageUrl = answerImageUrl;
	}

}
