package com.businet.GNavApp.models;

public class ReturnMachineAreaRegisterModel extends BaseModel {

	private Integer areaNumber;
	
	public Integer getAreaNumber() {
		return this.areaNumber;
	}
	public void setAreaNumber(Integer areaNumber) {
		this.areaNumber = areaNumber;
	}
	
}
