package com.businet.GNavApp.models;

public class GoogleDirectionServiceModel extends BaseModel {

	private Integer statusCode;
	private String carRouterData;
	private String walkRouterData;
	private String routeData;
	
	
	public String getCarRouterData() {
		return carRouterData;
	}
	public void setCarRouterData(String carRouterData) {
		this.carRouterData = carRouterData;
	}
	public String getWalkRouterData() {
		return walkRouterData;
	}
	public void setWalkRouterData(String walkRouterData) {
		this.walkRouterData = walkRouterData;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getRouteData() {
		return routeData;
	}
	public void setRouteData(String routeData) {
		this.routeData = routeData;
	}
	
}
