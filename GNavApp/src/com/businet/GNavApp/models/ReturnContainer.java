package com.businet.GNavApp.models;

public class ReturnContainer {
	private int statusCode;
	private Object data;
	
	public ReturnContainer(int code, Object data) {
		this.statusCode = code;
		this.data = data;
	}
	
	public ReturnContainer(int code) {
		this.statusCode = code;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
