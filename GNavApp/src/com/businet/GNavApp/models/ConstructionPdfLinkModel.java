package com.businet.GNavApp.models;


public class ConstructionPdfLinkModel {
	private String troubleName;
	private String pdfLink;
	
	public String getTroubleName() {
		return troubleName;
	}
	public void setTroubleName(String troubleName) {
		this.troubleName = troubleName;
	}
		
	public String getPdfLink() {
		return pdfLink;
	}
	public void setPdfLink(String pdfLink) {
		this.pdfLink = pdfLink;
	}
}
