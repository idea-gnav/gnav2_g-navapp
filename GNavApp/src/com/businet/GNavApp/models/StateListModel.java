package com.businet.GNavApp.models;


public class StateListModel{

	private String stateCd;
	private String stateName;
	private Double stateIdo;
	private Double stateKeido;


	public StateListModel() {}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}
	public String getStateCd() {
		return stateCd;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getStateName() {
		return stateName;
	}

	public void setStateIdo(Double stateIdo) {
		this.stateIdo = stateIdo;
	}
	public Double getStateIdo() {
		return stateIdo;
	}

	public void setStateKeido(Double stateKeido) {
		this.stateKeido = stateKeido;
	}
	public Double getStateKeido() {
		return stateKeido;
	}




}