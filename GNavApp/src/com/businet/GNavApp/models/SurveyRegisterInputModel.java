package com.businet.GNavApp.models;

import java.util.ArrayList;

public class SurveyRegisterInputModel {
	private String userId;
	private Integer surveyId;
	private Long serialNumber;
	private Integer surveyResultId;
	private Integer surveyDelFlg;
	private String kigyoName;
	private String answerUser;
	private String position;
	private ArrayList<SurveyRegisterQuestionListModel> questionList;
	private Integer deviceLanguage;
	private Double hourMeter;
	private String inputDate;
	// **««« 2019/9/18 RASISͺ{ AP[gρΚuZbgΗΑ«««**//
	private Double ido;
	private Double keido;
	// **ͺͺͺ 2019/9/18 RASISͺ{ AP[gρΚuZbgΗΑͺͺͺ**//


	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}
	public Long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getSurveyResultId() {
		return surveyResultId;
	}
	public void setSurveyResultId(Integer surveyResultId) {
		this.surveyResultId = surveyResultId;
	}
	public Integer getSurveyDelFlg() {
		return surveyDelFlg;
	}
	public void setSurveyDelFlg(Integer surveyDelFlg) {
		this.surveyDelFlg = surveyDelFlg;
	}
	public String getKigyoName() {
		return kigyoName;
	}
	public void setKigyoName(String kigyoName) {
		this.kigyoName = kigyoName;
	}
	public String getAnswerUser() {
		return answerUser;
	}
	public void setAnswerUser(String answerUser) {
		this.answerUser = answerUser;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public ArrayList<SurveyRegisterQuestionListModel> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(ArrayList<SurveyRegisterQuestionListModel> questionList) {
		this.questionList = questionList;
	}
	public Integer getDeviceLanguage() {
		return deviceLanguage;
	}
	public void setDeviceLanguage(Integer deviceLanguage) {
		this.deviceLanguage = deviceLanguage;
	}

	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	// **««« 2019/9/18 RASISͺ{ AP[gρΚuZbgΗΑ«««**//
	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}
	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}
	// **ͺͺͺ 2019/9/18 RASISͺ{ AP[gρΚuZbgΗΑͺͺͺ**//
}
