package com.businet.GNavApp.models;

public class ConstructionReportPhotoModel {
	private Integer photoNo;
	private String sekoPhotoPath;
	private String photoDate;
//	private String photoComment;
	
	public Integer getPhotoNo() {
		return photoNo;
	}
	public void setPhotoNo(Integer photoNo) {
		this.photoNo = photoNo;
	}
	public String getSekoPhotoPath() {
		return sekoPhotoPath;
	}
	public void setSekoPhotoPath(String sekoPhotoPath) {
		this.sekoPhotoPath = sekoPhotoPath;
	}
	public String getPhotoDate() {
		return photoDate;
	}
	public void setPhotoDate(String photoDate) {
		this.photoDate = photoDate;
	}
//	public String getPhotoComment() {
//		return photoComment;
//	}
//	public void setPhotoComment(String photoComment) {
//		this.photoComment = photoComment;
//	}
}
