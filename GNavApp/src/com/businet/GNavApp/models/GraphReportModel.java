package com.businet.GNavApp.models;

import java.util.List;

//public class GraphReportModel extends BaseModel{
public class GraphReportModel{
	private Integer statusCode;
	private String yearMonth;
//	private Integer serialNumber;
	private Long serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String customerManagementNo;
	private String customerManagementName;
	private String scmModel;
	private Integer iconType;
	private String latestLocation;
	private Double ido;
	private Double keido;
	private Double hourMeter;
	private Integer fuelLevel;
	private String latestUtcCommonDateTime;
	private Integer defLevel;
	private Integer constructionFlg;

	private List<ReportOperatingListModel> OperatingPartialList;
	private List<ReportOperatingListModel> OperatingWeekTotalList;

	private String engineOperatingTimeTotal;
	private String machineOperatingTimeTotal;
	private Double idlePercentageTotal;
	private Double fuelConsumptionTotal;
	private Double fuelEffciencyTotal;
	private Double defConsumpitonTotal;
	private String constructionTimeTotal;
	private Double constructionPercentageTotal;


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getYearMonth() {
		return yearMonth;
	}
	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}

	public Long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getCustomerManagementNo() {
		return customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementno) {
		this.customerManagementNo = customerManagementno;
	}

	public String getCustomerManagementName() {
		return customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}

	public String getScmModel() {
		return scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	//private Integer iconType;
	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}


	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}


	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Integer getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public String getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}

	public Integer getDefLevel() {
		return defLevel;
	}
	public void setDefLevel(Integer defLevel) {
		this.defLevel = defLevel;
	}

//	private Integer constructionFlg;
	public Integer getConstructionFlg() {
		return constructionFlg;
	}
	public void setConstructionFlg(Integer constructionFlg) {
		this.constructionFlg = constructionFlg;
	}



	public List<ReportOperatingListModel> getOperatingPartialList() {
		return OperatingPartialList;
	}
	public void setOperatingPartialList(List<ReportOperatingListModel> operatingPartialList) {
		OperatingPartialList = operatingPartialList;
	}

	public List<ReportOperatingListModel> getOperatingWeekTotalList() {
		return OperatingWeekTotalList;
	}
	public void setOperatingWeekTotalList(List<ReportOperatingListModel> operatingWeekTotalList) {
		OperatingWeekTotalList = operatingWeekTotalList;
	}



	public String getEngineOperatingTimeTotal() {
		return engineOperatingTimeTotal;
	}
	public void setEngineOperatingTimeTotal(String engineOperatingTimeTotal) {
		this.engineOperatingTimeTotal = engineOperatingTimeTotal;
	}

	public String getMachineOperatingTimeTotal() {
		return machineOperatingTimeTotal;
	}
	public void setMachineOperatingTimeTotal(String machineOperatingTimeTotal) {
		this.machineOperatingTimeTotal = machineOperatingTimeTotal;
	}

	public Double getIdlePercentageTotal() {
		return idlePercentageTotal;
	}
	public void setIdlePercentageTotal(Double idlePercentageTotal) {
		this.idlePercentageTotal = idlePercentageTotal;
	}

	public Double getFuelConsumptionTotal() {
		return fuelConsumptionTotal;
	}
	public void setFuelConsumptionTotal(Double fuelConsumptionTotal) {
		this.fuelConsumptionTotal = fuelConsumptionTotal;
	}

	public Double getFuelEffciencyTotal() {
		return fuelEffciencyTotal;
	}
	public void setFuelEffciencyTotal(Double fuelEffciencyTotal) {
		this.fuelEffciencyTotal = fuelEffciencyTotal;
	}

	public Double getDefConsumpitonTotal() {
		return defConsumpitonTotal;
	}
	public void setDefConsumpitonTotal(Double defConsumpitonTotal) {
		this.defConsumpitonTotal = defConsumpitonTotal;
	}


//	private double constructionTimeTotal;
	public String getConstructionTimeTotal() {
		return constructionTimeTotal;
	}
	public void setConstructionTimeTotal(String constructionTimeTotal) {
		this.constructionTimeTotal = constructionTimeTotal;
	}

//	private double constructionPercentageTotal;
	public Double getConstructionPercentageTotal() {
		return constructionPercentageTotal;
	}
	public void setConstructionPercentageTotal(Double constructionPercentageTotal) {
		this.constructionPercentageTotal = constructionPercentageTotal;
	}


}
