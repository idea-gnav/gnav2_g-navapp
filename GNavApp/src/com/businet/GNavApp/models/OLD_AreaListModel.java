package com.businet.GNavApp.models;

public class OLD_AreaListModel {
    private String kyotenCd;
	private Integer areaNumber;
	private String customerName;
	private String areaName;

	public Integer getAreaNumber() {
		return this.areaNumber;
	}
	public void setAreaNumber(Integer areaNumber) {
		this.areaNumber = areaNumber;
	}

	public String getCustomerName() {
		return this.customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAreaName() {
		return this.areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getKyotenCd() {
		return kyotenCd;
	}
	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

}
