package com.businet.GNavApp.models;

import java.util.ArrayList;

public class MachineFavoriteListModel {

	private Integer statusCode;
	private ArrayList<MachineFavoriteArraylistModel> machineList;
	private Integer machineCount;

	
	public MachineFavoriteListModel() {
		
	}

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public ArrayList<MachineFavoriteArraylistModel> getMachineList() {
		return machineList;
	}
	public void setMachineList(ArrayList<MachineFavoriteArraylistModel> machineList) {
		this.machineList = machineList;
	}

	public Integer getMachineFavoriteCount() {
		return machineCount;
	}
	public void setMachineFavoriteCount(Integer machineCount) {
		this.machineCount = machineCount;
	}

}
