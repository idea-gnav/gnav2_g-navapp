package com.businet.GNavApp.models;

public class SurveyFreeInputTextListModel extends BaseModel{

	private String freeInputText;

	public String getFreeInputText() {
		return freeInputText;
	}

	public void setFreeInputText(String freeInputText) {
		this.freeInputText = freeInputText;
	}
}