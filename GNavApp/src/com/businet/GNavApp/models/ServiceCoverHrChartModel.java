package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ServiceCoverHrChartModel {

	private Integer statusCode;
	private Integer surveyId;
	private String surveyName;
	private Integer targetFigures;
	private Integer achievementFigures;
	private ArrayList<ServiceCoverHrChartArraylistModel> serviceCoverHrChart;

	public ServiceCoverHrChartModel() {

	}


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}
	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}
	public Integer getTargetFigures() {
		return targetFigures;
	}
	public void setTargetFigures(Integer targetFigures) {
		this.targetFigures = targetFigures;
	}
	public Integer getAchievementFigures() {
		return achievementFigures;
	}
	public void setAchievementFigures(Integer achievementFigures) {
		this.achievementFigures = achievementFigures;
	}
	public ArrayList<ServiceCoverHrChartArraylistModel> getServiceCoverHrChart() {
		return serviceCoverHrChart;
	}
	public void setServiceCoverHrChart(ArrayList<ServiceCoverHrChartArraylistModel> serviceCoverHrChart) {
		this.serviceCoverHrChart = serviceCoverHrChart;
	}
}
