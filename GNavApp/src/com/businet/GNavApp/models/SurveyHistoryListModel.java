package com.businet.GNavApp.models;

import java.util.ArrayList;

public class SurveyHistoryListModel extends BaseModel {
	
	private ArrayList<SurveyResultModel> surveyResultList;
	private Integer surveyResultCount;
	
	
	public ArrayList<SurveyResultModel> getSurveyResultList() {
		return surveyResultList;
	}
	public void setSurveyResultList(ArrayList<SurveyResultModel> surveyResultList) {
		this.surveyResultList = surveyResultList;
	}
	public Integer getSurveyResultCount() {
		return surveyResultCount;
	}
	public void setSurveyResultCount(Integer surveyResultCount) {
		this.surveyResultCount = surveyResultCount;
	}

}
