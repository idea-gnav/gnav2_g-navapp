package com.businet.GNavApp.models;

import java.util.ArrayList;

public class SurveyStatisticsListModel {

	private Integer statusCode;
	private ArrayList<SurveyStatisticsArraylistModel> surveyStatisticsList;
	private Integer surveyStatisticsCount;



	public SurveyStatisticsListModel() {

	}


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public ArrayList<SurveyStatisticsArraylistModel> getSurveyStatisticsList() {
		return surveyStatisticsList;
	}
	public void setSurveyList(ArrayList<SurveyStatisticsArraylistModel> surveyStatisticsList) {
		this.surveyStatisticsList = surveyStatisticsList;
	}

	public Integer getSurveyStatisticsCount() {
		return surveyStatisticsCount;
	}
	public void setSurveyStatisticsCount(Integer surveyStatisticsCount) {
		this.surveyStatisticsCount = surveyStatisticsCount;
	}

}
