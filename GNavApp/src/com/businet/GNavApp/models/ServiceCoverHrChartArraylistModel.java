package com.businet.GNavApp.models;

public class ServiceCoverHrChartArraylistModel {

	private Integer summaryName;
	private Integer summaryCount;
	
	public Integer getSummaryName() {
		return summaryName;
	}
	public void setSummaryName(Integer summaryName) {
		this.summaryName = summaryName;
	}
	public Integer getSummaryCount() {
		return summaryCount;
	}
	public void setSummaryCount(Integer summaryCount) {
		this.summaryCount = summaryCount;
	}

}
