package com.businet.GNavApp.models;

public class ConstructionResultModel {
	private String reportDate;
	private String researcher;
	private String researchLocation;
	private Integer reportNo;

	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	
	public String getResearcher() {
		return researcher;
	}
	public void setResearcher(String researcher) {
		this.researcher = researcher;
	}
	
	public String getResearchLocation() {
		return researchLocation;
	}
	public void setResearchLocation(String researchLocation) {
		this.researchLocation = researchLocation;
	}
	
	public Integer getReportNo() {
		return reportNo;
	}
	public void setReportNo(Integer reportNo) {
		this.reportNo = reportNo;
	}
	
}
