package com.businet.GNavApp.models;

public class AreaListModel {
	private String kyotenCode;
	private Integer areaNumber;
	private String customerName;
	private String areaName;
	
	public Integer getAreaNumber() {
		return this.areaNumber;
	}
	public void setAreaNumber(Integer areaNumber) {
		this.areaNumber = areaNumber;
	}
	
	public String getCustomerName() {
		return this.customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getAreaName() {
		return this.areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getKyotenCode() {
		return kyotenCode;
	}
	public void setKyotenCode(String kyotenCode) {
		this.kyotenCode = kyotenCode;
	}
	
}
