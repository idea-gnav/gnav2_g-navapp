package com.businet.GNavApp.models;

public class BaseModel {
	private Integer statusCode;

	public BaseModel() {
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

}
