package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ConstructionPdfModel {
	private Integer choiceId;
	private ArrayList<ConstructionPdfLinkModel> linkList;
	
	public Integer getChoiceId() {
		return choiceId;
	}
	public void setChoiceId(Integer choiceId) {
		this.choiceId = choiceId;
	}
	public ArrayList<ConstructionPdfLinkModel> getLinkList() {
		return linkList;
	}
	public void setLinkList(ArrayList<ConstructionPdfLinkModel> linkList) {
		this.linkList = linkList;
	}
}
