package com.businet.GNavApp.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MachineSearchInfoModel {
	private Integer statusCode;
	private List<String> scmModelList;
	private Integer dealerNameFlg;
	private Integer customerNameFlg;
	private Integer customerInfoFlg;
	private ArrayList<HashMap<String, String>> toukatsubuList;
	private ArrayList<HashMap<String, String>> kyotenList;
	private ArrayList<HashMap<String, String>> dealerList;
	private ArrayList<HashMap<String, String>> serviceKoujyouList;
	private ArrayList<HashMap<String, Object>> machineIconList;
	// 2019/04/10 DucNKT 定期整備画面に追加されました：開始
	//↓↓↓ 2019/05/15 iDEA山下 ArrayListからListに変更 :start ↓↓↓//
	private List<String> maintainecePartItemList;
//	private ArrayList<MaintainecePartItemArrayListModel> maintainecePartItemList;
	//↑↑↑ 2019/05/15 iDEA山下 ArrayListからListに変更 :end ↑↑↑//
	// 2019/04/10 DucNKT 定期整備画面に追加されました：終了
	// **↓↓↓ 2019/10/17  RASIS岡本 お客様名検索対応↓↓↓**//
	private List<String> customerNameList;
	// **↑↑↑ 2019/10/17  RASIS岡本 お客様名検索対応↑↑↑**//

	public MachineSearchInfoModel() {
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public List<String> getScmModelList() {
		return scmModelList;
	}

	public void setScmModelList(List<String> scmModelList) {
		this.scmModelList = scmModelList;
	}

	public Integer getDealerNameFlg() {
		return dealerNameFlg;
	}

	public void setDealerNameFlg(Integer dealerNameFlg) {
		this.dealerNameFlg = dealerNameFlg;
	}

	public Integer getCustomerNameFlg() {
		return customerNameFlg;
	}

	public void setCustomerNameFlg(Integer customerNameFlg) {
		this.customerNameFlg = customerNameFlg;
	}

	public Integer getCustomerInfoFlg() {
		return customerInfoFlg;
	}

	public void setCustomerInfoFlg(Integer customerInfoFlg) {
		this.customerInfoFlg = customerInfoFlg;
	}

	public ArrayList<HashMap<String, String>> getToukatsubuList() {
		return toukatsubuList;
	}

	public void setToukatsubuList(ArrayList<HashMap<String, String>> toukatsubuList) {
		this.toukatsubuList = toukatsubuList;
	}

	public ArrayList<HashMap<String, String>> getKyotenList() {
		return kyotenList;
	}

	public void setKyotenList(ArrayList<HashMap<String, String>> kyotenList) {
		this.kyotenList = kyotenList;
	}

	public ArrayList<HashMap<String, String>> getDealerList() {
		return dealerList;
	}

	public void setDealerList(ArrayList<HashMap<String, String>> dealerList) {
		this.dealerList = dealerList;
	}

	public ArrayList<HashMap<String, String>> getServiceKoujyouList() {
		return serviceKoujyouList;
	}

	public void setServiceKoujyouList(ArrayList<HashMap<String, String>> serviceKoujyouList) {
		this.serviceKoujyouList = serviceKoujyouList;
	}

	public ArrayList<HashMap<String, Object>> getMachineIconList() {
		return machineIconList;
	}

	public void setMachineIconList(ArrayList<HashMap<String, Object>> machineIconList) {
		this.machineIconList = machineIconList;
	}

	// 2019/04/10 DucNKT 定期整備画面に追加されました：開始
    //↓↓↓ 2019/05/15 iDEA山下 ArrayListからListに変更 :start ↓↓↓//
	public void setMaintainecePartItemList(List<String> maintainecePartItemList) {
		this.maintainecePartItemList = maintainecePartItemList;
	}

	public List<String> getMaintainecePartItemList() {
		return maintainecePartItemList;
	}

//	public void setMaintainecePartItemList(ArrayList<MaintainecePartItemArrayListModel> maintainecePartItemList) {
//		this.maintainecePartItemList = maintainecePartItemList;
//	}

//	public ArrayList<MaintainecePartItemArrayListModel> getMaintainecePartItemList() {
//		return maintainecePartItemList;
//	}
	//↑↑↑ 2019/05/15 iDEA山下 ArrayListからListに変更 :end ↑↑↑//
	// 2019/04/10 DucNKT 定期整備画面に追加されました：終了
	// **↓↓↓ 2019/10/17  RASIS岡本 お客様名検索対応↓↓↓**//
	public void setCustomerNameList(List<String> customerNameList) {
		this.customerNameList = customerNameList;
	}

	public List<String> getCustomerNameList() {
		return customerNameList;
	}
	// **↑↑↑ 2019/10/17  RASIS岡本 お客様名検索対応↑↑↑**//
}
