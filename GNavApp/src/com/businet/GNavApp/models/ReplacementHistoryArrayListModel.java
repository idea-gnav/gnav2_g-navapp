package com.businet.GNavApp.models;

public class ReplacementHistoryArrayListModel{

	private Integer replacementNo;
	private Double replacementHour;
    private String replacementDate;

	public Integer getReplacementNo() {
		return replacementNo;
	}
	public void setReplacementNo(Integer replacementNo) {
		this.replacementNo = replacementNo;
	}

	public Double getReplacementHour() {
		return replacementHour;
	}
	public void setReplacementHour(Double replacementHour) {
		this.replacementHour = replacementHour;
	}

	public String getReplacementDate() {
		return replacementDate;
	}
	public void setReplacementDate(String replacementDate) {
		this.replacementDate = replacementDate;
	}


}