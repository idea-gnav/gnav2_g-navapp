package com.businet.GNavApp.models;

public class MachineFavoriteRegisterArrayListModel {


//	private Integer serialNumber;
	private Long serialNumber;
	private Integer favoriteDelFlg;

	public Long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getFavoriteFlg() {
		return favoriteDelFlg;
	}
	public void setFavoriteFlg(Integer favoriteFlg) {
		this.favoriteDelFlg = favoriteFlg;
	}
}
