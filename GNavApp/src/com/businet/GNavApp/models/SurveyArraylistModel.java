package com.businet.GNavApp.models;

public class SurveyArraylistModel {


	private Integer surveyId;
	private String surveyName;
	private String inputFrom;
	private String inputTo;

	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}

	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public String getInputFrom() {
		return inputFrom;
	}
	public void setInputFrom(String inputFrom) {
		this.inputFrom = inputFrom;
	}

	public String getInputTo() {
		return inputTo;
	}
	public void setInputTo(String inputTo) {
		this.inputTo = inputTo;
	}


}
