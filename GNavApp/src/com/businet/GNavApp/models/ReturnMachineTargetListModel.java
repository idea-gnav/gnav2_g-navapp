package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ReturnMachineTargetListModel extends BaseModel {

	private ArrayList<ReturnMachineListModel> machineList;
	private Integer machineCount;

	public ArrayList<ReturnMachineListModel> getMachineList() {
		return this.machineList;
	}
	public void setMachineList(ArrayList<ReturnMachineListModel> machineList) {
		this.machineList = machineList;
	}

	public Integer getMachineCount() {
		return this.machineCount;
	}
	public void setMachineCount(Integer machineCount) {
		this.machineCount = machineCount;
	}
}
