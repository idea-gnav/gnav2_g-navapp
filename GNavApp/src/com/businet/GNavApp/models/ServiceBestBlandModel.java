package com.businet.GNavApp.models;

import java.util.ArrayList;

public class ServiceBestBlandModel {

	private Integer statusCode;
	private Integer surveyId;
	private String surveyName;
	private Integer targetFigures;
	private Integer achievementFigures;
	private ArrayList<ServiceBestBlandArraylistModel> serviceBestBland;

	public ServiceBestBlandModel() {

	}


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}
	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}
	public Integer getTargetFigures() {
		return targetFigures;
	}
	public void setTargetFigures(Integer targetFigures) {
		this.targetFigures = targetFigures;
	}
	public Integer getAchievementFigures() {
		return achievementFigures;
	}
	public void setAchievementFigures(Integer achievementFigures) {
		this.achievementFigures = achievementFigures;
	}
	public ArrayList<ServiceBestBlandArraylistModel> getServiceBestBland() {
		return serviceBestBland;
	}
	public void setServiceBestBland(ArrayList<ServiceBestBlandArraylistModel> serviceBestBland) {
		this.serviceBestBland = serviceBestBland;
	}
}
