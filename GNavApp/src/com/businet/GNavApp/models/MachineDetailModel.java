package com.businet.GNavApp.models;

public class MachineDetailModel extends BaseModel {

	private Integer statusCode;
//	private Integer serialNumber;
	private Long serialNumber;
	private Integer favoriteDelFlg;
	private Integer totalBadgeCount; // dtcBadgeCount -> totalBadgeCount: 2019/06 DucNKT edited
	private Integer machineModelCategory;
	private Integer iconType;
	// **«««@2020/01/30  iDEARº ­¶xñxÇÁ «««**//
	private Integer alertLevelM;
	// **ªªª@2020/01/30  iDEARº ­¶xñxÇÁ ªªª**//
	private Double ido;
	private Double keido;
	private String latestLocation;
	private Integer patternType;
	private String displayItemPattern;
    private String managementNo;
    private String manufacturerSerialNumber;
    private String scmModel;
    private String warningCurrentlyInProgress;
    private Double hourMeter;
    private String constructionTime;	// Double -> String
    private Integer fuelLevel;
    private Integer defLevel;
    private Double fuelConsumption;
    private Double defConsumpiton;
    private String latestLocationLatLng;	//add 2018.12.20 LBNÎì
    private String latestUtcCommonDateTime;
    private Integer timeDifference;
    private String customerName;
    private String customerManagementName;	//add 2018.10.16 LBNÎì
    private String typeOfIndustry;
    private String dealerName;
    private String deliveryDate;
    private Double hotShutdown;
    private String breakerModeOpTime;
    private Integer lifMagSuction;
    private String lifMagTime;
    private Integer manualOptionalStart;
    private Integer manualRequestStart;
    private Integer autoPlayStart;
    private Integer autoPlayOver;
    private Integer manualPlayStart;
    private Integer manualPlayOver;
    // **««« 2021/02/03 iDEARº J®«ÏXÎ «««**//
//  private Double engineSerialNumber;
    private String engineSerialNumber;
    // **ªªª 2021/02/03 iDEARº J®«ÏXÎ ªªª**//
    private String computerAVersion;
    private String computerBVersion;
    private String computerCVersion;
    private String computerTVersion;
    private String x4ControllerSerialNo;
    private String x4ControllerPartNo;
    private String hbControllerPartNo;
    private String inverterPartNo;
    private String converterPartNo;
    private String bmuPartNo;
    private String q4000PartNo;
    private String monitorFlashVersion;
    private String monitorFirmwareVersion;
    private String serviceKoujyouName;
    private String toukatsubuName;
    private String kyotenName;
    private String twoDmgOpTime;
    private String twoDmgMajorModeOpTime;
    private String gyoushuNm;
    // **«««@2021/02/18  iDEARº SW2j[ÖÚAÌ×í«««**//
    // **««« 2019/07/12 iDEARº AP[gðJÚ{^ÇÁ «««**//
//  private Integer surveyFlg;
    // **ªªª 2019/07/12 iDEARº AP[gðJÚ{^ÇÁ ªªª**//
    // **ªªª@2021/02/18  iDEARº SW2j[ÖÚAÌ×íªªª**//
    // **«««@2021/02/18  iDEARº èú®õõ5^OÎ«««**//
    private Integer periodicServiceFlg;
    // **ªªª@2021/02/18  iDEARº èú®õõ5^OÎªªª**//



	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getFavoriteDelFlg() {
		return favoriteDelFlg;
	}
	public void setFavoriteDelFlg(Integer favoriteDelFlg) {
		this.favoriteDelFlg = favoriteDelFlg;
	}

	public Integer getTotalBadgeCount() {
		return totalBadgeCount;
	}
	public void setTotalBadgeCount(Integer totalBadgeCount) {
		this.totalBadgeCount = totalBadgeCount;
	}

	public Integer getMachineModelCategory() {
		return machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}

	// **«««@2020/01/30  iDEARº ­¶xñxÇÁ «««**//
	public Integer getAlertLevelM() {
		return alertLevelM;
	}
	public void setAlertLevelM(Integer alertLevelM) {
		this.alertLevelM = alertLevelM;
	}
	// **ªªª@2020/01/30  iDEARº ­¶xñxÇÁ ªªª**//

	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public Integer getPatternType() {
		return patternType;
	}
	public void setPatternType(Integer patternType) {
		this.patternType = patternType;
	}

	public String getDisplayItemPattern() {
		return displayItemPattern;
	}
	public void setDisplayItemPattern(String displayItemPattern) {
		this.displayItemPattern = displayItemPattern;
	}

	public String getManagementNo() {
		return managementNo;
	}
	public void setManagementNo(String managementNo) {
		this.managementNo = managementNo;
	}

	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getScmModel() {
		return scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	public String getWarningCurrentlyInProgress() {
		return warningCurrentlyInProgress;
	}
	public void setWarningCurrentlyInProgress(String warningCurrentlyInProgress) {
		this.warningCurrentlyInProgress = warningCurrentlyInProgress;
	}

	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getConstructionTime() {
		return constructionTime;
	}
	public void setConstructionTime(String constructionTime) {
		this.constructionTime = constructionTime;
	}

	public Integer getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public Integer getDefLevel() {
		return defLevel;
	}
	public void setDefLevel(Integer defLevel) {
		this.defLevel = defLevel;
	}

	public Double getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(Double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public Double getDefConsumpiton() {
		return defConsumpiton;
	}
	public void setDefConsumpiton(Double defConsumpiton) {
		this.defConsumpiton = defConsumpiton;
	}

	// **«««@2018/12/20 LBNÎì  n}ãÉZ\¦Î «««**//
	public String getLatestLocationLatLng() {
		return latestLocationLatLng;
	}
	public void setLatestLocationLatLng(String latestLocationLatLng) {
		this.latestLocationLatLng = latestLocationLatLng;
	}
	// **ªªª@2018/12/20 LBNÎì  n}ãÉZ\¦Î ªªª**//

	public String getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}

	public Integer getTimeDifference() {
		return timeDifference;
	}
	public void setTimeDifference(Integer timeDifference) {
		this.timeDifference = timeDifference;
	}

	// Modify 2018/10/16 aishikawa :Start
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerManagementName() {
		return customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}
	// Modify 2018/10/16 aishikawa :End

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}
	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Double getHotShutdown() {
		return hotShutdown;
	}
	public void setHotShutdown(Double hotShutdown) {
		this.hotShutdown = hotShutdown;
	}

	public String getBreakerModeOpTime() {
		return breakerModeOpTime;
	}
	public void setBreakerModeOpTime(String breakerModeOpTime) {
		this.breakerModeOpTime = breakerModeOpTime;
	}

	public Integer getLifMagSuction() {
		return lifMagSuction;
	}
	public void setLifMagSuction(Integer lifMagSuction) {
		this.lifMagSuction = lifMagSuction;
	}

	public String getLifMagTime() {
		return lifMagTime;
	}
	public void setLifMagTime(String lifMagTime) {
		this.lifMagTime = lifMagTime;
	}

	public Integer getManualOptionalStart() {
		return manualOptionalStart;
	}
	public void setManualOptionalStart(Integer manualOptionalStart) {
		this.manualOptionalStart = manualOptionalStart;
	}

	public Integer getManualRequestStart() {
		return manualRequestStart;
	}
	public void setManualRequestStart(Integer manualRequestStart) {
		this.manualRequestStart = manualRequestStart;
	}

	public Integer getAutoPlayStart() {
		return autoPlayStart;
	}
	public void setAutoPlayStart(Integer autoPlayStart) {
		this.autoPlayStart = autoPlayStart;
	}

	public Integer getAutoPlayOver() {
		return autoPlayOver;
	}
	public void setAutoPlayOver(Integer autoPlayOver) {
		this.autoPlayOver = autoPlayOver;
	}

	public Integer getManualPlayStart() {
		return manualPlayStart;
	}
	public void setManualPlayStart(Integer manualPlayStart) {
		this.manualPlayStart = manualPlayStart;
	}

	public Integer getManualPlayOver() {
		return manualPlayOver;
	}
	public void setManualPlayOver(Integer manualPlayOver) {
		this.manualPlayOver = manualPlayOver;
	}


	// **««« 2021/02/03 iDEARº J®«ÏXÎ «««**//
//	public Double getEngineSerialNumber() {
//		return engineSerialNumber;
//	}
//	public void setEngineSerialNumber(Double engineSerialNumber) {
//		this.engineSerialNumber = engineSerialNumber;
//	}
	public String getEngineSerialNumber() {
		return engineSerialNumber;
	}
	public void setEngineSerialNumber(String engineSerialNumber) {
		this.engineSerialNumber = engineSerialNumber;
	}
	// **ªªª 2021/02/03 iDEARº J®«ÏXÎ ªªª**//

	public String getComputerAVersion() {
		return computerAVersion;
	}
	public void setComputerAVersion(String computerAVersion) {
		this.computerAVersion = computerAVersion;
	}

	public String getComputerBVersion() {
		return computerBVersion;
	}
	public void setComputerBVersion(String computerBVersion) {
		this.computerBVersion = computerBVersion;
	}

	public String getComputerCVersion() {
		return computerCVersion;
	}
	public void setComputerCVersion(String computerCVersion) {
		this.computerCVersion = computerCVersion;
	}

	public String getComputerTVersion() {
		return computerTVersion;
	}
	public void setComputerTVersion(String computerTVersion) {
		this.computerTVersion = computerTVersion;
	}

	public String getX4ControllerSerialNo() {
		return x4ControllerSerialNo;
	}
	public void setX4ControllerSerialNo(String x4ControllerSerialNo) {
		this.x4ControllerSerialNo = x4ControllerSerialNo;
	}

	public String getX4ControllerPartNo() {
		return x4ControllerPartNo;
	}
	public void setX4ControllerPartNo(String x4ControllerPartNo) {
		this.x4ControllerPartNo = x4ControllerPartNo;
	}

	public String getHbControllerPartNo() {
		return hbControllerPartNo;
	}
	public void setHbControllerPartNo(String hbControllerPartNo) {
		this.hbControllerPartNo = hbControllerPartNo;
	}

	public String getInverterPartNo() {
		return inverterPartNo;
	}
	public void setInverterPartNo(String inverterPartNo) {
		this.inverterPartNo = inverterPartNo;
	}

	public String getConverterPartNo() {
		return converterPartNo;
	}
	public void setConverterPartNo(String converterPartNo) {
		this.converterPartNo = converterPartNo;
	}

	public String getBmuPartNo() {
		return bmuPartNo;
	}
	public void setBmuPartNo(String bmuPartNo) {
		this.bmuPartNo = bmuPartNo;
	}

	public String getQ4000PartNo() {
		return q4000PartNo;
	}
	public void setQ4000PartNo(String q4000PartNo) {
		this.q4000PartNo = q4000PartNo;
	}

	public String getMonitorFlashVersion() {
		return monitorFlashVersion;
	}
	public void setMonitorFlashVersion(String monitorFlashVersion) {
		this.monitorFlashVersion = monitorFlashVersion;
	}

	public String getMonitorFirmwareVersion() {
		return monitorFirmwareVersion;
	}
	public void setMonitorFirmwareVersion(String monitorFirmwareVersion) {
		this.monitorFirmwareVersion = monitorFirmwareVersion;
	}

	public String getServiceKoujyouName() {
		return serviceKoujyouName;
	}
	public void setServiceKoujyouName(String serviceKoujyouName) {
		this.serviceKoujyouName = serviceKoujyouName;
	}

	public String getToukatsubuName() {
		return toukatsubuName;
	}
	public void setToukatsubuName(String toukatsubuName) {
		this.toukatsubuName = toukatsubuName;
	}

	public String getKyotenName() {
		return kyotenName;
	}
	public void setKyotenName(String kyotenName) {
		this.kyotenName = kyotenName;
	}

	public String getTwoDmgOpTime() {
		return twoDmgOpTime;
	}
	public void setTwoDmgOpTime(String twoDmgOpTime) {
		this.twoDmgOpTime = twoDmgOpTime;
	}

	public String getTwoDmgMajorModeOpTime() {
		return twoDmgMajorModeOpTime;
	}
	public void setTwoDmgMajorModeOpTime(String twoDmgMajorModeOpTime) {
		this.twoDmgMajorModeOpTime = twoDmgMajorModeOpTime;
	}

	public String getGyoushuNm() {
		return gyoushuNm;
	}
	public void setGyoushuNm(String gyoushuNm) {
		this.gyoushuNm = gyoushuNm;
	}

	// **«««@2021/02/18  iDEARº SW2j[ÖÚAÌ×í«««**//
    // **««« 2019/07/12 iDEARº AP[gðJÚ{^ÇÁ «««**//
//	public Integer getSurveyFlg() {
//		return surveyFlg;
//	}
//	public void setSurveyFlg(Integer surveyFlg) {
//		this.surveyFlg = surveyFlg;
//	}
    // **ªªª 2019/07/12 iDEARº AP[gðJÚ{^ÇÁ ªªª**//
	// **ªªª@2021/02/18  iDEARº SW2j[ÖÚAÌ×íªªª**//

	// **«««@2021/02/18  iDEARº èú®õõ5^OÎ«««**//
	public Integer getPeriodicServiceFlg() {
		return periodicServiceFlg;
	}
	public void setPeriodicServiceFlg(Integer periodicServiceFlg) {
		this.periodicServiceFlg = periodicServiceFlg;
	}
	// **ªªª@2021/02/18  iDEARº èú®õõ5^OÎªªª**//




}