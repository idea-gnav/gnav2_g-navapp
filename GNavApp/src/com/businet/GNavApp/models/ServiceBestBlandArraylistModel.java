package com.businet.GNavApp.models;

public class ServiceBestBlandArraylistModel {
	
	private Integer freeInputFlg;
	private Integer itemId;
	private Integer itemNo;
	private String item;
	private Integer summaryCount;

	public Integer getFreeInputFlg() {
		return freeInputFlg;
	}
	public void setFreeInputFlg(Integer freeInputFlg) {
		this.freeInputFlg = freeInputFlg;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}	
	public Integer getItemNo() {
		return itemNo;
	}
	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Integer getSummaryCount() {
		return summaryCount;
	}
	public void setSummaryCount(Integer summaryCount) {
		this.summaryCount = summaryCount;
	}

}
