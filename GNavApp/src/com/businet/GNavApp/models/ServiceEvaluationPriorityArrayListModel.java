package com.businet.GNavApp.models;

public class ServiceEvaluationPriorityArrayListModel extends BaseModel {

	private Integer itemNo;
	private Integer itemId;
	private String item;
	private Double priority;
	private Double cspoint;


	public Integer getItemNo() {
		return itemNo;
	}
	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}

	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}

	public Double getPriority() {
		return priority;
	}
	public void setPriority(Double priority) {
		this.priority = priority;
	}

	public Double getCsPoint() {
		return cspoint;
	}
	public void setCsPoint(Double cspoint) {
		this.cspoint = cspoint;
	}

}
