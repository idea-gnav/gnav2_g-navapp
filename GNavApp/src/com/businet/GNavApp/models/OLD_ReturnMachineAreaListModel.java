package com.businet.GNavApp.models;

import java.util.ArrayList;

public class OLD_ReturnMachineAreaListModel {

	private Integer statusCode;
	private ArrayList<OLD_AreaListModel> areaList;
	private Integer returnAreaCount;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public ArrayList<OLD_AreaListModel> getAreaList() {
		return areaList;
	}

	public void setAreaList(ArrayList<OLD_AreaListModel> areaList) {
		this.areaList = areaList;
	}

	public Integer getReturnAreaCount() {
		return returnAreaCount;
	}

	public void setReturnAreaCount(Integer returnAreaCount) {
		this.returnAreaCount = returnAreaCount;
	}
}