package com.businet.GNavApp.models;

public class SurveyChoiceModel {
	private Integer choiceId;
	private String choice;
	
	public Integer getChoiceId() {
		return choiceId;
	}
	public void setChoiceId(Integer choiceId) {
		this.choiceId = choiceId;
	}
	public String getChoice() {
		return choice;
	}
	public void setChoice(String choice) {
		this.choice = choice;
	}
}
