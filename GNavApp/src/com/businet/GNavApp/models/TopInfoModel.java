package com.businet.GNavApp.models;

import java.util.ArrayList;

public class TopInfoModel {
	private Integer statusCode;
	private ArrayList<NewsListModel> newsList;
	private Integer newsCount;
	private ArrayList<DtcNoticesListModel> dtcNoticesList;
	private Integer dtcCount;
	private ArrayList<DocumentListModel> documentList;
	private Integer documentCount;
	// **«««@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ «««**//
	private Integer dtcBadgeCount;
	// **ªªª@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ ªªª**//

	// **«««@2019/06/07 DucNKT  ^[ñ}V[ ÇÁ «««**//
	private ArrayList<ReturnMachineNoticeListModel> returnMachineNoticesList;
	private Integer returnMachineCount;
	private Integer returnMachineBadgeCount;
	// **ªªª@2019/06/07 DucNKT  ^[ñ}V[ ÇÁ ªªª**//
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public ArrayList<NewsListModel> getNewsList() {
		return newsList;
	}
	public void setNewsList(ArrayList<NewsListModel> newsList) {
		this.newsList = newsList;
	}

	public Integer getNewsCount() {
		return newsCount;
	}
	public void setNewsCount(Integer newsCount) {
		this.newsCount = newsCount;
	}

	public ArrayList<DtcNoticesListModel> getDtcNoticesList() {
		return dtcNoticesList;
	}
	public void setDtcNoticesList(ArrayList<DtcNoticesListModel> dtcNoticesList) {
		this.dtcNoticesList = dtcNoticesList;
	}

	public Integer getDtcCount() {
		return dtcCount;
	}
	public void setDtcCount(Integer dtcCount) {
		this.dtcCount = dtcCount;
	}



	public ArrayList<DocumentListModel> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(ArrayList<DocumentListModel> documentList) {
		this.documentList = documentList;
	}

	public Integer getDocumentCount() {
		return documentCount;
	}
	public void setDocumentCount(Integer documentCount) {
		this.documentCount = documentCount;
	}

	// **«««@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ «««**//
	public Integer getDtcBadgeCount() {
		return dtcBadgeCount;
	}
	public void setDtcBadgeCount(Integer dtcBadgeCount) {
		this.dtcBadgeCount = dtcBadgeCount;
	}
	// **ªªª@2018/07/19 LBNÎì  v]Î ¨CÉüèDTCðÌùÇ ÇÁ ªªª**//

	// **«««@2019/06/07 DucNKT  ^[ñ}V[ ÇÁ «««**//
	public ArrayList<ReturnMachineNoticeListModel> getReturnMachineNoticeList(){
		return returnMachineNoticesList;
	}
	public void setReturnMachineNoticeList(ArrayList<ReturnMachineNoticeListModel> returnMachineNoticeList) {
		this.returnMachineNoticesList = returnMachineNoticeList;
	}
	public Integer getReturnMachineCount() {
		return returnMachineCount;
	}
	public void setReturnMachineCount(Integer returnMachineCount) {
		this.returnMachineCount = returnMachineCount;
	}
	public Integer getReturnMachineBadgeCount() {
		return returnMachineBadgeCount;
	}
	public void setReturnMachineBadgeCount(Integer returnMachineBadgeCount) {
		this.returnMachineBadgeCount = returnMachineBadgeCount;
	}
	
	// **ªªª@2019/06/07 DucNKT  ^[ñ}V[ ÇÁ ªªª**//
}
