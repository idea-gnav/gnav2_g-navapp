package com.businet.GNavApp.models;


import javax.validation.constraints.NotNull;
import javax.ws.rs.FormParam;

public class MachineModel extends BaseModel {

	@NotNull
	@FormParam("manufacturerSN")
	String manufacturerSN;

	//@NotNull
	@FormParam("latestLocation")
	String latestLocation;


	@FormParam("hourMeter")
	double hourMeter;


	public String getManufacturerSN() {
		return manufacturerSN;
	}

	public void setManufacturerSN(String manufacturerSN) {
		this.manufacturerSN = manufacturerSN;
	}

	public String getLatestLocation() {
		return latestLocation;
	}

	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public double getHourMeter() {
		return hourMeter;
	}

	public void setHourMeter(double hourMeter) {
		this.hourMeter = hourMeter;
	}





}
