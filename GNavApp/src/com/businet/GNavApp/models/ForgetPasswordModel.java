package com.businet.GNavApp.models;

public class ForgetPasswordModel {
	
	private Integer statusCode;
	private String mailAddress;
	
	
	public ForgetPasswordModel(){
	}
	
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}


}
