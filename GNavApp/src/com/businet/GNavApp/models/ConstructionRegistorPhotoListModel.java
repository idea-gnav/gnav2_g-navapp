package com.businet.GNavApp.models;

public class ConstructionRegistorPhotoListModel {

	private Integer photoNo;
	private Integer imageEditFlg;
	private String photoDate;
	
	public Integer getPhotoNo() {
		return photoNo;
	}
	public void setPhotoNo(Integer photoNo) {
		this.photoNo = photoNo;
	}
	public Integer getImageEditFlg() {
		return imageEditFlg;
	}
	public void setImageEditFlg(Integer imageEditFlg) {
		this.imageEditFlg = imageEditFlg;
	}
	public String getPhotoDate() {
		return photoDate;
	}
	public void setPhotoDate(String photoDate) {
		this.photoDate = photoDate;
	}
	
}
