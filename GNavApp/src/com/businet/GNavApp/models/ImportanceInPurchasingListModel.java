package com.businet.GNavApp.models;

public class ImportanceInPurchasingListModel extends BaseModel {

	private int choiceId;
	private String itemNo;
	private String item;
	private int choiceCount;
	private int answerPercentage;
	private int freeInputFlg;

	public int getChoiceId() {
		return choiceId;
	}
	public void setChoiceId(int choiceId) {
		this.choiceId = choiceId;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public int getChoiceCount() {
		return choiceCount;
	}
	public void setChoiceCount(int choiceCount) {
		this.choiceCount = choiceCount;
	}
	public int getAnswerPercentage() {
		return answerPercentage;
	}
	public void setAnswerPercentage(int answerPercentage) {
		this.answerPercentage = answerPercentage;
	}
	public int getFreeInputFlg() {
		return freeInputFlg;
	}
	public void setFreeInputFlg(int freeInputFlg) {
		this.freeInputFlg = freeInputFlg;
	}

}
