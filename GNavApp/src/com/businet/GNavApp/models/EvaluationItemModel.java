package com.businet.GNavApp.models;

import java.util.ArrayList;

public class EvaluationItemModel {

	private Integer statusCode;
	private Integer surveyId;
	private String surveyName;
	private Integer targetFigures;
	private Integer achievementFigures;
	private String evaluationLvText;
	private ArrayList<CompetitorListArraylistModel> competitorList;
	private ArrayList<ChartListArraylistModel> chartList;

	public EvaluationItemModel() {

	}


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}
	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}
	public Integer getTargetFigures() {
		return targetFigures;
	}
	public void setTargetFigures(Integer targetFigures) {
		this.targetFigures = targetFigures;
	}
	public Integer getAchievementFigures() {
		return achievementFigures;
	}
	public void setAchievementFigures(Integer achievementFigures) {
		this.achievementFigures = achievementFigures;
	}
	public String getEvaluationLvText() {
		return evaluationLvText;
	}
	public void setEvaluationLvText(String evaluationLvText) {
		this.evaluationLvText = evaluationLvText;
	}
	public ArrayList<CompetitorListArraylistModel> getCompetitorList() {
		return competitorList;
	}
	public void setCompetitorList(ArrayList<CompetitorListArraylistModel> competitorList) {
		this.competitorList = competitorList;
	}
	public ArrayList<ChartListArraylistModel> getChartList() {
		return chartList;
	}
	public void setChartListArrayList(ArrayList<ChartListArraylistModel> chartList) {
		this.chartList = chartList;
	}
}
