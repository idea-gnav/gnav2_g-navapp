package com.businet.GNavApp.annotations;

import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

import java.lang.annotation.Retention;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@NameBinding
public @interface Authorized{
	String value();
}
