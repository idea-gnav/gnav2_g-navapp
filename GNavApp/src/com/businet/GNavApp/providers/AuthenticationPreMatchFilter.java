package com.businet.GNavApp.providers;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

@PreMatching
@Provider
public class AuthenticationPreMatchFilter implements ContainerRequestFilter {

	public AuthenticationPreMatchFilter() {

	}

	@Override
	public void filter(ContainerRequestContext requestContext) {
		// pre match container request do filter.....
	}
}