package com.businet.GNavApp.providers;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(urlPatterns={"/*"})
public class ExecuseTimeFilter  implements Filter{

	private static final Logger logger = Logger.getLogger(ExecuseTimeFilter.class.getName());


	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		long startTime = System.currentTimeMillis();

		chain.doFilter(request, response);

		StringBuilder logMes = new StringBuilder();
		HttpServletRequest httpSerlet = (HttpServletRequest)request;
		logMes.append("[GNAV][TIME][");
		logMes.append(httpSerlet.getRequestURI());
		logMes.append("] ");
		logMes.append("Execused time: "+ (System.currentTimeMillis() - startTime) + "ms");

		logger.info(logMes.toString());
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
