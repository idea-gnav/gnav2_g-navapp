package com.businet.GNavApp.providers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.businet.GNavApp.Constants;
import com.businet.GNavApp.models.ReturnContainer;

@Provider
public class ExceptionHandle implements ExceptionMapper<Exception>{
	
	public static final Logger logger = Logger.getLogger(ExceptionHandle.class.getName());

	@Override
	public Response toResponse(Exception ex) {
		
			logger.log(Level.SEVERE, ex.fillInStackTrace().toString());

			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
	}

}
