package com.businet.GNavApp;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * 文字列や数値の変換を行うユーティリティークラス
 */
public class ConvertUtil {

	private static final Logger logger = Logger.getLogger(ConvertUtil.class.getName());

	private ConvertUtil() {
	}

	/**
	 * 対応言語変換
	 */
	public static String convertLanguageCd(Integer deviceLanguage) {

		String languageCd = null;

		if (deviceLanguage == null)
			languageCd = "en";
		else if (deviceLanguage == 0)
			languageCd = "en";
//		else if(deviceLanguage==1)	// GNav未使用
//			languageCd ="pt";
//		else if(deviceLanguage==2)	// GNav未使用
//			languageCd ="es";
		else if (deviceLanguage == 3)
			languageCd = "ja";
		else if (deviceLanguage == 4)
			languageCd = "id";
		else if (deviceLanguage == 5)
			languageCd = "tr";
		else
			languageCd = "en";

		return languageCd;
	}

	// **↓↓↓ 2020/09/03 iDEA山下 SW2機能追加 ↓↓↓**//
	/**
	 * SW2対応言語変換
	 */
	public static String convertSw2LanguageCd(Integer deviceLanguage) {

		String languageCd = null;

		if (deviceLanguage == null)
			languageCd = "E";
		else if (deviceLanguage == 0)
			languageCd = "E";
//		else if(deviceLanguage==1)	// SW2未使用
//			languageCd ="pt";
//		else if(deviceLanguage==2)	// SW2未使用
//			languageCd ="es";
		else if (deviceLanguage == 3)
			languageCd = "J";
//		else if(deviceLanguage==4)	// SW2未使用
//			languageCd ="id";
//		else if(deviceLanguage==5)  // SW2未使用
//			languageCd ="tr";
		else
			languageCd = "E";

		return languageCd;
	}
	// **↑↑↑ 2020/09/03 iDEA山下 SW2機能追加 ↑↑↑**//

	/**
	 * 時間変換 整数（分）→時分
	 */
	public static String convertTimeHhMm(Double time) {

		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("00");

		if (time != null) {
			int hour = (int) (time / 60.0);
			int min = (int) (time % 60.0);

			sb.append(df.format(hour));
			sb.append(":");
			sb.append(df.format(min));
		}

		return new String(sb);
	}

	/**
	 * 緯度、経度変換 秒単位変換
	 */
	public static double parseLatLng(double latlng) {

		DecimalFormat df = new DecimalFormat("#.######");
		BigDecimal bd = new BigDecimal(df.format(latlng / 1000 / 3600));

		return bd.doubleValue();
	}

	/**
	 * 緯度、経度変換 秒単位変換
	 */
	public static double parseLatLng2(double latlng) {

		DecimalFormat df = new DecimalFormat("#.######");
		BigDecimal bd = new BigDecimal(df.format(latlng / 10000000));

		return bd.doubleValue();
	}

	/**
	 * システム日付 Date
	 * 
	 * @throws Exception
	 */
	public static Date currentDate() throws Exception {

		Date nowDate = new Date(System.currentTimeMillis());

		return nowDate;
	}

	/**
	 * 書式フォーマット（年月日）
	 */
	public static String formatYMD(Timestamp date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * 書式フォーマット（年月日時分）
	 */
	public static String formatYMDHM(Timestamp date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return sdf.format(date);
	}

	/**
	 * 書式フォーマット（年月日時分秒）
	 */
	public static String formatYMDHMS(Timestamp date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}

	/**
	 *
	 * 小数点第2位切り捨て
	 */
	public static double secoundDecimalPlaceRoundDown(double value) {
//		return new BigDecimal(df.format().doubleValue()
		DecimalFormat df = new DecimalFormat("#.#");

		df.format(Math.floor(value * 10) / 10);

		return 0.0;
	}

	/**
	 * 任意換算日付取得(JST)
	 * 
	 * @param ago
	 * @return :String yyyy-mm-dd
	 */
	public static String getDays(int ago) {

		SimpleDateFormat fmtYmd = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.add(Calendar.DATE, ago);

		return fmtYmd.format(cal.getTime());
	}

	/**
	 * 任意換算日付取得（UTC）
	 * 
	 * @param ago
	 * @return :String yyyy-mm-dd
	 */
	public static String getDaysUTC(int ago) {

		SimpleDateFormat fmtUTC = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.add(Calendar.DATE, ago);

		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		fmtUTC.setTimeZone(timeZone);

		return fmtUTC.format(cal.getTime());
	}

	/**
	 * 任意換算日付取得（UTC）
	 * 
	 * @param ago
	 * @return :String
	 */
	public static String getTimestampUTC(int ago) {

		SimpleDateFormat fmtUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.add(Calendar.DATE, ago);

		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		fmtUTC.setTimeZone(timeZone);

		return fmtUTC.format(cal.getTime());
	}

	
	/**
	 * 任意換算日付取得（UTC）
	 * 
	 * @param ago
	 * @return :String
	 */
	public static Timestamp convertToTimeStamp(String str) {
		try {
			SimpleDateFormat fmtUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			java.util.Date dt = fmtUTC.parse(str);
			return new Timestamp(dt.getTime());
			
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 数値チェック
	 *
	 */
	public static boolean isNumber(String num) {
		try {

			Integer.parseInt(num);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * 検索文字列置換（機番） 数値チェック後方一致、他前方一致
	 */
	public static String searchStrKiban(String str) {

		if (str != null) {
			boolean bMatch = str.matches("\\d{4}$");
			logger.config("bMatch=" + bMatch);

			str.toUpperCase();
			logger.config("searchStrKiban=" + str);

			if (str.contains("*")) {
				return str.replaceAll("\\*", "%");
			} else {

				if (bMatch) {
					return "%" + str;
				} else {
					return str + "%";
				}
			}

		} else {
			return null;
		}
	}

	/**
	 * 検索文字列置換（共通） あいまい検索
	 */
	public static String searchFuzzyStr(String str) {

		if (str != null) {
			if (str.contains("*")) {
				return str.replaceAll("\\*", "%");
			} else {
				return "%" + str + "%";
			}
		} else {
			return null;
		}
	}

	/**
	 * 検索文字列置換（共通） 前方一致のみ
	 */
	public static String searchStr(String str) {

		if (str != null) {

			str.toUpperCase();
			if (str.contains("*")) {
				return str.replaceAll("\\*", "%");
			} else {
				return str + "%";
			}
		} else {
			return null;
		}
	}

	/**
	 * SQLインジェクション対策 [']
	 */
	public static String rpStr(String param) {
		return param.replace("'", "''");
	}

	/**
	 * SQLクエリ IN 検索文字列変換 文字 'あり
	 */
	public static String queryInStr(String str) {

		String[] strList = str.split(",");
		StringBuilder sb = new StringBuilder();

		for (int index = 0; index < strList.length; index++) {
			sb.append("'" + strList[index] + "'");
			if (index < (strList.length - 1))
				sb.append(",");
		}
		return new String(sb);
	}

	/**
	 * SQLクエリ IN 検索文字列変換 数値 'なし
	 */
	public static String queryInNumber(String str) {

		String[] strList = str.split(",");
		StringBuilder sb = new StringBuilder();

		for (int index = 0; index < strList.length; index++) {
			sb.append(strList[index]);
			if (index < (strList.length - 1))
				sb.append(",");
		}
		return new String(sb);
	}

	/**
	 * SQLQuery return string query with input is number or text
	 */
	public static String getQueryWithValidateValue(Object obj) {
		String str = "";

		if (obj == null)
			return "NULL";

		if (obj instanceof Number)
			return obj.toString();

		else if (obj instanceof String)
			return "'" + obj.toString() + "'";

		return str;
	}

}
