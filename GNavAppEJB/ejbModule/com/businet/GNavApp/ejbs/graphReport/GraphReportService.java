package com.businet.GNavApp.ejbs.graphReport;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.MstMachine;







@Stateless
@Local(IGraphReportService.class)
public class GraphReportService implements IGraphReportService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/**
	 * JPA-JPQL
	 * findByMachine: 機械情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MstMachine findByMachine(Integer serialNumber) {
		em.getEntityManagerFactory().getCache().evictAll();
		List<MstMachine> listMachine = em.createQuery("Select m from MstMachine m WHERE m.kibanSerno=:param ")
				.setParameter("param", serialNumber)
				.getResultList();
		if(listMachine.size()>0)
			return (MstMachine)listMachine.get(0);
		else
			return null;

	}


	/**
	 * JPA-JPQL
	 * findByKigyou: 企業情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByKigyou(Integer serialNumber) {
		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result = em.createQuery("SELECT mk.kigyouName, ms.sosikiName "	//mk.kigyouName
				+ " FROM MstKigyou mk"
				+ " JOIN mk.mstSosikis ms"
				+ " JOIN ms.mstMachines mc"
				+ " WHERE mc.kibanSerno = :param")
				.setParameter("param",serialNumber )
				.getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * getUserSetting: ユーザー設定単位取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer getUserSetting(String userId) {
		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> rst = em.createQuery("Select u.unitSelect FROM TblUserSetting u WHERE u.userId =:param")
				.setParameter("param", userId).getResultList();
		if(rst.size()>0)
		return rst.get(0);
		return null;
	}


	/**
	 * JPA-JPQL
	 * getAllInfo: レポート情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getAllInfo(Integer serialNumber, Timestamp dateFrom, Timestamp dateTo) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result = em.createQuery("Select r.kikaiKadobiLocal,r.hourMeter,r.kikaiSosaTime,r.nenryoConsum,r.ureaWaterConsum, m "
				+ "from TblTeijiReport r JOIN r.mstMachine m "
				+ "WHERE (m.kibanSerno = :serialNumber AND r.kikaiKadobiLocal >= :dateFrom AND r.kikaiKadobiLocal <=:dateTo) "
				+ "Order by r.kikaiKadobiLocal")
					.setParameter("serialNumber", serialNumber)
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.getResultList();

		 return result;
	}





}
