package com.businet.GNavApp.ejbs.graphReport;

import java.sql.Timestamp;
import java.util.List;

import com.businet.GNavApp.entities.MstMachine;

public interface IGraphReportService {

	/**
	 * JPA-JPQL
	 * findByMachine: 機械情報取得
	 */
	 MstMachine findByMachine(Integer serialNumber);

	/**
	 * JPA-JPQL
	 * findByKigyou: 企業情報取得
	 */
	 List<Object[]> findByKigyou(Integer serialNumber);

	/**
	 * JPA-JPQL
	 * getUserSetting: ユーザー設定単位取得
	 */
	 Integer getUserSetting(String userId);

	/**
	 * JPA-JPQL
	 * getAllInfo: レポート情報取得
	 */
	 List<Object[]> getAllInfo(Integer serialNumber,Timestamp dateFrom,Timestamp dateTo);


//	 List<TblTeijiReport> findByReport(Integer serialNumber,String dateFrom,String dateTo);




}
