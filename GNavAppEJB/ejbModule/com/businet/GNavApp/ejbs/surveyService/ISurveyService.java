package com.businet.GNavApp.ejbs.surveyService;

import java.util.List;

import com.businet.GNavApp.entities.MstSurveyChoice;
import com.businet.GNavApp.entities.TblSurveyResult;
public interface ISurveyService{

	/**
	 * NativeQuery
	 * findSurveyNativeQuery アンケート一覧取得
	 */
	List<Object[]> findSurveyNativeQuery(Integer sortFlg ,Integer deviceLanguage);


	/**
	 * NativeQuery
	 * findSurveyResultNativeQueryById アンケート結果取得
	 */
	List<Object[]> findSurveyResultNativeQueryById(Integer surveyResultID);

	/**
	 * NativeQuery
	 * findSurveyResultById
	 */
	TblSurveyResult findSurveyResultById(Integer surveyResultID);

	/**
	 * NativeQuery
	 * findMstSurveyChoiceById
	 */
	MstSurveyChoice findMstSurveyChoiceById(Integer questionId, Integer choiceId);

	/**
	 * NativeQuery
	 * findChoiceNativeQuery 回答選択肢取得
	 */
	List<Object[]> findChoiceNativeQuery(Integer questionId,Integer languageCd,Integer deleteFlg);

	/**
	 * NativeQuery
	 * findQuestionIdOfMstSurveyQuestionBySurveyId アンケート親取得
	 */
	// **↓↓↓ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↓↓↓**//
	//List<Integer> findQuestionIdOfMstSurveyQuestionBySurveyId(Integer surveyId);
	List<Object[]> findQuestionIdOfMstSurveyQuestionBySurveyId(Integer surveyId);
	// **↑↑↑ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↑↑↑**//

	/**
	 * NativeQuery
	 * findQuestionAndAnswerBySurveyResultID アンケート設問回答取得
	 */
	List<Object[]> findQuestionAndAnswerBySurveyResultID(Integer surveyResultId,Integer languageCd,Integer questionId);

	/**
	 * NativeQuery
	 * findAnswerChoiceIdByQuestionIDAndSurveyResultID 複数選択回答結果取得
	 */
	List<Integer> findAnswerChoiceIdByQuestionIDAndSurveyResultID(Integer surveyResultId, Integer questionId);

	/**
	 * NativeQuery
	 * findParentChoiceIdAndChildQuestionIdByParentQuestionId 下位設問チェック
	 */
	List<Object[]> findParentChoiceIdAndChildQuestionIdByParentQuestionId(Integer parentQuestionId);

	/**
	 * NativeQuery
	 * findSurveyHistoryListByMachine アンケート一覧取得
	 */
	List<Object[]> findSurveyHistoryListByMachine(Long kibanSerno,String inputDateFrom, String inputDateTo,int sortFlg,int languageCd);

	/**
	 * NativeQuery
	 * deleteSurveyResult
	 */
	Integer deleteSurveyResult(Integer surveyResultId,String userId);

	/**
	 * NativeQuery
	 * getMaxSurveyResultId
	 */
	Integer getMaxSurveyResultId();

	/**
	 * NativeQuery
	 * registerSurvey
	 */
	// **↓↓↓ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↓↓↓**//
//	Integer registerSurvey(Integer surveyResultId,Long kibanSerno,Integer surveyId,String kigyouName,String answerUser,String position,String userId,List<Object[]>surveyDetails,Double hourMeter,String inputDate);
	Integer registerSurvey(Integer surveyResultId,Long kibanSerno,Integer surveyId,String kigyouName
			,String answerUser,String position,String userId,List<Object[]>surveyDetails,Double hourMeter,String inputDate,String languageCd);
	// **↑↑↑ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↑↑↑**//

	/**
	 * NativeQuery
	 * updateSurveyImage
	 */
	Integer updateSurveyImage(String fileName, String userId,Integer surveyResultId, Integer questionId);

	/**
	 * NativeQuery
	 * findByKigyouName: 所有会社名取得
	 */
	String findByKigyouName(String sosikiCd);

	/**
	 * NativeQuery
	 * surveyEditableCheck
	 */
//	Integer surveyEditableCheck(Integer surveyId);
	// **↓↓↓ 2019/09/18 iDEA Nakata　各アンケートの登録ユーザーのみ再編集できるよう修正 ↓↓↓**//
	Integer surveyEditableCheck(Integer surveyId, Integer surveyResultId, String userId);
	// **↑↑↑ 2019/09/18 iDEA Nakata　各アンケートの登録ユーザーのみ再編集できるよう修正 ↑↑↑**//

	// **↓↓↓ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↓↓↓**//
	String insertLastPosition(Integer surveyResultIdReturn, Long kibanSerno, Double ido, Double keido, String userId);
	// **↑↑↑ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↑↑↑**//

}
