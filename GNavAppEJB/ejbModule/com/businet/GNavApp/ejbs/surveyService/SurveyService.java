package com.businet.GNavApp.ejbs.surveyService;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.entities.MstSurveyChoice;
import com.businet.GNavApp.entities.TblSurveyResult;

@Stateless
@Local(ISurveyService.class)
public class SurveyService implements ISurveyService {

	@PersistenceContext(unitName="SW2")//2020.08.04 Rasis Duc modified
	EntityManager em;

	/**
	 * NativeQuery findSurveyNativeQuery
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findSurveyNativeQuery(Integer sortFlg, Integer deviceLanguage) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT MS.SURVEY_ID, ");
		sb.append("MS.SURVEYU_NAME" + deviceLanguage + ", ");
		sb.append("MS.INPUT_FROM, ");
		sb.append("MS.INPUT_TO ");
		sb.append("FROM MST_SURVEY MS ");
		sb.append("WHERE MS.DELETE_FLG = 0 ");
		sb.append("AND MS.APP_FLG = 1 ");
		sb.append("AND TO_CHAR(MS.INPUT_FROM,'yyyy/mm/dd') <= SYSDATE ");
		sb.append("AND TO_CHAR(NVL(MS.INPUT_TO,'9999-12-31'),'yyyy/mm/dd') >= SYSDATE ");
		sb.append("AND TO_CHAR(NVL(MS.INPUT_TO,'9999-12-31'),'yyyy/mm/dd') >= SYSDATE ");
		sb.append("AND ROWNUM <= 1001 ");
		sb.append("ORDER BY ");
		if (sortFlg == 0) {
			sb.append("MS.INPUT_FROM,MS.INPUT_TO,MS.SURVEY_ID ");
		} else if (sortFlg == 1) {
			sb.append("MS.INPUT_FROM DESC,MS.INPUT_TO DESC,MS.SURVEY_ID DESC ");
		} else if (sortFlg == 2) {
			sb.append("MS.SURVEYU_NAME" + deviceLanguage + ",MS.SURVEY_ID ");
		} else if (sortFlg == 3) {
			sb.append("MS.SURVEYU_NAME" + deviceLanguage + " DESC,MS.SURVEY_ID DESC ");
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> surveyList = em.createNativeQuery(sb.toString()).getResultList();

		return surveyList;
	}

	@Override
	public List<Object[]> findSurveyResultNativeQueryById(Integer surveyResultID) {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("TSR.KIGYOU_NAME ");
		sb.append(",TSR.ANSWER_USER ");
		sb.append(",TSR.POSITION ");
		sb.append(",TSR.SURVEY_DATE ");
		sb.append(",TSR.HOUR_METER ");
		sb.append("FROM TBL_SURVEY_RESULT TSR ");
		sb.append("WHERE TSR.SURVEY_RESULT_ID = " + surveyResultID + " ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;

	}

	@Override
	public TblSurveyResult findSurveyResultById(Integer surveyResultID) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<TblSurveyResult> result = em
				.createQuery("SELECT m FROM TblSurveyResult m " + "WHERE m.surveyResultId=:surveyResultId ")
				.setParameter("surveyResultId", surveyResultID).getResultList();

		em.clear();

		if (result.size() > 0)
			return (TblSurveyResult) result.get(0);
		else
			return null;

	}

	@Override
	public MstSurveyChoice findMstSurveyChoiceById(Integer questionId, Integer choiceId) {

		em.getEntityManagerFactory().getCache().evictAll();

		String sql = "SELECT m FROM MstSurveyChoice m WHERE m.questionId =" + questionId;
		if (choiceId != null)
			sql += "AND choiceId=" + choiceId;

		List<MstSurveyChoice> result = em.createQuery(sql).getResultList();

		em.clear();

		if (result.size() > 0)
			return (MstSurveyChoice) result.get(0);
		else
			return null;

	}

	/**
	 * NativeQuery
	 * findChoiceNativeQuery 回答選択肢取得
	 */
	@Override
	public List<Object[]> findChoiceNativeQuery(Integer questionId, Integer languageCd, Integer deleteFlg) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT MSC.CHOICE_ID, MSC.CHOICE" + languageCd + " ");
		sb.append("FROM MST_SURVEY_CHOICE MSC ");
		sb.append("WHERE ");
		sb.append("QUESTION_ID =" + questionId + " ");
		if (deleteFlg != null)
			sb.append("AND MSC.DELETE_FLG =" + deleteFlg + " ");
		sb.append("ORDER BY MSC.DISPLAY_SORT ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}

	/**
	 * NativeQuery
	 * findQuestionIdOfMstSurveyQuestionBySurveyId アンケート親取得
	 */
	@Override
	// **↓↓↓ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↓↓↓**//
	//public List<Ineger> findQuestionIdOfMstSurveyQuestionBySurveyId(Integer surveyId) {
	public List<Object[]> findQuestionIdOfMstSurveyQuestionBySurveyId(Integer surveyId) {
	// **↑↑↑ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↑↑↑**//
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT  MSQ.QUESTION_ID, ");
		sb.append("MSQ.QUESTION_PATTERN ");
		sb.append("FROM MST_SURVEY_QUESTION MSQ ");
		sb.append("WHERE ");
		sb.append("MSQ.SURVEY_ID =" + surveyId + " ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");
		sb.append("AND MSQ.PARENT_FLG = 1 ");
		sb.append("ORDER BY MSQ.DISPLAY_SORT ");

		em.getEntityManagerFactory().getCache().evictAll();
		// **↓↓↓ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↓↓↓**//
		//List<Integer> rs = em.createNativeQuery(sb.toString()).getResultList();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();
		// **↑↑↑ 2019/09/12 iDEA山下 サブタイトル追加要望対応 ↑↑↑**//

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}

	/**
	 * NativeQuery
	 * findQuestionAndAnswerBySurveyResultID アンケート設問回答取得
	 */
	@Override
	public List<Object[]> findQuestionAndAnswerBySurveyResultID(Integer surveyResultId, Integer languageCd,
			Integer questionId) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT MSQ.QUESTION_ID ");
		sb.append(",MSQ.QUESTION" + languageCd + " ");
		sb.append(",MSQ.QUESTION_PATTERN ");
		sb.append(",MSQ.PARENT_FLG ");
		sb.append(",MSQ.FREE_INPUT_FLG ");
		sb.append(",MSQ.FREE_INPUT_CAPTION" + languageCd + " ");
		sb.append(",MSQ.FREE_INPUT_CHOICE_ID ");
		sb.append(",MSQ.MUST_ANSWER_FLG ");
		sb.append(",MSQ.MULTI_CHOICE_MAX ");
		sb.append(",TSD.ANSWER_CHOICE_ID ");
		sb.append(",TSD.ANSWER_FREEINPUT ");
		sb.append(",TSD.IMAGE_FILENAME ");
		sb.append("FROM MST_SURVEY_QUESTION MSQ ");
		sb.append("LEFT OUTER JOIN TBL_SURVEY_DETAIL TSD ");
		sb.append("ON TSD.SURVEY_RESULT_ID =" + surveyResultId + " ");
		sb.append("AND TSD.QUESTION_ID = MSQ.QUESTION_ID ");
		sb.append("AND TSD.SEQ = 1 ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("WHERE MSQ.QUESTION_ID = " + questionId + " ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}

	/**
	 * NativeQuery
	 * findQuestionAndAnswerBySurveyResultID 複数選択回答結果取得
	 */
	@Override
	public List<Integer> findAnswerChoiceIdByQuestionIDAndSurveyResultID(Integer surveyResultId, Integer questionId) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT TSD.ANSWER_CHOICE_ID ");
		sb.append("FROM TBL_SURVEY_DETAIL TSD ");
		sb.append("WHERE ");
		sb.append("TSD.SURVEY_RESULT_ID =" + surveyResultId + " ");
		sb.append("AND TSD.QUESTION_ID =" + questionId + " ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("ORDER BY TSD.SEQ ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}

	/**
	 * NativeQuery
	 * findParentChoiceIdAndChildQuestionIdByParentQuestionId 下位設問チェック
	 */
	@Override
	public List<Object[]> findParentChoiceIdAndChildQuestionIdByParentQuestionId(Integer parentQuestionId) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT MSQR.CHILD_QUESTION_ID ");
		sb.append(",MSQR.PARENT_CHOICE_ID ");
		sb.append("FROM MST_SURVEY_QUESTION_RELATION MSQR ");
		sb.append("INNER JOIN MST_SURVEY_QUESTION MSQ  ");
		sb.append("ON MSQ.QUESTION_ID = MSQR.CHILD_QUESTION_ID ");
		sb.append("WHERE ");
		sb.append("MSQR.PARENT_QUESTION_ID =" + parentQuestionId + " ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");
		sb.append("ORDER BY  MSQ.DISPLAY_SORT, MSQR.PARENT_CHOICE_ID ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}


	/**
	 * NativeQuery
	 * findSurveyHistoryListByMachine アンケート一覧取得
	 */
	@Override
	public List<Object[]> findSurveyHistoryListByMachine(Long kibanSerno, String inputDateFrom, String inputDateTo,
			int sortFlg, int languageCd) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TSR.SURVEY_RESULT_ID "); // 0
		sb.append(",TSR.SURVEY_ID "); // 1
		sb.append(",MS.SURVEYU_NAME" + languageCd + " "); // 2
		sb.append(",TSR.SURVEY_DATE "); // 3
		sb.append(",MSC.CHOICE" + languageCd + " "); // 4
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("INNER JOIN MST_SURVEY MS ");
		sb.append("ON MS.SURVEY_ID = TSR.SURVEY_ID ");
		sb.append("LEFT JOIN MST_SURVEY_CHOICE MSC ");
		sb.append("ON ( MSC.CHOICE_ID = TSR.POSITION AND MSC.QUESTION_ID = -1 ) ");
		sb.append("WHERE ");
		sb.append("TSR.KIBAN_SERNO =" + kibanSerno + " ");

		if (inputDateFrom != null && inputDateTo == null) {
			sb.append("AND TSR.SURVEY_DATE ");
			sb.append(">= TO_DATE('" + inputDateFrom + "', 'yyyy/MM/dd') ");
		}

		if (inputDateFrom != null && inputDateTo != null) {
			sb.append("AND TSR.SURVEY_DATE ");
			sb.append("BETWEEN TO_DATE('" + inputDateFrom + "', 'yyyy/MM/dd') ");
			sb.append("AND TO_DATE('" + inputDateTo + "', 'yyyy/MM/dd') ");
		}

		if (inputDateFrom == null && inputDateTo != null) {
			sb.append("AND TSR.SURVEY_DATE ");
			sb.append("<= TO_DATE('" + inputDateTo + "', 'yyyy/MM/dd') ");
		}

		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MS.APP_FLG =1 ");
		sb.append("AND MS.DELETE_FLG = 0 ");
		sb.append("AND ROWNUM <= 1001 ");


		sb.append("ORDER BY ");
		if (sortFlg == 0)
			sb.append("TSR.SURVEY_DATE, TSR.SURVEY_RESULT_ID ");
		else if (sortFlg == 1)
			sb.append("TSR.SURVEY_DATE DESC, TSR.SURVEY_RESULT_ID DESC ");
		else if (sortFlg == 2)
			sb.append("MS.SURVEYU_NAME" + languageCd + " ");
		else if (sortFlg == 3)
			sb.append("MS.SURVEYU_NAME" + languageCd + " DESC ");
		else if (sortFlg == 4)
			sb.append("TSR.POSITION ");
		else if (sortFlg == 5)
			sb.append("TSR.POSITION DESC");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}

	@Override
	public Integer deleteSurveyResult(Integer surveyResultId, String userId) {

		em.getEntityManagerFactory().getCache().evictAll();

		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE TBL_SURVEY_RESULT ");
		sb.append("SET ");
		sb.append("DELETE_FLG = 1 ");
		sb.append(",UPDATE_PRG = 'GNavApp' ");
		sb.append(",UPDATE_DTM = SYSDATE ");
		sb.append(",UPDATE_USER ='" + userId + "' ");
		sb.append("WHERE ");
		sb.append("SURVEY_RESULT_ID = " + surveyResultId + " ");

		em.createNativeQuery(sb.toString()).executeUpdate();

		sb = new StringBuilder();
		sb.append("UPDATE TBL_SURVEY_DETAIL ");
		sb.append("SET ");
		sb.append("DELETE_FLG = 1 ");
		sb.append(",UPDATE_PRG = 'GNavApp' ");
		sb.append(",UPDATE_DTM = SYSDATE ");
		sb.append(",UPDATE_USER ='" + userId + "' ");
		sb.append("WHERE ");
		sb.append("SURVEY_RESULT_ID = " + surveyResultId + " ");

		em.createNativeQuery(sb.toString()).executeUpdate();

		return 1;
	}

	@Override
	public Integer getMaxSurveyResultId() {

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> result = em.createQuery("SELECT  MAX(m.surveyResultId) FROM TblSurveyResult m ").getResultList();
		em.clear();

		if (result.size() > 0)
			return (Integer) result.get(0);
		else
			return null;
	}

	// **↓↓↓ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↓↓↓**//
	@Override
//	public Integer registerSurvey(Integer surveyResultId, Long kibanSerno, Integer surveyId, String kigyouName,
//			String answerUser, String position, String userId, List<Object[]> surveyDetails, Double hourMeter, String inputDate) {

	public Integer registerSurvey(Integer surveyResultId, Long kibanSerno, Integer surveyId, String kigyouName,
			String answerUser, String position, String userId, List<Object[]> surveyDetails, Double hourMeter, String inputDate,String languageCd) {
	// **↑↑↑ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↑↑↑**//
		Integer regisSurveyResultId = surveyResultId;
		StringBuilder sb = new StringBuilder();
		List<Object[]> BackUpLastImagePath = new ArrayList<Object[]>();

		// Insert
		if (regisSurveyResultId == null) {

			List<Integer> result = em.createQuery("SELECT  MAX(m.surveyResultId) FROM TblSurveyResult m ")
					.getResultList();

			if (result != null && result.size() > 0 && result.get(0) != null)
				regisSurveyResultId = result.get(0) + 1;
			else
				regisSurveyResultId = 1;

			sb.append("INSERT INTO TBL_SURVEY_RESULT ( ");
			sb.append("SURVEY_RESULT_ID ");
			sb.append(",KIBAN_SERNO ");
			sb.append(",SURVEY_ID ");
			sb.append(",KIGYOU_NAME ");
			sb.append(",ANSWER_USER ");
			sb.append(",POSITION ");
			sb.append(",SURVEY_DATE ");
			sb.append(",DELETE_FLG ");
			sb.append(",REGIST_PRG ");
			sb.append(",REGIST_DTM ");
			sb.append(",REGIST_USER ");
			sb.append(",UPDATE_PRG ");
			sb.append(",UPDATE_DTM ");
			sb.append(",UPDATE_USER ");
			sb.append(",HOUR_METER ");
			// **↓↓↓ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↓↓↓**//
			sb.append(",DEVICE_LANGUAGE ");
			// **↑↑↑ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↑↑↑**//
			sb.append(") VALUES ( ");
			sb.append(regisSurveyResultId + " ");
			sb.append("," + kibanSerno + " ");
			sb.append("," + surveyId + " ");
			sb.append("," + ConvertUtil.getQueryWithValidateValue(kigyouName) + " ");
			sb.append("," + ConvertUtil.getQueryWithValidateValue(answerUser) + " ");
			sb.append("," + ConvertUtil.getQueryWithValidateValue(position) + " ");
			sb.append(",'" + inputDate + "' ");
			sb.append(",0 ");
			sb.append(",'GNavApp' ");
			sb.append(",SYSDATE ");
			sb.append(",'" + userId + "' ");
			sb.append(",'GNavApp' ");
			sb.append(",SYSDATE ");
			sb.append(",'" + userId + "' ");
			sb.append("," + hourMeter + " ");
			// **↓↓↓ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↓↓↓**//
			sb.append(",'" + languageCd + "' ");
			// **↑↑↑ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↑↑↑**//
			sb.append(") ");

			em.createNativeQuery(sb.toString()).executeUpdate();
		}
		// Update
		else {

			sb.append("UPDATE TBL_SURVEY_RESULT SET ");
			sb.append("KIBAN_SERNO = " + kibanSerno + " ");
			sb.append(",SURVEY_ID = " + surveyId + " ");
			sb.append(",KIGYOU_NAME = " +ConvertUtil.getQueryWithValidateValue(kigyouName)+ " ");
			sb.append(",ANSWER_USER = " +ConvertUtil.getQueryWithValidateValue(answerUser)+ " ");
			sb.append(",POSITION = " +ConvertUtil.getQueryWithValidateValue(position)+ " ");
			sb.append(",SURVEY_DATE = '" + inputDate + "' ");
			sb.append(",DELETE_FLG = 0");
			sb.append(",UPDATE_PRG = 'GNavApp' ");
			sb.append(",UPDATE_DTM = SYSDATE ");
			sb.append(",UPDATE_USER = '" + userId + "' ");
			// **↓↓↓ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↓↓↓**//
			sb.append(",DEVICE_LANGUAGE = '" + languageCd + "' ");
			// **↑↑↑ 2019/10/17 iDEA山下 アンケート回答/更新時最新デバイス言語保持 追加 ↑↑↑**//
			sb.append("WHERE SURVEY_RESULT_ID = " + regisSurveyResultId + " ");

			em.createNativeQuery(sb.toString()).executeUpdate();

			sb = new StringBuilder();
			sb.append("SELECT QUESTION_ID, IMAGE_FILENAME ");
			sb.append("FROM TBL_SURVEY_DETAIL ");
			sb.append("WHERE SURVEY_RESULT_ID = " + regisSurveyResultId + " ");
			sb.append("AND SEQ = 1 AND IMAGE_FILENAME IS NOT NULL ");

			BackUpLastImagePath = em.createNativeQuery(sb.toString()).getResultList();

			sb = new StringBuilder();
			sb.append("DELETE FROM TBL_SURVEY_DETAIL ");
			sb.append("WHERE SURVEY_RESULT_ID = " + regisSurveyResultId + " ");

			em.createNativeQuery(sb.toString()).executeUpdate();

		}

		// INSERT INTO TBL_SURVEY_DETAIL
		if (surveyDetails != null && surveyDetails.size() > 0) {
			for (Object[] question : surveyDetails) {
				Integer questionId = (Integer) question[0];
				String answerFreeInput = question[2] != null ? question[2].toString() : "";
				String answerFreeInputEnglish = question[3] != null ? question[3].toString() : "";
				String path = null;

				if(BackUpLastImagePath != null && BackUpLastImagePath.size() > 0 && (Integer)question[4] == 0)
				{
					for (Object[] pathObj : BackUpLastImagePath) {
						if(((Number) pathObj[0]).intValue() == questionId)
							path = pathObj[1].toString();
					}
				}

				if (question[1] != null) {
					int[] answerChoiceIds = (int[]) question[1];
					for (int i = 0; i < answerChoiceIds.length; i++) {
						String sql = getNativeInsertTblSurveyDetailsQuery(regisSurveyResultId, questionId, i + 1,
								answerChoiceIds[i], answerFreeInput, answerFreeInputEnglish, userId,path);
						em.createNativeQuery(sql).executeUpdate();
					}
				} else {
					String sql = getNativeInsertTblSurveyDetailsQuery(regisSurveyResultId, questionId, 1, null,
							answerFreeInput, answerFreeInputEnglish, userId,path);
					em.createNativeQuery(sql).executeUpdate();
				}
			}
		}
		return regisSurveyResultId;
	}

	// **↓↓↓ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↓↓↓**//
	public String insertLastPosition(Integer surveyResultIdReturn, Long kibanSerno, Double ido, Double keido, String userId) {
		StringBuilder sb = new StringBuilder();

		sb.append("INSERT INTO TBL_SURVEY_POSITION_LOG (");
		sb.append("SURVEY_RESULT_ID ");
		sb.append(",SURVEY_DATE ");
		sb.append(",KIBAN_SERNO ");
		sb.append(",IDO ");
		sb.append(",KEIDO ");
		sb.append(",REGIST_PRG ");
		sb.append(",REGIST_DTM ");
		sb.append(",REGIST_USER ");
		sb.append(",UPDATE_PRG ");
		sb.append(",UPDATE_DTM ");
		sb.append(",UPDATE_USER ");
		sb.append(") VALUES ( ");
		sb.append(surveyResultIdReturn + " ");
		sb.append(",SYSDATE ");
		sb.append("," + kibanSerno + " ");
		sb.append(","+ ido * 3600 * 1000 + " ");
		sb.append(","+ keido * 3600 * 1000 + " ");
		sb.append(",'GNavApp' ");
		sb.append(",SYSDATE ");
		sb.append(",'" + userId + "' ");
		sb.append(",'GNavApp' ");
		sb.append(",SYSDATE ");
		sb.append(",'" + userId + "' ");
		sb.append(") ");

		em.createNativeQuery(sb.toString()).executeUpdate();

		return sb.toString();
	}

	// **↑↑↑ 2019/9/18 RASIS岡本 アンケート回答位置セット処理追加↑↑↑**//

	String getNativeInsertTblSurveyDetailsQuery(Integer surveyResultId, Integer questionId, Integer seq,
			Integer answerChoiceId, String answerFreeInput, String answerFreeInputEnglish, String userId, String oldPath) {
		StringBuilder sb = new StringBuilder();

		sb.append("INSERT INTO TBL_SURVEY_DETAIL (");
		sb.append("SURVEY_RESULT_ID ");
		sb.append(",QUESTION_ID ");
		sb.append(",SEQ ");
		sb.append(",ANSWER_CHOICE_ID ");
		sb.append(",ANSWER_FREEINPUT ");
		sb.append(",ANSWER_FREEINPUT_ENGLISH ");
		sb.append(",IMAGE_FILENAME ");
		sb.append(",DELETE_FLG ");
		sb.append(",REGIST_PRG ");
		sb.append(",REGIST_DTM ");
		sb.append(",REGIST_USER ");
		sb.append(",UPDATE_PRG ");
		sb.append(",UPDATE_DTM ");
		sb.append(",UPDATE_USER ");
		sb.append(") VALUES ( ");
		sb.append(surveyResultId + " ");
		sb.append("," + questionId + " ");
		sb.append("," + seq + " ");
		sb.append(","+ ConvertUtil.getQueryWithValidateValue(answerChoiceId) + " ");
		sb.append(","+ ConvertUtil.getQueryWithValidateValue(answerFreeInput) + " ");
		sb.append(","+ ConvertUtil.getQueryWithValidateValue(answerFreeInputEnglish) + " ");
		sb.append(","+ConvertUtil.getQueryWithValidateValue(oldPath) + " ");
		sb.append(",0 ");
		sb.append(",'GNavApp' ");
		sb.append(",SYSDATE ");
		sb.append(",'" + userId + "' ");
		sb.append(",'GNavApp' ");
		sb.append(",SYSDATE ");
		sb.append(",'" + userId + "' ");
		sb.append(") ");

		return sb.toString();
	}

	@Override
	public Integer updateSurveyImage(String fileName, String userId, Integer surveyResultId, Integer questionId) {

		em.getEntityManagerFactory().getCache().evictAll();

		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE TBL_SURVEY_DETAIL ");
		sb.append("SET ");
		sb.append("IMAGE_FILENAME = " + ConvertUtil.getQueryWithValidateValue(fileName)+ " ");
		sb.append(",UPDATE_PRG = 'GNavApp' ");
		sb.append(",UPDATE_DTM = SYSDATE ");
		sb.append(",UPDATE_USER ='" + userId + "' ");
		sb.append("WHERE ");
		sb.append("SURVEY_RESULT_ID = " + surveyResultId + " ");
		sb.append("AND QUESTION_ID  = " + questionId + " ");
		sb.append("AND SEQ = 1");

		return em.createNativeQuery(sb.toString()).executeUpdate();

	}


	/**
	 * NativeQuery
	 * findByKigyouName: 所有会社名取得
	 */
	@Override
	public String findByKigyouName(String sosikiCd) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.KIGYOU_NAME ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("AND MS.SOSIKI_CD = '"+sosikiCd+"' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;
	}



	/**
	 * NativeQuery
	 * surveyEditableCheck: アンケート編集可否チェック
	 */

	@Override
	public Integer surveyEditableCheck(Integer surveyId, Integer surveyResultId, String userId) {
		StringBuilder sb = new StringBuilder();

		// **↓↓↓ 2019/09/18 iDEA Nakata　各アンケートの登録ユーザーのみ再編集できるよう修正 ↓↓↓**//
		if(surveyResultId != null) {
			//更新
			sb.append("SELECT MS.SURVEY_ID ");
			sb.append("FROM TBL_SURVEY_RESULT TSR ");
			sb.append("JOIN MST_SURVEY MS ");
			sb.append("ON MS.SURVEY_ID = TSR.SURVEY_ID ");
			sb.append("WHERE MS.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND TSR.SURVEY_RESULT_ID = " + surveyResultId + " ");
			sb.append("AND MS.INPUT_TO >= SYSDATE ");
			sb.append("AND TSR.REGIST_USER = '" + userId + "' ");
		// **↑↑↑ 2019/09/18 iDEA Nakata　各アンケートの登録ユーザーのみ再編集できるよう修正 ↑↑↑**//

		}else {
			//新規
			sb.append("SELECT SURVEY_ID ");
			sb.append("FROM MST_SURVEY ");
			sb.append("WHERE DELETE_FLG = 0 ");
			sb.append("AND SURVEY_ID = " + surveyId + " ");
			sb.append("AND INPUT_TO >= SYSDATE ");
		}

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Integer> result =  em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return 0; //編集可
		else
			return 1; //参照のみ
	}

}
