package com.businet.GNavApp.ejbs.construction;

import java.util.List;

import com.businet.GNavApp.entities.TblSekoInfo;

public interface IConstructionService{

//48.constructionDetail({HîñÚ×æ¾)
	/**
	 * NativeQuery
	 * findMachineInfo : @Bîñæ¾
	 */
	List<Object[]> findMachineInfo(Long serialNumber,Integer constructionResultId);


	/**
	 * NativeQuery
	 * getUserName : ²¸Ò¼æ¾
	 */
	String getUserName(String userId,Long serialNumber,Integer constructionResultId);


	/**
	 * NativeQuery
	 * constructionEditableCheck : ÒWÂÛ`FbN
	 */
	Integer constructionEditableCheck(Integer constructionResultId, String userId);


	/**
	 * NativeQuery
	 * findConstructionResultById : {Hîñæ¾
	 */
	TblSekoInfo findConstructionResultById(Integer constructionResultId);


	/**
	 * NativeQuery
	 * findCountryList : îñæ¾
	 */
	List<Object[]> findCountryList(Integer deviceLanguage);

	/**
	 * NativeQuery
	 * findMaterialTypeList : ÞíÞæ¾
	 */
	List<Object[]> findMaterialTypeList(Integer deviceLanguage,Integer countryId);

	/**
	 * NativeQuery
	 * findWorkTypeList : HíÞæ¾
	 */
	List<Object[]> findWorkTypeList(Integer deviceLanguage,Integer countryId);

	/**
	 * NativeQuery
	 * findPhotoList : ææ¾
	 */
	List<Object[]> findPhotoList(Long serialNumber,Integer constructionResultId);

	/**
	 * NativeQuery
	 * findPavementPdfList : {HsïPDFæ¾
	 */
	List<Object[]> findPavementPdfList(String scmModel,String deviceLanguage);


//49.constructionHistoryList({Hîñðê)
	/**
	 * NativeQuery
	 * findConstructionHistoryList : {Hîñðæ¾
	 */
	List<Object[]> findConstructionHistoryList(Long serialNumber,Integer sortFlg);


//50.constructionRegistor({Hîño^)
	/**
	 * NativeQuery
	 * registerConstruction : {Hîño^
	 */
	Integer registerConstruction(
			String userId,
			Integer deviceLanguage,
			Integer reportDelFlg,
			Integer reportNo,
			Long serialNumber,
			Double hourmeter,
			String researcher,
			String researchDate,
			String researchLocation,
			Double workVolume,
			Integer workVolumeUnit,
			Integer reportCountry,
			Integer materialType,
			Integer materialTemperatureUse,
			Double materialTemperature,
			Integer materialTemperatureUnit,
			Double outsideAirTemperature,
			Integer outsideAirTemperatureUnit,
			Double plateTemperature,
			Integer plateTemperatureUnit,
			Integer workType,
			String spotSituation,
			Integer pavementDistanceUse,
			Double pavementDistance,
			Double pavementWidth,
			Double pavementWidthTo,//2020.01.14 DucNKT added
			Double pavementThicknessLevelingL,
			Double pavementThicknessLevelingC,
			Double pavementThicknessLevelingR,
			Double constructionSpeed,
			Double levelingCylinderScaleL,
			Double levelingCylinderScaleR,
			Double manualSixnessScaleL,
			Double manualSixnessScaleR,
			Integer manualSixnessScaleUnit,
			Double stepAdjustmentScaleL,
			Double stepAdjustmentScaleR,
			Double crown,
			Double slopeCrownL,
			Double slopeCrownR,
			Double stretchMoldBoardHeightL,
			Double stretchMoldBoardHeightR,
			Integer tampaRotationsUse,
			Double tampaRotations,
			Integer tampaAutoFunction,
			Double vibratorRotations,
			Double screwHeight,
			Double screwSpeedScaleL,
			Double screwSpeedScaleR,
			Double conveyorSpeedScaleL,
			Double conveyorSpeedScaleR,
			Integer screwflowControlUse,
			Integer agcUse,
			Integer agcType,
			Integer extensionScrewUse,
			String extensionScrew,
			Integer pavementTrouble1,
			Integer pavementTrouble2,
			Integer pavementTrouble3,
			Integer pavementTrouble4,
			Integer pavementTrouble5,
			Integer pavementTrouble6,
			Integer pavementTrouble7,
			Integer pavementTrouble8,
			Integer pavementTrouble9,
			Integer pavementTrouble10,
			Integer pavementTrouble11,
			Integer pavementTrouble12,
			Integer pavementTrouble13,
			Integer pavementTrouble14,
			Double tonnageMixedMaterials,
			String customerEvaluation,
			Double rearSlopeL,
			Double rearSlopeR,
			Double rearAttackShrinkL,
			Double rearAttackShrinkR,
			Double rearAttackMiddleL,
			Double rearAttackMiddleR,
			Double rearAttackStretchL,
			Double rearAttackStretchR,
			List<Object[]> constphotoList
			);

	/**
	 * NativeQuery
	 * updateConstructionResultPhoto : {Hîñæo^
	 */
	Integer updateConstructionResultPhoto(String fileName,String userId,Integer constructionResultId,Integer photoNo);
}
