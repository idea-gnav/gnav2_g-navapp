package com.businet.GNavApp.ejbs.construction;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.entities.TblSekoInfo;


@Stateless
@Local(IConstructionService.class)
public class ConstructionService implements IConstructionService {


	@PersistenceContext(unitName="GNavApp")
	EntityManager em;


//48.constructionDetail({HîñÚ×æ¾)
	/**
	 * NativeQuery findMachineInfo
	 * findMachineInfo : @Bîñæ¾
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findMachineInfo(Long serialNumber,Integer constructionResultId) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MM.MODEL_CD, "); 			//0
		sb.append("MM.KIBAN, "); 				//1
		if(constructionResultId != null) {
			//Æï
			sb.append("TSI.HOUR_METER, ");		//2
		}else {
			//VK
			sb.append("MM.NEW_HOUR_METER, ");	//2
		}
		sb.append("MM.SOSIKI_CD ");				//3
		sb.append("FROM MST_MACHINE MM ");
		if(constructionResultId != null) {
			//Æï
			sb.append("LEFT JOIN TBL_SEKO_INFO TSI ");
			sb.append("ON MM.KIBAN_SERNO = TSI.KIBAN_SERNO ");
		}
		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		sb.append("AND MM.KIBAN_SERNO = " + serialNumber + " ");
		if(constructionResultId != null) {
			//Æï
			sb.append("AND TSI.DELETE_FLAG = 0 ");
			sb.append("AND TSI.REPORT_NO = " + constructionResultId + " ");
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		return rs;
	}


	/**
	 * NativeQuery getUserName
	 * getUserName : ²¸Ò¼æ¾
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getUserName(String userId,Long serialNumber,Integer constructionResultId) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT MU.USER_NAME ");
		sb.append("FROM MST_USER MU ");
		sb.append("WHERE MU.DEL_FLG = 0 ");
		if(constructionResultId != null) {
			// Æï
			sb.append("AND MU.USER_ID = ( ");
			sb.append("SELECT ");
			sb.append("TSI.RESEARCH_USER ");
			sb.append("FROM ");
			sb.append("TBL_SEKO_INFO TSI ");
			sb.append("WHERE TSI.DELETE_FLAG = 0 ");
			sb.append("AND TSI.KIBAN_SERNO = " + serialNumber + " ");
			sb.append("AND TSI.REPORT_NO = " + constructionResultId + " ");
			sb.append(") ");
		}else{
			// VK
			sb.append("AND MU.USER_ID = '" + userId + "' ");
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> rs =  em.createNativeQuery(new String(sb)).getResultList();

		if (rs.size() > 0)
			return rs.get(0); //ÒWÂ
		else
			return null; //QÆÌÝ
	}


	/**
	 * NativeQuery constructionEditableCheck
	 * constructionEditableCheck : ÒWÂÛ`FbN
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer constructionEditableCheck(Integer constructionResultId, String userId) {
		StringBuilder sb = new StringBuilder();

		if(constructionResultId != null) {
			sb.append("SELECT TSI.REPORT_NO ");
			sb.append("FROM TBL_SEKO_INFO TSI ");
			sb.append("WHERE TSI.DELETE_FLAG = 0 ");
			sb.append("AND TSI.REPORT_NO = " + constructionResultId + " ");
			sb.append("AND TSI.REGIST_USER = '" + userId + "' ");
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> rs =  em.createNativeQuery(new String(sb)).getResultList();

		if (rs.size() > 0)
			return 0; //ÒWÂ
		else
			return 1; //QÆÌÝ
	}


	/**
	 * NativeQuery findConstructionResultById
	 * findConstructionResultById : {Hîñæ¾
	 */
	@Override
	public TblSekoInfo findConstructionResultById(Integer constructionResultId) {

		em.getEntityManagerFactory().getCache().evictAll();
		String sql = "SELECT m FROM TblSekoInfo m  WHERE m.reportNo = " + constructionResultId + " AND m.deleteFlag = 0 ";

		List<TblSekoInfo> rs = em.createQuery(sql).getResultList();

		em.clear();

		if (rs.size() > 0)
			return (TblSekoInfo) rs.get(0);
		else
			return null;
	}


	/**
	 * NativeQuery findCountryList
	 * findCountryList : îñæ¾
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findCountryList(Integer deviceLanguage) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MSC.CHOICE_ID, "); 					 //0
		sb.append("MSC.CHOICE" + deviceLanguage + ", "); //1
		sb.append("MSC.UNIT "); 						 //2
		sb.append("FROM MST_SEKO_CHOICE MSC ");
		sb.append("WHERE MSC.LIST_ID = 0 ");
		sb.append("AND MSC.DELETE_FLAG = 0 ");
		sb.append("ORDER BY MSC.CHOICE_ID ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		return rs;
	}


	/**
	 * NativeQuery findMaterialTypeList
	 * findMaterialTypeList : ÞíÞæ¾
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findMaterialTypeList(Integer deviceLanguage,Integer countryId) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MSC.COUNTRY_ID, "); 					//0
		sb.append("MSC.CHOICE_ID, "); 					//1
		sb.append("MSC.CHOICE" + deviceLanguage + " "); //2
		sb.append("FROM MST_SEKO_CHOICE MSC ");
		sb.append("WHERE MSC.LIST_ID = 1 ");
		sb.append("AND MSC.DELETE_FLAG = 0 ");
		sb.append("AND MSC.COUNTRY_ID = "+ countryId + " ");
		sb.append("ORDER BY MSC.COUNTRY_ID,MSC.CHOICE_ID ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		return rs;
	}


	/**
	 * NativeQuery findWorkTypeList
	 * findWorkTypeList : HíÞæ¾
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findWorkTypeList(Integer deviceLanguage,Integer countryId) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MSC.COUNTRY_ID, "); 					//0
		sb.append("MSC.CHOICE_ID, "); 					//1
		sb.append("MSC.CHOICE" + deviceLanguage + " "); //2
		sb.append("FROM MST_SEKO_CHOICE MSC ");
		sb.append("WHERE MSC.LIST_ID = 2 ");
		sb.append("AND MSC.DELETE_FLAG = 0 ");
		sb.append("AND MSC.COUNTRY_ID = "+ countryId + " ");
		sb.append("ORDER BY MSC.COUNTRY_ID,MSC.CHOICE_ID ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		return rs;
	}

	/**
	 * NativeQuery findPhotoList
	 * findPhotoList : ææ¾
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findPhotoList(Long serialNumber,Integer constructionResultId) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TSIP.PHOTO_NO, ");			//0
		sb.append("TSIP.SEKO_PHOTO_PATH, ");	//1
		sb.append("TSIP.PHOTO_DATE, ");			//2
		sb.append("TSIP.PHOTO_COMMENT ");		//3
		sb.append("FROM TBL_SEKO_INFO_PHOTO TSIP ");
		sb.append("WHERE TSIP.KIBAN_SERNO = " + serialNumber + " ");
		sb.append("AND TSIP.REPORT_NO = " + constructionResultId + " ");
		sb.append("ORDER BY PHOTO_NO ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		return rs;
	}

	/**
	 * NativeQuery findPavementPdfList
	 * findPavementPdfList : {HsïPDFæ¾
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findPavementPdfList(String scmModel,String deviceLanguage) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TPTP.TROUBLE_CODE, ");     //0
		sb.append("TPTP.TROUBLE_NAME, ");     //1
		sb.append("TPTP.PDF_NAME ");          //2
		sb.append("FROM TBL_PAVEMENT_TROUBLE_PDF TPTP ");
		sb.append("WHERE TPTP.DEL_FLG = 0 ");
		sb.append("AND TPTP.MODEL_CD = '" + scmModel + "' ");
		sb.append("AND TPTP.LANGUAGE_CD = '" + deviceLanguage + "' ");
		sb.append("ORDER BY TPTP.TROUBLE_CODE,TPTP.BRANCH_NUMBER ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if(rs.size() > 0)
			return rs;
		else
			return null;
	}


//49.constructionHistoryList({Hîñðê)
	/**
	 * NativeQuery findConstructionHistoryList
	 * findConstructionHistoryList : {Hîñðê
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findConstructionHistoryList(Long serialNumber,Integer sortFlg) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TO_CHAR(TSI.RESEARCH_DATE,'yyyy-mm-dd'), ");					//0
		sb.append("TSI.RESEARCH_USER, ");					//1
		sb.append("TSI.RESEARCH_LOCATION, ");				//2
		sb.append("TSI.REPORT_NO ");						//3
		sb.append("FROM TBL_SEKO_INFO TSI ");
		sb.append("WHERE TSI.KIBAN_SERNO = " + serialNumber + " ");
		sb.append("AND TSI.DELETE_FLAG = 0 ");
		sb.append("ORDER BY ");
		if(sortFlg == 0)
			sb.append("TSI.RESEARCH_DATE ,TSI.KIBAN_SERNO ,TSI.REPORT_NO ");
		if(sortFlg == 1)
			sb.append("TSI.RESEARCH_DATE DESC ,TSI.KIBAN_SERNO DESC ,TSI.REPORT_NO DESC ");
		if(sortFlg == 2)
			sb.append("TSI.RESEARCH_USER ,TSI.KIBAN_SERNO ,TSI.REPORT_NO ");
		if(sortFlg == 3)
			sb.append("TSI.RESEARCH_USER DESC ,TSI.KIBAN_SERNO DESC ,TSI.REPORT_NO DESC ");
		if(sortFlg == 4)
			sb.append("TSI.RESEARCH_LOCATION ,TSI.KIBAN_SERNO ,TSI.REPORT_NO ");
		if(sortFlg == 5)
			sb.append("TSI.RESEARCH_LOCATION DESC ,TSI.KIBAN_SERNO DESC ,TSI.REPORT_NO DESC ");

		System.out.println(sb);
		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		return rs;
	}

//50.constructionRegistor({Hîño^)
	/**
	 * NativeQuery registerConstruction
	 * registerConstruction : {Hîño^
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer registerConstruction(
			String userId,
			Integer deviceLanguage,
			Integer reportDelFlg,
			Integer reportNo,
			Long serialNumber,
			Double hourmeter,
			String researcher,
			String researchDate,
			String researchLocation,
			Double workVolume,
			Integer workVolumeUnit,
			Integer reportCountry,
			Integer materialType,
			Integer materialTemperatureUse,
			Double materialTemperature,
			Integer materialTemperatureUnit,
			Double outsideAirTemperature,
			Integer outsideAirTemperatureUnit,
			Double plateTemperature,
			Integer plateTemperatureUnit,
			Integer workType,
			String spotSituation,
			Integer pavementDistanceUse,
			Double pavementDistance,
			Double pavementWidth,
			Double pavementWidthTo,//2020.01.14 DucNKT added
			Double pavementThicknessLevelingL,
			Double pavementThicknessLevelingC,
			Double pavementThicknessLevelingR,
			Double constructionSpeed,
			Double levelingCylinderScaleL,
			Double levelingCylinderScaleR,
			Double manualSixnessScaleL,
			Double manualSixnessScaleR,
			Integer manualSixnessScaleUnit,
			Double stepAdjustmentScaleL,
			Double stepAdjustmentScaleR,
			Double crown,
			Double slopeCrownL,
			Double slopeCrownR,
			Double stretchMoldBoardHeightL,
			Double stretchMoldBoardHeightR,
			Integer tampaRotationsUse,
			Double tampaRotations,
			Integer tampaAutoFunction,
			Double vibratorRotations,
			Double screwHeight,
			Double screwSpeedScaleL,
			Double screwSpeedScaleR,
			Double conveyorSpeedScaleL,
			Double conveyorSpeedScaleR,
			Integer screwflowControlUse,
			Integer agcUse,
			Integer agcType,
			Integer extensionScrewUse,
			String extensionScrew,
			Integer pavementTrouble1,
			Integer pavementTrouble2,
			Integer pavementTrouble3,
			Integer pavementTrouble4,
			Integer pavementTrouble5,
			Integer pavementTrouble6,
			Integer pavementTrouble7,
			Integer pavementTrouble8,
			Integer pavementTrouble9,
			Integer pavementTrouble10,
			Integer pavementTrouble11,
			Integer pavementTrouble12,
			Integer pavementTrouble13,
			Integer pavementTrouble14,
			Double tonnageMixedMaterials,
			String customerEvaluation,
			Double rearSlopeL,
			Double rearSlopeR,
			Double rearAttackShrinkL,
			Double rearAttackShrinkR,
			Double rearAttackMiddleL,
			Double rearAttackMiddleR,
			Double rearAttackStretchL,
			Double rearAttackStretchR,
			List<Object[]> constphotoList
			) {

		Integer regisConstResultId = reportNo;
		StringBuilder sb = new StringBuilder();

		// VK
		if(regisConstResultId == null) {

			List<Integer> result = em.createQuery("SELECT  MAX(m.reportNo) FROM TblSekoInfo m ")
					.getResultList();

			if (result != null && result.size() > 0 && result.get(0) != null)
				regisConstResultId = result.get(0) + 1;
			else
				regisConstResultId = 1;

			// {Hîño^
			sb.append("INSERT INTO TBL_SEKO_INFO ( ");
			sb.append("KIBAN_SERNO, ");
			sb.append("REPORT_NO, ");
			sb.append("HOUR_METER, ");
			sb.append("RESEARCH_USER, ");
			sb.append("RESEARCH_DATE, ");
			sb.append("RESEARCH_LOCATION, ");
			sb.append("WORK_VOLUME, ");
			sb.append("WORK_VOLUME_UNIT, ");
			sb.append("REPORT_COUNTRY, ");
			sb.append("MATERIAL_TYPE, ");
			sb.append("MATERIAL_TEMPERATURE_USE, ");
			sb.append("MATERIAL_TEMPERATURE, ");
			sb.append("MATERIAL_TEMPERATURE_UNIT, ");
			sb.append("OUTSIDE_AIR_TEMPERATURE, ");
			sb.append("OUTSIDE_AIR_TEMPERATURE_UNIT, ");
			sb.append("PLATE_TEMPERATURE, ");
			sb.append("PLATE_TEMPERATURE_UNIT, ");
			sb.append("WORK_TYPE, ");
			sb.append("SPOT_SITUATION, ");
			sb.append("PAVEMENT_DISTANCE_USE, ");
			sb.append("PAVEMENT_DISTANCE, ");
			sb.append("PAVEMENT_WIDTH, ");
			sb.append("PAVEMENT_WIDTH_TO, ");//2020.01.14 DucNKT added
			sb.append("PAVEMENT_THICKNESS_LEVELING_L, ");
			sb.append("PAVEMENT_THICKNESS_LEVELING_C, ");
			sb.append("PAVEMENT_THICKNESS_LEVELING_R, ");
			sb.append("CONSTRUCTION_SPEED, ");
			sb.append("LEVELING_CYLINDER_SCALE_L, ");
			sb.append("LEVELING_CYLINDER_SCALE_R, ");
			sb.append("MANUAL_SIXNESS_SCALE_L, ");
			sb.append("MANUAL_SIXNESS_SCALE_R, ");
			sb.append("MANUAL_SIXNESS_SCALE_UNIT, ");
			sb.append("STEP_ADJUSTMENT_SCALE_L, ");
			sb.append("STEP_ADJUSTMENT_SCALE_R, ");
			sb.append("CROWN, ");
			sb.append("SLOPE_CROWN_L, ");
			sb.append("SLOPE_CROWN_R, ");
			sb.append("STRETCH_MOLD_BOARD_HEIGHT_L, ");
			sb.append("STRETCH_MOLD_BOARD_HEIGHT_R, ");
			sb.append("TAMPA_ROTATIONS_USE, ");
			sb.append("TAMPA_ROTATIONS, ");
			sb.append("TAMPA_AUTO_FUNCTION, ");
			sb.append("VIBRATOR_ROTATIONS, ");
			sb.append("SCREW_HEIGHT, ");
			sb.append("SCREW_SPEED_SCALE_L, ");
			sb.append("SCREW_SPEED_SCALE_R, ");
			sb.append("CONVEYOR_SPEED_SCALE_L, ");
			sb.append("CONVEYOR_SPEED_SCALE_R, ");
			sb.append("SCREW_FLOW_CONTROL_USE, ");
			sb.append("AGC_USE, ");
			sb.append("AGC_TYPE, ");
			sb.append("EXTENSION_SCREW_USE, ");
			sb.append("EXTENSION_SCREW, ");
			sb.append("PAVEMENT_TROUBLE_1, ");
			sb.append("PAVEMENT_TROUBLE_2, ");
			sb.append("PAVEMENT_TROUBLE_3, ");
			sb.append("PAVEMENT_TROUBLE_4, ");
			sb.append("PAVEMENT_TROUBLE_5, ");
			sb.append("PAVEMENT_TROUBLE_6, ");
			sb.append("PAVEMENT_TROUBLE_7, ");
			sb.append("PAVEMENT_TROUBLE_8, ");
			sb.append("PAVEMENT_TROUBLE_9, ");
			sb.append("PAVEMENT_TROUBLE_10, ");
			sb.append("PAVEMENT_TROUBLE_11, ");
			sb.append("PAVEMENT_TROUBLE_12, ");
			sb.append("PAVEMENT_TROUBLE_13, ");
			sb.append("PAVEMENT_TROUBLE_14, ");
			sb.append("TONNAGE_MIXED_MATERIALS, ");
			sb.append("CUSTOMER_EVALUATION, ");
			sb.append("REAR_SLOPE_L, ");
			sb.append("REAR_SLOPE_R, ");
			sb.append("REAR_ATTACK_SHRINK_L, ");
			sb.append("REAR_ATTACK_SHRINK_R, ");
			sb.append("REAR_ATTACK_MIDDLE_L, ");
			sb.append("REAR_ATTACK_MIDDLE_R, ");
			sb.append("REAR_ATTACK_STRETCH_L, ");
			sb.append("REAR_ATTACK_STRETCH_R, ");
			sb.append("DELETE_FLAG, ");
			sb.append("REGIST_DTM, ");
			sb.append("REGIST_USER, ");
			sb.append("REGIST_PRG, ");
			sb.append("UPDATE_DTM, ");
			sb.append("UPDATE_USER, ");
			sb.append("UPDATE_PRG ");
			sb.append(") VALUES ( ");
			sb.append(serialNumber + ", ");
			sb.append(regisConstResultId + ", ");
			sb.append(hourmeter + ", ");
			sb.append(ConvertUtil.getQueryWithValidateValue(researcher) + ", ");
			sb.append(ConvertUtil.getQueryWithValidateValue(researchDate) + ", ");
			sb.append(ConvertUtil.getQueryWithValidateValue(researchLocation) + ", ");
			sb.append(workVolume + ", ");
			sb.append(workVolumeUnit + ", ");
			sb.append(reportCountry + ", ");
			sb.append(materialType + ", ");
			sb.append(materialTemperatureUse + ", ");
			sb.append(materialTemperature + ", ");
			sb.append(materialTemperatureUnit + ", ");
			sb.append(outsideAirTemperature + ", ");
			sb.append(outsideAirTemperatureUnit + ", ");
			sb.append(plateTemperature + ", ");
			sb.append(plateTemperatureUnit + ", ");
			sb.append(workType + ", ");
			sb.append(ConvertUtil.getQueryWithValidateValue(spotSituation) + ", ");
			sb.append(pavementDistanceUse + ", ");
			sb.append(pavementDistance + ", ");
			sb.append(pavementWidth + ", ");
			sb.append(pavementWidthTo + ", ");//2020.01.14 DucNKT added
			sb.append(pavementThicknessLevelingL + ", ");
			sb.append(pavementThicknessLevelingC + ", ");
			sb.append(pavementThicknessLevelingR + ", ");
			sb.append(constructionSpeed + ", ");
			sb.append(levelingCylinderScaleL + ", ");
			sb.append(levelingCylinderScaleR + ", ");
			sb.append(manualSixnessScaleL + ", ");
			sb.append(manualSixnessScaleR + ", ");
			sb.append(manualSixnessScaleUnit + ", ");
			sb.append(stepAdjustmentScaleL + ", ");
			sb.append(stepAdjustmentScaleR + ", ");
			sb.append(crown + ", ");
			sb.append(slopeCrownL + ", ");
			sb.append(slopeCrownR + ", ");
			sb.append(stretchMoldBoardHeightL + ", ");
			sb.append(stretchMoldBoardHeightR + ", ");
			sb.append(tampaRotationsUse + ", ");
			sb.append(tampaRotations + ", ");
			sb.append(tampaAutoFunction + ", ");
			sb.append(vibratorRotations + ", ");
			sb.append(screwHeight + ", ");
			sb.append(screwSpeedScaleL + ", ");
			sb.append(screwSpeedScaleR + ", ");
			sb.append(conveyorSpeedScaleL + ", ");
			sb.append(conveyorSpeedScaleR + ", ");
			sb.append(screwflowControlUse + ", ");
			sb.append(agcUse + ", ");
			sb.append(agcType + ", ");
			sb.append(extensionScrewUse + ", ");
			sb.append(ConvertUtil.getQueryWithValidateValue(extensionScrew) + ", ");
			sb.append(pavementTrouble1 + ", ");
			sb.append(pavementTrouble2 + ", ");
			sb.append(pavementTrouble3 + ", ");
			sb.append(pavementTrouble4 + ", ");
			sb.append(pavementTrouble5 + ", ");
			sb.append(pavementTrouble6 + ", ");
			sb.append(pavementTrouble7 + ", ");
			sb.append(pavementTrouble8 + ", ");
			sb.append(pavementTrouble9 + ", ");
			sb.append(pavementTrouble10 + ", ");
			sb.append(pavementTrouble11 + ", ");
			sb.append(pavementTrouble12 + ", ");
			sb.append(pavementTrouble13 + ", ");
			sb.append(pavementTrouble14 + ", ");
			sb.append(tonnageMixedMaterials + ", ");
			sb.append(ConvertUtil.getQueryWithValidateValue(customerEvaluation) + ", ");
			sb.append(rearSlopeL + ", ");
			sb.append(rearSlopeR + ", ");
			sb.append(rearAttackShrinkL + ", ");
			sb.append(rearAttackShrinkR + ", ");
			sb.append(rearAttackMiddleL + ", ");
			sb.append(rearAttackMiddleR + ", ");
			sb.append(rearAttackStretchL + ", ");
			sb.append(rearAttackStretchR + ", ");
			sb.append(reportDelFlg + "0, ");
			sb.append("SYSDATE, ");
			sb.append("'" + userId + "', ");
			sb.append("'GNavApp', ");
			sb.append("SYSDATE, ");
			sb.append("'" + userId + "', ");
			sb.append("'GNavApp') ");

			em.createNativeQuery(sb.toString()).executeUpdate();

			// æîño^
			if(constphotoList.size() > 0) {
				for(Object[] photo : constphotoList) {
					sb = new StringBuilder();
					sb.append("INSERT INTO TBL_SEKO_INFO_PHOTO (");
					sb.append("KIBAN_SERNO, ");
					sb.append("REPORT_NO, ");
					sb.append("PHOTO_NO, ");
					if(((Number)photo[2]).intValue()==0) {
							sb.append("SEKO_PHOTO_PATH, ");
					}
					sb.append("PHOTO_DATE, ");
//					sb.append("PHOTO_COMMENT, ");
					sb.append("REGIST_DTM, ");
					sb.append("REGIST_USER, ");
					sb.append("REGIST_PRG, ");
					sb.append("UPDATE_DTM, ");
					sb.append("UPDATE_USER, ");
					sb.append("UPDATE_PRG ");
					sb.append(") VALUES ( ");
					sb.append(serialNumber + ", ");
					sb.append(regisConstResultId + ", ");
					sb.append(photo[0] + ", ");
					if(((Number)photo[2]).intValue()==0) {
						sb.append("'constraction-" + regisConstResultId + photo[1] + "', ");
					}
					sb.append("'" + photo[3] + "', ");
//					sb.append(", ");
					sb.append("SYSDATE, ");
					sb.append("'" + userId + "', ");
					sb.append("'GNavApp', ");
					sb.append("SYSDATE, ");
					sb.append("'" + userId + "', ");
					sb.append("'GNavApp') ");

					em.createNativeQuery(sb.toString()).executeUpdate();
				}
			}
		}else {
			// {HîñXV
			sb = new StringBuilder();
			sb.append("UPDATE TBL_SEKO_INFO SET ");
			sb.append("RESEARCH_DATE = " + ConvertUtil.getQueryWithValidateValue(researchDate) + ", ");
			sb.append("RESEARCH_LOCATION = " + ConvertUtil.getQueryWithValidateValue(researchLocation) + ", ");
			sb.append("WORK_VOLUME = " + workVolume + ", ");
			sb.append("WORK_VOLUME_UNIT = " + workVolumeUnit + ", ");
			sb.append("REPORT_COUNTRY = " + reportCountry + ", ");
			sb.append("MATERIAL_TYPE = " + materialType + ", ");
			sb.append("MATERIAL_TEMPERATURE_USE = " + materialTemperatureUse + ", ");
			sb.append("MATERIAL_TEMPERATURE = " + materialTemperature + ", ");
			sb.append("MATERIAL_TEMPERATURE_UNIT = " + materialTemperatureUnit + ", ");
			sb.append("OUTSIDE_AIR_TEMPERATURE = " + outsideAirTemperature + ", ");
			sb.append("OUTSIDE_AIR_TEMPERATURE_UNIT = " + outsideAirTemperatureUnit + ", ");
			sb.append("PLATE_TEMPERATURE = " + plateTemperature + ", ");
			sb.append("PLATE_TEMPERATURE_UNIT = " + plateTemperatureUnit + ", ");
			sb.append("WORK_TYPE = " + workType + ", ");
			sb.append("SPOT_SITUATION = " + ConvertUtil.getQueryWithValidateValue(spotSituation) + ", ");
			sb.append("PAVEMENT_DISTANCE_USE = " + pavementDistanceUse + ", ");
			sb.append("PAVEMENT_DISTANCE = " + pavementDistance + ", ");
			sb.append("PAVEMENT_WIDTH = " + pavementWidth + ", ");
			sb.append("PAVEMENT_WIDTH_TO = " + pavementWidthTo + ", ");//2020.01.14 DucNKT added
			sb.append("PAVEMENT_THICKNESS_LEVELING_L = " + pavementThicknessLevelingL + ", ");
			sb.append("PAVEMENT_THICKNESS_LEVELING_C = " + pavementThicknessLevelingC + ", ");
			sb.append("PAVEMENT_THICKNESS_LEVELING_R = " + pavementThicknessLevelingR + ", ");
			sb.append("CONSTRUCTION_SPEED = " + constructionSpeed + ", ");
			sb.append("LEVELING_CYLINDER_SCALE_L = " + levelingCylinderScaleL + ", ");
			sb.append("LEVELING_CYLINDER_SCALE_R = " + levelingCylinderScaleR + ", ");
			sb.append("MANUAL_SIXNESS_SCALE_L = " + manualSixnessScaleL + ", ");
			sb.append("MANUAL_SIXNESS_SCALE_R = " + manualSixnessScaleR + ", ");
			sb.append("MANUAL_SIXNESS_SCALE_UNIT = " + manualSixnessScaleUnit + ", ");
			sb.append("STEP_ADJUSTMENT_SCALE_L = " + stepAdjustmentScaleL + ", ");
			sb.append("STEP_ADJUSTMENT_SCALE_R = " + stepAdjustmentScaleR + ", ");
			sb.append("CROWN = " + crown + ", ");
			sb.append("SLOPE_CROWN_L = " + slopeCrownL + ", ");
			sb.append("SLOPE_CROWN_R = " + slopeCrownR + ", ");
			sb.append("STRETCH_MOLD_BOARD_HEIGHT_L = " + stretchMoldBoardHeightL + ", ");
			sb.append("STRETCH_MOLD_BOARD_HEIGHT_R = " + stretchMoldBoardHeightR + ", ");
			sb.append("TAMPA_ROTATIONS_USE = " + tampaRotationsUse + ", ");
			sb.append("TAMPA_ROTATIONS = " + tampaRotations + ", ");
			sb.append("TAMPA_AUTO_FUNCTION = " + tampaAutoFunction + ", ");
			sb.append("VIBRATOR_ROTATIONS = " + vibratorRotations + ", ");
			sb.append("SCREW_HEIGHT = " + screwHeight + ", ");
			sb.append("SCREW_SPEED_SCALE_L = " + screwSpeedScaleL + ", ");
			sb.append("SCREW_SPEED_SCALE_R = " + screwSpeedScaleR + ", ");
			sb.append("CONVEYOR_SPEED_SCALE_L = " + conveyorSpeedScaleL + ", ");
			sb.append("CONVEYOR_SPEED_SCALE_R = " + conveyorSpeedScaleR + ", ");
			sb.append("SCREW_FLOW_CONTROL_USE = " + screwflowControlUse + ", ");
			sb.append("AGC_USE = " + agcUse + ", ");
			sb.append("AGC_TYPE = " + agcType + ", ");
			sb.append("EXTENSION_SCREW_USE = " + extensionScrewUse + ", ");
			sb.append("EXTENSION_SCREW = " + ConvertUtil.getQueryWithValidateValue(extensionScrew) + ", ");
			sb.append("PAVEMENT_TROUBLE_1 = " + pavementTrouble1 + ", ");
			sb.append("PAVEMENT_TROUBLE_2 = " + pavementTrouble2 + ", ");
			sb.append("PAVEMENT_TROUBLE_3 = " + pavementTrouble3 + ", ");
			sb.append("PAVEMENT_TROUBLE_4 = " + pavementTrouble4 + ", ");
			sb.append("PAVEMENT_TROUBLE_5 = " + pavementTrouble5 + ", ");
			sb.append("PAVEMENT_TROUBLE_6 = " + pavementTrouble6 + ", ");
			sb.append("PAVEMENT_TROUBLE_7 = " + pavementTrouble7 + ", ");
			sb.append("PAVEMENT_TROUBLE_8 = " + pavementTrouble8 + ", ");
			sb.append("PAVEMENT_TROUBLE_9 = " + pavementTrouble9 + ", ");
			sb.append("PAVEMENT_TROUBLE_10 = " + pavementTrouble10 + ", ");
			sb.append("PAVEMENT_TROUBLE_11 = " + pavementTrouble11 + ", ");
			sb.append("PAVEMENT_TROUBLE_12 = " + pavementTrouble12 + ", ");
			sb.append("PAVEMENT_TROUBLE_13 = " + pavementTrouble13 + ", ");
			sb.append("PAVEMENT_TROUBLE_14 = " + pavementTrouble14 + ", ");
			sb.append("TONNAGE_MIXED_MATERIALS = " + tonnageMixedMaterials + ", ");
			sb.append("CUSTOMER_EVALUATION = " + ConvertUtil.getQueryWithValidateValue(customerEvaluation) + ", ");
			sb.append("REAR_SLOPE_L = " + rearSlopeL + ", ");
			sb.append("REAR_SLOPE_R = " + rearSlopeR + ", ");
			sb.append("REAR_ATTACK_SHRINK_L = " + rearAttackShrinkL + ", ");
			sb.append("REAR_ATTACK_SHRINK_R = " + rearAttackShrinkR + ", ");
			sb.append("REAR_ATTACK_MIDDLE_L = " + rearAttackMiddleL + ", ");
			sb.append("REAR_ATTACK_MIDDLE_R = " + rearAttackMiddleR + ", ");
			sb.append("REAR_ATTACK_STRETCH_L = " + rearAttackStretchL + ", ");
			sb.append("REAR_ATTACK_STRETCH_R = " + rearAttackStretchR + ", ");
			sb.append("DELETE_FLAG = " + reportDelFlg + ", ");
			sb.append("UPDATE_DTM = SYSDATE, ");
			sb.append("UPDATE_USER = '" + userId + "', ");
			sb.append("UPDATE_PRG = 'GNavApp' ");
			sb.append("WHERE ");
			sb.append("KIBAN_SERNO = " + serialNumber + " ");
			sb.append("AND REPORT_NO = " + regisConstResultId + " ");

			em.createNativeQuery(sb.toString()).executeUpdate();

			// æîñXV(DELETEËINSERT)
			// DELETE
			sb = new StringBuilder();
			sb.append("DELETE FROM TBL_SEKO_INFO_PHOTO ");
			sb.append("WHERE ");
			sb.append("KIBAN_SERNO = " + serialNumber + " ");
			sb.append("AND REPORT_NO = " + regisConstResultId + " ");

			em.createNativeQuery(sb.toString()).executeUpdate();
			// INSERT
			if(constphotoList.size() > 0) {
				for(Object[] photo : constphotoList) {
					sb = new StringBuilder();
					sb.append("INSERT INTO TBL_SEKO_INFO_PHOTO (");
					sb.append("KIBAN_SERNO, ");
					sb.append("REPORT_NO, ");
					sb.append("PHOTO_NO, ");
					if(((Number)photo[2]).intValue()==0) {
							sb.append("SEKO_PHOTO_PATH, ");
					}
					sb.append("PHOTO_DATE, ");
//					sb.append("PHOTO_COMMENT, ");
					sb.append("REGIST_DTM, ");
					sb.append("REGIST_USER, ");
					sb.append("REGIST_PRG, ");
					sb.append("UPDATE_DTM, ");
					sb.append("UPDATE_USER, ");
					sb.append("UPDATE_PRG ");
					sb.append(") VALUES ( ");
					sb.append(serialNumber + ", ");
					sb.append(regisConstResultId + ", ");
					sb.append(photo[0] + ", ");
					if(((Number)photo[2]).intValue()==0) {
						sb.append("'constraction-" + regisConstResultId + photo[1] + "', ");
					}
					sb.append("'" + photo[3] + "', ");
//					sb.append(", ");
					sb.append("SYSDATE, ");
					sb.append("'" + userId + "', ");
					sb.append("'GNavApp', ");
					sb.append("SYSDATE, ");
					sb.append("'" + userId + "', ");
					sb.append("'GNavApp') ");

					em.createNativeQuery(sb.toString()).executeUpdate();
				}
			}

		}
		return regisConstResultId;
	}
	/**
	 * NativeQuery registerConstruction
	 * updateConstructionResultPhoto : æt@C¼XV
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer updateConstructionResultPhoto(String fileName,String userId,Integer constructionResultId,Integer photoNo) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE TBL_SEKO_INFO_PHOTO SET ");
		sb.append("SEKO_PHOTO_PATH = '" + fileName + "', ");
		sb.append("UPDATE_DTM = SYSDATE, ");
		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG = 'GNavApp' ");
		sb.append("WHERE REPORT_NO = " + constructionResultId + " ");
		sb.append("AND PHOTO_NO = " + photoNo + " ");

		em.createNativeQuery(sb.toString()).executeUpdate();
		return constructionResultId;
	}

}
