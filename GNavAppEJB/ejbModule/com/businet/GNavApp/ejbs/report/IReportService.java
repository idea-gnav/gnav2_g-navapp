package com.businet.GNavApp.ejbs.report;

import java.sql.Timestamp;
import java.util.List;

import com.businet.GNavApp.entities.TblTeijiBunpuReport;
import com.businet.GNavApp.entities.TblTeijiReport;



public interface IReportService {


	/**
	 * JPA
	 * FindByDailyReport: レポート情報取得(日次)
	 * DailyReport
	 */
	 TblTeijiReport findByDailyReport(Long serialNumber, String searchDate);

	/**
	 * JPA
	 * findByTeijiBunpuReport: 定時分布レポート情報取得(日次)
	 * DailyReport
	 */
	TblTeijiBunpuReport findByTeijiBunpuReport(Long serialNumber, String searchDate);

	/**
	 * JPA-JPQL
	 * getAllInfo: レポート情報取得
	 * GraphReport
	 */
	List<Object[]> getAllInfo(Long serialNumber, Timestamp dateFrom, Timestamp dateTo);

}
