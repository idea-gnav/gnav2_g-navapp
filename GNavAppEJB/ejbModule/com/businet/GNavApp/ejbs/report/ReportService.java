package com.businet.GNavApp.ejbs.report;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.TblTeijiBunpuReport;
import com.businet.GNavApp.entities.TblTeijiReport;




@Stateless
@Local(IReportService.class)
public class ReportService implements IReportService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/**
	 * JPA
	 * FindByDailyReport: |[gîñæ¾(ú)
	 * DailyReport
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TblTeijiReport findByDailyReport(Long serialNumber, String searchDate) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<TblTeijiReport> result = em.createQuery("SELECT t FROM TblTeijiReport t "
				+ "WHERE t.kibanSerno=:serialNumber "
				// **««« 2019/1/11 LBNÎì «\eXgNGC³  «««**//
//				+ "AND t.kikaiKadobiLocal = {d '"+searchDate+"'} ")
				+ "AND t.kikaiKadobiLocal >= {ts '"+searchDate+" 00-00-00'} AND t.kikaiKadobiLocal <= {ts '"+searchDate+" 23-59-59'} ")
				// **ªªª 2019/1/11 LBNÎì «\eXgNGC³  ªªª**//
				.setParameter("serialNumber", serialNumber)
				.getResultList();


		if (result.size() > 0)
			return result.get(0);
		else
			return null;

	}


	/**
	 * JPA
	 * findByTeijiBunpuReport: èªz|[gîñæ¾(ú)
	 * DailyReport
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TblTeijiBunpuReport findByTeijiBunpuReport(Long serialNumber, String searchDate) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT t FROM TblTeijiBunpuReport t ");
		sb.append("WHERE t.kibanSerno=:serialNumber ");
		sb.append("AND t.kikaiKadobiLocal >= {ts '"+searchDate+" 00-00-00'} AND t.kikaiKadobiLocal <= {ts '"+searchDate+" 23-59-59'} ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<TblTeijiBunpuReport> result = em.createQuery(new String(sb))
				.setParameter("serialNumber", serialNumber)
				.getResultList();

		if (result.size() > 0)
			return result.get(0);
		else
			return null;

	}


	/**
	 * JPA-JPQL
	 * getAllInfo: |[gîñæ¾
	 * GraphReport
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getAllInfo(Long serialNumber, Timestamp dateFrom, Timestamp dateTo) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result = em.createQuery("Select r.kikaiKadobiLocal,r.hourMeter,r.kikaiSosaTime,r.nenryoConsum,r.ureaWaterConsum, r.sekoTime, m "
				+ "from TblTeijiReport r JOIN r.mstMachine m "
				+ "WHERE (m.kibanSerno = :serialNumber AND r.kikaiKadobiLocal >= :dateFrom AND r.kikaiKadobiLocal <=:dateTo) "
				+ "Order by r.kikaiKadobiLocal")
					.setParameter("serialNumber", serialNumber)
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.getResultList();

		 return result;
	}






}
