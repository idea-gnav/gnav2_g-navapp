package com.businet.GNavApp.ejbs.sosiki;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;

@Stateless
@Local(ISosikiService.class)
public class SosikiService implements ISosikiService{


	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;



	/**
	 * NativeQuery
	 * findByKigyouSosikiName: お客様名称取得
	 * サービス工場名取得
	 */
	@Override
	public String findByKigyouSosikiName(String sosikiCd) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.KIGYOU_NAME || ' ' || MS.SOSIKI_NAME ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("AND MS.SOSIKI_CD = '"+sosikiCd+"' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;
	}



	/**
	 * NativeQuery
	 * findByDiritenName: 代理店名称検索
	 */
	@Override
	public String findByDiritenName(String sosikiCd) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.KIGYOU_NAME || ' ' || RTRIM(LTRIM(MSD.SOSIKI_NAME)) ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("LEFT JOIN MST_SOSIKI MSD ");
		sb.append("ON MSD.SOSIKI_CD = MS.DAIRITENN_CD ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MSD.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("WHERE MK.DELETE_FLG <> 1 ");
		sb.append("AND MS.SOSIKI_CD = '"+sosikiCd+"' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;

	}





	/**
	 * NativeQuery
	 * findByServiceKoujouName: サービス工場名称取得
	 */
	@Override
	public String findByServiceKoujouName(String sosikiCd) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.KIGYOU_NAME || ' ' || MSD.SOSIKI_NAME ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("JOIN MST_SOSIKI MSD ");
		sb.append("ON MS.SOSIKI_CD = MSD.SERVICE_KOUJOU_CD ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MSD.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("WHERE MK.DELETE_FLG <> 1 ");
		sb.append("WHERE MSD.SOSIKI_CD = '"+sosikiCd+"' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;

	}


	/**
	 * JPA-JPQL
	 * findByGyouhshuNm： 業種名称取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByGyouhshuNm(String sosikiCd, String languageCd) {

			em.getEntityManagerFactory().getCache().evictAll();
			List<String> gyoushuCd = em.createQuery("SELECT s.gyoushuCd FROM MstSosiki s "
					+ "WHERE s.sosikiCd = :sosikiCd ")
					.setParameter("sosikiCd", sosikiCd)
					.getResultList();

			if (gyoushuCd.size() > 0 && gyoushuCd.get(0) !=null) {
				em.getEntityManagerFactory().getCache().evictAll();
				List<String> result = em.createQuery("SELECT g.gyoushuNm FROM MstGyoushu g "
						+ "WHERE g.gyoushuCd = :gyoushuCd "
						+ "AND g.hyoujiGengoCd = :deviceLanguage ")
						.setParameter("gyoushuCd", (String)gyoushuCd.get(0))
						.setParameter("deviceLanguage", languageCd)
						.getResultList();

				if (result.size() > 0 && result.get(0) != null)
					return (String)result.get(0);
				else
					return null;


			}else {
				return null;
			}

	}



	/**
	 * JPA-JPQL
	 * findByToukatsubuNm： 統括部名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByToukatsubuNm(String machineKyotenCd) {

			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT  t.toukatsubuName FROM MstToukatsubu t "
					+ "WHERE t.kyotenCd = :kyotenCd ")
					.setParameter("kyotenCd", machineKyotenCd)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}

	/**
	 * JPA-JPQL
	 * findByKyotenNm： 拠点名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByKyotenNm(String machineKyotenCd) {

			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT  t.kyotenName FROM MstKyoten t "
					+ "WHERE t.kyotenCd = :kyotenCd ")
					.setParameter("kyotenCd", machineKyotenCd)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}




	/**
	 * JPA findByCustomerName: 顧客住所取得
	 */
	@Override
	public List<Object[]> findByCustomerName(String userId, Integer typeFlg, String kigyouCd, String sosikiCd,
			String groupNo, String kengenCd, String customerNameInput) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.KIGYOU_NAME || MS.SOSIKI_NAME ");
		sb.append(",MS.SOSIKI_ADDR1 || MS.SOSIKI_ADDR2 || MS.SOSIKI_ADDR3 ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("LEFT JOIN MST_KIGYOU MK ");
		sb.append("ON ( MK.KIGYOU_CD = MS.KIGYOU_CD ");
		sb.append("AND MK.DELETE_FLG = 0 ) ");
		sb.append("WHERE MS.DELETE_FLG = 0 ");
		sb.append("AND ( NLS_UPPER(MK.KIGYOU_NAME || MS.SOSIKI_NAME) LIKE NLS_UPPER(REGEXP_REPLACE('"
				+ ConvertUtil.searchFuzzyStr(ConvertUtil.rpStr(customerNameInput)) + "',' ','')) ) ");

		// ユーザー権限 機械公開制御
		if (typeFlg == 0) {
			// 0:SCM
			sb.append("");

		} else if (typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
		    // ユーザーの所属する組織の企業、組織、グループのいずれかに該当する組織		
			sb.append("MS.GROUP_NO = '"+ groupNo + "' ");
			sb.append("OR ");
			sb.append("MS.KIGYOU_CD IN ( ");
			sb.append("SELECT MS1.KIGYOU_CD FROM MST_SOSIKI MS1 WHERE MS1.KYOTEN_CD IN( SELECT MS2.SOSIKI_CD FROM MST_SOSIKI MS2 WHERE MS2.KIGYOU_CD ='"+kigyouCd+"'))  ");
			// ユーザーの所属する組織の企業が代理店として設定されている
			sb.append("OR MS.KIGYOU_CD = '"+kigyouCd+"' ) ");
			
		} else if (typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			// ユーザーの所属する組織の企業、組織、グループのいずれかに該当する組織 （企業、グループ、組織）
			sb.append("MS.KIGYOU_CD = '"+kigyouCd+"' ");
			sb.append("OR MS.GROUP_NO = '"+ groupNo + "' ");
			sb.append("OR MS.SOSIKI_CD = '"+ sosikiCd + "' ");
			// ユーザーの所属する組織が代理店として設定されている
			sb.append("OR MS.DAIRITENN_CD = '"+ sosikiCd + "' ");
			sb.append(") ");

		} else if (typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MS.KIGYOU_CD = '"+kigyouCd+"' OR MS.GROUP_NO = '"+ groupNo + "' OR MS.SOSIKI_CD = '"+ sosikiCd + "' ");
			sb.append(") ");
			
		} else if (typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MS.KIGYOU_CD = '"+kigyouCd+"' OR MS.GROUP_NO = '"+ groupNo + "' OR MS.SOSIKI_CD = '"+ sosikiCd + "' ");
			sb.append(") ");
			
		} else if (typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MS.KENGEN_CD IN (201,205,207,208) ");
			sb.append(") ");

		} else if (typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MS.KENGEN_CD IN (301,305,307) ");
			sb.append(") ");

		} else if (typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MS.SOSIKI_CD = '" + sosikiCd + "' ");
			sb.append(") ");

		} else {
			sb.append("AND ROWNUM <= 0 ");
		}

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}


}
