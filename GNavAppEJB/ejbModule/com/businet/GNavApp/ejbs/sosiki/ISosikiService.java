package com.businet.GNavApp.ejbs.sosiki;

import java.util.List;

public interface ISosikiService{


	/**
	 * NativeQuery
	 * findByKigyouSosikiName: ���q�l���̎擾
	 */
	String findByKigyouSosikiName(String sosikiCd);

//	/**
//	 * JPA
//	 * findByLbxFlg: LBX�t���O�擾
//	 */
//	Integer findByLbxFlg(String sosikiCd);


	/**
	 * NativeQuery
	 * findByDiritenName: �㗝�X���̌���
	 */
	String findByDiritenName(String sosikiCd);


	/**
	 * NativeQuery
	 * findByServiceKoujouName: �T�[�r�X�H�ꖼ�̌���
	 */
	String findByServiceKoujouName(String sosikiCd);



	/**
	 * JPA-JPQL
	 * findByGyouhshuNm�F �Ǝ햼�̎擾
	 */
	String findByGyouhshuNm(String sosikiCd, String languageCd);



	/**
	 * JPA-JPQL
	 * findByToukatsubuNm�F ���������擾
	 */
	String findByToukatsubuNm(String machineKyotenCd);



	/**
	 * JPA-JPQL
	 * findByKyotenNm�F ���_���擾
	 */
	String findByKyotenNm(String machineKyotenCd);


	/**
	 * JPA findByCustomerName: �ڋq�Z���擾
	 */
	List<Object[]> findByCustomerName(String userId, Integer typeFlg, String kigyouCd, String sosikiCd, String groupNo, String kengenCd,String customerNameInput);

}
