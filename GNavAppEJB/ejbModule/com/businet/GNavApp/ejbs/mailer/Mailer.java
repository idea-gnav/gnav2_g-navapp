package com.businet.GNavApp.ejbs.mailer;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



/**
 * Session Bean implementation class Mailer
 */
@Stateless
@LocalBean
public class Mailer {

	private static final Logger logger = Logger.getLogger(Mailer.class.getName());

	@Resource(lookup="mail/GNAV2MailSession")
    Session mailSession;
	
    
	public boolean sendMail(
				String mailTo, 
				String subject, 
				String textMessage,
				List<String> pathFileAttachment
			) throws AddressException, MessagingException {
		
		logger.info("sendMail="+mailTo);
		
    	try {
            
            Message message = new MimeMessage(mailSession);      
            
            // header field of the header
//            message.setFrom(new InternetAddress(mailTo));
            
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(mailTo));
            message.setSubject(subject);
           
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(textMessage);
             
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            
            for (String stringPath : pathFileAttachment) {
            	
            	messageBodyPart = new MimeBodyPart();
                String filename = stringPath;
                DataSource source = new FileDataSource(filename);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(source.getName());
                multipart.addBodyPart(messageBodyPart);
			}
            
            message.setContent(multipart);

            Transport.send(message);
            
            return true;
            
            
        } catch (AddressException e) {
        	logger.log(Level.WARNING, e.getMessage(), e);
        	return false;
        	
        } catch (MessagingException e) {
        	logger.log(Level.WARNING, e.getMessage(), e);
        	return false;

        } catch (Exception e) {
        	logger.log(Level.WARNING, e.getMessage(), e);
        	return false;

        }
        
    }

}
