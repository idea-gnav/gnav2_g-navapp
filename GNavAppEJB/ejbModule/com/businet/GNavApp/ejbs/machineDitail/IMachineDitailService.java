package com.businet.GNavApp.ejbs.machineDitail;

public interface IMachineDitailService{



	/**
	 * JPA-JPQL
	 * findByCtrlBBuhinNo： コントローラーA部品名取得
	 */
	String findByCtrlABuhinNo(Integer machineModel, String buhinNo, String conType);

	/**
	 * JPA-JPQL
	 * findByCtrlBBuhinNo： コントローラーB部品名取得
	 */
	String findByCtrlBBuhinNo(Integer machineModel, String buhinNo, String conType);

	/**
	 * JPA-JPQL
	 * findByCtrlCBuhinNo： コントローラーC部品名取得
	 */
	String findByCtrlCBuhinNo(String buhinNo, String conType);

	/**
	 * JPA-JPQL
	 * findByCtrlSBuhinNo： コントローラーS部品名取得
	 */
	String findByCtrlSBuhinNo(String buhinNo);

	/**
	 * JPA-JPQL
	 * findByCtrlTBuhinNo： コントローラーT部品名取得
	 */
	String findByCtrlTBuhinNo(String buhinNo, String conType);

	/**
	 * JPA-JPQL
	 * findByMainConSerialNo： コントローラーシリアル番号取得
	 */
	String findByMainConSerialNo(String mainConParts, String conType);

	/**
	 * JPA-JPQL
	 * findByMainConParts： コントローラーシリアル部品名取得
	 */
	String findByMainConParts(String mainConParts, String conType);

	/**
	 * JPA-JPQL
	 * findByHbInvPartNo： インバータ部品番号取得
	 */
	String findByHbInvPartNo(String mainConParts);

	/**
	 * JPA-JPQL
	 * findByTypeQ4000： Q4000部品名取得
	 */
	String findByq4000(String q4000BuhinNo, String conType);


	/**
	 * JPA-JPQL
	 * findByHbConParts: HBコントローラ部品番号取得
	 */
	String findByHbConParts(String hbConParts);


	/**
	 * JPA-JPQL
	 * findByInvParts: インバータ部品番号取得
	 */
	String findByInvParts(String invnvParts);


	/**
	 * JPA-JPQL
	 * findByConvParts: コンバータ部品番号取得
	 */
	String findByConvParts(String convParts);


	/**
	 * JPA-JPQL
	 * findByBmuParts: BMU部品番号取得
	 */
	String findByBmuParts(String bmuParts);


	/**
	 * NativeQuery
	 * findByHotShutDown: ホットシャットダウン(最新日付)
	 */
	Double findByHotShutDown(Long kibanSerno);


	/**
	 * NativeQuery
	 * findByTwoDmgOptTime: 2DMG操作時間
	 */
	Double findByTwoDmgOptTime(Long kibanSerno);


	/**
	 * NativeQuery
	 * findByTwoDmgMajorOptTime: 2DMG（メジャーモード）操作時間
	 */
	Double findByTwoDmgMajorOptTime(Long kibanSerno);


}

