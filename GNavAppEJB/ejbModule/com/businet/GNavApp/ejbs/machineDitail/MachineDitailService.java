package com.businet.GNavApp.ejbs.machineDitail;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;


@Stateless
@Local(IMachineDitailService.class)
public class MachineDitailService implements IMachineDitailService {


	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByCtrlABuhinNo： コントローラーA部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByCtrlABuhinNo(Integer machineModel, String buhinNo, String conType) {

		if(!ConvertUtil.isNumber(buhinNo)){
			return null;
		}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeA t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.machineKbn = :machineModel "
					+ "AND t.code = :buhinNo "
					+ "AND t.conType = :conType ")
					.setParameter("machineModel", machineModel)
					.setParameter("buhinNo", Integer.parseInt(buhinNo))
					.setParameter("conType", conType)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	/**
	 * JPA-JPQL
	 * findByCtrlBBuhinNo： コントローラーB部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByCtrlBBuhinNo(Integer machineModel, String buhinNo, String conType) {

		if(!ConvertUtil.isNumber(buhinNo)){
			return null;
		}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeB t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.machineKbn = :machineModel "
					+ "AND t.code = :buhinNo "
					+ "AND t.conType = :conType ")
					.setParameter("machineModel", machineModel)
					.setParameter("buhinNo", Integer.parseInt(buhinNo))
					.setParameter("conType", conType)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	/**
	 * JPA-JPQL
	 * findByCtrlCBuhinNo： コントローラーC部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByCtrlCBuhinNo(String buhinNo, String conType) {

		if(!ConvertUtil.isNumber(buhinNo)){
			return null;
		}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeC t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.code = :buhinNo "
					+ "AND t.conType = :conType ")
					.setParameter("buhinNo", Integer.parseInt(buhinNo))
					.setParameter("conType", conType)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}




	/**
	 * JPA-JPQL
	 * findByCtrlSBuhinNo： コントローラーS部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByCtrlSBuhinNo(String buhinNo) {

		if(!ConvertUtil.isNumber(buhinNo)){
			return null;
		}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeS t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.code = :buhinNo ")
					.setParameter("buhinNo", Integer.parseInt(buhinNo))
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	/**
	 * JPA-JPQL
	 * findByCtrlTBuhinNo： コントローラーT部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByCtrlTBuhinNo(String BuhinNo, String conType) {

		if(!ConvertUtil.isNumber(BuhinNo)){
			return null;
		}

			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeT t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.code = :buhinNo "
					+ "AND t.conType = :conType ")
					.setParameter("buhinNo", Integer.parseInt(BuhinNo))
					.setParameter("conType", conType)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}




	/**
	 * JPA-JPQL
	 * findByMainConSerialNo： コントローラーシリアル部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByMainConSerialNo(String mainConParts, String conType) {

		if(!ConvertUtil.isNumber(mainConParts)){
			return null;
		}
		em.getEntityManagerFactory().getCache().evictAll();
		List<String> result = em.createQuery("SELECT t.name FROM MstTypeMainSer t "
				+ "WHERE t.deleteFlag = 0 "
				+ "AND t.conType = :conType "
				+ "AND t.code = :buhinNo ")
				.setParameter("conType", conType)
				.setParameter("buhinNo", Integer.parseInt(mainConParts))
				.getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;
	}




	/**
	 * JPA-JPQL
	 * findByMainConParts： コントローラー部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByMainConParts(String mainConParts, String conType) {

		if(!ConvertUtil.isNumber(mainConParts)){
			return null;
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> result = em.createQuery("SELECT t.name FROM MstTypeMainCon t "
				+ "WHERE t.deleteFlag = 0 "
				+ "AND t.conType = :conType "
				+ "AND t.code = :buhinNo ")
				.setParameter("conType", conType)
				.setParameter("buhinNo", Integer.parseInt(mainConParts))
				.getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;
	}



	/**
	 * JPA-JPQL
	 * findByHbInvPartNo： インバータ部品番号取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByHbInvPartNo(String mainConParts) {

		if(!ConvertUtil.isNumber(mainConParts)){
			return null;
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> result = em.createQuery("SELECT t.name FROM MstHbInverter t "
				+ "WHERE t.deleteFlag = 0 "
				+ "AND t.code = :buhinNo ")
				.setParameter("buhinNo", Integer.parseInt(mainConParts))
				.getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * findByTypeQ4000： Q4000部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByq4000(String q4000BuhinNo, String conType) {

		if(!ConvertUtil.isNumber(q4000BuhinNo)){
			return null;
		}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeQ4000 t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.conType = :conType "
					+ "AND t.code = :buhinNo ")
					.setParameter("conType", conType)
					.setParameter("buhinNo", Integer.parseInt(q4000BuhinNo))
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}


	/**
	 * JPA-JPQL
	 * findByHbConParts： HBコントローラ部品番号取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByHbConParts(String hbConParts) {

	if(!ConvertUtil.isNumber(hbConParts)){
		return null;
	}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT mh.name FROM MstHbController mh "
					+ "WHERE mh.deleteFlag = 0 "
					+ "AND mh.code = :buhinNo")
					.setParameter("buhinNo", Integer.parseInt(hbConParts))
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	/**
	 * JPA-JPQL
	 * findByInvParts： インバータ部品番号取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByInvParts(String invParts) {

	if(!ConvertUtil.isNumber(invParts)){
		return null;
	}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT mi.name FROM MstHbInverter mi "
					+ "WHERE mi.deleteFlag = 0 "
					+ "AND mi.code = :invParts")
					.setParameter("invParts", Integer.parseInt(invParts))
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	/**
	 * JPA-JPQL
	 * findByConvParts： コンバータ部品番号取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByConvParts(String convParts) {

	if(!ConvertUtil.isNumber(convParts)){
		return null;
	}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT mc.name FROM MstHbConverter mc "
					+ "WHERE mc.deleteFlag = 0 "
					+ "AND mc.code = :convParts")
					.setParameter("convParts", Integer.parseInt(convParts))
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	/**
	 * JPA-JPQL
	 * findByBmuParts： BMU部品番号取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByBmuParts(String bmuParts) {

	if(!ConvertUtil.isNumber(bmuParts)){
		return null;
	}
			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT mb.name FROM MstHbBmu mb "
					+ "WHERE mb.deleteFlag = 0 "
					+ "AND mb.code = :bmuParts ")
					.setParameter("bmuParts", Integer.parseInt(bmuParts))
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}




	/**
	 * NativeQuery
	 * findByHotShutdown: ホットシャットダウン(最新日付)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Double findByHotShutDown(Long kibanSerno) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT COOLING_TIME1 ");
		sb.append("FROM TBL_TEIJI_BUNPU_REPORT ");
		sb.append("WHERE KIKAI_KADOBI = ( ");
		sb.append("SELECT MAX(KIKAI_KADOBI) FROM TBL_TEIJI_BUNPU_REPORT ");
		sb.append("WHERE KIBAN_SERNO ="+kibanSerno+" ) ");
		sb.append("AND KIBAN_SERNO ="+kibanSerno+" ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object> result = em.createNativeQuery(new String(sb)).getResultList();


		if (result.size() > 0 && result.get(0)!=null)
			return Double.parseDouble(result.get(0).toString());
		else
			return null;

	}




	/**
	 * NativeQuery
	 * findByTwoDmgOptTime: 2DMG操作時間
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Double findByTwoDmgOptTime(Long kibanSerno) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT TWO_DMG_TIME ");
		sb.append("FROM TBL_TEIJI_REPORT ");
		sb.append("WHERE KIKAI_KADOBI = ( ");
		sb.append("SELECT MAX(KIKAI_KADOBI) FROM TBL_TEIJI_REPORT ");
		sb.append("WHERE KIBAN_SERNO = "+kibanSerno+") ");
		sb.append("AND KIBAN_SERNO ="+kibanSerno+" ");


		em.getEntityManagerFactory().getCache().evictAll();
		List<Object> result = em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0 && result.get(0)!=null)
			return Double.parseDouble(result.get(0).toString());
		else
			return null;

	}



	/**
	 * NativeQuery
	 * findByTwoDmgMajorOptTime: 2DMG（メジャーモード）操作時間
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Double findByTwoDmgMajorOptTime(Long kibanSerno) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT TWO_DMG_MAJOR_TIME ");
		sb.append("FROM TBL_TEIJI_REPORT ");
		sb.append("WHERE KIKAI_KADOBI = ( ");
		sb.append("SELECT MAX(KIKAI_KADOBI) FROM TBL_TEIJI_REPORT ");
		sb.append("WHERE KIBAN_SERNO = "+kibanSerno+") ");
		sb.append("AND KIBAN_SERNO ="+kibanSerno+" ");



		em.getEntityManagerFactory().getCache().evictAll();
		List<Object> result = em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0 && result.get(0)!=null)
			return Double.parseDouble(result.get(0).toString());
		else
			return null;
	}


}
