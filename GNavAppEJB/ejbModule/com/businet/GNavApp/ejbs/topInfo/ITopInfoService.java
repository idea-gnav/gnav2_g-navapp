package com.businet.GNavApp.ejbs.topInfo;

import java.util.List;

import com.businet.GNavApp.entities.TblDocuments;
import com.businet.GNavApp.entities.TblNews;

public interface ITopInfoService{



	/**
	 * JPA-JPQL
	 * findByNews: ニュース取得
	 */
	List<TblNews> findByNews(String languageCd, int appFlg);


	/**
	 * JPA-JPQL
	 * findByDocuments: 資料一覧取得
	 */
	List<TblDocuments> findByDocuments(String languageCd, int appFlg);



}
