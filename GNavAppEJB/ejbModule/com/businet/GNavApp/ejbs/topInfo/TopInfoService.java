package com.businet.GNavApp.ejbs.topInfo;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.TblDocuments;
import com.businet.GNavApp.entities.TblNews;


@Stateless
@Local(ITopInfoService.class)
public class TopInfoService implements ITopInfoService{


	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByNews: ニュース取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TblNews> findByNews(String languageCd, int appFlg) {

		String fmtDate = new SimpleDateFormat("yyyy-MM-dd")
				.format(new Timestamp(System.currentTimeMillis()));	// JST

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT t FROM TblNews t ");
		sb.append("WHERE t.delFlg = 0 ");
		sb.append("AND t.appFlg = :appFlg ");
		sb.append("AND t.languageCd = :languageCd ");
		sb.append("AND t.opDate <= {ts '" + fmtDate + " 23:59:59'} ");	// JST
		sb.append("AND (t.edDate >= {ts '" + fmtDate + " 00:00:00'} OR t.edDate IS NULL) ");	// JST
		sb.append("ORDER BY t.opDate desc, t.newsContents ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<TblNews> result = em.createQuery(new String(sb))
				.setParameter("appFlg", appFlg)
				.setParameter("languageCd", languageCd)
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * JPA-JPQL
	 * findByDocuments: 資料一覧取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TblDocuments> findByDocuments(String languageCd, int appFlg) {

		String fmtDate = new SimpleDateFormat("yyyy-MM-dd")
				.format(new Timestamp(System.currentTimeMillis()));

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT t FROM TblDocuments t ");
		sb.append("WHERE t.delFlg = :delFlg ");
		sb.append("AND t.appFlg = :appFlg ");
		sb.append("AND t.languageCd = :languageCd ");
		sb.append("AND t.opDate <= {ts '" + fmtDate + " 23:59:59'} ");	// JST
		sb.append("AND (t.edDate >= {ts '" + fmtDate + " 00:00:00'} OR t.edDate IS NULL) ");	// JST
		sb.append("ORDER BY t.opDate desc, t.docuName ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<TblDocuments> result = em.createQuery(new String(sb))
				.setParameter("delFlg", 0)
				.setParameter("appFlg", appFlg)
				.setParameter("languageCd", languageCd)
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}



}
