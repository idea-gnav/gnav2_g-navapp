package com.businet.GNavApp.ejbs.periodicServiceDitail;

import java.util.List;


public interface IPeriodicServiceDitailService {


	/**
	 * NativeQuery
	 * findByDtcDitail
	 */
	List<Object[]> findByServiceRequestDitailNativeQuery(
			Long serialNumber,
			String conType,
			Integer machineModel,
			int[] buhinNo,
			String languageCd,
			Double hourMeter
			);

	/**
	 * NativeQuery
	 * findByReplacementHistory
	 */
	List<Object[]>findByReplacementHistoryNativeQuery(
			Long serialNumber,
			int[] buhinNo
			);





}
