package com.businet.GNavApp.ejbs.googleMapAPILog;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.TblMapLog;

@Stateless
@Local(IGoogleMapAPILogService.class)
public class GoogleMapAPILogService implements IGoogleMapAPILogService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
	EntityManager em;

	@Override
	public void insertLog(String userId, int operatingStatus, int flg) {

		int insert = 0;
		if(operatingStatus==0)		//ルート検索
			insert = 2;
		else if(operatingStatus==1)	//ルート案内
			insert = 1;

		for(int counter=0; counter<insert; counter++ ) {

			em.getEntityManagerFactory().getCache().evictAll();

			TblMapLog tblMapLog = new TblMapLog();
			tblMapLog.setUserId(userId);
			tblMapLog.setOperationStatus(operatingStatus);
			tblMapLog.setRegistPrg("GNavApp");
			tblMapLog.setRegistDtm(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			tblMapLog.setRegistUser(userId);
			em.persist(tblMapLog);

			if(flg==1)
				em.flush();
			em.clear();
		}

		return;
	}




}
