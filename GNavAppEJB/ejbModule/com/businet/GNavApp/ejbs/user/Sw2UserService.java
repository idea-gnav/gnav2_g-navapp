package com.businet.GNavApp.ejbs.user;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.DatatypeConverter;

import com.businet.GNavApp.entities.Sw2MstUser;


@Stateless
@Local(ISw2UserService.class)
public class Sw2UserService implements ISw2UserService{


	@PersistenceContext(unitName="SW2")
    EntityManager em;

	/**
	 * NativeQuery
	 * findBySw2User: ユーザー組織権限処理タイプ取得
	 */
	/*
	@SuppressWarnings("unchecked")
	@Override
	public Integer findBySw2User(String userId, String password){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("COUNT(*)");
		sb.append("FROM SW2.MST_USER MU ");
		sb.append("WHERE MU.USER_ACCOUNT = '" + userId + "' ");
		sb.append("AND MU.PASSWORD = LOWER(standard_hash('" +  password + "','MD5')) ");
		sb.append("AND MU.ID_DELETE_DATE IS NULL ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result!=null)
			return Integer.parseInt(result.get(0).toString());
		else
			return null;
	}
	*/

	/**
	 * JPA-JPQL
	 * findByUser: ユーザー認証情報取得（SW2ユーザー）
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Sw2MstUser findBySw2User(String userId, String password) {

		StringBuilder sb = new StringBuilder();

		//MD5ハッシュ化
		MessageDigest md;
		String myHash = password;
		try {
			//MD5ハッシュ化
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
		    byte[]digest = md.digest();
		    myHash =  DatatypeConverter.printHexBinary(digest).toUpperCase().toLowerCase();

		} catch (NoSuchAlgorithmException e) {
			return null;
		}

		sb.append("SELECT mu FROM Sw2MstUser mu ");
		sb.append("WHERE mu.isDeleteDate IS NULL ");
		sb.append("AND mu.userAccount = :userId ");
		sb.append("AND mu.password = :password ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Sw2MstUser> result =  em.createQuery(new String(sb))
					.setParameter("userId", userId)
				    .setParameter("password", myHash)
				    .getResultList();

		if (result.size() > 0)
			return (Sw2MstUser)result.get(0);

		else
			return null;
	}

	/**
	 * JPA-JPQL
	 * findBySw2User: ユーザー認証情報取得（SW2ユーザー）
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Sw2MstUser findBySw2User(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT mu FROM Sw2MstUser mu ");
		sb.append("WHERE mu.isDeleteDate IS NULL ");
		sb.append("AND mu.userAccount = :userId ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Sw2MstUser> result =  em.createQuery(new String(sb))
					.setParameter("userId", userId)
				    .getResultList();

		if (result.size() > 0)
			return (Sw2MstUser)result.get(0);

		else
			return null;
	}


	/**
	 * NativeQuery
	 * findByRelationUser: リレーションユーザ確認
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByRelationUser(String userId, int systemFlg){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		if(systemFlg == 0) {
			sb.append("SW2_USER_ID ");
		}else if(systemFlg == 1) {
			sb.append("GNAV_USER_ID ");
		}
		sb.append("FROM TBL_GNAV_USER_RELATION ");
		sb.append("WHERE ");
        if(systemFlg == 0) {
        	sb.append("GNAV_USER_ID = '" + userId + "' ");
		}else if(systemFlg == 1) {
			sb.append("SW2_USER_ID = '" + userId + "' ");
		}


		em.getEntityManagerFactory().getCache().evictAll();
		List<Object> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result.get(0).toString();
		else
			return null;

	}







}
