package com.businet.GNavApp.ejbs.user;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.MstUser;


@Stateless
@Local(IUserService.class)
public class UserService implements IUserService{


	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByUser: ユーザー認証情報取得（GNavユーザー）
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MstUser findByUser(String userId, String password) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT mu FROM MstUser mu ");
		sb.append("JOIN mu.mstSosiki ms ");
		sb.append("WHERE mu.delFlg = 0 ");
		sb.append("AND ms.lbxFlg = :lbxFlg ");
		sb.append("AND mu.userId = :userId ");
		sb.append("AND mu.password = :password ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<MstUser> result =  em.createQuery(new String(sb))
					.setParameter("lbxFlg", 0)
					.setParameter("userId", userId)
				    .setParameter("password", password)
				    .getResultList();

		if (result.size() > 0)
			return (MstUser)result.get(0);

		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * findByUser: ユーザー情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MstUser findByUser(String userId) {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT mu FROM MstUser mu ");
		sb.append("JOIN mu.mstSosiki ms ");
		sb.append("WHERE mu.delFlg = 0 ");
		sb.append("AND ms.lbxFlg = :lbxFlg ");
		sb.append("AND mu.userId = :userId ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<MstUser> result =  em.createQuery(new String(sb))
					.setParameter("lbxFlg", 0)
					.setParameter("userId", userId)
				    .getResultList();

		if (result.size() > 0)
			return (MstUser)result.get(0);

		else
			return null;
	}


	/**
	 * NativeQuery
	 * findByUserSosikiKengenNativeQuery: ユーザー組織権限取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByUserSosikiKengenNativeQuery(String userId){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MU.USER_ID, ");			// [0] ユーザーID
		sb.append("MS.SOSIKI_CD, ");		// [1] 組織コード
		sb.append("MS.KIGYOU_CD, ");		// [2] 企業コード
		sb.append("MS.GROUP_NO, ");			// [3] グループNO
		sb.append("MK.KENGEN_CD, ");		// [4] 権限コード
		sb.append("MK.PROCESS_TYPE_FLG ");	// [5] 処理タイプフラグ
		sb.append("FROM MST_USER MU ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("JOIN MST_KENGEN MK ");
		sb.append("ON MS.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE MU.USER_ID = '" + userId + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}


	/**
	 * NativeQuery
	 * findByUserSosikiProcessTypeFlg: ユーザー組織権限処理タイプ取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer findByUserSosikiProcessTypeFlg(String userId){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.PROCESS_TYPE_FLG ");	// [0] 処理タイプフラグ
		sb.append("FROM MST_USER MU ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("JOIN MST_KENGEN MK ");
		sb.append("ON MS.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE MU.USER_ID = '" + userId + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result!=null)
			return Integer.parseInt(result.get(0).toString());
		else
			return null;
	}


}
