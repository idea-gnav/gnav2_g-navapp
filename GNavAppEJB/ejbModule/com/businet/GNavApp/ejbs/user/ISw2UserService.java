package com.businet.GNavApp.ejbs.user;

import com.businet.GNavApp.entities.Sw2MstUser;

public interface ISw2UserService {



	/**
	 * NativeQuery
	 * findBySw2User: SW2ユーザー認証情報取得
	 */
	//Integer findBySw2User(String userId, String password);

	/**
	 * NativeQuery
	 * findBySw2User: SW2ユーザー認証情報取得
	 */
	Sw2MstUser findBySw2User(String userId);

	/**
	 * NativeQuery
	 * findByRelationUser: SW2連携ユーザー取得
	 */
	String findByRelationUser(String userId, int systemFlg);

	/**
	 * JPA-JPQL
	 * findByUser: ユーザー認証情報取得
	 */
	Sw2MstUser findBySw2User(String userId, String password);


}
