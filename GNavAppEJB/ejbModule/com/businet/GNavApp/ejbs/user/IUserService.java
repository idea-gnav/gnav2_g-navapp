package com.businet.GNavApp.ejbs.user;

import java.util.List;

import com.businet.GNavApp.entities.MstUser;

public interface IUserService {


	/**
	 * JPA-JPQL
	 * findByUser: ユーザー認証情報取得
	 */
	MstUser findByUser(String userId, String password);


	/**
	 * JPA-JPQL
	 * findByUser: ユーザー情報取得
	 */
	MstUser findByUser(String userId);


	/**
	 * NativeQuery
	 * findByUserSosikiKengenNativeQuery: ユーザー権限取得
	 */
	List<Object[]> findByUserSosikiKengenNativeQuery(String userId);


	/**
	 * NativeQuery
	 * findByUserSosikiProcessTypeFlg: ユーザー組織権限処理タイプ取得
	 */
	Integer findByUserSosikiProcessTypeFlg(String userId);

}
