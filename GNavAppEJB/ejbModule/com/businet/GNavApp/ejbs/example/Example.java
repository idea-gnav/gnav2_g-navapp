package com.businet.GNavApp.ejbs.example;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.MstUser;


@Stateless
@Local(IExample.class)
public class Example implements IExample{


	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/**
	 * test
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MstUser> Test1(){
		List<MstUser> l =  em.createQuery(
			    "SELECT u FROM MstUser u INNER JOIN u.tblUserDevices s WHERE s.delFlg=:param1")
				.setParameter( "param1", 0)
			    .getResultList();

		return l;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> Test2(){
		List<Object[]> l =  em.createQuery(
			    "SELECT u.userId, s.deviceToken FROM MstUser u INNER JOIN u.tblUserDevices s WHERE s.delFlg=:param1")
				.setParameter( "param1", 0)
			    .getResultList();

		return l;
	}



	/**
	 * JPA-JPQL
	 * findByCtrlTBuhinNo： コントローラーT部品名取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByCtrlTBuhinNo(String BuhinNo, String conType) {

			em.getEntityManagerFactory().getCache().evictAll();
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeT t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.code = :buhinNo "
					+ "AND t.conType = :conType ")
					.setParameter("buhinNo", BuhinNo)
					.setParameter("conType", conType)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}

}
