package com.businet.GNavApp.ejbs.example;

import java.util.List;

import com.businet.GNavApp.entities.MstUser;


public interface IExample {


	public List<MstUser> Test1();

	public List<Object[]> Test2();


	/**
	 * JPA-JPQL
	 * findByCtrlTBuhinNo： コントローラーT部品名取得
	 */
	String findByCtrlTBuhinNo(String BuhinNo, String conType);


}
