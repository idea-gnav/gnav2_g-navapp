package com.businet.GNavApp.ejbs.surveySearchInfo.java;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;

@Stateless
@Local(ISurveySearchInfoService.class)
public class SurveySearchInfoService implements ISurveySearchInfoService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;

	/**
	 * NativeQuery
	 * findByDairitenList : 代理店一覧
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByDairitenList(){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MS.SOSIKI_CD, ");
		sb.append("MK.KIGYOU_NAME || NVL2(MS.SOSIKI_NAME,' ','') || MS.SOSIKI_NAME ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("AND MS.DELETE_FLG = 0 ");
		sb.append("AND MS.IS_DAIRITEN = 1 ");
		sb.append("ORDER BY MK.KIGYOU_CD, MS.SOSIKI_CD ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * NativeQuery
	 * findByModelTypeList : 型式一覧(5,6型)
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<String> findByModelTypeList(){
	public List<String> findByModelTypeList(String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){

	StringBuilder sb = new StringBuilder();
/*
	sb.append("SELECT ");
	sb.append("DISTINCT MM.MACHINE_GENERATION ");
	sb.append("FROM MST_MACHINE MM ");
	sb.append("WHERE ");
	sb.append("MM.DELETE_FLAG = 0 ");
	sb.append("AND MM.MACHINE_GENERATION IS NOT NULL ");
	sb.append("ORDER BY MM.MACHINE_GENERATION ");
*/
	sb.append("SELECT ");
	sb.append("DISTINCT MM.MACHINE_GENERATION ");
	sb.append("FROM MST_MACHINE MM ");
	sb.append("WHERE ");
	sb.append("MM.DELETE_FLAG = 0 ");
	sb.append("AND MM.MACHINE_GENERATION IS NOT NULL ");
	if(typeFlg != 0) {
		sb.append("AND ( ");
	}

	if(typeFlg == 0 ) {
		// SCM
		sb.append("");
	}else if(typeFlg == 1) {
		//地区権限
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
		sb.append("OR ");
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
		sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
		sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
		sb.append("OR ");
		sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
		sb.append("OR ");
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
		sb.append("OR ");
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
				+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
				+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");
	}else if(typeFlg == 2) {
		//代理店権限
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
		sb.append("OR ");
		sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
		sb.append("OR ");
		sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
		sb.append(") ");
	}else if(typeFlg == 3) {
		//一般,レンタル
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
		sb.append("OR ");
		sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
		sb.append(") ");
	}else if(typeFlg == 4) {
		//サービス工場
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
		sb.append("OR ");
		sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
		sb.append("OR ");
		sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
		sb.append(") ");
	}else if(typeFlg == 21) {
		//SCMSEA
		sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
		sb.append("OR ");
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
		sb.append(") ");
	}else if(typeFlg == 31) {
		//トルコ代理店
		sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
		sb.append("OR ");
		sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
		sb.append(") ");
	}else if(typeFlg == 35) {
		// トルコサブ代理店
		sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
		sb.append("OR ");
		sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
		sb.append(") ");
	}
	else {
		sb.append("ROWNUM <= 0 ");
		sb.append(")");
	}

	sb.append("ORDER BY MM.MACHINE_GENERATION ");

	em.getEntityManagerFactory().getCache().evictAll();
	List<String> result = em.createNativeQuery(new String(sb)).getResultList();

	if(result.size() > 0)
		return result;
	else
		return null;
	}

	/**
	 * NativeQuery
	 * findByModelTypeList : 型式一覧(SCMモデル)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findByModelList(String modelType){

	StringBuilder sb = new StringBuilder();

	sb.append("SELECT ");
	sb.append("DISTINCT MM.MODEL_CD ");
	sb.append("FROM MST_MACHINE MM ");
	sb.append("WHERE ");
	sb.append("MM.DELETE_FLAG = 0 ");
	sb.append("AND MM.MODEL_CD IS NOT NULL ");
	sb.append("AND MM.MACHINE_GENERATION = '" + ConvertUtil.rpStr(modelType) + "' ");
	sb.append("ORDER BY MM.MODEL_CD ");

	em.getEntityManagerFactory().getCache().evictAll();
	List<String> result = em.createNativeQuery(new String(sb)).getResultList();

	if(result.size() > 0)
		return result;
	else
		return null;
	}


	/**
	 * NativeQuery
	 * findByApplivableUsesList : 作業内容一覧
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByApplivableUsesList(Integer deviceLanguage){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("CHOICE_ID AS APPLIVABLE_USES_ID, ");
		sb.append("CHOICE" + deviceLanguage + " AS APPLIVABLE_USES" + deviceLanguage );
		sb.append(" FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("WHERE ");
		sb.append("MSC.QUESTION_ID = 1 ");
		sb.append("AND MSC.DELETE_FLG = 0 ");
		sb.append("ORDER BY 1");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * NativeQuery
	 * findByYearList : 年度一覧
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findByYearList(){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("DISTINCT TO_CHAR(SURVEY_DATE,'YYYY') AS YEAR ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("WHERE ");
		sb.append("TSR.SURVEY_ID = 1 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("ORDER BY 1");

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> result = em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}

}
