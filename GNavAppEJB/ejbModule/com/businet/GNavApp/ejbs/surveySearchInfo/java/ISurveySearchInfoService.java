package com.businet.GNavApp.ejbs.surveySearchInfo.java;

import java.util.List;

public interface ISurveySearchInfoService {
	/**
	 * NativeQuery
	 * findByDairitenList : 代理店一覧
	 */
	List<Object[]> findByDairitenList();

	/**
	 * NativeQuery
	 * findByModelTypeList : 型式一覧(5,6型)
	 */
	List<String> findByModelTypeList(String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);

	/**
	 * NativeQuery
	 * findByModelList : 型式一覧(SCMモデル)
	 */
	List<String> findByModelList(String modelType);

	/**
	 * NativeQuery
	 * findByApplivableUsesList : 作業内容一覧
	 */
	List<Object[]> findByApplivableUsesList(Integer deviceLanguage);

	/**
	 * NativeQuery
	 * findByYearList : 年度一覧
	 */
	List<String> findByYearList();

}
