package com.businet.GNavApp.ejbs.machineSerchInfo;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@Local(IMachineSearchInfoService.class)
public class MachineSearchInfoService implements IMachineSearchInfoService{


	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;



	/**
	 * NativeQuery
	 * findByScmModelList : SCMモデル一覧取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	//**↓↓↓ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限  ↓↓↓**//
	//**↓↓↓ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↓↓↓**//
//	public List<String> findByScmModelList(){
//	public List<String> findByScmModelList(int paverOnlyFlg){
	//**↑↑↑ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↑↑↑**//
	public List<String> findByScmModelList(int paverOnlyFlg, Integer processTypeFlg, String kigyouCd, String sosikiCd, String groupNo){
	//**↑↑↑ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限 ↑↑↑**//

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT KATASHIKI FROM MST_KATASHIKI ");
		sb.append("WHERE DELETE_FLAG <> 1 ");
//		sb.append("AND KATASHIKI IN (SELECT DISTINCT MODEL_CD FROM MST_MACHINE) ");
		//**↓↓↓ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↓↓↓**//
		if(paverOnlyFlg == 1) {
			sb.append("AND KATASHIKI IN (SELECT DISTINCT MM.MODEL_CD FROM MST_MACHINE MM WHERE MM.CON_TYPE IN ('D','ND') ");
		}else {
			sb.append("AND KATASHIKI IN (SELECT DISTINCT MM.MODEL_CD FROM MST_MACHINE MM ");
		}
		//**↑↑↑ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↑↑↑**//
		//**↓↓↓ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限  ↓↓↓**//

		if(processTypeFlg != 0) {
			if(paverOnlyFlg == 1 )
				sb.append("AND ( ");
			else
				sb.append("WHERE ( ");
		}

		if(processTypeFlg == 0 ) {
			// SCM
			sb.append("");
		}else if(processTypeFlg == 1) {
			//地区権限
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");
		}else if(processTypeFlg == 2) {
			//代理店権限
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");
		}else if(processTypeFlg == 3) {
			//一般,レンタル
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");
		}else if(processTypeFlg == 4) {
			//サービス工場
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");
		}else if(processTypeFlg == 21) {
			//SCMSEA
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");
		}else if(processTypeFlg == 31) {
			//トルコ代理店
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");
		}else if(processTypeFlg == 35) {
			// トルコサブ代理店
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");
		}
		else {
			sb.append("ROWNUM <= 0 ");
			sb.append(")");
		}
		sb.append(")");
		//**↑↑↑ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限  ↑↑↑**//
		sb.append("ORDER BY KATASHIKI ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}


	/**
	 * NativeQuery
	 * findByToukatsubuList : 統括部一覧取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByToukatsubuList(){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT MT.TOUKATSUBU_CD, RTRIM(LTRIM(MT.TOUKATSUBU_NAME)) ");
		sb.append("FROM MST_TOUKATSUBU MT ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0)
			return result;
		else
			return null;
	}


	/**
	 * NativeQuery
	 * findByKyotenList : 拠点一覧
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByKyotenList(){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MKT.KYOTEN_CD, ");
		sb.append("RTRIM(LTRIM(MT.TOUKATSUBU_NAME)) || NVL2(MKT.KYOTEN_NAME,' ','') || RTRIM(LTRIM(MKT.KYOTEN_NAME)) ");
		sb.append("FROM MST_KYOTEN MKT ");
		sb.append("JOIN MST_TOUKATSUBU MT ");
		sb.append("ON MKT.KYOTEN_CD = MT.KYOTEN_CD ");
		sb.append("ORDER BY MT.TOUKATSUBU_CD, MKT.KYOTEN_CD ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * NativeQuery
	 * findByDairitenList : 代理店一覧
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByDairitenList(){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MS.SOSIKI_CD, ");
		sb.append("MK.KIGYOU_NAME || NVL2(MS.SOSIKI_NAME,' ','') || MS.SOSIKI_NAME ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("AND MS.DELETE_FLG = 0 ");
		sb.append("AND MS.IS_DAIRITEN = 1 ");
		sb.append("ORDER BY MK.KIGYOU_CD, MS.SOSIKI_CD ");
//		sb.append("");
//		sb.append("");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * NativeQuery
	 * findByServiceKoujouList : サービス工場一覧
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByServiceKoujouList(){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MS.SOSIKI_CD, ");							// [0]サービス工場コード RTRIM(LTRIM(MS.SOSIKI_NAME))
		sb.append("MK.KIGYOU_NAME || NVL2(MS.SOSIKI_NAME,' ','') || MS.SOSIKI_NAME ");	// [1]サービス工場名
//		sb.append("MK.KIGYOU_NAME ");	// [1]サービス工場名
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("AND MS.IS_SERVICE_KOUJOU = 1 ");
		sb.append("AND MS.DELETE_FLG = 0 ");
		sb.append("ORDER BY MK.KIGYOU_CD, MS.SOSIKI_CD ");
		sb.append("");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0)
			return result;
		else
			return null;
	}


	// **↓↓↓ 2018/11/02 iDEA山下  機械アイコン区分一覧　外部ファイル参照化のため削除 ↓↓↓**//

	/**
	 * NativeQuery
	 * findByMachineIconTypeList : 機械アイコン区分一覧
	 */
//	@Override
//	public List<Integer> findByMachineIconList(){

//		em.getEntityManagerFactory().getCache().evictAll();
//		List<Integer> result =  em.createNativeQuery("SELECT DISTINCT ICON_TYPE FROM MST_APP_DISPLAY_CONDITION WHERE DEL_FLG = 0 ORDER BY ICON_TYPE ").getResultList();
//		List<Integer> result =  em.createNativeQuery("SELECT DISTINCT ICON_TYPE FROM MST_APP_MACHINE_TYPE_CONTROL WHERE DEL_FLG = 0 ORDER BY ICON_TYPE ").getResultList();

		/*
		if(result.size() > 0)
			return result;
		else
			return null;
			*/
//		return null;
//	}
	// **↑↑↑ 2018/11/02 iDEA山下  機械アイコン区分一覧　外部ファイル参照化のため削除 ↑↑↑**//

	// 2019/04/10 DucNKT  定期整備画面に追加されました：開始
	/**
	 * NativeQuery
	 * findByMaintainecePartItemList
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findByMaintainecePartItemList(String languageCd) {
		StringBuilder sb = new StringBuilder();
		//↓↓↓ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :start  ↓↓↓//
		//↓↓↓ 2019/05/22 iDEA山下 サプライモジュールフィルタ(en)注釈置換追加  :start  ↓↓↓//
		sb.append("SELECT CASE WHEN LANGUAGE_CD = 'en' THEN REPLACE(MAINTAINECE_PART_NAME,'X4','SH-7') ");
		sb.append("ELSE MAINTAINECE_PART_NAME ");
		sb.append("END AS \"MAINTAINECE_PART_NAME\"");
//		sb.append("SELECT MAINTAINECE_PART_NAME ");
		sb.append("FROM( SELECT LANGUAGE_CD, ");
		sb.append("MAINTAINECE_PART_NAME, ");
//		sb.append("FROM( SELECT MAINTAINECE_PART_NAME, ");
		sb.append("ROW_NUMBER() OVER(PARTITION BY MAINTAINECE_PART_NAME ORDER BY MAINTAINECE_PART_NUMBER) AS RN, ");
		sb.append("ROW_NUMBER() OVER(ORDER BY MAINTAINECE_PART_NUMBER) AS RN2 ");
		sb.append("FROM MST_MAINTAINECE_PART ");
		sb.append("WHERE LANGUAGE_CD = '" + languageCd + "' ");
		sb.append("AND CON_TYPE IN ('S','T','C','T2','C2','N','ND','D','NH') ) ");
		sb.append("WHERE RN = 1 ORDER BY RN2 ");
		//↑↑↑ 2019/05/22 iDEA山下 サプライモジュールフィルタ(en)注釈置換追加 :end  ↑↑↑//
//		sb.append("SELECT DISTINCT MAINTAINECE_PART_NUMBER ,MAINTAINECE_PART_NAME ");
//		sb.append("FROM MST_MAINTAINECE_PART ");
//		sb.append("WHERE LANGUAGE_CD = '" + languageCd + "' ");
//		sb.append("AND CON_TYPE IN ('S','T','C','T2','C2','N','ND','D','NH') ");
//		sb.append("ORDER BY MAINTAINECE_PART_NUMBER ,MAINTAINECE_PART_NAME ");
		//↑↑↑ 2019/05/15 iDEA山下 部品名のみリスト化するよう変更 :end  ↑↑↑//

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}
	// 2019/04/10 DucNKT 定期整備画面に追加されました：終了

	// **↓↓↓ 2019/10/17  RASIS岡本 お客様名検索対応↓↓↓**//
	/**
	 * NativeQuery
	 * findByCustomerNameList
	 */
	@SuppressWarnings("unchecked")
	public List<String> findByCustomerNameList(Integer typeFlg,String kigyouCd,String sosikiCd,String groupNo) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("DISTINCT CONCAT(MK.KIGYOU_NAME || ' ', MS.SOSIKI_NAME) ");
		sb.append("FROM MST_MACHINE MM ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MM.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");


		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}
		sb.append("ORDER BY 1 ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0)
			return result;
		else
			return null;

	}
	// **↑↑↑ 2019/10/17  RASIS岡本 お客様名検索対応↑↑↑**//
}
