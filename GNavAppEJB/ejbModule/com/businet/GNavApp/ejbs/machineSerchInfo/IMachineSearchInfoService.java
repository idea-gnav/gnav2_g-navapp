package com.businet.GNavApp.ejbs.machineSerchInfo;

import java.util.List;

public interface IMachineSearchInfoService {


	/**
	 * NativeQuery
	 * findByScmModelList : SCMモデル一覧取得
	 */
	//**↓↓↓ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限  ↓↓↓**//
	//**↓↓↓ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↓↓↓**//
//	List<String> findByScmModelList();
//	List<String> findByScmModelList(int paverOnlyFlg);
	//**↑↑↑ 2019/11/07 iDEA山下 施工情報画面 型式リスト値制御 ↑↑↑**//
	List<String> findByScmModelList(int paverOnlyFlg, Integer processTypeFlg, String kigyouCd, String sosikiCd, String groupNo);
	//**↑↑↑ 2019/11/15 iDEA山下 型式リスト値　参照可能機番のモデルのみに制限 ↑↑↑**//

	/**
	 * NativeQuery
	 * findByToukatsubuList : 統括部一覧取得
	 */
	List<Object[]> findByToukatsubuList();
//	ArrayList<HashMap> findByToukatsubuList();


	/**
	 * NativeQuery
	 * findByKyotenList : 拠点一覧
	 */
	List<Object[]> findByKyotenList();


	/**
	 * NativeQuery
	 * findByDairitenList : 代理店一覧
	 */
	List<Object[]> findByDairitenList();



	/**
	 * NativeQuery
	 * findByServiceKoujouList : サービス工場一覧
	 */
	List<Object[]> findByServiceKoujouList();


	// **↓↓↓ 2018/11/02 iDEA山下  機械アイコン区分一覧　外部ファイル参照化のため削除 ↓↓↓**//
	/**
	 * NativeQuery
	 * findByMachineIconTypeList : 機械アイコン区分一覧
	 */
//	List<Integer> findByMachineIconList();
	// **↑↑↑ 2018/11/02 iDEA山下  機械アイコン区分一覧　外部ファイル参照化のため削除 ↑↑↑**//

	// 2019/04/10 DucNKT 定期整備画面に追加されました：開始
	/**
	* NativeQuery
	* findByaintainecePartItemList
	*/
	List<String> findByMaintainecePartItemList(String languageCd);
	//↓↓↓ 2019/05/15 iDEA山下 ArrayListからListに変更 :end ↓↓↓//
//	List<Object[]> findByMaintainecePartItemList(String languageCd);
	//↑↑↑ 2019/05/15 iDEA山下 ArrayListからListに変更 :end ↑↑↑//
    // 2019/04/10 DucNKT 定期整備画面に追加されました：終了

	// **↓↓↓ 2019/10/17  RASIS岡本 お客様名検索対応↓↓↓**//
	/**
	* NativeQuery
	* findByCustomerNameList
	*/
	List<String> findByCustomerNameList(Integer typeFlg,String kigyouCd,String sosikiCd,String groupNo);
	// **↑↑↑ 2019/10/17  RASIS岡本 お客様名検索対応↑↑↑**//
}
