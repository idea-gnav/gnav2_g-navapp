package com.businet.GNavApp.ejbs.displayUpdate;


import java.sql.Timestamp;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.TblUserSetting;

@Stateless
@Local(IDisplayUpdateService.class)
public class DisplayUpdateService implements IDisplayUpdateService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/*
	 * JPA
	 * displayUpdate
	 */
	@Override
	public void displayUpdate(String userId,Integer unitSelect,Integer kibanSelect,Integer customerNamePriority, Integer searchRangeDistance) {

		TblUserSetting tus = em.find(TblUserSetting.class,userId);
		tus.setUnitSelect(unitSelect);
		tus.setKibanSelect(kibanSelect);
		// **««« 2019/11/28 iDEARΊ ¨qlΌυDζtOΗΑ «««**//
		tus.setCustomerNamePriority(customerNamePriority);
		// **ͺͺͺ 2019/11/28 iDEARΊ ¨qlΌυDζtOΗΑ ͺͺͺ**//
		// **«««@2018/05/22  LBNΞμ  v]sοΗδ  No45 ΑθΚuυΞ ΗΑ  «««**//
		tus.setSearchRange(searchRangeDistance);
		// **ͺͺͺ@2018/05/22  LBNΞμ  v]sοΗδ  No45 ΑθΚuυΞ ΗΑ  ͺͺͺ**//
		tus.setUpdateUser(userId);
		tus.setUpdatePrg("GNavApp");
		tus.setUpdateDtm(new Timestamp(System.currentTimeMillis()));

		em.persist(tus);
	}




}
