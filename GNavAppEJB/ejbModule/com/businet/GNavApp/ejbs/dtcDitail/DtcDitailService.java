package com.businet.GNavApp.ejbs.dtcDitail;


import java.util.Arrays;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;


@Stateless
@Local(IDtcDitailService.class)
public class DtcDitailService implements IDtcDitailService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/**
	 * NativeQuery
	 * findByDtcDitail
	 * DTCΪΧΜDTCκπζΎ
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByDtcDitailNativeQuery(
			Long serialNumber,
			Integer sortFlg,
			String warningDateFrom,
			String warningDateTo,
			String warningCd,
			Integer userKengenCd,
			String languageCd,
			// **«««@2020/01/30  iDEARΊ ­ΆxρxΗΑΞ «««**//
			Integer fukkyuFlg
			// **ͺͺͺ@2020/01/30  iDEARΊ ­ΆxρxΗΑΞ ͺͺͺ**//
		){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TES.KIBAN_SERNO, ");							//0
		sb.append("TES.CON_TYPE, ");							//1
		sb.append("TES.EVENT_NO, ");							//2
		sb.append("MES.FLG_VISIBLE, ");							//3
		sb.append("MES.EVENT_CODE, ");							//4
		sb.append("MES.KUBUN_NAME, ");							//5
		sb.append("MES.EVENT_NAME, ");							//6
		sb.append("MES.MESSAGE, ");								//7
//		sb.append("MES.ALERT_LV, ");							//8
		sb.append("NVL2(");
		sb.append("MES.ALERT_LV,");
		sb.append("MES.ALERT_LV,");
		sb.append("(");
		sb.append("SELECT DISTINCT MES2.ALERT_LV FROM MST_EVENT_SEND MES2 ");
		sb.append("WHERE MES2.EVENT_CODE = TES.EVENT_NO ");
		sb.append("AND MES2.CON_TYPE = TES.CON_TYPE ");
		sb.append("AND MES2.MACHINE_KBN = TES.MACHINE_KBN ");
		sb.append("AND MES2.SHUBETU_CODE = TES.EVENT_SHUBETU ");
		sb.append("AND ROWNUM = 1 ");
		sb.append(")");
		sb.append(") as ALERT_LV, ");							//8
		sb.append("NVL(TES.HOUR_METER, 0.0) AS HOUR_METER, ");	//9
		sb.append("TES.HASSEI_TIMESTAMP_LOCAL, ");				//10
		sb.append("TES.FUKKYU_TIMESTAMP_LOCAL, ");				//11
		sb.append("TES.SHOZAITI_EN, ");							//12
		sb.append("TES.IDO, ");									//13
		sb.append("TES.KEIDO, ");								//14
		sb.append("TES.RECV_TIMESTAMP_LOCAL, ");				//15
//		sb.append("NVL(MES.FLG_VISIBLE,TES.EVENT_CODE) AS DTC_EVENT_NO ");					//16
//		sb.append("NVL2(MES.FLG_VISIBLE,MES.FLG_VISIBLE,TES.EVENT_NO) AS DTC_EVENT_NO ");	//16
		sb.append("NVL2(");
		sb.append("MES.FLG_VISIBLE,");
		sb.append("MES.FLG_VISIBLE,");
		sb.append("(");
		sb.append("SELECT DISTINCT MES2.FLG_VISIBLE FROM MST_EVENT_SEND MES2 ");
		sb.append("WHERE MES2.EVENT_CODE = TES.EVENT_NO ");
		sb.append("AND MES2.CON_TYPE = TES.CON_TYPE ");
		sb.append("AND MES2.MACHINE_KBN = TES.MACHINE_KBN ");
		sb.append("AND MES2.SHUBETU_CODE = TES.EVENT_SHUBETU ");
		sb.append("AND ROWNUM = 1 ");
		sb.append(")");
		sb.append(") as DTC_EVENT_NO ");						//16

		sb.append("FROM TBL_EVENT_SEND TES ");
		sb.append("INNER JOIN MST_MACHINE MM ON TES.KIBAN_SERNO = MM.KIBAN_SERNO ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
		// **««« 2020/09/29 iDEA Yamashita@πΜC³ «««**//
//		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("ON ( ");
		sb.append("TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE ");
		sb.append("OR (( MM.CON_TYPE != 'D' OR MM.EISEI_FLG != 1) AND NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE) )) ");
		// **ͺͺͺ 2020/09/29 iDEA Yamashita@πΜC³ ͺͺͺ**//
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = " + userKengenCd + " ");

//		sb.append("INNER JOIN MST_EVENT_SEND MES ");
//		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
//		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
//		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
//		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

		sb.append("LEFT JOIN ( ");
		sb.append("SELECT * FROM MST_EVENT_SEND ");
		sb.append("WHERE DELETE_FLAG = 0 ");
//		sb.append("AND LANGUAGE_CD IN ('"+languageCd.toLowerCase()+"', '"+languageCd.toUpperCase()+"') ");
		sb.append("AND NLS_LOWER(LANGUAGE_CD) = '" + languageCd.toLowerCase() + "' ");
		sb.append(") MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");
		sb.append("AND MES.DELETE_FLAG = 0 ");
		// **««« 2020/09/29 iDEA Yamashita@tO9f[^ΜQΖ§δπΗΑ «««**//
		sb.append("LEFT JOIN MST_KENGEN MK ON TK.KENGEN_CD = MK.KENGEN_CD ");

		sb.append("WHERE TES.KIBAN_SERNO = " + serialNumber + " ");
//		sb.append("AND TES.SYORI_FLG <> 9 ");
		sb.append("AND (( MK.KEIHOU_KENGEN != 1 AND TES.SYORI_FLG != 9 ) ");
		sb.append("OR ( MK.KEIHOU_KENGEN = 1 )) ");
		// **ͺͺͺ 2020/09/29 iDEA Yamashita@tO9f[^ΜQΖ§δπΗΑ ͺͺͺ**//
//		sb.append("AND MES.LANGUAGE_CD = 'en' ");
//		sb.append("AND MES.LANGUAGE_CD IN ('"+languageCd.toLowerCase()+"', '"+languageCd.toUpperCase()+"') ");

//		sb.append("AND TES.EVENT_SHUBETU = 8 ");
//		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
		sb.append("AND ((TES.EVENT_SHUBETU = 8 AND TES.CON_TYPE IN ('C','C2')) ");
		sb.append("OR (TES.EVENT_SHUBETU = 3 AND TES.CON_TYPE IN ('T2', 'T', 'S', 'D', 'N', 'NH', 'ND'))) ");
		sb.append("");

		sb.append("AND TES.KIBAN_SERNO = " + serialNumber + " ");

		if(warningDateFrom != null && warningDateTo != null) {
			sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL ");
			sb.append("BETWEEN TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			sb.append("AND TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}
		if(warningDateFrom != null && warningDateTo == null) {
			sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL ");
			sb.append(">= TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}

		if( warningDateFrom == null && warningDateTo != null) {
			sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL  ");
			sb.append("<= TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}
		// **«««@2020/01/30  iDEARΊ ­ΆxρxΗΑΞ «««**//
		if(fukkyuFlg == 1 ) {
			//1:’DTCπζΎ
			sb.append("AND TES.FUKKYU_TIMESTAMP_LOCAL IS NULL  ");
		}
		// **ͺͺͺ@2020/01/30  iDEARΊ ­ΆxρxΗΑΞ ͺͺͺ**//

		if(warningCd != null) {
			// **««« 2019/1/18 LBNΞμ ½ΎκΞ }X^ΙΞΎκ³’κΜDTCΤυC³  «««**//
//			if(ConvertUtil.isNumber(warningCd))
//				sb.append("AND (MES.FLG_VISIBLE LIKE '%" + warningCd + "%' OR MES.EVENT_CODE = " + warningCd + ") ");
//			if(!ConvertUtil.isNumber(warningCd))
//				sb.append("AND MES.FLG_VISIBLE LIKE '%" + ConvertUtil.rpStr(warningCd.toUpperCase()) + "%' ");
			if(ConvertUtil.isNumber(warningCd)) {
				sb.append("AND (");
				sb.append("NVL2(");
				sb.append("MES.FLG_VISIBLE,");
				sb.append("MES.FLG_VISIBLE,");
				sb.append("(");
				sb.append("SELECT DISTINCT MES2.FLG_VISIBLE FROM MST_EVENT_SEND MES2 ");
				sb.append("WHERE MES2.EVENT_CODE = TES.EVENT_NO ");
				sb.append("AND MES2.CON_TYPE = TES.CON_TYPE ");
				sb.append("AND MES2.MACHINE_KBN = TES.MACHINE_KBN ");
				sb.append("AND MES2.SHUBETU_CODE = TES.EVENT_SHUBETU ");
				sb.append("AND ROWNUM = 1 ");
				sb.append(")");
				sb.append(") LIKE '%" + warningCd + "%' ");
				sb.append("OR MES.EVENT_CODE = " + warningCd + ") ");
			}if(!ConvertUtil.isNumber(warningCd)) {
				sb.append("AND (");
				sb.append("NVL2(");
				sb.append("MES.FLG_VISIBLE,");
				sb.append("MES.FLG_VISIBLE,");
				sb.append("(");
				sb.append("SELECT DISTINCT MES2.FLG_VISIBLE FROM MST_EVENT_SEND MES2 ");
				sb.append("WHERE MES2.EVENT_CODE = TES.EVENT_NO ");
				sb.append("AND MES2.CON_TYPE = TES.CON_TYPE ");
				sb.append("AND MES2.MACHINE_KBN = TES.MACHINE_KBN ");
				sb.append("AND MES2.SHUBETU_CODE = TES.EVENT_SHUBETU ");
				sb.append("AND ROWNUM = 1 ");
				sb.append(")");
				sb.append(") LIKE '%" + ConvertUtil.rpStr(warningCd.toUpperCase()) + "%' ");
				sb.append(") ");
			}
			// **ͺͺͺ 2019/1/18 LBNΞμ ½ΎκΞ }X^ΙΞΎκ³’κΜDTCΤυC³  ͺͺͺ**//

		}

		sb.append("AND ROWNUM <= 1001 ");


		if(sortFlg != null) {
			// DTCx
			if(sortFlg == 0 || sortFlg == 1)
				sb.append("ORDER BY MES.ALERT_LV ");
			// ­Άϊ
			if(sortFlg == 2 || sortFlg == 3)
				sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL ");
			// ϊ
			if(sortFlg == 4 || sortFlg == 5)
				sb.append("ORDER BY TES.FUKKYU_TIMESTAMP_LOCAL ");
			// DTCΰe
			if(sortFlg == 6 || sortFlg == 7)
				sb.append("ORDER BY DTC_EVENT_NO ");
			// A[^[
			if(sortFlg == 8 || sortFlg == 9)
				sb.append("ORDER BY HOUR_METER ");

			if(sortFlg == 1 || sortFlg == 3 || sortFlg == 5 || sortFlg == 7 || sortFlg == 9)
				sb.append("DESC ");

			sb.append(", DTC_EVENT_NO ");

		}


		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0) {
			return result;
		}else {

			return null;
		}
	}



	/**
	 * JPA
	 * findByPDF: A3_PDFΆέ`FbN
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByPDF(Long serialNumber, String dtcEventNo, String languageCd) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> modelList = em.createQuery("SELECT m.modelCd FROM MstMachine m WHERE m.kibanSerno =:kibanSerno")
										.setParameter("kibanSerno", serialNumber)
										.getResultList();
		if(modelList.size()>0) {
			String modelCd = modelList.get(0);
			List<String> pathList = em.createQuery("SELECT p.pdfName FROM TblDtcPdf p "
					+ "WHERE p.delFlg = 0 AND p.modelCd = :modelCd and p.dtcCode =:dtcEventNo  AND p.languageCd IN :languageCd")
										.setParameter("modelCd", modelCd)
										.setParameter("dtcEventNo", dtcEventNo)
										.setParameter("languageCd", Arrays.asList(languageCd.toLowerCase(),languageCd.toUpperCase()))	// εΆA¬ΆΞ
										.getResultList();


			if(pathList.size()>0)
				return pathList.get(0);
		}
		return null;
	}


}
