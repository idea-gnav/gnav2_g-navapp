package com.businet.GNavApp.ejbs.dtcDitail;

import java.util.List;


public interface IDtcDitailService {


	/**
	 * NativeQuery
	 * findByDtcDitail
	 */
	List<Object[]> findByDtcDitailNativeQuery(
			Long serialNumber,
			Integer sortFlg,
			String warningDateFrom,
			String warningDateTo,
			String warningCd,
			Integer userKengenId,
			String languageCd,
			// **«««@2020/01/30  iDEARΊ ­ΆxρxΗΑ «««**//
			Integer fukkyuFlg
			// **ͺͺͺ@2020/01/30  iDEARΊ ­ΆxρxΗΑ ͺͺͺ**//
			);


	/**
	 * JPA
	 * findByPDF: A3_PDFΆέ`FbN
	 */
	String findByPDF(Long serialNumber, String dtcEventNo, String languageCd);
}
