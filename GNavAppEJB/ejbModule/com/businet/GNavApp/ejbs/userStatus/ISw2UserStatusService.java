package com.businet.GNavApp.ejbs.userStatus;


import com.businet.GNavApp.entities.Sw2TblGnavUserSetting;

public interface ISw2UserStatusService{


	/**
	 * JPA-JPQL
	 * findByUserSetting: ユーザー設定情報取得
	 */
	Sw2TblGnavUserSetting findBySw2UserSetting(String userId);


	/**
	 * JPA-JPQL
	 * insertByUserSetting: ユーザー設定情報登録
	 */
	void insertBySw2UserSetting(String userId);


	/**
	 * SW2認証キー取得用 暗号化処理
	 * exchangeSecurityKey
	 */
	String exchangeSecurityKey(String target, String seqKey);


	/**
	 * SW2認証キー取得用 復元化処理
	 * restoreSecurityKey
	 */
	String restoreSecurityKey(String target, String seqKey);





}
