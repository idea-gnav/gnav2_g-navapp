package com.businet.GNavApp.ejbs.userStatus;



import java.sql.Timestamp;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.Sw2TblGnavUserSetting;


@Stateless
@Local(ISw2UserStatusService.class)
public  class Sw2UserStatusService implements ISw2UserStatusService{


	@PersistenceContext(unitName="SW2")
    EntityManager em;

	private static  SecretKeySpec secretKey;

	/**
	 * JPA-JPQL
	 * findBySw2UserSetting: ユーザー設定情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Sw2TblGnavUserSetting findBySw2UserSetting(String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<Sw2TblGnavUserSetting> result =  em.createQuery(
				"SELECT us FROM Sw2TblGnavUserSetting us WHERE us.userId LIKE :userId")
			    .setParameter("userId", userId)
			    .getResultList();

		if (result.size() > 0)
			return (Sw2TblGnavUserSetting)result.get(0);
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * insertByUserSetting: ユーザー設定情報登録
	 */
	@Override
	public void insertBySw2UserSetting(String userId) {

		Timestamp dtm = new Timestamp(System.currentTimeMillis());
		Sw2TblGnavUserSetting appUserSetting = new Sw2TblGnavUserSetting();

		appUserSetting.setUserId(userId);
		appUserSetting.setApplogFlg(0);	// 通常0で登録
		appUserSetting.setDelFlg(0);
		appUserSetting.setRegistPrg("GNavApp");
		appUserSetting.setRegistDtm(dtm);
		appUserSetting.setRegistUser(userId);
		appUserSetting.setUpdatePrg("GNavApp");
		appUserSetting.setUpdateDtm(dtm);
		appUserSetting.setUpdateUser(userId);

		em.persist(appUserSetting);
		em.flush();
		em.clear();

		return;
	}

	/**
	 * SW2認証キー取得用 暗号化処理
	 * exchangeSecurityKey
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String exchangeSecurityKey(String target, String seqKey) {

		try
        {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(seqKey.getBytes(), "AES"));
			return new String(Base64.getUrlEncoder().encode(cipher.doFinal(target.getBytes())));

        }
        catch (Exception e)
        {
            System.out.println("暗号化error: "+ target + " " + e.toString());
        }
        return null;
	}

	/**
	 * SW2認証キー取得用 復元化処理
	 * exchangeSecurityKey
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String restoreSecurityKey(String target, String seqKey) {

		try
        {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(seqKey.getBytes(), "AES"));
			return new String(cipher.doFinal(Base64.getUrlDecoder().decode(target.getBytes())));

        }
        catch (Exception e)
        {
            System.out.println("復元化error: "+ target + " " + e.toString());
        }
        return null;
	}



}
