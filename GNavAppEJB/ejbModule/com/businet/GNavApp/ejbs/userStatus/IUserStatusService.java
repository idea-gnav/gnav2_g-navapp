package com.businet.GNavApp.ejbs.userStatus;

import com.businet.GNavApp.entities.TblUserDevice;
import com.businet.GNavApp.entities.TblUserSetting;

public interface IUserStatusService{


	/**
	 * JPA-JPQL
	 * findByUserSetting: [U[Ýèîñæ¾
	 */
	TblUserSetting findByUserSetting(String userId);


	/**
	 * JPA-JPQL
	 * insertByUserSetting: [U[Ýèîño^
	 */
	void insertByUserSetting(String userId, Integer unitSelect, Integer kibanSelect, Integer pdfFlg);

	/**
	 * JPA-JPQL
	 * updateByUserSetting: [U[Ýèîño^
	 */
	void updateByUserSetting(String userId, Integer kibanSelect);


	/**
	 * JPA-JPQL
	 * findByDevice: foCXîñæ¾
	 */
	TblUserDevice findByDevice(String deviceToken, String userId);


	/**
	 * JPA-JPQL
	 * insertByUserDevice: foCXîñVKo^
	 */
	void insertByUserDevice(String deviceToken, Integer deviceType, String userId, String languageCd, int appFlg, String loginDtm);


	/**
	 * JPA-JPQL
	 * insertByUserDevice: foCXîñXV
	 */
	void updateByUserDevice(String deviceToken, String userId, String languageCd, String loginDtm);



	// **««« 2018/12/12 LBNÎì [U[FØÇe[uÇÁ «««**//
	/**
	 * JPA-JPQL
	 * insertByTblAppUserStatusLog: Av[U[FØOo^
	 */
	void insertByTblUserStatusAppLog(
				String userId,
				Integer deviceType,
				String languageCd,
				Integer mode,
				String loginDtm
			);
	// **ªªª 2018/12/12 LBNÎì [U[FØÇe[uÇÁ ªªª**//



}
