package com.businet.GNavApp.ejbs.userStatus;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.entities.TblUserDevice;
import com.businet.GNavApp.entities.TblUserSetting;
import com.businet.GNavApp.entities.TblUserStatusAppLog;

@Stateless
@Local(IUserStatusService.class)
public class UserStatusService implements IUserStatusService{


	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/**
	 * JPA-JPQL
	 * findByUserSetting: [U[έθξρζΎ
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TblUserSetting findByUserSetting(String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<TblUserSetting> result =  em.createQuery(
				"SELECT us FROM TblUserSetting us WHERE us.userId LIKE :userId")
			    .setParameter("userId", userId)
			    .getResultList();

		if (result.size() > 0)
			return (TblUserSetting)result.get(0);
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * insertByUserSetting: [U[έθξρo^
	 */
	@Override
	public void insertByUserSetting(
					String userId,
					Integer unitSelect,
					Integer kibanSelect,
					Integer pdfFlg
				) {

		Timestamp dtm = new Timestamp(System.currentTimeMillis());
		TblUserSetting userSetting = new TblUserSetting();

		userSetting.setUserId(userId);
		userSetting.setUnitSelect(unitSelect);
		// **««« 2019/12/09 iDEARΊ ¨qlΌυDζtOΗΑ «««**//
		userSetting.setCustomerNamePriority(0);
		// **ͺͺͺ 2019/12/09 iDEARΊ ¨qlΌυDζtOΗΑ ͺͺͺ**//
		userSetting.setKibanSelect(kibanSelect);
		userSetting.setPdfFlg(pdfFlg);
		userSetting.setApplogFlg(0);	// Κν0Εo^
		userSetting.setRegistPrg("GNavApp");
		userSetting.setRegistDtm(dtm);
		userSetting.setRegistUser(userId);
		userSetting.setUpdatePrg("GNavApp");
		userSetting.setUpdateDtm(dtm);
		userSetting.setUpdateUser(userId);

		em.persist(userSetting);
		em.flush();
		em.clear();

		return;
	}



	/**
	 * JPA-JPQL
	 * updateByUserSetting: [U[έθξρXV
	 */
	@Override
	public void updateByUserSetting(String userId, Integer kibanSelect) {

		TblUserSetting userSetting = em.find(TblUserSetting.class, userId);

		userSetting.setKibanSelect(kibanSelect);
		userSetting.setUpdatePrg("GNavApp");
		userSetting.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
		userSetting.setUpdateUser(userId);

		em.persist(userSetting);
		em.flush();
		em.clear();

		return;
	}


	/**
	 * JPA-JPQL
	 * findByDevice: foCXξρζΎ
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TblUserDevice findByDevice(String deviceToken, String userId) {


//		List<TblUserDevice> result = null;
		em.getEntityManagerFactory().getCache().evictAll();
		List<TblUserDevice> result = new ArrayList<TblUserDevice>();

		if(userId != null) {
			result =  em.createQuery(
			    "SELECT ud FROM TblUserDevice ud WHERE ud.delFlg = 0 AND ud.deviceToken LIKE :deviceToken AND ud.userId LIKE :userId")
			    .setParameter("userId", userId)
			    .setParameter("deviceToken", deviceToken)
			    .getResultList();
		}else {
			result =  em.createQuery(
				    "SELECT ud FROM TblUserDevice ud WHERE ud.delFlg = 0 AND ud.deviceToken LIKE :deviceToken")
				    .setParameter("deviceToken", deviceToken)
				    .getResultList();
		}

		if (result.size() > 0)
			return (TblUserDevice)result.get(0);
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * insertByUserDevice: foCXξρVKo^
	 */
	@Override
	public void insertByUserDevice(String deviceToken, Integer deviceType, String userId, String languageCd, int appFlg, String loginDtm) {

		TblUserDevice userDevice = new TblUserDevice();
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		userDevice.setDeviceToken(deviceToken);
		userDevice.setDeviceType(deviceType);
		userDevice.setUserId(userId);
		userDevice.setUserLoginDtm(ConvertUtil.convertToTimeStamp(loginDtm));
		//userDevice.setUserLoginDtm(loginDtm);
		userDevice.setDelFlg(0);
		userDevice.setLanguageCd(languageCd);
		userDevice.setAppFlg(appFlg);
		userDevice.setRegistUser(userId);
		userDevice.setRegistPrg("GNavApp");
		userDevice.setRegistDtm(ts);
//		userDevice.setRegistDtm(ConvertUtil.getTimestampUTC(0));
		userDevice.setUpdateUser(userId);
		userDevice.setUpdatePrg("GNavApp");
		userDevice.setUpdateDtm(ts);
//		userDevice.setUpdateDtm(ConvertUtil.getTimestampUTC(0));
		em.persist(userDevice);

		return;
	}


	/**
	 * JPA-JPQL
	 * insertByUserDevice: foCXξρXV
	 */
	@Override
	public void updateByUserDevice(String deviceToken, String userId, String languageCd, String loginDtm) {

		TblUserDevice userDevice = em.find(TblUserDevice.class, deviceToken);
		userDevice.setUserId(userId);
//		userDevice.setUserLoginDtm(new Timestamp(System.currentTimeMillis()));
		userDevice.setUserLoginDtm(ConvertUtil.convertToTimeStamp(loginDtm));
		userDevice.setUpdateUser(userId);
		userDevice.setLanguageCd(languageCd);
		userDevice.setUpdatePrg("GNavApp");
		userDevice.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
		em.persist(userDevice);

		return;
	}


	// **««« 2018/12/12 LBNΞμ [U[FΨΗe[uΗΑ «««**//
	/**
	 * JPA-JPQL
	 * insertByTblAppUserStatusLog: [U[FΨAvP[VOL^
	 */
	@Override
	public void insertByTblUserStatusAppLog(
				String userId,
				Integer deviceType,
				String languageCd,
				Integer mode,
				String loginDtm
			) {

		TblUserStatusAppLog userStatuslog = new TblUserStatusAppLog();
		userStatuslog.setUserId(userId);
		userStatuslog.setUserLoginDtm(ConvertUtil.convertToTimeStamp(loginDtm)); // UTC
		userStatuslog.setAppFlg(1);				// 1:GNavApp
		userStatuslog.setAppMode(mode);
		userStatuslog.setDeviceType(deviceType);
		userStatuslog.setDeviceLanguage(languageCd);
		userStatuslog.setRegistPrg("GNavApp");
		userStatuslog.setRegistDtm(new Timestamp(System.currentTimeMillis()));
		userStatuslog.setRegistUser(userId);

//		em.getEntityManagerFactory().getCache().evictAll();
		em.persist(userStatuslog);

		return;
	}
	// **ͺͺͺ 2018/12/12 LBNΞμ [U[FΨΗe[uΗΑ ͺͺͺ**//



}
