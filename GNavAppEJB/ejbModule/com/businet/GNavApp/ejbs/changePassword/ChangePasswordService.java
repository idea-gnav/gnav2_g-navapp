package com.businet.GNavApp.ejbs.changePassword;


import java.sql.Timestamp;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.MstUser;




@Stateless
@Local(IChangePasswordService.class)
public class ChangePasswordService implements IChangePasswordService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/**
	 * JPA-JPQL
	 * updateByPassword: パスワード更新
	 */
	@Override
	public void updateByPassword(String userId, String password) {

			MstUser user = em.find(MstUser.class, userId);
			user.setPassword(password);
			user.setNewPasswordDate(new Timestamp(System.currentTimeMillis()));
			user.setUpdpwdFlg("0");
			user.setUpdateUser(userId);
			user.setUpdatePrg("GNavApp");
			user.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			em.persist(user);

			return;
	}




}
