package com.businet.GNavApp.ejbs.changePassword;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.DatatypeConverter;

import com.businet.GNavApp.entities.Sw2MstUser;




@Stateless
@Local(ISw2ChangePasswordService.class)
public class Sw2ChangePasswordService implements ISw2ChangePasswordService{

	@PersistenceContext(unitName="SW2")
    EntityManager em;


	/**
	 * JPA-JPQL
	 * updateByPassword: SW2パスワード更新
	 */
	@Override
	public void updateByPassword(Integer id, String password ) {

		    MessageDigest md;
			try {
				//MD5ハッシュ化
				md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
			    byte[]digest = md.digest();
			    String myHash =  DatatypeConverter.printHexBinary(digest).toUpperCase().toLowerCase();

			    Sw2MstUser user = em.find(Sw2MstUser.class, id);
			    user.setPassword(myHash);
			    user.setPasswordLastUpdateDate(new Date(System.currentTimeMillis()));
			    user.setOneTimePasswordIssue("0");
			    user.setAccountLockTimes(0);
			    user.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
			    user.setLastUpdateUserId(id);
			    user.setLastUpdateProgram("GNAV");
			    em.persist(user);

			} catch (NoSuchAlgorithmException e) {
				return;
			}

			return;
	}




}
