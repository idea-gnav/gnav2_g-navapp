package com.businet.GNavApp.ejbs.dtcNotice;

import java.util.List;

public interface IDtcNoticeService {


//
//	/**
//	 *
//	 * deleteTblFavoriteDtcHistory　
//	 */
//	void deleteTblFavoriteDtcHistory(String userId);
//

	/**
	 * findByNotExistsTblFavoriteDtcHistory
	 * お気に入りDTC履歴テーブルに存在しない、お気に入り登録済の未復旧DTCを取得
	 */
	int insertByNotExistsTblFavoriteDtcHistory(String userId, Integer kengenCd, Long serialNumber, String languageCd);


	/**
	 * 既読フラグ0、または削除フラグを更新
	 * updateTblFavoriteDtcHistory　
	 */
	void updateTblFavoriteDtcHistory(String userId, Long serialNumber, Integer readFlg, Integer delFlg);


	/**
	 * 復旧済みの既読処理
	 * updateByFukkyuReadFlg
	 * DEL_FLG 0, 1 に関わらず、お気に入りDTCの復旧済み既読フラグ更新
	 */
	void updateFukkyuDtcTblFavoriteDtcHistory(String userId);


	/**
	 * TBL_EVENT_SEND に存在しないイベントデータの考慮
	 * 過去に履歴テーブルに記録されたが、イベントテーブルのキー整合性とれない場合の対応
	 * 既読フラグ1、削除フラグ1に更新
	 */
	void updateNoMatchTblFavoriteDtcHistory(String userId);


	/**
	 * findByDtcNotices: トップお気に入りDTC一覧取得
	 * 					  発生後直近2日DTCおよび3日以前の未復旧の未読DTC
	 */
	List<Object[]> findByDtcNoticesNativeQuery(String userId, Integer kengenCd, String languageCd);


	/**
	 * findByNoReadCount:未読件数を取得
	 *
	 */
	int findByNoReadCount(String userId);


}
