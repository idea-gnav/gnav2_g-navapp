package com.businet.GNavApp.ejbs.dtcNotice;


import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;


@Stateless
@Local(IDtcNoticeService.class)
public class DtcNoticeService implements IDtcNoticeService{

	private static final Logger logger = Logger.getLogger(DtcNoticeService.class.getName());

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;




	/**
	 * findByNotExistsTblFavoriteDtcHistory
	 * お気に入りDTC履歴テーブルに存在しない、お気に入り登録済の未復旧DTCを取得
	 */
	@Override
	public int insertByNotExistsTblFavoriteDtcHistory(String userId, Integer kengenCd, Long serialNumber, String languageCd) {



		StringBuilder sb = new StringBuilder();

		sb.append("INSERT INTO TBL_FAVORITE_DTC_HISTORY( ");
		sb.append("USER_ID, ");
		sb.append("KIBAN_SERNO, ");
		sb.append("KIBAN_ADDR, ");
		sb.append("EVENT_NO, ");
		sb.append("EVENT_SHUBETU, ");
		sb.append("RECV_TIMESTAMP, ");
		sb.append("HASSEI_TIMESTAMP, ");
		sb.append("CON_TYPE, ");
		sb.append("READ_FLG, ");
		sb.append("DEL_FLG, ");
		sb.append("REGIST_DTM, ");
		sb.append("REGIST_USER, ");
		sb.append("REGIST_PRG, ");
		sb.append("UPDATE_DTM, ");
		sb.append("UPDATE_USER, ");
		sb.append("UPDATE_PRG ");
		sb.append(") ");

		sb.append("SELECT '"+userId+"', ");
		sb.append("TES.KIBAN_SERNO, ");
		sb.append("TES.KIBAN_ADDR, ");
		sb.append("TES.EVENT_NO, ");
		sb.append("TES.EVENT_SHUBETU, ");
		sb.append("TES.RECV_TIMESTAMP, ");
		sb.append("TES.HASSEI_TIMESTAMP, ");
		sb.append("TES.CON_TYPE, ");
		sb.append("0, 0, SYSDATE, '"+userId+"', 'GNavApp', SYSDATE, '"+userId+"', 'GNavApp' ");

		sb.append("FROM TBL_EVENT_SEND TES ");

		sb.append("JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TES.KIBAN_SERNO = TUF.KIBAN_SERNO ");
		sb.append("AND TUF.DEL_FLG = 0 ");
		sb.append("AND TUF.USER_ID = '"+userId+"' ");

		// **↓↓↓ 2020/09/29 iDEA Yamashita　結合条件の修正 ↓↓↓**//
		sb.append("JOIN MST_MACHINE mm ON tes.KIBAN_SERNO = mm.KIBAN_SERNO ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
//		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("ON ( ");
		sb.append("TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE ");
		sb.append("OR (( MM.CON_TYPE != 'D' OR MM.EISEI_FLG != 1) AND NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE) )) ");
		// **↑↑↑ 2020/09/29 iDEA Yamashita　結合条件の修正 ↑↑↑**//
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = "+kengenCd+" ");

		sb.append("LEFT JOIN ( ");
		sb.append("SELECT DISTINCT EVENT_CODE, SHUBETU_CODE, CON_TYPE, MACHINE_KBN, DELETE_FLAG ");
		sb.append("FROM MST_EVENT_SEND ");
		sb.append("WHERE DELETE_FLAG = 0 ");
//		sb.append("AND LANGUAGE_CD IN ('"+languageCd.toLowerCase()+"', '"+languageCd.toUpperCase()+"') ");	// 言語考慮無し
		sb.append(") MES ");

//		sb.append("INNER JOIN MST_EVENT_SEND MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");
		sb.append("AND MES.DELETE_FLAG = 0 ");

//		sb.append("AND MES.LANGUAGE_CD = 'en' ");
//		sb.append("AND TES.EVENT_SHUBETU = 8 ");
//		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
//		sb.append("AND MES.LANGUAGE_CD = '"+languageCd+"' ");

//		sb.append("AND ((TES.EVENT_SHUBETU = 8 AND TES.CON_TYPE IN ('C','C2')) ");
//		sb.append("OR (TES.EVENT_SHUBETU = 3 AND TES.CON_TYPE IN ('T2', 'T', 'S', 'D', 'N', 'NH', 'ND'))) ");
//		sb.append("");

		// **↓↓↓ 2020/09/29 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↓↓↓**//
		sb.append("LEFT JOIN MST_KENGEN MK ON TK.KENGEN_CD = MK.KENGEN_CD ");

//		sb.append("WHERE TES.SYORI_FLG <> 9 ");
		sb.append("WHERE ((TES.EVENT_SHUBETU = 8 AND TES.CON_TYPE IN ('C','C2')) ");
		sb.append("OR (TES.EVENT_SHUBETU = 3 AND TES.CON_TYPE IN ('T2', 'T', 'S', 'D', 'N', 'NH', 'ND'))) ");

		sb.append("AND (( MK.KEIHOU_KENGEN != 1 AND TES.SYORI_FLG != 9 ) ");
		sb.append("OR ( MK.KEIHOU_KENGEN = 1 )) ");
		// **↑↑↑ 2020/09/29 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↑↑↑**//

		if(serialNumber!=null)
			sb.append("AND TES.KIBAN_SERNO = "+serialNumber+" ");

		sb.append("AND TES.FUKKYU_TIMESTAMP_LOCAL IS NULL ");

		sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL >=  TO_TIMESTAMP('" + ConvertUtil.getDays(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
//		sb.append("AND TES.REGIST_DTM >=  TO_TIMESTAMP('" + ConvertUtil.getDaysUTC(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");

		sb.append("AND NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"'");
		sb.append("AND TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.CON_TYPE = TFDH.CON_TYPE ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") ");

		sb.append("AND TES.FUKKYU_TIMESTAMP_LOCAL IS NULL ");

		sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL DESC ");

		em.getEntityManagerFactory().getCache().evictAll();
		int insert =  em.createNativeQuery(new String(sb)).executeUpdate();

		if(insert>0)
			em.flush();
		em.clear();

		return insert;

	}



	/**
	 * 既読フラグ0、または削除フラグを更新
	 * updateTblFavoriteDtcHistory
	 */
	@Override
	public void updateTblFavoriteDtcHistory(String userId, Long serialNumber, Integer readFlg, Integer delFlg) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY ");
		if(delFlg!=null)
			sb.append("SET DEL_FLG = " + delFlg + ", ");
		if(readFlg!=null)
			sb.append("SET READ_FLG = " + readFlg + ", ");

		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG = 'GNavApp', ");
		sb.append("UPDATE_DTM = SYSDATE ");
		sb.append("WHERE USER_ID = '" + userId + "' ");
		sb.append("AND KIBAN_SERNO = " + serialNumber);

		int updated = em.createNativeQuery(new String(sb)).executeUpdate();

		if(updated>0)
			em.flush();
		em.clear();

		return;
	}



	/**
	 * 復旧済みの既読処理
	 * updateByFukkyuReadFlg
	 * DEL_FLG 0, 1 に関わらず、お気に入りDTCの復旧済み既読フラグ更新
	 */
	@Override
	public void updateFukkyuDtcTblFavoriteDtcHistory(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH SET TFDH.READ_FLG = 1, ");
		sb.append("TFDH.UPDATE_USER = '"+userId+"', ");
		sb.append("TFDH.UPDATE_DTM = sysdate, ");
		sb.append("TFDH.UPDATE_PRG = 'GNavApp' ");
		sb.append("WHERE EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TES.FUKKYU_TIMESTAMP IS NOT NULL ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");
		sb.append(") ");

		int updated = em.createNativeQuery(new String(sb)).executeUpdate();

		if(updated>0)
			em.flush();
		em.clear();

		return;
	}



	/**
	 * TBL_EVENT_SEND に存在しないイベントデータの考慮
	 * 過去に履歴テーブルに記録されたが、イベントテーブルのキー整合性とれない場合の対応
	 * 既読フラグ1、削除フラグ1に更新
	 */
	@Override
	public void updateNoMatchTblFavoriteDtcHistory(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("SET DEL_FLG = 1, ");
		sb.append("READ_FLG = 1, ");
		sb.append("UPDATE_DTM = sysdate, ");
		sb.append("UPDATE_USER = '"+userId+"', ");
		sb.append("UPDATE_PRG = 'GNavApp' ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND USER_ID = '"+userId+"' ");
		sb.append("AND ( NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") OR EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TFDH.DEL_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");
		sb.append("AND TES.SYORI_FLG = 9 ");
		// **↓↓↓ 2020/09/29 iDEA Yamashita　履歴無効化対象から警報権限=1のユーザを除外 ↓↓↓**//
		sb.append("AND NOT EXISTS ( ");
		sb.append("SELECT * ");
		sb.append("FROM MST_USER MU ");
		sb.append("LEFT JOIN MST_SOSIKI MS ON MU.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KENGEN MK ON MS.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE TFDH.USER_ID = MU.USER_ID ");
		sb.append("AND MK.KEIHOU_KENGEN = 1 ");
		sb.append("))) ");
		// **↑↑↑ 2020/09/29 iDEA Yamashita　履歴無効化対象から警報権限=1のユーザを除外 ↑↑↑**//

		em.createNativeQuery(new String(sb)).executeUpdate();

		return;

	}






	/**
	 * NativeQuery
	 * findByDtcNotices: トップお気に入りDTC一覧取得
	 * 					  発生後直近2日DTCおよび3日以前の未復旧の未読DTC
	 */
	@Override
	public List<Object[]> findByDtcNoticesNativeQuery(String userId, Integer kengenCd, String languageCd){


		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");

		sb.append("TES.KIBAN_SERNO, ");				// 0
		sb.append("TES.CON_TYPE, ");				// 1
		sb.append("TES.EVENT_NO, ");				// 2
//		sb.append("NVL2(MES.FLG_VISIBLE,MES.FLG_VISIBLE,TES.EVENT_NO) as VISIBLE, ");	// 3
		sb.append("NVL2( ");
		sb.append("MES.FLG_VISIBLE,");
		sb.append("MES.FLG_VISIBLE,");
		sb.append("(");
		sb.append("SELECT DISTINCT MES2.FLG_VISIBLE FROM MST_EVENT_SEND MES2 ");
		sb.append("WHERE MES2.EVENT_CODE = TES.EVENT_NO ");
		sb.append("AND MES2.CON_TYPE = TES.CON_TYPE ");
		sb.append("AND MES2.MACHINE_KBN = TES.MACHINE_KBN ");
		sb.append("AND MES2.SHUBETU_CODE = TES.EVENT_SHUBETU ");
		sb.append("AND ROWNUM = 1 ");
		sb.append(")");
		sb.append(") as VISIBLE,");					// 3
		sb.append("MES.FLG_VISIBLE, ");				// 4
		sb.append("MES.KUBUN_NAME, ");				// 5
		sb.append("MES.EVENT_NAME, ");				// 6
		sb.append("MES.MESSAGE, ");					// 7
//		sb.append("MES.ALERT_LV, ");				// 8
		sb.append("NVL2( ");
		sb.append("MES.ALERT_LV,");
		sb.append("MES.ALERT_LV,");
		sb.append("(");
		sb.append("SELECT DISTINCT MES2.ALERT_LV FROM MST_EVENT_SEND MES2 ");
		sb.append("WHERE MES2.EVENT_CODE = TES.EVENT_NO ");
		sb.append("AND MES2.CON_TYPE = TES.CON_TYPE ");
		sb.append("AND MES2.MACHINE_KBN = TES.MACHINE_KBN ");
		sb.append("AND MES2.SHUBETU_CODE = TES.EVENT_SHUBETU ");
		sb.append("AND ROWNUM = 1 ");
		sb.append(")");
		sb.append(") as ALERT_LV,");				// 8
		sb.append("TES.HOUR_METER, ");				// 9
		sb.append("TES.HASSEI_TIMESTAMP_LOCAL, ");	// 10
		sb.append("TES.FUKKYU_TIMESTAMP_LOCAL, ");	// 11
		sb.append("TES.SHOZAITI_EN, ");				// 12
		sb.append("TES.IDO, ");						// 13
		sb.append("TES.KEIDO, ");					// 14
		sb.append("MM.KIBAN, ");					// 15
		sb.append("MM.LBX_KIBAN, ");				// 16
		sb.append("MM.USER_KANRI_NO, ");			// 17
		sb.append("TES.RECV_TIMESTAMP_LOCAL, ");	// 18
		sb.append("TFDH.READ_FLG ");				// 19

		sb.append("FROM TBL_EVENT_SEND TES ");

		sb.append("JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TES.KIBAN_SERNO = TUF.KIBAN_SERNO ");
		sb.append("AND TUF.USER_ID = '"+userId+"' ");
		sb.append("AND TUF.DEL_FLG = 0 ");

		sb.append("JOIN TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("ON TFDH.DEL_FLG = 0 ");
		sb.append("AND TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");

		sb.append("LEFT JOIN ( ");
		sb.append("SELECT * FROM MST_EVENT_SEND ");
		sb.append("WHERE DELETE_FLAG = 0 ");
//		sb.append("AND LANGUAGE_CD IN ('"+languageCd.toLowerCase()+"', '"+languageCd.toUpperCase()+"') ");
		sb.append("AND LOWER(LANGUAGE_CD) IN ('"+languageCd.toLowerCase()+"') ");
		sb.append(") MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

		sb.append("JOIN MST_MACHINE MM ON TES.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("AND ((TES.EVENT_SHUBETU = 8 AND TES.CON_TYPE IN ('C','C2')) ");
		sb.append("OR (TES.EVENT_SHUBETU = 3 AND TES.CON_TYPE IN ('T2', 'T', 'S', 'D', 'N', 'NH', 'ND'))) ");
		sb.append("AND ( ( ");
		sb.append("TES.FUKKYU_TIMESTAMP_LOCAL IS NULL ");
		sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL >= TO_TIMESTAMP('" + ConvertUtil.getDays(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		sb.append(") OR ( ");
		sb.append("EXISTS( ");
		sb.append("SELECT * FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE TFDH.DEL_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");
		sb.append("AND TFDH.READ_FLG = 0 ) ");
		sb.append("AND TFDH.READ_FLG = 0 ");
		sb.append("AND TES.FUKKYU_TIMESTAMP_LOCAL IS NULL ");
		sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL < TO_TIMESTAMP('" + ConvertUtil.getDays(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		sb.append(") ) ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
		// **↓↓↓ 2020/09/29 iDEA Yamashita　結合条件の修正 ↓↓↓**//
//		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("ON ( ");
		sb.append("TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE ");
		sb.append("OR (( MM.CON_TYPE != 'D' OR MM.EISEI_FLG != 1) AND NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE) )) ");
		// **↑↑↑ 2020/09/29 iDEA Yamashita　結合条件の修正 ↑↑↑**//
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = "+kengenCd+" ");

		// **↓↓↓ 2020/09/29 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↓↓↓**//
//		sb.append("AND TES.SYORI_FLG <> 9 ");
		sb.append("LEFT JOIN MST_KENGEN MK ON TK.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE ");
		sb.append("( MK.KEIHOU_KENGEN != 1 AND TES.SYORI_FLG != 9 ) ");
		sb.append("OR ( MK.KEIHOU_KENGEN = 1 ) ");
		// **↑↑↑ 2020/09/29 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↑↑↑**//


		sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL DESC, ");
		sb.append("MM.KIBAN,  VISIBLE");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();


		logger.config(new String(sb));

		if(result.size() > 0)
			return result;
		else
			return null;
	}





	/**
	 * findByNoReadCount:未読件数を取得
	 *
	 */
	@Override
	public int findByNoReadCount(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT COUNT(*) FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE TFDH.DEL_FLG = 0 ");
		sb.append("AND TFDH.READ_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		Object dtcNoReadCount = result.get(0);
		String count = dtcNoReadCount.toString();

		if(count!=null)
			return Integer.parseInt(count);
		else
			return 0;
	}




}
