package com.businet.GNavApp.ejbs;


import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * Session Bean implementation class Initializer
 * <p>
 * This bean initializes the database if it detects the database has not been initialized.
 * </p>
 */
//@Singleton
//@LocalBean
//@Startup
public class Initializer {

    /**
     * The name of this class
     */
    public static final String className = Initializer.class.getName();

    /**
     * The logger to use for this class
     */
    private static final Logger logger = Logger.getLogger( className );

    @PersistenceContext( unitName = "LBXEJB" )
    private EntityManager em;

    /**
     * Default constructor.
     */
    public Initializer() {
    }

    
    @PostConstruct
    @TransactionAttribute( TransactionAttributeType.REQUIRED )
    public void initialize() {
        final String methodName = "initialize";
		logger.entering(className, methodName);
		try {
			
		} finally {
			logger.exiting(className, methodName);
		}
    }

}
