package com.businet.GNavApp.ejbs.favorite;

import java.util.List;

import com.businet.GNavApp.entities.TblUserSetting;

public interface IMachineFavoriteListService {

	/**
	 * JPA
	 * findKibanSelect
	 */
	TblUserSetting findKibanSelect(String userId);


	/**
	 * JPA
	 * findFavoriteMachine
	 */
	List<Object[]> findFavoriteMachine(String userId, Integer kibanSelect, Integer sortFlg);


	/**
	 * NativeQuery
	 * findFavoriteMachine
	 */
	List<Object[]> findFavoriteMachineNativeQuery(String userId, Integer kibanSelect, Integer sortFlg);
}
