package com.businet.GNavApp.ejbs.favorite;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.TblUserSetting;


@Stateless
@Local(IMachineFavoriteListService.class)
public class MachineFavoriteListService implements IMachineFavoriteListService{

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;



	/**
	 * JPA
	 * findKibanSelect
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TblUserSetting findKibanSelect(String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<TblUserSetting> result = em.createQuery("SELECT t FROM TblUserSetting t WHERE t.userId = :userId ")
				.setParameter("userId", userId)
				.getResultList();

		if (result.size() > 0)
			return (TblUserSetting)result.get(0);
		else
			return null;
	}




	/**
	 * JPA
	 * findFavoriteMachine
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findFavoriteMachine(String userId, Integer kibanSelect, Integer sortFlg) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT tuf.userId, ");	// 0
		sb.append("mm.kibanSerno, ");		// 1
		sb.append("mm.kiban, ");			// 2
		sb.append("mm.lbxKiban, ");			// 3
		sb.append("mm.userKanriNo ");		// 4
		sb.append("FROM TblUserFavorite utf ");
		sb.append("JOIN uth.mstMachine mm ");
		sb.append("WHERE tuf.delFlg = 0 ");
		sb.append("AND tuf.userId = :userId");

		if(kibanSelect == 0 || kibanSelect == 3)
			sb.append(" ORDER BY mm.kiban");
		else if(kibanSelect == 1)
			sb.append(" ORDER BY mm.lbxKiban");
		else if(kibanSelect == 2)
			sb.append(" ORDER BY mm.userKanriNo");

		if(sortFlg == 1)
			sb.append(" DESC");


		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> favoriteMachineList = em.createQuery(sb.toString())
				  .setParameter("userId",userId)
				  .getResultList();

		return favoriteMachineList;
	}


	/**
	 * NativeQuery
	 * findFavoriteMachine
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findFavoriteMachineNativeQuery(String userId, Integer kibanSelect, Integer sortFlg) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("TUF.USER_ID, ");			// 0
		sb.append("MM.KIBAN_SERNO, ");		// 1
		sb.append("MM.KIBAN, ");			// 2
		sb.append("MM.LBX_KIBAN, ");		// 3
		sb.append("MM.USER_KANRI_NO, ");	// 4
		sb.append("CONCAT(MK.KIGYOU_NAME || ' ', MS.SOSIKI_NAME) AS CUSTOMERNAME ");	// 5
		sb.append("FROM MST_MACHINE MM ");
		sb.append("JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON MM.KIBAN_SERNO = TUF.KIBAN_SERNO ");
		sb.append("LEFT JOIN MST_SOSIKI MS ON  MM.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KIGYOU MK ON  MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		sb.append("AND TUF.DEL_FLG = 0 ");
		sb.append("AND TUF.USER_ID = '" + userId + "' ");

		if(kibanSelect == 0)
			sb.append("ORDER BY MM.KIBAN ");
		else if(kibanSelect == 1)
			sb.append("ORDER BY MM.LBX_KIBAN  ");
		else if(kibanSelect == 2)
			sb.append("ORDER BY MM.USER_KANRI_NO ");
		else if(kibanSelect == 3)
			sb.append("ORDER BY CUSTOMERNAME ");
		
		if(sortFlg == 1)
			sb.append("DESC ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> favoriteMachineList = em.createNativeQuery(sb.toString())
				  .getResultList();

		return favoriteMachineList;
	}

}
