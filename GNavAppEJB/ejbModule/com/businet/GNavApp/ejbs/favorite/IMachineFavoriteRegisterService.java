package com.businet.GNavApp.ejbs.favorite;

import com.businet.GNavApp.ejbs.user.IUserService;

public interface IMachineFavoriteRegisterService extends IUserService{


	/**
	 * JPA
	 * addFavoriteMachine
	 */
	void addFavoriteMachine(Long addSerialNumber, String userId);


	/**
	 * JPA
	 * removeFavoriteMachine
	 */
	void removeFavoriteMachine(Long removeSerialNumber,String userId);




}
