package com.businet.GNavApp.ejbs.authentication;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.entities.AuthenticationToken;


@Stateless
@Local(IAuthentication.class)
public class Authentication implements IAuthentication{
	
	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;
	
	@Override
	public AuthenticationToken generateToken(String deviceId, String loginId) {
		List tokens;
		AuthenticationToken token;
		
		
		tokens = em.createQuery(
			    "SELECT at FROM AuthenticationToken at WHERE at.deviceId LIKE :deviceId AND at.loginId LIKE :loginId")
			    .setParameter("deviceId", deviceId)
			    .setParameter("loginId", loginId)
			    .getResultList();
		
		if (tokens.size() == 0) {		
			token = new AuthenticationToken();
			token.setCreatedDate(new Date());
			token.setDeviceId(deviceId);
			token.setLoginId(loginId);
			token.setToken(java.util.UUID.randomUUID().toString());
		}
		else {
			token = (AuthenticationToken) tokens.get(0);
			token.setToken(java.util.UUID.randomUUID().toString());
		}
		em.persist(token);
		return token;
	}

	@Override
	public boolean verifyToken(String token) {
		List at =  em.createQuery(
			    "SELECT at FROM AuthenticationToken at WHERE at.token LIKE :token")
			    .setParameter("token", token)
			    .getResultList();

		if (at.size()> 0)
			return true;
		else
			return false;
	}

}
