package com.businet.GNavApp.ejbs.authentication;

import com.businet.GNavApp.entities.AuthenticationToken;

public interface IAuthentication {
	public AuthenticationToken generateToken(String DeviceId, String loginId);
	
	public boolean verifyToken(String token);
}
