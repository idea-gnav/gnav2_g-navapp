package com.businet.GNavApp.ejbs.returnMachine;

import java.util.List;

import com.businet.GNavApp.entities.OLD_TblReturnMachineArea;

public interface OLD_IReturnMachineService {

	/**
	 * JPA
	 * updateTblReturnMachineHistory: リターンマシーン履歴テーブル更新
	 */
    void updateTblReturnMachineHistory(String userId, Long serialNumber, Integer areaNo, Integer readFlg);

	/**
	 * JPA
	 * updateNoMatchTblFavoriteDtcHistory: 未読データを既読へ更新処理
	 */
    void updateAllReadFlgTblReturnMachineHistory(String userId);

	/**
	 * JPA
	 * findByReturnMachineNoticeNativeQuery: リターンマシン一覧を取得する 「２日以内」
	 */
	List<Object[]> findByReturnMachineNoticeNativeQuery(String userId);

	/**
	 * JPA
	 * findByReturnMachineAlertNativeQuery: リターンマシン通知一覧取得
	 */
	List<Object[]> findByReturnMachineAlertNativeQuery(String userId, Integer kibanSelect, Integer sortFlg,
			String returnDateFrom, String returnDateTo, String customerName);
	/**
	 * JPA
	 * findByUnReadCount: 未読リターンマシンの件数
	 */
    Integer findByUnReadCount(String userId);

	/**
	 * JPA findByReturnMachineAlertNativeQuery: リターンマシンエリア一覧
	 */
	List<Object[]> findByReturnMachineAreaNativeQuery(String userId, Integer sortFlg, String customerName);

	/**
	 * JPA findByReturnMachineAreaDetail: リターンマシンエリア設定情報取得
	 */
    OLD_TblReturnMachineArea findByReturnMachineAreaDetail(String userId, Integer areaNumber);

	/**
	 * JPA insertTblReturnMachineArea: リターンマシンエリア登録
	 */
    Integer insertTblReturnMachineArea(String userId,int areaNumber, String customerName, int searchRangeDistance,
			double searchRangeFromIdo, double searchRangeFromKeido, String areaName, Integer warningCurrentlyInProgressFlg,
			Integer periodicServiceNoticeFlg, Integer periodicServiceNowDueFlg, Integer hourMeterFlg, Double hourMeter);

	/**
	 * JPA updateTblReturnMachineArea: リターンマシンエリア登録
	 */
    void updateTblReturnMachineArea(String userId,int areaNumber, String customerName, int searchRangeDistance,
			double searchRangeFromIdo, double searchRangeFromKeido, String areaName, Integer warningCurrentlyInProgressFlg,
			Integer periodicServiceNoticeFlg, Integer periodicServiceNowDueFlg, Integer hourMeterFlg, Double hourMeter);

	/**
	 * JPA updateDelFlgTblReturnMachineArea: リターンマシンエリア登録
	 */
    void updateDelFlgTblReturnMachineArea(String userId,int areaNumber);

	/**
	 * JPA findMachineTargetByUserID: リターンマシン対象一覧取得
	 */
	List<Object[]> findMachineTargetByUserID(String userId, Integer kibanSelect, Integer sortFlg);

	/**
	 * JPA
	 * addReturnMachine
	 */
    void addReturnMachine(Long addSerialNumber, String userId);

	/**
	 * JPA
	 * removeReturnMachine
	 */
    void removeReturnMachine(Long removeSerialNumber,String userId);

	/**
	 * JPA
	 * getMaxAreaNoByUserID
	 */
    Integer getMaxAreaNoByUserID(String userId);


	/**
	 * JPA
	 * addReturnMachineWork
	 */
	Integer addReturnMachineWork(Long kibanSelect,String userId,Double ido,Double keido);


}
