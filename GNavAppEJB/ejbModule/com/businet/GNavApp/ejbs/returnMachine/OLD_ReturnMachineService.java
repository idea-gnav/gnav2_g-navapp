package com.businet.GNavApp.ejbs.returnMachine;

import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.entities.OLD_TblReturnMachineArea;
import com.businet.GNavApp.entities.TblReturnMachineWork;
import com.businet.GNavApp.entities.OLD_TblUserReturnMachine;
import com.businet.GNavApp.entities.OLD_TblUserReturnMachinePK;

@Stateless
@Local(OLD_IReturnMachineService.class)
public class OLD_ReturnMachineService implements OLD_IReturnMachineService {

	private static final Logger logger = Logger.getLogger(OLD_ReturnMachineService.class.getName());

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
	EntityManager em;

	/**
	 * JPA updateTblReturnMachineHistory: リターンマシーン履歴テーブル更新
	 */
	@Override
	public void updateTblReturnMachineHistory(String userId, Long serialNumber, Integer areaNo, Integer readFlg) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_RETURN_MACHINE_HISTORY ");

		if (readFlg != null)
			sb.append("SET READ_FLG = " + readFlg + ", ");

		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG = 'GNavApp', ");
		sb.append("UPDATE_DTM = SYSDATE ");
        sb.append("WHERE USER_ID = '" + userId + "' ");
		if (areaNo != null)
			sb.append("AND AREA_NO = " + areaNo + " ");
		sb.append("AND KIBAN_SERNO = " + serialNumber);

		int updated = em.createNativeQuery(new String(sb)).executeUpdate();

		if (updated > 0)
			em.flush();
		em.clear();

		return;
	}

	/**
	 * JPA updateNoMatchTblFavoriteDtcHistory: 未読データを既読へ更新処理
	 */
	@Override
    public void updateAllReadFlgTblReturnMachineHistory(String userId) {
		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_RETURN_MACHINE_HISTORY ");
		sb.append("SET READ_FLG = 1 , ");
		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG = 'GNavApp', ");
		sb.append("UPDATE_DTM = SYSDATE ");
        sb.append("WHERE USER_ID = '" + userId + "' ");
		sb.append("AND RECV_TIMESTAMP  < TO_TIMESTAMP('" + ConvertUtil.getDays(-1)
				+ " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");

		int updated = em.createNativeQuery(new String(sb)).executeUpdate();

		if (updated > 0)
			em.flush();
		em.clear();

		return;
	}

	/**
	 * JPA findByReturnMachineNoticeNativeQuery: リターンマシン一覧を取得する 「２日以内」
	 */
	@Override
	public List<Object[]> findByReturnMachineNoticeNativeQuery(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MM.KIBAN_SERNO, "); // 0
		sb.append("MM.KIBAN, "); // 1
		sb.append("MM.USER_KANRI_NO, "); // 2
		sb.append("TRMH.RECV_TIMESTAMP, "); // 3
		sb.append("TRMH.KEIHO_ICON_NO, "); // 4
		sb.append("TRMA.KIGYOU_NAME, "); // 5
		sb.append("TRMH.READ_FLG "); // 6

		sb.append("FROM TBL_RETURN_MACHINE_HISTORY TRMH ");
		sb.append("INNER JOIN TBL_USER_RETURN_MACHINE TURM ");
        sb.append("ON TRMH.USER_ID = TURM.USER_ID ");

		sb.append("AND TRMH.KIBAN_SERNO = TURM.KIBAN_SERNO ");
		sb.append("AND TURM.DEL_FLG = 0 ");

		sb.append("INNER JOIN TBL_RETURN_MACHINE_AREA TRMA ");
        sb.append("ON TRMH.USER_ID = TRMA.USER_ID ");

		sb.append("AND TRMH.AREA_NO = TRMA.AREA_NO ");
		sb.append("AND TRMA.DELETE_FLG = 0 ");

		sb.append("INNER JOIN MST_MACHINE MM ");
		sb.append("ON TRMH.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("AND MM.DELETE_FLAG = 0 ");

		sb.append("WHERE ");
		sb.append("TRMH.RECV_TIMESTAMP  >= TO_TIMESTAMP('" + ConvertUtil.getDays(-1)
				+ " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
        sb.append("AND TRMH.USER_ID = '" + userId + "' ");

		sb.append("ORDER BY TRMH.RECV_TIMESTAMP DESC ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		logger.config(new String(sb));

		if (result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * JPA findByUnReadCount: 未読リターンマシンの件数
	 */
	@Override
    public Integer findByUnReadCount(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT COUNT(*) FROM TBL_RETURN_MACHINE_HISTORY TRMH ");
		sb.append("WHERE TRMH.READ_FLG = 0 ");
        sb.append("AND TRMH.USER_ID = '" + userId + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		Object returnMachineUnReadCount = result.get(0);
		String count = returnMachineUnReadCount.toString();

		if (count != null)
			return Integer.parseInt(count);
		else
			return 0;

	}

	/**
	 * JPA findByReturnMachineAlertNativeQuery: リターンマシン通知一覧取得
	 */
	@Override
	public List<Object[]> findByReturnMachineAlertNativeQuery(String userId, Integer kibanSelect, Integer sortFlg,
			String returnDateFrom, String returnDateTo, String customerName) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TRMH.KEIHO_ICON_NO, "); // 0 --警報アイコンNo
		sb.append("MM.KIBAN_SERNO, "); // 1 --機械管理番号
		sb.append("TRMH.RECV_TIMESTAMP, "); // 2 --リターンマシン発生日
		sb.append("MM.KIBAN, "); // 3 --SCM機番
		sb.append("MM.USER_KANRI_NO, "); // 4 --お客様管理番号
		sb.append("TRMA.KIGYOU_NAME "); // 5 --お客様名称

		sb.append("FROM TBL_RETURN_MACHINE_HISTORY TRMH ");
		sb.append("INNER JOIN TBL_USER_RETURN_MACHINE TURM ");
        sb.append("ON TRMH.USER_ID = TURM.USER_ID ");

		sb.append("AND TRMH.KIBAN_SERNO = TURM.KIBAN_SERNO ");
		sb.append("AND TURM.DEL_FLG = 0 ");

		sb.append("INNER JOIN TBL_RETURN_MACHINE_AREA TRMA ");
        sb.append("ON TRMH.USER_ID = TRMA.USER_ID ");

		sb.append("AND TRMH.AREA_NO = TRMA.AREA_NO ");
		sb.append("AND TRMA.DELETE_FLG = 0 ");

		sb.append("INNER JOIN MST_MACHINE MM ");
		sb.append("ON TRMH.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("AND MM.DELETE_FLAG = 0 ");



		sb.append("WHERE ");
        sb.append("TRMH.USER_ID = '" + userId + "' ");


		if (returnDateFrom != null) {
			sb.append("AND TRMH.RECV_TIMESTAMP  ");
			sb.append(">= TO_TIMESTAMP('" + returnDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}

		if (returnDateTo != null) {
			sb.append("AND TRMH.RECV_TIMESTAMP  ");
			sb.append("<= TO_TIMESTAMP('" + returnDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}

		if (customerName != null && customerName.length() > 0)
			sb.append("AND ( NLS_UPPER(TRMA.KIGYOU_NAME) LIKE NLS_UPPER(REGEXP_REPLACE('"
					+ ConvertUtil.searchFuzzyStr(ConvertUtil.rpStr(customerName)) + "',' ','')) ) ");

		sb.append("AND ROWNUM <= 1001 ");

		// 0 : 機番（お客様管理番号）昇順 || 1: 機番（お客様管理番号）降順 || 2: 発生日時昇順 || 3: 発生日時降順 （デフォルト）|| 4:
		// お客様名昇順 || 5: お客様名降順 || 6:瓊浦イコン昇順 || 7：瓊浦イコン降順

		if (sortFlg == 0 || sortFlg == 1) {
			if (kibanSelect == 0 || kibanSelect == 3)
				sb.append("ORDER BY NLS_UPPER(MM.KIBAN) ");
			if (kibanSelect == 2)
				sb.append("ORDER BY NLS_UPPER(MM.USER_KANRI_NO) ");
		}

		if (sortFlg == 2 || sortFlg == 3)
			sb.append("ORDER BY TRMH.RECV_TIMESTAMP ");

		if (sortFlg == 4 || sortFlg == 5)
			sb.append("ORDER BY TRMA.KIGYOU_NAME ");

		if (sortFlg == 6 || sortFlg == 7)
			sb.append("ORDER BY TRMH.KEIHO_ICON_NO ");

		if (sortFlg == 1 || sortFlg == 3 || sortFlg == 5 || sortFlg == 7)
			sb.append("DESC ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		logger.config(new String(sb));

		if (result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * JPA findByReturnMachineAlertNativeQuery: リターンマシンエリア一覧
	 */
	@Override
	public List<Object[]> findByReturnMachineAreaNativeQuery(String userId, Integer sortFlg, String customerName) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
        sb.append("TRMA.AREA_NO, "); // 0 営業担当拠点コード
        sb.append("TRMA.KIGYOU_NAME, "); // 1 エリアNo
        sb.append("TRMA.AREA_NAME "); // 2 お客様名称
		sb.append("FROM TBL_RETURN_MACHINE_AREA TRMA ");

		sb.append("WHERE ");
		sb.append("TRMA.DELETE_FLG = 0 ");
        sb.append("AND TRMA.USER_ID = '" + userId + "' ");

		if (customerName != null && customerName.length() > 0)
			sb.append("AND ( NLS_UPPER(TRMA.KIGYOU_NAME) LIKE NLS_UPPER(REGEXP_REPLACE('"
					+ ConvertUtil.searchFuzzyStr(ConvertUtil.rpStr(customerName)) + "',' ','')) ) ");
		sb.append("AND ROWNUM <= 1001 ");

		// 0 : お客様名昇順 || 1: お客様名降順 || 2: エリア名昇順 || 3: エリア名降順
		if (sortFlg == 0 || sortFlg == 1)
			sb.append("ORDER BY TRMA.KIGYOU_NAME ");

		if (sortFlg == 2 || sortFlg == 3)
			sb.append("ORDER BY TRMA.AREA_NAME ");

		if (sortFlg == 1 || sortFlg == 3)
			sb.append("DESC ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		logger.config(new String(sb));

		if (result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * JPA findByReturnMachineAlertNativeQuery: リターンマシンエリア設定情報取得
	 */
	@Override
    public OLD_TblReturnMachineArea findByReturnMachineAreaDetail(String userId, Integer areaNumber) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<OLD_TblReturnMachineArea> result = em
                .createQuery("SELECT m FROM TblReturnMachineArea m "
                        + "WHERE m.deleteFlg=0 AND m.userId=:userId AND m.areaNo = :areaNo ")
                .setParameter("userId", userId).setParameter("areaNo", areaNumber).getResultList();

		em.clear();

		if (result.size() > 0)
			return (OLD_TblReturnMachineArea) result.get(0);
		else
			return null;
	}

	/**
	 * JPA updateTblReturnMachineArea: リターンマシンエリア登録
	 */
	@Override
    public void updateTblReturnMachineArea(String userId, int areaNumber, String customerName, int searchRangeDistance,
			double searchRangeFromIdo, double searchRangeFromKeido, String areaName,
			Integer warningCurrentlyInProgressFlg, Integer periodicServiceNoticeFlg, Integer periodicServiceNowDueFlg,
			Integer hourMeterFlg, Double hourMeter) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_RETURN_MACHINE_AREA ");
		sb.append("SET ");
		sb.append("KIGYOU_NAME ='" + customerName + "', ");
		sb.append("AREA_NAME ='" + areaName + "', ");
		sb.append("RADIUS =" + searchRangeDistance + ", ");
		sb.append("IDO =" + searchRangeFromIdo + ", ");
		sb.append("KEIDO =" + searchRangeFromKeido + ", ");
		sb.append("WARNINGCURRENTLYPROGRESS_FLG =" + warningCurrentlyInProgressFlg + ", ");
		sb.append("PERIODICSERVICENOTICE_FLG =" + periodicServiceNoticeFlg + ", ");
		sb.append("PERIODICSERVICENOWDUE_FLG =" + periodicServiceNowDueFlg + ", ");
		sb.append("HOUR_METER_FLG =" + hourMeterFlg + ", ");
		sb.append("HOUR_METER =" + hourMeter + ", ");
		sb.append("UPDATE_DTM = SYSDATE, ");
		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG ='GNavApp-Update' ");
		sb.append("WHERE ");
        sb.append("USER_ID ='" + userId + "' ");
		sb.append("AND AREA_NO =" + areaNumber + " ");
		sb.append("AND DELETE_FLG = 0 ");

		em.getEntityManagerFactory().getCache().evictAll();
		int rs = em.createNativeQuery(new String(sb)).executeUpdate();

		if (rs > 0)
			em.flush();
		em.clear();
		return;
	}

	/**
	 * JPA updateDelFlgTblReturnMachineArea: リターンマシンエリア登録
	 */
	@Override
    public void updateDelFlgTblReturnMachineArea(String userId, int areaNumber) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_RETURN_MACHINE_AREA ");
		sb.append("SET ");
		sb.append("DELETE_FLG = 1, ");
		sb.append("UPDATE_DTM = SYSDATE, ");
		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG ='GNavApp-Delete' ");
		sb.append("WHERE ");
        sb.append("USER_ID ='" + userId + "' ");
		sb.append("AND AREA_NO =" + areaNumber);

		em.getEntityManagerFactory().getCache().evictAll();
		int rs = em.createNativeQuery(new String(sb)).executeUpdate();

		if (rs > 0)
			em.flush();
		em.clear();
		return;
	}

	/**
	 * JPA findMachineTargetByUserID: リターンマシン対象一覧取得
	 */
	@Override
	public List<Object[]> findMachineTargetByUserID(String userId, Integer kibanSelect, Integer sortFlg) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MM.KIBAN_SERNO, "); // 0
		sb.append("MM.KIBAN, "); // 1
		sb.append("MM.LBX_KIBAN, "); // 2
		sb.append("MM.USER_KANRI_NO, "); // 3
		sb.append("CONCAT(MK.KIGYOU_NAME || ' ', MS.SOSIKI_NAME), "); // 4
		sb.append("MM.MODEL_CD, "); // 5
		sb.append("MM.POSITION_EN, "); // 6
		sb.append("MM.POSITION, "); // 7
		sb.append("MM.NEW_IDO, "); // 8
		sb.append("MM.NEW_KEIDO, "); // 9
		sb.append("NVL(MM.NEW_HOUR_METER, 0.0) AS HOUR_METER, "); // 10
		sb.append("MM.NENRYO_LV, "); // 11
		sb.append("MM.UREA_WATER_LEVEL, "); // 12
		sb.append("MM.RECV_TIMESTAMP, "); // 13
		sb.append("MM.CON_TYPE, "); // 14 machineGroup
		sb.append("MM.MACHINE_MODEL, "); // 15 machineGroup
		sb.append("TURM.DEL_FLG, "); // 16

		sb.append("CASE ");
		sb.append("WHEN MM.KEYIKOKU_COUNT > 0 THEN 2 ");
		sb.append("WHEN MM.YOKOKU_COUNT > 0 THEN 1 ");
		sb.append("ELSE 0 ");
		sb.append("END AS PERIODIC_SERVICE_ICON_TYPE "); // 17

		sb.append("FROM TBL_USER_RETURN_MACHINE TURM ");
		sb.append("JOIN MST_MACHINE MM ");
		sb.append("ON ");
		sb.append("( TURM.KIBAN_SERNO = MM.KIBAN_SERNO AND MM.DELETE_FLAG = 0 )	");
		sb.append("LEFT JOIN MST_SOSIKI MS ON ( MM.SOSIKI_CD = MS.SOSIKI_CD ) ");
		sb.append("LEFT JOIN MST_KIGYOU MK ON ( MS.KIGYOU_CD = MK.KIGYOU_CD ) ");

		sb.append("WHERE TURM.DEL_FLG = 0 ");
        sb.append("AND TURM.USER_ID ='" + userId + "' ");


		// **↓↓↓ 2019/10/23 iDEA山下 1000件上限解除 ↓↓↓**//
//		sb.append("AND ROWNUM <= 1001 ");
		// **↑↑↑ 2019/10/23 iDEA山下 1000件上限解除 ↑↑↑**//

		// 0 : 機番（orお客様管理番号）昇順 || 1: 機番（orお客様管理番号）降順 || 2: 最終所在地昇順 || 3: 最終所在地降順 ||
		// 4:アワメータ昇順 || 5:アワメータ降順
		if (sortFlg == 0 || sortFlg == 1) {
			if (kibanSelect == 0 || kibanSelect == 3)
				sb.append("ORDER BY NLS_UPPER(MM.KIBAN) ");
			if (kibanSelect == 2)
				sb.append("ORDER BY NLS_UPPER(MM.USER_KANRI_NO) ");
		}

		if (sortFlg == 2 || sortFlg == 3)
			sb.append("ORDER BY MM.POSITION ");

		if (sortFlg == 4 || sortFlg == 5)
			sb.append("ORDER BY HOUR_METER ");

		if (sortFlg == 1 || sortFlg == 3 || sortFlg == 5)
			sb.append("DESC ");

		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		logger.config(new String(sb));

		if (result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * JPA addReturnMachine
	 */
	@Override
    public void addReturnMachine(Long addSerialNumber, String userId) {
		/*
		 * UPDATE
		 */
		OLD_TblUserReturnMachine tum = new OLD_TblUserReturnMachine();
		OLD_TblUserReturnMachinePK pk = new OLD_TblUserReturnMachinePK();
		pk.setKibanSerno(addSerialNumber);
        pk.setUserId(userId);
		tum = em.find(OLD_TblUserReturnMachine.class, pk);

		if (tum != null) {
			tum.setDelFlg(0);
			tum.setUpdatePrg("GNavApp");
			tum.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			tum.setUpdateUser(userId);
			em.persist(tum);
		} else {
			/*
			 * INSERT
			 */
			tum = new OLD_TblUserReturnMachine();
			tum.setKibanSerno(addSerialNumber);
            tum.setUserId(userId);
			tum.setDelFlg(0);
			tum.setRegistPrg("GNavApp");
			tum.setRegistDtm(new Timestamp(System.currentTimeMillis()));
			tum.setRegistUser(userId);
			tum.setUpdatePrg("GNavApp");
			tum.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			tum.setUpdateUser(userId);
			em.persist(tum);
		}

		em.flush();
		em.clear();

		return;
	}

	/**
	 * JPA removeReturnMachine
	 */
	@Override
    public void removeReturnMachine(Long removeSerialNumber, String userId) {
		/*
		 * EntityTransaction etx = null; try { etx = em.getTransaction(); etx.begin();
		 */
		/*
		 * UPDATE
		 */
		OLD_TblUserReturnMachine tum = new OLD_TblUserReturnMachine();
		OLD_TblUserReturnMachinePK pk = new OLD_TblUserReturnMachinePK();
		pk.setKibanSerno(removeSerialNumber);
        pk.setUserId(userId);
		tum = em.find(OLD_TblUserReturnMachine.class, pk);
		tum.setDelFlg(1);
		tum.setUpdatePrg("GNavApp");
		tum.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
		tum.setUpdateUser("GNavApp");
		em.persist(tum);

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_RETURN_MACHINE_HISTORY ");
		sb.append("SET ");
		sb.append("READ_FLG = 1, ");
		sb.append("UPDATE_DTM = SYSDATE, ");
		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG ='GNavApp-Delete' ");
		sb.append("WHERE ");
        sb.append("USER_ID ='" + userId + "' ");
		sb.append("AND KIBAN_SERNO =" + removeSerialNumber + " ");

		em.getEntityManagerFactory().getCache().evictAll();
		int rs = em.createNativeQuery(new String(sb)).executeUpdate();

		em.flush();
		/*
		 * etx.commit(); } catch (Exception e) { if (etx != null && etx.isActive())
		 * etx.rollback(); e.printStackTrace(); } finally { em.close(); } return;
		 */
	}

	/**
	 * JPA insertTblReturnMachineArea: リターンマシンエリア登録
	 */
	@Override
    public Integer insertTblReturnMachineArea(String userId, int areaNumber, String customerName,
			int searchRangeDistance, double searchRangeFromIdo, double searchRangeFromKeido, String areaName,
			Integer warningCurrentlyInProgressFlg, Integer periodicServiceNoticeFlg, Integer periodicServiceNowDueFlg,
			Integer hourMeterFlg, Double hourMeter) {

		OLD_TblReturnMachineArea rma = new OLD_TblReturnMachineArea();
        rma.setUserId(userId);
		rma.setAreaNo(areaNumber);
		rma.setKigyouName(customerName);
		rma.setAreaName(areaName);
		rma.setRadius(searchRangeDistance);
		rma.setIdo(searchRangeFromIdo);
		rma.setKeido(searchRangeFromKeido);
		rma.setWarningCurrentlyProgressFlg(warningCurrentlyInProgressFlg);
		rma.setPeriodicServiceNoticeFLG(periodicServiceNoticeFlg);
		rma.setPeriodicServiceNowDueFLG(periodicServiceNowDueFlg);
		rma.setHourMeterFlg(hourMeterFlg);
		rma.setHourMeter(hourMeter);
		rma.setDeleteFlg(0);
		rma.setRegistPrg("GNavApp");
		rma.setRegistDtm(new Timestamp(System.currentTimeMillis()));
		rma.setRegistUser(userId);
		rma.setUpdatePrg("GNavApp");
		rma.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
		rma.setUpdateUser(userId);
		em.persist(rma);

		em.flush();
		em.clear();

		return 1;
	}

	/**
	 * JPA getMaxAreaNoByUserID
	 */
	@Override
	public Integer getMaxAreaNoByUserID(String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> result = em
                .createQuery("SELECT  MAX(m.areaNo) FROM TblReturnMachineArea m " + "WHERE m.userId=:userId")
                .setParameter("userId", userId).getResultList();

		em.clear();

		if (result.size() > 0)
			return (Integer) result.get(0);
		else
			return null;

	}


	/**
	 * JPA addReturnMachineWork: リターンマシンワーク追加
	 */
	@Override
    public Integer addReturnMachineWork(Long kibanSelect,String userId,Double ido,Double keido) {

		StringBuilder sb = new StringBuilder();

		TblReturnMachineWork tum = new TblReturnMachineWork();
		tum = em.find(TblReturnMachineWork.class, kibanSelect);

		if (tum == null) {
			// Insert
			tum = new TblReturnMachineWork();
			tum.setKibanSerno(kibanSelect);
			tum.setRecvTimestamp((new Timestamp(System.currentTimeMillis())));
			tum.setIdo(ido);
			tum.setKeido(keido);
			tum.setRegistPrg("GNavApp");
			tum.setRegistDtm(new Timestamp(System.currentTimeMillis()));
			tum.setRegistUser(userId);
			tum.setUpdatePrg("GNavApp");
			tum.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			tum.setUpdateUser(userId);

			em.persist(tum);

			em.flush();
			em.clear();
		}

		logger.config(new String(sb));

		return null;
	}


}
