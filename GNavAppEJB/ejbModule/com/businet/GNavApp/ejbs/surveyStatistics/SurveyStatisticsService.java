package com.businet.GNavApp.ejbs.surveyStatistics;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Local(ISurveyStatisticsService.class)
public class SurveyStatisticsService implements ISurveyStatisticsService {

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
	EntityManager em;



//共有

	/**
	 * NativeQuery findSurveyTargetFigures
	 * findSurveyTargetFigures : アンケート結果グラフのヘッダー情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<Object[]> findSurveyTargetFigures(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd) {
	public List<Object[]> findSurveyTargetFigures(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd
			                   ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg) {

	StringBuilder sb = new StringBuilder();

	/*
		sb.append("SELECT MS.SURVEYU_NAME" + deviceLanguage + ", "); //0
		sb.append("NVL(MS.ANSWER_TARGET_NUM,0), "); //1
		sb.append("NVL(T1.ANSWERCOUNT,0) "); //2
		sb.append("FROM MST_SURVEY MS ");
		sb.append("LEFT OUTER JOIN( ");
		sb.append("SELECT TSR.SURVEY_ID, ");
		sb.append("COUNT(*) AS ANSWERCOUNT ");
		sb.append("FROM TBL_SURVEY_RESULT TSR ");
		sb.append("INNER JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		if(surveyId == 1) {
			//お客様ヒアリングシート用絞り込み条件
			sb.append("INNER JOIN TBL_SURVEY_DETAIL TSD ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			if (year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
			}
			if (katashiki != null) {
				sb.append("AND MM.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
			}
			if (model != null) {
				sb.append("AND MM.MODEL_CD IN (" + model + ") "); //モデル
			}
			if (dealerCd != null) {
				sb.append("AND MM.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
			}
			sb.append("AND TSD.QUESTION_ID = 1 ");
			if (applicableUsesCd != null) {
				sb.append("AND TSD.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") "); //作業内容
			}
		}
		sb.append("GROUP BY TSR.SURVEY_ID ");
		sb.append(")T1 ON MS.SURVEY_ID = T1.SURVEY_ID ");
		sb.append("WHERE MS.SURVEY_ID = " + surveyId + " ");
	*/
		sb.append("SELECT MS.SURVEYU_NAME" + deviceLanguage + ", "); //0
		sb.append("NVL(MS.ANSWER_TARGET_NUM,0), "); //1
		sb.append("NVL(T1.ANSWERCOUNT,0) "); //2
		sb.append("FROM MST_SURVEY MS ");
		sb.append("LEFT OUTER JOIN( ");
		sb.append("SELECT TSR.SURVEY_ID, ");
		sb.append("COUNT(*) AS ANSWERCOUNT ");
		sb.append("FROM TBL_SURVEY_RESULT TSR ");
		sb.append("INNER JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		if(surveyId == 1) {
		//お客様ヒアリングシート用絞り込み条件
		sb.append("INNER JOIN TBL_SURVEY_DETAIL TSD ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		if (year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
		}
		if (katashiki != null) {
			sb.append("AND MM.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		sb.append("AND TSD.QUESTION_ID = 1 ");
		if (applicableUsesCd != null) {
			sb.append("AND TSD.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") "); //作業内容
		}
	}
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}

		sb.append("GROUP BY TSR.SURVEY_ID ");
		sb.append(")T1 ON MS.SURVEY_ID = T1.SURVEY_ID ");
		sb.append("WHERE MS.SURVEY_ID = " + surveyId + " ");



		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> targetFiguresList = em.createNativeQuery(sb.toString()).getResultList();

		return targetFiguresList;
	}

	/**
	 * NativeQuery choiceCountSum
	 * choiceCountSum : 対象設問回答総数取得
	 * questionNo 60,61:自社或いは他社で整備する理由
	 *            56:定期検査・整備
	 *            62:購入時の重要指数
	 *            63:サービスに今後期待すること
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<Integer> choiceCountSum(String year,String questionNo){
	public List<Integer> choiceCountSum(String year,String questionNo,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){
		StringBuilder sb = new StringBuilder();
	/*
		sb.append("SELECT ");
		sb.append("COUNT(*) AS COUNTALL "); //0
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
		sb.append("LEFT JOIN MST_SURVEY_CHOICE MSC ON (TSD.QUESTION_ID = MSC.QUESTION_ID AND TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ) ");
		sb.append("WHERE TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND MSC.DELETE_FLG = 0 ");
		sb.append("AND TSR.SURVEY_ID = 1 ");
		sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
		sb.append("AND TSD.QUESTION_ID IN (" + questionNo + ") ");
	*/
		sb.append("SELECT ");
		sb.append("COUNT(*) AS COUNTALL "); //0
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
		sb.append("LEFT JOIN MST_SURVEY_CHOICE MSC ON (TSD.QUESTION_ID = MSC.QUESTION_ID AND TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ) ");
		sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND MSC.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		sb.append("AND TSR.SURVEY_ID = 1 ");
		sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
		sb.append("AND TSD.QUESTION_ID IN (" + questionNo + ") ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}

		if(year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}







	//36.surveyStatisticsList(アンケート統計一覧)

		/**
		 * NativeQuery findSurveyStatistics
		 * findSurveyStatistics : アンケート統計一覧取得
		 */
		@SuppressWarnings("unchecked")
		@Override
	//	public List<Object[]> findSurveyStatistics(Integer sortFlg, Integer deviceLanguage) {
		public List<Object[]> findSurveyStatistics(Integer sortFlg ,Integer deviceLanguage
				                                     ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg) {
		StringBuilder sb = new StringBuilder();

	/*
			sb.append("SELECT MS.SURVEY_ID, "); //0
			sb.append("MS.SURVEYU_NAME" + deviceLanguage + ", "); //1
			sb.append("MS.INPUT_TO, "); //2
			sb.append("MS.ANSWER_TARGET_NUM, ");//3
			sb.append("CASE ");
			sb.append("WHEN TSR.ANSWERCOUNT IS NULL THEN 0 ");
			sb.append("ELSE TSR.ANSWERCOUNT ");
			sb.append("END AS COUNT, "); //4
			sb.append("CASE ");
			sb.append("WHEN TSR.ANSWERCOUNT/MS.ANSWER_TARGET_NUM*100 IS NULL THEN 0 ");
			sb.append("ELSE FLOOR(TSR.ANSWERCOUNT/MS.ANSWER_TARGET_NUM*100) ");
			sb.append("END AS RATE "); //5
			sb.append("FROM MST_SURVEY MS ");
			sb.append("LEFT OUTER JOIN ( ");
			sb.append("SELECT ");
			sb.append("TSR.SURVEY_ID, ");
			sb.append("COUNT(*) AS ANSWERCOUNT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_RESULT TSR ");
			sb.append("WHERE ");
			sb.append("TSR.SURVEY_ID IN ( SELECT MS1.SURVEY_ID FROM MST_SURVEY MS1 WHERE SURVEY_STATISTICS_FLG = 1 ) ");
			sb.append("AND ");
			sb.append("TSR.DELETE_FLG = 0 ");
			sb.append("GROUP BY ");
			sb.append("TSR.SURVEY_ID ");
			sb.append(") TSR ON MS.SURVEY_ID = TSR.SURVEY_ID ");
			sb.append("WHERE ");
			sb.append("MS.SURVEY_STATISTICS_FLG = 1 ");
			sb.append("AND ");
			sb.append("MS.DELETE_FLG = 0 ");
			sb.append("AND ");
			sb.append("MS.APP_FLG = 1 ");
			sb.append("AND ROWNUM <= 1001 ");
			sb.append("GROUP BY ");
		sb.append("TSR.SURVEY_ID ");
		sb.append(") TSR ON MS.SURVEY_ID = TSR.SURVEY_ID ");
		sb.append("WHERE ");
		sb.append("MS.SURVEY_STATISTICS_FLG = 1 ");
		sb.append("AND ");
		sb.append("MS.DELETE_FLG = 0 ");
		sb.append("AND ");
		sb.append("MS.APP_FLG = 1 ");
		sb.append("AND ROWNUM <= 1001 ");

			sb.append("ORDER BY ");
			if (sortFlg == 0) {
				sb.append("MS.SURVEYU_NAME" + deviceLanguage + ",MS.SURVEY_ID ");
			} else if (sortFlg == 1) {
				sb.append("MS.SURVEYU_NAME" + deviceLanguage + " DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 2) {
				sb.append("MS.INPUT_TO,MS.SURVEY_ID ");
			} else if (sortFlg == 3) {
				sb.append("MS.INPUT_TO DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 4) {
				sb.append("MS.ANSWER_TARGET_NUM,MS.SURVEY_ID ");
			} else if (sortFlg == 5) {
				sb.append("MS.ANSWER_TARGET_NUM DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 6) {
				sb.append("COUNT,MS.SURVEY_ID ");
			} else if (sortFlg == 7) {
				sb.append("COUNT DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 8) {
				sb.append("RATE,MS.SURVEY_ID ");
			} else if (sortFlg == 9) {
				sb.append("RATE DESC,MS.SURVEY_ID DESC ");
			}

	*/
		sb.append("SELECT MS.SURVEY_ID, "); //0
		sb.append("MS.SURVEYU_NAME" + deviceLanguage + ", "); //1
		sb.append("MS.INPUT_TO, "); //2
		sb.append("MS.ANSWER_TARGET_NUM, ");//3
		sb.append("CASE ");
		sb.append("WHEN TSR.ANSWERCOUNT IS NULL THEN 0 ");
		sb.append("ELSE TSR.ANSWERCOUNT ");
		sb.append("END AS COUNT, "); //4
		sb.append("CASE ");
		sb.append("WHEN TSR.ANSWERCOUNT/MS.ANSWER_TARGET_NUM*100 IS NULL THEN 0 ");
		sb.append("ELSE FLOOR(TSR.ANSWERCOUNT/MS.ANSWER_TARGET_NUM*100) ");
		sb.append("END AS RATE "); //5
		sb.append("FROM MST_SURVEY MS ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("TSR.SURVEY_ID, ");
		//2020.01.15 DucNKT modified for 実施台数
		//sb.append("COUNT(*) AS ANSWERCOUNT ");
		sb.append("COUNT(DISTINCT TSR.KIBAN_SERNO) AS ANSWERCOUNT ");
		
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("JOIN MST_MACHINE MM  ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE ");
		sb.append("TSR.SURVEY_ID IN ( SELECT MS1.SURVEY_ID FROM MST_SURVEY MS1 WHERE SURVEY_STATISTICS_FLG = 1 ) ");
		sb.append("AND ");
		sb.append("TSR.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		// ユーザー権限 機械公開制御
				if(typeFlg == 0 ) {
					// 0:SCM
					sb.append("");

				}else if(typeFlg == 1) {
					// 1:地区
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
					sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
					sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

				}else if(typeFlg == 2) {
					// 2:代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
					sb.append(") ");

				}else if(typeFlg == 3) {
					// 3:一般、レンタル
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 4) {
					// 4:サービス工場
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 21) {
					// 21:SCMSEA
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
					sb.append(") ");

				}else if(typeFlg == 31) {
					// 31:トルコ代理店
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
					sb.append(") ");

				}else if(typeFlg == 35) {
					// 35:トルコサブ代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
					sb.append(") ");

				}
				else {
					sb.append("AND ROWNUM <= 0 ");
				}

		sb.append("GROUP BY ");
		sb.append("TSR.SURVEY_ID ");
		sb.append(") TSR ON MS.SURVEY_ID = TSR.SURVEY_ID ");
		sb.append("WHERE ");
		sb.append("MS.SURVEY_STATISTICS_FLG = 1 ");
		sb.append("AND ");
		sb.append("MS.DELETE_FLG = 0 ");
		sb.append("AND ");
		sb.append("MS.APP_FLG = 1 ");
		sb.append("AND ROWNUM <= 1001 ");

			sb.append("ORDER BY ");
			if (sortFlg == 0) {
				sb.append("MS.SURVEYU_NAME" + deviceLanguage + ",MS.SURVEY_ID ");
			} else if (sortFlg == 1) {
				sb.append("MS.SURVEYU_NAME" + deviceLanguage + " DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 2) {
				sb.append("MS.INPUT_TO,MS.SURVEY_ID ");
			} else if (sortFlg == 3) {
				sb.append("MS.INPUT_TO DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 4) {
				sb.append("MS.ANSWER_TARGET_NUM,MS.SURVEY_ID ");
			} else if (sortFlg == 5) {
				sb.append("MS.ANSWER_TARGET_NUM DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 6) {
				sb.append("COUNT,MS.SURVEY_ID ");
			} else if (sortFlg == 7) {
				sb.append("COUNT DESC,MS.SURVEY_ID DESC ");
			} else if (sortFlg == 8) {
				sb.append("RATE,MS.SURVEY_ID ");
			} else if (sortFlg == 9) {
				sb.append("RATE DESC,MS.SURVEY_ID DESC ");
			}

			em.getEntityManagerFactory().getCache().evictAll();
			List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

			if (rs.size() > 0)
				return rs;
			else
				return null;
		}







//37.serviceCoverHrChart(サービスカバーHR分布図)

	/**
	 * NativeQuery findServiceCoverHrChart
	 * findServiceCoverHrChart : サービスカバーHR分布図取得
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<Object[]> findServiceCoverHrChart(Integer surveyId, String year) {
	public List<Object[]> findServiceCoverHrChart(Integer surveyId, String year,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg) {
		StringBuilder sb = new StringBuilder();

	/*
		sb.append("SELECT ");
		sb.append("DUMMY.HR, "); //0
		sb.append("NVL(SMY.COUNT,0) "); //1
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("LEVEL - 1 AS CLID, ");
		sb.append("( LEVEL - 1 ) * 500 AS HR ");
		sb.append("FROM ");
		sb.append("DUAL ");
		sb.append("CONNECT BY ");
		sb.append("LEVEL <= ( ");
		sb.append("SELECT ");
		sb.append("MAX(NVL(FLOOR(TSR.HOUR_METER / 60 / 500),0) ) + 1 ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		if (year != null) {
		sb.append("AND to_char(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
		}
		sb.append(") ");
		sb.append(") DUMMY ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("NVL(FLOOR(TSR.HOUR_METER / 60 / 500),0) AS RENGE, ");
		sb.append("COUNT(*) AS COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		if (year != null) {
		sb.append("AND to_char(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
		}
		sb.append("GROUP BY ");
		sb.append("NVL(FLOOR(TSR.HOUR_METER / 60 / 500),0) ");
		sb.append(") SMY ON DUMMY.CLID = SMY.RENGE ");
		sb.append("ORDER BY ");
		sb.append("DUMMY.CLID ");
	*/

		sb.append("SELECT ");
		sb.append("DUMMY.HR, "); //0
		sb.append("NVL(SMY.COUNT,0) "); //1
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("LEVEL - 1 AS CLID, ");
		sb.append("( LEVEL - 1 ) * 500 AS HR ");
		sb.append("FROM ");
		sb.append("DUAL ");
		sb.append("CONNECT BY ");
		sb.append("LEVEL <= ( ");
		sb.append("SELECT ");
		sb.append("MAX(NVL(FLOOR(TSR.HOUR_METER / 60 / 500),0) ) + 1 ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}
		if (year != null) {
		sb.append("AND to_char(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
		}
		sb.append(") ");
		sb.append(") DUMMY ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("NVL(FLOOR(TSR.HOUR_METER / 60 / 500),0) AS RENGE, ");
		sb.append("COUNT(*) AS COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		// ユーザー権限 機械公開制御
				if(typeFlg == 0 ) {
					// 0:SCM
					sb.append("");

				}else if(typeFlg == 1) {
					// 1:地区
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
					sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
					sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

				}else if(typeFlg == 2) {
					// 2:代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
					sb.append(") ");

				}else if(typeFlg == 3) {
					// 3:一般、レンタル
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 4) {
					// 4:サービス工場
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 21) {
					// 21:SCMSEA
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
					sb.append(") ");

				}else if(typeFlg == 31) {
					// 31:トルコ代理店
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
					sb.append(") ");

				}else if(typeFlg == 35) {
					// 35:トルコサブ代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
					sb.append(") ");

				}
				else {
					sb.append("AND ROWNUM <= 0 ");
				}

		if (year != null) {
		sb.append("AND to_char(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
		}
		sb.append("GROUP BY ");
		sb.append("NVL(FLOOR(TSR.HOUR_METER / 60 / 500),0) ");
		sb.append(") SMY ON DUMMY.CLID = SMY.RENGE ");
		sb.append("ORDER BY ");
		sb.append("DUMMY.CLID ");


		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}







//38.serviceBestBland(サービスベストブランド)

	/**
	 * NativeQuery findServiceBestBland
	 * findServiceBestBland : サービスベストブランド取得
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<Object[]> findServiceBestBland(Integer surveyId, Integer deviceLanguage, String year) {
	public List<Object[]> findServiceBestBland(Integer surveyId, Integer deviceLanguage, String year
			,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg) {
		StringBuilder sb = new StringBuilder();

/*
		sb.append("SELECT ");
		sb.append("NVL(SMY.FREE_INPUT_FLG,0) AS CLRFLG, "); //0
		sb.append("MSC.CHOICE_ID, "); //1
		sb.append("MSC.CHOICE" + deviceLanguage + " AS CHOICE" + deviceLanguage + ", "); //2
		sb.append("NVL(SMY.COUNT,0) AS COUNT "); //3
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE" + deviceLanguage + " ");
		sb.append("FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("WHERE ");
		sb.append("MSC.QUESTION_ID = 34 ");
		sb.append("AND ");
		sb.append("MSC.CHOICE_ID = 1 ");
		sb.append("UNION ALL ");
		sb.append("SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE" + deviceLanguage + " ");
		sb.append("FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("WHERE MSC.QUESTION_ID = 33 "); //住友以外のベストブランド設問
		sb.append("AND MSC.CHOICE_ID <> 1 "); //比較対象無除外
		sb.append(") MSC ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN TSD.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN TSDQ33.ANSWER_CHOICE_ID ");
		sb.append("END AS ANSWER_CHOICE_ID, ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN 0 ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN MSQ.FREE_INPUT_FLG ");
		sb.append("END AS FREE_INPUT_FLG, ");
		sb.append("COUNT(*) AS COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN TBL_SURVEY_DETAIL TSDQ33 ON TSD.SURVEY_RESULT_ID = TSDQ33.SURVEY_RESULT_ID ");
		sb.append("LEFT OUTER JOIN MST_SURVEY_QUESTION MSQ ON TSDQ33.QUESTION_ID = MSQ.QUESTION_ID AND TSDQ33.ANSWER_CHOICE_ID = MSQ.FREE_INPUT_CHOICE_ID ");
		sb.append("WHERE TSD.QUESTION_ID IN (34,35,36,37,38) "); //住友か競合の設問
		sb.append("AND TSD.ANSWER_CHOICE_ID IN (1,2) ");
		sb.append("AND TSDQ33.QUESTION_ID = 33 "); //住友以外のベストブランド設問
		sb.append("AND TSDQ33.ANSWER_CHOICE_ID <> 1 "); //比較対象無除外
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		if(year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
		}
		sb.append("GROUP BY ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN TSD.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN TSDQ33.ANSWER_CHOICE_ID ");
		sb.append("END, ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN 0 ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN MSQ.FREE_INPUT_FLG ");
		sb.append("END ");
		sb.append(") SMY ON MSC.CHOICE_ID = SMY.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("MSC.CHOICE_ID ");
*/
		sb.append("SELECT ");
		sb.append("NVL(SMY.FREE_INPUT_FLG,0) AS CLRFLG, "); //0
		sb.append("MSC.CHOICE_ID, "); //1
		sb.append("MSC.CHOICE" + deviceLanguage + " AS CHOICE" + deviceLanguage + ", "); //2
		sb.append("NVL(SMY.COUNT,0) AS COUNT "); //3
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE" + deviceLanguage + " ");
		sb.append("FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("WHERE ");
		sb.append("MSC.QUESTION_ID = 34 ");
		sb.append("AND ");
		sb.append("MSC.CHOICE_ID = 1 ");
		sb.append("UNION ALL ");
		sb.append("SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE" + deviceLanguage + " ");
		sb.append("FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("WHERE MSC.QUESTION_ID = 33 "); //住友以外のベストブランド設問
		sb.append("AND MSC.CHOICE_ID <> 1 "); //比較対象無除外
		sb.append(") MSC ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN TSD.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN TSDQ33.ANSWER_CHOICE_ID ");
		sb.append("END AS ANSWER_CHOICE_ID, ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN 0 ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN MSQ.FREE_INPUT_FLG ");
		sb.append("END AS FREE_INPUT_FLG, ");
		sb.append("COUNT(*) AS COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN TBL_SURVEY_DETAIL TSDQ33 ON TSD.SURVEY_RESULT_ID = TSDQ33.SURVEY_RESULT_ID ");
		sb.append("LEFT OUTER JOIN MST_SURVEY_QUESTION MSQ ON TSDQ33.QUESTION_ID = MSQ.QUESTION_ID AND TSDQ33.ANSWER_CHOICE_ID = MSQ.FREE_INPUT_CHOICE_ID ");
		sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE TSD.QUESTION_ID IN (34,35,36,37,38) "); //住友か競合の設問
		sb.append("AND TSD.ANSWER_CHOICE_ID IN (1,2) ");
		sb.append("AND TSDQ33.QUESTION_ID = 33 "); //住友以外のベストブランド設問
		sb.append("AND TSDQ33.ANSWER_CHOICE_ID <> 1 "); //比較対象無除外
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}

		if(year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY') IN (" + year + ") ");
		}
		sb.append("GROUP BY ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN TSD.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN TSDQ33.ANSWER_CHOICE_ID ");
		sb.append("END, ");
		sb.append("CASE ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 1 THEN 0 ");
		sb.append("WHEN TSD.ANSWER_CHOICE_ID = 2 THEN MSQ.FREE_INPUT_FLG ");
		sb.append("END ");
		sb.append(") SMY ON MSC.CHOICE_ID = SMY.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("MSC.CHOICE_ID ");


		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}







//39.evaluationItem(評価項目)

	/**
	 * NativeQuery findCompetitorList
	 * findCompetitorList : 評価項目　競合他社取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findCompetitorList(Integer deviceLanguage) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MSC.CHOICE_ID, "); //0
		sb.append("MSC.CHOICE" + deviceLanguage + " "); //1
		sb.append("FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("WHERE MSC.QUESTION_ID = 3 ");
		sb.append("ORDER BY MSC.CHOICE_ID ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}

	/**
	 * NativeQuery findEvaluationLvText
	 * findEvaluationLvText : 評価レベルテキスト取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findEvaluationLvText(Integer deviceLanguage) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MSQ.QUESTION" + deviceLanguage + " "); //0
		sb.append("FROM MST_SURVEY_QUESTION MSQ ");
		sb.append("WHERE MSQ.QUESTION_ID = 6 ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs.get(0);
		else
			return null;
	}

	/**
	 * NativeQuery findSumitomoCount
	 * findSumitomoCount : 住友データ件数取得
	 */
	@SuppressWarnings("unchecked")
	@Override
//  public List<Integer> findSumitomoCount(Integer surveyId ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd){
	public List<Integer> findSumitomoCount(Integer surveyId ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd
			,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("COUNT(*) AS COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("INNER JOIN MST_SURVEY_CHOICE MSC ON TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ");
		sb.append("AND TSD.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN MST_SURVEY_QUESTION MSQ ON MSQ.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}

		sb.append("AND ");
		sb.append("EXISTS ( ");
		sb.append("SELECT ");
		sb.append("* ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD1 ");
		sb.append("WHERE ");
		sb.append("TSD.SURVEY_RESULT_ID = TSD1.SURVEY_RESULT_ID ");
		sb.append("AND TSD1.QUESTION_ID = 2 ");
		sb.append("AND TSD1.ANSWER_CHOICE_ID = 2 ");
		sb.append(") "); //住友データ
		if (year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") "); //年度
		}
		if (katashiki != null) {
			sb.append("AND MM.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		if (applicableUsesCd != null) {
			sb.append("AND ");
			sb.append("EXISTS ( ");
			sb.append("SELECT ");
			sb.append("* ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD2 ");
			sb.append("WHERE ");
			sb.append("TSD.SURVEY_RESULT_ID = TSD2.SURVEY_RESULT_ID ");
			sb.append("AND TSD2.QUESTION_ID = 1 ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") ");
			sb.append(") "); //作業内容
		}

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}



	/**
	 * NativeQuery findChartList
	 * findChartList : レーダーチャート出力用データ取得
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<Object[]> findChartList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,Integer sumitomoCount) {
	public List<Object[]> findChartList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,Integer sumitomoCount
															,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg) {
		StringBuilder sb = new StringBuilder();

		if(sumitomoCount == 0) {
			//住友ダミーデータ作成
			sb.append("SELECT 0 AS CMPY,ROWNUM,QUESTION"+ deviceLanguage +",0 AS AVG FROM MST_SURVEY_QUESTION ");
			sb.append("WHERE QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
			sb.append("UNION ALL ");
		}
	/*
		sb.append("SELECT ");
		sb.append("SMY.CMPY, "); //0
		sb.append("ROW_NUMBER() OVER(PARTITION BY CMPY ORDER BY CMPY), "); //1
		sb.append("MSQ.QUESTION" + deviceLanguage + " AS CLNM, "); //2;
		sb.append("SMY.AVG "); //3
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("BASE.CMPY, ");
		sb.append("BASE.QUESTION_ID, ");
		sb.append("SUM(BASE.TOTAL) / SUM(BASE.COUNT) AS AVG ");
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("CASE ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 1 THEN TSDQ3.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 2 THEN 0 ");
		sb.append("END ");
		sb.append("AS CMPY, ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("COUNT(*) AS COUNT, ");
		sb.append("MSC.CHOICE0 * COUNT(*) AS TOTAL ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("INNER JOIN MST_SURVEY_CHOICE MSC ON TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ");
		sb.append("AND TSD.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("INNER JOIN TBL_SURVEY_DETAIL TSDQ2 ON TSD.SURVEY_RESULT_ID = TSDQ2.SURVEY_RESULT_ID "); //競合OR住友検索用
		sb.append("INNER JOIN TBL_SURVEY_DETAIL TSDQ3 ON TSD.SURVEY_RESULT_ID = TSDQ3.SURVEY_RESULT_ID "); //競合他社検索用
		sb.append("WHERE ");
		sb.append("TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND TSDQ2.QUESTION_ID = 2 "); // 競合OR住友
		sb.append("AND TSDQ3.QUESTION_ID = 3 "); // 競合他社
		sb.append("AND TSD.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		if (year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") "); //年度
		}
		if (katashiki != null) {
			sb.append("AND MM.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		if (applicableUsesCd != null) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT ");
			sb.append("* ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD2 ");
			sb.append("WHERE ");
			sb.append("TSD.SURVEY_RESULT_ID = TSD2.SURVEY_RESULT_ID ");
			sb.append("AND TSD2.QUESTION_ID = 1 ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") ");
			sb.append(")  "); //作業内容
		}
		sb.append("GROUP BY ");
		sb.append("CASE ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 1 THEN TSDQ3.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 2 THEN 0 ");
		sb.append("END, ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("MSC.CHOICE0 ");
		sb.append(") BASE ");
		sb.append("GROUP BY ");
		sb.append("BASE.CMPY, ");
		sb.append("BASE.QUESTION_ID ");
		sb.append("ORDER BY ");
		sb.append("BASE.CMPY, ");
		sb.append("BASE.QUESTION_ID ");
		sb.append(") SMY ");
		sb.append("INNER JOIN MST_SURVEY_QUESTION MSQ ON MSQ.QUESTION_ID = SMY.QUESTION_ID ");
		sb.append("WHERE ");
		sb.append("MSQ.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
	*/
		sb.append("SELECT ");
		sb.append("SMY.CMPY, "); //0
		sb.append("ROW_NUMBER() OVER(PARTITION BY CMPY ORDER BY CMPY), "); //1
		sb.append("MSQ.QUESTION" + deviceLanguage + " AS CLNM, "); //2;
		sb.append("SMY.AVG "); //3
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("BASE.CMPY, ");
		sb.append("BASE.QUESTION_ID, ");
		sb.append("SUM(BASE.TOTAL) / SUM(BASE.COUNT) AS AVG ");
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("CASE ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 1 THEN TSDQ3.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 2 THEN 0 ");
		sb.append("END ");
		sb.append("AS CMPY, ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("COUNT(*) AS COUNT, ");
		sb.append("MSC.CHOICE0 * COUNT(*) AS TOTAL ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("INNER JOIN MST_SURVEY_CHOICE MSC ON TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ");
		sb.append("AND TSD.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("INNER JOIN TBL_SURVEY_DETAIL TSDQ2 ON TSD.SURVEY_RESULT_ID = TSDQ2.SURVEY_RESULT_ID "); //競合OR住友検索用
		sb.append("INNER JOIN TBL_SURVEY_DETAIL TSDQ3 ON TSD.SURVEY_RESULT_ID = TSDQ3.SURVEY_RESULT_ID "); //競合他社検索用
		sb.append("WHERE ");
		sb.append("TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		sb.append("AND TSDQ2.QUESTION_ID = 2 "); // 競合OR住友
		sb.append("AND TSDQ3.QUESTION_ID = 3 "); // 競合他社
		sb.append("AND TSD.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}



		if (year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") "); //年度
		}
		if (katashiki != null) {
			sb.append("AND MM.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		if (applicableUsesCd != null) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT ");
			sb.append("* ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD2 ");
			sb.append("WHERE ");
			sb.append("TSD.SURVEY_RESULT_ID = TSD2.SURVEY_RESULT_ID ");
			sb.append("AND TSD2.QUESTION_ID = 1 ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") ");
			sb.append(")  "); //作業内容
		}
		sb.append("GROUP BY ");
		sb.append("CASE ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 1 THEN TSDQ3.ANSWER_CHOICE_ID ");
		sb.append("WHEN TSDQ2.ANSWER_CHOICE_ID = 2 THEN 0 ");
		sb.append("END, ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("MSC.CHOICE0 ");
		sb.append(") BASE ");
		sb.append("GROUP BY ");
		sb.append("BASE.CMPY, ");
		sb.append("BASE.QUESTION_ID ");
		sb.append("ORDER BY ");
		sb.append("BASE.CMPY, ");
		sb.append("BASE.QUESTION_ID ");
		sb.append(") SMY ");
		sb.append("INNER JOIN MST_SURVEY_QUESTION MSQ ON MSQ.QUESTION_ID = SMY.QUESTION_ID ");
		sb.append("WHERE ");
		sb.append("MSQ.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}

	/**
	 * NativeQuery findChoiceList
	 * findChoiceList : レーダーチャート出力用データ取得
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<Object[]> findChoiceList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,String strs[]):
	public List<Object[]> findChoiceList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,String strs[]
		                                 	,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg) {
		StringBuilder sb = new StringBuilder();
 /*
		sb.append("SELECT ");
		sb.append("CASE ");
		sb.append("WHEN MSC.CHOICE0 = '5' THEN MSC.CHOICE0 || ' (" + strs[0] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '4' THEN MSC.CHOICE0 || ' (" + strs[1] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '3' THEN MSC.CHOICE0 || ' (" + strs[2] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '2' THEN MSC.CHOICE0 || ' (" + strs[3] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '1' THEN MSC.CHOICE0 || ' (" + strs[4] + ")' ");
		sb.append("END ");
		sb.append("AS CHOICE0, ");
		sb.append("CASE ");
		sb.append("WHEN SMY.COUNT IS NULL THEN 0 ");
		sb.append("ELSE SMY.COUNT ");
		sb.append("END ");
		sb.append("AS COUNT, ");
		sb.append("CASE ");
		sb.append("WHEN FLOOR(SMY.COUNT / ALLCOUNT.ALLCOUNT * 100) IS NULL THEN 0 ");
		sb.append("ELSE FLOOR(SMY.COUNT / ALLCOUNT.ALLCOUNT * 100) ");
		sb.append("END ");
		sb.append("AS PERSENTAGE ");
		sb.append("FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("TSD.ANSWER_CHOICE_ID, ");
		sb.append("COUNT(*) AS COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("INNER JOIN MST_SURVEY_CHOICE MSC ON TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ");
		sb.append("AND TSD.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN MST_SURVEY_QUESTION MSQ ON MSQ.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		sb.append("AND ");
		sb.append("EXISTS ( ");
		sb.append("SELECT ");
		sb.append("* ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD1 ");
		sb.append("WHERE ");
		sb.append("TSD.SURVEY_RESULT_ID = TSD1.SURVEY_RESULT_ID ");
		sb.append("AND TSD1.QUESTION_ID = 2 ");
		sb.append("AND TSD1.ANSWER_CHOICE_ID = 2 ");
		sb.append(") "); //住友データ
		if (year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") "); //年度
		}
		if (katashiki != null) {
			sb.append("AND MM.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		if (applicableUsesCd != null) {
			sb.append("AND ");
			sb.append("EXISTS ( ");
			sb.append("SELECT ");
			sb.append("* ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD2 ");
			sb.append("WHERE ");
			sb.append("TSD.SURVEY_RESULT_ID = TSD2.SURVEY_RESULT_ID ");
			sb.append("AND TSD2.QUESTION_ID = 1 ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") ");
			sb.append(") "); //作業内容
		}
		sb.append("GROUP BY ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append(") SMY ON MSC.CHOICE_ID = SMY.ANSWER_CHOICE_ID ");
		sb.append("AND MSC.QUESTION_ID = SMY.QUESTION_ID ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("TSD3.QUESTION_ID, ");
		sb.append("COUNT(*) AS ALLCOUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD3 ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR2 ON TSD3.SURVEY_RESULT_ID = TSR2.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN MST_MACHINE MM2 ON TSR2.KIBAN_SERNO = MM2.KIBAN_SERNO ");
		sb.append("WHERE TSR2.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSD3.DELETE_FLG = 0 ");
		sb.append("AND TSR2.DELETE_FLG = 0 ");
		sb.append("AND TSD3.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		sb.append("AND ");
		sb.append("EXISTS ( ");
		sb.append("SELECT ");
		sb.append("* ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD4 ");
		sb.append("WHERE ");
		sb.append("TSD3.SURVEY_RESULT_ID = TSD4.SURVEY_RESULT_ID ");
		sb.append("AND TSD4.QUESTION_ID = 2 ");
		sb.append("AND TSD4.ANSWER_CHOICE_ID = 2 ");
		sb.append(")  "); //住友データ
		if (year != null) {
			sb.append("AND TO_CHAR(TSR2.SURVEY_DATE,'YYYY') IN (" + year + ") "); //年度
		}
		if (katashiki != null) {
			sb.append("AND MM2.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM2.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM2.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		if (applicableUsesCd != null) {
			sb.append("AND ");
			sb.append("EXISTS ( ");
			sb.append("SELECT ");
			sb.append("* ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD5 ");
			sb.append("WHERE ");
			sb.append("TSD3.SURVEY_RESULT_ID = TSD5.SURVEY_RESULT_ID ");
			sb.append("AND TSD5.QUESTION_ID = 1 ");
			sb.append("AND TSD5.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") ");
			sb.append(") ");
		}
		sb.append("GROUP BY ");
		sb.append("TSD3.QUESTION_ID ");
		sb.append(") ALLCOUNT ON ALLCOUNT.QUESTION_ID = SMY.QUESTION_ID ");
		sb.append("WHERE ");
		sb.append("MSC.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		sb.append("ORDER BY ");
		sb.append("MSC.QUESTION_ID, ");
		sb.append("MSC.CHOICE_ID ");
*/
		sb.append("SELECT ");
		sb.append("CASE ");
		sb.append("WHEN MSC.CHOICE0 = '5' THEN MSC.CHOICE0 || ' (" + strs[0] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '4' THEN MSC.CHOICE0 || ' (" + strs[1] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '3' THEN MSC.CHOICE0 || ' (" + strs[2] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '2' THEN MSC.CHOICE0 || ' (" + strs[3] + ")' ");
		sb.append("WHEN MSC.CHOICE0 = '1' THEN MSC.CHOICE0 || ' (" + strs[4] + ")' ");
		sb.append("END ");
		sb.append("AS CHOICE0, ");
		sb.append("CASE ");
		sb.append("WHEN SMY.COUNT IS NULL THEN 0 ");
		sb.append("ELSE SMY.COUNT ");
		sb.append("END ");
		sb.append("AS COUNT, ");
		sb.append("CASE ");
		sb.append("WHEN FLOOR(SMY.COUNT / ALLCOUNT.ALLCOUNT * 100) IS NULL THEN 0 ");
		sb.append("ELSE FLOOR(SMY.COUNT / ALLCOUNT.ALLCOUNT * 100) ");
		sb.append("END ");
		sb.append("AS PERSENTAGE ");
		sb.append("FROM ");
		sb.append("MST_SURVEY_CHOICE MSC ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("TSD.ANSWER_CHOICE_ID, ");
		sb.append("COUNT(*) AS COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("INNER JOIN MST_SURVEY_CHOICE MSC ON TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ");
		sb.append("AND TSD.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN MST_SURVEY_QUESTION MSQ ON MSQ.QUESTION_ID = MSC.QUESTION_ID ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE TSR.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		sb.append("AND TSD.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		// ユーザー権限 機械公開制御
				if(typeFlg == 0 ) {
					// 0:SCM
					sb.append("");

				}else if(typeFlg == 1) {
					// 1:地区
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
					sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
					sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

				}else if(typeFlg == 2) {
					// 2:代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
					sb.append(") ");

				}else if(typeFlg == 3) {
					// 3:一般、レンタル
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 4) {
					// 4:サービス工場
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 21) {
					// 21:SCMSEA
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
					sb.append(") ");

				}else if(typeFlg == 31) {
					// 31:トルコ代理店
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
					sb.append(") ");

				}else if(typeFlg == 35) {
					// 35:トルコサブ代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
					sb.append(") ");

				}
				else {
					sb.append("AND ROWNUM <= 0 ");
				}

		sb.append("AND ");
		sb.append("EXISTS ( ");
		sb.append("SELECT ");
		sb.append("* ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD1 ");
		sb.append("WHERE ");
		sb.append("TSD.SURVEY_RESULT_ID = TSD1.SURVEY_RESULT_ID ");
		sb.append("AND TSD1.QUESTION_ID = 2 ");
		sb.append("AND TSD1.ANSWER_CHOICE_ID = 2 ");
		sb.append(") "); //住友データ
		if (year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") "); //年度
		}
		if (katashiki != null) {
			sb.append("AND MM.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		if (applicableUsesCd != null) {
			sb.append("AND ");
			sb.append("EXISTS ( ");
			sb.append("SELECT ");
			sb.append("* ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD2 ");
			sb.append("WHERE ");
			sb.append("TSD.SURVEY_RESULT_ID = TSD2.SURVEY_RESULT_ID ");
			sb.append("AND TSD2.QUESTION_ID = 1 ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") ");
			sb.append(") "); //作業内容
		}
		sb.append("GROUP BY ");
		sb.append("TSD.QUESTION_ID, ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append(") SMY ON MSC.CHOICE_ID = SMY.ANSWER_CHOICE_ID ");
		sb.append("AND MSC.QUESTION_ID = SMY.QUESTION_ID ");
		sb.append("LEFT OUTER JOIN ( ");
		sb.append("SELECT ");
		sb.append("TSD3.QUESTION_ID, ");
		sb.append("COUNT(*) AS ALLCOUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD3 ");
		sb.append("INNER JOIN TBL_SURVEY_RESULT TSR2 ON TSD3.SURVEY_RESULT_ID = TSR2.SURVEY_RESULT_ID ");
		sb.append("INNER JOIN MST_MACHINE MM2 ON TSR2.KIBAN_SERNO = MM2.KIBAN_SERNO ");
		sb.append("WHERE TSR2.SURVEY_ID = " + surveyId + " ");
		sb.append("AND TSD3.DELETE_FLG = 0 ");
		sb.append("AND TSR2.DELETE_FLG = 0 ");
		sb.append("AND MM2.DELETE_FLAG = 0 ");
		// ユーザー権限 機械公開制御
				if(typeFlg == 0 ) {
					// 0:SCM
					sb.append("");

				}else if(typeFlg == 1) {
					// 1:地区
					sb.append("AND ( ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
					sb.append("OR ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
					sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
					sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
					sb.append("OR ");
					sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
					sb.append("OR ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

				}else if(typeFlg == 2) {
					// 2:代理店
					sb.append("AND ( ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM2.DAIRITENN_CD  = '"+sosikiCd+"' ");
					sb.append(") ");

				}else if(typeFlg == 3) {
					// 3:一般、レンタル
					sb.append("AND ( ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 4) {
					// 4:サービス工場
					sb.append("AND ( ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM2.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 21) {
					// 21:SCMSEA
					sb.append("AND ( ");
					sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
					sb.append(") ");

				}else if(typeFlg == 31) {
					// 31:トルコ代理店
					sb.append("AND ( ");
					sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
					sb.append(") ");

				}else if(typeFlg == 35) {
					// 35:トルコサブ代理店
					sb.append("AND ( ");
					sb.append("MM2.SOSIKI_TR = '"+sosikiCd+"' ");
					sb.append("OR ");
					sb.append("MM2.SOSIKI_CD = '"+sosikiCd+"' ");
					sb.append(") ");

				}
				else {
					sb.append("AND ROWNUM <= 0 ");
				}
		sb.append("AND TSD3.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		sb.append("AND ");
		sb.append("EXISTS ( ");
		sb.append("SELECT ");
		sb.append("* ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_DETAIL TSD4 ");
		sb.append("WHERE ");
		sb.append("TSD3.SURVEY_RESULT_ID = TSD4.SURVEY_RESULT_ID ");
		sb.append("AND TSD4.QUESTION_ID = 2 ");
		sb.append("AND TSD4.ANSWER_CHOICE_ID = 2 ");
		sb.append(")  "); //住友データ
		if (year != null) {
			sb.append("AND TO_CHAR(TSR2.SURVEY_DATE,'YYYY') IN (" + year + ") "); //年度
		}
		if (katashiki != null) {
			sb.append("AND MM2.MACHINE_GENERATION IN (" + katashiki + ") "); //型式
		}
		if (model != null) {
			sb.append("AND MM2.MODEL_CD IN (" + model + ") "); //モデル
		}
		if (dealerCd != null) {
			sb.append("AND MM2.DAIRITENN_CD = '" + dealerCd + "' "); //代理店
		}
		if (applicableUsesCd != null) {
			sb.append("AND ");
			sb.append("EXISTS ( ");
			sb.append("SELECT ");
			sb.append("* ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD5 ");
			sb.append("WHERE ");
			sb.append("TSD3.SURVEY_RESULT_ID = TSD5.SURVEY_RESULT_ID ");
			sb.append("AND TSD5.QUESTION_ID = 1 ");
			sb.append("AND TSD5.ANSWER_CHOICE_ID IN (" + applicableUsesCd + ") ");
			sb.append(") ");
		}
		sb.append("GROUP BY ");
		sb.append("TSD3.QUESTION_ID ");
		sb.append(") ALLCOUNT ON ALLCOUNT.QUESTION_ID = SMY.QUESTION_ID ");
		sb.append("WHERE ");
		sb.append("MSC.QUESTION_ID IN(8,9,11,12,14,15,16,17,18,19,21,22,24,25,27,28,29) ");
		sb.append("ORDER BY ");
		sb.append("MSC.QUESTION_ID, ");
		sb.append("MSC.CHOICE_ID ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	}







	//40.serviceEvaluationPriority(サービス項目への評価平均と優先度)

		/**
		 * NativeQuery findEvaluationPrioritys
		 * findEvaluationPrioritys : サービス項目への評価平均と優先度取得
		 */
		@SuppressWarnings("unchecked")
		@Override
	//	public List<Object[]> findEvaluationPrioritys(Integer deviceLanguage,String year){
		public List<Object[]> findEvaluationPrioritys(Integer deviceLanguage,String year,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){
			StringBuilder sb = new StringBuilder();

	/*
			sb.append("SELECT ");
			sb.append("SUBSTRB(T1.QUESTION,0,INSTR(T1.QUESTION,'.')+1) AS NUM, "); //0
			sb.append("T1.QUESTION_ID, ");      //1
			sb.append("T1.QUESTION, ");         //2
			sb.append("NVL(T3.PRIORITY,0), ");  //3
			sb.append("NVL(T4.CSPOINT,0) ");    //4
			sb.append("FROM(  ");
			sb.append("SELECT ");
			sb.append("MSQ.QUESTION_ID, ");
			sb.append("MSQ.QUESTION" + deviceLanguage + " AS QUESTION ");
			sb.append("FROM MST_SURVEY_QUESTION MSQ ");
			sb.append("JOIN MST_SURVEY MS ON MSQ.SURVEY_ID = MS.SURVEY_ID ");
			sb.append("WHERE MSQ.DELETE_FLG = 0 ");
			sb.append("AND MS.DELETE_FLG = 0 ");
			sb.append("AND MSQ.QUESTION_ID BETWEEN 40 AND 54 "); //サービス項目設問ID
			sb.append("ORDER BY MSQ.QUESTION_ID )T1 ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("ANSWERSUM.QUESTION_ID, ");
			sb.append("POINTTABLE.SUMPOINT, ");
			sb.append("ANSWERSUM.RESULTCOUNT, ");
			sb.append("ROUND(POINTTABLE.SUMPOINT / ANSWERSUM.RESULTCOUNT,1) AS PRIORITY ");
			sb.append("FROM( ");
			sb.append("SELECT T2.ANSWER_CHOICE_ID, ");
			sb.append("SUM(T2.POINT) AS SUMPOINT ");
			sb.append("FROM( ");
			sb.append("SELECT TSD.SURVEY_RESULT_ID, ");
			sb.append("(TSD.ANSWER_CHOICE_ID + 39) AS ANSWER_CHOICE_ID, "); //重要度の選択肢IDと評価項目の設問IDを揃える
			sb.append("CASE TSD.SEQ ");
			sb.append("WHEN 1 THEN 3 ");
			sb.append("WHEN 2 THEN 2 ");
			sb.append("WHEN 3 THEN 1 ");
			sb.append("ELSE 0 END AS POINT ");
			sb.append("FROM TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND TSD.QUESTION_ID = 55 "); //サービス項目重要度設問
			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append(")T2 ");
			sb.append("GROUP BY T2.ANSWER_CHOICE_ID ");
			sb.append(")POINTTABLE ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT TSD.QUESTION_ID, ");
			sb.append("COUNT(TSD.SURVEY_RESULT_ID) AS RESULTCOUNT ");
			sb.append("FROM TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY QUESTION_ID ");
			sb.append(")ANSWERSUM ON POINTTABLE.ANSWER_CHOICE_ID = ANSWERSUM.QUESTION_ID ");
			sb.append(")T3 ON T1.QUESTION_ID = T3.QUESTION_ID ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.QUESTION_ID, ");
			sb.append("ROUND(SUM(MSC.CHOICE"+ deviceLanguage + ")/ COUNT(TSD.QUESTION_ID),1) AS CSPOINT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("JOIN MST_SURVEY_CHOICE MSC ON (TSD.QUESTION_ID = MSC.QUESTION_ID AND TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ) ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND TSD.QUESTION_ID BETWEEN 40 AND 54 "); //サービス項目設問ID
			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY TSD.QUESTION_ID ");
			sb.append(")T4 ON T1.QUESTION_ID = T4.QUESTION_ID ");
	*/
			sb.append("SELECT ");
			sb.append("SUBSTRB(T1.QUESTION,0,INSTR(T1.QUESTION,'.')+1) AS NUM, "); //0
			sb.append("T1.QUESTION_ID, ");      //1
			sb.append("T1.QUESTION, ");         //2
			sb.append("NVL(T3.PRIORITY,0), ");  //3
			sb.append("NVL(T4.CSPOINT,0) ");    //4
			sb.append("FROM(  ");
			sb.append("SELECT ");
			sb.append("MSQ.QUESTION_ID, ");
			sb.append("MSQ.QUESTION" + deviceLanguage + " AS QUESTION ");
			sb.append("FROM MST_SURVEY_QUESTION MSQ ");
			sb.append("JOIN MST_SURVEY MS ON MSQ.SURVEY_ID = MS.SURVEY_ID ");
			sb.append("WHERE MSQ.DELETE_FLG = 0 ");
			sb.append("AND MS.DELETE_FLG = 0 ");
			sb.append("AND MSQ.QUESTION_ID BETWEEN 40 AND 54 "); //サービス項目設問ID
			sb.append("ORDER BY MSQ.QUESTION_ID )T1 ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("ANSWERSUM.QUESTION_ID, ");
			sb.append("POINTTABLE.SUMPOINT, ");
			sb.append("ANSWERSUM.RESULTCOUNT, ");
			sb.append("ROUND(POINTTABLE.SUMPOINT / ANSWERSUM.RESULTCOUNT,1) AS PRIORITY ");
			sb.append("FROM( ");
			sb.append("SELECT T2.ANSWER_CHOICE_ID, ");
			sb.append("SUM(T2.POINT) AS SUMPOINT ");
			sb.append("FROM( ");
			sb.append("SELECT TSD.SURVEY_RESULT_ID, ");
			sb.append("(TSD.ANSWER_CHOICE_ID + 39) AS ANSWER_CHOICE_ID, "); //重要度の選択肢IDと評価項目の設問IDを揃える
			sb.append("CASE TSD.SEQ ");
			sb.append("WHEN 1 THEN 3 ");
			sb.append("WHEN 2 THEN 2 ");
			sb.append("WHEN 3 THEN 1 ");
			sb.append("ELSE 0 END AS POINT ");
			sb.append("FROM TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND MM.DELETE_FLAG = 0 ");
			sb.append("AND TSD.QUESTION_ID = 55 "); //サービス項目重要度設問
			// ユーザー権限 機械公開制御
			if(typeFlg == 0 ) {
				// 0:SCM
				sb.append("");

			}else if(typeFlg == 1) {
				// 1:地区
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
				sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
				sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

			}else if(typeFlg == 2) {
				// 2:代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
				sb.append(") ");

			}else if(typeFlg == 3) {
				// 3:一般、レンタル
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 4) {
				// 4:サービス工場
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 21) {
				// 21:SCMSEA
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
				sb.append(") ");

			}else if(typeFlg == 31) {
				// 31:トルコ代理店
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
				sb.append(") ");

			}else if(typeFlg == 35) {
				// 35:トルコサブ代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
				sb.append(") ");

			}
			else {
				sb.append("AND ROWNUM <= 0 ");
			}

			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append(")T2 ");
			sb.append("GROUP BY T2.ANSWER_CHOICE_ID ");
			sb.append(")POINTTABLE ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT TSD.QUESTION_ID, ");
			sb.append("COUNT(TSD.SURVEY_RESULT_ID) AS RESULTCOUNT ");
			sb.append("FROM TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND MM.DELETE_FLAG = 0 ");
			// ユーザー権限 機械公開制御
			if(typeFlg == 0 ) {
				// 0:SCM
				sb.append("");

			}else if(typeFlg == 1) {
				// 1:地区
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
				sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
				sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

			}else if(typeFlg == 2) {
				// 2:代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
				sb.append(") ");

			}else if(typeFlg == 3) {
				// 3:一般、レンタル
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 4) {
				// 4:サービス工場
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 21) {
				// 21:SCMSEA
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
				sb.append(") ");

			}else if(typeFlg == 31) {
				// 31:トルコ代理店
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
				sb.append(") ");

			}else if(typeFlg == 35) {
				// 35:トルコサブ代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
				sb.append(") ");

			}
			else {
				sb.append("AND ROWNUM <= 0 ");
			}

			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY QUESTION_ID ");
			sb.append(")ANSWERSUM ON POINTTABLE.ANSWER_CHOICE_ID = ANSWERSUM.QUESTION_ID ");
			sb.append(")T3 ON T1.QUESTION_ID = T3.QUESTION_ID ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.QUESTION_ID, ");
			sb.append("ROUND(SUM(MSC.CHOICE"+ deviceLanguage + ")/ COUNT(TSD.QUESTION_ID),1) AS CSPOINT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("JOIN MST_SURVEY_CHOICE MSC ON (TSD.QUESTION_ID = MSC.QUESTION_ID AND TSD.ANSWER_CHOICE_ID = MSC.CHOICE_ID ) ");
			sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND MM.DELETE_FLAG = 0 ");
			sb.append("AND TSD.QUESTION_ID BETWEEN 40 AND 54 "); //サービス項目設問ID
			// ユーザー権限 機械公開制御
			if(typeFlg == 0 ) {
				// 0:SCM
				sb.append("");

			}else if(typeFlg == 1) {
				// 1:地区
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
				sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
				sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

			}else if(typeFlg == 2) {
				// 2:代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
				sb.append(") ");

			}else if(typeFlg == 3) {
				// 3:一般、レンタル
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 4) {
				// 4:サービス工場
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 21) {
				// 21:SCMSEA
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
				sb.append(") ");

			}else if(typeFlg == 31) {
				// 31:トルコ代理店
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
				sb.append(") ");

			}else if(typeFlg == 35) {
				// 35:トルコサブ代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
				sb.append(") ");

			}
			else {
				sb.append("AND ROWNUM <= 0 ");
			}

			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY TSD.QUESTION_ID ");
			sb.append(")T4 ON T1.QUESTION_ID = T4.QUESTION_ID ");

			em.getEntityManagerFactory().getCache().evictAll();
			List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();


			if (rs.size() > 0)
				return rs;
			else
				return null;
		}







	//41.maintenanceReason(自社或いは他社で整備する理由)

		/**
		 * NativeQuery findMaintenanceReason
		 * findMaintenanceReason : 自社或いは他社で整備する理由取得
		 */
		@SuppressWarnings("unchecked")
		@Override
//		public List<Object[]> findMaintenanceReason(Integer deviceLanguage,String year,Integer choiceCount){
		public List<Object[]> findMaintenanceReason(Integer deviceLanguage,String year,Integer choiceCount
				,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){
			StringBuilder sb = new StringBuilder();
		/*
			sb.append("SELECT ");
			sb.append("CHR(65 + (T3.CHOICE_ID - 1)), "); //0
			sb.append("T3.CHOICE_ID, ");                 //1
			sb.append("T3.CHOICE, ");                    //2
			sb.append("T3.FREE_INPUT_FLG, ");            //3
			sb.append("T3.CHOICE_COUNT, ");              //4
			sb.append("T3.PER ");                        //5
			sb.append("FROM( ");
			sb.append("SELECT ");
			sb.append("T1.CHOICE_ID, ");
			sb.append("T1.CHOICE, ");
			sb.append("T1.FREE_INPUT_FLG, ");
			sb.append("NVL(T2.CHOICE_COUNT,0) AS CHOICE_COUNT, ");
			sb.append("NVL(FLOOR((T2.CHOICE_COUNT / " + choiceCount + ") * 100),0) AS PER ");
			sb.append("FROM( ");
			sb.append("SELECT ");
			sb.append("MSC.CHOICE_ID, ");
			sb.append("MSC.CHOICE" + deviceLanguage + " AS CHOICE,");
			sb.append("CASE MSC.CHOICE_ID ");
			sb.append("WHEN MSQ.FREE_INPUT_CHOICE_ID THEN 1 ");
			sb.append("ELSE 0 ");
			sb.append("END AS FREE_INPUT_FLG ");
			sb.append("FROM ");
			sb.append("MST_SURVEY MS ");
			sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ON MS.SURVEY_ID = MSQ.SURVEY_ID ");
			sb.append("LEFT JOIN MST_SURVEY_CHOICE MSC ON MSQ.QUESTION_ID = MSC.QUESTION_ID ");
			sb.append("WHERE ");
			sb.append("MS.DELETE_FLG = 0 ");
			sb.append("AND MSQ.DELETE_FLG = 0 ");
			sb.append("AND MSC.DELETE_FLG = 0 ");
			sb.append("AND MS.SURVEY_ID = 1 ");
			sb.append("AND MSQ.QUESTION_ID IN (60,61) ");
			sb.append("GROUP BY MSC.CHOICE_ID, ");
			sb.append("MSC.CHOICE" + deviceLanguage + ", " );
			sb.append("MSQ.FREE_INPUT_CHOICE_ID ");
			sb.append("ORDER BY MSC.CHOICE_ID ");
			sb.append(")T1 ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.ANSWER_CHOICE_ID, ");
			sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS CHOICE_COUNT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_RESULT TSR ");
			sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			sb.append("WHERE ");
			sb.append("TSR.DELETE_FLG = 0 ");
			sb.append("AND TSD.DELETE_FLG = 0 ");
			sb.append("AND TSR.SURVEY_ID = 1 ");
			sb.append("AND TSD.QUESTION_ID IN (60,61) ");
			sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY TSD.ANSWER_CHOICE_ID ");
			sb.append("ORDER BY TSD.ANSWER_CHOICE_ID ");
			sb.append(")T2 ON T1.CHOICE_ID = T2.ANSWER_CHOICE_ID ");
			sb.append("ORDER BY T1.CHOICE_ID ");
			sb.append(")T3 ");
		 */
			sb.append("SELECT ");
			sb.append("CHR(65 + (T3.CHOICE_ID - 1)), "); //0
			sb.append("T3.CHOICE_ID, ");                 //1
			sb.append("T3.CHOICE, ");                    //2
			sb.append("T3.FREE_INPUT_FLG, ");            //3
			sb.append("T3.CHOICE_COUNT, ");              //4
			sb.append("T3.PER ");                        //5
			sb.append("FROM( ");
			sb.append("SELECT ");
			sb.append("T1.CHOICE_ID, ");
			sb.append("T1.CHOICE, ");
			sb.append("T1.FREE_INPUT_FLG, ");
			sb.append("NVL(T2.CHOICE_COUNT,0) AS CHOICE_COUNT, ");
			sb.append("NVL(FLOOR((T2.CHOICE_COUNT / " + choiceCount + ") * 100),0) AS PER ");
			sb.append("FROM( ");
			sb.append("SELECT ");
			sb.append("MSC.CHOICE_ID, ");
			sb.append("MSC.CHOICE" + deviceLanguage + " AS CHOICE,");
			sb.append("CASE MSC.CHOICE_ID ");
			sb.append("WHEN MSQ.FREE_INPUT_CHOICE_ID THEN 1 ");
			sb.append("ELSE 0 ");
			sb.append("END AS FREE_INPUT_FLG ");
			sb.append("FROM ");
			sb.append("MST_SURVEY MS ");
			sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ON MS.SURVEY_ID = MSQ.SURVEY_ID ");
			sb.append("LEFT JOIN MST_SURVEY_CHOICE MSC ON MSQ.QUESTION_ID = MSC.QUESTION_ID ");
			sb.append("WHERE ");
			sb.append("MS.DELETE_FLG = 0 ");
			sb.append("AND MSQ.DELETE_FLG = 0 ");
			sb.append("AND MSC.DELETE_FLG = 0 ");
			sb.append("AND MS.SURVEY_ID = 1 ");
			sb.append("AND MSQ.QUESTION_ID IN (60,61) ");
			sb.append("GROUP BY MSC.CHOICE_ID, ");
			sb.append("MSC.CHOICE" + deviceLanguage + ", " );
			sb.append("MSQ.FREE_INPUT_CHOICE_ID ");
			sb.append("ORDER BY MSC.CHOICE_ID ");
			sb.append(")T1 ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.ANSWER_CHOICE_ID, ");
			sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS CHOICE_COUNT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_RESULT TSR ");
			sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			sb.append("WHERE ");
			sb.append("TSR.DELETE_FLG = 0 ");
			sb.append("AND TSD.DELETE_FLG = 0 ");
			sb.append("AND MM.DELETE_FLAG = 0 ");
			sb.append("AND TSR.SURVEY_ID = 1 ");
			sb.append("AND TSD.QUESTION_ID IN (60,61) ");
			sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
			// ユーザー権限 機械公開制御
			if(typeFlg == 0 ) {
				// 0:SCM
				sb.append("");

			}else if(typeFlg == 1) {
				// 1:地区
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
				sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
				sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

			}else if(typeFlg == 2) {
				// 2:代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
				sb.append(") ");

			}else if(typeFlg == 3) {
				// 3:一般、レンタル
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 4) {
				// 4:サービス工場
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 21) {
				// 21:SCMSEA
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
				sb.append(") ");

			}else if(typeFlg == 31) {
				// 31:トルコ代理店
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
				sb.append(") ");

			}else if(typeFlg == 35) {
				// 35:トルコサブ代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
				sb.append(") ");

			}
			else {
				sb.append("AND ROWNUM <= 0 ");
			}

			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY TSD.ANSWER_CHOICE_ID ");
			sb.append("ORDER BY TSD.ANSWER_CHOICE_ID ");
			sb.append(")T2 ON T1.CHOICE_ID = T2.ANSWER_CHOICE_ID ");
			sb.append("ORDER BY T1.CHOICE_ID ");
			sb.append(")T3 ");


			em.getEntityManagerFactory().getCache().evictAll();
			List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

			if (rs.size() > 0)
				return rs;
			else
				return null;
		}

		 /**
		 * NativeQuery findCompetitorAnswerCount
		 * findCompetitorAnswerCount : 自社或いは他社で整備する理由 選択肢別　競合他社回答数取得
		 */
		@SuppressWarnings("unchecked")
		@Override
	//	public List<Object[]> findCompetitorAnswerCount(Integer deviceLanguage,String year,Integer choiceId){
		public List<Object[]> findCompetitorAnswerCount(Integer deviceLanguage,String year,Integer choiceId,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){
			StringBuilder sb = new StringBuilder();
		/*
			sb.append("SELECT ");
			sb.append("NVL(T2.ANSWER_COUNT, 0), ");            //0
			sb.append("MSC.CHOICE" + deviceLanguage + " ");   //1
			sb.append("FROM ");
			sb.append("MST_SURVEY_CHOICE MSC ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.QUESTION_ID, ");
			sb.append("TSD.ANSWER_CHOICE_ID, ");
			sb.append("COUNT(TSD.ANSWER_CHOICE_ID) ANSWER_COUNT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD2.SURVEY_RESULT_ID ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD2 ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD2.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("WHERE TSD2.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND TSD2.QUESTION_ID IN (60,61) ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID IS NOT NULL ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID =" + choiceId + " ");
			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append(")T1 ON TSD.SURVEY_RESULT_ID = T1.SURVEY_RESULT_ID ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSD.QUESTION_ID = 33 ");
			sb.append("GROUP BY TSD.QUESTION_ID,TSD.ANSWER_CHOICE_ID ");
			sb.append(")T2 ON (MSC.QUESTION_ID = T2.QUESTION_ID AND MSC.CHOICE_ID = T2.ANSWER_CHOICE_ID) ");
			sb.append("WHERE ");
			sb.append("MSC.DELETE_FLG = 0 ");
			sb.append("AND MSC.QUESTION_ID = 33 ");
			sb.append("ORDER BY MSC.CHOICE_ID ");
		*/

			sb.append("SELECT ");
			sb.append("NVL(T2.ANSWER_COUNT, 0), ");            //0
			sb.append("MSC.CHOICE" + deviceLanguage + " ");   //1
			sb.append("FROM ");
			sb.append("MST_SURVEY_CHOICE MSC ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.QUESTION_ID, ");
			sb.append("TSD.ANSWER_CHOICE_ID, ");
			sb.append("COUNT(TSD.ANSWER_CHOICE_ID) ANSWER_COUNT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD ");
			sb.append("JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD2.SURVEY_RESULT_ID ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_DETAIL TSD2 ");
			sb.append("JOIN TBL_SURVEY_RESULT TSR ON TSD2.SURVEY_RESULT_ID = TSR.SURVEY_RESULT_ID ");
			sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			sb.append("WHERE TSD2.DELETE_FLG = 0 ");
			sb.append("AND TSR.DELETE_FLG = 0 ");
			sb.append("AND MM.DELETE_FLAG = 0 ");
			sb.append("AND TSD2.QUESTION_ID IN (60,61) ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID IS NOT NULL ");
			sb.append("AND TSD2.ANSWER_CHOICE_ID =" + choiceId + " ");
			// ユーザー権限 機械公開制御
			if(typeFlg == 0 ) {
				// 0:SCM
				sb.append("");

			}else if(typeFlg == 1) {
				// 1:地区
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
				sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
				sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

			}else if(typeFlg == 2) {
				// 2:代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
				sb.append(") ");

			}else if(typeFlg == 3) {
				// 3:一般、レンタル
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 4) {
				// 4:サービス工場
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 21) {
				// 21:SCMSEA
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
				sb.append(") ");

			}else if(typeFlg == 31) {
				// 31:トルコ代理店
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
				sb.append(") ");

			}else if(typeFlg == 35) {
				// 35:トルコサブ代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
				sb.append(") ");

			}
			else {
				sb.append("AND ROWNUM <= 0 ");
			}

			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append(")T1 ON TSD.SURVEY_RESULT_ID = T1.SURVEY_RESULT_ID ");
			sb.append("WHERE TSD.DELETE_FLG = 0 ");
			sb.append("AND TSD.QUESTION_ID = 33 ");
			sb.append("GROUP BY TSD.QUESTION_ID,TSD.ANSWER_CHOICE_ID ");
			sb.append(")T2 ON (MSC.QUESTION_ID = T2.QUESTION_ID AND MSC.CHOICE_ID = T2.ANSWER_CHOICE_ID) ");
			sb.append("WHERE ");
			sb.append("MSC.DELETE_FLG = 0 ");
			sb.append("AND MSC.QUESTION_ID = 33 ");
			sb.append("ORDER BY MSC.CHOICE_ID ");

			em.getEntityManagerFactory().getCache().evictAll();
			List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

			if (rs.size() > 0)
				return rs;
			else
				return null;
		}







	//42.periodicInspection(定期検査・整備)

		 /**
		 * NativeQuery findPeriodicInspection
		 * findPeriodicInspection : 定期検査・整備取得
		 */
		@SuppressWarnings("unchecked")
		@Override
	//	public List<Object[]> findPeriodicInspection(Integer deviceLanguage,String year,Integer choiceCount){
		public List<Object[]> findPeriodicInspection(Integer deviceLanguage,String year,Integer choiceCount
				        ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){
			StringBuilder sb = new StringBuilder();
		/*
			sb.append("SELECT ");
			sb.append("CHR(65 + T2.CHOICE_ID - 1), "); //0
			sb.append("T2.CHOICE_ID, ");               //1
			sb.append("T2.CHOICE, ");                  //2
			sb.append("T2.FREE_INPUT_FLG, ");          //3
			sb.append("T2.CHOICE_ANSWER_COUNT, ");     //4
			sb.append("T2.PER ");                      //5
			sb.append("FROM( ");
			sb.append("SELECT ");
			sb.append("MSC.CHOICE_ID, ");
			sb.append("MSC.CHOICE" + deviceLanguage + " AS CHOICE, ");
			sb.append("NVL(MSQR.FREE_INPUT_FLG,0) AS FREE_INPUT_FLG, ");
			sb.append("NVL(T1.ANSWER_COUNT,0) AS CHOICE_ANSWER_COUNT, ");
			//sb.append("NVL(FLOOR(T1.ANSWER_COUNT / " + choiceCount + "  * 100),0) AS PER ");
			sb.append("NVL(T1.ANSWER_COUNT / " + choiceCount + "  * 100,0) AS PER ");
			sb.append("FROM ");
			sb.append("MST_SURVEY MS ");
			sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ON MS.SURVEY_ID = MSQ.SURVEY_ID ");
			sb.append("LEFT JOIN MST_SURVEY_CHOICE MSC ON MSQ.QUESTION_ID = MSC.QUESTION_ID ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("PARENT_CHOICE_ID, ");
			sb.append("1 AS FREE_INPUT_FLG ");
			sb.append("FROM MST_SURVEY_QUESTION_RELATION ");
			sb.append("WHERE CHILD_QUESTION_ID IN (57,58) ");
			sb.append("AND DELETE_FLG = 0 ");
			sb.append(")MSQR ON MSC.CHOICE_ID = MSQR.PARENT_CHOICE_ID ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.QUESTION_ID, ");
			sb.append("TSD.ANSWER_CHOICE_ID, ");
			sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS ANSWER_COUNT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_RESULT TSR ");
			sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			sb.append("WHERE ");
			sb.append("TSR.DELETE_FLG = 0 ");
			sb.append("AND TSD.DELETE_FLG = 0 ");
			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY TSD.QUESTION_ID, TSD.ANSWER_CHOICE_ID ");
			sb.append(")T1 ON (MSQ.QUESTION_ID = T1.QUESTION_ID AND MSC.CHOICE_ID = T1.ANSWER_CHOICE_ID) ");
			sb.append("WHERE ");
			sb.append("MS.DELETE_FLG = 0 ");
			sb.append("AND MSQ.DELETE_FLG = 0 ");
			sb.append("AND MSC.DELETE_FLG = 0 ");
			sb.append("AND MS.SURVEY_ID = 1 ");
			sb.append("AND MSQ.QUESTION_ID = 56 ");
			sb.append("ORDER BY MSC.CHOICE_ID ");
			sb.append(")T2 ");
		*/
			sb.append("SELECT ");
			sb.append("CHR(65 + T2.CHOICE_ID - 1), "); //0
			sb.append("T2.CHOICE_ID, ");               //1
			sb.append("T2.CHOICE, ");                  //2
			sb.append("T2.FREE_INPUT_FLG, ");          //3
			sb.append("T2.CHOICE_ANSWER_COUNT, ");     //4
			sb.append("T2.PER ");                      //5
			sb.append("FROM( ");
			sb.append("SELECT ");
			sb.append("MSC.CHOICE_ID, ");
			sb.append("MSC.CHOICE" + deviceLanguage + " AS CHOICE, ");
			sb.append("NVL(MSQR.FREE_INPUT_FLG,0) AS FREE_INPUT_FLG, ");
			sb.append("NVL(T1.ANSWER_COUNT,0) AS CHOICE_ANSWER_COUNT, ");
			sb.append("NVL(T1.ANSWER_COUNT / " + choiceCount + "  * 100,0) AS PER ");
			sb.append("FROM ");
			sb.append("MST_SURVEY MS ");
			sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ON MS.SURVEY_ID = MSQ.SURVEY_ID ");
			sb.append("LEFT JOIN MST_SURVEY_CHOICE MSC ON MSQ.QUESTION_ID = MSC.QUESTION_ID ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("PARENT_CHOICE_ID, ");
			sb.append("1 AS FREE_INPUT_FLG ");
			sb.append("FROM MST_SURVEY_QUESTION_RELATION ");
			sb.append("WHERE CHILD_QUESTION_ID IN (57,58) ");
			sb.append("AND DELETE_FLG = 0 ");
			sb.append(")MSQR ON MSC.CHOICE_ID = MSQR.PARENT_CHOICE_ID ");
			sb.append("LEFT JOIN( ");
			sb.append("SELECT ");
			sb.append("TSD.QUESTION_ID, ");
			sb.append("TSD.ANSWER_CHOICE_ID, ");
			sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS ANSWER_COUNT ");
			sb.append("FROM ");
			sb.append("TBL_SURVEY_RESULT TSR ");
			sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			sb.append("WHERE ");
			sb.append("TSR.DELETE_FLG = 0 ");
			sb.append("AND TSD.DELETE_FLG = 0 ");
			sb.append("AND MM.DELETE_FLAG = 0 ");
			// ユーザー権限 機械公開制御
			if(typeFlg == 0 ) {
				// 0:SCM
				sb.append("");

			}else if(typeFlg == 1) {
				// 1:地区
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
				sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
				sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
						+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

			}else if(typeFlg == 2) {
				// 2:代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
				sb.append(") ");

			}else if(typeFlg == 3) {
				// 3:一般、レンタル
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 4) {
				// 4:サービス工場
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
				sb.append("OR ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
				sb.append(") ");

			}else if(typeFlg == 21) {
				// 21:SCMSEA
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
				sb.append(") ");

			}else if(typeFlg == 31) {
				// 31:トルコ代理店
				sb.append("AND ( ");
				sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
				sb.append(") ");

			}else if(typeFlg == 35) {
				// 35:トルコサブ代理店
				sb.append("AND ( ");
				sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
				sb.append("OR ");
				sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
				sb.append(") ");

			}
			else {
				sb.append("AND ROWNUM <= 0 ");
			}

			if(year != null) {
				sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			}
			sb.append("GROUP BY TSD.QUESTION_ID, TSD.ANSWER_CHOICE_ID ");
			sb.append(")T1 ON (MSQ.QUESTION_ID = T1.QUESTION_ID AND MSC.CHOICE_ID = T1.ANSWER_CHOICE_ID) ");
			sb.append("WHERE ");
			sb.append("MS.DELETE_FLG = 0 ");
			sb.append("AND MSQ.DELETE_FLG = 0 ");
			sb.append("AND MSC.DELETE_FLG = 0 ");
			sb.append("AND MS.SURVEY_ID = 1 ");
			sb.append("AND MSQ.QUESTION_ID = 56 ");
			sb.append("ORDER BY MSC.CHOICE_ID ");
			sb.append(")T2 ");


			em.getEntityManagerFactory().getCache().evictAll();
			List<Object[]> rs = em.createNativeQuery(sb.toString()).getResultList();

			if (rs.size() > 0)
				return rs;
			else
				return null;
		}







//43.importanceInPurchasing(購入時の重要指数)

	/**
	 * NativeQuery findByImportanceInPurchasingList
	 * findByImportanceInPurchasingList : 「購入時の重要指数」詳細一覧
	 */
	@SuppressWarnings("unchecked")
	@Override
//	public List<Object[]> findByImportanceInPurchasingList(Integer deviceLanguage, Integer countAll, String year){
	public List<Object[]> findByImportanceInPurchasingList(Integer deviceLanguage, Integer countAll, String year
			           ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){

		StringBuilder sb = new StringBuilder();
	/*
		sb.append("SELECT ");
		sb.append("T1.CHOICE_ID, ");
		sb.append("T1.ITEM_NO, ");
		sb.append("T1.CHOICE"+ deviceLanguage +" AS ITEM, ");
		sb.append("NVL(T2.CHOICE_COUNT, 0), ");
		sb.append("NVL(FLOOR(T2.CHOICE_COUNT / "+ countAll + " * 100),0) AS ANSWER_PERCENTAGE, ");
		sb.append("T1.FREE_INPUT_FLG ");
		sb.append("FROM ");
		sb.append("(SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("CHR(65 + MSC.CHOICE_ID -1) AS ITEM_NO, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + ", ");
		sb.append("CASE MSC.CHOICE_ID WHEN MSQ.FREE_INPUT_CHOICE_ID THEN 1 ELSE 0 END AS FREE_INPUT_FLG ");
		sb.append("FROM ");
		sb.append("MST_SURVEY MS ");
		sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ");
		sb.append("ON(MS.SURVEY_ID = MSQ.SURVEY_ID) ");
		sb.append("LEFT OUTER JOIN MST_SURVEY_CHOICE MSC ");
		sb.append("ON(MSC.QUESTION_ID = MSQ.QUESTION_ID) ");
		sb.append("WHERE ");
		sb.append("MS.DELETE_FLG = 0 ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");
		sb.append("AND MSC.DELETE_FLG = 0 ");
		sb.append("AND MS.SURVEY_ID = 1 ");
		sb.append("AND MSQ.QUESTION_ID = 62 ");
		sb.append("GROUP BY ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + ", ");
		sb.append("MSQ.FREE_INPUT_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("MSC.CHOICE_ID ");
		sb.append(" ) T1 ");
		sb.append("LEFT JOIN ");
		sb.append("(SELECT ");
		sb.append("TSD.ANSWER_CHOICE_ID, ");
		sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS CHOICE_COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("LEFT JOIN ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("ON(TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID) ");
		sb.append("WHERE ");
		sb.append("TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.QUESTION_ID = 62 ");
		sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
		if(year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY')  IN (" + year + ") ");}
		sb.append("GROUP BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append(") T2 ON T1.CHOICE_ID = T2.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("T1.CHOICE_ID ");
	*/
		sb.append("SELECT ");
		sb.append("T1.CHOICE_ID, ");
		sb.append("T1.ITEM_NO, ");
		sb.append("T1.CHOICE"+ deviceLanguage +" AS ITEM, ");
		sb.append("NVL(T2.CHOICE_COUNT, 0), ");
		sb.append("NVL(FLOOR(T2.CHOICE_COUNT / "+ countAll + " * 100),0) AS ANSWER_PERCENTAGE, ");
		sb.append("T1.FREE_INPUT_FLG ");
		sb.append("FROM ");
		sb.append("(SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("CHR(65 + MSC.CHOICE_ID -1) AS ITEM_NO, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + ", ");
		sb.append("CASE MSC.CHOICE_ID WHEN MSQ.FREE_INPUT_CHOICE_ID THEN 1 ELSE 0 END AS FREE_INPUT_FLG ");
		sb.append("FROM ");
		sb.append("MST_SURVEY MS ");
		sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ");
		sb.append("ON(MS.SURVEY_ID = MSQ.SURVEY_ID) ");
		sb.append("LEFT OUTER JOIN MST_SURVEY_CHOICE MSC ");
		sb.append("ON(MSC.QUESTION_ID = MSQ.QUESTION_ID) ");
		sb.append("WHERE ");
		sb.append("MS.DELETE_FLG = 0 ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");
		sb.append("AND MSC.DELETE_FLG = 0 ");
		sb.append("AND MS.SURVEY_ID = 1 ");
		sb.append("AND MSQ.QUESTION_ID = 62 ");
		sb.append("GROUP BY ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + ", ");
		sb.append("MSQ.FREE_INPUT_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("MSC.CHOICE_ID ");
		sb.append(" ) T1 ");
		sb.append("LEFT JOIN ");
		sb.append("(SELECT ");
		sb.append("TSD.ANSWER_CHOICE_ID, ");
		sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS CHOICE_COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("LEFT JOIN ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("ON(TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID) ");
		sb.append("JOIN MST_MACHINE MM2 ON TSR.KIBAN_SERNO = MM2.KIBAN_SERNO ");
		sb.append("WHERE ");
		sb.append("TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MM2.DELETE_FLAG = 0 ");
		sb.append("AND TSD.QUESTION_ID = 62 ");
		sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM2.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM2.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM2.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM2.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM2.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM2.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}

		if(year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY')  IN (" + year + ") ");}
		sb.append("GROUP BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append(") T2 ON T1.CHOICE_ID = T2.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("T1.CHOICE_ID ");


		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}







//44.expectedService(サービスに今後期待すること)

	/**
	 * NativeQuery findByExpectedService
	 * findByExpectedServiceList : 「サービスに今後期待すること」詳細取得
	 */
//	public List<Object[]> findByExpectedService(Integer deviceLanguage, Integer countAll, String year){
	public List<Object[]> findByExpectedService(Integer deviceLanguage, Integer countAll, String year
			,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){

		StringBuilder sb = new StringBuilder();
	/*
		sb.append("SELECT ");
		sb.append("T1.CHOICE_ID, ");
		sb.append("T1.ITEM_NO, ");
		sb.append("T1.CHOICE"+ deviceLanguage +" AS ITEM"+ deviceLanguage +", ");
		sb.append("NVL(T2.CHOICE_COUNT, 0), ");
		sb.append("NVL(FLOOR(T2.CHOICE_COUNT / "+ countAll + " * 100),0) AS ANSWER_PERCENTAGE ");
		sb.append("FROM ");
		sb.append("(SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("CHR(65 + MSC.CHOICE_ID -1) AS ITEM_NO, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + " ");
		sb.append("FROM ");
		sb.append("MST_SURVEY MS ");
		sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ");
		sb.append("ON(MS.SURVEY_ID = MSQ.SURVEY_ID) ");
		sb.append("LEFT OUTER JOIN MST_SURVEY_CHOICE MSC ");
		sb.append("ON(MSC.QUESTION_ID = MSQ.QUESTION_ID) ");
		sb.append("WHERE ");
		sb.append("MS.DELETE_FLG = 0 ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");
		sb.append("AND MSC.DELETE_FLG = 0 ");
		sb.append("AND MS.SURVEY_ID = 1 ");
		sb.append("AND MSQ.QUESTION_ID = 63 ");
		sb.append("GROUP BY ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + " ");
		sb.append("ORDER BY ");
		sb.append("MSC.CHOICE_ID ");
		sb.append(" ) T1 ");
		sb.append("LEFT JOIN ");
		sb.append("(SELECT ");
		sb.append("TSD.ANSWER_CHOICE_ID, ");
		sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS CHOICE_COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("LEFT JOIN ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("ON(TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID) ");
		sb.append("WHERE ");
		sb.append("TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.QUESTION_ID = 63 ");
		sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
		if(year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY')  IN (" + year + ") ");}
		sb.append("GROUP BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append(") T2 ON T1.CHOICE_ID = T2.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("T1.CHOICE_ID ");
	*/

		sb.append("SELECT ");
		sb.append("T1.CHOICE_ID, ");
		sb.append("T1.ITEM_NO, ");
		sb.append("T1.CHOICE"+ deviceLanguage +" AS ITEM"+ deviceLanguage +", ");
		sb.append("NVL(T2.CHOICE_COUNT, 0), ");
		sb.append("NVL(FLOOR(T2.CHOICE_COUNT / "+ countAll + " * 100),0) AS ANSWER_PERCENTAGE ");
		sb.append("FROM ");
		sb.append("(SELECT ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("CHR(65 + MSC.CHOICE_ID -1) AS ITEM_NO, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + " ");
		sb.append("FROM ");
		sb.append("MST_SURVEY MS ");
		sb.append("LEFT JOIN MST_SURVEY_QUESTION MSQ ");
		sb.append("ON(MS.SURVEY_ID = MSQ.SURVEY_ID) ");
		sb.append("LEFT OUTER JOIN MST_SURVEY_CHOICE MSC ");
		sb.append("ON(MSC.QUESTION_ID = MSQ.QUESTION_ID) ");
		sb.append("WHERE ");
		sb.append("MS.DELETE_FLG = 0 ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");
		sb.append("AND MSC.DELETE_FLG = 0 ");
		sb.append("AND MS.SURVEY_ID = 1 ");
		sb.append("AND MSQ.QUESTION_ID = 63 ");
		sb.append("GROUP BY ");
		sb.append("MSC.CHOICE_ID, ");
		sb.append("MSC.CHOICE"+ deviceLanguage + " ");
		sb.append("ORDER BY ");
		sb.append("MSC.CHOICE_ID ");
		sb.append(" ) T1 ");
		sb.append("LEFT JOIN ");
		sb.append("(SELECT ");
		sb.append("TSD.ANSWER_CHOICE_ID, ");
		sb.append("COUNT(TSD.ANSWER_CHOICE_ID) AS CHOICE_COUNT ");
		sb.append("FROM ");
		sb.append("TBL_SURVEY_RESULT TSR ");
		sb.append("LEFT JOIN ");
		sb.append("TBL_SURVEY_DETAIL TSD ");
		sb.append("ON(TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID) ");
		sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE ");
		sb.append("TSR.DELETE_FLG = 0 ");
		sb.append("AND TSD.DELETE_FLG = 0 ");
		sb.append("AND TSR.DELETE_FLG = 0 ");
		sb.append("AND MM.DELETE_FLAG = 0 ");
		sb.append("AND TSD.QUESTION_ID = 63 ");
		sb.append("AND TSD.ANSWER_CHOICE_ID IS NOT NULL ");
		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}

		if(year != null) {
			sb.append("AND TO_CHAR(TSR.SURVEY_DATE, 'YYYY')  IN (" + year + ") ");}
		sb.append("GROUP BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("TSD.ANSWER_CHOICE_ID ");
		sb.append(") T2 ON T1.CHOICE_ID = T2.ANSWER_CHOICE_ID ");
		sb.append("ORDER BY ");
		sb.append("T1.CHOICE_ID ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}







//45.surveyFreeInputList(フリー入力一覧)

	/**
	 * NativeQuery findDispQuestionNo
	 * findDispQuestionNo : 表示設問番号&設問取得
	 */
	 public List<Integer> findDispQuestionNo(String questionId){
		 StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("T2.QUESTION_NO ");   //0
		sb.append("FROM( ");
		sb.append("SELECT ");
		sb.append("ROWNUM AS QUESTION_NO, ");
		sb.append("T1.QUESTION_ID ");
		sb.append("FROM( ");
		sb.append("SELECT ");
		sb.append("MSQ.QUESTION_ID ");
		sb.append("FROM MST_SURVEY_QUESTION MSQ ");
		sb.append("WHERE MSQ.SURVEY_ID = 1 ");
		sb.append("AND MSQ.DELETE_FLG = 0 ");
		sb.append("AND MSQ.PARENT_FLG = 1 ");
		sb.append("AND MSQ.QUESTION_PATTERN <> 11 ");
		sb.append("ORDER BY MSQ.DISPLAY_SORT ");
		sb.append(")T1 ");
		sb.append(")T2 ");
		sb.append("WHERE ");
		sb.append("T2.QUESTION_ID = " + questionId + " ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	 }

	/**
	 * NativeQuery findQuestion
	 * findQuestion : 設問取得
	 */
	public List<String> findQuestion(String questionId,Integer deviceLanguage){
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MSQ.QUESTION" + deviceLanguage + " ");
		sb.append("FROM ");
		sb.append("MST_SURVEY_QUESTION MSQ ");
		sb.append("WHERE ");
		sb.append("MSQ.DELETE_FLG = 0 ");
		sb.append("AND MSQ.SURVEY_ID = 1 ");
		sb.append("AND MSQ.QUESTION_ID IN(" + questionId + ") ");


		em.getEntityManagerFactory().getCache().evictAll();
		List<String> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	 }

	/**
	 * NativeQuery findFreeInput
	 * surveyFreeInputList : フリー入力一覧取得
	 */
//	 public List<String> findFreeInput(Integer apiFlg,String questionId,Integer choiceId,String year){
	public List<String> findFreeInput(Integer apiFlg,String questionId,Integer choiceId,String year
			,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg){
		 StringBuilder sb = new StringBuilder();

	/*
		 sb.append("SELECT ");

		 if(apiFlg == 0 || apiFlg == 2) {
			 sb.append("TSD.ANSWER_FREEINPUT ");  //0
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_RESULT TSR ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			 sb.append("WHERE ");
			 sb.append("TSR.DELETE_FLG = 0 ");
			 sb.append("AND TSD.DELETE_FLG = 0 ");
			 sb.append("AND TSR.SURVEY_ID = 1 ");
			 sb.append("AND TSD.QUESTION_ID IN(" + questionId + ") ");
			 sb.append("AND TSD.ANSWER_CHOICE_ID = " + choiceId + " ");
			 if(year != null) {
				 sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			 }
			 sb.append("AND ROWNUM <= 1001 ");
			 sb.append("ORDER BY TSD.SURVEY_RESULT_ID DESC ");

		 }else if(apiFlg == 1) {
			 sb.append("TSD2.ANSWER_FREEINPUT "); //0
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_DETAIL TSD2 ");
			 sb.append("WHERE ");
			 sb.append("TSD2.DELETE_FLG = 0 ");
			 sb.append("AND TSD2.SURVEY_RESULT_ID IN( ");
			 sb.append("SELECT ");
			 sb.append("TSD.SURVEY_RESULT_ID ");
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_RESULT TSR ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			 sb.append("WHERE ");
			 sb.append("TSR.DELETE_FLG = 0 ");
			 sb.append("AND TSD.DELETE_FLG = 0 ");
			 sb.append("AND TSR.SURVEY_ID = 1 ");
			 sb.append("AND TSD.QUESTION_ID = " + questionId + " ");
			 sb.append("AND TSD.ANSWER_CHOICE_ID = " + choiceId + " ");
			 if(year != null) {
				 sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			 }
			 if(choiceId == 3) {
				 sb.append(")AND TSD2.QUESTION_ID = 57 ");
			 }else if(choiceId == 4) {
				 sb.append(")AND TSD2.QUESTION_ID = 58 ");
			 }
			 sb.append("AND ROWNUM <= 1001 ");
			 sb.append("ORDER BY TSD2.SURVEY_RESULT_ID DESC ");

		 }else if(apiFlg == 3) {
			 sb.append("TSD1.ANSWER_FREEINPUT ");  //0
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_RESULT TSR ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD1 ON TSR.SURVEY_RESULT_ID = TSD1.SURVEY_RESULT_ID ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD2 ON TSR.SURVEY_RESULT_ID = TSD2.SURVEY_RESULT_ID ");
			 sb.append("WHERE ");
			 sb.append("TSR.DELETE_FLG = 0 ");
			 sb.append("AND TSD1.DELETE_FLG = 0 ");
			 sb.append("AND TSD2.DELETE_FLG = 0 ");
			 sb.append("AND TSR.SURVEY_ID = 1 ");
			 sb.append("AND TSD1.QUESTION_ID = " + questionId + " ");
			 sb.append("AND TSD1.ANSWER_CHOICE_ID = " + choiceId + " ");
			 sb.append("AND TSD2.QUESTION_ID = 38 ");
			 sb.append("AND TSD2.ANSWER_CHOICE_ID = 2 ");
			 if(year != null) {
				 sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			 }
			 sb.append("AND ROWNUM <= 1001 ");
			 sb.append("ORDER BY TSD1.SURVEY_RESULT_ID DESC ");

		 }
	*/
		 sb.append("SELECT ");

		 if(apiFlg == 0 || apiFlg == 2) {
			 sb.append("TSD.ANSWER_FREEINPUT ");  //0
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_RESULT TSR ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			 sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			 sb.append("WHERE ");
			 sb.append("TSR.DELETE_FLG = 0 ");
			 sb.append("AND TSD.DELETE_FLG = 0 ");
			 sb.append("AND MM.DELETE_FLAG = 0 ");
			 sb.append("AND TSR.SURVEY_ID = 1 ");
			 sb.append("AND TSD.QUESTION_ID IN(" + questionId + ") ");
			 sb.append("AND TSD.ANSWER_CHOICE_ID = " + choiceId + " ");
				// ユーザー権限 機械公開制御
				if(typeFlg == 0 ) {
					// 0:SCM
					sb.append("");

				}else if(typeFlg == 1) {
					// 1:地区
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
					sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
					sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

				}else if(typeFlg == 2) {
					// 2:代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
					sb.append(") ");

				}else if(typeFlg == 3) {
					// 3:一般、レンタル
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 4) {
					// 4:サービス工場
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 21) {
					// 21:SCMSEA
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
					sb.append(") ");

				}else if(typeFlg == 31) {
					// 31:トルコ代理店
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
					sb.append(") ");

				}else if(typeFlg == 35) {
					// 35:トルコサブ代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
					sb.append(") ");

				}
				else {
					sb.append("AND ROWNUM <= 0 ");
				}

			 if(year != null) {
				 sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			 }
			 sb.append("AND ROWNUM <= 1001 ");
			 sb.append("ORDER BY TSD.SURVEY_RESULT_ID DESC ");

		 }else if(apiFlg == 1) {
			 sb.append("TSD2.ANSWER_FREEINPUT "); //0
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_DETAIL TSD2 ");
			 sb.append("WHERE ");
			 sb.append("TSD2.DELETE_FLG = 0 ");
			 sb.append("AND TSD2.SURVEY_RESULT_ID IN( ");
			 sb.append("SELECT ");
			 sb.append("TSD.SURVEY_RESULT_ID ");
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_RESULT TSR ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD ON TSR.SURVEY_RESULT_ID = TSD.SURVEY_RESULT_ID ");
			 sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			 sb.append("WHERE ");
			 sb.append("TSR.DELETE_FLG = 0 ");
			 sb.append("AND TSD.DELETE_FLG = 0 ");
			 sb.append("AND MM.DELETE_FLAG = 0 ");
			 sb.append("AND TSR.SURVEY_ID = 1 ");
			 sb.append("AND TSD.QUESTION_ID = " + questionId + " ");
			 sb.append("AND TSD.ANSWER_CHOICE_ID = " + choiceId + " ");
				// ユーザー権限 機械公開制御
				if(typeFlg == 0 ) {
					// 0:SCM
					sb.append("");

				}else if(typeFlg == 1) {
					// 1:地区
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
					sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
					sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

				}else if(typeFlg == 2) {
					// 2:代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
					sb.append(") ");

				}else if(typeFlg == 3) {
					// 3:一般、レンタル
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 4) {
					// 4:サービス工場
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 21) {
					// 21:SCMSEA
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
					sb.append(") ");

				}else if(typeFlg == 31) {
					// 31:トルコ代理店
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
					sb.append(") ");

				}else if(typeFlg == 35) {
					// 35:トルコサブ代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
					sb.append(") ");

				}
				else {
					sb.append("AND ROWNUM <= 0 ");
				}


			 if(year != null) {
				 sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			 }
			 if(choiceId == 3) {
				 sb.append(")AND TSD2.QUESTION_ID = 57 ");
			 }else if(choiceId == 4) {
				 sb.append(")AND TSD2.QUESTION_ID = 58 ");
			 }
			 sb.append("AND ROWNUM <= 1001 ");
			 sb.append("ORDER BY TSD2.SURVEY_RESULT_ID DESC ");

		 }else if(apiFlg == 3) {
			 sb.append("TSD1.ANSWER_FREEINPUT ");  //0
			 sb.append("FROM ");
			 sb.append("TBL_SURVEY_RESULT TSR ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD1 ON TSR.SURVEY_RESULT_ID = TSD1.SURVEY_RESULT_ID ");
			 sb.append("LEFT JOIN TBL_SURVEY_DETAIL TSD2 ON TSR.SURVEY_RESULT_ID = TSD2.SURVEY_RESULT_ID ");
			 sb.append("JOIN MST_MACHINE MM ON TSR.KIBAN_SERNO = MM.KIBAN_SERNO ");
			 sb.append("WHERE ");
			 sb.append("TSR.DELETE_FLG = 0 ");
			 sb.append("AND TSD1.DELETE_FLG = 0 ");
			 sb.append("AND TSD2.DELETE_FLG = 0 ");
			 sb.append("AND MM.DELETE_FLAG = 0 ");
			 sb.append("AND TSR.SURVEY_ID = 1 ");
			 sb.append("AND TSD1.QUESTION_ID = " + questionId + " ");
			 sb.append("AND TSD1.ANSWER_CHOICE_ID = " + choiceId + " ");
			 sb.append("AND TSD2.QUESTION_ID = 38 ");
			 sb.append("AND TSD2.ANSWER_CHOICE_ID = 2 ");
				// ユーザー権限 機械公開制御
				if(typeFlg == 0 ) {
					// 0:SCM
					sb.append("");

				}else if(typeFlg == 1) {
					// 1:地区
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
					sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
					sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
							+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");

				}else if(typeFlg == 2) {
					// 2:代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
					sb.append(") ");

				}else if(typeFlg == 3) {
					// 3:一般、レンタル
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 4) {
					// 4:サービス工場
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
					sb.append("OR ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
					sb.append(") ");

				}else if(typeFlg == 21) {
					// 21:SCMSEA
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
					sb.append(") ");

				}else if(typeFlg == 31) {
					// 31:トルコ代理店
					sb.append("AND ( ");
					sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
					sb.append(") ");

				}else if(typeFlg == 35) {
					// 35:トルコサブ代理店
					sb.append("AND ( ");
					sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
					sb.append("OR ");
					sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
					sb.append(") ");

				}
				else {
					sb.append("AND ROWNUM <= 0 ");
				}


			 if(year != null) {
				 sb.append("AND TO_CHAR(TSR.SURVEY_DATE,'YYYY') IN (" + year + ") ");
			 }
			 sb.append("AND ROWNUM <= 1001 ");
			 sb.append("ORDER BY TSD1.SURVEY_RESULT_ID DESC ");

		 }

		 em.getEntityManagerFactory().getCache().evictAll();
		List<String> rs = em.createNativeQuery(sb.toString()).getResultList();

		if (rs.size() > 0)
			return rs;
		else
			return null;
	 }
}
