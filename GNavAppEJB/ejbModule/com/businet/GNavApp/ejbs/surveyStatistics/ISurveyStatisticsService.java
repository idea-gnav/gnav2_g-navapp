package com.businet.GNavApp.ejbs.surveyStatistics;

import java.util.List;
public interface ISurveyStatisticsService{


//共有

	/**
	 * NativeQuery
	 * findSurveyTargetFigures : アンケート結果グラフのヘッダー情報取得
	 */
//	List<Object[]> findSurveyTargetFigures(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd);
	List<Object[]> findSurveyTargetFigures(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd
			                                    ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


	/**
	 * NativeQuery
	 * choiceCountSum : 対象設問回答総数取得
	 * questionNo 60,61:自社或いは他社で整備する理由
	 *            56:定期検査・整備
	 *            62:購入時の重要指数
	 *            63:サービスに今後期待すること
	 */
//	 List<Integer> choiceCountSum(String year,String questionNo);
	List<Integer> choiceCountSum(String year,String questionNo,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//36.surveyStatisticsList(アンケート統計一覧)

	/**
	 * NativeQuery
	 * findSurveyStatistics : アンケート統計一覧取得
	 */

	//List<Object[]> findSurveyStatistics(Integer sortFlg ,Integer deviceLanguage);
	 List<Object[]> findSurveyStatistics(Integer sortFlg ,Integer deviceLanguage ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//37.serviceCoverHrChart(サービスカバーHR分布図)

	/**
	 * NativeQuery
	 * findServiceCoverHrChart : サービスカバーHR分布図取得
	 */

//	List<Object[]> findServiceCoverHrChart(Integer surveyId ,String year);
	 List<Object[]> findServiceCoverHrChart(Integer surveyId ,String year,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//38.serviceBestBland(サービスベストブランド)

	/**
	 * NativeQuery
	 * findServiceBestBland : サービスベストブランド取得
	 */

//	List<Object[]> findServiceBestBland(Integer surveyId ,Integer deviceLanguage ,String year);
	 List<Object[]> findServiceBestBland(Integer surveyId ,Integer deviceLanguage ,String year
			 ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//39.evaluationItem(評価項目)

	/**
	 * NativeQuery
	 * findCompetitorList : 評価項目 競合他社取得
	 */
	List<Object[]> findCompetitorList(Integer deviceLanguage);

	/**
	 * NativeQuery
	 * findEvaluationLvText : 評価レベルテキスト取得
	 */
	String findEvaluationLvText(Integer deviceLanguage);

	/**
	 * NativeQuery
	 * findSumitomoCount : 住友データ件数取得
	 */
//	List<Integer> findSumitomoCount(Integer surveyId ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd);
	List<Integer> findSumitomoCount(Integer surveyId ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd
			,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);

	/**
	 * NativeQuery
	 * findChartList : 評価項目 レーダーチャート出力用データ取得
	 */
//	List<Object[]> findChartList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,Integer sumitomoCount);
	List<Object[]> findChartList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,Integer sumitomoCount
		                                 	,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);

	/**
	 * NativeQuery
	 * findChoiceList : 評価項目 回答選択率取得
	 */
//	List<Object[]> findChoiceList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,String strs[]);
	List<Object[]> findChoiceList(Integer surveyId ,Integer deviceLanguage ,String year ,String katashiki ,String model ,String dealerCd ,String applicableUsesCd ,String strs[]
			                            ,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg );


//40.serviceEvaluationPriority(サービス項目への評価平均と優先度)

	/**
	 * NativeQuery
	 * findEvaluationPrioritys : サービス項目への評価平均と優先度取得
	 *
	 */
//	List<Object[]> findEvaluationPrioritys(Integer deviceLanguage,String year);
	List<Object[]> findEvaluationPrioritys(Integer deviceLanguage,String year,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg );


//41.maintenanceReason(自社或いは他社で整備する理由)

	/**
	 * NativeQuery
	 * findMaintenanceReason : 自社或いは他社で整備する理由取得
	 */
//	 List<Object[]> findMaintenanceReason(Integer deviceLanguage,String year,Integer choiceCount);
	List<Object[]> findMaintenanceReason(Integer deviceLanguage,String year,Integer choiceCount,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);

	 /**
	 * NativeQuery
	 * findCompetitorAnswerCount : 自社或いは他社で整備する理由 選択肢別　競合他社回答数取得
	 */
//	 List<Object[]> findCompetitorAnswerCount(Integer deviceLanguage,String year,Integer choiceId);
	List<Object[]> findCompetitorAnswerCount(Integer deviceLanguage,String year,Integer choiceId,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//42.periodicInspection(定期検査・整備)

	 /**
	 * NativeQuery
	 * findPeriodicInspection : 定期検査・整備取得
	 */
//	 List<Object[]> findPeriodicInspection(Integer deviceLanguage,String year,Integer choiceCount);
	List<Object[]> findPeriodicInspection(Integer deviceLanguage,String year,Integer choiceCount,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//43.importanceInPurchasing(購入時の重要指数)

	/**
	 * NativeQuery
	 * findByImportanceInPurchasingList : 「購入時の重要指数」詳細取得
	 */
//	List<Object[]> findByImportanceInPurchasingList(Integer devicelanguage , Integer countAll, String year);
	List<Object[]> findByImportanceInPurchasingList(Integer devicelanguage , Integer countAll, String year,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//44.expectedService(サービスに今後期待すること)

	/**
	 * NativeQuery
	 * findByExpectedServiceList : 「サービスに今後期待すること」詳細取得
	 */
//	List<Object[]> findByExpectedService(Integer devicelanguage , Integer countAll, String year);
	List<Object[]> findByExpectedService(Integer devicelanguage , Integer countAll, String year,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


//45.surveyFreeInputList(フリー入力一覧)

	/**
	 * NativeQuery
	 * findDispQuestionNo : 表示設問番号取得
	 */
	 List<Integer> findDispQuestionNo(String questionId);

	/**
	 * NativeQuery
	 * findQuestion : 設問取得
	 */
	 List<String> findQuestion(String questionId,Integer deviceLanguage);

	/**
	 * NativeQuery
	 * findFreeInput : フリー入力一覧取得
	 */
//	 List<String> findFreeInput(Integer apiFlg,String questionId,Integer choiceId,String year);
	 List<String> findFreeInput(Integer apiFlg,String questionId,Integer choiceId,String year,String sosikiCd ,String kigyouCd ,String groupNo ,Integer typeFlg);


}
