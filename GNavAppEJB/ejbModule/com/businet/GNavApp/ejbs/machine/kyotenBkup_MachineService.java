package com.businet.GNavApp.ejbs.machine;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.businet.GNavApp.ConvertUtil;
import com.businet.GNavApp.entities.MstAppMachineTypeControl;
import com.businet.GNavApp.entities.MstMachine;


@Stateless
@Local(kyotenBkup_IMachineService.class)
public class kyotenBkup_MachineService implements kyotenBkup_IMachineService {

	private static final Logger logger = Logger.getLogger(kyotenBkup_MachineService.class.getName());

	@PersistenceContext(unitName="GNavApp")//2020.08.04 Rasis Duc modified
    EntityManager em;


	/**
	 * JPA
	 * findByMachinePosition: 所在地取得
	 */
	@Override
	public String findByMachinePosition(Long serialNumber, String conType, String languageCd) {


		em.getEntityManagerFactory().getCache().evictAll();

		Object result = null;

		if(conType!=null) {
			if(conType.equals("C") || conType.equals("C2") ) {
				result = em.createQuery("SELECT m.positionEn FROM MstMachine m WHERE m.kibanSerno=:kibanSerno")
						.setParameter("kibanSerno", serialNumber)
						.getSingleResult();

			}else if(languageCd.toLowerCase().equals("en") ) {
				result = em.createQuery("SELECT m.positionEn FROM MstMachine m WHERE m.kibanSerno=:kibanSerno")
						.setParameter("kibanSerno", serialNumber)
						.getSingleResult();
				if(result==null) {
					result = em.createQuery("SELECT m.position FROM MstMachine m WHERE m.kibanSerno=:kibanSerno")
							.setParameter("kibanSerno", serialNumber)
							.getSingleResult();
				}

			}else {
				result = em.createQuery("SELECT m.position FROM MstMachine m WHERE m.kibanSerno=:kibanSerno")
						.setParameter("kibanSerno", serialNumber)
						.getSingleResult();
				if(result==null) {
					result = em.createQuery("SELECT m.positionEn FROM MstMachine m WHERE m.kibanSerno=:kibanSerno")
							.setParameter("kibanSerno", serialNumber)
							.getSingleResult();
				}

			}
		}


		if(result!=null)
			return result.toString();
		else
			return null;
	}





	/**
	 * JPA
	 * fingByDisplayContition: 機械型式別グループ取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MstAppMachineTypeControl findByMachineTypeControl(String conType, Integer machineModel) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<MstAppMachineTypeControl> result = em.createQuery("SELECT m FROM MstAppMachineTypeControl m "
				+ "WHERE m.delFlg=0 AND m.conType=:conType AND m.machineModel = :machineModel ")
				.setParameter("conType", conType)
				.setParameter("machineModel", machineModel)
				.getResultList();

		em.clear();

		if(result.size() > 0)
			return (MstAppMachineTypeControl)result.get(0);
		else
			return null;
	}





	/**
	 * JPA
	 * findByMachine: 機械マスタ情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MstMachine findByMachine(Long serialNumber) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<MstMachine> result = em.createQuery("SELECT m FROM MstMachine m WHERE m.kibanSerno=:param ")
				.setParameter("param", serialNumber)
				.getResultList();

		if(result.size() > 0)
			return (MstMachine)result.get(0);
		else
			return null;
	}



	/**
	 * JPA
	 * findByMachineSosiki: 機械所有組織取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByMachineSosiki(Long serialNumber) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("mm.sosikiCd, ");		// 0
		sb.append("ms.kengenCd, ");		// 1
		sb.append("mk.kigyouName, ");	// 2
		sb.append("ms.sosikiName, ");	// 3
		sb.append("mm.dairitennCd ");	// 4
		sb.append("FROM MstMachine mm ");
		sb.append("LEFT JOIN mm.mstSosiki ms ");
		sb.append("LEFT JOIN ms.mstKigyou mk ");
		sb.append("WHERE mm.kibanSerno = :kibanSerno");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result = em.createQuery(new String(sb))
				.setParameter("kibanSerno", serialNumber)
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * NativeQuery
	 * findByMachineList: 機械一覧取得
	 */
	public List<Object[]> findByMachineListNativeQuery(
			String userId,
			Integer kibanSelect,
			// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
			Integer customerNamePriority,
			// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//
			String kibanInput,
			Integer searchFlg,
			Integer sortFlg,
			String customerManagementNoFrom,
			String customerManagementNoTo,
			String manufacturerSerialNumberFrom,
			String manufacturerSerialNumberTo,
			String machineType,
			String scmModel,
			String customerName,
			Integer warningCurrentlyInProgress,
			String warningDateFrom,
			String warningDateTo,
			String dtcCode,
			Integer hourMeterFrom,
			Integer hourMeterTo,
//			String dealerName,
			String dealerCd,
			Integer fuelLevel,
			Integer defLevel,
			// **↓↓↓　2019/04/04  iDEA山下 定期整備検索追加 ↓↓↓**//
			Integer periodicServiceNotice,
			Integer periodicServiceNowDue,
			String replacementPart,
			// **↑↑↑　2019/04/04  iDEA山下 定期整備検索追加 ↑↑↑**//
			Integer favoriteSearchFlg,
			String toukatsubuCd,
			String kyotenCd,
			String serviceKoujyouCd,
			Integer typeFlg,
			String kigyouCd,
			String sosikiCd,
			String groupNo,
			String kengenCd,
			Double searchRangeFromIdo,
			Double searchRangeFromKeido,
			Integer searchRangeDistance,
			// **↓↓↓　2019/06/25  iDEA山下 アンケート検索項目追加 ↓↓↓**//
			Integer surveyInput,
			Integer surveySearchFlg,
			// **↑↑↑　2019/06/25  iDEA山下 アンケート検索項目追加 ↑↑↑**//
			Integer searchRangeUnit,
			String languageCd,
			// **↓↓↓　2019/09/18  RASIS岡本 アンケート入力日による検索条件追加↓↓↓**//
			String inputDateFrom,
			String inputDateTo,
			// **↑↑↑　2019/09/18  RASIS岡本 アンケート入力日による検索条件追加↑↑↑**//
			// **↓↓↓　2019/10/31  RASIS岡本 施工情報検索項目追加↓↓↓**//
			Integer constructionInput,
			Integer constructionSearchFlg
			// **↑↑↑　2019/10/31  RASIS岡本 施工情報検索項目追加↑↑↑**//
		){

		int dateSearchFlg = 0;

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MM.KIBAN_SERNO, ");										// 0
		sb.append("MM.KIBAN, ");											// 1
		sb.append("MM.LBX_KIBAN, ");										// 2
		sb.append("MM.USER_KANRI_NO, ");									// 3
		sb.append("CONCAT(MK.KIGYOU_NAME || ' ', MS.SOSIKI_NAME), ");		// 4
		sb.append("MM.MODEL_CD, ");											// 5
//		sb.append("MM.LBX_MODEL_CD, ");										// 6
		sb.append("MM.POSITION_EN, ");										// 6
		sb.append("MM.POSITION, ");											// 7
		sb.append("MM.NEW_IDO, ");											// 8
		sb.append("MM.NEW_KEIDO, ");										// 9
		sb.append("NVL(MM.NEW_HOUR_METER, 0.0) AS HOUR_METER, ");			// 10
		sb.append("MM.NENRYO_LV, ");										// 11
		sb.append("MM.UREA_WATER_LEVEL, ");									// 12
		sb.append("MM.RECV_TIMESTAMP, ");									// 13
		sb.append("MM.CON_TYPE, ");											// 14 machineGroup
		sb.append("MM.MACHINE_MODEL, ");									// 15 machineGroup
		// **↓↓↓　2019/04/04  iDEA山下 定期整備検索追加 ↓↓↓**//
		sb.append("CASE ");
		sb.append("WHEN MM.KEYIKOKU_COUNT > 0 THEN 2 ");
		sb.append("WHEN MM.YOKOKU_COUNT > 0 THEN 1 ");
		sb.append("ELSE 0 ");
		sb.append("END AS PERIODIC_SERVICE_ICON_TYPE ");					// 16
		// **↑↑↑　2019/04/04  iDEA山下 定期整備検索追加 ↑↑↑**//
		// **↓↓↓　2019/06/25  iDEA山下 アンケート検索項目追加 ↓↓↓**//
		if(surveySearchFlg!=null) {
			sb.append(",TSR.SURVAYFLG ");									// 17
		}
		// **↑↑　2019/06/25  iDEA山下 アンケート検索項目追加 ↑↑↑**↑//
		// **↓↓↓　2019/10/31  RASIS岡本 施工情報検索項目追加↓↓↓**//
		if(constructionSearchFlg!=null) {
			sb.append(",TSIR.CONSTRUCTIONFLG ");							// 18
		}
		// **↑↑↑　2019/10/31  RASIS岡本 施工情報検索項目追加↑↑↑**//


		sb.append("FROM MST_MACHINE MM ");
		sb.append("LEFT JOIN MST_SOSIKI MS ");
		sb.append("ON MM.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");

		// **↓↓↓　2019/06/25  iDEA山下 アンケート検索項目追加 ↓↓↓**//
		if(surveySearchFlg!=null) {
			sb.append("LEFT JOIN( ");
			sb.append("SELECT TS.KIBAN_SERNO, ");
			sb.append("CASE WHEN COUNT(TS.KIBAN_SERNO) > 0 THEN 1 ");
			sb.append("ELSE 0 END AS SURVAYFLG ");
			sb.append("FROM TBL_SURVEY_RESULT TS ");
			sb.append("JOIN MST_SURVEY MS ON TS.SURVEY_ID = MS.SURVEY_ID ");
			sb.append("WHERE TS.DELETE_FLG = 0 ");
			sb.append("AND MS.DELETE_FLG = 0 ");
			// **↓↓↓　2019/09/18  RASIS岡本 アンケート入力日による検索条件追加↓↓↓**//
			if(inputDateFrom!=null) {
				sb.append("AND TS.SURVEY_DATE >= '" + inputDateFrom + "' ");
				dateSearchFlg = 1;
			}
			if(inputDateTo!=null) {
				sb.append("AND TS.SURVEY_DATE <= '" + inputDateTo + "' ");
				dateSearchFlg = 1;
			}
			// **↑↑↑　2019/09/18  RASIS岡本 アンケート入力日による検索条件追加↑↑↑**//
			sb.append("GROUP BY TS.KIBAN_SERNO )TSR ");
			sb.append("ON MM.KIBAN_SERNO = TSR.KIBAN_SERNO ");
		}
		// **↑↑　2019/06/25  iDEA山下 アンケート検索項目追加 ↑↑↑**↑//


		// **↓↓↓　2019/10/31  RASIS岡本 施工情報検索項目追加↓↓↓**//
		if(constructionSearchFlg != null) {
			sb.append("LEFT JOIN( ");
			sb.append("SELECT TSI.KIBAN_SERNO, ");
			sb.append("CASE WHEN COUNT(TSI.KIBAN_SERNO) > 0 THEN 1 ");
			sb.append("ELSE 0 END AS CONSTRUCTIONFLG ");
			sb.append("FROM TBL_SEKO_INFO TSI ");
			sb.append("WHERE TSI.DELETE_FLAG = 0 ");
			sb.append("GROUP BY TSI.KIBAN_SERNO )TSIR ");
			sb.append("ON MM.KIBAN_SERNO = TSIR.KIBAN_SERNO ");
		}
		// **↑↑↑　2019/10/31  RASIS岡本 施工情報検索項目追加↑↑↑**//


		sb.append("WHERE MM.DELETE_FLAG = 0 ");
//		sb.append("AND MM.CON_TYPE <> 'S' ");	5型のSは対象外




		// ユーザー権限 機械公開制御
		if(typeFlg == 0 ) {
			// 0:SCM
			sb.append("");

		}else if(typeFlg == 1) {
			// 1:地区
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (GROUP_NO = '"+groupNo+"')) ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN ( ");
			sb.append("SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ( ");
			sb.append("SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"'))) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"' ) ");
			// **↓↓↓ 2019/10/15  iDEA山下 権限制御　Web版仕様踏襲 ↓↓↓**//
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE DAIRITENN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD IN (SELECT KIGYOU_CD FROM MST_SOSIKI WHERE KYOTEN_CD IN ("
					+ "SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"')))))) ");
			// **↑↑↑ 2019/10/15  iDEA山下 権限制御　Web版仕様踏襲 ↑↑↑**//


		}else if(typeFlg == 2) {
			// 2:代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD = '"+sosikiCd+"') ");
			sb.append("OR ");
			// **↓↓↓ 2019/09/18  iDEA山下 Web版仕様変更に伴い、代理店権限ユーザの検索制御を変更↓↓↓**//
//			sb.append("MM.SOSIKI_CD IN ( SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DAIRITENN_CD = '"+sosikiCd+"') ");
			sb.append("MM.DAIRITENN_CD  = '"+sosikiCd+"' ");
			// **↑↑↑ 2019/09/18  iDEA山下 Web版仕様変更に伴い、代理店権限ユーザの検索制御を変更 ↑↑↑**//
			sb.append(") ");

		}else if(typeFlg == 3) {
			// 3:一般、レンタル
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 4) {
			// 4:サービス工場
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"+kigyouCd+"' OR GROUP_NO = '"+groupNo+"' OR SOSIKI_CD = '"+sosikiCd+"')) ");
			sb.append("OR ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SERVICE_KOUJOU_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KIGYOU_CD = '"+kigyouCd+"') ");
			sb.append(") ");

		}else if(typeFlg == 21) {
			// 21:SCMSEA
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (201,205,207,208)) ");
			sb.append(") ");

		}else if(typeFlg == 31) {
			// 31:トルコ代理店
			sb.append("AND ( ");
			sb.append("MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"+sosikiCd+"') ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE KENGEN_CD IN (301,305,307)) ");
			sb.append(") ");

		}else if(typeFlg == 35) {
			// 35:トルコサブ代理店
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_TR = '"+sosikiCd+"' ");
			sb.append("OR ");
			sb.append("MM.SOSIKI_CD = '"+sosikiCd+"' ");
			sb.append(") ");

		}
		else {
			sb.append("AND ROWNUM <= 0 ");
		}



		// 通常検索 : 機械選択区分
		if( kibanInput != null) {
			// **↓↓↓ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↓↓↓**//
//			if(kibanSelect != 3) {
			//お客様名検索を優先しない場合
			if( customerNamePriority == 0) {
				if(kibanSelect == 0)
					sb.append("AND NLS_UPPER(MM.KIBAN) ");
				if(kibanSelect == 1)
					sb.append("AND NLS_UPPER(MM.LBX_KIBAN) ");
				if(kibanSelect == 2)
					sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) ");

				if(searchFlg == 0)
					sb.append("LIKE NLS_UPPER('%" + ConvertUtil.rpStr(kibanInput) + "%') ");
				if(searchFlg == 1)
					sb.append("LIKE NLS_UPPER('" + ConvertUtil.rpStr(kibanInput) + "%') ");
				if(searchFlg == 2)
					sb.append("LIKE NLS_UPPER('%" + ConvertUtil.rpStr(kibanInput) + "') ");
				if(searchFlg == 3)
					sb.append("= NLS_UPPER('" + ConvertUtil.rpStr(kibanInput) + "') ");
			// **↓↓↓ 2019/10/16  RASIS岡本 お客様名検索対応↓↓↓**//
			//お客様名検索を優先する場合
			}else{
				sb.append("AND EXISTS ( ");
				sb.append("SELECT * FROM MST_MACHINE MMM ");
				sb.append("JOIN MST_SOSIKI MS ON  MMM.SOSIKI_CD = MS.SOSIKI_CD ");
				sb.append("JOIN MST_KIGYOU MK ON  MS.KIGYOU_CD = MK.KIGYOU_CD ");
				sb.append("AND ( NLS_UPPER(REGEXP_REPLACE(MK.KIGYOU_NAME || MS.SOSIKI_NAME,' ','')) ");
				if(searchFlg == 0)
					sb.append("LIKE NLS_UPPER(REGEXP_REPLACE('%" + ConvertUtil.rpStr(kibanInput) + "%',' ','')) ) ");
				if(searchFlg == 1)
					sb.append("LIKE NLS_UPPER(REGEXP_REPLACE('" + ConvertUtil.rpStr(kibanInput) + "%',' ','')) ) ");
				if(searchFlg == 2)
					sb.append("LIKE NLS_UPPER(REGEXP_REPLACE('%" + ConvertUtil.rpStr(kibanInput) + "',' ','')) ) ");
				if(searchFlg == 3)
					sb.append("= NLS_UPPER(REGEXP_REPLACE('" + ConvertUtil.rpStr(kibanInput) + "',' ','')) ) ");
				sb.append("WHERE MM.KIBAN_SERNO = MMM.KIBAN_SERNO ");
				sb.append(") ");
			}
			// **↑↑↑ 2019/10/16  RASIS岡本 お客様名検索対応↑↑↑**//
			// **↑↑↑ 2019/11/28 iDEA山下 お客様名検索優先フラグ追加 ↑↑↑**//
		}





		// 詳細検索 : 機番
		// 11,12.お客様管理番号
		if( customerManagementNoFrom != null && customerManagementNoTo == null)
			sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStr(customerManagementNoFrom)) + "') ");
		if( customerManagementNoFrom == null && customerManagementNoTo != null)
			sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStr(customerManagementNoTo)) + "') ");

		if( customerManagementNoFrom != null && customerManagementNoTo != null)
			sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) BETWEEN NLS_UPPER('" + ConvertUtil.rpStr(customerManagementNoFrom) + "') AND NLS_UPPER('" + ConvertUtil.rpStr(customerManagementNoTo) + "') ");


		// 13,14.SCM機番
		if( manufacturerSerialNumberFrom != null && manufacturerSerialNumberTo == null)
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStrKiban(manufacturerSerialNumberFrom)) + "') ");
		if( manufacturerSerialNumberFrom == null && manufacturerSerialNumberTo != null)
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStrKiban(manufacturerSerialNumberTo)) + "') ");

		if( manufacturerSerialNumberFrom != null && manufacturerSerialNumberTo != null)
			sb.append("AND NLS_UPPER(MM.KIBAN) BETWEEN NLS_UPPER('" + ConvertUtil.rpStr(manufacturerSerialNumberFrom) + "') AND NLS_UPPER('" + ConvertUtil.rpStr(manufacturerSerialNumberTo) + "') ");


		// 15.機械種別
		// 0:ショベル, 1:道路	複数の場合「,(カンマ)」区切り
		if(machineType!=null) {
			String[] typeList = machineType.split(",");

				if(typeList.length==1 && (typeList[0].equals("0") || typeList[0]=="0")) {
					sb.append("AND MM.CON_TYPE NOT IN ('D','ND') ");
				}
				if(typeList.length==1 && (typeList[0].equals("1") || typeList[0]=="1")) {
					sb.append("AND MM.CON_TYPE IN ('D','ND') ");
				}
		}



		// 16. SCM型式
		if(scmModel!=null)
			sb.append("AND MM.MODEL_CD IN (" + ConvertUtil.queryInStr(scmModel) + ") ");


		// 17.お客様名
		if(customerName!=null && customerName.length()>0) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT * FROM MST_MACHINE MMM ");

			sb.append("JOIN MST_SOSIKI MS ON  MMM.SOSIKI_CD = MS.SOSIKI_CD ");
			sb.append("JOIN MST_KIGYOU MK ON  MS.KIGYOU_CD = MK.KIGYOU_CD ");
//			sb.append("AND ( NLS_UPPER(REGEXP_REPLACE(MK.KIGYOU_NAME || MS.SOSIKI_NAME,' ','')) LIKE NLS_UPPER(REGEXP_REPLACE('%" + ConvertUtil.rpStr(customerName)) + "%',' ','') ) ");
			sb.append("AND ( NLS_UPPER(REGEXP_REPLACE(MK.KIGYOU_NAME || MS.SOSIKI_NAME,' ','')) LIKE NLS_UPPER(REGEXP_REPLACE('" + ConvertUtil.searchFuzzyStr(ConvertUtil.rpStr(customerName)) + "',' ','')) ) ");
			sb.append("WHERE MM.KIBAN_SERNO = MMM.KIBAN_SERNO ");
			sb.append(") ");
		}


		// 18,19,20. DTC関連
		if( warningCurrentlyInProgress != 0 || warningDateFrom != null || warningDateTo != null || dtcCode!=null) {
			sb.append("AND EXISTS (");
			sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
			sb.append("INNER JOIN MST_MACHINE MM2 ON TES.KIBAN_SERNO = MM2.KIBAN_SERNO ");

			sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
			// **↓↓↓ 2020/09/29 iDEA Yamashita　結合条件の修正 ↓↓↓**//
//			sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
			sb.append("ON ( ");
			sb.append("TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE ");
			sb.append("OR (( MM2.CON_TYPE != 'D' OR MM2.EISEI_FLG != 1) AND NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE) )) ");
			// **↑↑↑ 2020/09/29 iDEA Yamashita　結合条件の修正 ↑↑↑**//
			sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
			sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
			sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
			sb.append("AND TK.KENGEN_CD = "+kengenCd+" ");

//			sb.append("JOIN MST_EVENT_SEND MES ");
			sb.append("LEFT JOIN ( ");
			sb.append("SELECT DISTINCT EVENT_CODE, SHUBETU_CODE, CON_TYPE, MACHINE_KBN, DELETE_FLAG, FLG_VISIBLE ");
			sb.append("FROM MST_EVENT_SEND ");
			sb.append("WHERE DELETE_FLAG = 0 ");
			sb.append(") MES ");
			sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
			sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
			sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
			sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

			// **↓↓↓ 2020/09/29 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↓↓↓**//
//			sb.append("WHERE TES.SYORI_FLG <> 9 ");
			sb.append("LEFT JOIN MST_KENGEN MK ON TK.KENGEN_CD = MK.KENGEN_CD ");
			sb.append("WHERE ");
			sb.append("( ( MK.KEIHOU_KENGEN != 1 AND TES.SYORI_FLG != 9 ) ");
			sb.append("OR ( MK.KEIHOU_KENGEN = 1 ) ) ");
			// **↑↑↑ 2020/09/29 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↑↑↑**//
			sb.append("AND ((TES.EVENT_SHUBETU = 8 AND TES.CON_TYPE IN ('C','C2')) ");
			sb.append("OR (TES.EVENT_SHUBETU = 3 AND TES.CON_TYPE IN ('T2', 'T', 'S', 'D', 'N', 'NH', 'ND'))) ");

			// 18.DTC発生中
			if( warningCurrentlyInProgress != 0 ) {
			sb.append("AND TES.FUKKYU_TIMESTAMP_LOCAL IS NULL ");
			}

			// 19,20.DTC発生日
			if( warningDateFrom != null && warningDateTo == null) {
				sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL  ");
				sb.append(">= TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			}

			if( warningDateFrom != null && warningDateTo != null) {
				sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL ");
				sb.append("BETWEEN TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
				sb.append("AND TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			}

			if( warningDateFrom == null && warningDateTo != null) {
					sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL  ");
							sb.append("<= TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			}

			// DTCコード
			if(dtcCode!=null ) {
				if(ConvertUtil.isNumber(dtcCode))
					sb.append("AND (MES.FLG_VISIBLE LIKE '%" + dtcCode + "%' OR MES.EVENT_CODE = " + dtcCode + ") ");
				if(!ConvertUtil.isNumber(dtcCode))
					sb.append("AND MES.FLG_VISIBLE LIKE '%" + ConvertUtil.rpStr(dtcCode).toUpperCase() + "%' ");
			}

			sb.append("AND MM.KIBAN_SERNO = TES.KIBAN_SERNO ");
			sb.append(") ");
		}

		// 21.アワメータ
		if(hourMeterFrom!=null)
			sb.append("AND FLOOR(MM.NEW_HOUR_METER/60*10) /10 >= "+hourMeterFrom+" ");
		if(hourMeterTo!=null)
			sb.append("AND FLOOR(MM.NEW_HOUR_METER/60*10) /10 <="+hourMeterTo+" ");


		// 22.代理店コード
		if(dealerCd!=null && dealerCd.length()>0) {
			// **↓↓↓ 2019/10/15  iDEA山下 代理店検索　Web版仕様踏襲 ↓↓↓**//
//			Object lbxFlg = em.createQuery("SELECT s.lbxFlg FROM MstSosiki s WHERE s.sosikiCd = :sosikiCd ")
//					.setParameter("sosikiCd", dealerCd)
//					.getSingleResult();
//			if(lbxFlg!=null) {
//				if(Integer.parseInt(lbxFlg.toString()) == 1 ) {
//					sb.append("AND MM.DAIRITENN_CD LIKE NLS_UPPER('%" + ConvertUtil.rpStr(dealerCd) + "%') ");
//				}else {
//					sb.append("AND MM.SOSIKI_CD IN ( SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DAIRITENN_CD LIKE NLS_UPPER('%" + ConvertUtil.rpStr(dealerCd) + "%')) ");
//				}
//			}
//			em.clear();
			sb.append("AND NVL(MM.DAIRITENN_CD, MS.DAIRITENN_CD) = '" + dealerCd + "' ");
			// **↑↑↑ 2019/10/15  iDEA山下 代理店検索　Web版仕様踏襲 ↑↑↑**//
		}


		// 23.燃料レベル
		if(fuelLevel!=null)
			sb.append("AND MM.NENRYO_LV <= "+fuelLevel+" ");


		// 24.尿素水レベル
		if(defLevel!=null)
			sb.append("AND MM.UREA_WATER_LEVEL <= "+defLevel+" ");


		// 25.お気に入り
		if(favoriteSearchFlg != 0)
			sb.append("AND EXISTS (SELECT * FROM TBL_USER_FAVORITE TUF WHERE DEL_FLG = 0 AND MM.KIBAN_SERNO = TUF.KIBAN_SERNO AND USER_ID = '"  + userId + "') ");


		// 日本独自項目
		// 27. 統括部コード
		if(toukatsubuCd != null && toukatsubuCd.length()>0) {
			sb.append("AND MM.KYOTEN_CD IN (SELECT KYOTEN_CD FROM MST_TOUKATSUBU WHERE TOUKATSUBU_CD = "+toukatsubuCd+") ");
		}

		// 28. 拠点コード
		if(kyotenCd != null && kyotenCd.length()>0) {
			sb.append("AND MM.KYOTEN_CD = " +kyotenCd+" ");
		}

		// 29. サービス工場コード
		if(serviceKoujyouCd != null && serviceKoujyouCd.length()>0) {
			sb.append("AND MM.SERVICE_KOUJOU_CD = '" + serviceKoujyouCd + "' ");
		}



		// 半径圏内の機械を抽出
		if(searchRangeFromIdo!=null && searchRangeFromKeido!=null) {
			sb.append("AND EXISTS (");
			sb.append("SELECT * FROM ( ");
			sb.append("SELECT ");
			sb.append("KIBAN_SERNO, ");
			sb.append("( 6378.137 * ACOS( ROUND( ");
			sb.append("SIN("+searchRangeFromIdo+" * ATAN2(0, -1) / 180) * SIN(NEW_IDO/3600/1000 * ATAN2(0, -1) / 180) ");
			sb.append("+ COS("+searchRangeFromIdo+" * ATAN2(0, -1) / 180)*COS(NEW_IDO/3600/1000 * ATAN2(0, -1) / 180) ");
			sb.append("* COS(ABS((NEW_KEIDO/3600/1000)-("+searchRangeFromKeido+")) * ATAN2(0, -1) / 180) ");
			sb.append(", 20) ) ) as DISTANCE ");
			sb.append("FROM MST_MACHINE ");
			sb.append("WHERE DELETE_FLAG = 0 ");
			sb.append("AND NEW_IDO IS NOT NULL ");
			sb.append("AND NEW_KEIDO IS NOT NULL ");
//			sb.append("AND KIBAN LIKE 'LBX%' ");
			sb.append(") SUB_MM ");
			sb.append("WHERE SUB_MM.DISTANCE <= "+searchRangeDistance+" ");

//			if(searchRangeUnit==0)	// USA mi
//				sb.append("WHERE SUB_MM.DISTANCE <= ("+searchRangeDistance+"*1.60934) ");
//			else if(searchRangeUnit==1)	// ISO km
//				sb.append("WHERE SUB_MM.DISTANCE <= "+searchRangeDistance+" ");

			sb.append("AND MM.KIBAN_SERNO = SUB_MM.KIBAN_SERNO) ");

		}
		// 2019/04/10 DucNKT 定期整備画面に追加されました：開始
				if(periodicServiceNotice!=null || periodicServiceNowDue!=null) {
					if(replacementPart!=null) {
						//↓↓↓ 2019/05/22 iDEA山下 サプライモジュールフィルタ(en)注釈置換追加  :start  ↓↓↓//
						if(languageCd == "en") {
							replacementPart = replacementPart.replace("SH-7", "X4");
						}
						//↑↑↑ 2019/05/22 iDEA山下 サプライモジュールフィルタ(en)注釈置換追加 :end  ↑↑↑//

						sb.append("AND EXISTS( ");
						sb.append("SELECT TES.* ");
						sb.append("FROM TBL_EVENT_SEND TES ");
						sb.append("INNER JOIN MST_MAINTAINECE_PART MMP ");
						sb.append("ON TES.MT_BUHIN_NO = MMP.MAINTAINECE_PART_NUMBER ");
						sb.append("AND TES.CON_TYPE = MMP.CON_TYPE ");
						//↓↓↓ 2019/05/10 iDEA山下　結合条件追加 :start ↓↓↓
						sb.append("AND TES.MACHINE_KBN = MMP.MACHINE_MODEL ");
						//↑↑↑ 2019/05/10 iDEA山下　結合条件追加 :end ↑↑↑
						sb.append("AND MMP.LANGUAGE_CD = '" + languageCd + "' ");
						sb.append("AND MMP.MAINTAINECE_PART_NAME IN (" + ConvertUtil.queryInStr(replacementPart) + ") ");
						sb.append("WHERE TES.EVENT_SHUBETU = 6 ");
//						sb.append("AND TES.EVENT_NO != 2 ");
//						sb.append("AND TES.EVENT_NO NOT IN (2,3) ");
						sb.append("AND TES.EVENT_NO IN ");
						if(periodicServiceNotice==1 && periodicServiceNowDue==1){
							sb.append("(0,1) ");
						}else if(periodicServiceNotice==1 && periodicServiceNowDue==0) {
							sb.append("(0) ");
						}else if(periodicServiceNotice==0 && periodicServiceNowDue==1) {
							sb.append("(1) ");
						}
						sb.append("AND TES.REGIST_DTM =( ");
						sb.append("SELECT MAX(TES2.REGIST_DTM) ");
						sb.append("FROM TBL_EVENT_SEND TES2 ");
						sb.append("WHERE TES.EVENT_SHUBETU =TES2.EVENT_SHUBETU ");
						sb.append("AND TES.MT_BUHIN_NO = TES2.MT_BUHIN_NO ");
						sb.append("AND TES.KIBAN_SERNO = TES2.KIBAN_SERNO ");
						sb.append("GROUP BY TES2.KIBAN_SERNO ) ");
						sb.append("AND MM.KIBAN_SERNO = TES.KIBAN_SERNO ");
						sb.append("AND ROWNUM <= 1 ) ");
					}else {
						if(periodicServiceNotice == 1 && periodicServiceNowDue == 1 ) {
							sb.append("AND ( MM.YOKOKU_COUNT > 0 OR MM.KEYIKOKU_COUNT > 0) ");
						}else if(periodicServiceNotice == 1 && periodicServiceNowDue == 0){
							sb.append("AND MM.YOKOKU_COUNT > 0 ");
					    }else if(periodicServiceNowDue>0) {
							sb.append("AND MM.KEYIKOKU_COUNT > 0 ");
						}
					}
				}
				// 2019/04/10 DucNKT 定期整備画面に追加されました：終了

		// **↓↓↓　2019/06/25  iDEA山下 アンケート検索項目追加 ↓↓↓**//
		if(surveySearchFlg!=null && surveyInput ==1 ) {
			sb.append("AND TSR.SURVAYFLG =1 ");
		}
		// **↑↑　2019/06/25  iDEA山下 アンケート検索項目追加 ↑↑↑**↑//
		// **↓↓↓　2019/09/25  RASIS岡本 アンケート入力日による検索条件追加↓↓↓**//
		if(surveySearchFlg!=null && dateSearchFlg ==1 ) {
			sb.append("AND TSR.SURVAYFLG =1 ");
		}
		// **↑↑↑　2019/09/25  RASIS岡本 アンケート入力日による検索条件追加↑↑↑**//

		// **↓↓↓　2019/10/31  RASIS岡本 施工情報検索項目追加↓↓↓**//
		if(constructionSearchFlg!=null && constructionInput ==1 ) {
			sb.append("AND TSIR.CONSTRUCTIONFLG =1 ");
		}
		// **↑↑↑　2019/10/31  RASIS岡本 施工情報検索項目追加↑↑↑**//

		sb.append("AND ROWNUM <= 1001 ");



		// 共通 : 並び替え条件
		// 0  機番昇順,  1 機番降順, 2 所在地昇順  , 3 所在地降順   , 4 アワメータ 昇順  , 5 アワメータ降順
		if(sortFlg == 0 || sortFlg == 1){
			if(kibanSelect == 0)
				sb.append("ORDER BY NLS_UPPER(MM.KIBAN) ");
			if(kibanSelect == 1)
				sb.append("ORDER BY NLS_UPPER(MM.LBX_KIBAN) ");
			if(kibanSelect == 2)
				sb.append("ORDER BY NLS_UPPER(MM.USER_KANRI_NO) ");
		}
		if(sortFlg == 2 || sortFlg == 3)
			sb.append("ORDER BY MM.POSITION ");
		if(sortFlg == 4 || sortFlg == 5)
			sb.append("ORDER BY HOUR_METER ");
		if(sortFlg == 1 || sortFlg == 3 || sortFlg == 5)
			sb.append("DESC ");


		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		logger.config(new String(sb));
		logger.config("findByMachineList="+result.size());


		if(result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * NativeQuery
	 * findByLatlngNativeQuery: 機械緯度経度取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findByLatlngNativeQuery(String searchKibanInput){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ROUND(NEW_IDO/3600/1000,6) IDO, ROUND(NEW_KEIDO/3600/1000,6) KEIDO ");
		sb.append("FROM MST_MACHINE ");
		sb.append("WHERE DELETE_FLAG = 0 ");
		sb.append("AND NLS_UPPER(KIBAN) LIKE NLS_UPPER('"+ ConvertUtil.rpStr(searchKibanInput)+"') ");
		sb.append("OR NLS_UPPER(LBX_KIBAN) LIKE NLS_UPPER('"+ConvertUtil.rpStr(searchKibanInput)+"') ");
		sb.append("OR NLS_UPPER(USER_KANRI_NO) LIKE NLS_UPPER('"+ConvertUtil.rpStr(searchKibanInput)+"') ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> latlng = em.createNativeQuery(new String(sb)).getResultList();

		logger.config(new String(sb));

		if (latlng.size() > 0)
			return latlng;
		else
			return null;
	}



	/**
	 * JPA-JPQL
	 * findByFavoriteFlg: お気に入り機械検索
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer findByFavoriteDelFlg(Long serialNumber, String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<Integer> result = em.createQuery("SELECT t.delFlg FROM TblUserFavorite t WHERE t.kibanSerno = :kibanSerno AND t.userId = :userId ")
				.setParameter("kibanSerno", serialNumber)
				.setParameter("userId", userId)
				.getResultList();

		if (result.size() > 0)
			return (Integer)result.get(0);
		else
			return null;
	}


	/** JPA-JPQL
	  * findByReturnMachineRegisterFlg: リターンマシン登録確認
	 */
	@Override
	public Integer findByReturnMachineRegisterFlg(Long serialNumber, String kyotenCd) {
		em.getEntityManagerFactory().getCache().evictAll();
		// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
		List<Integer> result = em.createQuery("SELECT t.delFlg FROM TblUserReturnMachine t  WHERE t.kibanSerno = :kibanSerno AND t.kyotenCd = :kyotenCd ")
				.setParameter("kibanSerno", serialNumber)
				.setParameter("kyotenCd", kyotenCd)
				.getResultList();
		// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
		if (result.size() > 0)
			return (Integer)result.get(0);
		else
			return null;
	}



}
