package com.businet.GNavApp.ejbs.machine;

import java.util.List;

import com.businet.GNavApp.entities.MstAppMachineTypeControl;
import com.businet.GNavApp.entities.MstMachine;


public interface kyotenBkup_IMachineService {



	/**
	 * JPA
	 * findByMachinePosition: Ýnæ¾
	 */
	String findByMachinePosition(Long serialNumber, String conType, String languageCd);


	/**
	 * JPA
	 * fingByDisplayContition: @B^®ÊO[væ¾
	 */
	MstAppMachineTypeControl findByMachineTypeControl(String conType, Integer machineModel);


	/**
	 * JPA
	 * findByMachine: @B}X^îñæ¾
	 */
	MstMachine findByMachine(Long serialNumber);

	/**
	 * JPA
	 * findByMachineSosiki: @BLgDæ¾
	 */
	List<Object[]> findByMachineSosiki(Long serialNumber);


	/**
	 * NativeQuery
	 * findByMachineList: @Bêæ¾
	 */
	List<Object[]> findByMachineListNativeQuery(
			String userId,
			Integer kibanSelect,
			// **««« 2019/11/28 iDEARº ¨ql¼õDætOÇÁ «««**//
			Integer customerNamePriority,
			// **ªªª 2019/11/28 iDEARº ¨ql¼õDætOÇÁ ªªª**//
			String kibanInput,
			Integer searchFlg,
			Integer sortFlg,
			String customerManagementNoFrom,
			String customerManagementNoTo,
			String manufacturerSerialNumberFrom,
			String manufacturerSerialNumberTo,
			String machineType,
			String scmModel,
			String customerName,
			Integer warningCurrentlyInProgress,
			String warningDateFrom,
			String warningDateTo,
			String dtcCode,
			Integer hourMeterFrom,
			Integer hourMeterTo,
			String dealerName,
			Integer fuelLevel,
			Integer defLevel,
			// **«««@2019/04/04  iDEARº èú®õõÇÁ «««**//
			Integer periodicServiceNotice,
			Integer periodicServiceNowDue,
			String replacementPart,
			// **ªªª@2019/04/04  iDEARº èú®õõÇÁ ªªª**//
			Integer favoriteSearchFlg,
			String toukatsubuCd,
			String kyotenCd,
			String serviceKoujyouCd,
			Integer typeFlg,
			String kigyouCd,
			String sosikiCd,
			String groupNo,
			String kengenCd,
			Double searchRangeFromIdo,
			Double searchRangeFromKeido,
			Integer searchRangeDistance,
			// **«««@2019/06/25  iDEARº AP[gõÚÇÁ «««**//
			Integer surveyInput,
			Integer surveySearchFlg,
			// **ªªª@2019/06/25  iDEARº AP[gõÚÇÁ ªªª**//
			Integer searchRangeUnit,
			String languageCd,
			// **«««@2019/09/18  RASISª{ AP[güÍúÉæéõðÇÁ«««**//
			String inputDateFrom,
			String inputDateTo,
			// **ªªª@2019/09/18  RASISª{ AP[güÍúÉæéõðÇÁªªª**//
			// **«««@2019/10/31  RASISª{ {HîñõÚÇÁ«««**//
			Integer constructionInput,
			Integer constructionSearchFlg
			// **ªªª@2019/10/31  RASISª{ {HîñõÚÇÁªªª**//
		);


	/**
	 * NativeQuery
	 * findByLatlngNativeQuery: @BÜxoxæ¾
	 */
	List<Object[]> findByLatlngNativeQuery(String searchKibanInput);



	 /** JPA-JPQL
	 * findByFavoriteFlg: ¨CÉüè@Bõ
	 */
	Integer findByFavoriteDelFlg(Long serialNumber, String userId);


	 /** JPA-JPQL
	  * findByReturnMachineRegisterFlg: ^[}Vo^mF
	 */
	Integer findByReturnMachineRegisterFlg(Long serialNumber, String kyotenCd);

}
