package com.businet.GNavApp.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * MST_AREA : エリアマスタ
 * 
 */
@Entity
@Table(name="MST_AREA")
@NamedQuery(name="MstArea.findAll", query="SELECT m FROM MstArea m")
public class MstArea implements Serializable {
	private static final long serialVersionUID = 1L;

	
	/*
	 * Mapping
	 */
	@OneToMany(mappedBy = "mstArea")  
	private List<MstSosiki> mstSosikis;
	public List<MstSosiki> getMstSosikis() {
		return this.mstSosikis;
	}
	public void setMstSosikis(List<MstSosiki> mstSosikis) {
		this.mstSosikis = mstSosikis;
	}
	public MstSosiki addMstMachine(MstSosiki mstSosiki) {
		getMstSosikis().add(mstSosiki);
		mstSosiki.setMstArea(this);
		return mstSosiki;
	}
	public MstSosiki removeMstMachine(MstSosiki mstSosiki) {
		getMstSosikis().remove(mstSosiki);
		mstSosiki.setMstArea(null);
		return mstSosiki;
	}	
	
	
	/*
	 * Field 
	 */
	@Id
	@Column(name="AREA_CD")
	private String areaCd;

	@Column(name="AREA_NAME")
	private String areaName;

	@Column(name="COUNTRY_CD")
	private String countryCd;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	
	public MstArea() {
	}

	
	/*
	 * Getter, Setter
	 */
	public String getAreaCd() {
		return this.areaCd;
	}
	public void setAreaCd(String areaCd) {
		this.areaCd = areaCd;
	}

	public String getAreaName() {
		return this.areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCountryCd() {
		return this.countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}