package com.businet.GNavApp.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * NEWS_ID : ニューステーブル
 * 
 */
@Entity
@Table(name="TBL_NEWS")
@NamedQuery(name="TblNews.findAll", query="SELECT t FROM TblNews t")
public class TblNews implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */
	@Id
	@Column(name="NEWS_ID")
	private Integer newsId;
	
	@Column(name="APP_FLG")
	private Integer appFlg;	

	@Column(name="TARGET_USER_ID")
	private String targetUserId;	
	
	@Column(name="TARGET_KENGEN_CD")
	private String targetKengenCd;

	@Column(name="LANGUAGE_CD")
	private String languageCd;
	
	@Column(name="NEWS_CONTENTS")
	private String newsContents;

	@Temporal(TemporalType.DATE)
	@Column(name="ED_DATE")
	private Date edDate;

	@Temporal(TemporalType.DATE)
	@Column(name="OP_DATE")
	private Date opDate;
	
	@Column(name="REMARKS")
	private String remarks;
	
	@Column(name="DEL_FLG")
	private Integer delFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	
	/*
	 * Constructor
	 */
	public TblNews() {
	}

	
	/*
	 * Setter, Getter
	 */
	public Integer getNewsId() {
		return this.newsId;
	}
	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}
	
	public Integer getAppFlg() {
		return this.appFlg;
	}
	public void setAppFlg(Integer appFlg) {
		this.appFlg = appFlg;
	}
	
	public String getTargetUserId() {
		return this.targetUserId;
	}
	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}
	
	public String getTargetKengenCd() {
		return this.targetKengenCd;
	}
	public void setTargetKengenCd(String targetKengenCd) {
		this.targetKengenCd = targetKengenCd;
	}
	
	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}
	
	public String getNewsContents() {
		return this.newsContents;
	}
	public void setNewsContents(String newsContents) {
		this.newsContents = newsContents;
	}

	public Date getOpDate() {
		return this.opDate;
	}
	public void setOpDate(Date opDate) {
		this.opDate = opDate;
	}

	public Date getEdDate() {
		return this.edDate;
	}
	public void setEdDate(Date edDate) {
		this.edDate = edDate;
	}
	
	public String getRemarks() {
		return this.remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}
	
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}


}