package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * TBL_FAVORITE_DTC_HISTORY
 *
 */
@Entity
@Table(name="TBL_FAVORITE_DTC_HISTORY")
@NamedQueries({
	@NamedQuery(name="TblFavoriteDtcHistory.findAll", query="SELECT t FROM TblFavoriteDtcHistory t"),
//	@NamedQuery(name="TblFavoriteDtcHistory.updateReadFlg",
//				query="UPDATE TblFavoriteDtcHistory t SET t.readFlg = :readFlg "
//						+ "WHERE t.userId = :userId AND t.kibanSerno = :kibanSerno")
})
public class TblFavoriteDtcHistory implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */
	@Id
	@Column(name="USER_ID")
	private String userId;

	@Column(name="KIBAN_SERNO")
//	private Integer kibanSerno;
	private Long kibanSerno;

	@Column(name="KIBAN_ADDR")
	private String kibanAddr;

	@Column(name="EVENT_NO")
	private Integer eventNo;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="EVENT_SHUBETU")
	private Integer eventShubetu;

	@Column(name="RECV_TIMESTAMP")
	private Timestamp recvTimestamp;

	@Column(name="HASSEI_TIMESTAMP")
	private Timestamp hasseiTimestamp;

	@Column(name="READ_FLG")
	private Integer readFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;


	/*
	 * Constructor
	 */
	public TblFavoriteDtcHistory() {
	}


	/*
	 * Setter, Getter
	 */
	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public String getKibanAddr() {
		return this.kibanAddr;
	}
	public void setKibanAddr(String kibanAddr) {
		this.kibanAddr = kibanAddr;
	}

	public Integer getEventNo() {
		return this.eventNo;
	}
	public void setEventNo(Integer eventNo) {
		this.eventNo = eventNo;
	}

	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}

	public Integer getEventShubetu() {
		return this.eventShubetu;
	}
	public void setEventShubetu(Integer eventShubetu) {
		this.eventShubetu = eventShubetu;
	}

	public Timestamp getRecvTimestamp() {
		return this.recvTimestamp;
	}
	public void setRecvTimestamp(Timestamp recvTimestamp) {
		this.recvTimestamp = recvTimestamp;
	}

	public Timestamp getHasseiTimestamp() {
		return this.hasseiTimestamp;
	}
	public void setHasseiTimestamp(Timestamp hasseiTimestamp) {
		this.hasseiTimestamp = hasseiTimestamp;
	}

	public Integer getReadFlg() {
		return this.readFlg;
	}
	public void setReadFlg(Integer readFlg) {
		this.readFlg = readFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


}