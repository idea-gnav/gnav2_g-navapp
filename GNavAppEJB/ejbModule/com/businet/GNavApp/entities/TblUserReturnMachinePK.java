package com.businet.GNavApp.entities;

import java.io.Serializable;

public class TblUserReturnMachinePK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TblUserReturnMachinePK() {

	}

	private String kyotenCd;

	private Long kibanSerno;

	public String getKyotenCd() {
		return this.kyotenCd;
	}

	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}

	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kibanSerno == null) ? 0 : kibanSerno.hashCode());
		result = prime * result + ((kyotenCd == null) ? 0 : kyotenCd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TblUserReturnMachinePK other = (TblUserReturnMachinePK) obj;
		if (kibanSerno == null) {
			if (other.kibanSerno != null)
				return false;
		} else if (!kibanSerno.equals(other.kibanSerno))
			return false;
		if (kyotenCd == null) {
			if (other.kyotenCd != null)
				return false;
		} else if (!kyotenCd.equals(other.kyotenCd))
			return false;
		return true;
	}

}
