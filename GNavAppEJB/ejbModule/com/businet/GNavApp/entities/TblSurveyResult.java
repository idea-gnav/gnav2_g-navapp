package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TBL_SURVEY_RESULT database table.
 *
 */
@Entity
@Table(name="TBL_SURVEY_RESULT")
@NamedQueries({
	@NamedQuery(name="TblSurveyResult.findAll", query="SELECT m FROM TblSurveyResult m"),
	@NamedQuery(name="TblSurveyResult.findBySurveyResultId", query="SELECT m FROM TblSurveyResult m Where m.surveyResultId = :surveyResultId")
})

public class TblSurveyResult implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */

	@Id
	@Column(name="SURVEY_RESULT_ID")
	private Integer surveyResultId;
	
	@Column(name="KIBAN_SERNO")
	private Long kibanSerno;	

	@Column(name="SURVEY_ID")
	private Integer surveyId;	
	
	@Column(name="KIGYOU_NAME")
	private String kigyouName;	
	
	@Column(name="ANSWER_USER")
	private String answerUser;
	
	@Column(name="POSITION")
	private String position;
	
	@Column(name="SURVEY_DATE")
	private Date surveyDate;
	
	@Column(name="DELETE_FLG")
	private Integer deleteFlg;
	
	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;
	
	
	public TblSurveyResult() {
	}
	
	
	/*
	 * setter, getter  
	 */
	
	public Integer getSurveyResultId() {
		return this.surveyResultId;
	}
	public void setSurveyResultId(Integer surveyResultId) {
		this.surveyResultId = surveyResultId;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}

	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}
	
	public String getKigyouName() {
		return this.kigyouName;
	}
	public void setKigyouName(String kigyouName) {
		this.kigyouName = kigyouName;
	}
	
	public String getAnswerUser() {
		return this.answerUser;
	}
	public void setAnswerUser(String answerUser) {
		this.answerUser = answerUser;
	}
	
	public String getPosition() {
		return this.position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
	public Date getSurveyDate() {
		return this.surveyDate;
	}
	public void setSurveyDate(Date surveyDate) {
		this.surveyDate = surveyDate;
	}
	
	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}
	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}
	
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}



}