package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="TBL_RETURN_MACHINE_WORK")
@NamedQuery(name="TblReturnMachineWork.findAll", query="SELECT m FROM TblReturnMachineWork m")
public class TblReturnMachineWork implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */

	@Id
	@Column(name="KIBAN_SERNO")
	private Long kibanSerno;

	@Column(name="RECV_TIMESTAMP")
	private Timestamp recvTimestamp;

	@Column(name="IDO")
	private Double ido;

	@Column(name="KEIDO")
	private Double keido;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;


	public TblReturnMachineWork() {
	}


	/*
	 * setter, getter
	 */

	public Long getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Timestamp getRecvTimestamp() {
		return this.recvTimestamp;
	}
	public void setRecvTimestamp(Timestamp recvTimestamp) {
		this.recvTimestamp = recvTimestamp;
	}

	public Double getIdo() {
		return this.ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return this.keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}



}