package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TBL_SURVEY_DETAIL database table.
 *
 */
@Entity
@Table(name = "TBL_SURVEY_DETAIL")
@NamedQuery(name = "TblSurveyDetail.findAll", query = "SELECT m FROM TblSurveyDetail m")
/*
@NamedQueries({
	@NamedQuery(name = "TblSurveyDetail.findAll", query = "SELECT m FROM TblSurveyDetail m"),
	@NamedQuery(name="TblSurveyDetail.findByPkId", query="SELECT m FROM TblSurveyDetail m Where m.surveyResultId = :surveyResultId And m.questionId =:questionId And m.seq =:seq")
})
*/

public class TblSurveyDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Field
	 */
	@Id
	@Column(name = "SURVEY_RESULT_ID")
	private Integer surveyResultId;

	@Column(name = "QUESTION_ID")
	private Integer questionId;

	@Column(name = "SEQ")
	private Integer seq;

	@Column(name = "ANSWER_CHOICE_ID")
	private Integer answerChoiceId;

	@Column(name = "ANSWER_FREEINPUT")
	private String answerFreeInput;

	@Column(name = "ANSWER_FREEINPUT_ENGLISH")
	private String answerFreeInputEnglish;

	@Column(name = "IMAGE_FILENAME")
	private String imageFileName;
	
	@Column(name = "DELETE_FLG")
	private Integer deleteFlg;

	@Column(name = "REGIST_USER")
	private String registUser;

	@Column(name = "REGIST_PRG")
	private String registPrg;

	@Column(name = "UPDATE_USER")
	private String updateUser;

	@Column(name = "UPDATE_PRG")
	private String updatePrg;

	@Column(name = "REGIST_DTM")
	private Timestamp registDtm;

	@Column(name = "UPDATE_DTM")
	private Timestamp updateDtm;

	public TblSurveyDetail() {
	}

	/*
	 * setter, getter
	 */

	
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public Integer getSurveyResultId() {
		return surveyResultId;
	}

	public void setSurveyResultId(Integer surveyResultId) {
		this.surveyResultId = surveyResultId;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getAnswerChoiceId() {
		return answerChoiceId;
	}

	public void setAnswerChoiceId(Integer answerChoiceId) {
		this.answerChoiceId = answerChoiceId;
	}

	public String getAnswerFreeInput() {
		return answerFreeInput;
	}

	public void setAnswerFreeInput(String answerFreeInput) {
		this.answerFreeInput = answerFreeInput;
	}

	public String getAnswerFreeInputEnglish() {
		return answerFreeInputEnglish;
	}

	public void setAnswerFreeInputEnglish(String answerFreeInputEnglish) {
		this.answerFreeInputEnglish = answerFreeInputEnglish;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public Integer getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
