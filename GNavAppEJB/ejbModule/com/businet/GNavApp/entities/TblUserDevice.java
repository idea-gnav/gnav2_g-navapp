package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_USER_DEVICE : ユーザーデバイステーブル
 *
 */
@Entity
@Table(name="TBL_USER_DEVICE")
@NamedQuery(name="TblUserDevice.findAll", query="SELECT t FROM TblUserDevice t")
public class TblUserDevice implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Mapping
	 */

	// user N : device 1
	@ManyToOne
	@JoinColumn(name="USER_ID" , insertable=false, updatable=false)
	private MstUser mstUser;
	public MstUser getMstUser() {
		return this.mstUser;
	}
	public void setMstUser(MstUser mstUser) {
		this.mstUser = mstUser;
	}



	/*
	 * Field
	 */
	@Id
	@Column(name="DEVICE_TOKEN")
	private String deviceToken;

	@Column(name="USER_ID")
	private String userId;

	@Column(name="DEVICE_TYPE")
	private Integer deviceType;

	@Column(name="USER_LOGIN_DTM")
	private Timestamp userLoginDtm;

	@Column(name="DEL_FLG")
	private Integer delFlg;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;

	// **↓↓↓　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↓↓↓**//
	@Column(name="APP_FLG")
	private Integer appFlg;

	@Column(name="LANGUAGE_CD")
	private String languageCd;
	// **↑↑↑　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↑↑↑**//




	public TblUserDevice() {
	}



	/*
	 * Setter, Getter
	 */

	public String getDeviceToken() {
		return this.deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Integer getDeviceType() {
		return this.deviceType;
	}
	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}

	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getUserLoginDtm() {
		return this.userLoginDtm;
	}
	public void setUserLoginDtm(Timestamp userLoginDtm) {
		this.userLoginDtm = userLoginDtm;
	}

	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	// **↓↓↓　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↓↓↓**//
	public Integer getAppFlg() {
		return this.appFlg;
	}
	public void setAppFlg(Integer appFlg) {
		this.appFlg = appFlg;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}
	// **↑↑↑　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↑↑↑**//


}