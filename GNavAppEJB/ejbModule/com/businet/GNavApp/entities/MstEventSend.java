package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * MST_EVENT_SEND : �C�x���g�}�X�^
 *
 */
@Entity
@Table(name="MST_EVENT_SEND")
@NamedQuery(name="MstEventSend.findAll", query="SELECT m FROM MstEventSend m")
public class MstEventSend implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Mapping
	 */
	@OneToMany(mappedBy="mstEventSend")
	private List<TblEventSend> mstEventSends;

	public List<TblEventSend> getMstEventSends() {
		return mstEventSends;
	}
	public void setMstEventSends(List<TblEventSend> mstEventSends) {
		this.mstEventSends = mstEventSends;
	}


	/*
	 * Field
	 */
	@Column(name="ALERT_LV")
	private String alertLv;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="DELETE_FLAG")
	private Integer deleteFlag;

	@Column(name="DUMP_CONT")
	private String dumpCont;

	@Id
	@Column(name="EVENT_CODE")
	private Integer eventCode;

	@Column(name="EVENT_NAME")
	private String eventName;

	@Column(name="FLG_VISIBLE")
	private String flgVisible;

	@Column(name="HASEI_COUNT")
	private Integer haseiCount;

	@Column(name="HASEI_NITI")
	private Integer haseiNiti;

	@Column(name="HASEI_TIME")
	private Integer haseiTime;

	@Column(name="KEIKA_JIKAN")
	private Integer keikaJikan;

	@Column(name="KUBUN_NAME")
	private String kubunName;

	@Column(name="LANGUAGE_CD")
	private String languageCd;

	@Column(name="MACHINE_KBN")
	private Integer machineKbn;

	@Column(name="MESSAGE")
	private String message;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="SEND_TIME_FROM")
	private String sendTimeFrom;

	@Column(name="SEND_TIME_TO")
	private String sendTimeTo;

	@Column(name="SHUBETU_CODE")
	private Integer shubetuCode;

	@Column(name="SHUBETU_KENGEN")
	private Integer shubetuKengen;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;




	public MstEventSend() {
	}


	/*
	 * Setter, Getter
	 */
	public String getAlertLv() {
		return this.alertLv;
	}
	public void setAlertLv(String alertLv) {
		this.alertLv = alertLv;
	}

	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}

	public Integer getDeleteFlag() {
		return this.deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getDumpCont() {
		return this.dumpCont;
	}
	public void setDumpCont(String dumpCont) {
		this.dumpCont = dumpCont;
	}

	public Integer getEventCode() {
		return this.eventCode;
	}
	public void setEventCode(Integer eventCode) {
		this.eventCode = eventCode;
	}

	public String getEventName() {
		return this.eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getFlgVisible() {
		return this.flgVisible;
	}
	public void setFlgVisible(String flgVisible) {
		this.flgVisible = flgVisible;
	}

	public Integer getHaseiCount() {
		return this.haseiCount;
	}
	public void setHaseiCount(Integer haseiCount) {
		this.haseiCount = haseiCount;
	}

	public Integer getHaseiNiti() {
		return this.haseiNiti;
	}
	public void setHaseiNiti(Integer haseiNiti) {
		this.haseiNiti = haseiNiti;
	}

	public Integer getHaseiTime() {
		return this.haseiTime;
	}
	public void setHaseiTime(Integer haseiTime) {
		this.haseiTime = haseiTime;
	}

	public Integer getKeikaJikan() {
		return this.keikaJikan;
	}
	public void setKeikaJikan(Integer keikaJikan) {
		this.keikaJikan = keikaJikan;
	}

	public String getKubunName() {
		return this.kubunName;
	}
	public void setKubunName(String kubunName) {
		this.kubunName = kubunName;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}

	public Integer getMachineKbn() {
		return this.machineKbn;
	}
	public void setMachineKbn(Integer machineKbn) {
		this.machineKbn = machineKbn;
	}

	public String getMessage() {
		return this.message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getSendTimeFrom() {
		return this.sendTimeFrom;
	}
	public void setSendTimeFrom(String sendTimeFrom) {
		this.sendTimeFrom = sendTimeFrom;
	}

	public String getSendTimeTo() {
		return this.sendTimeTo;
	}
	public void setSendTimeTo(String sendTimeTo) {
		this.sendTimeTo = sendTimeTo;
	}

	public Integer getShubetuCode() {
		return this.shubetuCode;
	}
	public void setShubetuCode(Integer shubetuCode) {
		this.shubetuCode = shubetuCode;
	}

	public Integer getShubetuKengen() {
		return this.shubetuKengen;
	}
	public void setShubetuKengen(Integer shubetuKengen) {
		this.shubetuKengen = shubetuKengen;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


}