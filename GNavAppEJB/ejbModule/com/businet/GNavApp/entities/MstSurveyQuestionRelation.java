package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the MST_SURVEY_QUESTION_RELATION database table.
 *
 */
@Entity
@Table(name = "MST_SURVEY_QUESTION_RELATION")
@NamedQuery(name = "MstSurveyQuestionRelation.findAll", query = "SELECT m FROM MstSurveyQuestionRelation m")
public class MstSurveyQuestionRelation implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Field
	 */
	@Id
	@Column(name = "CHILD_QUESTION_ID")
	private Integer childQuestionId;

	@Column(name = "PARENT_QUESTION_ID")
	private Integer parentQuestionId;

	@Column(name = "PARENT_CHOICE_ID")
	private Integer parentChoiceId;

	@Column(name = "DELETE_FLG")
	private Integer deleteFlg;

	@Column(name = "REGIST_USER")
	private String registUser;

	@Column(name = "REGIST_PRG")
	private String registPrg;

	@Column(name = "UPDATE_USER")
	private String updateUser;

	@Column(name = "UPDATE_PRG")
	private String updatePrg;

	@Column(name = "REGIST_DTM")
	private Timestamp registDtm;

	@Column(name = "UPDATE_DTM")
	private Timestamp updateDtm;

	public MstSurveyQuestionRelation() {
	}

	/*
	 * setter, getter
	 */

	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}

	public Integer getChildQuestionId() {
		return childQuestionId;
	}

	public void setChildQuestionId(Integer childQuestionId) {
		this.childQuestionId = childQuestionId;
	}

	public Integer getParentQuestionId() {
		return parentQuestionId;
	}

	public void setParentQuestionId(Integer parentQuestionId) {
		this.parentQuestionId = parentQuestionId;
	}

	public Integer getParentChoiceId() {
		return parentChoiceId;
	}

	public void setParentChoiceId(Integer parentChoiceId) {
		this.parentChoiceId = parentChoiceId;
	}

	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
