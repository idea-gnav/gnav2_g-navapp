package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



@Entity
@Table(name="TBL_DTC_PDF")
@NamedQuery(name="TblDtcPdf.findAll", query="SELECT t FROM TblDtcPdf t")
public class TblDtcPdf implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */
	@Id
	@Column(name="DTC_CODE")
	private String dtcCode;

	@Column(name="LANGUAGE_CD")
	private String languageCd;

	@Column(name="MODEL_CD")
	private String modelCd;

	@Column(name="PDF_NAME")
	private String pdfName;

	@Column(name="DEL_FLG")
	private Integer delFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public TblDtcPdf() {
	}


	/*
	 * Setter, Getter
	 */

	public String getDtcCode() {
		return this.dtcCode;
	}
	public void setDtcCode(String dtcCode) {
		this.dtcCode = dtcCode;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}


	public String getModelCd() {
		return this.modelCd;
	}
	public void setModelCd(String modelCd) {
		this.modelCd = modelCd;
	}

	public String getPdfPath() {
		return this.pdfName;
	}
	public void setPdfPath(String pdfName) {
		this.pdfName = pdfName;
	}

	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}