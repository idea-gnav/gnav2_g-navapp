package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the MST_SOSIKI database table.
 *
 */
@Entity
@Table(name="MST_SOSIKI")
@NamedQuery(name="MstSosiki.findAll", query="SELECT m FROM MstSosiki m")
public class MstSosiki implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Mapping
	 */
	@ManyToOne
	@JoinColumn(name="KIGYOU_CD", insertable=false, updatable=false)  /// foreig key
	private MstKigyou mstKigyou;
	public MstKigyou getMstKigyou() {
		return mstKigyou;
	}
	public void setMstKigyou(MstKigyou mstKigyou) {
		this.mstKigyou = mstKigyou;
	}

	@ManyToOne
	@JoinColumn(name="AREA_CD", insertable=false, updatable=false)  /// foreig key
	private MstArea mstArea;
	public MstArea getMstArea() {
		return mstArea;
	}
	public void setMstArea(MstArea mstArea) {
		this.mstArea = mstArea;
	}

	@ManyToOne
	@JoinColumn(name="RESION_SALES_MANAGER_CD", insertable=false, updatable=false)  /// foreig key
	private MstResionSalesManager mstResionSalesManager;
	public MstResionSalesManager getMstResionSalesManager() {
		return mstResionSalesManager;
	}
	public void setMstResionSalesManager(MstResionSalesManager mstResionSalesManager) {
		this.mstResionSalesManager = mstResionSalesManager;
	}



	@OneToMany(mappedBy = "mstSosiki") // class MstMachine => private MstSosiki mstSosiki;
	private List<MstMachine> mstMachines;
	public List<MstMachine> getMstMachines() {
		return this.mstMachines;
	}
	public void setMstMachines(List<MstMachine> mstMachines) {
		this.mstMachines = mstMachines;
	}

	@OneToMany(mappedBy="mstSosiki")		// class MstUser => private MstSosiki mstSosiki;
	private List<MstUser> mstUsers;

	public List<MstUser> getMstUsers() {
		return this.mstUsers;
	}
	public void setMstUsers(List<MstUser> mstUsers) {
		this.mstUsers = mstUsers;
	}



	/*
	 * Field
	 */
	@Id
	@Column(name="SOSIKI_CD")
	private String sosikiCd;

	@Column(name="AREA_CD")
	private String areaCd;

	@Column(name="DAIRITENN_CD")
	private String dairitennCd;

	@Column(name="DELETE_FLG")
	private Integer deleteFlg;

	@Column(name="FLG_SCM")
	private Integer flgScm;

	@Column(name="GROUP_NO")
	private String groupNo;

	@Column(name="GYOUSHU_CD")
	private String gyoushuCd;

	private Integer ido;

	@Column(name="IS_DAIRITEN")
	private Integer isDairiten;

	@Column(name="IS_SERVICE_KOUJOU")
	private Integer isServiceKoujou;

	private Integer keido;

	@Column(name="KENGEN_CD")
	private Integer kengenCd;

	@Column(name="KIGYOU_CD")
	private String kigyouCd;

	@Column(name="KYOTEN_CD")
	private String kyotenCd;

	@Column(name="LBX_FLG")
	private Integer lbxFlg;

	@Column(name="PATTERN_NO")
	private Integer patternNo;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="RESION_SALES_MANAGER_CD")
	private String resionSalesManagerCd;

	@Column(name="SERVICE_KOUJOU_CD")
	private String serviceKoujouCd;

	@Column(name="SOSIKI_ADDR1")
	private String sosikiAddr1;

	@Column(name="SOSIKI_ADDR2")
	private String sosikiAddr2;

	@Column(name="SOSIKI_ADDR3")
	private String sosikiAddr3;

	@Column(name="SOSIKI_COUNTRY")
	private String sosikiCountry;

	@Column(name="SOSIKI_FAX1")
	private String sosikiFax1;

	@Column(name="SOSIKI_FAX2")
	private String sosikiFax2;

	@Column(name="SOSIKI_FAX3")
	private String sosikiFax3;

	@Column(name="SOSIKI_HOUJIN_AFTER")
	private String sosikiHoujinAfter;

	@Column(name="SOSIKI_HOUJIN_BEFORE")
	private String sosikiHoujinBefore;

	@Column(name="SOSIKI_LANGUAGE")
	private String sosikiLanguage;

	@Column(name="SOSIKI_NAME")
	private String sosikiName;

	@Column(name="SOSIKI_POST")
	private String sosikiPost;

	@Column(name="SOSIKI_TEL1")
	private String sosikiTel1;

	@Column(name="SOSIKI_TEL2")
	private String sosikiTel2;

	@Column(name="SOSIKI_TEL3")
	private String sosikiTel3;

	@Column(name="STATE_CD")
	private String stateCd;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public MstSosiki() {
	}



	/*
	 * setter, getter
	 */
	public String getSosikiCd() {
		return this.sosikiCd;
	}

	public void setSosikiCd(String sosikiCd) {
		this.sosikiCd = sosikiCd;
	}

	public String getAreaCd() {
		return this.areaCd;
	}

	public void setAreaCd(String areaCd) {
		this.areaCd = areaCd;
	}

	public String getDairitennCd() {
		return this.dairitennCd;
	}

	public void setDairitennCd(String dairitennCd) {
		this.dairitennCd = dairitennCd;
	}

	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}

	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public Integer getFlgScm() {
		return this.flgScm;
	}

	public void setFlgScm(Integer flgScm) {
		this.flgScm = flgScm;
	}

	public String getGroupNo() {
		return this.groupNo;
	}

	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	public String getGyoushuCd() {
		return this.gyoushuCd;
	}

	public void setGyoushuCd(String gyoushuCd) {
		this.gyoushuCd = gyoushuCd;
	}

	public Integer getIdo() {
		return this.ido;
	}

	public void setIdo(Integer ido) {
		this.ido = ido;
	}

	public Integer getIsDairiten() {
		return this.isDairiten;
	}

	public void setIsDairiten(Integer isDairiten) {
		this.isDairiten = isDairiten;
	}

	public Integer getIsServiceKoujou() {
		return this.isServiceKoujou;
	}

	public void setIsServiceKoujou(Integer isServiceKoujou) {
		this.isServiceKoujou = isServiceKoujou;
	}

	public Integer getKeido() {
		return this.keido;
	}

	public void setKeido(Integer keido) {
		this.keido = keido;
	}

	public Integer getKengenCd() {
		return this.kengenCd;
	}

	public void setKengenCd(Integer kengenCd) {
		this.kengenCd = kengenCd;
	}

	public String getKigyouCd() {
		return this.kigyouCd;
	}

	public void setKigyouCd(String kigyouCd) {
		this.kigyouCd = kigyouCd;
	}

	public String getKyotenCd() {
		return this.kyotenCd;
	}

	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

	public Integer getLbxFlg() {
		return this.lbxFlg;
	}

	public void setLbxFlg(Integer lbxFlg) {
		this.lbxFlg = lbxFlg;
	}

	public Integer getPatternNo() {
		return this.patternNo;
	}

	public void setPatternNo(Integer patternNo) {
		this.patternNo = patternNo;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getResionSalesManagerCd() {
		return this.resionSalesManagerCd;
	}

	public void setResionSalesManagerCd(String resionSalesManagerCd) {
		this.resionSalesManagerCd = resionSalesManagerCd;
	}

	public String getServiceKoujouCd() {
		return this.serviceKoujouCd;
	}

	public void setServiceKoujouCd(String serviceKoujouCd) {
		this.serviceKoujouCd = serviceKoujouCd;
	}

	public String getSosikiAddr1() {
		return this.sosikiAddr1;
	}

	public void setSosikiAddr1(String sosikiAddr1) {
		this.sosikiAddr1 = sosikiAddr1;
	}

	public String getSosikiAddr2() {
		return this.sosikiAddr2;
	}

	public void setSosikiAddr2(String sosikiAddr2) {
		this.sosikiAddr2 = sosikiAddr2;
	}

	public String getSosikiAddr3() {
		return this.sosikiAddr3;
	}

	public void setSosikiAddr3(String sosikiAddr3) {
		this.sosikiAddr3 = sosikiAddr3;
	}

	public String getSosikiCountry() {
		return this.sosikiCountry;
	}

	public void setSosikiCountry(String sosikiCountry) {
		this.sosikiCountry = sosikiCountry;
	}

	public String getSosikiFax1() {
		return this.sosikiFax1;
	}

	public void setSosikiFax1(String sosikiFax1) {
		this.sosikiFax1 = sosikiFax1;
	}

	public String getSosikiFax2() {
		return this.sosikiFax2;
	}

	public void setSosikiFax2(String sosikiFax2) {
		this.sosikiFax2 = sosikiFax2;
	}

	public String getSosikiFax3() {
		return this.sosikiFax3;
	}

	public void setSosikiFax3(String sosikiFax3) {
		this.sosikiFax3 = sosikiFax3;
	}

	public String getSosikiHoujinAfter() {
		return this.sosikiHoujinAfter;
	}

	public void setSosikiHoujinAfter(String sosikiHoujinAfter) {
		this.sosikiHoujinAfter = sosikiHoujinAfter;
	}

	public String getSosikiHoujinBefore() {
		return this.sosikiHoujinBefore;
	}

	public void setSosikiHoujinBefore(String sosikiHoujinBefore) {
		this.sosikiHoujinBefore = sosikiHoujinBefore;
	}

	public String getSosikiLanguage() {
		return this.sosikiLanguage;
	}

	public void setSosikiLanguage(String sosikiLanguage) {
		this.sosikiLanguage = sosikiLanguage;
	}

	public String getSosikiName() {
		return this.sosikiName;
	}

	public void setSosikiName(String sosikiName) {
		this.sosikiName = sosikiName;
	}

	public String getSosikiPost() {
		return this.sosikiPost;
	}

	public void setSosikiPost(String sosikiPost) {
		this.sosikiPost = sosikiPost;
	}

	public String getSosikiTel1() {
		return this.sosikiTel1;
	}

	public void setSosikiTel1(String sosikiTel1) {
		this.sosikiTel1 = sosikiTel1;
	}

	public String getSosikiTel2() {
		return this.sosikiTel2;
	}

	public void setSosikiTel2(String sosikiTel2) {
		this.sosikiTel2 = sosikiTel2;
	}

	public String getSosikiTel3() {
		return this.sosikiTel3;
	}

	public void setSosikiTel3(String sosikiTel3) {
		this.sosikiTel3 = sosikiTel3;
	}

	public String getStateCd() {
		return this.stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}