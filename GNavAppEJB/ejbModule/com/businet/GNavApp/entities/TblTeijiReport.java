package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
//import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
//import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_TEIJI_REPORT : 定時レポートテーブル
 *
 */
@Entity
@Table(name="TBL_TEIJI_REPORT")
@NamedQuery(name="TblTeijiReport.findAll", query="SELECT t FROM TblTeijiReport t")
public class TblTeijiReport implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Mapping
	 */
	@ManyToOne
	@JoinColumn(name="KIBAN_SERNO", insertable=false, updatable=false)
	private MstMachine mstMachine;
	public MstMachine getMstMachine() {
		return mstMachine;
	}
	public void setMstMachine(MstMachine mstMachine) {
		this.mstMachine = mstMachine;
	}

	/*
	 * Field
	 */
	@Id
	@Column(name="KIBAN_SERNO")
//	private Integer kibanSerno;
	private Long kibanSerno;

	@Column(name="KIKAI_KADOBI")
	private Timestamp kikaiKadobi;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="HOUR_METER")
	private Double hourMeter;

	@Column(name="KIBAN")
	private String kiban;

	@Column(name="KIKAI_KADOBI_LOCAL")
	private Timestamp kikaiKadobiLocal;

	@Column(name="KIKAI_SOSA_TIME")
	private Double kikaiSosaTime;

	@Column(name="NENRYO_CONSUM")
	private Double nenryoConsum;

	@Column(name="NENRYO_LV")
	private Integer nenryoLv;

	@Column(name="NIPPO_DATA")
	private String nippoData;

	@Column(name="RADIATOR_ONDO_MAX")	//ラジエター冷却水温度Max = クーラントMAX
	private Double radiatorOndoMax;

	@Column(name="RECV_TIMESTAMP")
	private Timestamp recvTimestamp;

	@Column(name="SADOYU_ONDO_MAX")
	private Double sadoyuOndoMax;

	@Column(name="UREA_WATER_CONSUM")
	private Double ureaWaterConsum;

	@Column(name="UREA_WATER_LV")
	private Integer ureaWaterLv;

	@Column(name="SEKO_TIME")
	private Double sekoTime;

	@Column(name="MIXTURE_PROCESSING_TON")
	private Double mixtureProcessingTon;

	@Column(name="SEKO_KYORI")
	private Double sekoKyori;

	@Column(name="VEHICLE_BATTERY_VOLTAGE")
	private Double vehicleBatteryVoltage;

	@Column(name="ALTERNATOR_GENERATING_VOLTAGE")
	private Double alternatorGeneratingVoltage;

	@Column(name="RAN_SHUDO_SAISEI_START_NUM")
	private Integer ranShudoSaiseiStartNum;

	@Column(name="REQ_SHUDO_SAISEI_START_NUM")
	private Integer reqShudoSaiseiStartNum;

	@Column(name="JIDO_SAISEI_OVER_NUM")
	private Integer jidoSaiseiOverNum;

	@Column(name="JIDO_SAISEI_START_NUM")
	private Integer jidoSaiseiStartNum;

	@Column(name="SHUDO_SAISEI_START_NUM")
	private Integer shudoSaiseiStartNum;

	@Column(name="SHUDO_SAISEI_OVER_NUM")
	private Integer shudoSaiseiOverNum;

	@Column(name="LIFTING_MAGNET_CRANE")
	private Integer liftingMagnetCrane;

	@Column(name="LIFTING_MAGNET_TIME")
	private Double liftingMagnetTime;

	@Column(name="BREAKER_TIME")
	private Double breakerTime;



	public TblTeijiReport() {
	}



	/*
	 * setter, getter
	 */

	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}


	public Double getHourMeter() {
		return this.hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getKiban() {
		return this.kiban;
	}
	public void setKiban(String kiban) {
		this.kiban = kiban;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Timestamp getKikaiKadobi() {
		return this.kikaiKadobi;
	}
	public void setKikaiKadobi(Timestamp kikaiKadobi) {
		this.kikaiKadobi = kikaiKadobi;
	}

	public Timestamp getKikaiKadobiLocal() {
		return this.kikaiKadobiLocal;
	}
	public void setKikaiKadobiLocal(Timestamp kikaiKadobiLocal) {
		this.kikaiKadobiLocal = kikaiKadobiLocal;
	}

	public Double getKikaiSosaTime() {
		return this.kikaiSosaTime;
	}
	public void setKikaiSosaTime(Double kikaiSosaTime) {
		this.kikaiSosaTime = kikaiSosaTime;
	}

	public Double getNenryoConsum() {
		return this.nenryoConsum;
	}
	public void setNenryoConsusum(Double nenryoConsum) {
		this.nenryoConsum = nenryoConsum;
	}

	public Integer getNenryoLv() {
		return this.nenryoLv;
	}
	public void setNenryoLv(Integer nenryoLv) {
		this.nenryoLv = nenryoLv;
	}

	public String getNippoData() {
		return this.nippoData;
	}
	public void setNippoData(String nippoData) {
		this.nippoData = nippoData;
	}

	public Double getRadiatorOndoMax() {
		return this.radiatorOndoMax;
	}
	public void setRadiatorOndoMax(Double radiatorOndoMax) {
		this.radiatorOndoMax = radiatorOndoMax;
	}

	public Timestamp getRecvTimestamp() {
		return this.recvTimestamp;
	}
	public void setRecvTimestamp(Timestamp recvTimestamp) {
		this.recvTimestamp = recvTimestamp;
	}

	public Double getSadoyuOndoMax() {
		return this.sadoyuOndoMax;
	}
	public void setSadoyuOndoMax(Double sadoyuOndoMax) {
		this.sadoyuOndoMax = sadoyuOndoMax;
	}

	public Double getUreaWaterConsum() {
		return this.ureaWaterConsum;
	}
	public void setUreaWaterConsum(Double ureaWaterConsum) {
		this.ureaWaterConsum = ureaWaterConsum;
	}

	public Integer getUreaWaterLv() {
		return this.ureaWaterLv;
	}
	public void setUreaWaterLv(Integer ureaWaterLv) {
		this.ureaWaterLv = ureaWaterLv;
	}

//	private Integer sekoTime;
	public Double getSekoTime() {
		return this.sekoTime;
	}
	public void setSekoTime(Double sekoTime) {
		this.sekoTime = sekoTime;
	}

//	private Integer mixtureProcessingTon;
	public Double getMixtureProcessingTon() {
		return this.mixtureProcessingTon;
	}
	public void setMixtureProcessingTon(Double mixtureProcessingTon) {
		this.mixtureProcessingTon = mixtureProcessingTon;
	}

//	private Integer sekoKyori;
	public Double getSekoKyori() {
		return this.sekoKyori;
	}
	public void setSekoKyori(Double sekoKyori) {
		this.sekoKyori = sekoKyori;
	}

//	private Integer vehicleBatteryVoltage;
	public Double getVehicleBatteryVoltage() {
		return this.vehicleBatteryVoltage;
	}
	public void setVehicleBatteryVoltage(Double vehicleBatteryVoltage) {
		this.vehicleBatteryVoltage = vehicleBatteryVoltage;
	}

//	private Integer alternatorGeneratingVoltage;
	public Double getAlternatorGeneratingVoltage() {
		return this.alternatorGeneratingVoltage;
	}
	public void setAlternatorGeneratingVoltage(Double alternatorGeneratingVoltage) {
		this.alternatorGeneratingVoltage = alternatorGeneratingVoltage;
	}

//	private Integer ranShudoSaiseiStartNum;
	public Integer getRanShudoSaiseiStartNum() {
		return this.ranShudoSaiseiStartNum;
	}
	public void setRanShudoSaiseiStartNum(Integer ranShudoSaiseiStartNum) {
		this.ranShudoSaiseiStartNum = ranShudoSaiseiStartNum;
	}

//	private Integer reqShudoSaiseiStartNum;
	public Integer getReqShudoSaiseiStartNum() {
		return this.reqShudoSaiseiStartNum;
	}
	public void setReqShudoSaiseiStartNum(Integer reqShudoSaiseiStartNum) {
		this.reqShudoSaiseiStartNum = reqShudoSaiseiStartNum;
	}

	public Integer getJidoSaiseiOverNum() {
		return this.jidoSaiseiOverNum;
	}
	public void setJidoSaiseiOverNum(Integer jidoSaiseiOverNum) {
		this.jidoSaiseiOverNum = jidoSaiseiOverNum;
	}

	public Integer getJidoSaiseiStartNum() {
		return this.jidoSaiseiStartNum;
	}
	public void setJidoSaiseiStartNum(Integer jidoSaiseiStartNum) {
		this.jidoSaiseiStartNum = jidoSaiseiStartNum;
	}

//	private Integer shudoSaiseiStartNum;
	public Integer getShudoSaiseiStartNum() {
		return this.shudoSaiseiStartNum;
	}
	public void setShudoSaiseiStartNum(Integer shudoSaiseiStartNum) {
		this.shudoSaiseiStartNum = shudoSaiseiStartNum;
	}

//	private Integer shudoSaiseiOverNum;
	public Integer getShudoSaiseiOverNum() {
		return this.shudoSaiseiOverNum;
	}
	public void setShudoSaiseiOverNum(Integer shudoSaiseiOverNum) {
		this.shudoSaiseiOverNum = shudoSaiseiOverNum;
	}

//	private Integer liftingMagnetCrane;
	public Integer getLiftingMagnetCrane() {
		return this.liftingMagnetCrane;
	}
	public void setLiftingMagnetCrane(Integer liftingMagnetCrane) {
		this.liftingMagnetCrane = liftingMagnetCrane;
	}

//	private Integer liftingMagnetTime;
	public Double getLiftingMagnetTime() {
		return this.liftingMagnetTime;
	}
	public void setLiftingMagnetTime(Double liftingMagnetTime) {
		this.liftingMagnetTime = liftingMagnetTime;
	}

//	private Integer breakerTime;
	public Double getBreakerTime() {
		return this.breakerTime;
	}
	public void setBreakerTime(Double breakerTime) {
		this.breakerTime = breakerTime;
	}



}