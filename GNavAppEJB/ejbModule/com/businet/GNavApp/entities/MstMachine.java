package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
//import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the MST_MACHINE database table.
 *
 */
@Entity
@Table(name="MST_MACHINE")
@NamedQuery(name="MstMachine.findAll", query="SELECT m FROM MstMachine m")
public class MstMachine implements Serializable {

	private static final long serialVersionUID = 1L;


	/*
	 * Mapping
	 */

	@OneToMany(mappedBy = "mstMachine")
	private List<TblTeijiReport> tblTeijiReports;


	@ManyToOne
	@JoinColumn(name="SOSIKI_CD", insertable=false, updatable=false)  /// foreig key
	private MstSosiki mstSosiki;
	public MstSosiki getMstSosiki() {
		return mstSosiki;
	}
	public void setMstSosiki(MstSosiki mstSosiki) {
		this.mstSosiki = mstSosiki;
	}


	@OneToMany(mappedBy="mstMachine")
	private List<TblUserFavorite> tblUserFavorites;

	public List<TblUserFavorite> getTblUserFavorites() {
		return this.tblUserFavorites;
	}
	public void setTblUserFavorites(List<TblUserFavorite> tblUserFavorites) {
		this.tblUserFavorites = tblUserFavorites;
	}




	/*
	 * Field
	 */

	@Id
	@Column(name="KIBAN_SERNO")
//	private Integer kibanSerno;
	private Long kibanSerno;

	@Column(name="KIBAN")
	private String kiban;

	@Column(name="KIBAN_ADDR")
	private String kibanAddr;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="SHIMUKESAKI")
	private Integer shimukesaki;

	// **««« 2021/02/03 iDEARº J®«ÏXÎ «««**//
//	@Column(name="ENGINE_SERNO")
//	private Double engineSerno;
	@Column(name="ENGINE_SERNO")
	private String engineSerno;
	// **ªªª 2021/02/03 iDEARº J®«ÏXÎ ªªª**//

	@Column(name="ECM_NO")
	private String ecmNo;

	@Column(name="CTRL_A_BUHIN_NO")
	private String ctrlABuhinNo;

	@Column(name="CTRL_B_BUHIN_NO")
	private String ctrlBBuhinNo;

	@Column(name="CTRL_SC_BUHIN_NO")
	private String ctrlScBuhinNo;

	@Column(name="Q1200_SERNO")
	private Integer q1200Serno;

	@Column(name="Q4000_BUHIN_NO")
	private String q4000BuhinNo;

	@Column(name="Q4000_SERNO")
	private Integer q4000Serno;

	@Column(name="CDMA_SERNO")
	private Integer cdmaSerno;

	@Column(name="CDMA_TELNO")
	private String cdmaTelno;

	@Column(name="WAKEUP_HOUR")
	private Integer wakeupHour;

	@Column(name="WAKEUP_MINUTE")
	private Integer wakeupMinute;

	@Column(name="WAKEUP_SECOND")
	private Integer wakeupSecond;

	@Column(name="SOSIKI_CD")
	private String sosikiCd;

	@Column(name="SOSIKI_TR")
	private String sosikiTr;

	@Column(name="MATOME_KISHU_CD")
	private String matomeKishuCd;

	@Column(name="PROMENT_SHUBETU_CD")
	private String promentShubetuCd;

	@Column(name="MODEL_CD")
	private String modelCd;

	@Column(name="GNAV_START_DATE")
	private Date gnavStartDate;

	@Column(name="GNAV_END_DATE")
	private Date gnavEndDate;

	@Column(name="GNAV_END_YOTEI_DATE")
	private Date gnavEndYoteiDate;

	@Column(name="USER_KANRI_NO")
	private String userKanriNo;

	@Column(name="KISHU")
	private Integer kishu;

	@Column(name="TOUNAN_BOSI_STATUS")
	private Integer tounanBosiStatus;

	@Column(name="CURFEW_STATUS")
	private Integer curfewStatus;

	@Column(name="GEOFENCE_STATUS")
	private Integer geofenceStatus;

	@Column(name="FLG_NENRYO_MITSUDO")
	private Integer flgNenryoMitsudo;

	@Column(name="TEIJI_KADOBI")
	private Timestamp teijiKadobi;

	@Column(name="TEIKI_KADOBI")
	private Timestamp teikiKadobi;

	@Column(name="RECV_TIMESTAMP")
	private Timestamp recvTimestamp;

	@Column(name="POSITION")
	private String position;

	@Column(name="KOSHO_COUNTER")
	private Integer koshoCounter;

	@Column(name="NEW_IDO")
	private Double newIdo;

	@Column(name="NEW_KEIDO")
	private Double newKeido;

	@Column(name="NEW_HOUR_METER")
	private Double newHourMeter;

	@Column(name="NEW_KIKAI_TIME")
	private Integer newKikaiTime;

	@Column(name="NEW_UWAMONO_TIME")
	private Integer newUwamonoTime;

	@Column(name="NEW_SENKAI_TIME")
	private Integer newSenkaiTime;

	@Column(name="NEW_SOKO_TIME")
	private Integer newSokoTime;

	@Column(name="NEW_SP_MODE_TIME")
	private Integer newSpModeTime;

	@Column(name="NEW_BREAKER_TIME")
	private Double newBreakerTime;

	@Column(name="NEW_HASAI_TIME")
	private Integer newHasaiTime;

	@Column(name="NEW_A_MODE_TIME")
	private Integer newAModeTime;

	@Column(name="NEW_GUREN_TIME")
	private Integer newGurenTime;

	@Column(name="NEW_NENRYO_L_0_2000")
	private Double newNenryoL02000;

	@Column(name="NONYUBI")
	private Timestamp nonyubi;

	@Column(name="SHOGAI_FOLLOW")
	private String shogaiFollow;

	@Column(name="HAJIMEBI")
	private Timestamp hajimebi;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="TOUNAN_TUISEKI_STATUS")
	private Integer tounanTuisekiStatus;

	@Column(name="PROMENT_KAISI_BI")
	private Date promentKaisiBi;

	@Column(name="COMMAND_KAISU")
	private Integer commandKaisu;

	@Column(name="CAMPAIGN")
	private String campaign;

	@Column(name="DELETE_FLAG")
	private Integer deleteFlag;

	@Column(name="CTRL_T_BUHIN_NO")
	private String ctrlTBuhinNo;

	@Column(name="MAINTAINCE_KBN")
	private Integer maintainceKbn;

	@Column(name="MONITOR_RUSH_NO")
	private String monitorRushNo;

	@Column(name="MONITOR_FIRM_NO")
	private String monitorFirmNo;

	@Column(name="SERVICE_KOUJOU_CD")
	private String serviceKoujouCd;

	@Column(name="MACHINE_MODEL")
	private Integer machineModel;

	@Column(name="NEW_HIGH_LOAD_PER")
	private Integer newHighLoadPer;

	@Column(name="NEW_HIGH_SUION_PER")
	private Integer newHighSuionPer;

	@Column(name="NEW_MID_SUION_PER")
	private Integer newMidSuionPer;

	@Column(name="REMOTE_LOCK")
	private Integer remoteLock;

	@Column(name="NENRYO_LV")
	private Integer nenryoLv;

	@Column(name="KEYIHOU_HASSEYI_TYU_COUNT")
	private Integer keyihouHasseyiTyuCount;

	@Column(name="YOKOKU_COUNT")
	private Integer yokokuCount;

	@Column(name="KEYIKOKU_COUNT")
	private Integer keyikokuCount;

	@Column(name="JISA")
	private Integer jisa;

	@Column(name="EVENT_STATES")
	private Integer eventStates;

	@Column(name="EVENT_NASI_CNT")
	private Integer eventNasiCnt;

	@Column(name="EVENT_TUJYO_CNT")
	private Integer eventTujyoCnt;

	@Column(name="EVENT_KINKYU_CNT")
	private Integer eventKinkyuCnt;

	@Column(name="EVENT_MITAIOU_CNT")
	private Integer eventMitaiouCnt;

	@Column(name="EVENT_YOUSUMI_CNT")
	private Integer eventYousumiCnt;

	@Column(name="EVENT_TAIOU_CNT")
	private Integer eventTaiouCnt;

	@Column(name="LIFTING_MAGNET_CRANE")
	private Integer liftingMagnetCrane;

	@Column(name="LIFTING_MAGNET_TIME")
	private Double liftingMagnetTime;

	@Column(name="ASPIRATIONS_NUM")
	private Integer aspirationsNum;

	@Column(name="RAN_SYUDOU_SAISEI_START_NUM")
	private Integer ranSyudouSaiseiStartNum;

	@Column(name="REQ_SYUDOU_SAISEI_START_NUM")
	private Integer reqSyudouSaiseiStartNum;

	@Column(name="SYUDOU_SAISEI_START_NUM")
	private Integer syudouSaiseiStartNum;

	@Column(name="SYUDOU_SAISEI_OVER_NUM")
	private Integer syudouSaiseiOverNum;

	@Column(name="JIDOU_SAISEI_START_NUM")
	private Integer jidouSaiseiStartNum;

	@Column(name="JIDOU_SAISEI_OVER_NUM")
	private Integer jidouSaiseiOverNum;

	@Column(name="H_TIME")
	private Integer hTime;

	@Column(name="TOUROKU_TIMESTAMP")
	private Timestamp tourokuTimestamp;

	@Column(name="RTC_START_TIME")
	private Integer rtcStartTime;

	@Column(name="RTC_END_TIME")
	private Integer rtcEndTime;

	@Column(name="CURFEW_REGIST_DTM")
	private Timestamp curfewRegistDtm;

	@Column(name="REMODE_REGIST_DTM")
	private Timestamp remodeRegistDtm;

	@Column(name="SET_IDO")
	private Integer setIdo;

	@Column(name="SET_KEIDO")
	private Integer setKeido;

	@Column(name="SET_RANGE")
	private String setRange;

	@Column(name="SET_RELEASE_PWD")
	private String setReleasePwd;

	@Column(name="SET_PASSWORD_LASTUSER")
	private String setPasswordLastuser;

	@Column(name="FLG_NEED_JISA")
	private Integer flgNeedJisa;

	@Column(name="TOUKATSUBU_CD")
	private String toukatsubuCd;

	@Column(name="KYOTEN_CD")
	private String kyotenCd;

	@Column(name="NEW_CONVEYER")
	private Integer newConveyer;

	@Column(name="NEW_KANETSU_TIME")
	private Integer newKanetsuTime;

	@Column(name="NEW_KANETSU_MODE_TIME")
	private Integer newKanetsuModeTime;

	@Column(name="NEW_KANETSU_KAISU")
	private Integer newKanetsuKaisu;

	@Column(name="NEW_KOUSOKU_MODE_SOKO_TIME")
	private Integer newKousokuModeSokoTime;

	@Column(name="NEW_TEISOKU_MODE_SOKO_TIME")
	private Integer newTeisokuModeSokoTime;

	@Column(name="NEW_TEISOKU_4WD_MODE_SOKO_TIME")
	private Integer newTeisoku4wdModeSokoTime;

	@Column(name="NEW_SHIKOU_TIME")
	private Double newShikouTime;

	@Column(name="NEW_SHIKOU_KYOURI")
	private Integer newShikouKyouri;

	@Column(name="NEW_KOUSOKU_MODE_IDOU_KYOURI")
	private Integer newKousokuModeIdouKyouri;

	@Column(name="NEW_KOURIN_SLIP_KAISU")
	private Integer newKourinSlipKaisu;

	@Column(name="LBX_KIBAN")
	private String lbxKiban;

	@Column(name="X4_CONTROLLER_SERIAL_NO")
	private String x4ControllerSerialNo;

	@Column(name="X4_CONTROLLER_PART_NO")
	private String x4ControllerPartNo;

	@Column(name="X4_SUB_CONTROLLER_PART_NO")
	private String x4SubControllerPartNo;

	@Column(name="X4_BACK_SCREEN_LOCK")
	private Integer x4BackScreenLock;

	@Column(name="DAIRITENN_CD")
	private String dairitennCd;

	@Column(name="LBX_MODEL_CD")
	private String lbxModelCd;

	@Column(name="ENGINE_RUN_TIME_VOLTAGE")
	private Integer engineRunTimeVoltage;

	@Column(name="KON_AFTER_BATIRY_VOLTAGE")
	private Integer konAfterBatiryVoltage;

	@Column(name="UREA_WATER_LEVEL")
	private Integer ureaWaterLevel;

	@Column(name="UREA_WATER_CONSUM")
	private Double ureaWaterConsum;

	@Column(name="MANUAL_LOCK")
	private Integer manualLock;

	@Column(name="POSITION_EN")
	private String positionEn;

	@Column(name="HB_CONTROLLER_PART_NO")
	private String hbControllerPartNo;

	@Column(name="HB_INVERTER_PART_NO")
	private String hbInverterPartNo;

	@Column(name="HB_CONVERTER_PART_NO")
	private String hbConverterPartNo;

	@Column(name="HB_BMU_PART_NO")
	private String hbBmuPartNo;

	@Column(name="BOOM_OPERATION_TIME")
	private Integer boomOperationTime;

	@Column(name="ARM_OPERATION_TIME")
	private Integer armOperationTime;

	@Column(name="BUCKET_OPERATION_TIME")
	private Integer bucketOperationTime;

	@Column(name="EMERGENCY_STOP_NUM")
	private Integer emergencyStopNum;

	@Column(name="BRAKE_NUM")
	private Integer brakeNum;

	@Column(name="SHORT_KATASIKI")
	private String shortKatasiki;

	@Column(name="DASH_FLG")
	private Integer dashFlg;

	@Column(name="NEW_WARNING_LEVEL")
	private Integer newWarningLevel;

	@Column(name="ENFORCE_LOCK_USER_LEVEL")
	private Integer enforceLockUserLevel;

	@Column(name="ENFORCE_LOCK_SET_FLG")
	private Integer enforceLockSetFlg;

	@Column(name="VERSION_CHANGED_FLG")
	private Integer versionChangedFlg;

	@Column(name="NOTICE_TIME")
	private Integer noticeTime;

	@Column(name="NEW_VERSION_FLG")
	private Integer newVersionFlg;

	@Column(name="NEW_CTLRA_KOSHO_FLG")
	private Integer newCtlraKoshoFlg;

	@Column(name="NEW_Q4000_KOSHO_FLG")
	private Integer newQ4000KoshoFlg;

	@Column(name="ENGINE_MODEL")
	private String engineModel;

	@Column(name="PARTS_X4_SUB_BUHIN1")
	private String partsX4SubBuhin1;

	@Column(name="PARTS_X4_SUB_BUHIN2")
	private String partsX4SubBuhin2;

	@Column(name="PARTS_X4_SUB_BUHIN3")
	private String partsX4SubBuhin3;

	@Column(name="PARTS_X4_SUB_SER1")
	private String partsX4SubSer1;

	@Column(name="PARTS_X4_SUB_SER2")
	private String partsX4SubSer2;

	@Column(name="PARTS_X4_SUB_SER3")
	private String partsX4SubSer3;

	@Column(name="PARTS_T_SOFTWARE")
	private String partsTSoftware;

	@Column(name="MONITOR_SER_NO")
	private String monitorSerNo;

	@Column(name="BLAND")
	private Integer bland;

	@Column(name="ATTACHMENT_TYPE")
	private Integer attachmentType;

	@Column(name="FIRST_LINE_CIRCUIT")
	private Integer firstLineCircuit;

	@Column(name="ELECTRIC_ADJUSTMENT")
	private Integer electricAdjustment;

	@Column(name="OPERATING_SYSTEM")
	private Integer operatingSystem;

	@Column(name="SECOND_LINE_CIRCUIT")
	private Integer secondLineCircuit;

	@Column(name="QUICK_COUPLER")
	private Integer quickCoupler;

	@Column(name="OVERLOAD_ALARM")
	private Integer overloadAlarm;

	@Column(name="INTERFERENCE_PREVENTION")
	private Integer interferencePrevention;

	@Column(name="FREE_SWING")
	private Integer freeSwing;

	@Column(name="ONE_PEDAL_RUNNNING")
	private Integer onePedalRunnning;

	@Column(name="RUNNNING_ALARM")
	private Integer runnningAlarm;

	@Column(name="AUTO_STOP_PUMP_OIL")
	private Integer autoStopPumpOil;

	@Column(name="ELEVATOR_CAB")
	private Integer elevatorCab;


	@Column(name="APP_MACHINE")
	private Integer appMachine;

	@Column(name="FAN")
	private Integer fan;

	@Column(name="INFO_CONSTRUCTION")
	private Integer infoConstruction;

	@Column(name="BUCKET_SENSOR")
	private Integer bucketSensor;

	@Column(name="GRAPPLE_FLOATING")
	private Integer grappleFloating;

	@Column(name="VERSION_FLG")
	private Integer versionFlg;

	@Column(name="TEIKI_FLG")
	private Integer teikiFlg;

	@Column(name="DOCOMO_FLG")
	private Integer docomoFlg;

	@Column(name="LEFT_MONITOR_RUSH_NO")
	private String leftMonitorRushNo;

	@Column(name="RIGHT_MONITOR_RUSH_NO")
	private String rightMonitorRushNo;

	@Column(name="MAIN_MONITOR_NO")
	private String mainMonitorNo;

	@Column(name="HAIGASU_TAIOU")
	private Integer haigasuTaiou;

	@Column(name="KANETSU_SHIYO")
	private Integer kanetsuShiyo;

	@Column(name="SHIME_GATAME_SHIYO")
	private Integer shimeGatameShiyo;

	@Column(name="DENKI_KANETSU_SETTEI")
	private Integer denkiKanetsuSettei;

	@Column(name="BRAKE_STEER")
	private Integer brakeSteer;

	@Column(name="ZENRIN_KUDO_TYOSEI")
	private Integer zenrinKudoTyosei;

	@Column(name="CLIMB_LOCK")
	private Integer climbLock;

	@Column(name="ICT_CONTROLLER_FIRM_VER")
	private String ictControllerFirmVer;

	@Column(name="BOOM_IMU_FIRM_VER")
	private String boomImuFirmVer;

	@Column(name="ARM_IMU_FIRM_VER")
	private String armImuFirmVer;

	@Column(name="BUCKET_IMU_FIRM_VER")
	private String bucketImuFirmVer;

	@Column(name="ICT_CONTROLLER_SER")
	private String ictControllerSer;

	@Column(name="BOOM_IMU_SER")
	private String boomImuSer;

	@Column(name="ARM_IMU_SER")
	private String armImuSer;

	@Column(name="BUCKET_IMU_SER")
	private String bucketImuSer;

	@Column(name="HB_CONTROLLER_SER_NO")
	private String hbControllerSerNo;

	//««« 2019/09/30 iDEARº }X^JÇÁ :start  «««//
	@Column(name="FVM2")
	private Integer fvm2;

	@Column(name="RANGE_LIMIT_VALID_TIME")
	private Integer rangeLimitValidTime;

	@Column(name="HGT_DPT_LIMIT_VALID_TIME")
	private Integer hgtDptLimitValidTime;

	@Column(name="HEIGHT_LIMIT_VALID_TIME")
	private Integer heightLimitValidTime;

	@Column(name="DEPTH_LIMIT_VALID_TIME")
	private Integer depthLimitValidTime;

	@Column(name="FVM2_NOTICE_VALID_TIME")
	private Integer fvm2NoticeValidTime;

	@Column(name="LASER_FIRM_VER")
	private String laserFirmVer;

	@Column(name="HEADING_SENSOR_FIRM_VER")
	private String headingSensorFirmVer;

	@Column(name="ICT_MONITOR_FIRM_VER")
	private String ictMonitorFirmVer;

	@Column(name="MC_VALVE_MODULE_FIRM_VER")
	private String mcValveModuleFirmVer;

	@Column(name="LASER_SER_NO")
	private String laserSerNo;

	@Column(name="HEADING_SENSOR_SER_NO")
	private String headingSensorSerNo;

	@Column(name="ICT_MONITOR_SER_NO")
	private String ictMonitorSerNo;

	@Column(name="MC_VALVE_MODULE_SER_NO")
	private String mcValveModuleSerNo;

	@Column(name="NON_ANALYSIS_FLG")
	private Integer nonAnalysisFlg;

	@Column(name="ADDITIONAL_FUNCTION_1")
	private String additionalFunction1;

	@Column(name="POSITION_POST_CD")
	private String positionPostCd;

	@Column(name="POSITION_COUNTRY_CD")
	private String positionCountryCd;

	@Column(name="POSITION_AREA1")
	private String positionArea1;

	@Column(name="POSITION_AREA2")
	private String positionArea2;

	@Column(name="POSITION_AREA3")
	private String positionArea3;

	@Column(name="EISEI_FLG")
	private Integer eiseiFlg;

	@Column(name="WHEEL_FLG")
	private Integer wheelFlg;

	@Column(name="SHORT_KIBAN")
	private String shortKiban;

	//ªªª 2019/09/30 iDEARº }X^JÇÁ :end  ªªª//



	public MstMachine() {
	}



	/*
	 * Getter, Setter
	 */

	//1

	public Long getKibanSerno() {
		return this.kibanSerno;
	}

	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	//2
	public String getKiban() {
		return this.kiban;
	}

	public void setKiban(String kiban) {
		this.kiban = kiban;
	}

	//3
	public String getKibanAddr() {
		return this.kibanAddr;
	}
	public void setKibanAddr(String kibanAddr) {
		this.kibanAddr = kibanAddr;
	}

	//4
	public String getConType() {
		return this.conType;
	}

	public void setConType(String conType) {
		this.conType = conType;
	}

	//5
	public Integer getShimukesaki() {
		return this.shimukesaki;
	}

	public void setShimukesaki(Integer shimukesaki) {
		this.shimukesaki = shimukesaki;
	}

	// **««« 2021/02/03 iDEARº J®«ÏXÎ «««**//
	//6
//	public Double getEngineSerno() {
//		return this.engineSerno;
//	}

//	public void setEngineSerno(Double engineSerno) {
//		this.engineSerno = engineSerno;
//	}
	public String getEngineSerno() {
		return this.engineSerno;
	}

	public void setEngineSerno(String engineSerno) {
		this.engineSerno = engineSerno;
	}
	// **ªªª 2021/02/03 iDEARº J®«ÏXÎ ªªª**//

	//7
	public String getEcmNo() {
		return this.ecmNo;
	}

	public void setEcmNo(String ecmNo) {
		this.ecmNo = ecmNo;
	}

	//8
	public String getCtrlABuhinNo() {
		return this.ctrlABuhinNo;
	}

	public void setCtrlABuhinNo(String ctrlABuhinNo) {
		this.ctrlABuhinNo = ctrlABuhinNo;
	}

	//9
	public String getCtrlBBuhinNo() {
		return this.ctrlBBuhinNo;
	}

	public void setCtrlBBuhinNo(String ctrlBBuhinNo) {
		this.ctrlBBuhinNo = ctrlBBuhinNo;
	}

	//10
	public String getCtrlScBuhinNo() {
		return this.ctrlScBuhinNo;
	}

	public void setCtrlScBuhinNo(String ctrlScBuhinNo) {
		this.ctrlScBuhinNo = ctrlScBuhinNo;
	}

	//11
	public Integer getQ1200Serno() {
		return this.q1200Serno;
	}

	public void setQ1200Serno(Integer q1200Serno) {
		this.q1200Serno = q1200Serno;
	}

	//12
	public String getQ4000BuhinNo() {
		return this.q4000BuhinNo;
	}

	public void setQ4000BuhinNo(String q4000BuhinNo) {
		this.q4000BuhinNo = q4000BuhinNo;
	}

	//13
	public Integer getQ4000Serno() {
		return this.q4000Serno;
	}

	public void setQ4000Serno(Integer q4000Serno) {
		this.q4000Serno = q4000Serno;
	}

	//14
	public Integer getCdmaSerno() {
		return this.cdmaSerno;
	}

	public void setCdmaSerno(Integer cdmaSerno) {
		this.cdmaSerno = cdmaSerno;
	}

	//15
	public String getCdmaTelno() {
		return this.cdmaTelno;
	}

	public void setCdmaTelno(String cdmaTelno) {
		this.cdmaTelno = cdmaTelno;
	}

	//16
	public Integer getWakeupHour() {
		return this.wakeupHour;
	}

	public void setWakeupHour(Integer wakeupHour) {
		this.wakeupHour = wakeupHour;
	}

	//17
	public Integer getWakeupMinute() {
		return this.wakeupMinute;
	}

	public void setWakeupMinute(Integer wakeupMinute) {
		this.wakeupMinute = wakeupMinute;
	}

	//18
	public Integer getWakeupSecond() {
		return this.wakeupSecond;
	}

	public void setWakeupSecond(Integer wakeupSecond) {
		this.wakeupSecond = wakeupSecond;
	}

	//19
	public String getSosikiCd() {
		return this.sosikiCd;
	}

	public void setSosikiCd(String sosikiCd) {
		this.sosikiCd = sosikiCd;
	}

//	sosikiTr
	//19
	public String getSosikiTr() {
		return this.sosikiTr;
	}

	public void setSosikiTr(String sosikiTr) {
		this.sosikiTr = sosikiTr;
	}



	//20
	public String getMatomeKishuCd() {
		return this.matomeKishuCd;
	}

	public void setMatomeKishuCd(String matomeKishuCd) {
		this.matomeKishuCd = matomeKishuCd;
	}

	//21
	public String getPromentShubetuCd() {
		return this.promentShubetuCd;
	}

	public void setPromentShubetuCd(String promentShubetuCd) {
		this.promentShubetuCd = promentShubetuCd;
	}

	//22
	public String getModelCd() {
		return this.modelCd;
	}

	public void setModelCd(String modelCd) {
		this.modelCd = modelCd;
	}

	//23
	public Date getGnavStartDate() {
		return this.gnavStartDate;
	}

	public void setGnavStartDate(Date gnavStartDate) {
		this.gnavStartDate = gnavStartDate;
	}

	//24
	public Date getGnavEndDate() {
		return this.gnavEndDate;
	}

	public void setGnavEndDate(Date gnavEndDate) {
		this.gnavEndDate = gnavEndDate;
	}


	//25
	public Date getGnavEndYoteiDate() {
		return this.gnavEndYoteiDate;
	}

	public void setGnavEndYoteiDate(Date gnavEndYoteiDate) {
		this.gnavEndYoteiDate = gnavEndYoteiDate;
	}

	//26
	public String getUserKanriNo() {
		return this.userKanriNo;
	}

	public void setUserKanriNo(String userKanriNo) {
		this.userKanriNo = userKanriNo;
	}

	//27
	public Integer getKishu() {
		return this.kishu;
	}

	public void setKishu(Integer kishu) {
		this.kishu = kishu;
	}

	//28
	public Integer getTounanBosiStatus() {
		return this.tounanBosiStatus;
	}

	public void setTounanBosiStatus(Integer tounanBosiStatus) {
		this.tounanBosiStatus = tounanBosiStatus;
	}

	//29
	public Integer getCurfewStatus() {
		return this.curfewStatus;
	}

	public void setCurfewStatus(Integer curfewStatus) {
		this.curfewStatus = curfewStatus;
	}

	//30
	public Integer getGeofenceStatus() {
		return this.geofenceStatus;
	}

	public void setGeofenceStatus(Integer geofenceStatus) {
		this.geofenceStatus = geofenceStatus;
	}

	//31
	public Integer getFlgNenryoMitsudo() {
		return this.flgNenryoMitsudo;
	}

	public void setFlgNenryoMitsudo(Integer flgNenryoMitsudo) {
		this.flgNenryoMitsudo = flgNenryoMitsudo;
	}

	//32
	public Timestamp getTeijiKadobi() {
		return this.teijiKadobi;
	}

	public void setTeijiKadobi(Timestamp teijiKadobi) {
		this.teijiKadobi = teijiKadobi;
	}

	//33
	public Timestamp getTeikiKadobi() {
		return this.teikiKadobi;
	}

	public void setTeikiKadobi(Timestamp teikiKadobi) {
		this.teikiKadobi = teikiKadobi;
	}

	//34
	public Timestamp getRecvTimestamp() {
		return this.recvTimestamp;
	}

	public void setRecvTimestamp(Timestamp recvTimestamp) {
		this.recvTimestamp = recvTimestamp;
	}

	//35
	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	//36
	public Integer getKoshoCounter() {
		return this.koshoCounter;
	}

	public void setKoshoCounter(Integer koshoCounter) {
		this.koshoCounter = koshoCounter;
	}

	//37
	public Double getNewIdo() {
		return this.newIdo;
	}

	public void setNewIdo(Double newIdo) {
		this.newIdo = newIdo;
	}

	//38
	public Double getNewKeido() {
		return this.newKeido;
	}

	public void setNewKeido(Double newKeido) {
		this.newKeido = newKeido;
	}

	//39
	public Double getNewHourMeter() {
		return this.newHourMeter;
	}

	public void setNewHourMeter(Double newHourMeter) {
		this.newHourMeter = newHourMeter;
	}

	//40
	public Integer getNewKikaiTime() {
		return this.newKikaiTime;
	}

	public void setNewKikaiTime(Integer newKikaiTime) {
		this.newKikaiTime = newKikaiTime;
	}

	//41
	public Integer getNewUwamonoTime() {
		return this.newUwamonoTime;
	}

	public void setNewUwamonoTime(Integer newUwamonoTime) {
		this.newUwamonoTime = newUwamonoTime;
	}

	//42
	public Integer getNewSenkaiTime() {
		return this.newSenkaiTime;
	}

	public void setNewSenkaiTime(Integer newSenkaiTime) {
		this.newSenkaiTime = newSenkaiTime;
	}

	//43
	public Integer getNewSokoTime() {
		return this.newSokoTime;
	}

	public void setNewSokoTime(Integer newSokoTime) {
		this.newSokoTime = newSokoTime;
	}

	//44
	public Integer getNewSpModeTime() {
		return this.newSpModeTime;
	}

	public void setNewSpModeTime(Integer newSpModeTime) {
		this.newSpModeTime = newSpModeTime;
	}

	//45
	public Double getNewBreakerTime() {
		return this.newBreakerTime;
	}

	public void setNewBreakerTime(Double newBreakerTime) {
		this.newBreakerTime = newBreakerTime;
	}

	//46
	public Integer getNewHasaiTime() {
		return this.newHasaiTime;
	}

	public void setNewHasaiTime(Integer newHasaiTime) {
		this.newHasaiTime = newHasaiTime;
	}

	//47
	public Integer getNewAModeTime() {
		return this.newAModeTime;
	}

	public void setNewAModeTime(Integer newAModeTime) {
		this.newAModeTime = newAModeTime;
	}

	//48
	public Integer getNewGurenTime() {
		return this.newGurenTime;
	}

	public void setNewGurenTime(Integer newGurenTime) {
		this.newGurenTime = newGurenTime;
	}

	//49
	public Double getNewNenryoL02000() {
		return this.newNenryoL02000;
	}

	public void setNewNenryoL02000(Double newNenryoL02000) {
		this.newNenryoL02000 = newNenryoL02000;
	}

	//50
	public Timestamp getNonyubi() {
		return this.nonyubi;
	}

	public void setNonyubi(Timestamp nonyubi) {
		this.nonyubi = nonyubi;
	}

	//51
	public String getShogaiFollow() {
		return this.shogaiFollow;
	}

	public void setShogaiFollow(String shogaiFollow) {
		this.shogaiFollow = shogaiFollow;
	}

	//52
	public Timestamp getHajimebi() {
		return this.hajimebi;
	}

	public void setHajimebi(Timestamp hajimebi) {
		this.hajimebi = hajimebi;
	}

	//53
	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	//54
	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	//55
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	//56
	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	//57
	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	//58
	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	//59
	public Integer getTounanTuisekiStatus() {
		return this.tounanTuisekiStatus;
	}

	public void setTounanTuisekiStatus(Integer tounanTuisekiStatus) {
		this.tounanTuisekiStatus = tounanTuisekiStatus;
	}

	//60
	public Date getPromentKaisiBi() {
		return this.promentKaisiBi;
	}

	public void setPromentKaisiBi(Date promentKaisiBi) {
		this.promentKaisiBi = promentKaisiBi;
	}

	//61
	public Integer getCommandKaisu() {
		return this.commandKaisu;
	}

	public void setCommandKaisu(Integer commandKaisu) {
		this.commandKaisu = commandKaisu;
	}

	//62
	public String getCampaign() {
		return this.campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	//63
	public Integer getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	//64
	public String getCtrlTBuhinNo() {
		return this.ctrlTBuhinNo;
	}

	public void setCtrlTBuhinNo(String ctrlTBuhinNo) {
		this.ctrlTBuhinNo = ctrlTBuhinNo;
	}

	//65
	public Integer getMaintainceKbn() {
		return this.maintainceKbn;
	}

	public void setMaintainceKbn(Integer maintainceKbn) {
		this.maintainceKbn = maintainceKbn;
	}

	//66
	public String getMonitorRushNo() {
		return this.monitorRushNo;
	}

	public void setMonitorRushNo(String monitorRushNo) {
		this.monitorRushNo = monitorRushNo;
	}

	//67
	public String getMonitorFirmNo() {
		return this.monitorFirmNo;
	}

	public void setMonitorFirmNo(String monitorFirmNo) {
		this.monitorFirmNo = monitorFirmNo;
	}

	//68
	public String getServiceKoujouCd() {
		return this.serviceKoujouCd;
	}

	public void setServiceKoujouCd(String serviceKoujouCd) {
		this.serviceKoujouCd = serviceKoujouCd;
	}

	//69
	public Integer getMachineModel() {
		return this.machineModel;
	}

	public void setMachineModel(Integer machineModel) {
		this.machineModel = machineModel;
	}

	//70
	public Integer getNewHighLoadPer() {
		return this.newHighLoadPer;
	}

	public void setNewHighLoadPer(Integer newHighLoadPer) {
		this.newHighLoadPer = newHighLoadPer;
	}


	//71
	public Integer getNewHighSuionPer() {
		return this.newHighSuionPer;
	}

	public void setNewHighSuionPer(Integer newHighSuionPer) {
		this.newHighSuionPer = newHighSuionPer;
	}

	//72
	public Integer getNewMidSuionPer() {
		return this.newMidSuionPer;
	}

	public void setNewMidSuionPer(Integer newMidSuionPer) {
		this.newMidSuionPer = newMidSuionPer;
	}

	//73
	public Integer getRemoteLock() {
		return this.remoteLock;
	}

	public void setRemoteLock(Integer remoteLock) {
		this.remoteLock = remoteLock;
	}

	//74
	public Integer getNenryoLv() {
		return this.nenryoLv;
	}

	public void setNenryoLv(Integer nenryoLv) {
		this.nenryoLv = nenryoLv;
	}

	//75
	public Integer getKeyihouHasseyiTyuCount() {
		return this.keyihouHasseyiTyuCount;
	}

	public void setKeyihouHasseyiTyuCount(Integer keyihouHasseyiTyuCount) {
		this.keyihouHasseyiTyuCount = keyihouHasseyiTyuCount;
	}

	//76
	public Integer getYokokuCount() {
		return this.yokokuCount;
	}

	public void setYokokuCount(Integer yokokuCount) {
		this.yokokuCount = yokokuCount;
	}

	//77
	public Integer getKeyikokuCount() {
		return this.keyikokuCount;
	}

	public void setKeyikokuCount(Integer keyikokuCount) {
		this.keyikokuCount = keyikokuCount;
	}

	//78
	public Integer getJisa() {
		return this.jisa;
	}

	public void setJisa(Integer jisa) {
		this.jisa = jisa;
	}

	//79
	public Integer getEventStates() {
		return this.eventStates;
	}

	public void setEventStates(Integer eventStates) {
		this.eventStates = eventStates;
	}

	//80
	public Integer getEventNasiCnt() {
		return this.eventNasiCnt;
	}

	public void setEventNasiCnt(Integer eventNasiCnt) {
		this.eventNasiCnt = eventNasiCnt;
	}

	//81
	public Integer getEventTujyoCnt() {
		return this.eventTujyoCnt;
	}

	public void setEventTujyoCnt(Integer eventTujyoCnt) {
		this.eventTujyoCnt = eventTujyoCnt;
	}

	//82
	public Integer getEventKinkyuCnt() {
		return this.eventKinkyuCnt;
	}

	public void setEventKinkyuCnt(Integer eventKinkyuCnt) {
		this.eventKinkyuCnt = eventKinkyuCnt;
	}

	//83
	public Integer getEventMitaiouCnt() {
		return this.eventMitaiouCnt;
	}

	public void setEventMitaiouCnt(Integer eventMitaiouCnt) {
		this.eventMitaiouCnt = eventMitaiouCnt;
	}

	//84
	public Integer getEventYousumiCnt() {
		return this.eventYousumiCnt;
	}

	public void setEventYousumiCnt(Integer eventYousumiCnt) {
		this.eventYousumiCnt = eventYousumiCnt;
	}

	//85
	public Integer getEventTaiouCnt() {
		return this.eventTaiouCnt;
	}

	public void setEventTaiouCnt(Integer eventTaiouCnt) {
		this.eventTaiouCnt = eventTaiouCnt;
	}

	//86
	public Integer getLiftingMagnetCrane() {
		return this.liftingMagnetCrane;
	}

	public void setLiftingMagnetCrane(Integer liftingMagnetCrane) {
		this.liftingMagnetCrane = liftingMagnetCrane;
	}

	//87
	public Integer getAspirationsNum() {
		return this.aspirationsNum;
	}

	public void setAspirationsNum(Integer aspirationsNum) {
		this.aspirationsNum = aspirationsNum;
	}

	//88
	public Integer getRanSyudouSaiseiStartNum() {
		return this.ranSyudouSaiseiStartNum;
	}

	public void setRanSyudouSaiseiStartNum(Integer ranSyudouSaiseiStartNum) {
		this.ranSyudouSaiseiStartNum = ranSyudouSaiseiStartNum;
	}

	//89
	public Integer getReqSyudouSaiseiStartNum() {
		return this.reqSyudouSaiseiStartNum;
	}

	public void setReqSyudouSaiseiStartNum(Integer reqSyudouSaiseiStartNum) {
		this.reqSyudouSaiseiStartNum = reqSyudouSaiseiStartNum;
	}

	//90
	public Integer getSyudouSaiseiStartNum() {
		return this.syudouSaiseiStartNum;
	}

	public void setSyudouSaiseiStartNum(Integer syudouSaiseiStartNum) {
		this.syudouSaiseiStartNum = syudouSaiseiStartNum;
	}

	//91
	public Integer getSyudouSaiseiOverNum() {
		return this.syudouSaiseiOverNum;
	}

	public void setSyudouSaiseiOverNum(Integer syudouSaiseiOverNum) {
		this.syudouSaiseiOverNum = syudouSaiseiOverNum;
	}

	//92
	public Integer getJidouSaiseiStartNum() {
		return this.jidouSaiseiStartNum;
	}

	public void setJidouSaiseiStartNum(Integer jidouSaiseiStartNum) {
		this.jidouSaiseiStartNum = jidouSaiseiStartNum;
	}

	//93
	public Integer getJidouSaiseiOverNum() {
		return this.jidouSaiseiOverNum;
	}

	public void setJidouSaiseiOverNum(Integer jidouSaiseiOverNum) {
		this.jidouSaiseiOverNum = jidouSaiseiOverNum;
	}

	//94
	public Integer getHTime() {
		return this.hTime;
	}

	public void setHTime(Integer hTime) {
		this.hTime = hTime;
	}

	//95
	public Timestamp getTourokuTimestamp() {
		return this.tourokuTimestamp;
	}

	public void setTourokuTimestamp(Timestamp tourokuTimestamp) {
		this.tourokuTimestamp = tourokuTimestamp;
	}

	//96
	public Integer getRtcStartTime() {
		return this.rtcStartTime;
	}

	public void setRtcStartTime(Integer rtcStartTime) {
		this.rtcStartTime = rtcStartTime;
	}

	//97
	public Integer getRtcEndTime() {
		return this.rtcEndTime;
	}

	public void setRtcEndTime(Integer rtcEndTime) {
		this.rtcEndTime = rtcEndTime;
	}

	//98
	public Timestamp getCurfewRegistDtm() {
		return this.curfewRegistDtm;
	}

	public void setCurfewRegistDtm(Timestamp curfewRegistDtm) {
		this.curfewRegistDtm = curfewRegistDtm;
	}

	//99
	public Timestamp getRemodeRegistDtm() {
		return this.remodeRegistDtm;
	}

	public void setRemodeRegistDtm(Timestamp remodeRegistDtm) {
		this.remodeRegistDtm = remodeRegistDtm;
	}

	//100
	public Integer getSetIdo() {
		return this.setIdo;
	}

	public void setSetIdo(Integer setIdo) {
		this.setIdo = setIdo;
	}

	//101
	public Integer getSetKeido() {
		return this.setKeido;
	}

	public void setSetKeido(Integer setKeido) {
		this.setKeido = setKeido;
	}

	//102
	public String getSetRange() {
		return this.setRange;
	}

	public void setSetRange(String setRange) {
		this.setRange = setRange;
	}

	//103
	public String getSetReleasePwd() {
		return this.setReleasePwd;
	}

	public void setSetReleasePwd(String setReleasePwd) {
		this.setReleasePwd = setReleasePwd;
	}

	//104
	public String getSetPasswordLastuser() {
		return this.setPasswordLastuser;
	}

	public void setSetPasswordLastuser(String setPasswordLastuser) {
		this.setPasswordLastuser = setPasswordLastuser;
	}

	//105
	public Integer getFlgNeedJisa() {
		return this.flgNeedJisa;
	}

	public void setFlgNeedJisa(Integer flgNeedJisa) {
		this.flgNeedJisa = flgNeedJisa;
	}

	//106
	public String getToukatsubuCd() {
		return this.toukatsubuCd;
	}

	public void setToukatsubuCd(String toukatsubuCd) {
		this.toukatsubuCd = toukatsubuCd;
	}

	//107
	public String getKyotenCd() {
		return this.kyotenCd;
	}

	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

	//108
	public Integer getNewConveyer() {
		return this.newConveyer;
	}

	public void setNewConveyer(Integer newConveyer) {
		this.newConveyer = newConveyer;
	}

	//109
	public Integer getNewKanetsuTime() {
		return this.newKanetsuTime;
	}

	public void setNewKanetsuTime(Integer newKanetsuTime) {
		this.newKanetsuTime = newKanetsuTime;
	}

	//110
	public Integer getNewKanetsuModeTime() {
		return this.newKanetsuModeTime;
	}

	public void setNewKanetsuModeTime(Integer newKanetsuModeTime) {
		this.newKanetsuModeTime = newKanetsuModeTime;
	}

	//111
	public Integer getNewKanetsuKaisu() {
		return this.newKanetsuKaisu;
	}

	public void setNewKanetsuKaisu(Integer newKanetsuKaisu) {
		this.newKanetsuKaisu = newKanetsuKaisu;
	}

	//112
	public Integer getNewKousokuModeSokoTime() {
		return this.newKousokuModeSokoTime;
	}

	public void setNewKousokuModeSokoTime(Integer newKousokuModeSokoTime) {
		this.newKousokuModeSokoTime = newKousokuModeSokoTime;
	}

	//113
	public Integer getNewTeisokuModeSokoTime() {
		return this.newTeisokuModeSokoTime;
	}

	public void setNewTeisokuModeSokoTime(Integer newTeisokuModeSokoTime) {
		this.newTeisokuModeSokoTime = newTeisokuModeSokoTime;
	}

	//114
	public Integer getNewTeisoku4wdModeSokoTime() {
		return this.newTeisoku4wdModeSokoTime;
	}

	public void setNewTeisoku4wdModeSokoTime(Integer newTeisoku4wdModeSokoTime) {
		this.newTeisoku4wdModeSokoTime = newTeisoku4wdModeSokoTime;
	}

	//115
	public Double getNewShikouTime() {
		return this.newShikouTime;
	}

	public void setNewShikouTime(Double newShikouTime) {
		this.newShikouTime = newShikouTime;
	}

	//116
	public Integer getNewShikouKyouri() {
		return this.newShikouKyouri;
	}

	public void setNewShikouKyouri(Integer newShikouKyouri) {
		this.newShikouKyouri = newShikouKyouri;
	}

	//117
	public Integer getNewKousokuModeIdouKyouri() {
		return this.newKousokuModeIdouKyouri;
	}

	public void setNewKousokuModeIdouKyouri(Integer newKousokuModeIdouKyouri) {
		this.newKousokuModeIdouKyouri = newKousokuModeIdouKyouri;
	}

	//118
	public Integer getNewKourinSlipKaisu() {
		return this.newKourinSlipKaisu;
	}

	public void setNewKourinSlipKaisu(Integer newKourinSlipKaisu) {
		this.newKourinSlipKaisu = newKourinSlipKaisu;
	}

	//119
	public String getLbxKiban() {
		return this.lbxKiban;
	}

	public void setLbxKiban(String lbxKiban) {
		this.lbxKiban = lbxKiban;
	}

	//120
	public String getX4ControllerSerialNo() {
		return this.x4ControllerSerialNo;
	}

	public void setX4ControllerSerialNo(String x4ControllerSerialNo) {
		this.x4ControllerSerialNo = x4ControllerSerialNo;
	}

	//121
	public String getX4ControllerPartNo() {
		return this.x4ControllerPartNo;
	}

	public void setX4ControllerPartNo(String x4ControllerPartNo) {
		this.x4ControllerPartNo = x4ControllerPartNo;
	}

	//122
	public String getX4SubControllerPartNo() {
		return this.x4SubControllerPartNo;
	}

	public void setX4SubControllerPartNo(String x4SubControllerPartNo) {
		this.x4SubControllerPartNo = x4SubControllerPartNo;
	}

	//123
	public Integer getX4BackScreenLock() {
		return this.x4BackScreenLock;
	}

	public void setX4BackScreenLock(Integer x4BackScreenLock) {
		this.x4BackScreenLock = x4BackScreenLock;
	}

	//124
	public String getDairitennCd() {
		return this.dairitennCd;
	}

	public void setDairitennCd(String dairitennCd) {
		this.dairitennCd = dairitennCd;
	}

	//125
	public String getLbxModelCd() {
		return this.lbxModelCd;
	}

	public void setLbxModelCd(String lbxModelCd) {
		this.lbxModelCd = lbxModelCd;
	}

	//126
	public Integer getEngineRunTimeVoltage() {
		return this.engineRunTimeVoltage;
	}

	public void setEngineRunTimeVoltage(Integer engineRunTimeVoltage) {
		this.engineRunTimeVoltage = engineRunTimeVoltage;
	}

	//127
	public Integer getKonAfterBatiryVoltage() {
		return this.konAfterBatiryVoltage;
	}

	public void setKonAfterBatiryVoltage(Integer konAfterBatiryVoltage) {
		this.konAfterBatiryVoltage = konAfterBatiryVoltage;
	}

	//128
	public Integer getUreaWaterLevel() {
		return this.ureaWaterLevel;
	}

	public void setUreaWaterLevel(Integer ureaWaterLevel) {
		this.ureaWaterLevel = ureaWaterLevel;
	}

	//129
	public Double getUreaWaterConsum() {
		return this.ureaWaterConsum;
	}

	public void setUreaWaterConsum(Double ureaWaterConsum) {
		this.ureaWaterConsum = ureaWaterConsum;
	}

	//130
	public Integer getManualLock() {
		return this.manualLock;
	}

	public void setManualLock(Integer manualLock) {
		this.manualLock = manualLock;
	}

	//131
	public String getPositionEn() {
		return this.positionEn;
	}

	public void setPositionEn(String positionEn) {
		this.positionEn = positionEn;
	}

	//132
	public String getHbControllerPartNo() {
		return this.hbControllerPartNo;
	}

	public void setHbControllerPartNo(String hbControllerPartNo) {
		this.hbControllerPartNo = hbControllerPartNo;
	}

	//133
	public String getHbInverterPartNo() {
		return this.hbInverterPartNo;
	}

	public void setHbInverterPartNo(String hbInverterPartNo) {
		this.hbInverterPartNo = hbInverterPartNo;
	}

	//134
	public String getHbConverterPartNo() {
		return this.hbConverterPartNo;
	}

	public void setHbConverterPartNo(String hbConverterPartNo) {
		this.hbConverterPartNo = hbConverterPartNo;
	}

	//135
	public String getHbBmuPartNo() {
		return this.hbBmuPartNo;
	}

	public void setHbBmuPartNo(String hbBmuPartNo) {
		this.hbBmuPartNo = hbBmuPartNo;
	}

	//136
	public Integer getBoomOperationTime() {
		return this.boomOperationTime;
	}

	public void setBoomOperationTime(Integer boomOperationTime) {
		this.boomOperationTime = boomOperationTime;
	}

	//137
	public Integer getArmOperationTime() {
		return this.armOperationTime;
	}

	public void setArmOperationTime(Integer armOperationTime) {
		this.armOperationTime = armOperationTime;
	}

	//138
	public Integer getBucketOperationTime() {
		return this.bucketOperationTime;
	}

	public void setBucketOperationTime(Integer bucketOperationTime) {
		this.bucketOperationTime = bucketOperationTime;
	}

	//139
	public Integer getEmergencyStopNum() {
		return this.emergencyStopNum;
	}

	public void setEmergencyStopNum(Integer emergencyStopNum) {
		this.emergencyStopNum = emergencyStopNum;
	}

	//140
	public Integer getBrakeNum() {
		return this.brakeNum;
	}

	public void setBrakeNum(Integer brakeNum) {
		this.brakeNum = brakeNum;
	}

	//141
	public String getShortKatasiki() {
		return this.shortKatasiki;
	}

	public void setShortKatasiki(String shortKatasiki) {
		this.shortKatasiki = shortKatasiki;
	}

	//142
	public Integer getDashFlg() {
		return this.dashFlg;
	}

	public void setDashFlg(Integer dashFlg) {
		this.dashFlg = dashFlg;
	}

	//143
	public Integer getNewWarningLevel() {
		return this.newWarningLevel;
	}

	public void setNewWarningLevel(Integer newWarningLevel) {
		this.newWarningLevel = newWarningLevel;
	}

	//144
	public Integer getEnforceLockUserLevel() {
		return this.enforceLockUserLevel;
	}

	public void setEnforceLockUserLevel(Integer enforceLockUserLevel) {
		this.enforceLockUserLevel = enforceLockUserLevel;
	}

	//145
	public Integer getEnforceLockSetFlg() {
		return this.enforceLockSetFlg;
	}

	public void setEnforceLockSetFlg(Integer enforceLockSetFlg) {
		this.enforceLockSetFlg = enforceLockSetFlg;
	}

	//146
	public Integer getVersionChangedFlg() {
		return this.versionChangedFlg;
	}

	public void setVersionChangedFlg(Integer versionChangedFlg) {
		this.versionChangedFlg = versionChangedFlg;
	}

	//147
	public Integer getNoticeTime() {
		return this.noticeTime;
	}

	public void setNoticeTime(Integer noticeTime) {
		this.noticeTime = noticeTime;
	}

	//148
	public Integer getNewVersionFlg() {
		return this.newVersionFlg;
	}

	public void setNewVersionFlg(Integer newVersionFlg) {
		this.newVersionFlg = newVersionFlg;
	}

	//149
	public Integer getNewCtlraKoshoFlg() {
		return this.newCtlraKoshoFlg;
	}

	public void setNewCtlraKoshoFlg(Integer newCtlraKoshoFlg) {
		this.newCtlraKoshoFlg = newCtlraKoshoFlg;
	}

	//150
	public Integer getNewQ4000KoshoFlg() {
		return this.newQ4000KoshoFlg;
	}

	public void setNewQ4000KoshoFlg(Integer newQ4000KoshoFlg) {
		this.newQ4000KoshoFlg = newQ4000KoshoFlg;
	}

	//151
	public String getEngineModel() {
		return this.engineModel;
	}

	public void setEngineModel(String engineModel) {
		this.engineModel = engineModel;
	}

	//152
	public String getPartsX4SubBuhin1() {
		return this.partsX4SubBuhin1;
	}

	public void setPartsX4SubBuhin1(String partsX4SubBuhin1) {
		this.partsX4SubBuhin1 = partsX4SubBuhin1;
	}

	//153
	public String getPartsX4SubBuhin2() {
		return this.partsX4SubBuhin2;
	}

	public void setPartsX4SubBuhin2(String partsX4SubBuhin2) {
		this.partsX4SubBuhin2 = partsX4SubBuhin2;
	}

	//154
	public String getPartsX4SubBuhin3() {
		return this.partsX4SubBuhin3;
	}

	public void setPartsX4SubBuhin3(String partsX4SubBuhin3) {
		this.partsX4SubBuhin3 = partsX4SubBuhin3;
	}

	//155
	public String getPartsX4SubSer1() {
		return this.partsX4SubSer1;
	}

	public void setPartsX4SubSer1(String partsX4SubSer1) {
		this.partsX4SubSer1 = partsX4SubSer1;
	}

	//156
	public String getPartsX4SubSer2() {
		return this.partsX4SubSer2;
	}

	public void setPartsX4SubSer2(String partsX4SubSer2) {
		this.partsX4SubSer2 = partsX4SubSer2;
	}

	//157
	public String getPartsX4SubSer3() {
		return this.partsX4SubSer3;
	}

	public void setPartsX4SubSer3(String partsX4SubSer3) {
		this.partsX4SubSer3 = partsX4SubSer3;
	}

	//158
	public String getPartsTSoftware() {
		return this.partsTSoftware;
	}

	public void setPartsTSoftware(String partsTSoftware) {
		this.partsTSoftware = partsTSoftware;
	}

	//159
	public String getMonitorSerNo() {
		return this.monitorSerNo;
	}

	public void setMonitorSerNo(String monitorSerNo) {
		this.monitorSerNo = monitorSerNo;
	}

	//160
	public Integer getBland() {
		return this.bland;
	}

	public void setBland(Integer bland) {
		this.bland = bland;
	}

	//161
	public Integer getAttachmentType() {
		return this.attachmentType;
	}

	public void setAttachmentType(Integer attachmentType) {
		this.attachmentType = attachmentType;
	}

	//162
	public Integer getFirstLineCircuit() {
		return this.firstLineCircuit;
	}

	public void setFirstLineCircuit(Integer firstLineCircuit) {
		this.firstLineCircuit = firstLineCircuit;
	}

	//163
	public Integer getElectricAdjustment() {
		return this.electricAdjustment;
	}

	public void setElectricAdjustment(Integer electricAdjustment) {
		this.electricAdjustment = electricAdjustment;
	}

	//164
	public Integer getOperatingSystem() {
		return this.operatingSystem;
	}

	public void setOperatingSystem(Integer operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	//165
	public Integer getSecondLineCircuit() {
		return this.secondLineCircuit;
	}

	public void setSecondLineCircuit(Integer secondLineCircuit) {
		this.secondLineCircuit = secondLineCircuit;
	}

	//166
	public Integer getQuickCoupler() {
		return this.quickCoupler;
	}

	public void setQuickCoupler(Integer quickCoupler) {
		this.quickCoupler = quickCoupler;
	}

	//167
	public Integer getOverloadAlarm() {
		return this.overloadAlarm;
	}

	public void setOverloadAlarm(Integer overloadAlarm) {
		this.overloadAlarm = overloadAlarm;
	}

	//168
	public Integer getInterferencePrevention() {
		return this.interferencePrevention;
	}

	public void setInterferencePrevention(Integer interferencePrevention) {
		this.interferencePrevention = interferencePrevention;
	}

	//169
	public Integer getFreeSwing() {
		return this.freeSwing;
	}

	public void setFreeSwing(Integer freeSwing) {
		this.freeSwing = freeSwing;
	}

	//170
	public Integer getOnePedalRunnning() {
		return this.onePedalRunnning;
	}

	public void setOnePedalRunnning(Integer onePedalRunnning) {
		this.onePedalRunnning = onePedalRunnning;
	}

	//171
	public Integer getRunnningAlarm() {
		return this.runnningAlarm;
	}

	public void setRunnningAlarm(Integer runnningAlarm) {
		this.runnningAlarm = runnningAlarm;
	}

	//172
	public Integer getAutoStopPumpOil() {
		return this.autoStopPumpOil;
	}

	public void setAutoStopPumpOil(Integer autoStopPumpOil) {
		this.autoStopPumpOil = autoStopPumpOil;
	}

	//173
	public Integer getElevatorCab() {
		return this.elevatorCab;
	}

	public void setElevatorCab(Integer elevatorCab) {
		this.elevatorCab = elevatorCab;
	}

	//174
	public Double getLiftingMagnetTime() {
		return this.liftingMagnetTime;
	}

	public void setLiftingMagnetTime(Double liftingMagnetTime) {
		this.liftingMagnetTime = liftingMagnetTime;
	}

	//175
	public Integer getAppMachine() {
		return this.appMachine;
	}

	public void setAppMachine(Integer appMachine) {
		this.appMachine = appMachine;
	}

	//176
	public Integer getFan() {
		return this.fan;
	}

	public void setFan(Integer fan) {
		this.fan = fan;
	}

	//177
	public Integer getInfoConstruction() {
		return this.infoConstruction;
	}

	public void setInfoConstruction(Integer infoConstruction) {
		this.infoConstruction = infoConstruction;
	}

	//178
	public Integer getBucketSensor() {
		return this.bucketSensor;
	}

	public void setBucketSensor(Integer bucketSensor) {
		this.bucketSensor = bucketSensor;
	}

	//179
	public Integer getGrappleFloating() {
		return this.grappleFloating;
	}

	public void setGrappleFloating(Integer grappleFloating) {
		this.grappleFloating = grappleFloating;
	}

	//180
	public Integer getVersionFlg() {
		return this.versionFlg;
	}

	public void setVersionFlg(Integer versionFlg) {
		this.versionFlg = versionFlg;
	}

	//181
	public Integer getTeikiFlg() {
		return this.teikiFlg;
	}

	public void setTeikiFlg(Integer teikiFlg) {
		this.teikiFlg = teikiFlg;
	}

	//182
	public Integer getDocomoFlg() {
		return this.docomoFlg;
	}

	public void setDocomoFlg(Integer docomoFlg) {
		this.docomoFlg = docomoFlg;
	}

	//183
	public String getLeftMonitorRushNo() {
		return this.leftMonitorRushNo;
	}

	public void setLeftMonitorRushNo(String leftMonitorRushNo) {
		this.leftMonitorRushNo = leftMonitorRushNo;
	}

	//184
	public String getRightMonitorRushNo() {
		return this.rightMonitorRushNo;
	}

	public void setRightMonitorRushNo(String rightMonitorRushNo) {
		this.rightMonitorRushNo = rightMonitorRushNo;
	}

	//185
	public String getMainMonitorNo() {
		return this.mainMonitorNo;
	}

	public void setMainMonitorNo(String mainMonitorNo) {
		this.mainMonitorNo = mainMonitorNo;
	}

	//186
	public Integer getHaigasuTaiou() {
		return this.haigasuTaiou;
	}

	public void setHaigasuTaiou(Integer haigasuTaiou) {
		this.haigasuTaiou = haigasuTaiou;
	}

	//187
	public Integer getKanetsuShiyo() {
		return this.kanetsuShiyo;
	}

	public void setKanetsuShiyo(Integer kanetsuShiyo) {
		this.kanetsuShiyo = kanetsuShiyo;
	}

	//188
	public Integer getShimeGatameShiyo() {
		return this.shimeGatameShiyo;
	}

	public void setShimeGatameShiyo(Integer shimeGatameShiyo) {
		this.shimeGatameShiyo = shimeGatameShiyo;
	}

	//189
	public Integer getDenkiKanetsuSettei() {
		return this.denkiKanetsuSettei;
	}

	public void setDenkiKanetsuSettei(Integer denkiKanetsuSettei) {
		this.denkiKanetsuSettei = denkiKanetsuSettei;
	}

	//190
	public Integer getBrakeSteer() {
		return this.brakeSteer;
	}

	public void setBrakeSteer(Integer brakeSteer) {
		this.brakeSteer = brakeSteer;
	}


	//191
	public Integer getZenrinKudoTyosei() {
		return this.zenrinKudoTyosei;
	}

	public void setZenrinKudoTyosei(Integer zenrinKudoTyosei) {
		this.zenrinKudoTyosei = zenrinKudoTyosei;
	}

	//192
	public Integer getClimbLock() {
		return this.climbLock;
	}

	public void setClimbLock(Integer climbLock) {
		this.climbLock = climbLock;
	}

	//193
	public String getIctControllerFirmVer() {
		return this.ictControllerFirmVer;
	}

	public void setIctControllerFirmVer(String ictControllerFirmVer) {
		this.ictControllerFirmVer = ictControllerFirmVer;
	}

	//194
	public String getBoomImuFirmVer() {
		return this.boomImuFirmVer;
	}

	public void setBoomImuFirmVer(String boomImuFirmVer) {
		this.boomImuFirmVer = boomImuFirmVer;
	}

	//195
	public String getArmImuFirmVer() {
		return this.armImuFirmVer;
	}

	public void setArmImuFirmVer(String armImuFirmVer) {
		this.armImuFirmVer = armImuFirmVer;
	}

	//196
	public String getBucketImuFirmVer() {
		return this.bucketImuFirmVer;
	}

	public void setBucketImuFirmVer(String bucketImuFirmVer) {
		this.bucketImuFirmVer = bucketImuFirmVer;
	}

	//197
	public String getIctControllerSer() {
		return this.ictControllerSer;
	}

	public void setIctControllerSer(String ictControllerSer) {
		this.ictControllerSer = ictControllerSer;
	}

	//198
	public String getBoomImuSer() {
		return this.boomImuSer;
	}

	public void setBoomImuSer(String boomImuSer) {
		this.boomImuSer = boomImuSer;
	}

	//199
	public String getArmImuSer() {
		return this.armImuSer;
	}

	public void setArmImuSer(String armImuSer) {
		this.armImuSer = armImuSer;
	}

	//200
	public String getBucketImuSer() {
		return this.bucketImuSer;
	}

	public void setBucketImuSer(String bucketImuSer) {
		this.bucketImuSer = bucketImuSer;
	}

	//201
	public String getHbControllerSerNo() {
		return this.hbControllerSerNo;
	}

	public void setHbControllerSerNo(String hbControllerSerNo) {
		this.hbControllerSerNo = hbControllerSerNo;
	}


	//««« 2019/09/30 iDEARº }X^JÇÁ :start  «««//

	public Integer getFvm2() {
		return fvm2;
	}
	public void setFvm2(Integer fvm2) {
		this.fvm2 = fvm2;
	}
	public Integer getRangeLimitValidTime() {
		return rangeLimitValidTime;
	}
	public void setRangeLimitValidTime(Integer rangeLimitValidTime) {
		this.rangeLimitValidTime = rangeLimitValidTime;
	}
	public Integer getHgtDptLimitValidTime() {
		return hgtDptLimitValidTime;
	}
	public void setHgtDptLimitValidTime(Integer hgtDptLimitValidTime) {
		this.hgtDptLimitValidTime = hgtDptLimitValidTime;
	}
	public Integer getHeightLimitValidTime() {
		return heightLimitValidTime;
	}
	public void setHeightLimitValidTime(Integer heightLimitValidTime) {
		this.heightLimitValidTime = heightLimitValidTime;
	}
	public Integer getDepthLimitValidTime() {
		return depthLimitValidTime;
	}
	public void setDepthLimitValidTime(Integer depthLimitValidTime) {
		this.depthLimitValidTime = depthLimitValidTime;
	}
	public Integer getFvm2NoticeValidTime() {
		return fvm2NoticeValidTime;
	}
	public void setFvm2NoticeValidTime(Integer fvm2NoticeValidTime) {
		this.fvm2NoticeValidTime = fvm2NoticeValidTime;
	}
	public String getLaserFirmVer() {
		return laserFirmVer;
	}
	public void setLaserFirmVer(String laserFirmVer) {
		this.laserFirmVer = laserFirmVer;
	}
	public String getHeadingSensorFirmVer() {
		return headingSensorFirmVer;
	}
	public void setHeadingSensorFirmVer(String headingSensorFirmVer) {
		this.headingSensorFirmVer = headingSensorFirmVer;
	}
	public String getIctMonitorFirmVer() {
		return ictMonitorFirmVer;
	}
	public void setIctMonitorFirmVer(String ictMonitorFirmVer) {
		this.ictMonitorFirmVer = ictMonitorFirmVer;
	}
	public String getMcValveModuleFirmVer() {
		return mcValveModuleFirmVer;
	}
	public void setMcValveModuleFirmVer(String mcValveModuleFirmVer) {
		this.mcValveModuleFirmVer = mcValveModuleFirmVer;
	}
	public String getLaserSerNo() {
		return laserSerNo;
	}
	public void setLaserSerNo(String laserSerNo) {
		this.laserSerNo = laserSerNo;
	}
	public String getHeadingSensorSerNo() {
		return headingSensorSerNo;
	}
	public void setHeadingSensorSerNo(String headingSensorSerNo) {
		this.headingSensorSerNo = headingSensorSerNo;
	}
	public String getIctMonitorSerNo() {
		return ictMonitorSerNo;
	}
	public void setIctMonitorSerNo(String ictMonitorSerNo) {
		this.ictMonitorSerNo = ictMonitorSerNo;
	}
	public String getMcValveModuleSerNo() {
		return mcValveModuleSerNo;
	}
	public void setMcValveModuleSerNo(String mcValveModuleSerNo) {
		this.mcValveModuleSerNo = mcValveModuleSerNo;
	}
	public Integer getNonAnalysisFlg() {
		return nonAnalysisFlg;
	}
	public void setNonAnalysisFlg(Integer nonAnalysisFlg) {
		this.nonAnalysisFlg = nonAnalysisFlg;
	}
	public String getAdditionalFunction1() {
		return additionalFunction1;
	}
	public void setAdditionalFunction1(String additionalFunction1) {
		this.additionalFunction1 = additionalFunction1;
	}
	public String getPositionPostCd() {
		return positionPostCd;
	}
	public void setPositionPostCd(String positionPostCd) {
		this.positionPostCd = positionPostCd;
	}
	public String getPositionCountryCd() {
		return positionCountryCd;
	}
	public void setPositionCountryCd(String positionCountryCd) {
		this.positionCountryCd = positionCountryCd;
	}
	public String getPositionArea1() {
		return positionArea1;
	}
	public void setPositionArea1(String positionArea1) {
		this.positionArea1 = positionArea1;
	}
	public String getPositionArea2() {
		return positionArea2;
	}
	public void setPositionArea2(String positionArea2) {
		this.positionArea2 = positionArea2;
	}
	public String getPositionArea3() {
		return positionArea3;
	}
	public void setPositionArea3(String positionArea3) {
		this.positionArea3 = positionArea3;
	}
	public Integer getEiseiFlg() {
		return eiseiFlg;
	}
	public void setEiseiFlg(Integer eiseiFlg) {
		this.eiseiFlg = eiseiFlg;
	}
	public Integer getWheelFlg() {
		return wheelFlg;
	}
	public void setWheelFlg(Integer wheelFlg) {
		this.wheelFlg = wheelFlg;
	}
	public String getShortKiban() {
		return shortKiban;
	}
	public void setShortKiban(String shortKiban) {
		this.shortKiban = shortKiban;
	}

	//ªªª 2019/09/30 iDEARº }X^JÇÁ :end  ªªª//



}




