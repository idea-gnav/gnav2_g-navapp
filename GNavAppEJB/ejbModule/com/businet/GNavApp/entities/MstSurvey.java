package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the MST_SURVEY database table.
 *
 */
@Entity
@Table(name="MST_SURVEY")
@NamedQuery(name="MstSurvey.findAll", query="SELECT m FROM MstSurvey m")
public class MstSurvey implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Field
	 */
	@Id
	@Column(name="SURVEY_ID")
	private Integer surveyId;

	@Column(name="SURVEY_NAME0")
	private String surveyName0;
	
	@Column(name="SURVEY_NAME1")
	private String surveyName1;
	
	@Column(name="SURVEY_NAME2")
	private String surveyName2;
	
	@Column(name="SURVEY_NAME3")
	private String surveyName3;
	
	@Column(name="SURVEY_NAME4")
	private String surveyName4;
	
	@Column(name="SURVEY_NAME5")
	private String surveyName5;
	
	@Column(name="INPUT_FROM")
	private Date inputFrom;
	
	@Column(name="INPUT_TO")
	private Date inputTo;
	
	@Column(name="APP_FLG")
	private Integer appFlg;

	@Column(name="DELETE_FLG")
	private Integer deleteFlg;
	
	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;
	
	public MstSurvey() {
	}



	/*
	 * setter, getter
	 */
	public Integer getSurveyId() {
		return this.surveyId;
	}
	
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}
	
	public String getSurveyName0() {
		return this.surveyName0;
	}
	
	public void setSurveyName0(String surveyName0) {
		this.surveyName0 = surveyName0;
	}
	
	public String getSurveyName1() {
		return this.surveyName1;
	}
	
	public void setSurveyName1(String surveyName1) {
		this.surveyName1 = surveyName1;
	}
	
	public String getSurveyName2() {
		return this.surveyName2;
	}
	
	public void setSurveyName2(String surveyName2) {
		this.surveyName2 = surveyName2;
	}
	public String getSurveyName3() {
		return this.surveyName3;
	}
	
	public void setSurveyName3(String surveyName3) {
		this.surveyName3 = surveyName3;
	}
	public String getSurveyName4() {
		return this.surveyName4;
	}
	
	public void setSurveyName4(String surveyName4) {
		this.surveyName4 = surveyName4;
	}
	public String getSurveyName5() {
		return this.surveyName5;
	}
	
	public void setSurveyName5(String surveyName5) {
		this.surveyName5 = surveyName5;
	}
	
	public Date getInputFrom() {
		return this.inputFrom;
	}
	public void setInputFrom(Date inputFrom) {
		this.inputFrom = inputFrom;
	}
	
	public Date getInputTo() {
		return this.inputTo;
	}
	public void setInputTo(Date inputTo) {
		this.inputTo = inputTo;
	}
	
	public Integer getAppFlg() {
		return this.appFlg;
	}
	public void setAppFlg(Integer appFlg) {
		this.appFlg = appFlg;
	}
	
	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}
	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}
	
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
