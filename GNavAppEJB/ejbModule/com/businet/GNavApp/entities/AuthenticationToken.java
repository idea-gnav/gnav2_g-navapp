package com.businet.GNavApp.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the AUTHENTICATION_TOKEN database table.
 * 
 */
@Entity
//@Table(name="AUTHENTICATION_TOKEN", uniqueConstraints = {@UniqueConstraint(columnNames = {"ID"}) })
@Table(name="AUTHENTICATION_TOKEN")
@NamedQuery(name="AuthenticationToken.findAll", query="SELECT a FROM AuthenticationToken a")
public class AuthenticationToken implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private Date createdDate;
	private String deviceId;
	private String loginId;
	private String token;

	public AuthenticationToken() {
	}


	@Id
	@SequenceGenerator(name="AUTHENTICATION_TOKEN_GENERATOR",sequenceName="AUTHENTICATION_TOKEN_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator="AUTHENTICATION_TOKEN_GENERATOR")
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	@Column(name="DEVICE_ID")
	public String getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	@Column(name="LOGIN_ID")
	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}


	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}