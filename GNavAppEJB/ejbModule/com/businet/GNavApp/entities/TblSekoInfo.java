package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TBL_SEKO_INFO database table.
 *
 */
@Entity
@Table(name="TBL_SEKO_INFO")
@NamedQueries({
	@NamedQuery(name="TblSekoInfo.findAll", query="SELECT m FROM TblSekoInfo m"),
})

public class TblSekoInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Field
	 */

	@Id
	@Column(name="KIBAN_SERNO")
	private Long kibanSerno;
	@Column(name="REPORT_NO")
	private Integer reportNo;
	@Column(name="HOUR_METER")
	private Double hourMeter;
	@Column(name="RESEARCH_USER")
	private String researchUser;
	@Column(name="RESEARCH_DATE")
	private Timestamp researchDate;
	@Column(name="RESEARCH_LOCATION")
	private String researchLocation;
	@Column(name="WORK_VOLUME")
	private Double workVolume;
	@Column(name="WORK_VOLUME_UNIT")
	private Integer workVolumeUnit;
	@Column(name="REPORT_COUNTRY")
	private Integer reportCountry;
	@Column(name="MATERIAL_TYPE")
	private Integer materialType;
	@Column(name="MATERIAL_TEMPERATURE_USE")
	private Integer materialTemperatureUse;
	@Column(name="MATERIAL_TEMPERATURE")
	private Double materialTemperature;
	@Column(name="MATERIAL_TEMPERATURE_UNIT")
	private Integer materialTemperatureUnit;
	@Column(name="OUTSIDE_AIR_TEMPERATURE")
	private Double outsideAirTemperature;
	@Column(name="OUTSIDE_AIR_TEMPERATURE_UNIT")
	private Integer outsideAirTemperatureUnit;
	@Column(name="PLATE_TEMPERATURE")
	private Double plateTemperature;
	@Column(name="PLATE_TEMPERATURE_UNIT")
	private Integer plateTemperatureUnit;
	@Column(name="WORK_TYPE")
	private Integer workType;
	@Column(name="SPOT_SITUATION")
	private String spotSituation;
	@Column(name="PAVEMENT_DISTANCE_USE")
	private Integer pavementDistanceUse;
	@Column(name="PAVEMENT_DISTANCE")
	private Double pavementDistance;
	@Column(name="PAVEMENT_WIDTH")
	private Double pavementWidth;
	
	//2020.01.14 DucNKT added
	@Column(name="PAVEMENT_WIDTH_TO")
	private Double pavementWidthTo;
	
	@Column(name="PAVEMENT_THICKNESS_LEVELING_L")
	private Double pavementThicknessLevelingL;
	@Column(name="PAVEMENT_THICKNESS_LEVELING_C")
	private Double pavementThicknessLevelingC;
	@Column(name="PAVEMENT_THICKNESS_LEVELING_R")
	private Double pavementThicknessLevelingR;
	@Column(name="CONSTRUCTION_SPEED")
	private Double constructionSpeed;
	@Column(name="LEVELING_CYLINDER_SCALE_L")
	private Double levelingCylinderScaleL;
	@Column(name="LEVELING_CYLINDER_SCALE_R")
	private Double levelingCylinderScaleR;
	@Column(name="MANUAL_SIXNESS_SCALE_L")
	private Double manualSixnessScaleL;
	@Column(name="MANUAL_SIXNESS_SCALE_R")
	private Double manualSixnessScaleR;
	@Column(name="MANUAL_SIXNESS_SCALE_UNIT")
	private Integer manualSixnessScaleUnit;
	@Column(name="STEP_ADJUSTMENT_SCALE_L")
	private Double stepAdjustmentScaleL;
	@Column(name="STEP_ADJUSTMENT_SCALE_R")
	private Double stepAdjustmentScaleR;
	@Column(name="CROWN")
	private Double crown;
	@Column(name="SLOPE_CROWN_L")
	private Double slopeCrownL;
	@Column(name="SLOPE_CROWN_R")
	private Double slopeCrownR;
	@Column(name="STRETCH_MOLD_BOARD_HEIGHT_L")
	private Double stretchMoldBoardHeightL;
	@Column(name="STRETCH_MOLD_BOARD_HEIGHT_R")
	private Double stretchMoldBoardHeightR;
	@Column(name="TAMPA_ROTATIONS_USE")
	private Integer tampaRotationsUse;
	@Column(name="TAMPA_ROTATIONS")
	private Double tampaRotations;
	@Column(name="TAMPA_AUTO_FUNCTION")
	private Integer tampaAutoFunction;
	@Column(name="VIBRATOR_ROTATIONS")
	private Double vibratorRotations;
	@Column(name="SCREW_HEIGHT")
	private Double screwHeight;
	@Column(name="SCREW_SPEED_SCALE_L")
	private Double screwSpeedScaleL;
	@Column(name="SCREW_SPEED_SCALE_R")
	private Double screwSpeedScaleR;
	@Column(name="CONVEYOR_SPEED_SCALE_L")
	private Double conveyorSpeedScaleL;
	@Column(name="CONVEYOR_SPEED_SCALE_R")
	private Double conveyorSpeedScaleR;
	@Column(name="SCREW_FLOW_CONTROL_USE")
	private Integer screwFlowControlUse;
	@Column(name="AGC_USE")
	private Integer agcUse;
	@Column(name="AGC_TYPE")
	private Integer agcType;
	@Column(name="EXTENSION_SCREW_USE")
	private Integer extensionScrewUse;
	@Column(name="EXTENSION_SCREW")
	private String extensionScrew;
	@Column(name="PAVEMENT_TROUBLE_1")
	private Integer pavementTrouble1;
	@Column(name="PAVEMENT_TROUBLE_2")
	private Integer pavementTrouble2;
	@Column(name="PAVEMENT_TROUBLE_3")
	private Integer pavementTrouble3;
	@Column(name="PAVEMENT_TROUBLE_4")
	private Integer pavementTrouble4;
	@Column(name="PAVEMENT_TROUBLE_5")
	private Integer pavementTrouble5;
	@Column(name="PAVEMENT_TROUBLE_6")
	private Integer pavementTrouble6;
	@Column(name="PAVEMENT_TROUBLE_7")
	private Integer pavementTrouble7;
	@Column(name="PAVEMENT_TROUBLE_8")
	private Integer pavementTrouble8;
	@Column(name="PAVEMENT_TROUBLE_9")
	private Integer pavementTrouble9;
	@Column(name="PAVEMENT_TROUBLE_10")
	private Integer pavementTrouble10;
	@Column(name="PAVEMENT_TROUBLE_11")
	private Integer pavementTrouble11;
	@Column(name="PAVEMENT_TROUBLE_12")
	private Integer pavementTrouble12;
	@Column(name="PAVEMENT_TROUBLE_13")
	private Integer pavementTrouble13;
	@Column(name="PAVEMENT_TROUBLE_14")
	private Integer pavementTrouble14;
	@Column(name="TONNAGE_MIXED_MATERIALS")
	private Double tonnageMixedMaterials;
	@Column(name="CUSTOMER_EVALUATION")
	private String customerEvaluation;
	@Column(name="REAR_SLOPE_L")
	private Double rearSlopeL;
	@Column(name="REAR_SLOPE_R")
	private Double rearSlopeR;
	@Column(name="REAR_ATTACK_SHRINK_L")
	private Double rearAttackShrinkL;
	@Column(name="REAR_ATTACK_SHRINK_R")
	private Double rearAttackShrinkR;
	@Column(name="REAR_ATTACK_MIDDLE_L")
	private Double rearAttackMiddleL;
	@Column(name="REAR_ATTACK_MIDDLE_R")
	private Double rearAttackMiddleR;
	@Column(name="REAR_ATTACK_STRETCH_L")
	private Double rearAttackStretchL;
	@Column(name="REAR_ATTACK_STRETCH_R")
	private Double rearAttackStretchR;
	@Column(name="DELETE_FLAG")
	private Integer deleteFlag;
	@Column(name="REGIST_DTM")
	private Timestamp registDtm;
	@Column(name="REGIST_USER")
	private String registUser;
	@Column(name="REGIST_PRG")
	private String registPrg;
	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;
	@Column(name="UPDATE_USER")
	private String updateUser;
	@Column(name="UPDATE_PRG")
	private String updatePrg;

	
	public TblSekoInfo() {
	}
	
	
	/*
	 * setter, getter  
	 */

	public Long getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}
	public Integer getReportNo() {
		return this.reportNo;
	}
	public void setReportNo(Integer reportNo) {
		this.reportNo = reportNo;
	}
	public Double getHourMeter() {
		return this.hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}
	public String getResearchUser() {
		return this.researchUser;
	}
	public void setResearchUser(String researchUser) {
		this.researchUser = researchUser;
	}
	public Timestamp getResearchDate() {
		return this.researchDate;
	}
	public void setResearchDate(Timestamp researchDate) {
		this.researchDate = researchDate;
	}
	public String getResearchLocation() {
		return this.researchLocation;
	}
	public void setResearchLocation(String researchLocation) {
		this.researchLocation = researchLocation;
	}
	public Double getWorkVolume() {
		return this.workVolume;
	}
	public void setWorkVolume(Double workVolume) {
		this.workVolume = workVolume;
	}
	public Integer getWorkVolumeUnit() {
		return this.workVolumeUnit;
	}
	public void setWorkVolumeUnit(Integer workVolumeUnit) {
		this.workVolumeUnit = workVolumeUnit;
	}
	public Integer getReportCountry() {
		return this.reportCountry;
	}
	public void setReportCountry(Integer reportCountry) {
		this.reportCountry = reportCountry;
	}
	public Integer getMaterialType() {
		return this.materialType;
	}
	public void setMaterialType(Integer materialType) {
		this.materialType = materialType;
	}
	public Integer getMaterialTemperatureUse() {
		return this.materialTemperatureUse;
	}
	public void setMaterialTemperatureUse(Integer materialTemperatureUse) {
		this.materialTemperatureUse = materialTemperatureUse;
	}
	public Double getMaterialTemperature() {
		return this.materialTemperature;
	}
	public void setMaterialTemperature(Double materialTemperature) {
		this.materialTemperature = materialTemperature;
	}
	public Integer getMaterialTemperatureUnit() {
		return this.materialTemperatureUnit;
	}
	public void setMaterialTemperatureUnit(Integer materialTemperatureUnit) {
		this.materialTemperatureUnit = materialTemperatureUnit;
	}
	public Double getOutsideAirTemperature() {
		return this.outsideAirTemperature;
	}
	public void setOutsideAirTemperature(Double outsideAirTemperature) {
		this.outsideAirTemperature = outsideAirTemperature;
	}
	public Integer getOutsideAirTemperatureUnit() {
		return this.outsideAirTemperatureUnit;
	}
	public void setOutsideAirTemperatureUnit(Integer outsideAirTemperatureUnit) {
		this.outsideAirTemperatureUnit = outsideAirTemperatureUnit;
	}
	public Double getPlateTemperature() {
		return this.plateTemperature;
	}
	public void setPlateTemperature(Double plateTemperature) {
		this.plateTemperature = plateTemperature;
	}
	public Integer getPlateTemperatureUnit() {
		return this.plateTemperatureUnit;
	}
	public void setPlateTemperatureUnit(Integer plateTemperatureUnit) {
		this.plateTemperatureUnit = plateTemperatureUnit;
	}
	public Integer getWorkType() {
		return this.workType;
	}
	public void setWorkType(Integer workType) {
		this.workType = workType;
	}
	public String getSpotSituation() {
		return this.spotSituation;
	}
	public void setSpotSituation(String spotSituation) {
		this.spotSituation = spotSituation;
	}
	public Integer getPavementDistanceUse() {
		return this.pavementDistanceUse;
	}
	public void setPavementDistanceUse(Integer pavementDistanceUse) {
		this.pavementDistanceUse = pavementDistanceUse;
	}
	public Double getPavementDistance() {
		return this.pavementDistance;
	}
	public void setPavementDistance(Double pavementDistance) {
		this.pavementDistance = pavementDistance;
	}
	public Double getPavementWidth() {
		return this.pavementWidth;
	}
	public void setPavementWidth(Double pavementWidth) {
		this.pavementWidth = pavementWidth;
	}
	
	public Double getPavementWidthTo() {
		return this.pavementWidthTo;
	}
	public void setPavementWidthTo(Double pavementWidthTo) {
		this.pavementWidthTo = pavementWidthTo;
	}
	
	public Double getPavementThicknessLevelingL() {
		return this.pavementThicknessLevelingL;
	}
	public void setPavementThicknessLevelingL(Double pavementThicknessLevelingL) {
		this.pavementThicknessLevelingL = pavementThicknessLevelingL;
	}
	public Double getPavementThicknessLevelingC() {
		return this.pavementThicknessLevelingC;
	}
	public void setPavementThicknessLevelingC(Double pavementThicknessLevelingC) {
		this.pavementThicknessLevelingC = pavementThicknessLevelingC;
	}
	public Double getPavementThicknessLevelingR() {
		return this.pavementThicknessLevelingR;
	}
	public void setPavementThicknessLevelingR(Double pavementThicknessLevelingR) {
		this.pavementThicknessLevelingR = pavementThicknessLevelingR;
	}
	public Double getConstructionSpeed() {
		return this.constructionSpeed;
	}
	public void setConstructionSpeed(Double constructionSpeed) {
		this.constructionSpeed = constructionSpeed;
	}
	public Double getLevelingCylinderScaleL() {
		return this.levelingCylinderScaleL;
	}
	public void setLevelingCylinderScaleL(Double levelingCylinderScaleL) {
		this.levelingCylinderScaleL = levelingCylinderScaleL;
	}
	public Double getLevelingCylinderScaleR() {
		return this.levelingCylinderScaleR;
	}
	public void setLevelingCylinderScaleR(Double levelingCylinderScaleR) {
		this.levelingCylinderScaleR = levelingCylinderScaleR;
	}
	public Double getManualSixnessScaleL() {
		return this.manualSixnessScaleL;
	}
	public void setManualSixnessScaleL(Double manualSixnessScaleL) {
		this.manualSixnessScaleL = manualSixnessScaleL;
	}
	public Double getManualSixnessScaleR() {
		return this.manualSixnessScaleR;
	}
	public void setManualSixnessScaleR(Double manualSixnessScaleR) {
		this.manualSixnessScaleR = manualSixnessScaleR;
	}
	public Integer getManualSixnessScaleUnit() {
		return this.manualSixnessScaleUnit;
	}
	public void setManualSixnessScaleUnit(Integer manualSixnessScaleUnit) {
		this.manualSixnessScaleUnit = manualSixnessScaleUnit;
	}
	public Double getStepAdjustmentScaleL() {
		return this.stepAdjustmentScaleL;
	}
	public void setStepAdjustmentScaleL(Double stepAdjustmentScaleL) {
		this.stepAdjustmentScaleL = stepAdjustmentScaleL;
	}
	public Double getStepAdjustmentScaleR() {
		return this.stepAdjustmentScaleR;
	}
	public void setStepAdjustmentScaleR(Double stepAdjustmentScaleR) {
		this.stepAdjustmentScaleR = stepAdjustmentScaleR;
	}
	public Double getCrown() {
		return this.crown;
	}
	public void setCrown(Double crown) {
		this.crown = crown;
	}
	public Double getSlopeCrownL() {
		return this.slopeCrownL;
	}
	public void setSlopeCrownL(Double slopeCrownL) {
		this.slopeCrownL = slopeCrownL;
	}
	public Double getSlopeCrownR() {
		return this.slopeCrownR;
	}
	public void setSlopeCrownR(Double slopeCrownR) {
		this.slopeCrownR = slopeCrownR;
	}
	public Double getStretchMoldBoardHeightL() {
		return this.stretchMoldBoardHeightL;
	}
	public void setStretchMoldBoardHeightL(Double stretchMoldBoardHeightL) {
		this.stretchMoldBoardHeightL = stretchMoldBoardHeightL;
	}
	public Double getStretchMoldBoardHeightR() {
		return this.stretchMoldBoardHeightR;
	}
	public void setStretchMoldBoardHeightR(Double stretchMoldBoardHeightR) {
		this.stretchMoldBoardHeightR = stretchMoldBoardHeightR;
	}
	public Integer getTampaRotationsUse() {
		return this.tampaRotationsUse;
	}
	public void setTampaRotationsUse(Integer tampaRotationsUse) {
		this.tampaRotationsUse = tampaRotationsUse;
	}
	public Double getTampaRotations() {
		return this.tampaRotations;
	}
	public void setTampaRotations(Double tampaRotations) {
		this.tampaRotations = tampaRotations;
	}
	public Integer getTampaAutoFunction() {
		return this.tampaAutoFunction;
	}
	public void setTampaAutoFunction(Integer tampaAutoFunction) {
		this.tampaAutoFunction = tampaAutoFunction;
	}
	public Double getVibratorRotations() {
		return this.vibratorRotations;
	}
	public void setVibratorRotations(Double vibratorRotations) {
		this.vibratorRotations = vibratorRotations;
	}
	public Double getScrewHeight() {
		return this.screwHeight;
	}
	public void setScrewHeight(Double screwHeight) {
		this.screwHeight = screwHeight;
	}
	public Double getScrewSpeedScaleL() {
		return this.screwSpeedScaleL;
	}
	public void setScrewSpeedScaleL(Double screwSpeedScaleL) {
		this.screwSpeedScaleL = screwSpeedScaleL;
	}
	public Double getScrewSpeedScaleR() {
		return this.screwSpeedScaleR;
	}
	public void setScrewSpeedScaleR(Double screwSpeedScaleR) {
		this.screwSpeedScaleR = screwSpeedScaleR;
	}
	public Double getConveyorSpeedScaleL() {
		return this.conveyorSpeedScaleL;
	}
	public void setConveyorSpeedScaleL(Double conveyorSpeedScaleL) {
		this.conveyorSpeedScaleL = conveyorSpeedScaleL;
	}
	public Double getConveyorSpeedScaleR() {
		return this.conveyorSpeedScaleR;
	}
	public void setConveyorSpeedScaleR(Double conveyorSpeedScaleR) {
		this.conveyorSpeedScaleR = conveyorSpeedScaleR;
	}
	public Integer getScrewFlowControlUse() {
		return this.screwFlowControlUse;
	}
	public void setScrewFlowControlUse(Integer screwFlowControlUse) {
		this.screwFlowControlUse = screwFlowControlUse;
	}
	public Integer getAgcUse() {
		return this.agcUse;
	}
	public void setAgcUse(Integer agcUse) {
		this.agcUse = agcUse;
	}
	public Integer getAgcType() {
		return this.agcType;
	}
	public void setAgcType(Integer agcType) {
		this.agcType = agcType;
	}
	public Integer getExtensionScrewUse() {
		return this.extensionScrewUse;
	}
	public void setExtensionScrewUse(Integer extensionScrewUse) {
		this.extensionScrewUse = extensionScrewUse;
	}
	public String getExtensionScrew() {
		return this.extensionScrew;
	}
	public void setExtensionScrew(String extensionScrew) {
		this.extensionScrew = extensionScrew;
	}
	public Integer getPavementTrouble1() {
		return this.pavementTrouble1;
	}
	public void setPavementTrouble1(Integer pavementTrouble1) {
		this.pavementTrouble1 = pavementTrouble1;
	}
	public Integer getPavementTrouble2() {
		return this.pavementTrouble2;
	}
	public void setPavementTrouble2(Integer pavementTrouble2) {
		this.pavementTrouble2 = pavementTrouble2;
	}
	public Integer getPavementTrouble3() {
		return this.pavementTrouble3;
	}
	public void setPavementTrouble3(Integer pavementTrouble3) {
		this.pavementTrouble3 = pavementTrouble3;
	}
	public Integer getPavementTrouble4() {
		return this.pavementTrouble4;
	}
	public void setPavementTrouble4(Integer pavementTrouble4) {
		this.pavementTrouble4 = pavementTrouble4;
	}
	public Integer getPavementTrouble5() {
		return this.pavementTrouble5;
	}
	public void setPavementTrouble5(Integer pavementTrouble5) {
		this.pavementTrouble5 = pavementTrouble5;
	}
	public Integer getPavementTrouble6() {
		return this.pavementTrouble6;
	}
	public void setPavementTrouble6(Integer pavementTrouble6) {
		this.pavementTrouble6 = pavementTrouble6;
	}
	public Integer getPavementTrouble7() {
		return this.pavementTrouble7;
	}
	public void setPavementTrouble7(Integer pavementTrouble7) {
		this.pavementTrouble7 = pavementTrouble7;
	}
	public Integer getPavementTrouble8() {
		return this.pavementTrouble8;
	}
	public void setPavementTrouble8(Integer pavementTrouble8) {
		this.pavementTrouble8 = pavementTrouble8;
	}
	public Integer getPavementTrouble9() {
		return this.pavementTrouble9;
	}
	public void setPavementTrouble9(Integer pavementTrouble9) {
		this.pavementTrouble9 = pavementTrouble9;
	}
	public Integer getPavementTrouble10() {
		return this.pavementTrouble10;
	}
	public void setPavementTrouble10(Integer pavementTrouble10) {
		this.pavementTrouble10 = pavementTrouble10;
	}
	public Integer getPavementTrouble11() {
		return this.pavementTrouble11;
	}
	public void setPavementTrouble11(Integer pavementTrouble11) {
		this.pavementTrouble11 = pavementTrouble11;
	}
	public Integer getPavementTrouble12() {
		return this.pavementTrouble12;
	}
	public void setPavementTrouble12(Integer pavementTrouble12) {
		this.pavementTrouble12 = pavementTrouble12;
	}
	public Integer getPavementTrouble13() {
		return this.pavementTrouble13;
	}
	public void setPavementTrouble13(Integer pavementTrouble13) {
		this.pavementTrouble13 = pavementTrouble13;
	}
	public Integer getPavementTrouble14() {
		return this.pavementTrouble14;
	}
	public void setPavementTrouble14(Integer pavementTrouble14) {
		this.pavementTrouble14 = pavementTrouble14;
	}
	public Double getTonnageMixedMaterials() {
		return this.tonnageMixedMaterials;
	}
	public void setTonnageMixedMaterials(Double tonnageMixedMaterials) {
		this.tonnageMixedMaterials = tonnageMixedMaterials;
	}
	public String getCustomerEvaluation() {
		return this.customerEvaluation;
	}
	public void setCustomerEvaluation(String customerEvaluation) {
		this.customerEvaluation = customerEvaluation;
	}
	public Double getRearSlopeL() {
		return this.rearSlopeL;
	}
	public void setRearSlopeL(Double rearSlopeL) {
		this.rearSlopeL = rearSlopeL;
	}
	public Double getRearSlopeR() {
		return this.rearSlopeR;
	}
	public void setRearSlopeR(Double rearSlopeR) {
		this.rearSlopeR = rearSlopeR;
	}
	public Double getRearAttackShrinkL() {
		return this.rearAttackShrinkL;
	}
	public void setRearAttackShrinkL(Double rearAttackShrinkL) {
		this.rearAttackShrinkL = rearAttackShrinkL;
	}
	public Double getRearAttackShrinkR() {
		return this.rearAttackShrinkR;
	}
	public void setRearAttackShrinkR(Double rearAttackShrinkR) {
		this.rearAttackShrinkR = rearAttackShrinkR;
	}
	public Double getRearAttackMiddleL() {
		return this.rearAttackMiddleL;
	}
	public void setRearAttackMiddleL(Double rearAttackMiddleL) {
		this.rearAttackMiddleL = rearAttackMiddleL;
	}
	public Double getRearAttackMiddleR() {
		return this.rearAttackMiddleR;
	}
	public void setRearAttackMiddleR(Double rearAttackMiddleR) {
		this.rearAttackMiddleR = rearAttackMiddleR;
	}
	public Double getRearAttackStretchL() {
		return this.rearAttackStretchL;
	}
	public void setRearAttackStretchL(Double rearAttackStretchL) {
		this.rearAttackStretchL = rearAttackStretchL;
	}
	public Double getRearAttackStretchR() {
		return this.rearAttackStretchR;
	}
	public void setRearAttackStretchR(Double rearAttackStretchR) {
		this.rearAttackStretchR = rearAttackStretchR;
	}
	public Integer getDeleteFlag() {
		return this.deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}
	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}
	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}
	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}
	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

}