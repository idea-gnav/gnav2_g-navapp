package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * MST_EVENT_DETAIL
 *
 */
@Entity
@Table(name="MST_EVENT_DETAIL")
@NamedQuery(name="MstEventDetail.findAll", query="SELECT m FROM MstEventDetail m")
public class MstEventDetail implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */
	@Id
	@Column(name="DETAIL_CODE")
	private String detailCode;

	@Column(name="DETAIL_NAME")
	private String detailName;

	@Column(name="EVENT_NO")
	private BigDecimal eventNo;

	@Column(name="EVENT_SHUBETU")
	private BigDecimal eventShubetu;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="LANGUAGE_CD")
	private String languageCd;

	@Column(name="MACHINE_KBN")
	private BigDecimal machineKbn;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public MstEventDetail() {
	}


	/*
	 * Setter, Getter
	 */
	public String getDetailCode() {
		return this.detailCode;
	}
	public void setDetailCode(String detailCode) {
		this.detailCode = detailCode;
	}

	public String getDetailName() {
		return this.detailName;
	}
	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}

	public BigDecimal getEventNo() {
		return this.eventNo;
	}
	public void setEventNo(BigDecimal eventNo) {
		this.eventNo = eventNo;
	}

	public BigDecimal getEventShubetu() {
		return this.eventShubetu;
	}
	public void setEventShubetu(BigDecimal eventShubetu) {
		this.eventShubetu = eventShubetu;
	}

	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}

	public BigDecimal getMachineKbn() {
		return this.machineKbn;
	}
	public void setMachineKbn(BigDecimal machineKbn) {
		this.machineKbn = machineKbn;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}