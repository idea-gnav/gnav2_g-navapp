package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="TBL_DOCUMENTS")
@NamedQuery(name="TblDocuments.findAll", query="SELECT t FROM TblDocuments t")
public class TblDocuments implements Serializable {
	private static final long serialVersionUID = 1L;
	

	/*
	 * Field 
	 */
	@Id
	@Column(name="DOCU_ID")
	private Integer docuId;

	@Column(name="APP_FLG")
	private Integer appFlg;	

	@Column(name="TARGET_USER_ID")
	private String targetUserId;	
	
	@Column(name="TARGET_KENGEN_CD")
	private String targetKengenCd;
	
	@Column(name="LANGUAGE_CD")
	private String languageCd;
	
	@Column(name="DOCU_NAME")
	private String docuName;
	
	@Column(name="DOCU_PATH")
	private String docuPath;
	
	@Column(name="OP_DATE")
	private Date opDate;
	
	@Column(name="ED_DATE")
	private Date edDate;
	
	@Column(name="REMARKS")
	private String remarks;
	
	@Column(name="DEL_FLG")
	private Integer delFlg;
	
	
	
	
	public TblDocuments() {
	}
	
	
	/*
	 * setter, getter  
	 */

	public Integer getDocuId() {
		return this.docuId;
	}
	public void setDocuId(Integer docuId) {
		this.docuId = docuId;
	}
	
	public Integer getAppFlg() {
		return this.appFlg;
	}
	public void setAppFlg(Integer appFlg) {
		this.appFlg = appFlg;
	}
	
	public String getTargetUserId() {
		return this.targetUserId;
	}
	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}
	
	public String getTargetKengenCd() {
		return this.targetKengenCd;
	}
	public void setTargetKengenCd(String targetKengenCd) {
		this.targetKengenCd = targetKengenCd;
	}
	
	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}
	
	public String getDocuName() {
		return this.docuName;
	}
	public void setDocuName(String docuName) {
		this.docuName = docuName;
	}
	
	public String getDocuPath() {
		return this.docuPath;
	}
	public void setDocuPath(String docuPath) {
		this.docuPath = docuPath;
	}
	
	public Date getOpDate() {
		return this.opDate;
	}
	public void setOpDate(Date opDate) {
		this.opDate = opDate;
	}
	
	public Date getEdDate() {
		return this.edDate;
	}
	public void setEdDate(Date edDate) {
		this.edDate = edDate;
	}
	
	public String getRemarks() {
		return this.remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}
	

}
