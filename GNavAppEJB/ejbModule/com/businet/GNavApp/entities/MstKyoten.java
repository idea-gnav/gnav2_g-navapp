package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the MST_KENGEN database table.
 *
 */
@Entity
@Table(name="MST_KYOTEN")
@NamedQuery(name="MstKyoten.findAll", query="SELECT m FROM MstKyoten m")
public class MstKyoten implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */

	@Id
	@Column(name="KYOTEN_CD")
	private String kyotenCd;

	@Column(name="KYOTEN_NAME")
	private String kyotenName;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;









	public MstKyoten() {
	}



	/*
	 * Getter, Setter
	 */

	public String getKyotenCd() {
		return this.kyotenCd;
	}
	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

	public String getKyotenName() {
		return this.kyotenName;
	}
	public void setKyotenName(String kyotenName) {
		this.kyotenName = kyotenName;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}



}