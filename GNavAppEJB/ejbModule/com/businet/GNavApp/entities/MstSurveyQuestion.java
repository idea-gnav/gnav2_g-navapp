package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the MST_SURVEY_QUESTION database table.
 *
 */
@Entity
@Table(name = "MST_SURVEY_QUESTION")
@NamedQuery(name = "MstSurveyQuestion.findAll", query = "SELECT m FROM MstSurveyQuestion m")
public class MstSurveyQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Field
	 */
	@Id
	@Column(name = "QUESTION_ID")
	private Integer questionId;

	@Column(name = "SURVEY_ID")
	private Integer surveyId;

	@Column(name = "QUESTION0")
	private String question0;

	@Column(name = "QUESTION1")
	private String question1;

	@Column(name = "QUESTION2")
	private String question2;

	@Column(name = "QUESTION3")
	private String question3;

	@Column(name = "QUESTION4")
	private String question4;

	@Column(name = "QUESTION5")
	private String question5;

	@Column(name = "QUESTION_PATTERN")
	private Integer questionPattern;

	@Column(name = "DISPLAY_SORT")
	private Integer displaySort;

	@Column(name = "PARENT_FLG")
	private Integer parentFlg;

	@Column(name = "FREE_INPUT_FLG")
	private Integer freeInputFlg;

	@Column(name = "FREE_INPUT_CAPTION0")
	private String freeInputCaption0;

	@Column(name = "FREE_INPUT_CAPTION1")
	private String freeInputCaption1;

	@Column(name = "FREE_INPUT_CAPTION2")
	private String freeInputCaption2;

	@Column(name = "FREE_INPUT_CAPTION3")
	private String freeInputCaption3;

	@Column(name = "FREE_INPUT_CAPTION4")
	private String freeInputCaption4;

	@Column(name = "FREE_INPUT_CAPTION5")
	private String freeInputCaption5;

	@Column(name = "FREE_INPUT_CHOICE_ID")
	private Integer freeInputChoiceId;

	@Column(name = "MUST_ANSWER_FLG")
	private Integer mustAnswerFlg;

	@Column(name = "MULTI_CHOICE_MAX")
	private Integer multiChoiceMax;

	@Column(name = "KENGEN_CD")
	private String kengenCd;

	@Column(name = "DELETE_FLG")
	private Integer deleteFlg;

	@Column(name = "REGIST_USER")
	private String registUser;

	@Column(name = "REGIST_PRG")
	private String registPrg;

	@Column(name = "UPDATE_USER")
	private String updateUser;

	@Column(name = "UPDATE_PRG")
	private String updatePrg;

	@Column(name = "REGIST_DTM")
	private Timestamp registDtm;

	@Column(name = "UPDATE_DTM")
	private Timestamp updateDtm;

	public MstSurveyQuestion() {
	}

	/*
	 * setter, getter
	 */

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}

	public String getQuestion0() {
		return question0;
	}

	public void setQuestion0(String question0) {
		this.question0 = question0;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public String getQuestion4() {
		return question4;
	}

	public void setQuestion4(String question4) {
		this.question4 = question4;
	}

	public String getQuestion5() {
		return question5;
	}

	public void setQuestion5(String question5) {
		this.question5 = question5;
	}

	public Integer getQuestionPattern() {
		return questionPattern;
	}

	public void setQuestionPattern(Integer questionPattern) {
		this.questionPattern = questionPattern;
	}

	public Integer getDisplaySort() {
		return displaySort;
	}

	public void setDisplaySort(Integer displaySort) {
		this.displaySort = displaySort;
	}

	public Integer getParentFlg() {
		return parentFlg;
	}

	public void setParentFlg(Integer parentFlg) {
		this.parentFlg = parentFlg;
	}

	public Integer getFreeInputFlg() {
		return freeInputFlg;
	}

	public void setFreeInputFlg(Integer freeInputFlg) {
		this.freeInputFlg = freeInputFlg;
	}

	public String getFreeInputCaption0() {
		return freeInputCaption0;
	}

	public void setFreeInputCaption0(String freeInputCaption0) {
		this.freeInputCaption0 = freeInputCaption0;
	}

	public String getFreeInputCaption1() {
		return freeInputCaption1;
	}

	public void setFreeInputCaption1(String freeInputCaption1) {
		this.freeInputCaption1 = freeInputCaption1;
	}

	public String getFreeInputCaption2() {
		return freeInputCaption2;
	}

	public void setFreeInputCaption2(String freeInputCaption2) {
		this.freeInputCaption2 = freeInputCaption2;
	}

	public String getFreeInputCaption3() {
		return freeInputCaption3;
	}

	public void setFreeInputCaption3(String freeInputCaption3) {
		this.freeInputCaption3 = freeInputCaption3;
	}

	public String getFreeInputCaption4() {
		return freeInputCaption4;
	}

	public void setFreeInputCaption4(String freeInputCaption4) {
		this.freeInputCaption4 = freeInputCaption4;
	}

	public String getFreeInputCaption5() {
		return freeInputCaption5;
	}

	public void setFreeInputCaption5(String freeInputCaption5) {
		this.freeInputCaption5 = freeInputCaption5;
	}

	public Integer getFreeInputChoiceId() {
		return freeInputChoiceId;
	}

	public void setFreeInputChoiceId(Integer freeInputChoiceId) {
		this.freeInputChoiceId = freeInputChoiceId;
	}

	public Integer getMustAnswerFlg() {
		return mustAnswerFlg;
	}

	public void setMustAnswerFlg(Integer mustAnswerFlg) {
		this.mustAnswerFlg = mustAnswerFlg;
	}

	public Integer getMultiChoiceMax() {
		return multiChoiceMax;
	}

	public void setMultiChoiceMax(Integer multiChoiceMax) {
		this.multiChoiceMax = multiChoiceMax;
	}

	public String getKengenCd() {
		return kengenCd;
	}

	public void setKengenCd(String kengenCd) {
		this.kengenCd = kengenCd;
	}

	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}

	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
