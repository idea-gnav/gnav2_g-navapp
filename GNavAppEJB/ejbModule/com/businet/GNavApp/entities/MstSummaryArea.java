package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


@Entity
@Table(name="MST_SUMMARY_AREA")
@NamedQuery(name="MstSummaryArea.findAll", query="SELECT m FROM MstSummaryArea m")
public class MstSummaryArea implements Serializable {
	private static final long serialVersionUID = 1L;

	
	/*
	 * Mapping
	 */
	@ManyToOne
	@JoinColumn(name="COUNTRY_CD", insertable=false, updatable=false)  /// --> foreig key
	private MstSummaryCountry mstSummaryCountry;
	public MstSummaryCountry getMstSummaryCountry() {
		return mstSummaryCountry;
	}
	public void setMstSummaryCountry(MstSummaryCountry mstSummaryCountry) {
		this.mstSummaryCountry = mstSummaryCountry;
	}

	
	
	
	/*
	 * Field 
	 */
	@Column(name="COUNTRY_CD")
	private String countryCd;
	
	@Id
	@Column(name="AREA_CD")
	private String areaCd;
	
	@Column(name="LANGUAGE_CD")
	private String languageCd;
	
	@Column(name="AREA_NM")
	private String areaNm;
	
	@Column(name="IDO")
	private Double ido;
	
	@Column(name="KEIDO")
	private Double keido;

	@Column(name="DELETE_FLG")
	private Integer deleteFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	
	
	public MstSummaryArea() {
	}

	
	/*
	 * setter, getter
	 */
	public String getCountryCd() {
		return this.countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	
	
	public String getAreaCd() {
		return this.areaCd;
	}
	public void setAreaCd(String areaCd) {
		this.areaCd = areaCd;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}
	
	public String getAreaNm() {
		return this.areaNm;
	}
	public void setAreaNm(String areaNm) {
		this.areaNm = areaNm;
	}
	
	public Double getIdo() {
		return this.ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}
	
	public Double getKeido() {
		return this.keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}
	
	public Integer getDelFlg() {
		return this.deleteFlg;
	}
	public void setDelFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}
	
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}
	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
}