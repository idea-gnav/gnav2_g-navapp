package com.businet.GNavApp.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the MST_KATASHIKI database table.
 * 
 */
@Entity
@Table(name="MST_KATASHIKI")
@NamedQuery(name="MstKatashiki.findAll", query="SELECT m FROM MstKatashiki m")
public class MstKatashiki implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Field
	 */
	@Id
	private String katashiki;

	@Column(name="AIR_BREATHER_ELEMENT_TIME")
	private Integer airBreatherElementTime;

	@Column(name="AIR_CLEANER_ELEMENT_TIME")
	private Integer airCleanerElementTime;

	@Column(name="CLASS")
	private Integer class_;

	@Column(name="COEFFICIENT_TONS")
	private Integer coefficientTons;

	@Column(name="DELETE_FLAG")
	private Integer deleteFlag;

	@Column(name="DOSING_MODULE_TIME")
	private Integer dosingModuleTime;

	@Column(name="ENGINE_OIL_FILTER_TIME")
	private Integer engineOilFilterTime;

	@Column(name="ENGINE_OILTIME")
	private Integer engineOiltime;

	@Column(name="MATOME_KISHU_CD")
	private String matomeKishuCd;

	@Column(name="MOVEMENT_OIL_TIME")
	private Integer movementOilTime;

	@Column(name="NENRYOU_FILTER_MAIN_TIME")
	private Integer nenryouFilterMainTime;

	@Column(name="NENRYOU_FILTER_PRE_TIME")
	private Integer nenryouFilterPreTime;

	@Column(name="NO_SENSA_TIME")
	private Integer noSensaTime;

	@Column(name="PILOT_OIL_FILTER_TIME")
	private Integer pilotOilFilterTime;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="REPLACE_AIL_CON_FILTER_TIME")
	private Integer replaceAilConFilterTime;

	@Column(name="RETURN_FILTER_TIME")
	private Integer returnFilterTime;

	@Column(name="SEIBI_JIKI_TIME")
	private Integer seibiJikiTime;

	@Column(name="SPLITTER_GEAROIL_TIME")
	private Integer splitterGearoilTime;

	@Column(name="SUCTION_TIME")
	private Integer suctionTime;

	@Column(name="TRAVEL_SPDDOWN_GEAROIL_TIME")
	private Integer travelSpddownGearoilTime;

	@Column(name="TURNING_SD_H_GEAROIL_TIME")
	private Integer turningSdHGearoilTime;

	@Column(name="TURNING_SPDDOWN_GEAROIL_TIME")
	private Integer turningSpddownGearoilTime;

	@Column(name="ULTRA_SMALL_FLAG")
	private Integer ultraSmallFlag;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	
	public MstKatashiki() {
	}

	
	/*
	 * Setter, Getter
	 */
	public String getKatashiki() {
		return this.katashiki;
	}
	public void setKatashiki(String katashiki) {
		this.katashiki = katashiki;
	}

	public Integer getAirBreatherElementTime() {
		return this.airBreatherElementTime;
	}
	public void setAirBreatherElementTime(Integer airBreatherElementTime) {
		this.airBreatherElementTime = airBreatherElementTime;
	}

	public Integer getAirCleanerElementTime() {
		return this.airCleanerElementTime;
	}
	public void setAirCleanerElementTime(Integer airCleanerElementTime) {
		this.airCleanerElementTime = airCleanerElementTime;
	}

	public Integer getClass_() {
		return this.class_;
	}
	public void setClass_(Integer class_) {
		this.class_ = class_;
	}

	public Integer getCoefficientTons() {
		return this.coefficientTons;
	}
	public void setCoefficientTons(Integer coefficientTons) {
		this.coefficientTons = coefficientTons;
	}

	public Integer getDeleteFlag() {
		return this.deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Integer getDosingModuleTime() {
		return this.dosingModuleTime;
	}
	public void setDosingModuleTime(Integer dosingModuleTime) {
		this.dosingModuleTime = dosingModuleTime;
	}

	public Integer getEngineOilFilterTime() {
		return this.engineOilFilterTime;
	}
	public void setEngineOilFilterTime(Integer engineOilFilterTime) {
		this.engineOilFilterTime = engineOilFilterTime;
	}

	public Integer getEngineOiltime() {
		return this.engineOiltime;
	}
	public void setEngineOiltime(Integer engineOiltime) {
		this.engineOiltime = engineOiltime;
	}

	public String getMatomeKishuCd() {
		return this.matomeKishuCd;
	}
	public void setMatomeKishuCd(String matomeKishuCd) {
		this.matomeKishuCd = matomeKishuCd;
	}

	public Integer getMovementOilTime() {
		return this.movementOilTime;
	}
	public void setMovementOilTime(Integer movementOilTime) {
		this.movementOilTime = movementOilTime;
	}

	public Integer getNenryouFilterMainTime() {
		return this.nenryouFilterMainTime;
	}
	public void setNenryouFilterMainTime(Integer nenryouFilterMainTime) {
		this.nenryouFilterMainTime = nenryouFilterMainTime;
	}

	public Integer getNenryouFilterPreTime() {
		return this.nenryouFilterPreTime;
	}
	public void setNenryouFilterPreTime(Integer nenryouFilterPreTime) {
		this.nenryouFilterPreTime = nenryouFilterPreTime;
	}

	public Integer getNoSensaTime() {
		return this.noSensaTime;
	}
	public void setNoSensaTime(Integer noSensaTime) {
		this.noSensaTime = noSensaTime;
	}

	public Integer getPilotOilFilterTime() {
		return this.pilotOilFilterTime;
	}
	public void setPilotOilFilterTime(Integer pilotOilFilterTime) {
		this.pilotOilFilterTime = pilotOilFilterTime;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Integer getReplaceAilConFilterTime() {
		return this.replaceAilConFilterTime;
	}
	public void setReplaceAilConFilterTime(Integer replaceAilConFilterTime) {
		this.replaceAilConFilterTime = replaceAilConFilterTime;
	}

	public Integer getReturnFilterTime() {
		return this.returnFilterTime;
	}
	public void setReturnFilterTime(Integer returnFilterTime) {
		this.returnFilterTime = returnFilterTime;
	}

	public Integer getSeibiJikiTime() {
		return this.seibiJikiTime;
	}
	public void setSeibiJikiTime(Integer seibiJikiTime) {
		this.seibiJikiTime = seibiJikiTime;
	}

	public Integer getSplitterGearoilTime() {
		return this.splitterGearoilTime;
	}
	public void setSplitterGearoilTime(Integer splitterGearoilTime) {
		this.splitterGearoilTime = splitterGearoilTime;
	}

	public Integer getSuctionTime() {
		return this.suctionTime;
	}
	public void setSuctionTime(Integer suctionTime) {
		this.suctionTime = suctionTime;
	}

	public Integer getTravelSpddownGearoilTime() {
		return this.travelSpddownGearoilTime;
	}
	public void setTravelSpddownGearoilTime(Integer travelSpddownGearoilTime) {
		this.travelSpddownGearoilTime = travelSpddownGearoilTime;
	}

	public Integer getTurningSdHGearoilTime() {
		return this.turningSdHGearoilTime;
	}
	public void setTurningSdHGearoilTime(Integer turningSdHGearoilTime) {
		this.turningSdHGearoilTime = turningSdHGearoilTime;
	}

	public Integer getTurningSpddownGearoilTime() {
		return this.turningSpddownGearoilTime;
	}
	public void setTurningSpddownGearoilTime(Integer turningSpddownGearoilTime) {
		this.turningSpddownGearoilTime = turningSpddownGearoilTime;
	}

	public Integer getUltraSmallFlag() {
		return this.ultraSmallFlag;
	}
	public void setUltraSmallFlag(Integer ultraSmallFlag) {
		this.ultraSmallFlag = ultraSmallFlag;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
}