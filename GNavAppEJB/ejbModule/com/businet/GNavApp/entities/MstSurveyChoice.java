package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the MST_SURVEY_CHOICE database table.
 *
 */
@Entity
@Table(name = "MST_SURVEY_CHOICE")
@NamedQuery(name = "MstSurveyChoice.findAll", query = "SELECT m FROM MstSurveyChoice m")
public class MstSurveyChoice implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Field
	 */
	@Id
	@Column(name = "QUESTION_ID")
	private Integer questionId;

	@Column(name = "CHOICE_ID")
	private Integer choiceId;

	@Column(name = "CHOICE0")
	private String choice0;

	@Column(name = "CHOICE1")
	private String choice1;

	@Column(name = "CHOICE2")
	private String choice2;

	@Column(name = "CHOICE3")
	private String choice3;

	@Column(name = "CHOICE4")
	private String choice4;

	@Column(name = "CHOICE5")
	private String choice5;

	@Column(name = "DISPLAY_SORT")
	private Integer displaySort;

	@Column(name = "DELETE_FLG")
	private Integer deleteFlg;

	@Column(name = "REGIST_USER")
	private String registUser;

	@Column(name = "REGIST_PRG")
	private String registPrg;

	@Column(name = "UPDATE_USER")
	private String updateUser;

	@Column(name = "UPDATE_PRG")
	private String updatePrg;

	@Column(name = "REGIST_DTM")
	private Timestamp registDtm;

	@Column(name = "UPDATE_DTM")
	private Timestamp updateDtm;

	public MstSurveyChoice() {
	}

	/*
	 * setter, getter
	 */
	public Integer getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getChoiceId() {
		return choiceId;
	}

	public void setChoiceId(Integer choiceId) {
		this.choiceId = choiceId;
	}

	public String getChoice0() {
		return choice0;
	}

	public void setChoice0(String choice0) {
		this.choice0 = choice0;
	}

	public String getChoice1() {
		return choice1;
	}

	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	public String getChoice2() {
		return choice2;
	}

	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	public String getChoice3() {
		return choice3;
	}

	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	public String getChoice4() {
		return choice4;
	}

	public void setChoice4(String choice4) {
		this.choice4 = choice4;
	}

	public String getChoice5() {
		return choice5;
	}

	public void setChoice5(String choice5) {
		this.choice5 = choice5;
	}

	public Integer getDisplaySort() {
		return displaySort;
	}

	public void setDisplaySort(Integer displaySort) {
		this.displaySort = displaySort;
	}

	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}

	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
