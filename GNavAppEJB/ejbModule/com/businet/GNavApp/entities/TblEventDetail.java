package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_EVENT_DETAIL
 *
 */
@Entity
@Table(name="TBL_EVENT_DETAIL")
@NamedQuery(name="TblEventDetail.findAll", query="SELECT t FROM TblEventDetail t")
public class TblEventDetail implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */
	@Id
	@Column(name="KIBAN_SERNO")
//	private Integer kibanSerno;
	private Long kibanSerno;

	@Column(name="EVENT_NO")
	private Integer eventNo;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="EVENT_SHUBETU")
	private Integer eventShubetu;

	@Column(name="MACHINE_KBN")
	private Integer machineKbn;

	@Column(name="PARENT_RECORD_ID")
	private Integer parentRecordId;

	@Column(name="RECV_TIMESTAMP")
	private Timestamp recvTimestamp;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="RESET_CONTENCT")
	private Integer resetContenct;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public TblEventDetail() {
	}


	/*
	 * Setter, Getter
	 */
	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}

	public Integer getEventNo() {
		return this.eventNo;
	}
	public void setEventNo(Integer eventNo) {
		this.eventNo = eventNo;
	}

	public Integer getEventShubetu() {
		return this.eventShubetu;
	}
	public void setEventShubetu(Integer eventShubetu) {
		this.eventShubetu = eventShubetu;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Integer getMachineKbn() {
		return this.machineKbn;
	}
	public void setMachineKbn(Integer machineKbn) {
		this.machineKbn = machineKbn;
	}

	public Integer getParentRecordId() {
		return this.parentRecordId;
	}
	public void setParentRecordId(Integer parentRecordId) {
		this.parentRecordId = parentRecordId;
	}

	public Timestamp getRecvTimestamp() {
		return this.recvTimestamp;
	}
	public void setRecvTimestamp(Timestamp recvTimestamp) {
		this.recvTimestamp = recvTimestamp;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Integer getResetContenct() {
		return this.resetContenct;
	}
	public void setResetContenct(Integer resetContenct) {
		this.resetContenct = resetContenct;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


}