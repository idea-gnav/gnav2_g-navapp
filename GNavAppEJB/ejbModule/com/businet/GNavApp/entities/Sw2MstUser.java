package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="MST_USER")
@NamedQuery(name="Sw2MstUser.findAll", query="SELECT m FROM MstUser m")
public class Sw2MstUser implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */

	@Id
	@Column(name="ID")
	private Integer id;

	@Column(name="USER_ACCOUNT")
	private String userAccount;

	@Column(name="USER_NAME")
	private String userName;

	@Column(name="LANGUAGE")
	private String language;

	@Column(name="EMAIL")
	private String email;

	@Column(name="ID_DELETE_DATE")
	private Date isDeleteDate;

	@Column(name="ENTRY_APPD_DATE")
	private Date entryAppdDate;

	@Column(name="ONE_TIME_PASSWORD_ISSUE")
	private String oneTimePasswordIssue;

	@Column(name="PASSWORD")
	private String password;

	@Column(name="PASSWORD_LAST_UPDATE_DATE")
	private Date passwordLastUpdateDate;

	@Column(name="ACCOUNT_LOCK_TIMES")
	private Integer accountLockTimes;

	@Column(name="USER_GROUP")
	private String userGroup;

	@Column(name="USER_JOBSYSTEM")
	private String userJobsystem;

	@Column(name="WF_AUTHORITY")
	private String wfAuthority;

	@Column(name="RESPONSIBLE_MODEL_SH")
	private String responsibleModelSh;

	@Column(name="RESPONSIBLE_MODEL_AF")
	private String responsibleModelAf;

	@Column(name="SUPER_USER_AUTHORITY")
	private String superuserAuthority;

	@Column(name="APPD_CANCEL_BUTTON_DISPLAY")
	private String appdCancelButtonDisplay;

	@Column(name="CREATE_DATE")
	private Timestamp createDate;

	@Column(name="CREATE_USER_ID")
	private Integer createUserId;

	@Column(name="CREATE_PROGRAM")
	private String createProgram;

	@Column(name="LAST_UPDATE_DATE")
	private Timestamp lastUpdateDate;

	@Column(name="LAST_UPDATE_USER_ID")
	private Integer lastUpdateUserId;

	@Column(name="LAST_UPDATE_PROGRAM")
	private String lastUpdateProgram;

	@Column(name="INVOICE_CONFIRM_FLG")
	private String invoiceConfirmFlg;

	@Column(name="TASK_RECEIVE")
	private String taskReceive;


	public Sw2MstUser() {
	}



	/*
	 * Getter, Setter
	 */

	public Integer getId() {
		return this.id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserAccount() {
		return this.userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserName() {
		return this.userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLanguage() {
		return this.language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Date getIsDeleteDate() {
		return this.isDeleteDate;
	}
	public void setIsDeleteDate(Date isDeleteDate) {
		this.isDeleteDate = isDeleteDate;
	}

	public Date getEntryAppdDate() {
		return this.entryAppdDate;
	}
	public void setEntryAppdDate(Date entryAppdDate) {
		this.entryAppdDate = entryAppdDate;
	}

	public String getOneTimePasswordIssue() {
		return this.oneTimePasswordIssue;
	}
	public void setOneTimePasswordIssue(String oneTimePasswordIssue) {
		this.oneTimePasswordIssue = oneTimePasswordIssue;
	}

	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Date getPasswordLastUpdateDate() {
		return this.passwordLastUpdateDate;
	}
	public void setPasswordLastUpdateDate(Date passwordLastUpdateDate) {
		this.passwordLastUpdateDate = passwordLastUpdateDate;
	}

	public Integer getAccountLockTimes() {
		return this.accountLockTimes;
	}
	public void setAccountLockTimes(Integer accountLockTimes) {
		this.accountLockTimes = accountLockTimes;
	}

	public String getUserGroup() {
		return this.password;
	}
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}

	public String getUserJobsystem() {
		return this.userJobsystem;
	}
	public void setUserJobsystem(String userJobsystem) {
		this.userJobsystem = userJobsystem;
	}

	public String getWfAuthority() {
		return this.wfAuthority;
	}
	public void setWfAuthority(String wfAuthority) {
		this.wfAuthority = wfAuthority;
	}

	public String getResponsibleModelSh() {
		return this.responsibleModelSh;
	}
	public void setResponsibleModelSh(String responsibleModelSh) {
		this.responsibleModelSh = responsibleModelSh;
	}

	public String getResponsibleModelAf() {
		return this.responsibleModelAf;
	}
	public void setResponsibleModelAf(String responsibleModelAf) {
		this.responsibleModelAf = responsibleModelAf;
	}

	public String getSuperuserAuthority() {
		return this.superuserAuthority;
	}
	public void setSuperuserAuthority(String superuserAuthority) {
		this.superuserAuthority = superuserAuthority;
	}

	public String getAppdCancelButtonDisplay() {
		return this.appdCancelButtonDisplay;
	}
	public void setAppdCancelButtonDisplay(String appdCancelButtonDisplay) {
		this.appdCancelButtonDisplay = appdCancelButtonDisplay;
	}

	public Timestamp getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Integer getCreateUserId() {
		return this.createUserId;
	}
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateProgram() {
		return this.createProgram;
	}
	public void setCreateProgram(String createProgram) {
		this.createProgram = createProgram;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Integer getLastUpdateUserId() {
		return this.lastUpdateUserId;
	}
	public void setLastUpdateUserId(Integer lastUpdateUserId) {
		this.lastUpdateUserId = lastUpdateUserId;
	}

	public String getLastUpdateProgram() {
		return this.lastUpdateProgram;
	}
	public void setLastUpdateProgram(String lastUpdateProgram) {
		this.lastUpdateProgram = lastUpdateProgram;
	}

	public String getInvoiceConfirmFlg() {
		return this.invoiceConfirmFlg;
	}
	public void setInvoiceConfirmFlg(String invoiceConfirmFlg) {
		this.invoiceConfirmFlg = invoiceConfirmFlg;
	}

	public String getTaskReceive() {
		return this.taskReceive;
	}
	public void setTaskReceive(String taskReceive) {
		this.taskReceive = taskReceive;
	}



}