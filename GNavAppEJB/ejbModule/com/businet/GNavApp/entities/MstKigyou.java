package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the MST_KIGYOU database table.
 *
 */
@Entity
@Table(name="MST_KIGYOU")
@NamedQuery(name="MstKigyou.findAll", query="SELECT m FROM MstKigyou m")
public class MstKigyou implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Mapping
	 */

	@OneToMany(mappedBy = "mstKigyou") // class MstMachine => private MstSosiki mstSosiki;
	private List<MstSosiki> mstSosikis;
	public List<MstSosiki> getMstSosikis() {
		return this.mstSosikis;
	}
	public void setMstSosikis(List<MstSosiki> mstSosikis) {
		this.mstSosikis = mstSosikis;
	}
//	public MstSosiki addMstMachine(MstSosiki mstSosiki) {
//		getMstSosikis().add(mstSosiki);
//		mstSosiki.setMstKigyou(this);
//		return mstSosiki;
//	}
//	public MstSosiki removeMstMachine(MstSosiki mstSosiki) {
//		getMstSosikis().remove(mstSosiki);
//		mstSosiki.setMstKigyou(null);
//		return mstSosiki;
//	}




	/*
	 * Field
	 */

	@Id
	@Column(name="KIGYOU_CD")
	private String kigyouCd;

	@Column(name="COUNTRY_CD")
	private String countryCd;

	@Column(name="DELETE_FLG")
	private Integer deleteFlg;

	@Column(name="KIGYOU_ADDR")
	private String kigyouAddr;

	@Column(name="KIGYOU_NAME")
	private String kigyouName;

	@Column(name="KIGYOU_POST")
	private String kigyouPost;

	@Column(name="NENHI_DISP_FLG")
	private Integer nenhiDispFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="RENRAKUSAKI_FAX")
	private String renrakusakiFax;

	@Column(name="RENRAKUSAKI_TEL")
	private String renrakusakiTel;

	@Column(name="STATE_CD")
	private String stateCd;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public MstKigyou() {
	}


	/*
	 * Setter, Getter
	 */
	public String getKigyouCd() {
		return this.kigyouCd;
	}
	public void setKigyouCd(String kigyouCd) {
		this.kigyouCd = kigyouCd;
	}

	public String getCountryCd() {
		return this.countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}
	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public String getKigyouAddr() {
		return this.kigyouAddr;
	}
	public void setKigyouAddr(String kigyouAddr) {
		this.kigyouAddr = kigyouAddr;
	}

	public String getKigyouName() {
		return this.kigyouName;
	}
	public void setKigyouName(String kigyouName) {
		this.kigyouName = kigyouName;
	}

	public String getKigyouPost() {
		return this.kigyouPost;
	}
	public void setKigyouPost(String kigyouPost) {
		this.kigyouPost = kigyouPost;
	}

	public Integer getNenhiDispFlg() {
		return this.nenhiDispFlg;
	}
	public void setNenhiDispFlg(Integer nenhiDispFlg) {
		this.nenhiDispFlg = nenhiDispFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getRenrakusakiFax() {
		return this.renrakusakiFax;
	}
	public void setRenrakusakiFax(String renrakusakiFax) {
		this.renrakusakiFax = renrakusakiFax;
	}

	public String getRenrakusakiTel() {
		return this.renrakusakiTel;
	}
	public void setRenrakusakiTel(String renrakusakiTel) {
		this.renrakusakiTel = renrakusakiTel;
	}

	public String getStateCd() {
		return this.stateCd;
	}
	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}