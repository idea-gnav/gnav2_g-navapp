package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the MST_RESION_SALES_MANAGER database table.
 *
 */
@Entity
@Table(name="MST_RESION_SALES_MANAGER")
@NamedQuery(name="MstResionSalesManager.findAll", query="SELECT m FROM MstResionSalesManager m")
public class MstResionSalesManager implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Mapping
	 */

	@OneToMany(mappedBy = "mstResionSalesManager") // class MstMachine => private MstResionSalesManager mstResionSalesManager;
	private List<MstSosiki> mstSosikis;
	public List<MstSosiki> getMstSosikis() {
		return this.mstSosikis;
	}
	public void setMstSosikis(List<MstSosiki> mstSosikis) {
		this.mstSosikis = mstSosikis;
	}
//	public MstSosiki addMstMachine(MstSosiki mstSosiki) {
//		getMstSosikis().add(mstSosiki);
//		mstSosiki.setMstResionSalesManager(this);
//		return mstSosiki;
//	}
//	public MstSosiki removeMstMachine(MstSosiki mstSosiki) {
//		getMstSosikis().remove(mstSosiki);
//		mstSosiki.setMstResionSalesManager(null);
//		return mstSosiki;
//	}




	/*
	 * Field
	 */

	@Id
	@Column(name="RESION_SALES_MANAGER_CD")
	private String resionSalesManagerCd;

	@Column(name="RESION_SALES_MANAGER_NAME")
	private String resionSalesManagerName;

	@Column(name="COUNTRY_CD")
	private String countryCd;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;




	public MstResionSalesManager() {
	}



	/*
	 * Setter, Getter
	 */
	public String getCountryCd() {
		return this.countryCd;
	}

	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getResionSalesManagerCd() {
		return this.resionSalesManagerCd;
	}

	public void setResionSalesManagerCd(String resionSalesManagerCd) {
		this.resionSalesManagerCd = resionSalesManagerCd;
	}

	public String getResionSalesManagerName() {
		return this.resionSalesManagerName;
	}

	public void setResionSalesManagerName(String resionSalesManagerName) {
		this.resionSalesManagerName = resionSalesManagerName;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}