package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="TBL_USER_STATUS_APP_LOG")
@NamedQuery(name="TblUserStatusAppLog.findAll", query="SELECT t FROM TblUserStatusAppLog t")
public class TblUserStatusAppLog implements Serializable {


	private static final long serialVersionUID = 1L;

	// ユーザーID
	@Id
	@Column(name="USER_ID")
	private String userId;

	// ログイン日時
	@Column(name="USER_LOGIN_DTM")
	private Timestamp userLoginDtm;

	// アプリケーション区分
	@Column(name="APP_FLG")
	private Integer appFlg;

	// 操作モード
	@Column(name="APP_MODE")
	private Integer appMode;

	// 設定言語
	@Column(name="DEVICE_LANGUAGE")
	private String deviceLanguage;

	// デバイス種別
	@Column(name="DEVICE_TYPE")
	private Integer deviceType;

	// 登録日時
	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	// 登録プログラム
	@Column(name="REGIST_PRG")
	private String registPrg;

	// 登録ユーザー
	@Column(name="REGIST_USER")
	private String registUser;


	public TblUserStatusAppLog() {
	}


	// ユーザーID
	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	// ログイン日時
	public Timestamp getUserLoginDtm() {
		return this.userLoginDtm;
	}
	public void setUserLoginDtm(Timestamp userLoginDtm) {
		this.userLoginDtm = userLoginDtm;
	}

	// アプリケーション区分
	public Integer getAppFlg() {
		return this.appFlg;
	}
	public void setAppFlg(Integer appFlg) {
		this.appFlg = appFlg;
	}

	// 操作モード
	public Integer getAppMode() {
		return this.appMode;
	}
	public void setAppMode(Integer appMode) {
		this.appMode = appMode;
	}

	// 設定言語
	public String getDeviceLanguage() {
		return this.deviceLanguage;
	}
	public void setDeviceLanguage(String deviceLanguage) {
		this.deviceLanguage = deviceLanguage;
	}

	// デバイス種別
	public Integer getDeviceType() {
		return this.deviceType;
	}
	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}

	// 登録日時
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	// 登録プログラム
	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	// 登録ユーザー
	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}


}
