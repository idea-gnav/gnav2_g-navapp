package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the MST_KENGEN database table.
 *
 */
@Entity
@Table(name="MST_KENGEN")
@NamedQuery(name="MstKengen.findAll", query="SELECT m FROM MstKengen m")
public class MstKengen implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */

	@Id
	@Column(name="KENGEN_CD")
	private Integer kengenCd;

	@Column(name="KENGEN_NAME")
	private String kengenName;

	@Column(name="PROCESS_TYPE_FLG")
	private Integer processTypeFlg;

	@Column(name="CONNECTION_LOG_FLG")
	private Integer connectionLogFlg;

	@Column(name="DAILY_REPORT_FLG")
	private Integer dailyReportFlg;

	@Column(name="DAY_REPORT_FLG")
	private Integer dayReportFlg;

	@Column(name="DELETE_FLAG")
	private Integer deleteFlag;

	@Column(name="DISTRIBUTION_FLG")
	private Integer distributionFlg;

	@Column(name="DOUKI_HENKOU_TAB_FLG")
	private Integer doukiHenkouTabFlg;

	@Column(name="DOURO_SECURITY_FLG")
	private Integer douroSecurityFlg;

	@Column(name="DOUROKIKAI_FLG")
	private Integer dourokikaiFlg;

	@Column(name="DOWNLOAD_REPORT_FLG")
	private Integer downloadReportFlg;

	@Column(name="ENFORCE_LOCK_DISP_FLG")
	private Integer enforceLockDispFlg;

	@Column(name="ENFORCE_LOCK_LEVEL")
	private Integer enforceLockLevel;

	@Column(name="EVENT_RECV_COMMAND_SEND_FLG")
	private Integer eventRecvCommandSendFlg;

	@Column(name="EVENT_REPORT_FLG")
	private Integer eventReportFlg;

	@Column(name="GEOFENCE_CURFEW_FLG")
	private Integer geofenceCurfewFlg;

	@Column(name="KEIHO_DISPLAY_SET_FILENAME")
	private String keihoDisplaySetFilename;

	@Column(name="KEIHOU_KENGEN")
	private Integer keihouKengen;

	@Column(name="LBX_DAILYREPORT_TAB_X4_FLG")
	private Integer lbxDailyreportTabX4Flg;

	@Column(name="LBX_EDIT_USER")
	private Integer lbxEditUser;

	@Column(name="LIST_DISPLY_FLG")
	private Integer listDisplyFlg;

	@Column(name="MACHINE_PUBLIC_FLG")
	private Integer machinePublicFlg;

	@Column(name="MACHINE_REPORT_FLG")
	private Integer machineReportFlg;

	@Column(name="MAIL_KENGEN_FLG")
	private Integer mailKengenFlg;

	@Column(name="MAINTAINECE_CHANGE_FLG")
	private Integer maintaineceChangeFlg;

	@Column(name="MAINTENANCE_REPORT_FLG")
	private Integer maintenanceReportFlg;

	@Column(name="MASTER_MODEL_FLG")
	private Integer masterModelFlg;

	@Column(name="MONTH_REPORT_FLG")
	private Integer monthReportFlg;

	@Column(name="NENPI_FLG")
	private Integer nenpiFlg;

	@Column(name="NOTICE_CHANGE_FLG")
	private Integer noticeChangeFlg;

	@Column(name="OPERATION_GUIDANCE_PDF_FLG")
	private Integer operationGuidancePdfFlg;

	@Column(name="ORBCOMM_REPORT_FLG")
	private Integer orbcommReportFlg;

	@Column(name="PDF_CONTENT_DISP_FLG")
	private Integer pdfContentDispFlg;

	@Column(name="POSITION_REPORT_FLG")
	private Integer positionReportFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="RIMOTO_LOCK_FLG")
	private Integer rimotoLockFlg;

	@Column(name="SECURITY_FLG")
	private Integer securityFlg;

	@Column(name="SEIGYO_HISTORY_FLG")
	private Integer seigyoHistoryFlg;

	@Column(name="TOUKATUBU_DISP_FLG")
	private Integer toukatubuDispFlg;

	@Column(name="TOUNAN_TUISEKI_FLG")
	private Integer tounanTuisekiFlg;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="WARNHISTORY_FLG")
	private Integer warnhistoryFlg;

	@Column(name="WARNHISTORY_MANAGE_FLG")
	private Integer warnhistoryManageFlg;

	@Column(name="WEEK_REPORT_FLG")
	private Integer weekReportFlg;

	@Column(name="WEEKLY_REPORT_FLG")
	private Integer weeklyReportFlg;

	@Column(name="WORK_REPORT_FLG")
	private Integer workReportFlg;

	@Column(name="YEAR_REPORT_FLG")
	private Integer yearReportFlg;

	@Column(name="ZUKAYIJATA_FLG")
	private Integer zukayijataFlg;





	public MstKengen() {
	}



	/*
	 * Getter, Setter
	 */

	public Integer getConnectionLogFlg() {
		return this.connectionLogFlg;
	}
	public void setConnectionLogFlg(Integer connectionLogFlg) {
		this.connectionLogFlg = connectionLogFlg;
	}

	public Integer getDailyReportFlg() {
		return this.dailyReportFlg;
	}
	public void setDailyReportFlg(Integer dailyReportFlg) {
		this.dailyReportFlg = dailyReportFlg;
	}

	public Integer getDayReportFlg() {
		return this.dayReportFlg;
	}
	public void setDayReportFlg(Integer dayReportFlg) {
		this.dayReportFlg = dayReportFlg;
	}

	public Integer getDeleteFlag() {
		return this.deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Integer getDistributionFlg() {
		return this.distributionFlg;
	}
	public void setDistributionFlg(Integer distributionFlg) {
		this.distributionFlg = distributionFlg;
	}

	public Integer getDoukiHenkouTabFlg() {
		return this.doukiHenkouTabFlg;
	}
	public void setDoukiHenkouTabFlg(Integer doukiHenkouTabFlg) {
		this.doukiHenkouTabFlg = doukiHenkouTabFlg;
	}

	public Integer getDouroSecurityFlg() {
		return this.douroSecurityFlg;
	}
	public void setDouroSecurityFlg(Integer douroSecurityFlg) {
		this.douroSecurityFlg = douroSecurityFlg;
	}

	public Integer getDourokikaiFlg() {
		return this.dourokikaiFlg;
	}
	public void setDourokikaiFlg(Integer dourokikaiFlg) {
		this.dourokikaiFlg = dourokikaiFlg;
	}

	public Integer getDownloadReportFlg() {
		return this.downloadReportFlg;
	}
	public void setDownloadReportFlg(Integer downloadReportFlg) {
		this.downloadReportFlg = downloadReportFlg;
	}

	public Integer getEnforceLockDispFlg() {
		return this.enforceLockDispFlg;
	}
	public void setEnforceLockDispFlg(Integer enforceLockDispFlg) {
		this.enforceLockDispFlg = enforceLockDispFlg;
	}

	public Integer getEnforceLockLevel() {
		return this.enforceLockLevel;
	}
	public void setEnforceLockLevel(Integer enforceLockLevel) {
		this.enforceLockLevel = enforceLockLevel;
	}

	public Integer getEventRecvCommandSendFlg() {
		return this.eventRecvCommandSendFlg;
	}
	public void setEventRecvCommandSendFlg(Integer eventRecvCommandSendFlg) {
		this.eventRecvCommandSendFlg = eventRecvCommandSendFlg;
	}

	public Integer getEventReportFlg() {
		return this.eventReportFlg;
	}
	public void setEventReportFlg(Integer eventReportFlg) {
		this.eventReportFlg = eventReportFlg;
	}

	public Integer getGeofenceCurfewFlg() {
		return this.geofenceCurfewFlg;
	}
	public void setGeofenceCurfewFlg(Integer geofenceCurfewFlg) {
		this.geofenceCurfewFlg = geofenceCurfewFlg;
	}

	public String getKeihoDisplaySetFilename() {
		return this.keihoDisplaySetFilename;
	}
	public void setKeihoDisplaySetFilename(String keihoDisplaySetFilename) {
		this.keihoDisplaySetFilename = keihoDisplaySetFilename;
	}

	public Integer getKeihouKengen() {
		return this.keihouKengen;
	}
	public void setKeihouKengen(Integer keihouKengen) {
		this.keihouKengen = keihouKengen;
	}

	public Integer getKengenCd() {
		return this.kengenCd;
	}
	public void setKengenCd(Integer kengenCd) {
		this.kengenCd = kengenCd;
	}

	public String getKengenName() {
		return this.kengenName;
	}
	public void setKengenName(String kengenName) {
		this.kengenName = kengenName;
	}

	public Integer getLbxDailyreportTabX4Flg() {
		return this.lbxDailyreportTabX4Flg;
	}
	public void setLbxDailyreportTabX4Flg(Integer lbxDailyreportTabX4Flg) {
		this.lbxDailyreportTabX4Flg = lbxDailyreportTabX4Flg;
	}

	public Integer getLbxEditUser() {
		return this.lbxEditUser;
	}
	public void setLbxEditUser(Integer lbxEditUser) {
		this.lbxEditUser = lbxEditUser;
	}

	public Integer getListDisplyFlg() {
		return this.listDisplyFlg;
	}
	public void setListDisplyFlg(Integer listDisplyFlg) {
		this.listDisplyFlg = listDisplyFlg;
	}

	public Integer getMachinePublicFlg() {
		return this.machinePublicFlg;
	}
	public void setMachinePublicFlg(Integer machinePublicFlg) {
		this.machinePublicFlg = machinePublicFlg;
	}

	public Integer getMachineReportFlg() {
		return this.machineReportFlg;
	}
	public void setMachineReportFlg(Integer machineReportFlg) {
		this.machineReportFlg = machineReportFlg;
	}

	public Integer getMailKengenFlg() {
		return this.mailKengenFlg;
	}
	public void setMailKengenFlg(Integer mailKengenFlg) {
		this.mailKengenFlg = mailKengenFlg;
	}

	public Integer getMaintaineceChangeFlg() {
		return this.maintaineceChangeFlg;
	}
	public void setMaintaineceChangeFlg(Integer maintaineceChangeFlg) {
		this.maintaineceChangeFlg = maintaineceChangeFlg;
	}

	public Integer getMaintenanceReportFlg() {
		return this.maintenanceReportFlg;
	}
	public void setMaintenanceReportFlg(Integer maintenanceReportFlg) {
		this.maintenanceReportFlg = maintenanceReportFlg;
	}

	public Integer getMasterModelFlg() {
		return this.masterModelFlg;
	}
	public void setMasterModelFlg(Integer masterModelFlg) {
		this.masterModelFlg = masterModelFlg;
	}

	public Integer getMonthReportFlg() {
		return this.monthReportFlg;
	}
	public void setMonthReportFlg(Integer monthReportFlg) {
		this.monthReportFlg = monthReportFlg;
	}

	public Integer getNenpiFlg() {
		return this.nenpiFlg;
	}
	public void setNenpiFlg(Integer nenpiFlg) {
		this.nenpiFlg = nenpiFlg;
	}

	public Integer getNoticeChangeFlg() {
		return this.noticeChangeFlg;
	}
	public void setNoticeChangeFlg(Integer noticeChangeFlg) {
		this.noticeChangeFlg = noticeChangeFlg;
	}

	public Integer getOperationGuidancePdfFlg() {
		return this.operationGuidancePdfFlg;
	}
	public void setOperationGuidancePdfFlg(Integer operationGuidancePdfFlg) {
		this.operationGuidancePdfFlg = operationGuidancePdfFlg;
	}

	public Integer getOrbcommReportFlg() {
		return this.orbcommReportFlg;
	}
	public void setOrbcommReportFlg(Integer orbcommReportFlg) {
		this.orbcommReportFlg = orbcommReportFlg;
	}

	public Integer getPdfContentDispFlg() {
		return this.pdfContentDispFlg;
	}
	public void setPdfContentDispFlg(Integer pdfContentDispFlg) {
		this.pdfContentDispFlg = pdfContentDispFlg;
	}

	public Integer getPositionReportFlg() {
		return this.positionReportFlg;
	}
	public void setPositionReportFlg(Integer positionReportFlg) {
		this.positionReportFlg = positionReportFlg;
	}

	public Integer getProcessTypeFlg() {
		return this.processTypeFlg;
	}
	public void setProcessTypeFlg(Integer processTypeFlg) {
		this.processTypeFlg = processTypeFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Integer getRimotoLockFlg() {
		return this.rimotoLockFlg;
	}
	public void setRimotoLockFlg(Integer rimotoLockFlg) {
		this.rimotoLockFlg = rimotoLockFlg;
	}

	public Integer getSecurityFlg() {
		return this.securityFlg;
	}
	public void setSecurityFlg(Integer securityFlg) {
		this.securityFlg = securityFlg;
	}

	public Integer getSeigyoHistoryFlg() {
		return this.seigyoHistoryFlg;
	}
	public void setSeigyoHistoryFlg(Integer seigyoHistoryFlg) {
		this.seigyoHistoryFlg = seigyoHistoryFlg;
	}

	public Integer getToukatubuDispFlg() {
		return this.toukatubuDispFlg;
	}
	public void setToukatubuDispFlg(Integer toukatubuDispFlg) {
		this.toukatubuDispFlg = toukatubuDispFlg;
	}

	public Integer getTounanTuisekiFlg() {
		return this.tounanTuisekiFlg;
	}
	public void setTounanTuisekiFlg(Integer tounanTuisekiFlg) {
		this.tounanTuisekiFlg = tounanTuisekiFlg;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getWarnhistoryFlg() {
		return this.warnhistoryFlg;
	}
	public void setWarnhistoryFlg(Integer warnhistoryFlg) {
		this.warnhistoryFlg = warnhistoryFlg;
	}

	public Integer getWarnhistoryManageFlg() {
		return this.warnhistoryManageFlg;
	}
	public void setWarnhistoryManageFlg(Integer warnhistoryManageFlg) {
		this.warnhistoryManageFlg = warnhistoryManageFlg;
	}

	public Integer getWeekReportFlg() {
		return this.weekReportFlg;
	}
	public void setWeekReportFlg(Integer weekReportFlg) {
		this.weekReportFlg = weekReportFlg;
	}

	public Integer getWeeklyReportFlg() {
		return this.weeklyReportFlg;
	}
	public void setWeeklyReportFlg(Integer weeklyReportFlg) {
		this.weeklyReportFlg = weeklyReportFlg;
	}

	public Integer getWorkReportFlg() {
		return this.workReportFlg;
	}
	public void setWorkReportFlg(Integer workReportFlg) {
		this.workReportFlg = workReportFlg;
	}

	public Integer getYearReportFlg() {
		return this.yearReportFlg;
	}
	public void setYearReportFlg(Integer yearReportFlg) {
		this.yearReportFlg = yearReportFlg;
	}

	public Integer getZukayijataFlg() {
		return this.zukayijataFlg;
	}
	public void setZukayijataFlg(Integer zukayijataFlg) {
		this.zukayijataFlg = zukayijataFlg;
	}


}