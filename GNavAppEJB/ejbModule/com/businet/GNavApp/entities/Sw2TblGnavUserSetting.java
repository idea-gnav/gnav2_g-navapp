package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_GNAV_USER_SETTING : APP用ユーザー設定テーブル
 *
 */
@Entity
@Table(name="TBL_GNAV_USER_SETTING")
@NamedQuery(name="Sw2TblGnavUserSetting.findAll", query="SELECT t FROM Sw2TblGnavUserSetting t")
public class Sw2TblGnavUserSetting implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */
	@Id
	@Column(name="USER_ID")
	private String userId;

	@Column(name="APPLOG_FLG")
	private Integer applogFlg;

	@Column(name="DEL_FLG")
	private Integer delFlg;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public Sw2TblGnavUserSetting() {
	}


	/*
	 * Setter, Getter
	 */
	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getApplogFlg() {
		return this.applogFlg;
	}
	public void setApplogFlg(Integer applogFlg) {
		this.applogFlg = applogFlg;
	}
	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}
	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


}