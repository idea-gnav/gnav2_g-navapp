package com.businet.GNavApp.entities;

import java.io.Serializable;

public class TblUserFavoritePK implements Serializable {
	public TblUserFavoritePK() {

	}

	private String userId;

//	private Integer kibanSerno;
	private Long kibanSerno;

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}

	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kibanSerno == null) ? 0 : kibanSerno.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TblUserFavoritePK other = (TblUserFavoritePK) obj;
		if (kibanSerno == null) {
			if (other.kibanSerno != null)
				return false;
		} else if (!kibanSerno.equals(other.kibanSerno))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
