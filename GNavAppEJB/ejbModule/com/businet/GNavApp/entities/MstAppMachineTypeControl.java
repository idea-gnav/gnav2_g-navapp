package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="MST_APP_MACHINE_TYPE_CONTROL")
@NamedQuery(name="MstAppMachineTypeControl.findAll", query="SELECT m FROM MstAppMachineTypeControl m")
public class MstAppMachineTypeControl implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */
	@Id
	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="MACHINE_MODEL")
	private Integer machineModel;

	@Column(name="ICON_TYPE")
	private Integer iconType;

	@Column(name="MACHINE_DITAIL_TYPE")
	private Integer machineDitailType;

	@Column(name="DAILY_REPORT_TYPE")
	private Integer dailyReportType;

	@Column(name="GRAPHE_REPORT_FLG")
	private Integer grapheReportFlg;

	@Column(name="MACHINE_GROUP")
	private String machineGroup;

	@Column(name="DEL_FLG")
	private Integer delFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public MstAppMachineTypeControl() {
	}


	/*
	 * Setter, Getter
	 */
	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}

	public Integer getMachineModel() {
		return this.machineModel;
	}
	public void setMachineModel(Integer machineModel) {
		this.machineModel = machineModel;
	}

	public Integer getIconType() {
		return this.iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}

	public Integer getMachineDitailType() {
		return this.machineDitailType;
	}
	public void setMachineDitailType(Integer machineDitailType) {
		this.machineDitailType = machineDitailType;
	}

	public Integer getDailyReportType() {
		return this.dailyReportType;
	}
	public void setDailyReportType(Integer dailyReportType) {
		this.dailyReportType = dailyReportType;
	}

	public Integer getGrapheReportFlg() {
		return this.grapheReportFlg;
	}
	public void setGrapheReportFlg(Integer grapheReportFlg) {
		this.grapheReportFlg = grapheReportFlg;
	}

	public String getMachineGroup() {
		return this.machineGroup;
	}
	public void setMachineGroup(String machineGroup) {
		this.machineGroup = machineGroup;
	}

	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}



}