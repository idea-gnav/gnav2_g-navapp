package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="MST_GYOUSHU")
@NamedQuery(name="MstGyoushu.findAll", query="SELECT m FROM MstGyoushu m")
public class MstGyoushu implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */
	@Id
	@Column(name="GYOUSHU_CD")
	private String gyoushuCd;

	@Column(name="HYOUJI_GENGO_CD")
	private String hyoujiGengoCd;

	@Column(name="GYOUSHU_NM")
	private String gyoushuNm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_DTM")
	private Date registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_DTM")
	private Date updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;


	public MstGyoushu() {
	}


	/*
	 * Setter, Getter
	 */
	public String getGyoushuCd() {
		return this.gyoushuCd;
	}
	public void setGyoushuCd(String gyoushuCd) {
		this.gyoushuCd = gyoushuCd;
	}

	public String getHyoujiGengoCd() {
		return this.hyoujiGengoCd;
	}
	public void setHyoujiGengoCd(String hyoujiGengoCd) {
		this.hyoujiGengoCd = hyoujiGengoCd;
	}

	public String getGyoushuNm() {
		return this.gyoushuNm;
	}
	public void setGyoushuNm(String gyoushuNm) {
		this.gyoushuNm = gyoushuNm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public Date getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Date registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public Date getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Date updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}
	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}




}