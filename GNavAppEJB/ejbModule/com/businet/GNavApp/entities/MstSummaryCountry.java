package com.businet.GNavApp.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(name="MST_SUMMARY_COUNTRY")
@NamedQuery(name="MstSummaryCountry.findAll", query="SELECT m FROM MstSummaryCountry m")
public class MstSummaryCountry implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	
	/*
	 * Mapping
	 */
	@OneToMany(mappedBy = "mstSummaryCountry")  
	private List<MstSummaryArea> mstSummaryArea;
	public List<MstSummaryArea> getMstSummaryAreas() {
		return this.mstSummaryArea;
	}
	public void setMstSummaryAreas(List<MstSummaryArea> mstSummaryArea) {
		this.mstSummaryArea = mstSummaryArea;
	}


	
	
	/*
	 * Field 
	 */
	@Id
	@Column(name="COUNTRY_CD")
	private String countryCd;
	
	@Column(name="LANGUAGE_CD")
	private String languageCd;
	
	@Column(name="COUNTRY_NM")
	private String countryNm;
	
	@Column(name="IDO")
	private Double ido;
	
	@Column(name="KEIDO")
	private Double keido;
	
	@Column(name="DELETE_FLG")
	private Integer deleteFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	
	public MstSummaryCountry() {
	}

	/*
	 * Getter, Setter
	 */
	public String getCountryCd() {
		return this.countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}
	
	public String getCountryNm() {
		return this.countryNm;
	}
	public void setCountryNm(String countryNm) {
		this.countryNm = countryNm;
	}
	
	public Double getIdo() {
		return this.ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}
	
	public Double getKeido() {
		return this.keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}
	
	public Integer getDelFlg() {
		return this.deleteFlg;
	}
	public void setDelFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}
	
	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}