package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="TBL_RETURN_MACHINE_AREA")
@NamedQuery(name="TblReturnMachineArea.findAll", query="SELECT m FROM TblReturnMachineArea m")
public class OLD_TblReturnMachineArea implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */

	@Id
    @Column(name="USER_ID")
    private String userId;

	@Column(name="AREA_NO")
	private Integer areaNo;

	@Column(name="KIGYOU_NAME")
	private String kigyouName;

	@Column(name="AREA_NAME")
	private String areaName;

	@Column(name="RADIUS")
	private Integer radius;

	@Column(name="IDO")
	private Double ido;

	@Column(name="KEIDO")
	private Double keido;

	@Column(name="WARNINGCURRENTLYPROGRESS_FLG")
	private Integer warningCurrentlyProgressFlg;

	@Column(name="PERIODICSERVICENOTICE_FLG")
	private Integer periodicServiceNoticeFLG;

	@Column(name="PERIODICSERVICENOWDUE_FLG")
	private Integer periodicServiceNowDueFLG;

	@Column(name="HOUR_METER_FLG")
	private Integer hourMeterFlg;

	@Column(name="HOUR_METER")
	private Double hourMeter;

	@Column(name="DELETE_FLG")
	private Integer deleteFlg;


	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;


	public OLD_TblReturnMachineArea() {
	}


	/*
	 * setter, getter
	 */

    public String getUserId() {
        return this.userId;
	}
    public void setUserId(String userId) {
        this.userId = userId;
	}

	public Integer getAreaNo() {
		return this.areaNo;
	}
	public void setAreaNo(Integer areaNo) {
		this.areaNo = areaNo;
	}

	public String getKigyouName() {
		return this.kigyouName;
	}
	public void setKigyouName(String kigyouName) {
		this.kigyouName = kigyouName;
	}

	public String getAreaName() {
		return this.areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}


	public Integer getRadius() {
		return this.radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}

	public Double getIdo() {
		return this.ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return this.keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Integer getWarningCurrentlyProgressFlg() {
		return this.warningCurrentlyProgressFlg;
	}
	public void setWarningCurrentlyProgressFlg(Integer warningCurrentlyProgressFlg) {
		this.warningCurrentlyProgressFlg = warningCurrentlyProgressFlg;
	}

	public Integer getPeriodicServiceNoticeFLG() {
		return this.periodicServiceNoticeFLG;
	}
	public void setPeriodicServiceNoticeFLG(Integer periodicServiceNoticeFLG) {
		this.periodicServiceNoticeFLG = periodicServiceNoticeFLG;
	}
	public Integer getPeriodicServiceNowDueFLG() {
		return this.periodicServiceNowDueFLG;
	}
	public void setPeriodicServiceNowDueFLG(Integer periodicServiceNowDueFLG) {
		this.periodicServiceNowDueFLG = periodicServiceNowDueFLG;
	}
	public Integer getHourMeterFlg() {
		return this.hourMeterFlg;
	}
	public void setHourMeterFlg(Integer hourMeterFlg) {
		this.hourMeterFlg = hourMeterFlg;
	}

	public Double getHourMeter() {
		return this.hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Integer getDeleteFlg() {
		return this.deleteFlg;
	}
	public void setDeleteFlg(Integer deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}



}