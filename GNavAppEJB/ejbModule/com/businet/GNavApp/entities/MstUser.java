package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="MST_USER")
@NamedQuery(name="MstUser.findAll", query="SELECT m FROM MstUser m")
public class MstUser implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Mapping
	 */

	@OneToMany(mappedBy="mstUser")
	private List<TblUserDevice> tblUserDevices;

	public List<TblUserDevice> getTblUserDevices() {
		return this.tblUserDevices;
	}
	public void setTblUserDevices(List<TblUserDevice> tblUserDevices) {
		this.tblUserDevices = tblUserDevices;
	}


	@ManyToOne
	@JoinColumn(name="SOSIKI_CD", insertable=false, updatable=false)  /// foreig key
	private MstSosiki mstSosiki;
	public MstSosiki getMstSosiki() {
		return mstSosiki;
	}
	public void setMstSosiki(MstSosiki mstSosiki) {
		this.mstSosiki = mstSosiki;
	}



	/*
	 * Field
	 */

	@Id
	@Column(name="USER_ID")
	private String userId;

	@Column(name="PASSWORD")
	private String password;

	@Column(name="SOSIKI_CD")
	private String sosikiCd;

	@Column(name="KENGEN_CD")
	private Integer kengenCd;

	@Column(name="AREA_CD")
	private String areaCd;

	@Column(name="NEW_PASSWORD_DATE")
	private Timestamp newPasswordDate;

	@Column(name="USER_MAIL_ADDR")
	private String userMailAddr;

	@Column(name="LANGUAGE_CD")
	private String languageCd;

	@Column(name="USER_NAME")
	private String userName;

	@Column(name="USER_BUSHO")
	private String userBusho;

	@Column(name="USER_ADDR")
	private String userAddr;

	@Column(name="USER_TEL")
	private String userTel;

	@Column(name="SERVICE_KOUJOU_CD")
	private String serviceKoujouCd;

	@Column(name="DEL_FLG")
	private Integer delFlg;

	@Column(name="LOGIN_FLG")
	private Integer loginFlg;

	@Column(name="USER_KBN")
	private Integer userKbn;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="USER_KENGEN_CD")
	private Integer userKengenCd;

	@Column(name="UNIT_FLG")
	private Integer unitFlg;

	@Column(name="UNIT_STRING_CD")
	private String unitStringCd;

	@Column(name="RESION_SALES_MANAGER_CD")
	private String resionSalesManagerCd;

	@Column(name="UPDPWD_FLG")
	private String updpwdFlg;

	@Column(name="SEND_FLG")
	private Integer sendFlg;



	public MstUser() {
	}



	/*
	 * Getter, Setter
	 */

	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getSosikiCd() {
		return this.sosikiCd;
	}
	public void setSosikiCd(String sosikiCd) {
		this.sosikiCd = sosikiCd;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}

	public String getUserName() {
		return this.userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserBusho() {
		return this.userBusho;
	}
	public void setUserBusho(String userBusho) {
		this.userBusho = userBusho;
	}

	public String getUserAddr() {
		return this.userAddr;
	}
	public void setUserAddr(String userAddr) {
		this.userAddr = userAddr;
	}

	public String getUserTel() {
		return this.userTel;
	}
	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public String getUserMailAddr() {
		return this.userMailAddr;
	}
	public void setUserMailAddr(String userMailAddr) {
		this.userMailAddr = userMailAddr;
	}

	public String getServiceKoujouCd() {
		return this.serviceKoujouCd;
	}
	public void setServiceKoujouCd(String serviceKoujouCd) {
		this.serviceKoujouCd = serviceKoujouCd;
	}

	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}

	public Integer getLoginFlg() {
		return this.loginFlg;
	}
	public void setLoginFlg(Integer loginFlg) {
		this.loginFlg = loginFlg;
	}

	public Integer getUserKbn() {
		return this.userKbn;
	}
	public void setUserKbn(Integer userKbn) {
		this.userKbn = userKbn;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public Timestamp getNewPasswordDate() {
		return this.newPasswordDate;
	}
	public void setNewPasswordDate(Timestamp newPasswordDate) {
		this.newPasswordDate = newPasswordDate;
	}

	public Integer getUserKengenCd() {
		return this.userKengenCd;
	}
	public void setUserKengenCd(Integer userKengenCd) {
		this.userKengenCd = userKengenCd;
	}

	public Integer getUnitFlg() {
		return this.unitFlg;
	}
	public void setUnitFlg(Integer unitFlg) {
		this.unitFlg = unitFlg;
	}

	public Integer getKengenCd() {
		return this.kengenCd;
	}
	public void setKengenCd(Integer kengenCd) {
		this.kengenCd = kengenCd;
	}

	public String getAreaCd() {
		return this.areaCd;
	}
	public void setAreaCd(String areaCd) {
		this.areaCd = areaCd;
	}

	public String getUnitStringCd() {
		return this.unitStringCd;
	}
	public void setUnitStringCd(String unitStringCd) {
		this.unitStringCd = unitStringCd;
	}

	public String getResionSalesManagerCd() {
		return this.resionSalesManagerCd;
	}
	public void setResionSalesManagerCd(String resionSalesManagerCd) {
		this.resionSalesManagerCd = resionSalesManagerCd;
	}

	public String getUpdpwdFlg() {
		return this.updpwdFlg;
	}
	public void setUpdpwdFlg(String updpwdFlg) {
		this.updpwdFlg = updpwdFlg;
	}

	public Integer getSendFlg() {
		return this.sendFlg;
	}
	public void setSendFlg(Integer sendFlg) {
		this.sendFlg = sendFlg;
	}



}