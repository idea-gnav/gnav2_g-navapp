package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
/**
 * TBL_USER_RETURN_MACHINE : リターンマシーン機番テーブル
 *
 */
@Entity
@IdClass(TblUserReturnMachinePK.class)
@Table(name="TBL_USER_RETURN_MACHINE")
@NamedQuery(name="TblUserReturnMachine.findAll", query="SELECT t FROM TblUserReturnMachine t")
public class TblUserReturnMachine implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Mapping
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// user N : device 1
	@ManyToOne
	@JoinColumn(name="KIBAN_SERNO", insertable=false, updatable=false)
	private MstMachine mstMachine;
	public MstMachine getMstMachine() {
		return this.mstMachine;
	}
	public void setMstMachine(MstMachine mstMachine) {
		this.mstMachine = mstMachine;
	}


	/*
	 * Field
	 */
	@Id
	@Column(name="KYOTEN_CD")
	private String kyotenCd;

	@Id
	@Column(name="KIBAN_SERNO")
//	private Integer kibanSerno;
	private Long kibanSerno;

	@Column(name="DEL_FLG")
	private Integer delFlg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;


	/*
	 * Constructor
	 */
	public TblUserReturnMachine() {
	}


	/*
	 * Setter, Getter
	 */
	public Integer getDelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getKyotenCd() {
		return this.kyotenCd;
	}
	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

}