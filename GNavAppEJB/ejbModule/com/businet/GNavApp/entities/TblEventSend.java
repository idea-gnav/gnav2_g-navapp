package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_EVENT_SEND : イベントテーブル
 *
 */
@Entity
@Table(name="TBL_EVENT_SEND")
@NamedQuery(name="TblEventSend.findAll", query="SELECT t FROM TblEventSend t")
public class TblEventSend implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Mapping
	 */
	@ManyToOne
	@JoinColumn(name="EVENT_NO", insertable=false, updatable=false)
	private MstEventSend mstEventSend;
	public MstEventSend getMstEventSend() {
		return mstEventSend;
	}
	public void setMstEventSend(MstEventSend mstEventSend) {
		this.mstEventSend = mstEventSend;
	}

	/*
	 * Field
	 */
	@Column(name="CAPACITOR_CELL_TEMP")
	private Integer capacitorCellTemp;

	@Column(name="CDMA_SER_NO")
	private Integer cdmaSerNo;

	@Column(name="CDMA_TEL_NO")
	private String cdmaTelNo;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="DEUCE_INSTRUMENT_LV")
	private Integer deuceInstrumentLv;

	@Column(name="ENGINE_MODEL")
	private String engineModel;

	@Column(name="ENGINE_SERNO")
	private Integer engineSerno;

	@Column(name="EVENT_JYOUTAI_FLG")
	private Integer eventJyoutaiFlg;

	@Column(name="EVENT_NO")
	private Integer eventNo;

	@Column(name="EVENT_SHUBETU")
	private Integer eventShubetu;

	@Column(name="EVENT_STATUS")
	private Integer eventStatus;

	@Column(name="FLG_JIDO_FUKKYU")
	private Integer flgJidoFukkyu;

	@Column(name="FUKKYU_TIMESTAMP")
	private Timestamp fukkyuTimestamp;

	@Column(name="FUKKYU_TIMESTAMP_LOCAL")
	private Timestamp fukkyuTimestampLocal;

	@Column(name="HASSEI_TIMESTAMP")
	private Timestamp hasseiTimestamp;

	@Column(name="HASSEI_TIMESTAMP_LOCAL")
	private Timestamp hasseiTimestampLocal;

	@Column(name="HB_COOLING_WATER_TEMP")
	private Integer hbCoolingWaterTemp;

	@Column(name="HOUR_METER")
	private double hourMeter;

	@Column(name="HOUR_METER_SEND")
	private Integer hourMeterSend;

	@Column(name="ICT_TYPE")
	private Integer ictType;

	@Column(name="IDO")
	private Integer ido;

	@Column(name="KEIDO")
	private Integer keido;

	@Column(name="KIBAN")
	private String kiban;

	@Column(name="KIBAN_ADDR")
	private String kibanAddr;

	@Id
	@Column(name="KIBAN_SERNO")
//	private Integer kibanSerno;
	private Long kibanSerno;

	@Column(name="KOUKAN_KANNKAKU")
	private Integer koukanKannkaku;

	@Column(name="KYUKI_ONDO")
	private Integer kyukiOndo;

	@Column(name="MACHINE_KBN")
	private Integer machineKbn;

	@Column(name="MT_BUHIN_NO")
	private Integer mtBuhinNo;

	@Column(name="N_EVENT_NO")
	private String nEventNo;

	@Column(name="REAL_HASSEI_TIMESTAMP")
	private Timestamp realHasseiTimestamp;

	@Column(name="RECORD_ID")
	private Integer recordId;

	@Column(name="RECV_TIMESTAMP")
	private Timestamp recvTimestamp;

	@Column(name="RECV_TIMESTAMP_LOCAL")
	private Timestamp recvTimestampLocal;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="RESET_CONTENCT")
	private Integer resetContenct;

	@Column(name="SEND_FLG")
	private Integer sendFlg;

	@Column(name="SEND_TIME_FROM")
	private String sendTimeFrom;

	@Column(name="SEND_TIME_TO")
	private String sendTimeTo;

	private String shozaiti;

	@Column(name="SHOZAITI_EN")
	private String shozaitiEn;

	@Column(name="SOSIKI_CD")
	private String sosikiCd;

	@Column(name="SYORI_FLG")
	private Integer syoriFlg;

	@Column(name="SYOTI_TANTOUSYA")
	private String syotiTantousya;

	@Column(name="SYOTI_TIMESTAMP")
	private Timestamp syotiTimestamp;

	@Column(name="SYOTI_TIMESTAMP_UTC")
	private Timestamp syotiTimestampUtc;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;




	public TblEventSend() {
	}



	/*
	 * Setter, Getter
	 */
	public Integer getCapacitorCellTemp() {
		return this.capacitorCellTemp;
	}

	public void setCapacitorCellTemp(Integer capacitorCellTemp) {
		this.capacitorCellTemp = capacitorCellTemp;
	}

	public Integer getCdmaSerNo() {
		return this.cdmaSerNo;
	}

	public void setCdmaSerNo(Integer cdmaSerNo) {
		this.cdmaSerNo = cdmaSerNo;
	}

	public String getCdmaTelNo() {
		return this.cdmaTelNo;
	}

	public void setCdmaTelNo(String cdmaTelNo) {
		this.cdmaTelNo = cdmaTelNo;
	}

	public String getConType() {
		return this.conType;
	}

	public void setConType(String conType) {
		this.conType = conType;
	}

	public Integer getDeuceInstrumentLv() {
		return this.deuceInstrumentLv;
	}

	public void setDeuceInstrumentLv(Integer deuceInstrumentLv) {
		this.deuceInstrumentLv = deuceInstrumentLv;
	}

	public String getEngineModel() {
		return this.engineModel;
	}

	public void setEngineModel(String engineModel) {
		this.engineModel = engineModel;
	}

	public Integer getEngineSerno() {
		return this.engineSerno;
	}

	public void setEngineSerno(Integer engineSerno) {
		this.engineSerno = engineSerno;
	}

	public Integer getEventJyoutaiFlg() {
		return this.eventJyoutaiFlg;
	}

	public void setEventJyoutaiFlg(Integer eventJyoutaiFlg) {
		this.eventJyoutaiFlg = eventJyoutaiFlg;
	}

	public Integer getEventNo() {
		return this.eventNo;
	}

	public void setEventNo(Integer eventNo) {
		this.eventNo = eventNo;
	}

	public Integer getEventShubetu() {
		return this.eventShubetu;
	}

	public void setEventShubetu(Integer eventShubetu) {
		this.eventShubetu = eventShubetu;
	}

	public Integer getEventStatus() {
		return this.eventStatus;
	}

	public void setEventStatus(Integer eventStatus) {
		this.eventStatus = eventStatus;
	}

	public Integer getFlgJidoFukkyu() {
		return this.flgJidoFukkyu;
	}

	public void setFlgJidoFukkyu(Integer flgJidoFukkyu) {
		this.flgJidoFukkyu = flgJidoFukkyu;
	}

	public Timestamp getFukkyuTimestamp() {
		return this.fukkyuTimestamp;
	}

	public void setFukkyuTimestamp(Timestamp fukkyuTimestamp) {
		this.fukkyuTimestamp = fukkyuTimestamp;
	}

	public Timestamp getFukkyuTimestampLocal() {
		return this.fukkyuTimestampLocal;
	}

	public void setFukkyuTimestampLocal(Timestamp fukkyuTimestampLocal) {
		this.fukkyuTimestampLocal = fukkyuTimestampLocal;
	}

	public Timestamp getHasseiTimestamp() {
		return this.hasseiTimestamp;
	}

	public void setHasseiTimestamp(Timestamp hasseiTimestamp) {
		this.hasseiTimestamp = hasseiTimestamp;
	}

	public Timestamp getHasseiTimestampLocal() {
		return this.hasseiTimestampLocal;
	}

	public void setHasseiTimestampLocal(Timestamp hasseiTimestampLocal) {
		this.hasseiTimestampLocal = hasseiTimestampLocal;
	}

	public Integer getHbCoolingWaterTemp() {
		return this.hbCoolingWaterTemp;
	}

	public void setHbCoolingWaterTemp(Integer hbCoolingWaterTemp) {
		this.hbCoolingWaterTemp = hbCoolingWaterTemp;
	}

	public double getHourMeter() {
		return this.hourMeter;
	}

	public void setHourMeter(double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Integer getHourMeterSend() {
		return this.hourMeterSend;
	}

	public void setHourMeterSend(Integer hourMeterSend) {
		this.hourMeterSend = hourMeterSend;
	}

	public Integer getIctType() {
		return this.ictType;
	}

	public void setIctType(Integer ictType) {
		this.ictType = ictType;
	}

	public Integer getIdo() {
		return this.ido;
	}

	public void setIdo(Integer ido) {
		this.ido = ido;
	}

	public Integer getKeido() {
		return this.keido;
	}

	public void setKeido(Integer keido) {
		this.keido = keido;
	}

	public String getKiban() {
		return this.kiban;
	}

	public void setKiban(String kiban) {
		this.kiban = kiban;
	}

	public String getKibanAddr() {
		return this.kibanAddr;
	}

	public void setKibanAddr(String kibanAddr) {
		this.kibanAddr = kibanAddr;
	}

	public Long getKibanSerno() {
		return this.kibanSerno;
	}

	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Integer getKoukanKannkaku() {
		return this.koukanKannkaku;
	}

	public void setKoukanKannkaku(Integer koukanKannkaku) {
		this.koukanKannkaku = koukanKannkaku;
	}

	public Integer getKyukiOndo() {
		return this.kyukiOndo;
	}

	public void setKyukiOndo(Integer kyukiOndo) {
		this.kyukiOndo = kyukiOndo;
	}

	public Integer getMachineKbn() {
		return this.machineKbn;
	}

	public void setMachineKbn(Integer machineKbn) {
		this.machineKbn = machineKbn;
	}

	public Integer getMtBuhinNo() {
		return this.mtBuhinNo;
	}

	public void setMtBuhinNo(Integer mtBuhinNo) {
		this.mtBuhinNo = mtBuhinNo;
	}

	public String getNEventNo() {
		return this.nEventNo;
	}

	public void setNEventNo(String nEventNo) {
		this.nEventNo = nEventNo;
	}

	public Timestamp getRealHasseiTimestamp() {
		return this.realHasseiTimestamp;
	}

	public void setRealHasseiTimestamp(Timestamp realHasseiTimestamp) {
		this.realHasseiTimestamp = realHasseiTimestamp;
	}

	public Integer getRecordId() {
		return this.recordId;
	}

	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}

	public Timestamp getRecvTimestamp() {
		return this.recvTimestamp;
	}

	public void setRecvTimestamp(Timestamp recvTimestamp) {
		this.recvTimestamp = recvTimestamp;
	}

	public Timestamp getRecvTimestampLocal() {
		return this.recvTimestampLocal;
	}

	public void setRecvTimestampLocal(Timestamp recvTimestampLocal) {
		this.recvTimestampLocal = recvTimestampLocal;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}

	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}

	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}

	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Integer getResetContenct() {
		return this.resetContenct;
	}

	public void setResetContenct(Integer resetContenct) {
		this.resetContenct = resetContenct;
	}

	public Integer getSendFlg() {
		return this.sendFlg;
	}

	public void setSendFlg(Integer sendFlg) {
		this.sendFlg = sendFlg;
	}

	public String getSendTimeFrom() {
		return this.sendTimeFrom;
	}

	public void setSendTimeFrom(String sendTimeFrom) {
		this.sendTimeFrom = sendTimeFrom;
	}

	public String getSendTimeTo() {
		return this.sendTimeTo;
	}

	public void setSendTimeTo(String sendTimeTo) {
		this.sendTimeTo = sendTimeTo;
	}

	public String getShozaiti() {
		return this.shozaiti;
	}

	public void setShozaiti(String shozaiti) {
		this.shozaiti = shozaiti;
	}

	public String getShozaitiEn() {
		return this.shozaitiEn;
	}

	public void setShozaitiEn(String shozaitiEn) {
		this.shozaitiEn = shozaitiEn;
	}

	public String getSosikiCd() {
		return this.sosikiCd;
	}

	public void setSosikiCd(String sosikiCd) {
		this.sosikiCd = sosikiCd;
	}

	public Integer getSyoriFlg() {
		return this.syoriFlg;
	}

	public void setSyoriFlg(Integer syoriFlg) {
		this.syoriFlg = syoriFlg;
	}

	public String getSyotiTantousya() {
		return this.syotiTantousya;
	}

	public void setSyotiTantousya(String syotiTantousya) {
		this.syotiTantousya = syotiTantousya;
	}

	public Timestamp getSyotiTimestamp() {
		return this.syotiTimestamp;
	}

	public void setSyotiTimestamp(Timestamp syotiTimestamp) {
		this.syotiTimestamp = syotiTimestamp;
	}

	public Timestamp getSyotiTimestampUtc() {
		return this.syotiTimestampUtc;
	}

	public void setSyotiTimestampUtc(Timestamp syotiTimestampUtc) {
		this.syotiTimestampUtc = syotiTimestampUtc;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}

	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}

	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}