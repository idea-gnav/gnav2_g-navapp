package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="MST_TOUKATSUBU")
@NamedQuery(name="MstToukatsubu.findAll", query="SELECT m FROM MstToukatsubu m")
public class MstToukatsubu implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */

	@Id
	@Column(name="TOUKATSUBU_CD")
	private String toukatsubuCd;

	@Column(name="TOUKATSUBU_NAME")
	private String toukatsubuName;

	@Column(name="KYOTEN_CD")
	private String kyotenCd;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public MstToukatsubu() {
	}


	/*
	 * Setter, Getter
	 */

	public String getToukatsubuCd() {
		return this.toukatsubuCd;
	}
	public void setToukatsubuCd(String toukatsubuCd) {
		this.toukatsubuCd = toukatsubuCd;
	}

	public String getToukatsubuName() {
		return this.toukatsubuName;
	}
	public void setToukatsubuName(String toukatsubuName) {
		this.toukatsubuName = toukatsubuName;
	}

	public String getKyotenCd() {
		return this.kyotenCd;
	}
	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getupdatePrg() {
		return this.updatePrg;
	}
	public void setupdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public Timestamp getupdateDtm() {
		return this.updateDtm;
	}
	public void setupdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getupdatetUser() {
		return this.updateUser;
	}
	public void setupdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




}