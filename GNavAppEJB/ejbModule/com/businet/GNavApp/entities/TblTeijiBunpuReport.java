package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_TEIJI_BUNPU_REPORT : 定時分布レポートテーブル
 *
 */
@Entity
@Table(name="TBL_TEIJI_BUNPU_REPORT")
@NamedQuery(name="TblTeijiBunpuReport.findAll", query="SELECT t FROM TblTeijiBunpuReport t")
public class TblTeijiBunpuReport implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */
	@Id
	@Column(name="KIBAN_SERNO")
//	private Integer kibanSerno;
	private Long kibanSerno;

	@Column(name="KIKAI_KADOBI")
	private Timestamp kikaiKadobi;

	@Column(name="KIKAI_KADOBI_LOCAL")
	private Timestamp kikaiKadobiLocal;

	@Column(name="COOLING_TIME1")
	private Integer coolingTime1;



	public TblTeijiBunpuReport() {
	}



	/*
	 * setter, getter
	 */
	public Long getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Timestamp getKikaiKadobi() {
		return this.kikaiKadobi;
	}
	public void setKikaiKadobi(Timestamp kikaiKadobi) {
		this.kikaiKadobi = kikaiKadobi;
	}

	public Timestamp getKikaiKadobiLocal() {
		return this.kikaiKadobiLocal;
	}
	public void setKikaiKadobiLocal(Timestamp kikaiKadobiLocal) {
		this.kikaiKadobiLocal = kikaiKadobiLocal;
	}

	public Integer getCoolingTime1() {
		return this.coolingTime1;
	}
	public void setCoolingTime1(Integer coolingTime1) {
		this.coolingTime1 = coolingTime1;
	}



}