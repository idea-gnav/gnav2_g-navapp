package com.businet.GNavApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="MST_HB_CONTROLLER_PART_NO")
@NamedQuery(name="MstHbController.findAll", query="SELECT m FROM MstHbController m")
public class MstHbController implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */

	@Id
	@Column(name="CODE")
	private Integer code;

	@Column(name="NAME")
	private String name;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_USER")
	private String updateUser;

	@Column(name="DELETE_FLAG")
	private Integer deleteFlag;





	public MstHbController() {
	}


	/*
	 * Setter, Getter
	 */


	public Integer getCode() {
		return this.code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getDeleteFlag() {
		return this.deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}



}